package com.parshvaa.isccore.activity.iconnect.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.iconnect.activity.CctTestActivity;
import com.parshvaa.isccore.activity.mcq.PagerListner;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.EvaluterMcqTestMain;
import com.parshvaa.isccore.model.TempMcqTestDtl;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.ISccoreWebView;
import com.parshvaa.isccore.utils.PagerSlidingTabStrip;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */

public class CctTestFrgment extends Fragment {
    public View v;
    private static final String ARG_PAGE_NUMBER = "page_number";
    public ISccoreWebView qWbeView, aWebView, bWebView, cWebView, dWebView;
    public LinearLayout lin_a, lin_b, lin_c, lin_d;
    public TextView tv_q, tv_time_subName, tv_a, tv_b, tv_c, tv_d, ans_a, ans_b, ans_c, ans_d, tv_time_quetion, tv_que_no;
    public CheckBox chk_flag;
    public int pageNumber, pagerTabStriNumber;
    public App app;
    private boolean isViewShown = false;

    private Handler customHandler = new Handler();

    public long updatedTime = 0L;
    public EvaluterMcqTestMain myQuestion;
    public MyDBManager mDb;

    public PagerListner pagerlistner;

    public Fragment newInstance(int page) {
        CctTestFrgment fragment = new CctTestFrgment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(
                R.layout.fragment_question, container, false);
        app = App.getInstance();
        mDb = MyDBManager.getInstance(getActivity());
        mDb.open(getActivity());
        pagerlistner = (PagerListner) getActivity();
        chk_flag = v.findViewById(R.id.chk_flag);

        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
        if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber)
            myQuestion = CctTestActivity.EvalMainArray.get(pageNumber);

        //  pagerTabStriNumber = ((CctTestActivity) getActivity()).pagerTabStri.pager.getCurrentItem();
        chk_flag.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                App.flags.put(pageNumber, true);
            } else {
                App.flags.put(pageNumber, false);
            }

            PagerSlidingTabStrip.setFlags();

        });

        qWbeView = v.findViewById(R.id.qWebView);
        aWebView = v.findViewById(R.id.aWebView);
        bWebView = v.findViewById(R.id.bWebView);
        cWebView = v.findViewById(R.id.cWebView);
        dWebView = v.findViewById(R.id.dWebView);
        lin_a = v.findViewById(R.id.lin_a);
        lin_b = v.findViewById(R.id.lin_b);
        lin_c = v.findViewById(R.id.lin_c);
        lin_d = v.findViewById(R.id.lin_d);
        tv_q = v.findViewById(R.id.tv_q);
        tv_a = v.findViewById(R.id.tv_a);
        tv_b = v.findViewById(R.id.tv_b);
        tv_c = v.findViewById(R.id.tv_c);
        tv_d = v.findViewById(R.id.tv_d);

        tv_que_no = v.findViewById(R.id.tv_que_no);
        tv_time_quetion = v.findViewById(R.id.tv_time_quetion);
        ans_a = v.findViewById(R.id.a_ans);
        ans_b = v.findViewById(R.id.b_ans);
        ans_c = v.findViewById(R.id.c_ans);
        ans_d = v.findViewById(R.id.d_ans);
        setListners();
        if (!isViewShown) {

            setListners();
            setData();
        }


        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (getView() != null) {
            isViewShown = true;
            setListners();
            setData();
        } else {
            isViewShown = false;
        }


    }


    public void setListners() {
        lin_a.setOnClickListener(view -> {
            tv_a.setBackgroundResource(R.drawable.circle_button_blue);

            tv_b.setBackgroundColor(Color.TRANSPARENT);
            tv_a.setTextColor(Color.WHITE);
            tv_b.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.BLACK);
            tv_c.setBackgroundColor(Color.TRANSPARENT);
            tv_d.setBackgroundColor(Color.TRANSPARENT);
            if (myQuestion != null) {

                myQuestion.selectedAns = 1;
                if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                    CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 1;
                    CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("1");
                    if (myQuestion.optionArray.get(0).getIsCorrect().equalsIgnoreCase("1")) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                        CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(0).isRight = true;
                    }
                    CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(0).getMCQOPtionID());
                    selectAns(myQuestion.optionArray.get(0).getMCQOPtionID(), myQuestion.optionArray.get(0).isCorrect);
                }
            }

        });
        ans_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_a.setBackgroundResource(R.drawable.circle_button_blue);

                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_a.setTextColor(Color.WHITE);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                if (myQuestion != null) {
                    myQuestion.selectedAns = 1;
                    if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 1;
                        CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("1");
                        if (myQuestion.optionArray.get(0).getIsCorrect().equalsIgnoreCase("1")) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                            CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(0).isRight = true;
                        }
                        CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(0).getMCQOPtionID());
                        selectAns(myQuestion.optionArray.get(0).getMCQOPtionID(), myQuestion.optionArray.get(0).isCorrect);
                    }
                }
            }
        });
        aWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.aWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tv_a.setBackgroundResource(R.drawable.circle_button_blue);

                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_a.setTextColor(Color.WHITE);
                    tv_b.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    if (myQuestion != null) {
                        myQuestion.selectedAns = 1;
                        if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 1;
                            CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("1");
                            if (myQuestion.optionArray.get(0).getIsCorrect().equalsIgnoreCase("1")) {
                                CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                                CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(0).isRight = true;
                            }
                            CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(0).getMCQOPtionID());
                            selectAns(myQuestion.optionArray.get(0).getMCQOPtionID(), myQuestion.optionArray.get(0).isCorrect);
                        }

                    }
                }
                return false;
            }
        });
        lin_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_b.setBackgroundResource(R.drawable.circle_button_blue);
                tv_a.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_b.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                if (myQuestion != null) {
                    myQuestion.selectedAns = 2;
                    if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 2;
                        CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("2");
                        if (myQuestion.optionArray.get(1).getIsCorrect().equalsIgnoreCase("1")) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                            CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(1).isRight = true;
                        }
                        CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(1).getMCQOPtionID());
                        selectAns(myQuestion.optionArray.get(1).getMCQOPtionID(), myQuestion.optionArray.get(1).isCorrect);
                    }
                }
            }


        });
        ans_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_b.setBackgroundResource(R.drawable.circle_button_blue);
                tv_a.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_b.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                if (myQuestion != null) {
                    myQuestion.selectedAns = 2;
                    if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 2;
                        CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("2");
                        if (myQuestion.optionArray.get(1).getIsCorrect().equalsIgnoreCase("1")) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                            CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(1).isRight = true;
                        }
                        CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(1).getMCQOPtionID());
                        selectAns(myQuestion.optionArray.get(1).getMCQOPtionID(), myQuestion.optionArray.get(1).isCorrect);
                    }
                }
            }


        });
        bWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.bWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tv_b.setBackgroundResource(R.drawable.circle_button_blue);
                    tv_a.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_b.setTextColor(Color.WHITE);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    if (myQuestion != null) {
                        myQuestion.selectedAns = 2;
                        if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 2;
                            CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("2");
                            if (myQuestion.optionArray.get(1).getIsCorrect().equalsIgnoreCase("1")) {
                                CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                                CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(1).isRight = true;
                            }
                            CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(1).getMCQOPtionID());
                            selectAns(myQuestion.optionArray.get(1).getMCQOPtionID(), myQuestion.optionArray.get(1).isCorrect);
                        }
                    }
                }
                return false;
            }
        });

        lin_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_c.setBackgroundResource(R.drawable.circle_button_blue);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.WHITE);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                if (myQuestion != null) {
                    myQuestion.selectedAns = 3;
                    if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 3;
                        CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("3");
                        if (myQuestion.optionArray.get(2).getIsCorrect().equalsIgnoreCase("1")) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                            CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(2).isRight = true;
                        }
                        CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(2).getMCQOPtionID());
                        selectAns(myQuestion.optionArray.get(2).getMCQOPtionID(), myQuestion.optionArray.get(2).isCorrect);
                    }
                }
            }
        });
        ans_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_c.setBackgroundResource(R.drawable.circle_button_blue);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.WHITE);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                if (myQuestion != null) {
                    myQuestion.selectedAns = 3;
                    if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 3;
                        CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("3");
                        if (myQuestion.optionArray.get(2).getIsCorrect().equalsIgnoreCase("1")) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                            CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(2).isRight = true;
                        }
                        CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(2).getMCQOPtionID());
                        selectAns(myQuestion.optionArray.get(2).getMCQOPtionID(), myQuestion.optionArray.get(2).isCorrect);
                    }
                }
            }
        });

        cWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.cWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tv_c.setBackgroundResource(R.drawable.circle_button_blue);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_b.setTextColor(Color.BLACK);
                    tv_a.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.WHITE);
                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    if (myQuestion != null) {
                        myQuestion.selectedAns = 3;
                        if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 3;
                            CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("3");
                            if (myQuestion.optionArray.get(2).getIsCorrect().equalsIgnoreCase("1")) {
                                CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                                CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(2).isRight = true;
                            }
                            CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(2).getMCQOPtionID());
                            selectAns(myQuestion.optionArray.get(2).getMCQOPtionID(), myQuestion.optionArray.get(2).isCorrect);
                        }

                    }
                }
                return false;
            }
        });
        lin_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_d.setBackgroundResource(R.drawable.circle_button_blue);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                if (myQuestion != null) {
                    myQuestion.selectedAns = 4;
                    if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 4;
                        CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("4");
                        if (myQuestion.optionArray.get(3).getIsCorrect().equalsIgnoreCase("1")) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                            CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(3).isRight = true;
                        }
                        CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(3).getMCQOPtionID());
                        selectAns(myQuestion.optionArray.get(3).getMCQOPtionID(), myQuestion.optionArray.get(3).isCorrect);
                    }

                }
            }
        });

        ans_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_d.setBackgroundResource(R.drawable.circle_button_blue);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                if (myQuestion != null) {
                    myQuestion.selectedAns = 4;
                    if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                        CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 4;
                        CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("4");
                        if (myQuestion.optionArray.get(3).getIsCorrect().equalsIgnoreCase("1")) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                            CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(3).isRight = true;
                        }
                        CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(3).getMCQOPtionID());
                        selectAns(myQuestion.optionArray.get(3).getMCQOPtionID(), myQuestion.optionArray.get(3).isCorrect);
                    }

                }
            }
        });
        dWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.dWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tv_d.setBackgroundResource(R.drawable.circle_button_blue);
                    tv_b.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_a.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.WHITE);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    if (myQuestion != null) {
                        myQuestion.selectedAns = 4;
                        if (CctTestActivity.EvalMainArray != null && CctTestActivity.EvalMainArray.size() > pageNumber) {
                            CctTestActivity.EvalMainArray.get(pageNumber).isAttemptedd = 4;
                            CctTestActivity.EvalMainArray.get(pageNumber).setIsAttempt("4");
                            if (myQuestion.optionArray.get(3).getIsCorrect().equalsIgnoreCase("1")) {
                                CctTestActivity.EvalMainArray.get(pageNumber).isRightt = true;
                                CctTestActivity.EvalMainArray.get(pageNumber).optionArray.get(3).isRight = true;
                            }
                            CctTestActivity.EvalMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(3).getMCQOPtionID());
                            selectAns(myQuestion.optionArray.get(3).getMCQOPtionID(), myQuestion.optionArray.get(3).isCorrect);
                        }

                    }
                }
                return false;
            }
        });
    }

    public void selectAns(String selectedAns, String isCorrect) {
        if (myQuestion != null) {
            pagerlistner.onPageVisible(pageNumber);
            TempMcqTestDtl tempmcqdtl = new TempMcqTestDtl();
            tempmcqdtl.setQuestionID(myQuestion.ClassMCQTestHDRID);
            tempmcqdtl.setAnswerID(selectedAns);
            tempmcqdtl.setIsAttempt("1");
            myQuestion.isRightt = isCorrect.equalsIgnoreCase("1");
        }
    }

    public void setData() {
        tv_que_no.setText((pageNumber + 1) + ".");
        if (myQuestion != null) {
            try {
                if ((myQuestion.getQuestion().contains("<img")) || (myQuestion.getQuestion().contains("<math") || (myQuestion.getQuestion().contains("<table")))) {
                    qWbeView.setVisibility(View.VISIBLE);
                    qWbeView.loadHtmlFromLocal(myQuestion.getQuestion());
                } else {
                    tv_q.setText(Html.fromHtml(myQuestion.getQuestion()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (myQuestion != null && myQuestion.optionArray.size() > 0 &&!myQuestion.optionArray.get(0).getOptions().isEmpty() && !myQuestion.optionArray.get(0).getOptions().equals("")) {
                    lin_a.setVisibility(View.VISIBLE);
                    if (myQuestion.optionArray.get(0).getOptions().contains("<img") || myQuestion.optionArray.get(0).getOptions().contains("<math") || myQuestion.optionArray.get(0).getOptions().contains("<table")) {
                        aWebView.setVisibility(View.VISIBLE);
                        aWebView.loadHtmlFromLocal(myQuestion.optionArray.get(0).getOptions());
                    } else {
                        ans_a.setText(Html.fromHtml(myQuestion.optionArray.get(0).getOptions()).toString().trim());

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (myQuestion != null && myQuestion.optionArray.size() > 1 && !myQuestion.optionArray.get(1).getOptions().isEmpty() && !myQuestion.optionArray.get(1).getOptions().equalsIgnoreCase("")) {
                    lin_b.setVisibility(View.VISIBLE);
                    if (myQuestion.optionArray.get(1).getOptions().contains("<img") || myQuestion.optionArray.get(1).getOptions().contains("<math") || myQuestion.optionArray.get(1).getOptions().contains("<table")) {
                        bWebView.setVisibility(View.VISIBLE);
                        bWebView.loadHtmlFromLocal(myQuestion.optionArray.get(1).getOptions());
                    } else {
                        ans_b.setText(Html.fromHtml(myQuestion.optionArray.get(1).getOptions()).toString().trim());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (myQuestion != null && myQuestion.optionArray.size() > 2 &&!myQuestion.optionArray.get(2).getOptions().isEmpty() && !myQuestion.optionArray.get(2).getOptions().equalsIgnoreCase("")) {
                    lin_c.setVisibility(View.VISIBLE);

                    if (myQuestion.optionArray.get(2).getOptions().contains("<img") || myQuestion.optionArray.get(2).getOptions().contains("<math") || myQuestion.optionArray.get(2).getOptions().contains("<table")) {
                        cWebView.setVisibility(View.VISIBLE);
                        cWebView.loadHtmlFromLocal(myQuestion.optionArray.get(2).getOptions());

                    } else {
                        ans_c.setText(Html.fromHtml(myQuestion.optionArray.get(2).getOptions()).toString().trim());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (myQuestion != null && myQuestion.optionArray.size() > 3 &&!myQuestion.optionArray.get(3).getOptions().isEmpty() && !myQuestion.optionArray.get(3).getOptions().equalsIgnoreCase("")) {
                    lin_d.setVisibility(View.VISIBLE);
                    if (myQuestion.optionArray.get(3).getOptions().contains("<img") || myQuestion.optionArray.get(3).getOptions().contains("<math") || myQuestion.optionArray.get(3).getOptions().contains("<table")) {
                        dWebView.setVisibility(View.VISIBLE);
                        ans_d.setVisibility(View.GONE);

                        dWebView.loadHtmlFromLocal(myQuestion.optionArray.get(3).getOptions());

                    } else {
                        ans_d.setGravity(Gravity.CENTER_VERTICAL);
                        ans_d.setText(Html.fromHtml(myQuestion.optionArray.get(3).getOptions()).toString().trim());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (myQuestion.selectedAns != -1) {
                switch (myQuestion.selectedAns) {
                    case 1:
                        tv_a.setBackgroundResource(R.drawable.circle_button_blue);

                        tv_b.setBackgroundColor(Color.TRANSPARENT);
                        tv_a.setTextColor(Color.WHITE);
                        tv_b.setTextColor(Color.BLACK);
                        tv_c.setTextColor(Color.BLACK);
                        tv_d.setTextColor(Color.BLACK);
                        tv_c.setBackgroundColor(Color.TRANSPARENT);
                        tv_d.setBackgroundColor(Color.TRANSPARENT);
                        break;

                    case 2:
                        tv_b.setBackgroundResource(R.drawable.circle_button_blue);
                        tv_a.setTextColor(Color.BLACK);
                        tv_c.setTextColor(Color.BLACK);
                        tv_d.setTextColor(Color.BLACK);
                        tv_b.setTextColor(Color.WHITE);
                        tv_a.setBackgroundColor(Color.TRANSPARENT);
                        tv_c.setBackgroundColor(Color.TRANSPARENT);
                        tv_d.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case 3:
                        tv_c.setBackgroundResource(R.drawable.circle_button_blue);
                        tv_a.setBackgroundColor(Color.TRANSPARENT);
                        tv_b.setTextColor(Color.BLACK);
                        tv_a.setTextColor(Color.BLACK);
                        tv_d.setTextColor(Color.BLACK);
                        tv_c.setTextColor(Color.WHITE);
                        tv_b.setBackgroundColor(Color.TRANSPARENT);
                        tv_d.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case 4:
                        tv_d.setBackgroundResource(R.drawable.circle_button_blue);
                        tv_b.setTextColor(Color.BLACK);
                        tv_c.setTextColor(Color.BLACK);
                        tv_a.setTextColor(Color.BLACK);
                        tv_d.setTextColor(Color.WHITE);
                        tv_a.setBackgroundColor(Color.TRANSPARENT);
                        tv_b.setBackgroundColor(Color.TRANSPARENT);
                        tv_c.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }
            }
        }
    }
}
