package com.parshvaa.isccore.activity.practiespaper.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.practiespaper.adapter.ExamTypeAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.TestType;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.ExamTypesInstruction;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ReadyPaperTypeActivity extends BaseActivity {
    private static final String TAG = "ReadyPaperTypeActivity";
    private List<TestType> testTypeList = new ArrayList<>();
    private RecyclerView recycler_test_type;
    private ExamTypeAdapter testTypeAdapter;
    public ImageView img_back;
    public TextView tv_title;
    public Button btn_next;
    public ReadyPaperTypeActivity instance;
    public String PaperTypeId = "";
    public MyDBManager mDb;
    public ArrayList<ExamTypesInstruction> examTypesArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_ready_paper_type);
        Utils.logUser();


        instance = this;
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        recycler_test_type = findViewById(R.id.recycler_test_type);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        btn_next = findViewById(R.id.btn_next);
        PaperTypeId = getIntent().getExtras().getString("PaperTypeId");
        tv_title.setText("Ready Paper Type");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                App.practiesPaperObj.setExamTypeID(testTypeAdapter.getSelectedItem());
                Utils.Log("Tag", "ExamType :-> " + App.practiesPaperObj.getExamTypeID());
                startActivity(new Intent(instance, PractiesChapterActivity.class)
                        .putExtra("ReadyPaper", "true"));
            }
        });

        getExamType();
        setupRecyclerView();
    }


    public void getExamType() {
        if (App.practiesPaperObj !=null && !TextUtils.isEmpty(App.practiesPaperObj.getSubjectID())){
            Cursor cExamTypes = mDb.getAllRows(DBQueries.getExamtypesID(App.practiesPaperObj.getSubjectID()));
            String examTypesID = "";
            if (cExamTypes != null && cExamTypes.moveToFirst()) {
                do {
                    examTypesID += cExamTypes.getString(0) + ",";
                } while (cExamTypes.moveToNext());

                if (examTypesID.length() > 0) {
                    examTypesID = examTypesID.substring(0, examTypesID.length() - 1);
                }
                Log.d(TAG, "getExamType: "+examTypesID);
                cExamTypes.close();
            }
            try {

                Log.d(TAG, "getExamType: "+App.practiesPaperObj.getPaperTypeID());
                Log.d(TAG, "getExamType: "+App.practiesPaperObj.getSubjectID());
                examTypesArray = (ArrayList) new CustomDatabaseQuery(this, new ExamTypesInstruction())
                        .execute(new String[]{DBNewQuery.getExamTypeTest(examTypesID, App.practiesPaperObj.getPaperTypeID(), App.practiesPaperObj.getSubjectID())}).get();

            } catch (InterruptedException e) {
                Log.d(TAG, "getExamType: "+e);
                e.printStackTrace();
            } catch (ExecutionException e) {

                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getInstriction(String ExamTypeId, String SubjectID) {
        Cursor cInstruction = mDb.getAllRows(DBQueries.getInstruction(ExamTypeId, SubjectID));
        String Instruction = "";
        if (cInstruction != null && cInstruction.moveToFirst()) {
            do {
                Instruction += cInstruction.getString(cInstruction.getColumnIndex("Instruction"));
            } while (cInstruction.moveToNext());
            cInstruction.close();
        }
        if (Instruction.isEmpty()) {
            return "";
        } else {
            return Instruction;
        }
    }

    private void setupRecyclerView() {
        final Context context = recycler_test_type.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._8sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_test_type.setLayoutAnimation(controller);
        recycler_test_type.scheduleLayoutAnimation();
        recycler_test_type.setLayoutManager(new LinearLayoutManager(context));
        Log.d(TAG, "setupRecyclerView: "+examTypesArray.size());
        testTypeAdapter = new ExamTypeAdapter(examTypesArray, context);
        recycler_test_type.setAdapter(testTypeAdapter);
        recycler_test_type.addItemDecoration(new ItemOffsetDecoration(spacing));

    }
}
