package com.parshvaa.isccore.activity.library;

public interface RowType {
    int FOLDER_TYPE = 0;
    int FILE_TYPE = 1;
    int IMAGE_TYPE = 2;
}