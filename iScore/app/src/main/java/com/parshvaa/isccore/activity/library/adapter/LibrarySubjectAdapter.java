package com.parshvaa.isccore.activity.library.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.parshvaa.isccore.activity.library.activity.LibraryChapterActivity;
import com.parshvaa.isccore.activity.library.activity.PersonalListActivity;
import com.parshvaa.isccore.activity.library.Library;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.util.List;

public class LibrarySubjectAdapter extends RecyclerView.Adapter<LibrarySubjectAdapter.MyViewHolder> {

    private List<Library> subjectList;
    public Context context;
    public AQuery aQuery;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name;
        public ImageView img_sub;
        public View lineview;
        public ProgressBar pBar;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.lbl_level_one);
            img_sub = view.findViewById(R.id.img_sub);
            lineview = view.findViewById(R.id.view);
            pBar = view.findViewById(R.id.pBar);
        }
    }


    public LibrarySubjectAdapter(List<Library> moviesList, Context context) {
        this.subjectList = moviesList;
        this.context = context;
        aQuery = new AQuery(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subject_revision_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_subject_name.setText(subjectList.get(position).getFolderName());

        holder.pBar.setVisibility(View.GONE);
        Utils.setImage(context, subjectList.get(position).getFolderIcon(), holder.img_sub, R.drawable.warning);


        // aQuery.id(holder.img_sub).progress(null).image(subjectList.get(position).getFolderIcon(), true, true, 0, R.drawable.profile, null, 0, 0.0f);
        Utils.Log("TAG", "SUBJECT  :-> " + subjectList.get(position).getFolderIcon());
        holder.lineview.setVisibility(View.GONE);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subjectList.get(position).getFolderType().equalsIgnoreCase("system")) {

                    context.startActivity(new Intent(context, LibraryChapterActivity.class)
                            .putExtra("FolderID", subjectList.get(position).getFolderID())
                            .putExtra("SubjectID", subjectList.get(position).getSubjectID())
                            .putExtra("Subject", subjectList.get(position).getFolderName())
                    );
                } else if (subjectList.get(position).getFolderType().equalsIgnoreCase("personal")) {
                    context.startActivity(new Intent(context, PersonalListActivity.class)
                            .putExtra("FolderID", subjectList.get(position).getFolderID())
                            .putExtra("SubjectID", subjectList.get(position).getSubjectID())
                            .putExtra("Subject", subjectList.get(position).getFolderName()));

                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }
}



