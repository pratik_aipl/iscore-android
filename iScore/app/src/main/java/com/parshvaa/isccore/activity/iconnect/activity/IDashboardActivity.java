package com.parshvaa.isccore.activity.iconnect.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bumptech.glide.Glide;
import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.LegalActivity;
import com.parshvaa.isccore.activity.library.activity.LibrarySubjectActivity;
import com.parshvaa.isccore.activity.NotificationActivity;
import com.parshvaa.isccore.activity.profile.MyProfileActivity;
import com.parshvaa.isccore.activity.QueryCallUsActivity;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.activity.referal.ReferalActivity;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.NotificationOneSignal;
import com.parshvaa.isccore.model.NotificationType;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.db.MyDBManagerOneSignal;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.FileDownloader;
import com.parshvaa.isccore.utils.FragmentDrawer;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class IDashboardActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener, AsynchTaskListner {
    public RelativeLayout lin_iSearch, lin_cct, lin_practies_paper, lin_noticeboard, lin_library,lin_lacture;

    private static final String TAG = "IDashboardActivity";
    public IDashboardActivity instance;
    public ImageView img_class_logo;
    public AQuery aQuery;
    public TextView tv_class, tv_class_name, tv_practies_count, tv_cct_count, tv_noticboard_count, tv_lib_count, tv_search_count;
    public ImageView img_back, drawerButton;
    public TextView tv_title;
    public LinearLayout mainLay;
    private FragmentDrawer drawerFragment;
    public MyDBManager mDb;
    public int cct_count = 0, practies_count = 0, noticeboard_count = 0, lib_count = 0;
    public static ArrayList<NotificationOneSignal> OneSignalArray = new ArrayList<>();
    public String ClassName, ClassLogo, ClassRoundLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_idashboard);
        Utils.logUser();

        instance = this;
        aQuery = new AQuery(instance);
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);

        mainLay = findViewById(R.id.mainLay);
        drawerFragment = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerButton = findViewById(R.id.drawerButton);

        img_class_logo = findViewById(R.id.img_class_logo);
        tv_class = findViewById(R.id.tv_class);
        tv_class_name = findViewById(R.id.tv_class_name);
        tv_cct_count = findViewById(R.id.tv_cct_count);
        tv_practies_count = findViewById(R.id.tv_practies_count);
        tv_noticboard_count = findViewById(R.id.tv_noticboard_count);
        tv_lib_count = findViewById(R.id.tv_lib_count);
        tv_search_count = findViewById(R.id.tv_search_count);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Institute Dashboard");

        lin_iSearch = findViewById(R.id.lin_iSearch);
        lin_cct = findViewById(R.id.lin_cct);
        lin_practies_paper = findViewById(R.id.lin_practies_paper);
        lin_noticeboard = findViewById(R.id.lin_noticeboard);
        lin_library = findViewById(R.id.lin_library);
        lin_lacture = findViewById(R.id.lin_lacture);
        showProgress(true, true);
        new CallRequest(instance).get_student_class_dtl();
        tv_class.setText("Class: " + mySharedPref.getStandardName());
        // tv_class_name.setText(mySharedPref.getClassName());
        // new getRecorrd(instance).execute();
        lin_cct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, CctActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        lin_noticeboard.setOnClickListener(v -> startActivity(new Intent(instance, NoticeBoardActivity.class)));
        lin_iSearch.setOnClickListener(v -> startActivity(new Intent(instance, ISearchActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));

        lin_practies_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, TestPaperActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        lin_library.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, LibrarySubjectActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        lin_lacture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, LaturesActivity.class));
            }
        });
        drawerFragment.setUp(R.id.fragment_navigation_drawer, findViewById(R.id.drawer_layout), drawerButton, mainLay);
        drawerFragment.setDrawerListener(this);

    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {

        switch (position) {
            case 0:
                startActivity(new Intent(instance, DashBoardActivity.class));
                break;
            case 1:
                startActivity(new Intent(instance, MyProfileActivity.class));

                break;
            case 2:
                startActivity(new Intent(instance, QueryCallUsActivity.class));
                break;

            case 3:
                startActivity(new Intent(IDashboardActivity.this, ReferalActivity.class));
                break;

            case 4:
                launchMarket();
                break;

            case 5:
                startActivity(new Intent(instance, LegalActivity.class));
                break;

            case 6:
                startActivity(new Intent(instance, ChooseOptionActivity.class));
                break;

            case 7:
                startActivity(new Intent(instance, NotificationActivity.class));
                break;

            default:
                break;
        }

    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Utils.Log("TAG url ::--> ", String.valueOf(uri));
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    public void ReferFriend() {
        //"market://details?id=" + getPackageName()
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.parshvaa.isccore&hl=en");

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "iSccore");
        i.putExtra(Intent.EXTRA_TEXT, String.valueOf(uri));
        startActivity(Intent.createChooser(i, "Share via"));
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case get_student_class_dtl:
                    showProgress(false, true);
                    try {
                        Log.d(TAG, "resultt>>>>>>>>>>>>: "+result);
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");
                            if (jData != null) {
                                ClassName = jData.getString("ClassName");
                                ClassLogo = jData.getString("ClassLogo");
                                ClassRoundLogo = jData.getString("ClassRoundLogo");
                                tv_class_name.setText(ClassName);
                                if (!TextUtils.isEmpty(ClassLogo))
                                    Glide.with(IDashboardActivity.this)
                                            .load(ClassLogo)
                                            .centerCrop()
                                            .placeholder(R.drawable.profile)
                                            .into(img_class_logo);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public class getRecorrd extends AsyncTask<String, String, String> {
        Context context;
        private MyDBManager mDb;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";


        private getRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);

        }

        @Override
        protected void onPreExecute() {
            OneSignalArray.clear();
            noticeboard_count = 0;
            lib_count = 0;
            practies_count = 0;
            cct_count = 0;
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            if (mDb.isTableExists(Constant.notification)) {
                int N_ID = mDb.getLastRecordIDForRequest("notification", "OneSignalID");
                if (N_ID != -1) {
                    Cursor noti = mDbOneSignal.getAllRows(DBNewQuery.getNotificatonDash(N_ID));
                    if (noti != null && noti.moveToFirst()) {
                        do {
                            NotificationOneSignal notiObj = (NotificationOneSignal) cParse.parseCursor(noti, new NotificationOneSignal());
                            OneSignalArray.add(notiObj);
                        } while (noti.moveToNext());
                        noti.close();
                    }
                }

            }

            if (OneSignalArray != null && OneSignalArray.size() > 0) {
                for (int i = 0; i < OneSignalArray.size(); i++) {
                    try {
                        NotificationType typeObj = new NotificationType();

                        JSONObject jObj = new JSONObject(OneSignalArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        notification_type_id = jtype.getString("notification_type_id");
                        created_on = jtype.getString("created_on");
                        SubjectName = jtype.getString("subject_name");
                        paper_type = jtype.getString("paper_type");
                        typeObj.setOneSignalID(Integer.parseInt(OneSignalArray.get(i).get_id()));
                        typeObj.setCreatedOn(created_on);
                        typeObj.setIsOpen(OneSignalArray.get(i).getOpened());
                        typeObj.setIsTypeOpen("0");
                        typeObj.setSubjectName(SubjectName);
                        typeObj.setNotification_type_id(notification_type_id);
                        typeObj.setModuleType(type);
                        typeObj.setPaperType(paper_type);
                        typeObj.setMessage(OneSignalArray.get(i).getMessage());
                        typeObj.setTitle(OneSignalArray.get(i).getTitle());
                        mDb.insertWebRow(typeObj, "notification");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            Cursor c = mDb.getAllRows("Select * from notification where ModuleType = 'cct' AND isTypeOpen = 0");
            // Utils.Log("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                cct_count = c.getCount();
                c.close();
            } else {
                cct_count = 0;
            }
            c = mDb.getAllRows("Select * from notification where ModuleType = 'notice_board' AND isTypeOpen = 0");
            // Utils.Log("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                noticeboard_count = c.getCount();
                c.close();
            } else {
                noticeboard_count = 0;
            }
            c = mDb.getAllRows("Select * from notification where ModuleType = 'library' AND isTypeOpen = 0");
            // Utils.Log("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                Utils.Log("TAG", "In IF library:-> ");
                lib_count = c.getCount();
                c.close();
            } else {
                lib_count = 0;
            }
            c = mDb.getAllRows("Select * from notification where ModuleType = 'test_paper' AND isTypeOpen = 0");
            // Utils.Log("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                Utils.Log("TAG", "In IF test_paper:-> ");
                practies_count = c.getCount();
                c.close();
            } else {
                practies_count = 0;
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (cct_count != 0) {
                tv_cct_count.setVisibility(View.VISIBLE);
                tv_cct_count.setText(cct_count + "");
            } else {
                tv_cct_count.setVisibility(View.GONE);
            }
            if (practies_count != 0) {
                tv_practies_count.setVisibility(View.VISIBLE);
                tv_practies_count.setText(practies_count + "");
            } else {
                tv_practies_count.setVisibility(View.GONE);
            }
            if (lib_count != 0) {
                tv_lib_count.setVisibility(View.VISIBLE);
                tv_lib_count.setText(lib_count + "");
            } else {
                tv_lib_count.setVisibility(View.GONE);
            }
            if (noticeboard_count != 0) {
                tv_noticboard_count.setVisibility(View.VISIBLE);
                tv_noticboard_count.setText(noticeboard_count + "");
            } else {
                tv_noticboard_count.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new getRecorrd(IDashboardActivity.this).execute();
    }
}
