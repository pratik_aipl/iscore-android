package com.parshvaa.isccore.activity.library;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class Library implements Serializable {
    @JsonField
    public String FolderID = "";
    @JsonField
    public String ParentID = "";
    @JsonField
    public String FolderName = "";
    @JsonField
    public String ChapterID="";
    @JsonField
    public String FolderIcon = "";
    @JsonField
    public String FileCount = "";
    @JsonField
    public String FolderType = "";
    @JsonField
    public String SubjectID = "";


    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getFolderID() {
        return FolderID;
    }

    public void setFolderID(String folderID) {
        FolderID = folderID;
    }

    public String getParentID() {
        return ParentID;
    }

    public void setParentID(String parentID) {
        ParentID = parentID;
    }

    public String getFolderName() {
        return FolderName;
    }

    public void setFolderName(String folderName) {
        FolderName = folderName;
    }

    public String getFolderIcon() {
        return FolderIcon;
    }

    public void setFolderIcon(String folderIcon) {
        FolderIcon = folderIcon;
    }

    public String getFileCount() {
        return FileCount;
    }

    public void setFileCount(String fileCount) {
        FileCount = fileCount;
    }

    public String getFolderType() {
        return FolderType;
    }

    public void setFolderType(String folderType) {
        FolderType = folderType;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }
}
