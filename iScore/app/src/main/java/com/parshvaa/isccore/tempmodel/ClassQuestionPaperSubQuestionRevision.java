package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 7/9/2018.
 */

public  class ClassQuestionPaperSubQuestionRevision implements Serializable {


    public String MPSQID = "";
    public String Marks = "";
    public String SubQuestion = "";
    public String SubAnswer = "";
    public String PassageSubQuestionTypeID = "";
    public String SubQuestionTypeID = "";

    public String getMarks() {
        return Marks;
    }

    public void setMarks(String marks) {
        Marks = marks;
    }

    public String getSubQuestion() {
        return SubQuestion;
    }

    public void setSubQuestion(String subQuestion) {
        SubQuestion = subQuestion;
    }

    public String getSubAnswer() {
        return SubAnswer;
    }

    public void setSubAnswer(String subAnswer) {
        SubAnswer = subAnswer;
    }

    public String getPassageSubQuestionTypeID() {
        return PassageSubQuestionTypeID;
    }

    public void setPassageSubQuestionTypeID(String passageSubQuestionTypeID) {
        PassageSubQuestionTypeID = passageSubQuestionTypeID;
    }

    public String getSubQuestionTypeID() {
        return SubQuestionTypeID;
    }

    public void setSubQuestionTypeID(String subQuestionTypeID) {
        SubQuestionTypeID = subQuestionTypeID;
    }

    public String getSubQuestionType() {
        return SubQuestionType;
    }

    public void setSubQuestionType(String subQuestionType) {
        SubQuestionType = subQuestionType;
    }

    public String SubQuestionType = "";


    public String getMPSQID() {
        return MPSQID;
    }

    public void setMPSQID(String MPSQID) {
        this.MPSQID = MPSQID;
    }

}