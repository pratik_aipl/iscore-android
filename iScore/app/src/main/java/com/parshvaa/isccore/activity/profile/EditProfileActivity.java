package com.parshvaa.isccore.activity.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.androidquery.AQuery;
import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.activity.library.videoPleyar.utility.Util;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.KeyData;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "EditProfileActivity";

    public EditText et_target_goal, et_name, et_schoolName, et_last_name, et_previous_goal;
    public CircleImageView img_profile;
    Uri selectedUri;
    public String profImagePath = "", PleyaerID = "";
    public App app;
    public Button btn_update;
    public ImageView img_back;
    public TextView tv_title, tv_email, tv_mob_no;
    public AQuery aQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Utils.logUser();
        aQuery = new AQuery(this);
        app = App.getInstance();
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Edit profile");
        img_back.setOnClickListener(v -> onBackPressed());
        img_profile = findViewById(R.id.img_profile);
        et_target_goal = findViewById(R.id.et_target_goal);
        et_previous_goal = findViewById(R.id.tv_previous_goal);
        et_name = findViewById(R.id.et_name);
        et_schoolName = findViewById(R.id.et_schoolName);
        et_last_name = findViewById(R.id.et_last_name);
        tv_email = findViewById(R.id.tv_email);
        tv_mob_no = findViewById(R.id.tv_mob_no);
        btn_update = findViewById(R.id.btn_update);
        PleyaerID = Utils.OneSignalPlearID();

        img_profile.setOnClickListener(view -> selectImage());
        et_target_goal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    String n = et_target_goal.getText().toString();
                    if (n.length() > 0) {
                        int number = Integer.parseInt(n);
                        if (number > 100) {
                            et_target_goal.setText(n.substring(0, n.length() - 1));
                            et_target_goal.setSelection(et_target_goal.getText().length());
                            Utils.showToast("Target should be only Lessthen or equal 100", EditProfileActivity.this);
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_previous_goal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    String n = et_previous_goal.getText().toString();
                    if (n.length() > 0) {
                        int number = Integer.parseInt(n);
                        if (n.length() > 3) {
                            if (number > 100) {
                                et_previous_goal.setText(n.substring(0, n.length() - 1));
                                et_previous_goal.setSelection(et_target_goal.getText().length());
                                Utils.showToast("Target should be only Lessthen or equal 100", EditProfileActivity.this);
                            }
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        setUserData();
        btn_update.setOnClickListener(view -> {
            String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + profImagePath + "";
            File f = new File(SDCardPath);
            if (!f.exists()) {
                f.mkdirs();
            }

            if (et_name.getText().toString().equals("")) {
                et_name.setError("Please Enter Firstname");
            } else if (et_last_name.getText().toString().equals("")) {
                et_last_name.setError("Please Enter LastName");
            } else if (et_schoolName.getText().toString().equals("")) {
                et_schoolName.setError("Please Enter School");
            } else if (et_target_goal.getText().toString().equals("")) {
                et_target_goal.setError("Please Enter Target");
            } else if (et_previous_goal.getText().toString().equals("")) {
                et_previous_goal.setError("Please Enter Previous goal");
            } else if (PleyaerID.equalsIgnoreCase("")) {
                et_previous_goal.setError("Please Try Again");
            } else {
                new CallRequest(EditProfileActivity.this)
                        .upadateProfile(et_name.getText().toString(),
                                et_last_name.getText().toString(), et_schoolName.getText().toString(),
                                et_target_goal.getText().toString(), et_previous_goal.getText().toString(),
                                profImagePath, PleyaerID);
            }
        });


    }

    private void setUserData() {
        et_name.setText(MainUser.getFirstName());
        et_last_name.setText(MainUser.getLastName());
        tv_mob_no.setText(MainUser.getRegMobNo());
        tv_email.setText(MainUser.getEmailID());
        et_schoolName.setText(TextUtils.isEmpty(MainUser.getSchoolName()) ? "" : MainUser.getSchoolName());
        et_target_goal.setText(mySharedPref.getlblTarget());
        et_previous_goal.setText(mySharedPref.getlblPrviousTarget());

        if (!TextUtils.isEmpty(mySharedPref.getProf_image())) {
            String ClassLogoName = mySharedPref.getProf_image().substring(mySharedPref.getProf_image().lastIndexOf("/") + 1);
            File imageDirClassLogoName = new File(Constant.LOCAL_IMAGE_PATH + "/profile/");
            File imageFile = new File(imageDirClassLogoName.getAbsolutePath() + "/" + ClassLogoName);
            if (!TextUtils.isEmpty(ClassLogoName)) {
                if (imageFile.exists()) {
                    Utils.setImage(this, imageFile, img_profile, R.drawable.profile);
                } else {
                    if (Utils.isNetworkAvailable(this)) {
                        Util.downloadClassIcon(this, aQuery, mySharedPref.getProf_image(), img_profile, imageFile, R.drawable.profile);
                    }
                }
            } else {
                img_profile.setImageResource(R.drawable.profile);
            }
        } else {
            img_profile.setImageResource(R.drawable.profile);
        }
    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath();
                Savefile(profImagePath);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                img_profile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath();
                Savefile(profImagePath);
                App.Image = selectedUri.getPath();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    public void Savefile(String path) {
        File direct = new File(path);

        Log.d(TAG, "Savefile: " + path);

        if (!direct.exists()) {
            try {
                direct.createNewFile();
                FileChannel src = new FileInputStream(path).getChannel();
                FileChannel dst = new FileOutputStream(direct).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            showProgress(false, true);
            try {
                switch (request) {
                    case upadateProfile:
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "onTaskCompleted: " + jObj.toString());
                        if (jObj.getBoolean("status")) {
                            MainUser.setProfileLastSync(jObj.getString("ProfileLastSync"));
                            JSONObject jData = jObj.getJSONObject("data");
                            MainUser.setStudentId(jData.getString("StudentID"));
                            MainUser.setFirstName(jData.getString("FirstName"));
                            MainUser.setLastName(jData.getString("LastName"));
                            MainUser.setGuid(jData.getString("IMEINo"));
                            MainUser.setEmailID(jData.getString("EmailID"));
                            MainUser.setRegMobNo(jData.getString("RegMobNo"));
                            MainUser.setSchoolName(jData.getString("SchoolName"));
                            MainUser.setProfile_image(jData.getString("ProfileImage"));
                            MainUser.setStudentCode(jData.getString("StudentCode"));

                            mySharedPref.setStudent_id(MainUser.getStudentId());
                            mySharedPref.setFirst_name(MainUser.getFirstName());
                            mySharedPref.setLast_name(MainUser.getLastName());
                            mySharedPref.setSchool_name(MainUser.getSchoolName());
                            mySharedPref.setGuuid(MainUser.getGuid());
                            mySharedPref.setEmailID(MainUser.getEmailID());
                            mySharedPref.setRegMobNo(MainUser.getRegMobNo());
                            mySharedPref.setStudentCode(MainUser.getStudentCode());
                            mySharedPref.setProfileImage(MainUser.getProfile_image());
//                            mySharedPref.setVersioCode(String.valueOf(BuildConfig.VERSION_CODE));
                            KeyData keydata;
                            JSONArray jsonArray = jData.getJSONArray("KeyData");
                            String checkUser = "";
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jKey = jsonArray.getJSONObject(i);
                                keydata = LoganSquare.parse(jKey.toString(), KeyData.class);
                                MainUser.keyDataArray.add(keydata);

                                MainUser.setRegKey(MainUser.keyDataArray.get(i).getStudentKey());
                                MainUser.setPreviousYearScore(MainUser.keyDataArray.get(i).getPreviousYearScore());

                                mySharedPref.setStudent_id(MainUser.keyDataArray.get(i).getStudentID());
                                mySharedPref.setBoardID(MainUser.keyDataArray.get(i).getBoardID());
                                mySharedPref.setMediumID(MainUser.keyDataArray.get(i).getMediumID());
                                mySharedPref.setStandardID(MainUser.keyDataArray.get(i).getStandardID());
                                mySharedPref.setClassID(MainUser.keyDataArray.get(i).getClassID());
                                mySharedPref.setBranchID(MainUser.keyDataArray.get(i).getBranchID());
                                mySharedPref.setBatchID(MainUser.keyDataArray.get(i).getBatchID());
                                mySharedPref.setReg_key(MainUser.keyDataArray.get(i).getStudentKey());
                                mySharedPref.setVersion(MainUser.keyDataArray.get(i).getIsDemo());
                                mySharedPref.setStandardName(MainUser.keyDataArray.get(i).getStandardName());
                                mySharedPref.setClassName(MainUser.keyDataArray.get(i).getClassName());
                                mySharedPref.setBoardName(MainUser.keyDataArray.get(i).getBoardName());
                                mySharedPref.setMediumName(MainUser.keyDataArray.get(i).getMediumName());
                                mySharedPref.setClassLogo(MainUser.keyDataArray.get(i).getClassLogo());
                                mySharedPref.setClassLogo2(MainUser.keyDataArray.get(i).getSplashLogo());
                                mySharedPref.setClassRoundLogo(MainUser.keyDataArray.get(i).getClassRoundLogo());
                                mySharedPref.setCreatedOn(MainUser.keyDataArray.get(i).getCreatedOn());
                                mySharedPref.setlblTarget(MainUser.keyDataArray.get(i).getSetTarget());
                                mySharedPref.setlblPrviousTarget(MainUser.keyDataArray.get(i).getPreviousYearScore());

                            }
                            mySharedPref.setFirst_name(MainUser.getFirstName());
                            mySharedPref.setLast_name(MainUser.getLastName());
                            mySharedPref.setSchool_name(MainUser.getSchoolName());
                            mySharedPref.setlblTarget(et_target_goal.getText().toString());
                            mySharedPref.setlblPrviousTarget(et_previous_goal.getText().toString());
                            setUserData();
                            Utils.showToast("Your profile Update successfully....  !", EditProfileActivity.this);
                            if (EditProfileActivity.this != null)
                                onBackPressed();
                        }
                        break;
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
