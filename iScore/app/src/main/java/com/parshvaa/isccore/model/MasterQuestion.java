package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by admin on 2/28/2017.
 */
@JsonObject
public class MasterQuestion implements Serializable {

    @JsonField
    public String MQuestionID;
    @JsonField
    public String Question;
    @JsonField
    public String ImageURL;
    @JsonField
    public String Answer;
    @JsonField
    public String BoardID;
    @JsonField
    public String MediumID;
    @JsonField
    public String ClassID;
    @JsonField
    public String StandardID;
    @JsonField
    public String SubjectID;
    @JsonField
    public String ChapterID;
    @JsonField
    public String QuestionTypeID;
    @JsonField
    public String isBoard;
    @JsonField
    public String ExerciseNo;
    @JsonField
    public String QuestionNo;
    @JsonField
    public String Question_Year;
    @JsonField
    public String is_demo;
    @JsonField
    public String CreatedBy;

    public String getIs_demo() {
        return is_demo;
    }

    public void setIs_demo(String is_demo) {
        this.is_demo = is_demo;
    }

    public String CreatedOn;
    public String ModifiedBy;
    public String ModifiedOn;

    public MasterQuestion() {

    }

    public MasterQuestion(String MQuestionID, String Question, String ImageURL, String Answer,
                          String BoardID, String MediumID, String ClassID, String StandardID,
                          String SubjectID, String ChapterID, String QuestionTypeID,
                          String isBoard, String ExerciseNo, String QuestionNo,
                          String Question_Year, String CreatedBy,
                          String CreatedOn, String ModifiedBy, String ModifiedOn,String is_demo) {

        this.MQuestionID = MQuestionID;
        this.Question = Question;
        this.ImageURL = ImageURL;
        this.Answer = Answer;
        this.BoardID = BoardID;
        this.MediumID = MediumID;
        this.ClassID = ClassID;
        this.StandardID = StandardID;
        this.SubjectID = SubjectID;
        this.ChapterID = ChapterID;
        this.QuestionTypeID = QuestionTypeID;
        this.isBoard = isBoard;
        this.ExerciseNo = ExerciseNo;
        this.QuestionNo = QuestionNo;
        this.Question_Year = Question_Year;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;
        this.is_demo=is_demo;

    }

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getIsBoard() {
        return isBoard;
    }

    public void setIsBoard(String isBoard) {
        this.isBoard = isBoard;
    }

    public String getExerciseNo() {
        return ExerciseNo;
    }

    public void setExerciseNo(String exerciseNo) {
        ExerciseNo = exerciseNo;
    }

    public String getQuestionNo() {
        return QuestionNo;
    }

    public void setQuestionNo(String questionNo) {
        QuestionNo = questionNo;
    }

    public String getQuestion_Year() {
        return Question_Year;
    }

    public void setQuestion_Year(String question_Year) {
        Question_Year = question_Year;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}