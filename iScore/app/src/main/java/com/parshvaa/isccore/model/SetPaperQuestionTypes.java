package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by admin on 2/28/2017.
 */
public class SetPaperQuestionTypes implements Serializable {

    public String QuestionTypeID="";
    public String QuestionType="";
    public String BoardID="";
    public String MediumID="";
    public String ClassID="";
    public String StandardID="";
    public String SubjectID="";
    public String Marks="";
    public String Type="";
    public String RevisionNo="";
    public String CreatedBy="";
    public String CreatedOn="";
    public String ModifiedBy="";
    public String ModifiedOn="";
    public String TotalQuestion="";
    public int noOfQuestion = 0;
    public int noOfAnswer = 0;

    public int getNoOfAnswer() {
        return noOfAnswer;
    }

    public void setNoOfAnswer(int noOfAnswer) {
        this.noOfAnswer = noOfAnswer;
    }

    public int getNoOfMarks() {
        return noOfMarks;
    }

    public void setNoOfMarks(int noOfMarks) {
        this.noOfMarks = noOfMarks;
    }

    public int getNoOfQuestion() {
        return noOfQuestion;
    }

    public void setNoOfQuestion(int noOfQuestion) {
        this.noOfQuestion = noOfQuestion;
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    public int noOfMarks =0;
    public int isSelected = -1;
    public String getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(String totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public SetPaperQuestionTypes(){

    }
    public SetPaperQuestionTypes(String QuestionTypeID, String QuestionType, String BoardID, String MediumID, String ClassID, String StandardID,
                                 String SubjectID, String Marks, String Type, String CreatedBy,
                                 String CreatedOn, String ModifiedBy, String ModifiedOn, String RevisionNo) {

        this.QuestionTypeID = QuestionTypeID;
        this.QuestionType = QuestionType;
        this.BoardID = BoardID;
        this.MediumID = MediumID;
        this.ClassID = ClassID;
        this.StandardID = StandardID;
        this.SubjectID = SubjectID;
        this.Marks = Marks;
        this.Type = Type;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;
        this.RevisionNo=RevisionNo;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(String questionType) {
        QuestionType = questionType;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getMarks() {
        return Marks;
    }

    public void setMarks(String marks) {
        Marks = marks;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getRevisionNo() {
        return RevisionNo;
    }

    public void setRevisionNo(String revisionNo) {
        RevisionNo = revisionNo;
    }

}
