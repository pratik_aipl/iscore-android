package com.parshvaa.isccore.activity.textbook;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class TextBookSubjectActivity extends BaseActivity implements AsynchTaskListner {

    private static final String TAG = "LibrarySubjectActivity";
    private RecyclerView recyclerView;
    private TextBookSubjectAdapter mAdapter;
    public ImageView img_back;
    public TextView tv_title;
    ArrayList<TextBook> TextBookArrayList = new ArrayList<>();
    public TextBook textbook;

    public RelativeLayout rel_library, rel_subject, rel_chapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_subject);
        Utils.logUser();

        recyclerView = findViewById(R.id.recycler_view);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        rel_library = findViewById(R.id.rel_library);
        rel_subject = findViewById(R.id.rel_subject);
        rel_chapter = findViewById(R.id.rel_chapter);

        rel_library.setVisibility(View.GONE);
        rel_subject.setVisibility(View.GONE);
        rel_chapter.setVisibility(View.GONE);

        tv_title.setText("SUBJECT");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        showProgress(true, true);
        new CallRequest(TextBookSubjectActivity.this).get_textbook_list();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    private void setupRecyclerView() {
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(TextBookSubjectActivity.this, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(TextBookSubjectActivity.this));
        mAdapter = new TextBookSubjectAdapter(TextBookArrayList, this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.d(TAG, "libresult: "+result);
            switch (request) {

                case get_textbook_list:
                    TextBookArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jDataArray = jObj.getJSONArray("Subjects");
                            TextBookArrayList.addAll(LoganSquare.parseList(jDataArray.toString(),TextBook.class));
                                setupRecyclerView();
                                showProgress(false, true);

                        } else {
                            showProgress(false, true);
                            showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(TextBookSubjectActivity.this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    finish();
                    dialog.dismiss();
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

