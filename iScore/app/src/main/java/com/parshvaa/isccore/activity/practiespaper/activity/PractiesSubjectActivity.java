package com.parshvaa.isccore.activity.practiespaper.activity;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.activity.mcq.SubjectMcqActivity;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.practiespaper.adapter.PractiesSubjectAdapter;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.DBTables;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.BoardPaperDownload;
import com.parshvaa.isccore.model.BoardsPaper;
import com.parshvaa.isccore.model.Chapter;
import com.parshvaa.isccore.model.ClassQuestionPaperSubQuestion;
import com.parshvaa.isccore.model.ExamTypePattern;
import com.parshvaa.isccore.model.ExamTypePatternDetail;
import com.parshvaa.isccore.model.ExamTypeSubject;
import com.parshvaa.isccore.model.ExamTypes;
import com.parshvaa.isccore.model.ImageData;
import com.parshvaa.isccore.model.ImageRecords;
import com.parshvaa.isccore.model.MainData;
import com.parshvaa.isccore.model.MasterPassageSubQuestion;
import com.parshvaa.isccore.model.MasterQuestion;
import com.parshvaa.isccore.model.Paper_type;
import com.parshvaa.isccore.model.PassageSubQuestionType;
import com.parshvaa.isccore.model.QuestionTypes;
import com.parshvaa.isccore.model.StudentMcqTestChap;
import com.parshvaa.isccore.model.StudentMcqTestDtl;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.model.StudentMcqTestLevel;
import com.parshvaa.isccore.model.StudentQuestionPaper;
import com.parshvaa.isccore.model.StudentQuestionPaperChapter;
import com.parshvaa.isccore.model.StudentQuestionPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperQuestionType;
import com.parshvaa.isccore.model.SubQuestionTypes;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.model.SubjectIcon;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.PractiesPaper;
import com.parshvaa.isccore.tempmodel.SubjectAccuracy;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.MessageEvent;
import com.parshvaa.isccore.utils.RobotoTextView;
import com.parshvaa.isccore.utils.Utils;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.parshvaa.isccore.utils.Constant.IMAGEURL;
import static com.parshvaa.isccore.utils.Utils.closeDb;

public class PractiesSubjectActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "PractiesSubjectActivity";
    //    ArrayList<SubjectAccuracy> subjectArray=new ArrayList<>();
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_refresh)
    ImageView img_refresh;
    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.mDownloadMcqTest)
    ImageView mDownloadMcqTest;
    @BindView(R.id.mProgressMcqTest)
    ProgressBar mProgressMcqTest;
    @BindView(R.id.mProgressMcqTestValue)
    TextView mProgressMcqTestValue;
    @BindView(R.id.mTransViewMcqTest)
    RelativeLayout mTransViewMcqTest;

    public MyDBManager mDb;
    public static App app;
    public static JSONArray ParseArrayPracties, ParseArrayBoard;
    public static JsonArray jStudQuePaperArray;
    public static JsonArray jStudHdrArray;
    public static JsonArray jBoardPaperArray;
    public static JsonArray jIncorrect;
    public static JsonArray jNotAppeared;
    public static StudentMcqTestHdr StudentHdrObj;
    public static StudentMcqTestChap StudentChapObj;
    public static StudentMcqTestDtl mcqTestDtl;
    public static StudentMcqTestLevel mcqTestLevel;

    public static ArrayList<StudentMcqTestHdr> StuHdrArray = new ArrayList<>();
    public List<StudentQuestionPaper> StuQuePaperArray = new ArrayList<>();
    public List<BoardPaperDownload> BoardpaperArray = new ArrayList<>();
    public CursorParserUniversal cParse;

    public int myImgTempCounter = 0;
    public LinkedHashMap<String, String> mapImages = new LinkedHashMap<>();
    @BindView(R.id.mErrorMsg)
    TextView mErrorMsg;

    @Override
    protected void onCreate(Bundle savedthisState) {
        super.onCreate(savedthisState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_select_subject);
        ButterKnife.bind(this);
        img_refresh.setVisibility(View.VISIBLE);

        cParse = new CursorParserUniversal();
        app = App.getInstance();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        Utils.logUser();

        tvTitle.setText("Select Subject");
        App.practiesPaperObj = new PractiesPaper();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));
        setupRecyclerView();
        setProgressMaxValue();
        mProgressMcqTestValue.setText("Click on the icon to download Practice Paper.");
        callProgressUpdate(false);
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down));
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setProgressMaxValue() {
        Log.d(TAG, "tableRecords: " + tableRecords.toString());
        if (tableRecords != null) {
            int practicePaperTotal = tableRecords.getSubjects() + tableRecords.getChapters() + tableRecords.getPaper_type()
                    + tableRecords.getMasterquestion() + tableRecords.getExamtype_subject() + tableRecords.getExamtypes()
                    + tableRecords.getExam_type_pattern() + tableRecords.getExam_type_pattern_detail() + tableRecords.getMaster_passage_sub_question()
                    + tableRecords.getPassage_sub_question_type() + tableRecords.getSub_question_types() + tableRecords.getQuestion_types();

            Log.d(TAG, "subjects > " + mDb.getTableCount(Constant.subjects));
            Log.d(TAG, "paper_type > " + mDb.getTableCount(Constant.paper_type));
            Log.d(TAG, "masterquestion >" + mDb.getTableCount(Constant.masterquestion));
            Log.d(TAG, "examtype_subject: > " + mDb.getTableCount(Constant.examtype_subject));
            Log.d(TAG, "examtypes: > " + mDb.getTableCount(Constant.examtypes));
            Log.d(TAG, "exam_type_pattern:> " + mDb.getTableCount(Constant.exam_type_pattern));

            Log.d(TAG, "exam_type_pattern_detail:> " + mDb.getTableCount(Constant.exam_type_pattern_detail));
            Log.d(TAG, "master_passage_sub_question:> " + mDb.getTableCount(Constant.master_passage_sub_question));
            Log.d(TAG, "passage_sub_question_type:> " + mDb.getTableCount(Constant.passage_sub_question_type));
            Log.d(TAG, "sub_question_types:> " + mDb.getTableCount(Constant.sub_question_types));
            Log.d(TAG, "question_types:> " + mDb.getTableCount(Constant.question_types));
            mProgressMcqTest.setMax(practicePaperTotal);
        }

    }

    private void callProgressUpdate(boolean isFirst) {
        if (mDb != null && mDb.isDbOpen()) {
            int practicePaperTotal = mDb.getTableCount(Constant.subjects)
                    + mDb.getTableCount(Constant.chapters)
                    + mDb.getTableCount(Constant.paper_type)
                    + mDb.getTableCount(Constant.masterquestion)
                    + mDb.getTableCount(Constant.examtype_subject)
                    + mDb.getTableCount(Constant.examtypes)
                    + mDb.getTableCount(Constant.exam_type_pattern)
                    + mDb.getTableCount(Constant.exam_type_pattern_detail)
                    + mDb.getTableCount(Constant.master_passage_sub_question)
                    + mDb.getTableCount(Constant.passage_sub_question_type)
                    + mDb.getTableCount(Constant.sub_question_types)
                    + mDb.getTableCount(Constant.question_types);

            Log.d(TAG, "subjects: " + mDb.getTableCount(Constant.subjects));
            Log.d(TAG, "paper_type: " + mDb.getTableCount(Constant.paper_type));
            Log.d(TAG, "masterquestion: " + mDb.getTableCount(Constant.masterquestion));
            Log.d(TAG, "examtype_subject: " + mDb.getTableCount(Constant.examtype_subject));
            Log.d(TAG, "examtypes: " + mDb.getTableCount(Constant.examtypes));
            Log.d(TAG, "exam_type_pattern: " + mDb.getTableCount(Constant.exam_type_pattern));

            Log.d(TAG, "exam_type_pattern_detail: " + mDb.getTableCount(Constant.exam_type_pattern_detail));
            Log.d(TAG, "master_passage_sub_question: " + mDb.getTableCount(Constant.master_passage_sub_question));
            Log.d(TAG, "passage_sub_question_type: " + mDb.getTableCount(Constant.passage_sub_question_type));
            Log.d(TAG, "sub_question_types: " + mDb.getTableCount(Constant.sub_question_types));
            Log.d(TAG, "question_types: " + mDb.getTableCount(Constant.question_types));

            if (mProgressMcqTest != null) {
                mProgressMcqTest.setProgress(practicePaperTotal);
                Log.d(TAG, "callProgressUpdate: " + practicePaperTotal);
                int practicePaper = (mProgressMcqTest.getProgress() * 100) / mProgressMcqTest.getMax();
                // if (mProgressMcqTestValue != null) {
                if (isFirst) {
                    mProgressMcqTest.setVisibility(View.VISIBLE);
                    if (practicePaper < 99)
                        mProgressMcqTestValue.setText(String.format(getString(R.string.progress_per), practicePaper) + "%");
                } else {
                    mProgressMcqTest.setVisibility(View.GONE);
                }
                //  }
                Log.d(TAG, "mySharedPref.getMasterImageDownloadStatus() >>: " + mySharedPref.getMasterImageDownloadStatus());
                if (mySharedPref.getMasterImageDownloadStatus()) {
                    Log.d(TAG, "IN 99 per >>: ");

                    if (mySharedPref.getMasterImageDownloadStatus()) {
                        Log.d(TAG, "getMasterImageDownloadStatus call >>: " + mySharedPref.getMasterImageDownloadStatus());
                        setdata();
                    } else {
                        Log.d(TAG, "callDownloadImage call  true >>: ");
                        callDownloadImage(true);
                    }

                } else {
                    mTransViewMcqTest.setVisibility(View.VISIBLE);
                }
            }


        }
    }


    private void callDownloadImage(boolean b) {
        mProgressMcqTestValue.setText(String.format(getString(R.string.progress_per), 99) + "%");
        new CallRequest(PractiesSubjectActivity.this).getMasterQuestionImages();
    }

    public void setdata() {
        mTransViewMcqTest.setVisibility(View.GONE);
        try {
            String standardId = mySharedPref.getStandardID();
            Log.d(TAG, "standardId: " + standardId);
            Log.d(TAG, "query DaTA : " + new CustomDatabaseQuery(this, new SubjectAccuracy())
                    .execute(new String[]{DBNewQuery.getAllSubjectPractiesPaper(standardId)}).get());
            recyclerView.setAdapter(new PractiesSubjectAdapter((ArrayList) new CustomDatabaseQuery(this, new SubjectAccuracy())
                    .execute(new String[]{DBNewQuery.getAllSubjectPractiesPaper(standardId)}).get(), this));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void callDownloadEvent() {
        if (Utils.isNetworkAvailable(this))
            subjectDataDownloadNew();
        else {
            Utils.showToast(getString(R.string.no_internet_msg), this);
        }
    }

    // TODO: 8/3/2019 -> subject
    public void subjectDataDownloadNew() {
        mProgressMcqTestValue.setVisibility(View.VISIBLE);
        mProgressMcqTest.setVisibility(View.VISIBLE);
        mErrorMsg.setText("");
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.subjects);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getSubjects()) {
                    mDb.emptyTable(Constant.subjects);
                    new CallRequest(this).getSubjectTable();
                } else {
                    chapterDataDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter
    public void chapterDataDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.chapters);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getChapters()) {
                    mDb.emptyTable(Constant.chapters);
                    new CallRequest(this).getChapterTable();
                } else {
                    paperTypesDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 sget_question_typesubject -> Chapter -> PaperType
    public void paperTypesDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.paper_type);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getPaper_type()) {
                    mDb.emptyTable(Constant.paper_type);
                    new CallRequest(this).getPaper_Type();
                } else {
                    examTypesSubjectDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject
    public void examTypesSubjectDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.examtype_subject);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getExamtype_subject()) {
                    mDb.emptyTable(Constant.examtype_subject);
                    new CallRequest(this).getExamTypeSubject();
                } else {
                    examTypesDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question -> passage_sub_question_type -> sub_question_types -> McqQuestions -> McqOptions -> MasterQuestions
    public void masterQuestionsDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.masterquestion);
            int id = mDb.getLastRecordIDForRequest(Constant.masterquestion, "MQuestionID");
            Log.d(TAG, "masterQuestionsDownloadNew: total " + total);
            Log.d(TAG, "masterQuestionsDownloadNew: --> " + tableRecords.getMasterquestion());
            if (tableRecords != null) {
                if (total < tableRecords.getMasterquestion()) {
                    new CallRequest(this).getMasterQuestions(id);
                } else {
                    Log.d(TAG, "masterQuestionsDownloadNew: call download img");
                    callDownloadImage(true);
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType
    public void examTypesDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.examtypes);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getExamtypes()) {
                    mDb.emptyTable(Constant.examtypes);
                    new CallRequest(this).getExamTypes();
                } else {
                    examTypePatternDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern
    public void examTypePatternDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.exam_type_pattern);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getExam_type_pattern()) {
                    mDb.emptyTable(Constant.exam_type_pattern);
                    new CallRequest(this).getExamTypePattern();
                } else {
                    examTypePatternDetailDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail
    public void examTypePatternDetailDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.exam_type_pattern_detail);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getExam_type_pattern_detail()) {
                    mDb.emptyTable(Constant.exam_type_pattern_detail);
                    new CallRequest(this).getExamTypePatternDetail();
                } else {
                    get_master_passage_sub_questionNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question
    public void get_master_passage_sub_questionNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.master_passage_sub_question);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getMaster_passage_sub_question()) {
                    mDb.emptyTable(Constant.master_passage_sub_question);
                    new CallRequest(this).get_master_passage_sub_question();
                } else {
                    get_passage_sub_question_typesNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question -> passage_sub_question_type
    public void get_passage_sub_question_typesNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.passage_sub_question_type);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getPassage_sub_question_type()) {
                    mDb.emptyTable(Constant.passage_sub_question_type);
                    new CallRequest(this).get_passage_sub_question_types();
                } else {
                    get_sub_question_typesNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question -> passage_sub_question_type -> sub_question_types
    public void get_sub_question_typesNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.sub_question_types);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getSub_question_types()) {
                    mDb.emptyTable("sub_question_types");
                    new CallRequest(this).get_sub_question_types();
                } else {
                    questionTypesDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question -> passage_sub_question_type -> sub_question_types -> McqQuestions -> McqOptions -> MasterQuestions -> QuestionTypes
    public void questionTypesDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.question_types);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getQuestion_types()) {
                    mDb.emptyTable(Constant.question_types);
                    new CallRequest(this).getQuesTypeTable();
                } else {
                    masterQuestionsDownloadNew();
                }
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            callProgressUpdate(true);
            switch (request) {
                // TODO: 8/3/2019  1.Subject download
                case getSubjectTable:
                    new insertSubjectRow().execute(result);
                    break;
                // TODO: 8/3/2019 2. Chapter download
                case getChapterTable:
                    new insertChapterRow().execute(result);
                    break;
                // TODO: 8/3/2019 3. Paper Type Download
                case getPaper_Type:
                    new insertPaperTypesRow().execute(result);
                    break;
                // TODO: 8/3/2019 4. Examtype Subject Download
                case getExamtype_subject:
                    new insertExamTypesSubjectRow().execute(result);
                    break;
                // TODO: 8/3/2019 5. Examtype Download
                case getExamtypes:
                    new insertExamTypesRow().execute(result);
                    break;
                // TODO: 8/3/2019 6. ExamtypePattern Download
                case getExamTypePattern:
                    new insertExamTypePatternsRow().execute(result);
                    break;
                // TODO: 8/3/2019 7. ExamtypePatternDetail Download
                case getExamTypePatternDetail:
                    new insertExamTypePatternDetailRow().execute(result);
                    break;
                // TODO: 8/3/2019 8.master_passage_sub_question Download
                case get_master_passage_sub_question:
                    new insertMasterPassageSubQuestionRow().execute(result);
                    break;
                // TODO: 8/3/2019 9.passage_sub_question_type Download
                case get_passage_sub_question_types:
                    new insertMasterPassageSubQuestionTypeRow().execute(result);
                    break;
                // TODO: 8/3/2019 10.sub_question_types Download
                case get_sub_question_types:
                    new insertSubQuestionTypeRow().execute(result);
                    break;
                case getQuesTypeTable:
                    new insertQuestionTypeRow().execute(result);
                    break;
                // TODO: 8/3/2019 13.MasterQuestion Download
                case getMasterQuestionTable:
                    new insertMasterQuestionRow().execute(result);
                    break;

                // TODO: 8/3/2019 14.QuestionTypes Download
                case sendMcqDataNew:
                    new insertParseWebMCQDataNewRow().execute(result);
                    break;
                case getBoardsPaperTable:
                    new insertBoardsPaperRow().execute(result);
                    break;
                case sendGanreatPaperNew:
                    new insertGeneratePaperRow().execute(result);
                    break;
                case sendBoardPaperDownload:
                    new insertBoardPaperRow().execute(result);
                    break;
                case getMasterQuestionImages:
                    new downloadMasterQuestionImages().execute(result);
                    break;


            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class insertSubjectRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<Subject> subjectList = LoganSquare.parseList(jArray.toString(), Subject.class);
                Log.d(TAG, "subjectList >>: " + subjectList.size());
                for (int i = 0; i < subjectList.size(); i++) {
                    mDb.insertRow(subjectList.get(i), Constant.subjects);
                }
                Utils.setTransaction(mDb);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            chapterDataDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class insertChapterRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<Chapter> chapterList = LoganSquare.parseList(jArray.toString(), Chapter.class);
                Log.d(TAG, "chapterList >>: " + chapterList.size());

                if (mDb.isDbOpen()) {
                    for (int i = 0; i < chapterList.size(); i++) {
                        mDb.insertRow(chapterList.get(i), Constant.chapters);
                    }
                    Utils.setTransaction(mDb);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            paperTypesDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class insertPaperTypesRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<Paper_type> paper_types = LoganSquare.parseList(jArray.toString(), Paper_type.class);
                Log.d(TAG, "paper_types >>: " + paper_types.size());

                if (paper_types != null && paper_types.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < paper_types.size(); i++) {
                            mDb.insertRow(paper_types.get(i), Constant.paper_type);
                        }
                        Utils.setTransaction(mDb);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            examTypesSubjectDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertMasterQuestionRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<MasterQuestion> masterQuestion = LoganSquare.parseList(params[0], MasterQuestion.class);
                Log.d(TAG, "masterQuestion >>: " + masterQuestion.size());

                if (masterQuestion != null && masterQuestion.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < masterQuestion.size(); i++) {
                            mDb.insertRow(masterQuestion.get(i), Constant.masterquestion);
                        }
                        Utils.setTransaction(mDb);
                    }
                    isNext = false;
                } else {
                    isNext = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            if (isNext) {
                callDownloadImage(true);
            } else {
                masterQuestionsDownloadNew();
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertExamTypesSubjectRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<ExamTypeSubject> examTypeSubjects = LoganSquare.parseList(jArray.toString(), ExamTypeSubject.class);
                Log.d(TAG, "examTypeSubjects >>: " + examTypeSubjects.size());

                if (examTypeSubjects != null && examTypeSubjects.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < examTypeSubjects.size(); i++) {
                            mDb.insertRow(examTypeSubjects.get(i), Constant.examtype_subject);
                        }
                        Utils.setTransaction(mDb);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            examTypesDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertExamTypesRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<ExamTypes> examTypesList = LoganSquare.parseList(jArray.toString(), ExamTypes.class);
                Log.d(TAG, "examTypesList >>: " + examTypesList.size());

                if (examTypesList != null && examTypesList.size() > 0) {
                    if (mDb.isDbOpen()) {

                        ;
                        for (int i = 0; i < jArray.length(); i++) {
                            mDb.insertRow(examTypesList.get(i), Constant.examtypes);
                        }
                        Utils.setTransaction(mDb);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            examTypePatternDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertExamTypePatternsRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<ExamTypePattern> examTypePatternList = LoganSquare.parseList(params[0], ExamTypePattern.class);
                Log.d(TAG, "examTypePatternList >>: " + examTypePatternList.size());

                if (examTypePatternList != null && examTypePatternList.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < examTypePatternList.size(); i++) {
                            mDb.insertRow(examTypePatternList.get(i), Constant.exam_type_pattern);
                        }
                        Utils.setTransaction(mDb);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            examTypePatternDetailDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertExamTypePatternDetailRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<ExamTypePatternDetail> examTypePatternDetail = LoganSquare.parseList(params[0], ExamTypePatternDetail.class);
                Log.d(TAG, "examTypePatternDetail >>: " + examTypePatternDetail.size());

                if (examTypePatternDetail != null && examTypePatternDetail.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < examTypePatternDetail.size(); i++) {
                            mDb.insertRow(examTypePatternDetail.get(i), Constant.exam_type_pattern_detail);
                        }
                        Utils.setTransaction(mDb);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            get_master_passage_sub_questionNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertMasterPassageSubQuestionRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<MasterPassageSubQuestion> typePatternList = LoganSquare.parseList(params[0], MasterPassageSubQuestion.class);
                Log.d(TAG, "typePatternList >>: " + typePatternList.size());

                if (typePatternList != null && typePatternList.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < typePatternList.size(); i++) {
                            MasterPassageSubQuestion examTypePattern = typePatternList.get(i);
                            mDb.insertRow(examTypePattern, Constant.master_passage_sub_question);
                        }
                        Utils.setTransaction(mDb);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            get_passage_sub_question_typesNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertMasterPassageSubQuestionTypeRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<PassageSubQuestionType> subQuestionTypeList = LoganSquare.parseList(params[0], PassageSubQuestionType.class);
                Log.d(TAG, "subQuestionTypeList >>: " + subQuestionTypeList.size());

                if (subQuestionTypeList != null && subQuestionTypeList.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < subQuestionTypeList.size(); i++) {
                            mDb.insertRow(subQuestionTypeList.get(i), Constant.passage_sub_question_type);
                        }
                        Utils.setTransaction(mDb);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            get_sub_question_typesNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertSubQuestionTypeRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<SubQuestionTypes> subQuestionTypes = LoganSquare.parseList(params[0], SubQuestionTypes.class);
                Log.d(TAG, "subQuestionTypes >>: " + subQuestionTypes.size());

                if (subQuestionTypes != null && subQuestionTypes.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < subQuestionTypes.size(); i++) {
                            mDb.insertRow(subQuestionTypes.get(i), Constant.sub_question_types);
                        }
                        Utils.setTransaction(mDb);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            questionTypesDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertQuestionTypeRow extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<QuestionTypes> questionTypes = LoganSquare.parseList(params[0], QuestionTypes.class);
                Log.d(TAG, "questionTypes >>: " + questionTypes.size());

                if (questionTypes != null && questionTypes.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < questionTypes.size(); i++) {
                            mDb.insertRow(questionTypes.get(i), Constant.question_types);
                        }
                        Utils.setTransaction(mDb);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            masterQuestionsDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public class insertParseWebMCQDataNewRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                jStudHdrArray = null;
                MainData mainData = LoganSquare.parse(params[0], MainData.class);
                if (mainData.isStatus()) {
                    if (!TextUtils.isEmpty(mainData.getMCQLastSync())) {
                        mySharedPref.setLastMCQlastSync(mainData.getMCQLastSync());
                        mySharedPref.setLastSyncTime(mainData.getMCQLastSync());
                    }
                    parseWebMCQDataNew(mainData.getStudentMcqTestHdrs());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            getGeneratePaperDataNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertBoardsPaperRow extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<BoardsPaper> boardsPapers = LoganSquare.parseList(new JSONArray(params[0]).toString(), BoardsPaper.class);
                Log.d(TAG, "boardsPapers >>: " + boardsPapers.size());

                if (boardsPapers != null && boardsPapers.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < boardsPapers.size(); i++) {
                            mDb.insertRow(boardsPapers.get(i), "boards_paper");
                        }
                        Utils.setTransaction(mDb);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            paperTypesDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertBoardPaperRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                JSONObject jObj = new JSONObject(params[0]);
                if (jObj.getBoolean("status")) {
                    if (jObj.has("BoardPaperLastSync")) {
                        String setLastBoardPaperDownloadSyncTime = jObj.getString("BoardPaperLastSync");
                        mySharedPref.setLastBoardPaperDownloadSyncTime(setLastBoardPaperDownloadSyncTime);
                        mySharedPref.setLastSyncTime(setLastBoardPaperDownloadSyncTime);
                    }
                    if (jObj.has("data")) {
                        JSONArray jData = jObj.getJSONArray("data");
                        ParseArrayBoard = jData;
                        parseWebBoardPaperDataNew(jData);
                    }
                }
                if (mDb.isDbOpen())
                    mDb.removeTable("class_evaluater_cct_count");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            callProgressUpdate(true);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertGeneratePaperRow extends AsyncTask<String, Void, Boolean> {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                JSONObject jObj = new JSONObject(params[0]);
                if (jObj.getBoolean("status")) {
                    if (jObj.has("GeneratePaperLastSync")) {
                        String lastGeneratePaperSyncTime = jObj.getString("GeneratePaperLastSync");
                        mySharedPref.setLastGeneratePaperlastSync(lastGeneratePaperSyncTime);
                        mySharedPref.setLastSyncTime(lastGeneratePaperSyncTime);
                    }
                    if (jObj.getJSONArray("data") != null) {
                        JSONArray jData = jObj.getJSONArray("data");
                        if (mDb.isDbOpen()) {
                            mDb.removeTable("student_question_paper");
                            mDb.removeTable("student_question_paper_detail");
                            mDb.removeTable("student_question_paper_chapter");
                            mDb.removeTable("student_set_paper_question_type");
                            mDb.removeTable("student_set_paper_detail");
                            DBTables dbTables = new DBTables();
                            dbTables.student_question_paper(mDb);
                            dbTables.student_question_paper_detail(mDb);
                            dbTables.student_question_paper_chapter(mDb);
                            dbTables.student_set_paper_question_type(mDb);
                            dbTables.student_set_paper_detail(mDb);
                            parseWebGeneratePaperDataNew(jData);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            getBoardPaperDataNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question -> passage_sub_question_type -> sub_question_types -> McqQuestions -> McqOptions -> MasterQuestions -> QuestionTypes ->MCQDataDownload
    public void getMCQDataDownloadNew() {
        StuHdrArray.clear();
        jStudHdrArray = null;
        Cursor c = mDb.getAllRows(DBQueries.getAllStudentHdr(mySharedPref.getLastMCQSyncTime()));
        if (c != null && c.moveToFirst()) {
            do {
                StudentHdrObj = new StudentMcqTestHdr();
                StudentHdrObj = (StudentMcqTestHdr) cParse.parseCursor(c, new StudentMcqTestHdr());
                StuHdrArray.add(StudentHdrObj);
            } while (c.moveToNext());
            c.close();
        }
        for (StudentMcqTestHdr header : StuHdrArray) {
            String hdr = String.valueOf(header.getStudentMCQTestHDRID());
            Cursor chap = mDb.getAllRows(DBQueries.getStudentMcqChap(hdr));
            if (chap != null && chap.moveToFirst()) {
                do {
                    StudentChapObj = new StudentMcqTestChap();
                    StudentChapObj = (StudentMcqTestChap) cParse.parseCursor(chap, new StudentMcqTestChap());
                    header.chapterArray.add(StudentChapObj);
                } while (chap.moveToNext());
                chap.close();
            }
            Cursor det = mDb.getAllRows(DBQueries.getStudentMcqDtl(hdr));
            if (det != null && det.moveToFirst()) {
                do {
                    mcqTestDtl = new StudentMcqTestDtl();
                    mcqTestDtl = (StudentMcqTestDtl) cParse.parseCursor(det, new StudentMcqTestDtl());
                    header.detailArray.add(mcqTestDtl);
                } while (det.moveToNext());
                det.close();
            }
        }

        Gson gson = new GsonBuilder().create();
        jStudHdrArray = gson.toJsonTree(StuHdrArray).getAsJsonArray();
        if (jStudHdrArray.size() > 0) {
            new CallRequest(this).sendMcqDataLoginNew(jStudHdrArray.toString());
        } else {
            new CallRequest(this).sendMcqDataLoginNew("[]");
        }

    }

    //--------------------------    get Generate Paper Data    ----------------------------------
    public void getGeneratePaperDataNew() {
        StuQuePaperArray.clear();
        jStudQuePaperArray = null;
        Cursor c = mDb.getAllRows(DBQueries.getAllStudentQuestionPaper(mySharedPref.getLastGeneratePaperSyncTime()));
        if (c != null && c.moveToFirst()) {
            do {
                StuQuePaperArray.add((StudentQuestionPaper) cParse.parseCursor(c, new StudentQuestionPaper()));
            } while (c.moveToNext());
            c.close();
        }
        for (StudentQuestionPaper header : StuQuePaperArray) {
            String hdr = String.valueOf(header.getStudentQuestionPaperID());
            if (header.getPaperTypeID().equals("1") || header.getPaperTypeID().equals(1)) {
                StudentQuePaperDetailNew(header, hdr);
            } else if (header.getPaperTypeID().equals("2") || header.getPaperTypeID().equals(2)) {
                StudentQuePaperChapNew(header, hdr);
                StudentQuePaperDetailNew(header, hdr);
            } else {
                // paper type id 3
                if (mDb.isDbOpen()) {
                    StudentQuePaperChapNew(header, hdr);
                    Cursor set = mDb.getAllRows(DBQueries.getStudentSetPaperQueType(hdr));
                    if (set != null && set.moveToFirst()) {
                        do {
                            StudentSetPaperQuestionType StudentSetPaperQueTypeObj = new StudentSetPaperQuestionType();
                            StudentSetPaperQueTypeObj = (StudentSetPaperQuestionType) cParse.parseCursor(set, new StudentSetPaperQuestionType());
                            header.queTypeArray.add(StudentSetPaperQueTypeObj);
                        } while (set.moveToNext());
                        set.close();
                    }
                    for (StudentSetPaperQuestionType Quetypeid : header.queTypeArray) {
                        String id = String.valueOf(Quetypeid.getStudentSetPaperQuestionTypeID());

                        Cursor dtl = mDb.getAllRows(DBQueries.getStudentSetPaperDtl(id));
                        if (dtl != null && dtl.moveToFirst()) {
                            do {
                                StudentSetPaperDetail StudentSetPaperDtlObj = new StudentSetPaperDetail();
                                StudentSetPaperDtlObj = (StudentSetPaperDetail) cParse.parseCursor(dtl, new StudentSetPaperDetail());
                                Quetypeid.setdetailArray.add(StudentSetPaperDtlObj);
                                for (StudentSetPaperDetail detaiObj : Quetypeid.setdetailArray) {
                                    String detaiId = String.valueOf(detaiObj.getStudentSetPaperDetailID());
                                    try {
                                        detaiObj.subQuesArray = (ArrayList) new CustomDatabaseQuery(this, new ClassQuestionPaperSubQuestion()).execute(new String[]{DBQueries.getClassSubQue(detaiId)}).get();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } catch (ExecutionException e) {
                                        e.printStackTrace();
                                    }
                                }

                            } while (dtl.moveToNext());
                            dtl.close();
                        }
                    }
                }
            }
        }
        Gson gson = new GsonBuilder().create();
        jStudQuePaperArray = gson.toJsonTree(StuQuePaperArray).getAsJsonArray();
        if (jStudQuePaperArray.size() > 0) {
            if (BuildConfig.VERSION_CODE <= 37) {
                new CallRequest(this).sendGanreatPaperVersionNew(String.valueOf(jStudQuePaperArray), "45");
            } else {
                new CallRequest(this).sendGanreatPaperLoginNew(String.valueOf(jStudQuePaperArray));
            }
        } else {
            new CallRequest(this).sendGanreatPaperLoginNew("[]");
        }
    }

    //--------------------------    get Board Paper Data    ----------------------------------
    public void getBoardPaperDataNew() {
        BoardpaperArray.clear();
        jBoardPaperArray = null;
        if (mDb.isDbOpen()) {

            Cursor c = mDb.getAllRows(DBQueries.getAllBoardPapereDownload(mySharedPref.getLastBoardPaperDownloadSyncTime()));
            if (c != null && c.moveToFirst()) {
                do {
                    BoardpaperArray.add((BoardPaperDownload) cParse.parseCursor(c, new BoardPaperDownload()));
                } while (c.moveToNext());
                c.close();
            }
        }
        Gson gson = new GsonBuilder().create();
        jBoardPaperArray = gson.toJsonTree(BoardpaperArray).getAsJsonArray();
        if (jBoardPaperArray.size() > 0) {
            new CallRequest(this).sendBoardPaperDownloadLogin(String.valueOf(jBoardPaperArray));
        } else {
            new CallRequest(this).sendBoardPaperDownloadLogin("[]");
        }
    }

    //--------------------------    Parse Web Generate Paper Data    ----------------------------------
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void parseWebGeneratePaperDataNew(JSONArray jArray) throws IOException {
        StuQuePaperArray.clear();
        StuQuePaperArray = LoganSquare.parseList(jArray.toString(), StudentQuestionPaper.class);
        if (mDb.isDbOpen()) {
            for (int i = 0; i < StuQuePaperArray.size(); i++) {
                StudentQuestionPaper studentQuestionPaper = StuQuePaperArray.get(i);
                if (mDb.isTableExists(Constant.student_question_paper)) {
                    int inserID = Math.toIntExact(mDb.insertWebRow(studentQuestionPaper, Constant.student_question_paper));
                    List<StudentQuestionPaperChapter> studentQuestionPaperChapter = studentQuestionPaper.getChapterArray();
                    for (int k = 0; k < studentQuestionPaperChapter.size(); k++) {
                        StudentQuestionPaperChapter studentQuePaeprChpObj = new StudentQuestionPaperChapter();
                        studentQuePaeprChpObj.setStudentQuestionPaperID(inserID);
                        if (mDb.isTableExists(Constant.student_question_paper_chapter)) {
                            mDb.insertWebRow(studentQuePaeprChpObj, Constant.student_question_paper_chapter);
                        } else {
                            break;
                        }
                    }
                    List<StudentQuestionPaperDetail> studentQuestionPaperDetail = studentQuestionPaper.getDetailArray();
                    for (int j = 0; j < studentQuestionPaperDetail.size(); j++) {
                        StudentQuestionPaperDetail studentQuestionPaperDetail1 = studentQuestionPaperDetail.get(j);
                        if (mDb.isTableExists(Constant.student_question_paper_detail)) {
                            long l_d_id = mDb.insertWebRow(studentQuestionPaperDetail1, Constant.student_question_paper_detail);
                            int DetailID = Integer.parseInt(l_d_id + "");
                            List<ClassQuestionPaperSubQuestion> classQuestionPaperSubQuestions = studentQuestionPaperDetail1.getSubQuesArray();
                            for (int k = 0; k < classQuestionPaperSubQuestions.size(); k++) {
                                ClassQuestionPaperSubQuestion sub = classQuestionPaperSubQuestions.get(k);
                                sub.setDetailID(DetailID + "");
                                if (mDb.isTableExists(Constant.class_question_paper_sub_question)) {
                                    mDb.insertWebRow(sub, Constant.class_question_paper_sub_question);
                                } else {
                                    break;
                                }
                            }
                        } else {
                            break;
                        }
                    }
                    List<StudentSetPaperQuestionType> studentSetPaperQuestionType = studentQuestionPaper.getQueTypeArray();
                    for (int j = 0; j < studentSetPaperQuestionType.size(); j++) {
                        StudentSetPaperQuestionType setPaperQuestionType = studentSetPaperQuestionType.get(j);
                        setPaperQuestionType.setStudentQuestionPaperID(String.valueOf(inserID));
                        if (mDb.isTableExists(Constant.student_set_paper_question_type)) {
                            long QuTypeID = mDb.insertWebRow(setPaperQuestionType, Constant.student_set_paper_question_type);
                            for (int k = 0; k < setPaperQuestionType.getSetdetailArray().size(); k++) {
                                StudentSetPaperDetail setPaperDetail = setPaperQuestionType.getSetdetailArray().get(k);
                                setPaperDetail.setStudentSetPaperQuestionTypeID(Math.toIntExact(QuTypeID));
                                if (mDb.isTableExists(Constant.student_set_paper_detail)) {
                                    long l_d_id = mDb.insertWebRow(setPaperDetail, Constant.student_set_paper_detail);
                                    int DetailID = Integer.parseInt(l_d_id + "");
                                    for (int l = 0; l < setPaperDetail.getSubQuesArray().size(); l++) {
                                        ClassQuestionPaperSubQuestion classQuestionPaperSubQuestion = setPaperDetail.getSubQuesArray().get(l);
                                        classQuestionPaperSubQuestion.setPaperID(inserID + "");
                                        classQuestionPaperSubQuestion.setDetailID(DetailID + "");
                                        if (mDb.isTableExists(Constant.class_question_paper_sub_question)) {
                                            mDb.insertWebRow(classQuestionPaperSubQuestion, Constant.class_question_paper_sub_question);
                                        } else {
                                            break;
                                        }
                                    }
                                } else {
                                    break;
                                }
                            }
                        } else {
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
        }
    }

    public void StudentQuePaperDetailNew(StudentQuestionPaper header, String hdr) {
        Cursor dtl = mDb.getAllRows(DBQueries.getStudentQuePaperDetail(hdr));
        if (dtl != null && dtl.moveToFirst()) {
            do {
                StudentQuestionPaperDetail StudentQuePaeprDtlObj = new StudentQuestionPaperDetail();
                StudentQuePaeprDtlObj = (StudentQuestionPaperDetail) cParse.parseCursor(dtl, new StudentQuestionPaperDetail());
                if (header != null) {
                    header.detailArray.add(StudentQuePaeprDtlObj);
                    for (StudentQuestionPaperDetail detaiObj : header.detailArray) {
                        String detaiId = String.valueOf(detaiObj.getStudentQuestionPaperDetailID());
                        try {
                            detaiObj.subQuesArray = (ArrayList) new CustomDatabaseQuery(this, new ClassQuestionPaperSubQuestion())
                                    .execute(new String[]{DBQueries.getClassSubQue(detaiId)}).get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } while (dtl.moveToNext());
            dtl.close();
        } else {
            Utils.showToast("Not any SubjectAccuracy detail Found from : ", this);
        }
        Gson gson = new GsonBuilder().create();
        JsonArray jStudQuePaperArray = gson.toJsonTree(header.detailArray).getAsJsonArray();
    }

    public void StudentQuePaperChapNew(StudentQuestionPaper header, String hdr) {
        if (mDb.isDbOpen()) {

            Cursor chap = mDb.getAllRows(DBQueries.getStudentQuePaperChap(hdr));
            if (chap != null && chap.moveToFirst()) {
                do {
                    header.chapterArray.add((StudentQuestionPaperChapter) cParse.parseCursor(chap, new StudentQuestionPaperChapter()));
                } while (chap.moveToNext());
                chap.close();
            } else {
                Utils.showToast("Not any SubjectAccuracy Found from : ", this);
            }

            Gson gson = new GsonBuilder().create();
            JsonArray jStudQuePaperArray = gson.toJsonTree(header.chapterArray).getAsJsonArray();
        }
    }

    //--------------------------    Parse Web  mcq Data    ----------------------------------
    public void parseWebMCQDataNew(List<StudentMcqTestHdr> studentMcqTestHdrs) {
        if (mDb.isDbOpen()) {

            if (studentMcqTestHdrs != null && studentMcqTestHdrs.size() > 0) {
                for (int i = 0; i < studentMcqTestHdrs.size(); i++) {
                    StudentMcqTestHdr studentMcqTestHdr = studentMcqTestHdrs.get(i);
                    long ID = mDb.insertWebRow(studentMcqTestHdr, Constant.student_mcq_test_hdr);
                    int inserID = Integer.parseInt(ID + "");

                    for (int j = 0; j < studentMcqTestHdr.getChapterArray().size(); j++) {
                        StudentMcqTestChap mcqTestChap = studentMcqTestHdr.getChapterArray().get(j);
                        mcqTestChap.setStudentMCQTestHDRID((inserID + ""));
                        mDb.insertWebRow(mcqTestChap, Constant.student_mcq_test_chapter);
                    }

                    for (int j = 0; j < studentMcqTestHdr.getDetailArray().size(); j++) {
                        StudentMcqTestDtl mcqTestDtl = studentMcqTestHdr.getDetailArray().get(j);
                        mcqTestDtl.setStudentMCQTestHDRID(inserID);
                        mDb.insertWebRow(mcqTestDtl, Constant.student_mcq_test_dtl);
                    }
                    for (int k = 0; k < studentMcqTestHdr.getLevelArray().size(); k++) {
                        StudentMcqTestLevel mcqTestLevel = studentMcqTestHdr.getLevelArray().get(k);
                        mcqTestLevel.setStudentMCQTestHDRID(inserID + "");
                        mDb.insertWebRow(mcqTestLevel, Constant.student_mcq_test_level);
                    }
                }

            }
        }
    }

    //--------------------------    Parse Web Board Paper Data    ----------------------------------
    public void parseWebBoardPaperDataNew(JSONArray jArray) throws IOException {
        BoardpaperArray.clear();
        BoardpaperArray = LoganSquare.parseList(jArray.toString(), BoardPaperDownload.class);
        for (int i = 0; i < BoardpaperArray.size(); i++) {
            mDb.insertWebRow(BoardpaperArray.get(i), Constant.board_paper_download);
        }
        if (mDb.isDbOpen())
            mDb.removeTable(Constant.class_evaluater_cct_count);

    }


    @SuppressLint("StaticFieldLeak")
    public class downloadMasterQuestionImages extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                ImageData imageData = LoganSquare.parse(params[0], ImageData.class);
                if (imageData.isStatus()) {
                    ImageRecords imageRecords = imageData.getImageRecordsList();

//                    if (imageRecords.getSubjectIcon() != null) {
//                        SubjectIcon subjectIcon = imageRecords.getSubjectIcon();
//                        File imageDir = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/");
//                        if (subjectIcon != null && subjectIcon.getImages().size() > 0) {
//                            for (int i = 0; i < subjectIcon.getImages().size(); i++) {
//                                if (Utils.isNetworkAvailable(PractiesSubjectActivity.this)) {
//                                    if (!imageDir.exists()) {
//                                        imageDir.mkdirs();
//                                    }
//                                    String imgUrl = IMAGEURL + "subject_icon/" + subjectIcon.getImages().get(i);
//                                    downloadFile(imgUrl, Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + subjectIcon.getImages().get(i));
//                                }
//                            }
//                        }
//                    }


                    if (imageRecords.getQuestionImages() != null) {
                        for (int k = 0; k < imageRecords.getQuestionImages().size(); k++) {
                            String folderPath = String.valueOf(imageRecords.getQuestionImages().get(k));
                            Log.d(TAG, "folderPath : " + folderPath);
                            if (folderPath.contains("http:")) {
                              //  folderPath = folderPath.substring(26);
                                folderPath = folderPath.substring(24);
                                folderPath = folderPath.substring(0, folderPath.lastIndexOf("/"));
                                Log.d(TAG, "folderPath http: " + folderPath);

                            } else {
                                folderPath = folderPath.substring(25);

                               // folderPath = folderPath.substring(27);

                                folderPath = folderPath.substring(0, folderPath.lastIndexOf("/"));
                                Log.d(TAG, "folderPath https: " + folderPath);

                            }
                            if (folderPath.contains("../")) {
                                folderPath = folderPath.replace("../", "");
                            }
                            if (folderPath.contains("&amp;")) {
                                folderPath = folderPath.replaceAll("&amp;", "&");
                            }

                            String tempPath = imageRecords.getQuestionImages().get(k);
                            Log.d(TAG, "tempPath : " + tempPath);

                            String fileName = tempPath.substring(tempPath.lastIndexOf("/") + 1);
                            Log.d(TAG, "fileName : " + fileName);

                            String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + folderPath + "/" + fileName;
                            Log.d(TAG, "SDCardPath : " + SDCardPath);
                            Log.d(TAG, "Constant.LOCAL_IMAGE_PATH  : " + Constant.LOCAL_IMAGE_PATH);
                            Log.d(TAG, "folderPath : " + folderPath);
                            Log.d(TAG, "fileName : " + fileName);

                            File imageFile = new File(SDCardPath);
                            if (Utils.isNetworkAvailable(PractiesSubjectActivity.this)) {
                                File dir = new File(imageFile.getParent());
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                downloadFile(imageRecords.getQuestionImages().get(k), SDCardPath);
                            }

                        }
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            mySharedPref.setMasterImageDownloadStatus(true);

//            new CountDownTimer(2000, 1000) { // adjust the milli seconds here
//                public void onTick(long millisUntilFinished) {
//                    mProgressMcqTestValue.setText(""+String.format("Image Downloading.. %d min, %d sec",
//                            TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
//                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
//                }
//
//                public void onFinish() {
            // mProgressMcqTestValue.setText("done!");
            callProgressUpdate(false);
//                }
//            }.start();
            // setdata();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void downloadFile(String url, String path) {
        Log.d(TAG, "downloadFile() called with: url = [" + url + "], path = [" + path + "]");
        File qLocalFile = new File(path);
        Request request = null;
        if (!qLocalFile.exists()) {
            request = new Request(url, path);
            request.setPriority(Priority.HIGH);
            request.setNetworkType(NetworkType.ALL);
            fetch.enqueue(request, updatedRequest -> {
                Log.d(TAG, "manageImagesDownload: ");
            }, error -> {
                Toast.makeText(this, "error enqueue download!!!", Toast.LENGTH_SHORT).show();
            });

        }

    }

    @OnClick({R.id.img_back, R.id.mDownloadMcqTest, R.id.img_refresh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.mDownloadMcqTest:
                if (Utils.isNetworkAvailable(this)) {
                    callDownloadEvent();
                } else {
                    Utils.showToast(getString(R.string.no_internet_msg), this);
                }
                break;
            case R.id.img_refresh:
                if (Utils.isNetworkAvailable(this)) {
                    mTransViewMcqTest.setVisibility(View.VISIBLE);
                    callDownloadEvent();
                } else {
                    Utils.showToast(getString(R.string.no_internet_msg), this);
                }
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        closeDb(mDb);
        fetch.close();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        mProgressMcqTestValue.setText("Click on the icon to download Practice Paper.");
        callProgressUpdate(false);
        if (event.isFaild()) {
            mErrorMsg.setText("Please retry.");
        }
    }

}
