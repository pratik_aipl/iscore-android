package com.parshvaa.isccore.tempmodel;

import com.parshvaa.isccore.model.ClassQuestionPaperSubQuestion;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 6/4/2018.
 */

public class SubQueKey implements Serializable {
    public String key="";

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    public ArrayList<ClassQuestionPaperSubQuestion> internalArray = new ArrayList<>();
    public ArrayList<ClassQuestionPaperSubQuestionRevision> RevisioninternalArray = new ArrayList<>();
}
