package com.parshvaa.isccore.custominterface;


import com.parshvaa.isccore.utils.Constant;

/**
 * Created by admin on 10/12/2016.
 */
public interface AsynchTaskListner {

    void onTaskCompleted(String result, Constant.REQUESTS request) ;

  }
