package com.parshvaa.isccore.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nineoldandroids.view.ViewHelper;
import com.parshvaa.isccore.model.Zooki;
import com.parshvaa.isccore.observscroll.ObservableScrollView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.R;

/**
 * Created by Karan Pitroda 7405646599
 */

public class ZookiFragment extends Fragment implements ObservableScrollViewCallbacks {
    public Zooki zookiObj;
    private TextView tv_FullDesc, tv_title, tv_date, tv_header_title;
    private ImageView mImageView;
    private ObservableScrollView mScrollView;
    public int mParallaxImageHeight;
    public Button btn_viewmore;
    public FrameLayout frame_content, frame_viewMore;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_common, null);
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_FullDesc = rootView.findViewById(R.id.tv_FullDesc);
        tv_date = rootView.findViewById(R.id.tv_date);
        btn_viewmore = rootView.findViewById(R.id.btn_viewmore);
        frame_content = rootView.findViewById(R.id.frame_content);
        frame_viewMore = rootView.findViewById(R.id.frame_viewMore);
        mImageView = rootView.findViewById(R.id.image);


      /*  if (zookiObj.getTitle().equals("View More")) {
            frame_content.setVisibility(View.GONE);
            frame_viewMore.setVisibility(View.VISIBLE);
            btn_viewmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isNetworkAvailable(getActivity())) {
                        startActivity(new Intent(getActivity(), ZookiWebActivity.class));
                    } else {
                        Utils.showToast(getResources().getString(R.string.conect_internet), getActivity());
                    }
                }
            });
        } else {
            tv_title.setText(zookiObj.getTitle());
            String date = zookiObj.getCreatedOn();
            try {
                date = date.substring(0, 10);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tv_date.setText(Utils.changeDateToDDMMYYYY(date));
            tv_FullDesc.setText(zookiObj.getDesc());
            File image = new File(Constant.LOCAL_IMAGE_PATH + "/zooki/" + zookiObj.getImage());
            if (image.exists()) {
                Utils.setImage(getActivity(),image,mImageView,R.drawable.warning);
            }


            mScrollView = rootView.findViewById(R.id.scroll);
            mScrollView.setScrollViewCallbacks(this);

            mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen._180sdp);
            onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
        }
*/

        return rootView;
    }

    /**
     * @param obj
     */
    public void bindData(Zooki obj) {
        this.zookiObj = obj;
    }

    /**
     * @param scrollY     Scroll position in Y axis.
     * @param firstScroll True when this is called for the first time in the consecutive motion events.
     * @param dragging    True when the view is dragged and false when the view is scrolled in the inertia.
     */
    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }
}
