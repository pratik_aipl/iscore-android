package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/4/2017.
 */

@JsonObject
public class StudentMcqTestDtl implements Serializable {
    @JsonField
    public int StudentMCQTestDTLID = -1, StudentMCQTestHDRID = -1;
    @JsonField
    public String QuestionTime;
    @JsonField
    public String QuestionID = "";
    @JsonField
    public String AnswerID = "";
    @JsonField
    public String IsAttempt = "";
    @JsonField
    public String srNo = "";
    @JsonField
    public String CreatedBy = "";
    @JsonField
    public String CreatedOn = "";
    @JsonField
    public String ModifiedBy = "";
    @JsonField
    public String ModifiedOn = "";

    public String getQuestionTime() {
        return QuestionTime;
    }

    public void setQuestionTime(String questionTime) {
        QuestionTime = questionTime;
    }

    public int getStudentMCQTestDTLID() {
        return StudentMCQTestDTLID;
    }

    public void setStudentMCQTestDTLID(int studentMCQTestDTLID) {
        StudentMCQTestDTLID = studentMCQTestDTLID;
    }

    public int getStudentMCQTestHDRID() {
        return StudentMCQTestHDRID;
    }

    public void setStudentMCQTestHDRID(int studentMCQTestHDRID) {
        StudentMCQTestHDRID = studentMCQTestHDRID;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getIsAttempt() {
        return IsAttempt;
    }

    public void setIsAttempt(String isAttempt) {
        IsAttempt = isAttempt;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

}
