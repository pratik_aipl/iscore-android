package com.parshvaa.isccore.activity.paytm;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Karan - Empiere on 5/11/2018.
 */

public class VerifyChecksum {
    @SerializedName("IS_CHECKSUM_VALID")
    private String IS_CHECKSUM_VALID;
    public VerifyChecksum(String is_valid) {
        this.IS_CHECKSUM_VALID = is_valid;
    }

    public String getValidChecksumHash() {
        return IS_CHECKSUM_VALID;
    }
}
