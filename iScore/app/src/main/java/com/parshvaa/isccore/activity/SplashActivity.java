package com.parshvaa.isccore.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.event.EventDetailsActivity;
import com.parshvaa.isccore.activity.event.EventListActivity;
import com.parshvaa.isccore.activity.library.videoPleyar.utility.Util;
import com.parshvaa.isccore.activity.mcq.SubjectMcqActivity;
import com.parshvaa.isccore.activity.practiespaper.activity.PractiesSubjectActivity;
import com.parshvaa.isccore.activity.referal.ReferalActivity;
import com.parshvaa.isccore.activity.revisionnote.activity.RevisionNoteSubjectActivity;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.custominterface.OnTaskCompletedDashboard;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.iconnect.activity.CctTestActivity;
import com.parshvaa.isccore.activity.iconnect.activity.IGeneratePaperActivity;
import com.parshvaa.isccore.activity.iconnect.activity.ISetPaperActivity;
import com.parshvaa.isccore.activity.iconnect.activity.NoticeBoardActivity;
import com.parshvaa.isccore.activity.library.activity.LibrarySubjectActivity;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBTables;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.db.MyDBManagerOneSignal;
import com.parshvaa.isccore.model.MainData;
import com.parshvaa.isccore.model.NotificationOneSignal;
import com.parshvaa.isccore.model.NotificationType;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.Utils;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class SplashActivity extends BaseActivity implements OnTaskCompletedDashboard, AsynchTaskListner {
    private static final String TAG = "SplashActivity";
    private final int SPLASH_DISPLAY_LENGTH = 2500;
    public int DATE_TIME_REQUEST = 999;
    public SplashActivity instance;
    public MyDBManager mDb;
    public App app;
    public String LastVersion;
    String version = "";
    public ImageView img_class_logo;
    public static ArrayList<NotificationOneSignal> OneSignalArray = new ArrayList<>();
    RxPermissions rxPermissions;
    AQuery aQuery;
    AlertDialog alertDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        app = App.getInstance();
        App.mainUser = Utils.DataSherdToAppClass(app,mySharedPref);
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        aQuery = new AQuery(this);
        rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        callData();
                    } else {
                        // Oops permission denied
                    }
                });


    }


    private void callData() {
        instance = this;
        app = App.getInstance();
        int verCode = Utils.currentAppVersionCode(instance);
        version = String.valueOf(verCode);

        Log.d(TAG, "callData: " + mySharedPref.getClassLogo2());
        //------------  NotificationOneSignal  Start  --------------------//
        if (!mySharedPref.getIsProperLogin().equals("true")) {
            Utils.logUser();
            App.deleteCache(instance);
            mySharedPref.clearApp();
            mySharedPref.setIsLoggedIn(false);
            mySharedPref.setDownloadStatus("n");
            resumeManually();
            startActivity(new Intent(SplashActivity.this, TourActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        } else {
            Log.d(TAG, "callData: else ");
            if (mySharedPref.getIsLoggedIn()) {
                if (mySharedPref.getDownloadStatus().equals("y")) {
                    if (App.isNoti) {
                        new getRecorrd(instance).execute();
                    }
                }
            }
            //------------  NotificationOneSignal  END --------------------//

            if (mySharedPref.getIsLoggedIn()) {
                Log.d(TAG, "Splash Logo: " + mySharedPref.getClassLogo2());
                if (!mySharedPref.getClassLogo2().isEmpty()) {
                    String fileName = mySharedPref.getClassLogo2().substring(mySharedPref.getClassLogo2().lastIndexOf('/') + 1);
                    File imageDir = new File(Constant.LOCAL_IMAGE_PATH + "/class/");
                    File imageFile = new File(imageDir.getAbsolutePath() + "/" + fileName);
                    Log.d(TAG, "callData: " + imageFile.getAbsolutePath());
                    setContentView(R.layout.activity_class_splash);
                    img_class_logo = findViewById(R.id.img_class_logo);
                    if (!TextUtils.isEmpty(fileName)) {
                        if (imageFile.exists()) {
                            Utils.setImage(this, imageFile, img_class_logo,R.drawable.warning);
                        } else {
                            if (Utils.isNetworkAvailable(this)) {
                                Util.downloadClassIcon(this, aQuery, mySharedPref.getClassLogo2(), img_class_logo, imageFile, R.drawable.warning);
                            }
                        }
                    } else {
                        img_class_logo.setImageResource(R.drawable.profile);
                    }
                } else {
                    setContentView(R.layout.activity_splash);
                    //  Utils.logUser();
                }
                if (Utils.checkOutDated(mySharedPref.getExpiryDate())) {
                    new Handler().postDelayed(() -> {
                        Utils.DataSherdToAppClass(app, mySharedPref);
                        App.app_Key = mySharedPref.getReg_key();

                        if (!version.equalsIgnoreCase(mySharedPref.getVersioCode())) {
                            goDownloadDataAndSync(BuildConfig.VERSION_CODE);
                        } else {
                            if (Utils.isTimeAutomatic(instance)) {
                                manageLogin();
                            } else {
                                setAutomaticTimeDateAlert();
                            }
                        }


                    }, SPLASH_DISPLAY_LENGTH);
                } else {
                    new androidx.appcompat.app.AlertDialog.Builder(this)
                            .setTitle("Expired")
                            .setMessage("Your iSccore Product has Expired, Kindly contact Parshvaa Publications")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }


            } else {
                setContentView(R.layout.activity_splash);
                Utils.logUser();
                new Handler().postDelayed(() -> {
                    startActivity(new Intent(SplashActivity.this, TourActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }, SPLASH_DISPLAY_LENGTH);
            }
        }
    }

    public void resumeManually() {
        DBTables dbTables = new DBTables();
        dbTables.createAll(mDb);
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Log.d(TAG, "onTaskCompleted: " + result);
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case getTableRecords:
                    showProgress(false, true);
                    try {
                        MainData mainData = LoganSquare.parse(result, MainData.class);
                        if (mainData.isStatus()) {
                            prefs.save(Constant.tableRecords, gson.toJson(mainData.getData()));
                            tableRecords = mainData.getData();
                            Log.d(TAG, "onTaskCompleted>>> " + gson.toJson(tableRecords));
                        }
                        showProgress(false, true);
                        callData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } else {
            callData();
        }
    }


    public void goDownloadDataAndSync(int LastVersion) {
        if (mySharedPref.getIsLoggedIn()) {
            if (mySharedPref.getFirstName().isEmpty() && mySharedPref.getLastName().isEmpty()) {
                redirectToLogin();
            } else {
                Utils.DataSherdToAppClass(app, mySharedPref);
                startActivity(new Intent(instance, DashBoardActivity.class)
                        .putExtra("LastVersion", BuildConfig.VERSION_CODE)
                        .putExtra("sync", "y"));
            }
        } else {
            redirectToLogin();
        }
    }

    public void redirectToLogin() {
        Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    public void goToDownloadOrHome(boolean isCompleteDownloaded) {
        if (!isCompleteDownloaded) {
            LastVersion = mySharedPref.getVersioCode();
            Utils.Log("TAG", "SPlASH : " + LastVersion);
            Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("LastVersion", LastVersion);
            startActivity(i);
            finish();
        } else {
            App.Target = mySharedPref.getTarget();
            if (App.isNoti) {
                if (App.isType.equalsIgnoreCase("cct")) {
                    String id = getIntent().getExtras().getString("Notification_type_id");
                    int NotificationID = getIntent().getExtras().getInt("NotificationID");
                    Intent intent = new Intent(instance, CctTestActivity.class);
                    intent.putExtra("id", id);
                    intent.putExtra("NotificationID", NotificationID);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("event")) {
                    Intent intent = new Intent(instance, EventListActivity.class);
                    startActivity(intent);
                    finish();
                }else if (App.isType.equalsIgnoreCase("event_register")) {
                    String id = getIntent().getExtras().getString("Event_id");
                    Intent intent = new Intent(instance, EventDetailsActivity.class);
                    intent.putExtra("Event_ID", id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }else if (App.isType.equalsIgnoreCase("refernearn")) {
                    Intent intent = new Intent(instance, ReferalActivity.class);
                    startActivity(intent);
                    finish();
                }else if (App.isType.equalsIgnoreCase("mcq")) {
                    Intent intent = new Intent(instance, SubjectMcqActivity.class);
                    startActivity(intent);
                    finish();
                }else if (App.isType.equalsIgnoreCase("practice")) {
                    Intent intent = new Intent(instance, PractiesSubjectActivity.class);
                    startActivity(intent);
                    finish();
                }else if (App.isType.equalsIgnoreCase("notes")) {
                    Intent intent = new Intent(instance, RevisionNoteSubjectActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("dashboard")) {
                    Intent intent = new Intent(instance, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("callnow")) {
                    String phone = "+917710012112";
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                } else if (App.isType.equalsIgnoreCase("subscribe")) {
                    Intent intent = new Intent(instance, ChooseOptionActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("referral")) {
                    Intent intent = new Intent(instance, ReferalActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("notice_board")) {
                    Intent intent = new Intent(instance, NoticeBoardActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("library")) {
                    Intent intent = new Intent(instance, LibrarySubjectActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("test_paper")) {
                    String id = getIntent().getExtras().getString("Notification_type_id");
                    String paper_type = getIntent().getExtras().getString("paper_type");
                    if (paper_type.equals("1") || paper_type.equals("2")) {
                        Intent intent = new Intent(instance, IGeneratePaperActivity.class);
                        intent.putExtra("id", id);
                        intent.putExtra(Constant.TYPE, "TESTPAPER");
                        intent.putExtra(Constant.isOnlyQuestionPaper, "1");
                        intent.putExtra(Constant.AnswerPaperDate, "");
                        startActivity(intent);
                        finish();
                    } else if (paper_type.equals("3")) {
                        Intent intent = new Intent(instance, ISetPaperActivity.class);
                        intent.putExtra("id", id);
                        intent.putExtra(Constant.TYPE, "TESTPAPER");
                        intent.putExtra(Constant.isOnlyQuestionPaper, "1");
                        intent.putExtra(Constant.AnswerPaperDate, "");
                        startActivity(intent);
                        finish();
                    }

                } else if (App.isType.equalsIgnoreCase("zooki")) {
                    Intent intent = new Intent(instance, ZookiNotificationActivity.class);
                    intent.putExtra("title", getIntent().getExtras().getString("title"));
                    intent.putExtra("bigPicture", getIntent().getExtras().getString("bigPicture"));
                    intent.putExtra("created_on", getIntent().getExtras().getString("created_on"));
                    intent.putExtra("body", getIntent().getExtras().getString("body"));
                    startActivity(intent);
                    finish();
                } else {
                    Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }
            } else {

                Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
                i.putExtra("sync", "sync");
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        }
    }

    public void setAutomaticTimeDateAlert() {
        alertDialog = new AlertDialog.Builder(SplashActivity.this)
                .setTitle("Date Time ")
                .setMessage("Automatic Date time is not enabled, Do you want to enable it?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), DATE_TIME_REQUEST);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.showToast("Please enable automatic date time for use this app", SplashActivity.this);
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.warning)
                .show();
    }

    public void manageLogin() {
        if (mySharedPref.getDownloadStatus().equals("n")) {
            if (!Utils.isNetworkAvailable(this)) {
                alertDialog = new AlertDialog.Builder(SplashActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("oops..!")
                        .setCancelable(false)
                        .setMessage("kindly switch on internet connection!")
                        .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                manageLogin();
                            }
                        })
                        .show();
            } else {
                goToDownloadOrHome(true);
            }
        } else {

            goToDownloadOrHome(true);

        }
    }

    @Override
    public void onGetRecorComplite() {

    }

    @Override
    public void onInsertRecorComplite() {

    }

    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public OnTaskCompletedDashboard listener;
        public String SubjectName = "", created_on = "", notification_type_id = "", type = "";


        private getRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);
            this.listener = (OnTaskCompletedDashboard) context;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            if (mDb.isTableExists(Constant.notification)) {
                int N_ID = mDb.getLastRecordIDForRequest("notification", "OneSignalID");
                if (N_ID != -1) {
                    Cursor noti = mDbOneSignal.getAllRows(DBNewQuery.getNotificatonDash(N_ID));
                    if (noti != null && noti.moveToFirst()) {
                        do {
                            NotificationOneSignal notiObj = (NotificationOneSignal) cParse.parseCursor(noti, new NotificationOneSignal());
                            OneSignalArray.add(notiObj);
                        } while (noti.moveToNext());
                        noti.close();
                    }
                }
            }

            if (OneSignalArray != null && OneSignalArray.size() > 0) {
                for (int i = 0; i < OneSignalArray.size(); i++) {
                    try {
                        NotificationType typeObj = new NotificationType();

                        JSONObject jObj = new JSONObject(OneSignalArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        notification_type_id = jtype.getString("notification_type_id");
                        created_on = jtype.getString("created_on");
                        SubjectName = jtype.getString("subject_name");
                        typeObj.setOneSignalID(Integer.parseInt(OneSignalArray.get(i).get_id()));
                        typeObj.setCreatedOn(created_on);
                        typeObj.setIsOpen(OneSignalArray.get(i).getOpened());
                        typeObj.setIsTypeOpen("0");
                        typeObj.setSubjectName(SubjectName);
                        typeObj.setNotification_type_id(notification_type_id);
                        typeObj.setModuleType(type);
                        typeObj.setMessage(OneSignalArray.get(i).getMessage());
                        typeObj.setTitle(OneSignalArray.get(i).getTitle());
                        mDb.insertWebRow(typeObj, "notification");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.onGetRecorComplite();
        }
    }


    @Override
    protected void onDestroy() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        Utils.closeDb(mDb);
        super.onDestroy();
    }
}

