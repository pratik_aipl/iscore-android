package com.parshvaa.isccore.activity.mcq;

import android.os.Bundle;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Utils;

public class TypeOfQuestionsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_type_of_questions);
        Utils.logUser();

    }
}
