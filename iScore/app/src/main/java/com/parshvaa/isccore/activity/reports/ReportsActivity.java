package com.parshvaa.isccore.activity.reports;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.db.DBTables;
import com.parshvaa.isccore.model.CctReport;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.parshvaa.isccore.App.PractisePaperTotal;
import static com.parshvaa.isccore.App.cctArray;

public class ReportsActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, AsynchTaskListner {
    private static final String TAG = "ReportsActivity";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public MyDBManager mDb;
    public int attempted;
    public static TextView tv_accuracy_level, tv_target;
    public String testHeaderIDS = "";
    public CursorParserUniversal cParse;
    public App app;
    public ReportsActivity instance;
    public ImageView img_back;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_reports);
        Utils.logUser();

        instance = this;
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        app = App.getInstance();
        cParse = new CursorParserUniversal();
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Reports");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_accuracy_level = findViewById(R.id.tv_accuracy_level);
        testHeaderIDS = getTestHeaderID();
        int totalAttQue = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(testHeaderIDS));
        int totalRightAns = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(testHeaderIDS));

        App.mcqTotalAccuracy = 0;
        App.mcqTotalAccuracy = Integer.parseInt(Utils.getAccuraccy(totalAttQue, totalRightAns));
        tv_accuracy_level.setText(App.mcqTotalAccuracy + "");
        tv_target = findViewById(R.id.tv_target);
        tv_target.setText(mySharedPref.getlblTarget());
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabFont();

        if (Utils.isNetworkAvailable(this)) {
            new CallRequest(instance).getIcctReport();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setupTabFont() {

        TextView OverAll = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        OverAll.setText("Overall");
        tabLayout.getTabAt(0).setCustomView(OverAll);

        TextView MCQ = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        MCQ.setText("MCQ");
        tabLayout.getTabAt(1).setCustomView(MCQ);

    /*    TextView CCT = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        CCT.setText("CCT");
        tabLayout.getTabAt(2).setCustomView(CCT);*/
        TextView Test = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        Test.setText("Practise");
        tabLayout.getTabAt(2).setCustomView(Test);
        TextView Comparision = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        Comparision.setText("Comparison");
        tabLayout.getTabAt(3).setCustomView(Comparision);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(instance.getSupportFragmentManager());
        adapter.addFragment(new ReportOverAllFragment(), "Overall");
        adapter.addFragment(new ReportMCQFragment(), "MCQ");
    //    adapter.addFragment(new ReportCCTFragment(), "CCT");
        adapter.addFragment(new ReportTestFragment(), "Practise");
        adapter.addFragment(new ReportComparisionFragment(), "Comparison");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case getCctReport:
                    showProgress(false, true);
                    Log.d(TAG, "onTaskCompleted: " + result);
                    new insertCCTReportRow().execute(result);
                    break;
            }
        }
    }

    public class insertCCTReportRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            JSONObject jObj = null;
            try {
                jObj = new JSONObject(params[0]);
                if (jObj.getBoolean("status") && jObj.has("data")) {
                    JSONArray jData = jObj.getJSONArray("data");
                    List<CctReport> cctReportArray = LoganSquare.parseList(jData.toString(), CctReport.class);
                    if (!mDb.isTableExists(Constant.class_evaluater_cct_count))
                        new DBTables().class_evaluater_cct_count(mDb);

                    mDb.emptyTable(Constant.class_evaluater_cct_count);
                    for (int i = 0; i < cctReportArray.size(); i++) {
                        mDb.insertWebRow(cctReportArray.get(i), Constant.class_evaluater_cct_count);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public String getTestHeaderID() {
        String IDS = "";
        StudentMcqTestHdr stdntHdr;
        Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllRowQuery("student_mcq_test_hdr"));
        if (MCqHdr != null && MCqHdr.moveToFirst()) {
            do {
                stdntHdr = (StudentMcqTestHdr) cParse.parseCursor(MCqHdr, new StudentMcqTestHdr());
                IDS += stdntHdr.getStudentMCQTestHDRID() + ",";
            } while (MCqHdr.moveToNext());
            MCqHdr.close();
        }

        return Utils.removeLastComma(IDS);
    }

    @Override
    protected void onDestroy() {
        cctArray = null;
        App.cctTotal = 0;
        App.cctTotalAccuracy = 0;
        App.mcqTotalAccuracy = 0;
        App.McqTotal = 0;
        PractisePaperTotal = 0;
        super.onDestroy();
    }
}