package com.parshvaa.isccore.utils;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.Button;

/**
* Created by empiere-vaibhav on 9/5/2018.
*/

public class RobotoButton extends Button {
public RobotoButton(Context context) {
super(context);


this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Roboto-Regular.ttf")));


this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
}

public RobotoButton(Context context, AttributeSet attrs) {
super(context, attrs);


this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Roboto-Regular.ttf")));

this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
}

public RobotoButton(Context context, AttributeSet attrs, int defStyle) {
super(context, attrs, defStyle);


this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Roboto-Regular.ttf")));

this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
}
}