package com.parshvaa.isccore.activity.textbook;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.util.List;

public class TextBookSubjectAdapter extends RecyclerView.Adapter<TextBookSubjectAdapter.MyViewHolder> {

    private List<TextBook> subjectList;
    public Context context;
    public AQuery aQuery;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name;
        public ImageView img_sub;
        public View lineview;
        public ProgressBar pBar;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.lbl_level_one);
            img_sub = view.findViewById(R.id.img_sub);
            lineview = view.findViewById(R.id.view);
            pBar = view.findViewById(R.id.pBar);
        }
    }


    public TextBookSubjectAdapter(List<TextBook> moviesList, Context context) {
        this.subjectList = moviesList;
        this.context = context;
        aQuery = new AQuery(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subject_revision_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_subject_name.setText(subjectList.get(position).getSubjectName());

        holder.pBar.setVisibility(View.GONE);
        Utils.setImage(context, subjectList.get(position).getSubjectIcon(), holder.img_sub, R.drawable.warning);


        Utils.Log("TAG", "SUBJECT  :-> " + subjectList.get(position).getSubjectIcon());
        holder.lineview.setVisibility(View.GONE);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, TextBookChapterActivity.class)
                        .putExtra("SubjectID", subjectList.get(position).getSubjectID()));

            }
        });
    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }
}



