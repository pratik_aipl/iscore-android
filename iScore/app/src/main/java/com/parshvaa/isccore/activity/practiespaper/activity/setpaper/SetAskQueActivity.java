package com.parshvaa.isccore.activity.practiespaper.activity.setpaper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.practiespaper.adapter.SetPaperTypeAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.SetPaperQuestionTypes;
import com.parshvaa.isccore.model.SetPaperType;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SetAskQueActivity extends BaseActivity {
    private List<SetPaperType> setPaperTypeList = new ArrayList<>();
    private RecyclerView recycler_set_paper_type;
    private SetPaperTypeAdapter setPaperTypeAdapter;
    public ImageView img_back;
    public TextView tv_title;
    public Button btn_next;
    public SetAskQueActivity instance;
    public ArrayList<SetPaperQuestionTypes> questionTypesArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_set_paper);
        Utils.logUser();


        instance = this;
        recycler_set_paper_type = findViewById(R.id.recycler_set_paper_type);
        img_back = findViewById(R.id.img_back);
        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                App.practiesPaperObj.setPaperQuestionTypesArray = setPaperTypeAdapter.getSelQuestionTypesArray();
                if(setPaperTypeAdapter.getTempsArray().size() == 0){
                    Utils.showToast("Please select the question type.", instance);
                }else{
                    if (App.practiesPaperObj.setPaperQuestionTypesArray == null || App.practiesPaperObj.setPaperQuestionTypesArray.size() == 0) {}
                    else {
                        Utils.Log("TAG ", "Array size:- > " + setPaperTypeAdapter.getSelQuestionTypesArray().size());

                        startActivity(new Intent(SetAskQueActivity.this, SetAskQuePreviewActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        );
                    }

                }


            }
        });
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Set Paper");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        try {
            questionTypesArrayList = (ArrayList) new CustomDatabaseQuery(this, new SetPaperQuestionTypes())
                    .execute(new String[]{DBNewQuery.getSubjectWiseQuesTypesSetpaper(App.practiesPaperObj.getSubjectID(), App.practiesPaperObj.getChapterID())}).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupRecyclerView();
    }


    private void setupRecyclerView() {
        SetPaperQuestionTypes obj = new SetPaperQuestionTypes();
        obj.setType("footer");
        questionTypesArrayList.add(obj);
        final Context context = recycler_set_paper_type.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._8sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_set_paper_type.setLayoutAnimation(controller);
        recycler_set_paper_type.scheduleLayoutAnimation();
        recycler_set_paper_type.setLayoutManager(new LinearLayoutManager(context));
        setPaperTypeAdapter = new SetPaperTypeAdapter(questionTypesArrayList, instance, recycler_set_paper_type);
        recycler_set_paper_type.setAdapter(setPaperTypeAdapter);
        recycler_set_paper_type.addItemDecoration(new ItemOffsetDecoration(spacing));

    }
}

