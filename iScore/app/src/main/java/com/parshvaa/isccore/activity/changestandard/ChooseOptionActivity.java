package com.parshvaa.isccore.activity.changestandard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class   ChooseOptionActivity extends BaseActivity {

    public ChooseOptionActivity instance;
    public RecyclerView rv_options;
    public ImageView img_back;
    public TextView tv_title;
    public ArrayList<Options> optionList = new ArrayList<>();

    public String[] title = {"Already Have A Key!", "Purchase A Key!"};
    public String[] disc = {"Please enter iSccore activation key received from your institute.", "You are few steps away from purchasing a license key of iSccore. Please follow instructions for making a payment."};
    public int[] icon = {R.drawable.already_key, R.drawable.purchase_key};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_choose_option);
        Utils.logUser();
        setContentView(R.layout.activity_choose_option);
        instance = this;
        rv_options = findViewById(R.id.rv_options);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Choose options");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        for (int i = 0; i < title.length; i++) {
            optionList.add(new Options(title[i], disc[i], icon[i]));
        }
        setupRecyclerView();
    }

    public void setupRecyclerView() {
        final Context context = rv_options.getContext();

        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rv_options.setLayoutAnimation(controller);
        rv_options.scheduleLayoutAnimation();
        rv_options.setLayoutManager(new LinearLayoutManager(context));
        OptionAdapter adapter = new OptionAdapter(optionList, context);
        rv_options.setAdapter(adapter);

    }

    public class Options implements Serializable {
        public String Title = "", Discription = "";
        public int Icone;

        public Options(String Title, String Discription, int Icone) {
            this.Title = Title;
            this.Discription = Discription;
            this.Icone = Icone;
        }


    }

    public class OptionAdapter extends RecyclerView.Adapter<OptionAdapter.MyViewHolder> {

        private List<Options> optList;
        public Context context;
        Options subObj;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_title, tv_discription;
            public ImageView img_option;

            public MyViewHolder(View view) {
                super(view);
                tv_title = view.findViewById(R.id.tv_title);
                tv_discription = view.findViewById(R.id.tv_discription);
                img_option = view.findViewById(R.id.img_option);
            }
        }


        public OptionAdapter(List<Options> moviesList, Context context) {
            this.optList = moviesList;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_choose_option_row, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.tv_title.setText(optList.get(position).Title);
            holder.tv_discription.setText(optList.get(position).Discription);
            holder.img_option.setImageResource(optList.get(position).Icone);


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (position == 0) {
                        context.startActivity(new Intent(context, ChangeKeyActivity.class));
                        //  .putExtra("suObj", mcqdata));
                    }
                    if (position == 1) {
                        context.startActivity(new Intent(context, BoardActivity.class));
                        //  .putExtra("suObj", mcqdata));
                    }
                }
            });

        }


        @Override
        public int getItemCount() {
            return optList.size();
        }
    }
}
