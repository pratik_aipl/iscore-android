package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/26/2018.
 */
@JsonObject
public class StudentMcqTestLevel implements Serializable {

    @JsonField
    public String StudentMCQTestHDRID = "", LevelID = "";
    @JsonField
    public int StudentMCQTestLevelID = -1;

    public int getStudentMCQTestLevelID() {
        return StudentMCQTestLevelID;
    }

    public void setStudentMCQTestLevelID(int studentMCQTestLevelID) {
        StudentMCQTestLevelID = studentMCQTestLevelID;
    }

    public String getStudentMCQTestHDRID() {
        return StudentMCQTestHDRID;
    }

    public void setStudentMCQTestHDRID(String studentMCQTestHDRID) {
        StudentMCQTestHDRID = studentMCQTestHDRID;
    }

    public String getLevelID() {
        return LevelID;
    }

    public void setLevelID(String levelID) {
        LevelID = levelID;
    }
}
