package com.parshvaa.isccore.activity.searchpaper.feftdrawer;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by empiere-vaibhav on 1/11/2018.
 */

public class LeftSubjectAdapter extends RecyclerView.Adapter<LeftSubjectAdapter.MyViewHolder> {
    List<Subject> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    ArrayList<String> checked_items = new ArrayList<>();

    public LeftSubjectAdapter(List<Subject> data, Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;


    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_checkbox_sub_raw, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Subject current = data.get(position);
        holder.chk_group.setText(current.getSubjectName());
        holder.chk_group.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    compoundButton.setChecked(true);
                    App.checked_subject.add(current.getSubjectID());
                    checked_items.add(current.getSubjectID());

                } else {
                    compoundButton.setChecked(false);
                    App.checked_subject.remove(current.getSubjectID());
                    checked_items.remove(current.getSubjectID());
                }
            }
        });
    }


    public ArrayList<String> checkdItem() {
        return checked_items;
    }

    public String getSelectedChapters() {
        return android.text.TextUtils.join(",", checked_items);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox chk_group;
        View itemView;

        public MyViewHolder(View itemView) {
            super(itemView);
            chk_group = itemView.findViewById(R.id.chk_group);
            this.itemView = itemView;

        }
    }
}

