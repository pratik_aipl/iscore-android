package com.parshvaa.isccore.activity.iconnect.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.iconnect.fragment.IPractiesPaperSolutionFragment;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.PrelimTestRecord;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.PrelimQuestionListModel;
import com.parshvaa.isccore.tempmodel.SetPaperQuestionAndTypes;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class IGeneratePaperActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "IGeneratePaperActivity";
    public ArrayList<PrelimQuestionListModel> ReadyquestionsList = new ArrayList<>();
    public PrelimTestRecord preTestObj;
    public PrelimQuestionListModel preQueObj;
    public JsonParserUniversal jParser;
    public ViewPager viewPager;
    public MyViewPagerAdapter myViewPagerAdapter;
    public FloatingActionButton float_right_button, float_left_button;
    public TextView tv_pager_count;
    public ImageView img_back, img_home, img_right, img_left;
    public TextView tv_title;
    public IGeneratePaperActivity instance;
    public Button btn_privew_paper, btn_model_paper;
    public static ArrayList<PrelimQuestionListModel> questionsList = new ArrayList<>();
    PrelimQuestionListModel questionListModel;
    SetPaperQuestionAndTypes setPaperQuestionAndTypes;
    String paper_id, isOnlyQuestionPaper, AnswerPaperDate,UPLOADSTARTTIME,UPLOADENDTIME,STARTTIME,ENDTIME,FROM;

    public MyDBManager mDb;
    long milli,Tenmilli,Currentmillisecond,millii;
    View parentLayout;
    Date Startdate,Enddate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_generate_paper_solution);

        parentLayout = findViewById(android.R.id.content);

        Utils.logUser();
        jParser = new JsonParserUniversal();
        instance = this;
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);

        paper_id = getIntent().getExtras().getString("id");
        isOnlyQuestionPaper = getIntent().getExtras().getString(Constant.isOnlyQuestionPaper);
        AnswerPaperDate = getIntent().getExtras().getString(Constant.AnswerPaperDate);
        FROM = getIntent().getExtras().getString(Constant.TYPE);


        img_home = findViewById(R.id.img_home);

        if(FROM.equalsIgnoreCase("TESTPAPER")){
            UPLOADSTARTTIME = getIntent().getExtras().getString(Constant.UPLOADSTARTTIME);
            UPLOADENDTIME = getIntent().getExtras().getString(Constant.UPLOADENDTIME);
            STARTTIME = getIntent().getExtras().getString(Constant.STARTTIME);
            ENDTIME = getIntent().getExtras().getString(Constant.ENDTIME);

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
            try {
                 Startdate = format.parse(STARTTIME);
                 Enddate = format.parse(ENDTIME);
                Currentmillisecond = new Date().getTime();

                Log.d(TAG, "startmilli: "+Startdate.getTime());
                Log.d(TAG, "Endmilli: "+Enddate.getTime());
                Log.d(TAG, "currentmilli: "+Currentmillisecond);
                milli=Enddate.getTime()-Currentmillisecond;
                Tenmilli=milli-600000;

            } catch (ParseException e) {
                e.printStackTrace();
            }
            new CountDownTimer(Tenmilli,1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }
                @Override
                public void onFinish() {
                    millii=Enddate.getTime()-Currentmillisecond;
                    milliseconds(millii);
                }
            }.start();

            new CountDownTimer(milli,1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }
                @Override
                public void onFinish() {
                    gotoBack();
                }
            }.start();

            img_home.setVisibility(View.VISIBLE);
        }else{
            img_home.setVisibility(View.GONE);

        }

        mDb.dbQuery("UPDATE notification SET isTypeOpen = 1 WHERE ModuleType = 'test_paper' AND notification_type_id = " + paper_id);

        showProgress(true, true);
        new CallRequest(instance).get_single_question_paper(paper_id);

        viewPager = findViewById(R.id.pager);
        img_home.setImageResource(R.drawable.addimg);
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });
        btn_privew_paper = findViewById(R.id.btn_privew_paper);
        btn_model_paper = findViewById(R.id.btn_model_paper);
        tv_pager_count = findViewById(R.id.tv_pager_count);
        img_back = findViewById(R.id.img_back);
        img_right = findViewById(R.id.img_right);
        img_left = findViewById(R.id.img_left);
        tv_title = findViewById(R.id.tv_title);
        float_right_button = findViewById(R.id.float_right_button);
        float_left_button = findViewById(R.id.float_left_button);


        btn_privew_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, IPreviewPaperActivity.class)
                        .putExtra("paper_id", paper_id)
                        .putExtra("privew", "privew")
                        .putExtra(Constant.TYPE,"GENERATE")
                        .putExtra(Constant.FROM,FROM)
                        .putExtra(Constant.UPLOADSTARTTIME, UPLOADSTARTTIME)
                        .putExtra(Constant.UPLOADENDTIME, UPLOADENDTIME)
                        .putExtra(Constant.STARTTIME, STARTTIME)
                        .putExtra(Constant.ENDTIME, ENDTIME)
                );
            }
        });

        if (!isOnlyQuestionPaper.equalsIgnoreCase("1")) {
            if (!isOnlyQuestionPaper.equalsIgnoreCase("1") && Utils.checkDate(AnswerPaperDate)) {
                btn_model_paper.setVisibility(View.VISIBLE);
            } else {
                btn_model_paper.setVisibility(View.GONE);
            }
        } else {
            btn_model_paper.setVisibility(View.GONE);
        }

        btn_model_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, IPreviewPaperActivity.class)
                        .putExtra("paper_id", paper_id).putExtra("model", "model"));
            }
        });
        tv_title.setText("Practice Test");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        float_left_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        float_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < 10) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });
        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < 10) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });


    }

    public void gotoBack() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("You have completed your time limit for your test.\n All the best")
                .setPositiveButton("Yes", (dialog, which) -> {
                    onBackPressed();

                })
                .show();
    }

    public void milliseconds(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;
        Snackbar snackbar = Snackbar
                .make(parentLayout, "Exam over in "+ minutes+" minutes.\n Good luck !", Snackbar.LENGTH_LONG);
        snackbar.show();
    }


  public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("Are You Sure You want To Upload Paper ?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    showProgress(true, true);//Utils.showProgressDialog(instance);
                    startActivity(new Intent(instance, UploadPaperImagesActivity.class)
                            .putExtra(Constant.PAPERID,paper_id)
                            .putExtra(Constant.TYPE,"GENERATE"));
                    finish();


                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    public class MyViewPagerAdapter extends FragmentPagerAdapter {
        int mNumOfTabs;
        PrelimQuestionListModel questionMod;

        public MyViewPagerAdapter(FragmentManager fm, int i, PrelimQuestionListModel questionMode) {
            super(fm);
            this.mNumOfTabs = i;
            this.questionMod = questionMod;
        }

        @Override
        public Fragment getItem(int position) {
            return new IPractiesPaperSolutionFragment().newInstance(position, questionMod,(btn_model_paper.getVisibility()==View.VISIBLE));
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return String.valueOf(position + 1);
        }


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.d(TAG, "resulttt>>: "+result);
            switch (request) {

                case get_single_question_paper:
                    showProgress(false, true);
                    questionsList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {
                                parseData(jDataArray);
                            }
                        } else {
                            showProgress(false, true);
                            showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }


    private void parseData(JSONArray jsonArray) {

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                preQueObj = new PrelimQuestionListModel();
                preQueObj.setPos(jsonObject.getString("pos"));
                JSONArray jPrelimListArray = jsonObject.getJSONArray("prelimList");
                for (int j = 0; j < jPrelimListArray.length(); j++) {
                    JSONObject jPreListObjest = jPrelimListArray.getJSONObject(j);
                    preTestObj = LoganSquare.parse(jPreListObjest.toString(),PrelimTestRecord.class);
                    preQueObj.prelimList.add(preTestObj);
                }
                questionsList.add(preQueObj);
                myViewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager(), questionsList.size(), questionListModel);
                viewPager.setAdapter(myViewPagerAdapter);
                int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
                viewPager.setPageMargin(pageMargin);
                myViewPagerAdapter.notifyDataSetChanged();

               tv_pager_count.setText(1 + "/" + questionsList.size());
                viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        tv_pager_count.setText(position + 1 + "/" + questionsList.size());
                        if ((questionsList.size() - 1) == position) {

                            float_right_button.hide();
                        } else {
                            float_right_button.show();
                        }
                        if (position == 0) {
                            float_left_button.hide();
                        } else {
                            float_left_button.show();
                        }
                    }
                });
                showProgress(false, true);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (questionsList.size() == 1) {
            float_left_button.hide();
            float_right_button.hide();
        } else {
            float_left_button.hide();
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    finish();
                    dialog.dismiss();
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
