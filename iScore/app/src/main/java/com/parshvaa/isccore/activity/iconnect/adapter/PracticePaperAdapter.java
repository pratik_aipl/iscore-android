package com.parshvaa.isccore.activity.iconnect.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.iconnect.ITestPaper;
import com.parshvaa.isccore.activity.iconnect.activity.IGeneratePaperActivity;
import com.parshvaa.isccore.activity.iconnect.activity.ISetPaperActivity;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ProgressBarAnimation;
import com.parshvaa.isccore.utils.Utils;

import java.util.List;


public class PracticePaperAdapter extends RecyclerView.Adapter<PracticePaperAdapter.MyViewHolder> {

    private static final String TAG = "PracticePaperAdapter";
    private List<ITestPaper> paperTypeList;
    public Context context;
    public int SubjectID;
    public String SubjectName;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_month, tv_marks, tv_date;
        public ImageView img_watch;
        public ProgressBar pbar_accuracy;
        public RelativeLayout rel_img;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_month = view.findViewById(R.id.tv_month);
            tv_date = view.findViewById(R.id.tv_date);
            tv_marks = view.findViewById(R.id.tv_marks);
            img_watch = view.findViewById(R.id.img_watch);

            pbar_accuracy = view.findViewById(R.id.pbar_accuracy);
            rel_img = view.findViewById(R.id.rel_img);
        }
    }


    public PracticePaperAdapter(List<ITestPaper> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;

        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.serach_i_practies_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ITestPaper cons = paperTypeList.get(position);
        holder.tv_subject_name.setText(cons.getSubjectName());
        String startDate = "";
        String endDate = "";
        try {
            startDate = cons.getPaperDate();
            startDate = startDate.substring(0, 10);
            endDate = cons.getPaperDate();
            endDate = endDate.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startDate = Utils.changeDateToMMDDYYYY(startDate);
        holder.tv_month.setText(startDate.substring(3, 6));
        holder.tv_date.setText(startDate.substring(0, 2));
        try {
            if (cons.getIsStudentMarkUploaded().equals("0") || cons.getIsStudentMarkUploaded().equals("")) {
                holder.pbar_accuracy.setVisibility(View.GONE);
                holder.rel_img.setVisibility(View.GONE);
                holder.tv_marks.setText(cons.getTotalMarks() + " Marks");
            } else {
                holder.pbar_accuracy.setMax(Integer.parseInt(cons.getTotalMarks()));
                ProgressBarAnimation anim = new ProgressBarAnimation(holder.pbar_accuracy, 0, Float
                        .parseFloat(cons.getStudentMark()));
                anim.setDuration(1000);
                holder.pbar_accuracy.startAnimation(anim);
                holder.tv_marks.setText(cons.getStudentMark() + "/" + cons.getTotalMarks() + " Marks");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        holder.tv_marks.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cons.getPaperTypeName().equalsIgnoreCase("Ready Paper")) {
                    Intent intent = new Intent(context, IGeneratePaperActivity.class);
                    intent.putExtra("id", cons.getPaperID());
                    intent.putExtra(Constant.TYPE, "OTHER");
                    intent.putExtra(Constant.isOnlyQuestionPaper, cons.getIsOnlyQuestionPaper());
                    intent.putExtra(Constant.AnswerPaperDate, cons.getAnswerPaperDate() != null ? cons.getAnswerPaperDate() : "");
                    context.startActivity(intent);

                } else if (cons.getPaperTypeName().equalsIgnoreCase("Prelim Paper")) {
                    Log.d(TAG, "isOnlyQuestionPaper: "+cons.getIsOnlyQuestionPaper());
                    Intent intent = new Intent(context, IGeneratePaperActivity.class);
                    intent.putExtra("id", cons.getPaperID());
                    intent.putExtra(Constant.TYPE, "OTHER");
                    intent.putExtra(Constant.isOnlyQuestionPaper, cons.getIsOnlyQuestionPaper());
                    intent.putExtra(Constant.AnswerPaperDate, cons.getAnswerPaperDate() != null ? cons.getAnswerPaperDate() : "");
                    context.startActivity(intent);

                } else {
                    Intent intent = new Intent(context, ISetPaperActivity.class);
                    intent.putExtra("id", cons.getPaperID());
                    intent.putExtra(Constant.TYPE, "OTHER");
                    intent.putExtra(Constant.isOnlyQuestionPaper, cons.getIsOnlyQuestionPaper());
                    intent.putExtra(Constant.AnswerPaperDate, cons.getAnswerPaperDate() != null ? cons.getAnswerPaperDate() : "");
                    context.startActivity(intent);

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return paperTypeList.size();
    }
}


