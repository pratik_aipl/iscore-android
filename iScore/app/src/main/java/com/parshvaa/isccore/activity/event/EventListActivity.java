package com.parshvaa.isccore.activity.event;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.ArtiseModel;
import com.parshvaa.isccore.model.CourseModel;
import com.parshvaa.isccore.model.EventModel;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.RobotoButton;
import com.parshvaa.isccore.utils.RobotoTextView;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventListActivity extends BaseActivity implements AsynchTaskListner {

    private static final String TAG = "EventListActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.btn_subscribe)
    RobotoButton btnSubscribe;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.recy_event)
    RecyclerView recyEvent;
    @BindView(R.id.swip_layout)
    SwipeRefreshLayout swipLayout;

    public EventModel eventModel;
    public ArrayList<EventModel> eventList = new ArrayList<>();
    EventListAdpater eventListAdpater;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);
        ButterKnife.bind(this);

        swipLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showProgress(true, false);
                new CallRequest(EventListActivity.this).GetEvents();
                swipLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isNetworkAvailable(EventListActivity.this)) {
            showProgress(true, false);
            new CallRequest(EventListActivity.this).GetEvents();
        }
    }

    @OnClick({R.id.img_back, R.id.btn_subscribe})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_subscribe:

                break;
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case GetEvent:
                    Log.d(TAG, "result: " + result);
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jBoardArray = jObj.getJSONArray("data");

                            if (jBoardArray != null && jBoardArray.length() > 0) {
                                eventList.clear();
                                for (int i = 0; i < jBoardArray.length(); i++) {
                                    JSONObject jBoard = jBoardArray.getJSONObject(i);
                                    eventModel = new EventModel();
                                    eventModel.setEventID(jBoard.getString("EventID"));
                                    eventModel.setTitle(jBoard.getString("Title"));
                                    eventModel.setBannerImage(jBoard.getString("BannerImage"));
                                    eventModel.setStartDate(jBoard.getString("StartDate"));
                                    eventModel.setEndDate(jBoard.getString("EndDate"));

                                    eventModel.setActualPrice(jBoard.getString("ActualPrice"));
                                    eventModel.setPrice(jBoard.getString("Price"));
                                    eventModel.setIsDemo(jBoard.getString("isDemo"));

                                    eventModel.setIsEventDemo(jBoard.getString("isEventDemo"));
                                    eventModel.setIsRegister(jBoard.getString("isRegister"));
                                    eventModel.setIsSubscribe(jBoard.getString("isSubscribe"));

                                    eventModel.setDemoTitle(jBoard.getString("DemoTitle"));
                                    eventModel.setDemoStartDateTime(jBoard.getString("DemoStartDateTime"));
                                    eventModel.setDemoEndDateTime(jBoard.getString("DemoEndDateTime"));
                                    eventModel.setDemoImage(jBoard.getString("DemoImage"));
                                    eventModel.setDemoLink(jBoard.getString("DemoLink"));

                                    eventModel.setWatchOn(jBoard.getString("WatchOn"));
                                    eventModel.setDuration(jBoard.getString("Duration"));
                                    eventModel.setLanguage(jBoard.getString("Language"));
                                    eventModel.setAge(jBoard.getString("Age"));
                                    eventModel.setText(jBoard.getString("Text"));

                                    eventModel.setAbout(jBoard.getString("About"));
                                    eventModel.setTerms(jBoard.getString("Terms"));
                                    eventModel.setArtistName(jBoard.getString("ArtistName"));
                                    eventModel.setInstruction(jBoard.getString("Instruction"));
                                    eventModel.setType(jBoard.getString("Type"));


                                    JSONArray artdArray = jBoard.getJSONArray("Artists");
                                    ArrayList<ArtiseModel> artiselist = new ArrayList<>();
                                    for (int j = 0; j < artdArray.length(); j++) {
                                        JSONObject art = artdArray.getJSONObject(j);
                                        ArtiseModel artiseModel = new ArtiseModel();
                                        artiseModel.setArtistID(art.getString("ArtistID"));
                                        artiseModel.setArtistName(art.getString("ArtistName"));
                                        artiseModel.setImage(art.getString("Image"));
                                        artiselist.add(artiseModel);
                                    }

                                    JSONArray courseArray = jBoard.getJSONArray("Series");
                                    ArrayList<CourseModel> courseList = new ArrayList<>();
                                    for (int k = 0; k < courseArray.length(); k++) {
                                        JSONObject course = courseArray.getJSONObject(k);
                                        CourseModel courseModel = new CourseModel();
                                        courseModel.setEventSeriesID(course.getString("EventSeriesID"));
                                        courseModel.setEventID(course.getString("EventID"));
                                        courseModel.setTitle(course.getString("Title"));
                                        courseModel.setStartDateTime(course.getString("StartDateTime"));
                                        courseModel.setEndDateTime(course.getString("EndDateTime"));
                                        courseModel.setDuration(course.getString("Duration"));
                                        courseModel.setLink(course.getString("Link"));
                                        courseModel.setPrice(course.getString("Price"));
                                        courseModel.setClassHolderShare(course.getString("ClassHolderShare"));
                                        courseList.add(courseModel);
                                    }

                                    eventModel.setArtists(artiselist);
                                    eventModel.setCourseDetails(courseList);
                                    eventList.add(eventModel);
                                }
                                setupRecyclerView(eventList);

                            } else {
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    private void setupRecyclerView(ArrayList<EventModel> eventList) {
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_from_right);

        recyEvent.setLayoutAnimation(controller);
        recyEvent.scheduleLayoutAnimation();
        recyEvent.setLayoutManager(new LinearLayoutManager(this));
        eventListAdpater = new EventListAdpater(EventListActivity.this, eventList);
        recyEvent.setAdapter(eventListAdpater);
    }
}
