package com.parshvaa.isccore.activity.iconnect;

import java.io.Serializable;

public class LaturesDataModel implements Serializable {

    public String ClassLessonPlannerID ;
    public String ClassID ;
    public String Title ;
    public String ClassName ;
    public String IsLive ;
    public String LiveClassID ;
    public String TeacherID ;
    public String SubjectID ;
    public String BranchID ;
    public String BatchID ;
    public String Duration ;
    public String Status ;
    public String PaperDate ;
    public String CreatedBy ;
    public String CreatedOn ;
    public String ModifiedBy ;
    public String ModifiedOn ;
    public String TeacherName ;
    public String Time ;
    public String SubjectName ;
    public String BoardName ;
    public String MediumName ;
    public String StandardName ;
    public String endTime ;
    public String startTime ;
    public String JoinURL ;

    public String getJoinURL() {
        return JoinURL;
    }

    public void setJoinURL(String joinURL) {
        JoinURL = joinURL;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getClassLessonPlannerID() {
        return ClassLessonPlannerID;
    }

    public void setClassLessonPlannerID(String classLessonPlannerID) {
        ClassLessonPlannerID = classLessonPlannerID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getIsLive() {
        return IsLive;
    }

    public void setIsLive(String isLive) {
        IsLive = isLive;
    }

    public String getLiveClassID() {
        return LiveClassID;
    }

    public void setLiveClassID(String liveClassID) {
        LiveClassID = liveClassID;
    }

    public String getTeacherID() {
        return TeacherID;
    }

    public void setTeacherID(String teacherID) {
        TeacherID = teacherID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getBranchID() {
        return BranchID;
    }

    public void setBranchID(String branchID) {
        BranchID = branchID;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPaperDate() {
        return PaperDate;
    }

    public void setPaperDate(String paperDate) {
        PaperDate = paperDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }
}
