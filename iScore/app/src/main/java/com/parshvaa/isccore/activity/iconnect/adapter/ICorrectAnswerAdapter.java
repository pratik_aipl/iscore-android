package com.parshvaa.isccore.activity.iconnect.adapter;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.model.EvaluterMcqTestMain;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.ISccoreWebView;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 2/3/2018.
 */
public class ICorrectAnswerAdapter extends RecyclerView.Adapter<ICorrectAnswerAdapter.ViewHolder> {

    private static final int VIEW_TYPE_ITEM = 1;
    private LayoutInflater mInflater;
    private ArrayList<EvaluterMcqTestMain> mItems;

    public static Context mContext;
    public static boolean isSolution = false;

    public ICorrectAnswerAdapter(Context context, ArrayList<EvaluterMcqTestMain> items) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        mContext = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvQueNO, tv_solution,
                tv_question, tv_a, tv_b, tv_c, tv_d, ans_a, ans_b, ans_c, ans_d, tv_level;
        public ISccoreWebView qWbeView, aWebView, bWebView, cWebView, dWebView;
        public LinearLayout lin_a, lin_b, lin_c, lin_d;
        public LinearLayout lin_solution;


        public ViewHolder(View view) {
            super(view);
            tvQueNO = view.findViewById(R.id.tvQueNO);
            tv_question = view.findViewById(R.id.tv_q);
            tv_solution = view.findViewById(R.id.tv_solution);
            lin_solution = view.findViewById(R.id.lin_solution);

            qWbeView = view.findViewById(R.id.qWebView);
            aWebView = view.findViewById(R.id.aWebView);
            bWebView = view.findViewById(R.id.bWebView);
            cWebView = view.findViewById(R.id.cWebView);
            dWebView = view.findViewById(R.id.dWebView);
            lin_a = view.findViewById(R.id.lin_a);
            lin_b = view.findViewById(R.id.lin_b);
            lin_c = view.findViewById(R.id.lin_c);
            lin_d = view.findViewById(R.id.lin_d);
            tv_question = view.findViewById(R.id.tv_q);
            tv_a = view.findViewById(R.id.tv_a);
            tv_b = view.findViewById(R.id.tv_b);
            tv_c = view.findViewById(R.id.tv_c);
            tv_d = view.findViewById(R.id.tv_d);
            ans_a = view.findViewById(R.id.a_ans);
            ans_b = view.findViewById(R.id.b_ans);
            ans_c = view.findViewById(R.id.c_ans);
            ans_d = view.findViewById(R.id.d_ans);
            tv_level = view.findViewById(R.id.tv_level);

            /*for (int i = 0; i < mItems.size(); i++) {
                try {
                    if (mItems.get(i).optionArray.get(0).getOptions().contains("<img") || mItems.get(i).optionArray.get(0).getOptions().contains("<math") ||
                            mItems.get(i).optionArray.get(0).getOptions().contains("<table")) {
                        aWebView.setVisibility(View.VISIBLE);
                        ans_a.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (mItems.get(i).optionArray.get(1).getOptions().contains("<img") || mItems.get(i).optionArray.get(1).getOptions().contains("<math") ||
                            mItems.get(i).optionArray.get(1).getOptions().contains("<table")) {
                        bWebView.setVisibility(View.VISIBLE);
                        ans_b.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (mItems.get(i).optionArray.get(2).getOptions().contains("<img") || mItems.get(i).optionArray.get(2).getOptions().contains("<math") || mItems.get(i).optionArray.get(2).getOptions().contains("<table")) {
                        cWebView.setVisibility(View.VISIBLE);
                        ans_c.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (mItems.get(i).optionArray.get(3).getOptions().contains("<img") || mItems.get(i).optionArray.get(3).getOptions().contains("<math") || mItems.get(i).optionArray.get(3).getOptions().contains("<table")) {
                        dWebView.setVisibility(View.VISIBLE);
                        ans_d.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/


        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_correct_question_raw, parent, false));

    }

    @Override
    public void onBindViewHolder(final ViewHolder itemHolder,
                                 int position) {
        itemHolder.tv_solution.setVisibility(View.GONE);
        itemHolder.tvQueNO.setText((position + 1) + ".");
        if (mItems.get(position).getQuestion().contains("<img") || mItems.get(position).getQuestion().contains("<math") || mItems.get(position).getQuestion().contains("<table")) {
            itemHolder.qWbeView.setVisibility(View.VISIBLE);
            itemHolder.tv_question.setVisibility(View.GONE);
            itemHolder.qWbeView.loadHtmlFromLocal(mItems.get(position).getQuestion());
        } else {
            itemHolder.tv_question.setText(Html.fromHtml(mItems.get(position).getQuestion().trim()));

        }
        itemHolder.tv_level.setText("(Level " + mItems.get(position).getLevel() + ")");
        itemHolder.tv_a.setBackgroundResource(0);
        itemHolder.tv_b.setBackgroundResource(0);
        itemHolder.tv_c.setBackgroundResource(0);
        itemHolder.tv_d.setBackgroundResource(0);
        isSolution = false;

        try {
            if (mItems.get(position).optionArray.size() >= 1 && !mItems.get(position).optionArray.get(0).getOptions().isEmpty() && !mItems.get(position).optionArray.get(0).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_a.setVisibility(View.VISIBLE);
                if (mItems.get(position).optionArray.get(0).getOptions().contains("<img") || mItems.get(position).optionArray.get(0).getOptions().contains("<math") ||
                        mItems.get(position).optionArray.get(0).getOptions().contains("<table")) {
                    itemHolder.aWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_a.setVisibility(View.GONE);
                    itemHolder.aWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(0).getOptions());

                } else {
                    itemHolder.ans_a.setText(Html.fromHtml(mItems.get(position).optionArray.get(0).getOptions()).toString().trim());

                }

                if (mItems.get(position).optionArray.get(0).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {
                    if (mItems.get(position).optionArray.get(0).isRightAns == 1) {
                        itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_right);

                    } else {
                        itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_wrong);

                    }
                }
                if (mItems.get(position).optionArray.get(0).getIsCorrect().equals("1")) {
                    itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_right);
                }
            }else{
                itemHolder.lin_a.setVisibility(View.GONE);
            }


            if (mItems.get(position).optionArray.size() >= 2 && !mItems.get(position).optionArray.get(1).getOptions().isEmpty() && !mItems.get(position).optionArray.get(1).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_b.setVisibility(View.VISIBLE);

                if (mItems.get(position).optionArray.get(1).getOptions().contains("<img") || mItems.get(position).optionArray.get(1).getOptions().contains("<math") ||
                        mItems.get(position).optionArray.get(1).getOptions().contains("<table")) {
                    itemHolder.bWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_b.setVisibility(View.GONE);
                    itemHolder.bWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(1).getOptions());
                } else {
                    itemHolder.ans_b.setText(Html.fromHtml(mItems.get(position).optionArray.get(1).getOptions()).toString().trim());

                }

                if (mItems.get(position).optionArray.get(1).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {

                    if (mItems.get(position).optionArray.get(1).isRightAns == 1) {
                        itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_right);
                    } else {
                        itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_wrong);
                    }
                }
                if (mItems.get(position).optionArray.get(1).getIsCorrect().equalsIgnoreCase("1")) {
                    itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_right);
                }
            }else{
                itemHolder.lin_b.setVisibility(View.GONE);
            }


            if (mItems.get(position).optionArray.size() >= 3 && !mItems.get(position).optionArray.get(2).getOptions().isEmpty() && !mItems.get(position).optionArray.get(2).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_c.setVisibility(View.VISIBLE);

                if (mItems.get(position).optionArray.get(2).getOptions().contains("<img") || mItems.get(position).optionArray.get(2).getOptions().contains("<math") ||
                        mItems.get(position).optionArray.get(2).getOptions().contains("<table")) {
                    itemHolder.cWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_c.setVisibility(View.GONE);
                    itemHolder.cWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(2).getOptions());
                } else {
                    itemHolder.ans_c.setText(Html.fromHtml(mItems.get(position).optionArray.get(2).getOptions()).toString().trim());

                }




                if (mItems.get(position).optionArray.get(2).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {

                    if (mItems.get(position).optionArray.get(2).isRightAns == 1) {
                        itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_right);
                    } else {
                        itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_wrong);
                    }
                }
                if (mItems.get(position).optionArray.get(2).getIsCorrect().equalsIgnoreCase("1")) {
                    itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_right);
                }
            }else{
                itemHolder.lin_c.setVisibility(View.GONE);
            }


            if (mItems.get(position).optionArray.size() >= 4 && !mItems.get(position).optionArray.get(3).getOptions().isEmpty() && !mItems.get(position).optionArray.get(3).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_d.setVisibility(View.VISIBLE);

                if (mItems.get(position).optionArray.get(3).getOptions().contains("<img") || mItems.get(position).optionArray.get(3).getOptions().contains("<math") ||
                        mItems.get(position).optionArray.get(3).getOptions().contains("<table")) {
                    itemHolder.dWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_d.setVisibility(View.GONE);
                    itemHolder.dWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(3).getOptions());
                } else {
                    itemHolder.ans_d.setText(Html.fromHtml(mItems.get(position).optionArray.get(3).getOptions()).toString().trim());

                }
                if (mItems.get(position).optionArray.get(3).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {

                    if (mItems.get(position).optionArray.get(3).isRightAns == 1) {
                        itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_right);
                    } else {
                        itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_wrong);
                    }
                }
                if (mItems.get(position).optionArray.get(3).isCorrect.equalsIgnoreCase("1")) {
                    itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_right);
                }

            }else{
                itemHolder.lin_d.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        itemHolder.tv_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!isSolution) {
                    isSolution = true;

                    itemHolder.lin_solution.setVisibility(View.VISIBLE);
                    itemHolder.tv_solution.setText("    Seen    ");
                    itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
                    itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_button));


                } else {
                    isSolution = false;

                    itemHolder.lin_solution.setVisibility(View.GONE);
                    itemHolder.tv_solution.setText("  Solution  ");
                    itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_line));

                }
                //notifyDataSetChanged();
            }
        });
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }


}

