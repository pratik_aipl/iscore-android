package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/10/2018.
 */

public class McqData implements Serializable {
    public SubjectAccuracy subObj;
    public String SelctedChapID = "";
    public String level = "";
    public boolean timer = false;

    public boolean isTimer() {
        return timer;
    }

    public void setTimer(boolean timer) {
        this.timer = timer;
    }

    public String getSelctedChapID() {
        return SelctedChapID;
    }

    public void setSelctedChapID(String selctedChapID) {
        SelctedChapID = selctedChapID;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getQueAppear() {
        return QueAppear;
    }

    public void setQueAppear(String queAppear) {
        QueAppear = queAppear;
    }

    public String QueAppear = "";


}
