package com.parshvaa.isccore.activity.referal;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Utils;

public class ReferalActivity extends BaseActivity {

    public ReferalActivity instance;
    public Button btn_summary, btn_wts;
    public ImageView img_back, img_share, img_invite;
    public TextView tv_student_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referal);
        instance = this;
        img_back = findViewById(R.id.img_back);
        img_share = findViewById(R.id.img_share);
        img_invite = findViewById(R.id.img_invite);
        tv_student_code = findViewById(R.id.tv_student_code);
        btn_summary = findViewById(R.id.btn_summary);
        btn_wts = findViewById(R.id.btn_wts);
        btn_summary.setOnClickListener(view -> startActivity(new Intent(instance, HowItWorkActivity.class)));
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReferFriend();

            }
        });
        tv_student_code.setText(MainUser.getStudentCode());
        btn_wts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatsUp();
            }
        });
        img_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                alertHowItWorks();
            }
        });
    }

    private void alertHowItWorks() {
        final Dialog dialog = new Dialog(ReferalActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_how_it_works);
        TextView tv_close = dialog.findViewById(R.id.tv_close);
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public void ReferFriend() {
        //"market://details?id=" + getPackageName()

        Bundle bundle = new Bundle();
        mFirebaseAnalytics.logEvent("SHARE", bundle);

        Uri uri = Uri.parse("http://bit.ly/iScore_App");

        String msg = getString(R.string.share_msg, MainUser.getStudentCode());

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "iSocre");
        i.putExtra(Intent.EXTRA_TEXT, msg);
        startActivity(Intent.createChooser(i, "Share via"));
    }

    public void whatsUp() {
        Uri uri = Uri.parse("http://bit.ly/iScore_App");

        Bundle bundle = new Bundle();
        mFirebaseAnalytics.logEvent("SHARE", bundle);


        String msg = getString(R.string.share_msg, MainUser.getStudentCode());

        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, msg );
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Utils.showToast("Whatsapp have not been installed.", instance);
        }
    }

}
