package com.parshvaa.isccore.activity.iconnect.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.iconnect.AddRemoveProductImage;
import com.parshvaa.isccore.activity.iconnect.adapter.ProductImageAdapter;
import com.parshvaa.isccore.model.ImageDataModel;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.AsyncTaskListner;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.RobotoBoldButton;
import com.parshvaa.isccore.utils.RobotoTextView;
import com.parshvaa.isccore.utils.Utils;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;

public class UploadPaperImagesActivity extends BaseActivity implements AddRemoveProductImage,AsyncTaskListner {

    private static final String TAG = "UploadPaperImagesActivi";
    List<ImageDataModel> imageList = new ArrayList<>();
    File image;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.img_home)
    ImageView imgHome;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;
    @BindView(R.id.li_imagelist)
    LinearLayout liImagelist;
    ProductImageAdapter productImageAdapter;
    private static final int REQUEST_CODE = 732;
    @BindView(R.id.btn_submit)
    RobotoBoldButton btnSubmit;
    private ArrayList<String> mResults = new ArrayList<>();
    ArrayList<String> imgremove = new ArrayList<>();

    int selectPos = -1;
    String PAPERID,TYPE="generate";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_paper_images);
        ButterKnife.bind(this);

        imgHome.setImageResource(R.drawable.right_green);
        imgHome.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        ImageViewCompat.setImageTintList(imgHome, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));

        tvTitle.setText("Upload Images");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        PAPERID = getIntent().getExtras().getString(Constant.PAPERID);
        TYPE = getIntent().getExtras().getString(Constant.TYPE);
        Log.d(TAG, "TYPE: "+TYPE);

        for (int i = 0; i < 1; i++) {
            imageList.add(new ImageDataModel("", "0", null));
        }

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new GridLayoutManager(this, 3));
        productImageAdapter = new ProductImageAdapter(UploadPaperImagesActivity.this, imageList);
        recyclerlist.setAdapter(productImageAdapter);

        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           /*     List<MultipartBody.Part> parts = new ArrayList<>();
                for (int i = 0; i < imageList.size(); i++) {
                    if (imageList.get(i).getFile() != null && !TextUtils.isEmpty(imageList.get(i).getImagePath())) {
                        RequestBody requestFile = null;
                        try {
                            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new Compressor(this).compressToFile(imageList.get(i).getFile()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "uploadStepDataSIZE: " + imageList.size());
                        Log.d(TAG, "uploadStepDataFILENAME: " + imageList.get(i).getFile().getName());
                        parts.add(MultipartBody.Part.createFormData("ProductImage[]", imageList.get(i).getFile().getName(), requestFile));
                    }
                }*/
                if(imageList.size()>1){
                  //  showProgress(true, false);
                    if(TYPE.equalsIgnoreCase("UPLOAD")){
                        new CallRequest(UploadPaperImagesActivity.this)
                                .SubmitUploadPaperImages(imageList,PAPERID);
                    }else{
                        new CallRequest(UploadPaperImagesActivity.this)
                                .UploadPaperImages(imageList,PAPERID);
                    }

                }else {
                    Toast.makeText(UploadPaperImagesActivity.this, "Please Select Images", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
         //   showProgress(false, false);
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case UploadPaperImages:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                Utils.showToast(object.getString("message"), this);
                                onBackPressed();
                            } else {
                                Utils.showToast(object.getString("message"), this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   /* @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            showProgress(false, true);
            try {
                switch (request) {
                    case UploadPaperImages:
                        Log.d(TAG, "UploadPaperImages>>: " + result);
                        JSONObject jObj = new JSONObject(result);
                      *//*  if (jObj.getBoolean("status")) {
                        }*//*
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }*/

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onAddImage(int pos) {
        selectPos = pos;
        if (imageList.size() <= 20) {
            //selectImage();
            // start multiple photos selector
            Intent intent = new Intent(UploadPaperImagesActivity.this, ImagesSelectorActivity.class);
            // max number of images to be selected
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 20);
            // min size of image which will be shown; to filter tiny images (mainly icons)
            //  intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 10000);
            // show camera or not
            intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
            // pass current selected images as the initial value
            intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
            // start the selector
            startActivityForResult(intent, REQUEST_CODE);
        } else {
            Toast.makeText(this, "Add Maximum 20 images", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRemoveImage(int position, String id) {
        for (int i = 0; i < mResults.size(); i++) {
            if (imageList.get(position).getImagePath().equalsIgnoreCase(mResults.get(i))) {
                mResults.remove(i);
                break;
            }
        }
        Log.d(TAG, "onRemoveImage: " + position + "  >>>  " + imageList.get(position).getImagePath());
        if (!imageList.get(position).getImagePath().equalsIgnoreCase("")) {
            imageList.remove(position);
            imgremove.add(id);
        }
        productImageAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // get selected images from selector
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                Log.d(TAG, "onActivityResult: " + mResults.size());
                assert mResults != null;
                if (mResults.size() <= 20) {
                    // show results in textview
                    //StringBuilder sb = new StringBuilder();
                    // sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                    imageList.clear();
                    for (int i = 0; i < 1; i++) {
                        imageList.add(new ImageDataModel("", "0", null));
                    }

                    for (String result : mResults) {
                        //sb.append(result).append("\n");
                        Log.d(TAG, "onActivityResult:path " + result + " isexist " + new File(result).exists());
                        try {
                            imageList.add(new ImageDataModel(result, "0", new Compressor(this).setQuality(90).compressToFile(new File(result))));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    productImageAdapter.notifyDataSetChanged();
                   /* if(imageList.size()>1){
                        btnSubmit.setVisibility(View.VISIBLE);
                    }else{
                        btnSubmit.setVisibility(View.GONE);
                    }*/
                    //  tvResults.setText(sb.toString());
                } else {
                    Toast.makeText(this, "Please Select 20 Images ", Toast.LENGTH_SHORT).show();
                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
