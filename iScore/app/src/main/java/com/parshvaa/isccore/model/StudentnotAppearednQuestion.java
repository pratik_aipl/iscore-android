package com.parshvaa.isccore.model;

import java.io.Serializable;

public class StudentnotAppearednQuestion implements Serializable {
    public String StudentID="";
    public String  CreatedOn="";

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String QuestionID="";
}