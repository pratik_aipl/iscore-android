package com.parshvaa.isccore.activity.revisionnote.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.revisionnote.fragment.PreviewRevisionNoteFragment;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.ClassQuestionPaperSubQuestionRevision;
import com.parshvaa.isccore.tempmodel.SubQueKey;
import com.parshvaa.isccore.tempmodel.SubQueLabel;
import com.parshvaa.isccore.tempmodel.TempQuestionTypes;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.RomanNumber;
import com.parshvaa.isccore.utils.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PreviewRevisionNoteActivity extends BaseActivity {

    public String ChapterID = "", chapterNumber = "", QuestionTypeIDs = "";
    public Button btn_preview_paper;
    public static ViewPager viewPager;
    public MyViewPagerAdapter myViewPagerAdapter;
    public FloatingActionButton float_right_button, float_left_button;
    public TextView tv_pager_count;
    public ImageView img_back, img_right, img_left, img_home;
    public TextView tv_title;
    public ArrayList<TempQuestionTypes> tempQuestionTypesArray;
    public HashMap<String, ArrayList<TempQuestionTypes>> stringArrayListHashMap = new HashMap<>();
    ArrayList<TempQuestionTypes> values = null;

    public SubQueLabel subQuesLabelObj;
    public SubQueKey subQueskeyObj;

    public ArrayList<SubQueLabel> subQuesLabelArray = new ArrayList<>();
    private MyDBManager mDb;
    public float mark = 0;
    public String MPSQID = "0";
    String msg = "";
    public ClassQuestionPaperSubQuestionRevision subQuesObj;
    public ArrayList<ClassQuestionPaperSubQuestionRevision> subQuesArray = new ArrayList<>();
    public CursorParserUniversal cParse;
    public String HTML = "", SubQuestionTypeID = "";
    public int subQue_count = 1, label_count = 1, subQue_type_count = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_revision_paper_solution);
        Utils.logUser();
        this.cParse = new CursorParserUniversal();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        btn_preview_paper = findViewById(R.id.btn_preview_paper);
        ChapterID = getIntent().getExtras().getString("ChapterIds");
        chapterNumber = getIntent().getExtras().getString("ChapterNumber");
        QuestionTypeIDs = getIntent().getExtras().getString("selectedQuesTypeIDs");
        viewPager = findViewById(R.id.pager);
        tv_pager_count = findViewById(R.id.tv_pager_count);
        img_back = findViewById(R.id.img_back);
        img_right = findViewById(R.id.img_right);
        img_left = findViewById(R.id.img_left);
        tv_title = findViewById(R.id.tv_title);
        img_home = findViewById(R.id.img_home);

        float_right_button = findViewById(R.id.float_right_button);
        float_left_button = findViewById(R.id.float_left_button);
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });

        List<String> items = Arrays.asList(QuestionTypeIDs.split("\\s*,\\s*"));
        stringArrayListHashMap.clear();
        for (int k = 0; k < items.size(); k++) {
            try {
                tempQuestionTypesArray = (ArrayList) new CustomDatabaseQuery(this, new TempQuestionTypes())
                        .execute(new String[]{DBNewQuery.getQuestions(items.get(k), ChapterID)}).get();

                outerloop:
                for (int j = 0; j < tempQuestionTypesArray.size(); j++) {

                    if (tempQuestionTypesArray.get(j).getIsPassage().equals("1")) {
                        HTML = "";
                        HTML = HTML +
                                "<table >" +
                                "   <tr class=\"margin_top\">\n" +
                                "       <td width=\"30\"></td>\n" +
                                "       <td width=\"30\" style=\"vertical-align: top;\"><b></b></td>" +
                                "       <td colspan=\"2\" class=\"question\">" +
                                "           <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                                "               <tbody>" +
                                "                    <tr>" +
                                "                        <td style=\"text-align:justify\">" +
                                tempQuestionTypesArray.get(j).getQuestion() +
                                "                        </td>" +
                                "                    </tr>" +
                                "               </tbody>" +
                                "           </table>" +
                                "       </td>" +
                                "   </tr>";




                               /* "<table>" +
                                    "<tr class=\"margin_top\">\n" +
                                    "<td width=\"30\"></td>\n" +
                                "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                "<td width=\"660\">" +
                                tempQuestionTypesArray.get(j).getQuestion() +
                                "</td>\n" +
                                "<td align=\"right\" width=\"30\"></td>\n" +
                                "</tr>     \n";*/
                        subQuesLabelArray.clear();
                        Cursor clabel = mDb.getAllRows(DBQueries.getLabel(tempQuestionTypesArray.get(j).getMQuestionID()));
                        //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                        if (clabel != null && clabel.moveToFirst()) {

                            do {
                                label_count = 1;
                                subQue_type_count = 1;
                                subQuesLabelObj = new SubQueLabel();
                                subQuesLabelObj.setLabel(clabel.getString(clabel.getColumnIndexOrThrow("Label")));

                                mark = clabel.getInt(clabel.getColumnIndexOrThrow("Mark"));

                                subQueskeyObj = new SubQueKey();

                                SubQuestionTypeID = "";
                                Cursor cQue = mDb.getAllRows(DBQueries.getPassageSubquetionsRivistion(MPSQID,
                                        tempQuestionTypesArray.get(j).getMQuestionID(),
                                        tempQuestionTypesArray.get(j).getQuestionTypeID(), String.valueOf(mark), clabel.getString(clabel.getColumnIndexOrThrow("PassageSubQuestionTypeID"))));
                                //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                                if (cQue != null && cQue.moveToFirst()) {
                                    subQue_count = 1;

                                    do {
                                        if (!SubQuestionTypeID.equals(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestionTypeID")))) {

                                            HTML = HTML +
                                                    "<tr class=\"margin_top\">" +
                                                    "   <td width=\"30\"></td>" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"><b>";
                                            if (label_count == 1) {
                                                HTML = HTML + subQuesLabelObj.getLabel() + ")</b>" +
                                                        "</td>";
                                                label_count++;
                                            }

                                            HTML = HTML +
                                                    "   <td width=\"660\">" +
                                                    "       <table width=\"100%\">" +
                                                    "           <tr>" +
                                                    "               <td width=\"630\">";




                                          /*  HTML = HTML + " <tr class=\"margin_top\">\n" +
                                                    "<td width=\"30\"></td>\n" +
                                                    "<td width=\"30\" style=\"vertical-align: top;\"><b>";

                                            HTML = HTML + "  <td width=\"660\">\n" +
                                                    "<table width=\"100%\">\n" +
                                                    "   <tr>\n" +
                                                    "       <td width=\"30\">\n" +
                                                    "           <b>";

                                            if (label_count == 1) {
                                                HTML = HTML + subQuesLabelObj.getLabel() + ")";
                                                label_count++;
                                            }

                                            HTML = HTML + "           </b>\n" +
                                                    "       </td>\n" +
                                                    "       <td width=\"630\">\n";*/

                                            HTML = HTML + RomanNumber.toRoman(subQue_type_count) + ") " + cQue.getString(cQue.getColumnIndexOrThrow("SubQuestionType"));
                                            HTML = HTML + "         </td>\n";
                                            double marks = Float.parseFloat(cQue.getString(cQue.getColumnIndexOrThrow("Marks")));

                                            DecimalFormat format = new DecimalFormat();
                                            format.setDecimalSeparatorAlwaysShown(false);
/*
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + format.format(marks) + "</td>" +
                                                    "   </tr>\n" +
                                                    "</table>\n" +
                                                    "</td>";
                                            HTML = HTML + "</tr>" +
                                                    " <tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";


                                            HTML = HTML + subQue_count + ") " + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestion")));


                                            HTML = HTML + "   </td>\n" +
                                                    "   <td align=\"right\" width=\"30\"></td>\n" +
                                                    "</tr>      ";*/
                                            HTML = HTML + "        <td align=\"right\" width=\"30\">" + format.format(marks) + "</td>" +
                                                    "          </tr>\n" +
                                                    "       </table>\n" +
                                                    "  </td>" +
                                                    "</tr>" +
                                                    "<tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 3%;\" class=\"sub\">\n" +
                                                    "       <table width=\"100%\">" +
                                                    "          <tr>" +
                                                    "               <td width=\"30\" style=\"vertical-align: top\">" + subQue_count + ")  </td>" +
                                                    "               <td width=\"630\">";
                                            HTML = HTML + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestion")));
                                            HTML = HTML + "         </td>\n" +
                                                    "          </tr>" +
                                                    "       </table>" +
                                                    "    </td>" +
                                                    "   <td align=\"right\" width=\"30\"></td>" +
                                                    "</tr>";
                                            SubQuestionTypeID = cQue.getString(cQue.getColumnIndexOrThrow("SubQuestionTypeID"));
                                            subQue_count++;
                                            subQue_type_count++;
                                        } else {
                                            HTML = HTML +
                                                    "<tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 3%;\" class=\"sub\">\n" +
                                                    "       <table width=\"100%\">" +
                                                    "          <tr>" +
                                                    "               <td width=\"30\" style=\"vertical-align: top\">" + subQue_count + ")  </td>" +
                                                    "               <td width=\"630\">";
                                            HTML = HTML + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestion")));
                                            HTML = HTML + "         </td>\n" +
                                                    "          </tr>" +
                                                    "       </table>" +
                                                    "    </td>" +
                                                    "   <td align=\"right\" width=\"30\"></td>" +
                                                    "</tr>";

                                      /*


                                            HTML = HTML + "</tr>" +
                                                    " <tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";

                                            HTML = HTML + subQue_count + ") " + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestion")));

                                            HTML = HTML + "   </td>\n" +
                                                    "   <td align=\"right\" width=\"30\"></td>\n" +
                                                    "</tr>      ";*/
                                            subQue_count++;
                                        }
                                    } while (cQue.moveToNext());
                                    cQue.close();
                                }


                            } while (clabel.moveToNext());
                            clabel.close();


                        }
                        tempQuestionTypesArray.get(j).setQuestion(HTML + "</table>");
                        if (j == 0) {
                            longInfo(HTML + "</table>", "HEader");
                        }
                    }


                }
                stringArrayListHashMap.put(items.get(k), tempQuestionTypesArray);


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

        tv_pager_count.setText("1/" + stringArrayListHashMap.size());
        myViewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager(), stringArrayListHashMap.size(), values);
        viewPager.setAdapter(myViewPagerAdapter);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        myViewPagerAdapter.notifyDataSetChanged();
        tv_pager_count.setText(viewPager.getCurrentItem() + 1 + "/" + stringArrayListHashMap.size());
        tv_title.setText(" Revision");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (stringArrayListHashMap.size() == 1) {
            float_left_button.hide();
            float_right_button.hide();
        } else {
            float_left_button.hide();
        }
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageSelected(int position) {
                if ((stringArrayListHashMap.size() - 1) == position) {

                    float_right_button.hide();
                } else {
                    float_right_button.show();
                }
                if (position == 0) {
                    float_left_button.hide();
                } else {
                    float_left_button.show();
                }
                if (stringArrayListHashMap.size() == 1) {
                    float_left_button.hide();
                    float_right_button.hide();
                }
                tv_pager_count.setText(position + 1 + "/" + stringArrayListHashMap.size());
            }
        });
        float_left_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        float_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < stringArrayListHashMap.size()) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });
        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < stringArrayListHashMap.size()) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });

        btn_preview_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PreviewRevisionNoteActivity.this, PreviewPdfActivity.class)
                        .putExtra("ChapterIds", ChapterID)
                        .putExtra("ChapterNumber", chapterNumber)
                        .putExtra("selectedQuesTypeIDs", QuestionTypeIDs)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });
    }
    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState) {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }
    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }

    public void gotoDashboard() {
        new AlertDialog.Builder(PreviewRevisionNoteActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)
                .setMessage("Sure about visiting the dashboard?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    startActivity(new Intent(PreviewRevisionNoteActivity.this, DashBoardActivity.class));
                    finish();
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }

    public class MyViewPagerAdapter extends FragmentPagerAdapter {
        int mNumOfTabs;
        List<TempQuestionTypes> arrayList;

        public MyViewPagerAdapter(FragmentManager fm, int i, List<TempQuestionTypes> questionTypesArrayList) {
            super(fm);
            this.mNumOfTabs = i;
            this.arrayList = questionTypesArrayList;
        }

        @Override
        public Fragment getItem(int position) {
            return new PreviewRevisionNoteFragment().newInstance(position, arrayList, stringArrayListHashMap);
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return String.valueOf(position + 1);
        }
    }


}
