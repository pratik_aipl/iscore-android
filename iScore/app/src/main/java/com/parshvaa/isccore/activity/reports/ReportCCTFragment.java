package com.parshvaa.isccore.activity.reports;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;


import com.parshvaa.isccore.activity.reports.adapter.SubjectCCTAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;

/**
 * Created by Karan - Empiere on 11/3/2017.
 */

public class ReportCCTFragment extends Fragment {
    public App app;
    public GridView gridView;
    public CursorParserUniversal cParse;
    public MyDBManager mDb;
    public SubjectCCTAdapter adapter;
    public View v;
    public TextView tv_totalTest, tv_cct_accuracy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_cct, container, false);
        cParse = new CursorParserUniversal();
        app = App.getInstance();
        mDb = MyDBManager.getInstance(getActivity());
        mDb.open(getActivity());
        tv_totalTest = v.findViewById(R.id.tv_total_test);
        tv_cct_accuracy = v.findViewById(R.id.tv_cct_accuracy);
        tv_cct_accuracy.setText(App.cctTotalAccuracy + "% Accuracy");
        tv_totalTest.setText(App.cctTotal + "");
        gridView = v.findViewById(R.id.grid_view);

        if (App.cctArray != null)
            adapter = new SubjectCCTAdapter(ReportCCTFragment.this, App.cctArray);
        if (adapter != null) {
            gridView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        return v;
    }
}
