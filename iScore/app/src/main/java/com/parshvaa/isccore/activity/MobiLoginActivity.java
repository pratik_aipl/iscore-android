package com.parshvaa.isccore.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.androidquery.AQuery;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBTables;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.DBfile;
import com.parshvaa.isccore.model.KeyData;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class MobiLoginActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "MobiLoginActivity";
    public MobiLoginActivity instance;
    public Button btn_login;
    public EditText etMobileNo;
    public DBfile db;
    public String data;
    public MyDBManager mDb;
    public App app;
    public String DeviceID = "";
    public String version = "", OTP = "", PleyaerID = "", Guid;
    public JsonParserUniversal jParser;
    AQuery aQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_mobi_login);
        Utils.logUser();
        aQuery = new AQuery(this);
        instance = this;
        jParser = new JsonParserUniversal();
        PleyaerID = Utils.OneSignalPlearID();
        etMobileNo = findViewById(R.id.et_mobile);
        mySharedPref = new MySharedPref(MobiLoginActivity.this);
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        app = App.getInstance();
        int verCode = Utils.currentAppVersionCode(MobiLoginActivity.this);
        version = String.valueOf(verCode);

        mySharedPref.setDeviceID(DeviceID);
        if (!mySharedPref.getGuuid().isEmpty()) {
            Guid = mySharedPref.getGuuid();
        } else {
            Guid = UUID.randomUUID().toString();

        }
        PleyaerID = Utils.OneSignalPlearID();
        Log.d(TAG, "login: "+PleyaerID);

        if (BuildConfig.DEBUG) {
            etMobileNo.setText("8200747549");
            etMobileNo.setText("7710004570");
        }
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(v -> {
            btn_login.setEnabled(false);

            if (etMobileNo.getText().toString().equals("")) {
                btn_login.setEnabled(true);
                etMobileNo.setError("Enter appropriate mobile number.");
            } else {
                if (Utils.isNetworkAvailable(MobiLoginActivity.this)) {
                    showProgress(true, true);
                    new CallRequest(MobiLoginActivity.this).getLogin(etMobileNo.getText().toString(), Guid, PleyaerID);
                } else {
                    Utils.showToast("Please connect internet to Login", MobiLoginActivity.this);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        btn_login.setEnabled(true);
    }

    public void removeAllTables() {


        mDb.removeTable("boards");
        mDb.removeTable("boards_paper");
        mDb.removeTable("boards_paper_data");
        mDb.removeTable("board_paper_files");
        mDb.removeTable("chapters");
        mDb.removeTable("examtypes");
        mDb.removeTable("examtype_subject");
        mDb.removeTable("masterquestion");
        mDb.removeTable("mcqoption");
        mDb.removeTable("mcqquestion");
        mDb.removeTable("mediums");

        mDb.removeTable("question_types");
        mDb.removeTable("standards");
        mDb.removeTable("mcqquestion");
        mDb.removeTable("subjects");


        mDb.removeTable("board_paper_download");
        mDb.removeTable("student_set_paper_question_type");
        mDb.removeTable("student_set_paper_detail");
        mDb.removeTable("notification");
        mDb.removeTable("class_evaluater_mcq_test_temp");
        mDb.removeTable("class_evaluater_cct_count");
        mDb.removeTable("updated_masterquetion");
        mDb.removeTable("student_mcq_test_level");
        mDb.removeTable("temp_mcq_test_level");
        mDb.removeTable("student_not_appeared_question");
        mDb.removeTable("student_incorrect_question");
        mDb.removeTable("temp_mcq_test_hdr");
        mDb.removeTable("temp_mcq_test_dtl");
        mDb.removeTable("temp_mcq_test_chapter");
        mDb.removeTable("student_mcq_test_hdr");
        mDb.removeTable("student_mcq_test_dtl");
        mDb.removeTable("student_mcq_test_chapter");
        mDb.removeTable("paper_type");
        mDb.removeTable("exam_type_pattern");
        mDb.removeTable("exam_type_pattern_detail");
        mDb.removeTable("student_question_paper");
        mDb.removeTable("student_question_paper_detail");
        mDb.removeTable("student_question_paper_chapter");


        DBTables dbTables = new DBTables();
        dbTables.createAll(mDb);


    }

    public void showAlertback(String msg) {
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(MobiLoginActivity.this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();                    }
                });

        android.app.AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false, true);
        //showAlertback(result);
        if (!result.isEmpty()) {
            try {
                switch (request) {
                    case getLogin:
                        Log.d(TAG, "onTaskCompleted: " + result);
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.deleteAllTableRecord(mDb);
                            JSONObject jData = jObj.getJSONObject("data");
                            MainUser.setStudentId(jData.getString("StudentID"));
                            MainUser.setFirstName(jData.getString("FirstName"));
                            MainUser.setLastName(jData.getString("LastName"));
                            MainUser.setGuid(jData.getString("IMEINo"));
                            MainUser.setEmailID(jData.getString("EmailID"));
                            MainUser.setRegMobNo(jData.getString("RegMobNo"));
                            MainUser.setSchoolName(jData.getString("SchoolName"));
                            MainUser.setProfile_image(jData.getString("ProfileImage"));
                            MainUser.setStudentCode(jData.getString("StudentCode"));

                            mySharedPref.setStudent_id(MainUser.getStudentId());
                            mySharedPref.setFirst_name(MainUser.getFirstName());
                            mySharedPref.setLast_name(MainUser.getLastName());
                            mySharedPref.setGuuid(MainUser.getGuid());
                            mySharedPref.setEmailID(MainUser.getEmailID());
                            mySharedPref.setRegMobNo(MainUser.getRegMobNo());
                            mySharedPref.setStudentCode(MainUser.getStudentCode());
                            mySharedPref.setProfileImage(MainUser.getProfile_image());
                            mySharedPref.setVersioCode(version);
                            OTP = jObj.getString("otp");
                            App.Otp = OTP;
                            KeyData keydata;
                            JSONArray jsonArray = jData.getJSONArray("KeyData");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jKey = jsonArray.getJSONObject(i);
                                keydata = (KeyData) jParser.parseJson(jsonArray.getJSONObject(i), new KeyData());
                                MainUser.keyDataArray.add(keydata);

                                MainUser.setRegKey(MainUser.keyDataArray.get(i).getStudentKey());
                                MainUser.setPreviousYearScore(MainUser.keyDataArray.get(i).getPreviousYearScore());

                                mySharedPref.setStudent_id(MainUser.keyDataArray.get(i).getStudentID());
                                mySharedPref.setBoardID(MainUser.keyDataArray.get(i).getBoardID());
                                mySharedPref.setMediumID(MainUser.keyDataArray.get(i).getMediumID());
                                mySharedPref.setStandardID(MainUser.keyDataArray.get(i).getStandardID());
                                mySharedPref.setClassID(MainUser.keyDataArray.get(i).getClassID());
                                mySharedPref.setBranchID(MainUser.keyDataArray.get(i).getBranchID());
                                mySharedPref.setBatchID(MainUser.keyDataArray.get(i).getBatchID());
                                mySharedPref.setReg_key(MainUser.keyDataArray.get(i).getStudentKey());
                                mySharedPref.setVersion(MainUser.keyDataArray.get(i).getIsDemo());
                                mySharedPref.setStandardName(MainUser.keyDataArray.get(i).getStandardName());
                                mySharedPref.setClassName(MainUser.keyDataArray.get(i).getClassName());
                                mySharedPref.setBoardName(MainUser.keyDataArray.get(i).getBoardName());
                                mySharedPref.setMediumName(MainUser.keyDataArray.get(i).getMediumName());
                                mySharedPref.setClassLogo(MainUser.keyDataArray.get(i).getClassLogo());
                                mySharedPref.setClassLogo2(MainUser.keyDataArray.get(i).getSplashLogo());
                                mySharedPref.setClassRoundLogo(MainUser.keyDataArray.get(i).getClassRoundLogo());
                                mySharedPref.setCreatedOn(MainUser.keyDataArray.get(i).getCreatedOn());
                                mySharedPref.setlblTarget(MainUser.keyDataArray.get(i).getSetTarget());

                                mySharedPref.setlblPrviousTarget(MainUser.keyDataArray.get(i).getPreviousYearScore());

                            }
                            String fileName = MainUser.getProfile_image().substring(MainUser.getProfile_image().lastIndexOf("/") + 1);
                            String ClassLogoName = mySharedPref.getClassLogo().substring(mySharedPref.getClassLogo().lastIndexOf("/") + 1);
                            String fileNameCircle = MainUser.getProfile_image().substring(MainUser.getProfile_image().lastIndexOf("/") + 1);
                            String ClassLogoNameCircle = mySharedPref.getClassRoundLogo().substring(mySharedPref.getClassRoundLogo().lastIndexOf("/") + 1);


                            if (Utils.checkOutDated(mySharedPref.getExpiryDate())) {
                                DBTables dbTables = new DBTables();
                                dbTables.createAll(mDb);
                                goAhead();
                            } else {
                                new AlertDialog.Builder(this)
                                        .setTitle("Expired")
                                        .setMessage("Your iSccore Product has Expired, Kindly contact Parshvaa Publications")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                MobiLoginActivity.this.finish();
                                                dialog.dismiss();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }

                        } else {
                            btn_login.setEnabled(true);
                            showProgress(false, true);
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;
                }
            } catch (JSONException e) {
                btn_login.setEnabled(true);
                showProgress(false, true);
                Utils.showToast("Invalid Login ID or Password", this);
                e.printStackTrace();
            }
        }else{
            Utils.showToast("Something Goes Wrong", this);
        }
    }

    public void goAhead() {
        if (Utils.isNetworkAvailable(MobiLoginActivity.this)) {
            startActivity(new Intent(MobiLoginActivity.this, VerifyOtpActivity.class)
                    .putExtra(Constant.MobLogin, "MobLogin")
                    .putExtra(Constant.isDemo, false)
                    .putExtra(Constant.isKeyRegiste, false));
        } else {
            Utils.showToast("Please connect Internet to Singup", MobiLoginActivity.this);
        }
    }

}
