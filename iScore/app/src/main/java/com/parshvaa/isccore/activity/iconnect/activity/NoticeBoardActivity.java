package com.parshvaa.isccore.activity.iconnect.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.iconnect.adapter.NoticeBoardAdapter;
import com.parshvaa.isccore.activity.iconnect.NoticeBoard;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class NoticeBoardActivity extends BaseActivity implements AsynchTaskListner {
    public App app;
    public JsonParserUniversal jParser;
    public NoticeBoardAdapter adapter;
    public ArrayList<NoticeBoard> noticArray = new ArrayList<>();
//    String pathUrl;

    public RecyclerView recycler_notice;
    public ImageView img_back;
    public TextView tv_title;

    public MyDBManager mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_board);
        Utils.logUser();
        recycler_notice = findViewById(R.id.recycler_notice);
        tv_title = findViewById(R.id.tv_title);
        img_back = findViewById(R.id.img_back);
        app = App.getInstance();

        jParser = new JsonParserUniversal();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        mDb.dbQuery("UPDATE notification SET isTypeOpen = 1,isOpen=1 WHERE ModuleType = 'notice_board' ");
        img_back.setOnClickListener(v -> onBackPressed());
        tv_title.setText("Notice Board");
        showProgress(true, true);
        new CallRequest(this).get_notice_board();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case get_notice_board:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jDataArray = jObj.getJSONArray("data");
//                            pathUrl = jObj.getString("url");
                            if (jDataArray != null && jDataArray.length() > 0) {
                                noticArray.clear();
                                noticArray.addAll(LoganSquare.parseList(jDataArray.toString(), NoticeBoard.class));
                                setupRecyclerView();
                                showProgress(false, true);
                            }
                        } else {
                            showProgress(false, true);
                            showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

            }


        }
    }

    private void setupRecyclerView() {
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        recycler_notice.setLayoutAnimation(controller);
        recycler_notice.scheduleLayoutAnimation();
        recycler_notice.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NoticeBoardAdapter(noticArray, this, "");
        recycler_notice.setAdapter(adapter);
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(NoticeBoardActivity.this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    finish();
                    dialog.dismiss();
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}

