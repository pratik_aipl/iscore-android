package com.parshvaa.isccore.activity.reports;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.reports.adapter.SubjectAccuracyLevelAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.McqReport;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportMCQFragment extends Fragment {
    private static final String TAG = "ReportMCQFragment";
    public GridView gridView;
    public CursorParserUniversal cParse;
    public Subject subjectObj;
    public ArrayList<Subject> subArray = new ArrayList<>();
    public MyDBManager mDb;
    public SubjectAccuracyLevelAdapter adapter;
    public View v;
    public int attempted;
    public TextView tv_totalTest, tv_defLevel1, tv_defLevel2, tv_defLevel3, tv_mcq_accuracy;
    public String testHeaderIDS = "";
    private LinkedHashMap<String, String> array, tempArray;
    public App app;
    public McqReport mcqReport;
    public ArrayList<McqReport> mcqReportArray = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_mcq, container, false);
        subArray.clear();

        cParse = new CursorParserUniversal();
        app = App.getInstance();
        mDb = MyDBManager.getInstance(getActivity());
        mDb.open(getActivity());
        attempted = mDb.getAllRecords("student_mcq_test_hdr");
        tv_totalTest = v.findViewById(R.id.tv_total_test);

        tv_totalTest.setText(attempted + "");

        testHeaderIDS = getTestHeaderID();

        tv_defLevel1 = v.findViewById(R.id.tv_defLevel1);
        tv_defLevel2 = v.findViewById(R.id.tv_defLevel2);
        tv_defLevel3 = v.findViewById(R.id.tv_defLevel3);
        tv_mcq_accuracy = v.findViewById(R.id.tv_mcq_accuracy);

        Cursor levl1 = mDb.getAllRows(DBNewQuery.getLevelviseTestCount("1"));

        int i = levl1.getCount();
        tv_defLevel1.setText(String.valueOf(i));
        levl1.close();

        Cursor levl2 = mDb.getAllRows(DBNewQuery.getLevelviseTestCount("2"));
        int i2 = levl2.getCount();
        tv_defLevel2.setText(String.valueOf(i2));
        levl2.close();

        Cursor levl3 = mDb.getAllRows(DBNewQuery.getLevelviseTestCount("3"));
        int i3 = levl3.getCount();
        tv_defLevel3.setText(String.valueOf(i3));
        levl3.close();
        tv_mcq_accuracy.setText(App.mcqTotalAccuracy + "% Accuracy");
        tempArray = new LinkedHashMap<String, String>();
        mcqReportArray.clear();

        Cursor c = mDb.getAllRows(DBQueries.getTotalAttemptedSubjectsWithName());
        if (c != null && c.moveToFirst()) {
            do {

                mcqReport = new McqReport();
                mcqReport = (McqReport) cParse.parseCursor(c, new McqReport());
                mcqReportArray.add(mcqReport);
               /* tempArray.put(c.getString(c.getColumnIndex("StudentMCQTestHDRID")),
                        c.getString(c.getColumnIndex("SubjectName")));*/
            } while (c.moveToNext());
            c.close();
        } else {
            //   Utils.showToast("Not any Subject Found from : ", getActivity());
        }

        for (McqReport key : mcqReportArray) {
            Utils.Log("TAG", "KEY::->" + key);
            key.setTotalTest(String.valueOf(mDb.getAllReports("select * from student_mcq_test_hdr where SubjectID = " + key.getSubjectID())));
            key.setTotalDFLone(String.valueOf(mDb.getAllReports("select * from student_mcq_test_hdr where SubjectID = " + key.getSubjectID() + " AND LevelID = 1")));
            key.setTotalDFLtwo(String.valueOf(mDb.getAllReports("select * from student_mcq_test_hdr where SubjectID = " + key.getSubjectID() + " AND LevelID = 2")));
            key.setTotalDFLthree(String.valueOf(mDb.getAllReports("select * from student_mcq_test_hdr where SubjectID = " + key.getSubjectID() + " AND LevelID = 3")));

            int tempTotalAttempted = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(getTestHeaderID(key.getSubjectID())));
            int tempTotalRight = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(getTestHeaderID(key.getSubjectID())));
            key.setTotalAccuraacy(Utils.getAccuraccy(tempTotalAttempted, tempTotalRight));

            int tempTotalAttemptedOne = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(getTestHeaderID(key.getSubjectID(), "1")));
            int tempTotalRightOne = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(getTestHeaderID(key.getSubjectID(), "1")));
            key.setAccuLone(Utils.getAccuraccy(tempTotalAttemptedOne, tempTotalRightOne));

            int tempTotalAttemptedTwo = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(getTestHeaderID(key.getSubjectID(), "2")));
            int tempTotalRightTwo = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(getTestHeaderID(key.getSubjectID(), "2")));
            key.setAccuLtwo(Utils.getAccuraccy(tempTotalAttemptedTwo, tempTotalRightTwo));

            int tempTotalAttemptedThree = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(getTestHeaderID(key.getSubjectID(), "3")));
            int tempTotalRightThree = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(getTestHeaderID(key.getSubjectID(), "3")));
            key.setAccuthree(Utils.getAccuraccy(tempTotalAttemptedThree, tempTotalRightThree));


        }
        adapter = new SubjectAccuracyLevelAdapter(this, mcqReportArray);
        gridView = v.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
        return v;
    }

    public String getTestHeaderID() {
        String IDS = "";
        StudentMcqTestHdr stdntHdr;
        Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllRowQuery("student_mcq_test_hdr"));
        if (MCqHdr != null && MCqHdr.moveToFirst()) {
            do {
                stdntHdr = (StudentMcqTestHdr) cParse.parseCursor(MCqHdr, new StudentMcqTestHdr());
                IDS += stdntHdr.getStudentMCQTestHDRID() + ",";
            } while (MCqHdr.moveToNext());
            MCqHdr.close();
        } else {
            //    Utils.showToast("Not any Mcq hader", getActivity());
        }


        return Utils.removeLastComma(IDS);
    }

    public String getTestHeaderID(String subID, String levelID) {
        String IDS = "";
        StudentMcqTestHdr stdntHdr;
        if (isFieldExist(Constant.student_mcq_test_hdr,"LevelId")){
        Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllSujectviseHdrIDONE(subID, levelID));
            if (MCqHdr != null && MCqHdr.moveToFirst()) {
                do {
                    stdntHdr = (StudentMcqTestHdr) cParse.parseCursor(MCqHdr, new StudentMcqTestHdr());
                    IDS += stdntHdr.getStudentMCQTestHDRID() + ",";
                } while (MCqHdr.moveToNext());
                MCqHdr.close();
            } else {
                //    Utils.showToast("Not any Mcq hader", getActivity());
            }

        }
        return Utils.removeLastComma(IDS);
    }

    // This method will check if column exists in your table
    public boolean isFieldExist(String tableName, String fieldName)
    {
        boolean isExist = false;
        SQLiteDatabase db = mDb.getWritableDatabase();
        Cursor res = db.rawQuery("PRAGMA table_info("+tableName+")",null);
        res.moveToFirst();
        do {
            String currentColumn = res.getString(1);
            if (currentColumn.equals(fieldName)) {
                isExist = true;
            }
        } while (res.moveToNext());
        return isExist;
    }
    public String getTestHeaderID(String subID) {
        String IDS = "";
        StudentMcqTestHdr stdntHdr;
        Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllSujectviseHdrID(subID));
        if (MCqHdr != null && MCqHdr.moveToFirst()) {
            do {
                stdntHdr = (StudentMcqTestHdr) cParse.parseCursor(MCqHdr, new StudentMcqTestHdr());
                IDS += stdntHdr.getStudentMCQTestHDRID() + ",";
            } while (MCqHdr.moveToNext());
            MCqHdr.close();
        } else {
            //    Utils.showToast("Not any Mcq hader", getActivity());
        }


        return Utils.removeLastComma(IDS);
    }
}
