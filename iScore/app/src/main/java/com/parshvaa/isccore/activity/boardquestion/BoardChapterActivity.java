package com.parshvaa.isccore.activity.boardquestion;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.revisionnote.adapter.RevisionChapterExpAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.ChapterInterface;
import com.parshvaa.isccore.model.Chapter;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.ObservableExpandableListView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.Utils;
import com.nineoldandroids.view.ViewHelper;

import java.io.File;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class BoardChapterActivity extends BaseActivity implements ObservableScrollViewCallbacks, ChapterInterface {
    public BoardChapterActivity instance;
    private Toolbar mToolbar;
    private View mFlexibleSpaceView, mListBackgroundView, mOverlayView, mImageView;
    private int mActionBarSize, mFlexibleSpaceImageHeight;
    public ImageView img_back;
    public TextView mTitleView, tv_accuracy, tv_sub_name, tv_totalAttempts;
    public Chapter chapterObj;
    public Button btn_next;
    public ArrayList<Chapter> tempArray = new ArrayList<>();
    public CheckBox cbSelectAll;
    public ObservableExpandableListView expChapter;
    public RevisionChapterExpAdapter chapterAdapter;
    public Subject subObj;
    public ArrayList<Chapter> chapterArray = new ArrayList<>();
    public CircleImageView icon_sub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_revision_chapter);
        Utils.logUser();
        btn_next = findViewById(R.id.btn_next);
        icon_sub = findViewById(R.id.img_sub);
        instance = this;
        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mActionBarSize = getActionBarSize();

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (chapterAdapter.getSelectedChaptersCount() == 0) {
                    Utils.showToast("Kindly select minimum 1 chapter.", instance);
                    return;
                }
                startActivity(new Intent(instance, SelecteBoardQueTypeActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("ChapterIds", chapterAdapter.getSelectedChapters())
                        .putExtra("ChapterNumber", chapterAdapter.getSelectedChaptersNumber())
                );

            }
        });

        expChapter = findViewById(R.id.expChapter);
        mToolbar = findViewById(R.id.toolbar);
        mOverlayView = findViewById(R.id.overlay);
        mImageView = findViewById(R.id.image);
        mListBackgroundView = findViewById(R.id.list_background);
        expChapter.setScrollViewCallbacks(this);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        img_back = findViewById(R.id.img_back);
        mTitleView = findViewById(R.id.tv_title);
        tv_accuracy = findViewById(R.id.tv_accuracy);
        tv_sub_name = findViewById(R.id.tv_sub_name);


        tv_totalAttempts = findViewById(R.id.tv_totalAttempts);
        File image = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + App.subObj.getSubjectIcon());
        if (image.exists()) {
            Utils.setImage(this,image,icon_sub,R.drawable.sub_english);
//            Picasso.with(this).load(image).into(icon_sub);
        }
        img_back.setOnClickListener(v -> onBackPressed());
        mTitleView.setText("Select Chapter");
        tv_sub_name.setText(App.subObj.getSubjectName());

        getChapter();

        addHeaderInList();


        chapterAdapter = new RevisionChapterExpAdapter(instance, expChapter, chapterArray, cbSelectAll);
        expChapter.setAdapter(chapterAdapter);
        expChapter.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        expChapter.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });

    }

    public void addHeaderInList() {
        View paddingView = new View(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        paddingView = inflater.inflate(R.layout.expand_header, null);

        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen._200sdp));
        paddingView.setLayoutParams(lp);
        // This is required to disable header's list selector effect
        paddingView.setClickable(true);
        expChapter.addHeaderView(paddingView);
        cbSelectAll = paddingView.findViewById(R.id.cbSelectAll);
        cbSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mySharedPref.getVersion().equals("0")) {
                    if (cbSelectAll.isChecked()) {
                        chapterAdapter.selectAll();
                        chapterAdapter.notifyDataSetChanged();
                    } else {
                        chapterAdapter.deSelectAll();
                        chapterAdapter.notifyDataSetChanged();
                    }
                } else {
                    cbSelectAll.setChecked(false);
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(BoardChapterActivity.this);
                    builder1.setMessage("Alert! To view this please purchase the complete version.");
                    builder1.setCancelable(true);
                    builder1.setNegativeButton("Close", (dialog, i) -> {
                        dialog.cancel();
                    });
                    builder1.setPositiveButton(
                            "Subscribe",
                            (dialog, id) -> {
                                dialog.cancel();
                                startActivity(new Intent(BoardChapterActivity.this, ChooseOptionActivity.class));
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
//                if (cbSelectAll.isChecked()) {
//                    chapterAdapter.selectAll();
//                    chapterAdapter.notifyDataSetChanged();
//                } else {
//                    chapterAdapter.deSelectAll();
//                    chapterAdapter.notifyDataSetChanged();
//                }
            }
        });
    }

    public void getChapter() {
        try {
            chapterArray = (ArrayList) new CustomDatabaseQuery(this, new Chapter())
                    .execute(new String[]{DBNewQuery.getChapterGenrate(App.subObj.getSubjectID())}).get();
        } catch (Exception e) {
            e.printStackTrace();
        }


        for (Chapter main : chapterArray) {
            try {
                main.subChapters = (ArrayList) new CustomDatabaseQuery(this, new Chapter())
                        .execute(new String[]{DBNewQuery.getSubChapterGenrate(main.getChapterID())}).get();
                if (main.subChapters.size() < 1) {
                    main.subChapters.add(main);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onCheckBoxDeselect(boolean b) {
        cbSelectAll.setChecked(b);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mFlexibleSpaceView, -scrollY);

        float flexibleRange = mFlexibleSpaceImageHeight - mActionBarSize;
        int minOverlayTransitionY = mActionBarSize - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));

        // Translate list background
        ViewHelper.setTranslationY(mListBackgroundView, Math.max(0, -scrollY + mFlexibleSpaceImageHeight));

        // Change alpha of overlay
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));

        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 180);

        mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }


}

