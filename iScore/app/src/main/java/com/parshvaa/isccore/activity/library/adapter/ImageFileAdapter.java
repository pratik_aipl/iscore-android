package com.parshvaa.isccore.activity.library.adapter;

import android.app.AlertDialog;
import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.androidquery.AQuery;
import com.parshvaa.isccore.activity.library.SystemFiles;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 2/28/2018.
 */

public class ImageFileAdapter extends RecyclerView.Adapter<ImageFileAdapter.ViewHolder> {

    public Context context;
    public LinearLayoutManager lln;
    public ArrayList<SystemFiles> imgArray;
    public AQuery aQuery;
    public LayoutInflater inflater;

    public String SubjectID = "", ChapterID = "";
    // private CAdapter checkBoxAdapter;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public ProgressBar pBar;

        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
            pBar = itemView.findViewById(R.id.pBar);
        }
    }

    public ImageFileAdapter(ArrayList<SystemFiles> imgArray, Context context, String SubjectID, String ChapterID) {
        this.imgArray = imgArray;
        this.context = context;
        this.SubjectID = SubjectID;
        this.ChapterID = ChapterID;
        aQuery = new AQuery(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_image, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        lln = new LinearLayoutManager(context);
//        File imageFile=new File(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + imgArray.get(position).getFileName());
        String file = Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + imgArray.get(position).getFileName();
        Utils.setImage(context, file, holder.img, R.drawable.warning);
        Utils.Log("Image Path", "==>" +file);
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertOffer(file, view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imgArray.size();
    }

    public void alertOffer(String fileName, View view) {

        // This method will be executed once the ti
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final AlertDialog alertTarge = dialogBuilder.create();
        inflater = LayoutInflater.from(view.getContext());

        View alertDialogView = inflater.inflate(R.layout.alert_image, null);
        ProgressBar pBar = alertDialogView.findViewById(R.id.pBar);
        alertTarge.setView(alertDialogView);
        alertTarge.setCancelable(true);
        ImageView imageView = alertDialogView.findViewById(R.id.imageView);
        Utils.setImage(context, fileName, imageView, R.drawable.warning);
        alertTarge.show();


    }
}
