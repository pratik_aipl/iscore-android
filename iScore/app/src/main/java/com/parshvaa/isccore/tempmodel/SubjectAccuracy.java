package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 12/21/2017.
 */

public class SubjectAccuracy implements Serializable {

    public String SubjectID;
    public String SubjectName;
    public String BoardID;
    public String MediumID;
    public String StandardID;
    public String CreatedBy;
    public String CreatedOn;
    public String ModifiedBy;
    public String ModifiedOn;
    public String SubjectIcon;
    public String SubjectOrder;
    public String isMCQ;
    public String IsMapped;
    public String MappedSubjectID;

    public String getIsMapped() {
        return IsMapped;
    }

    public void setIsMapped(String isMapped) {
        IsMapped = isMapped;
    }

    public String getMappedSubjectID() {
        return MappedSubjectID;
    }

    public void setMappedSubjectID(String mappedSubjectID) {
        MappedSubjectID = mappedSubjectID;
    }


    public int getAccuracy1() {
        return accuracy1;
    }

    public void setAccuracy1(int accuracy1) {
        this.accuracy1 = accuracy1;
    }

    public String getLast_Date() {
        return Last_Date;
    }

    public void setLast_Date(String last_Date) {
        Last_Date = last_Date;
    }

    public int getTotalSubjectTest() {
        return TotalSubjectTest;
    }

    public void setTotalSubjectTest(int totalSubjectTest) {
        TotalSubjectTest = totalSubjectTest;
    }

    public String isGeneratePaper;
    public int accuracy1 = 0;
    public String Last_Date = "";
    public int TotalSubjectTest = 0;

    public String getIsMCQ() {
        return isMCQ;
    }

    public void setIsMCQ(String isMCQ) {
        this.isMCQ = isMCQ;
    }

    public String getIsGeneratePaper() {
        return isGeneratePaper;
    }

    public void setIsGeneratePaper(String isGeneratePaper) {
        this.isGeneratePaper = isGeneratePaper;
    }

    public String getSubjectOrder() {
        return SubjectOrder;
    }

    public void setSubjectOrder(String subjectOrder) {
        SubjectOrder = subjectOrder;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String Price;   //tables.put("Price","integer (11)");

    public String getSubjectIcon() {
        return SubjectIcon;
    }

    public void setSubjectIcon(String subjectIcon) {
        SubjectIcon = subjectIcon;
    }

  /*  public SubjectAccuracy(String SubjectID, String SubjectName, String BoardID, String MediumID, String StandardID, String CreatedBy,
                   String CreatedOn, String ModifiedBy, String ModifiedOn, String Price) {

        this.SubjectID = SubjectID;
        this.SubjectName = SubjectName;
        this.BoardID = BoardID;
        this.MediumID = MediumID;
        this.StandardID = StandardID;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;
        this.SubjectIcon = SubjectIcon;
        this.Price = Price;


    }

    public SubjectAccuracy() {

    }*/
    public String OldSubjectID="";

    public String getOldSubjectID() {
        return OldSubjectID;
    }

    public void setOldSubjectID(String oldSubjectID) {
        OldSubjectID = oldSubjectID;
    }


    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}