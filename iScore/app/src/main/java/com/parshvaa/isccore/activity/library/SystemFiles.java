package com.parshvaa.isccore.activity.library;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 2/28/2018.
 */
@JsonObject
public class SystemFiles implements Serializable {
    @JsonField
    public String LFID = "";
    @JsonField
    public String FolderType = "";
    @JsonField
    public String FileType = "";
    @JsonField
    public String FileName = "";
    @JsonField
    public String FileTitle = "";
    @JsonField
    public String DisplayDate="";
    @JsonField
    public String VideoType="";
    @JsonField
    public String PublishOn = "";
    @JsonField
    public String FileSize = "";
    @JsonField
    public boolean isHeader=false;

    public String getVideoType() {
        return VideoType;
    }

    public void setVideoType(String videoType) {
        VideoType = videoType;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getDisplayDate() {
        return DisplayDate;
    }

    public void setDisplayDate(String displayDate) {
        DisplayDate = displayDate;
    }


    public String getFolderType() {
        return FolderType;
    }

    public void setFolderType(String folderType) {
        FolderType = folderType;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getLFID() {
        return LFID;
    }

    public void setLFID(String LFID) {
        this.LFID = LFID;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileTitle() {
        return FileTitle;
    }

    public void setFileTitle(String fileTitle) {
        FileTitle = fileTitle;
    }

    public String getPublishOn() {
        return PublishOn;
    }

    public void setPublishOn(String publishOn) {
        PublishOn = publishOn;
    }

    public String getFileSize() {
        return FileSize;
    }

    public void setFileSize(String fileSize) {
        FileSize = fileSize;
    }


}
