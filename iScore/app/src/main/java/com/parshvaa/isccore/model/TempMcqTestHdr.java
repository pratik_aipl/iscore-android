package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/3/2017.
 */

public class TempMcqTestHdr implements Serializable {


    public int getTempMCQTestHDRID() {
        return TempMCQTestHDRID;
    }

    public void setTempMCQTestHDRID(int tempMCQTestHDRID) {
        TempMCQTestHDRID = tempMCQTestHDRID;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }


    public String getTimeleft() {
        return Timeleft;
    }

    public void setTimeleft(String timeleft) {
        Timeleft = timeleft;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getAddType() {
        return AddType;
    }

    public void setAddType(String addType) {
        AddType = addType;
    }

    public String StudentID = "";
    public String TestTime = "";

    public String getTestTime() {
        return TestTime;
    }

    public void setTestTime(String testTime) {
        TestTime = testTime;
    }

    public int getTimerStatus() {
        return TimerStatus;
    }

    public void setTimerStatus(int timerStatus) {
        TimerStatus = timerStatus;
    }

    public String AddType = "A";
    public String BoardID = "";
    public String MediumID = "";
    public String SubjectID = "";
    public String Timeleft = "";
    public String CreatedBy = "";
    public String CreatedOn = "";
    public String ModifiedBy = "";
    public String ModifiedOn = "";
    public int TempMCQTestHDRID = -1, TimerStatus;
}
