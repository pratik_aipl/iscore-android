package com.parshvaa.isccore.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.Zooki;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.CustPagerTransformer;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ZookiOfflineActivity extends FragmentActivity {

    private int Postion;
    public App app;
    public ImageView img_back;
    ZookiOfflineActivity instance;
    public Zooki zookiObj;
    public ArrayList<Zooki> zookiList = new ArrayList<>();
    private TextView indicatorTv;
    private View close, positionView;
    private ViewPager viewPager;
    private List<ZookiFragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_zooki_offline);
        Utils.logUser();
        instance = this;
 /*       try {
            zookiList = (ArrayList) new CustomDatabaseQuery(this, new Zooki())
                    .execute(new String[]{DBNewQuery.zooki()}).get();
            Zooki obj = new Zooki();
            obj.setTitle("View More");
            zookiList.add(obj);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        positionView = findViewById(R.id.position_view);

        close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        try {
            Postion = getIntent().getExtras().getInt("pos");
        } catch (Exception e) {
            e.printStackTrace();
        }
        fillViewPager();
    }


    private void fillViewPager() {
        indicatorTv = findViewById(R.id.indicator_tv);
        viewPager = findViewById(R.id.viewpager);
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));
        for (int i = 0; i < zookiList.size(); i++) {
            fragments.add(new ZookiFragment());
        }
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                ZookiFragment fragment = fragments.get(position % 10);
                fragment.bindData(zookiList.get(position % zookiList.size()));
                return fragment;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return super.isViewFromObject(view, object);
            }

            @Override
            public int getCount() {
                return zookiList.size();
            }
        });

        viewPager.setCurrentItem(Postion);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateIndicatorTv();
    }

    private void updateIndicatorTv() {
        int totalNum = viewPager.getAdapter().getCount();
        int currentItem = viewPager.getCurrentItem() + 1;
        indicatorTv.setText(Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum));
    }
}
