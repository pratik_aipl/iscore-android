package com.parshvaa.isccore.activity.library;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Constant;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.youtubeplayer.player.playerUtils.FullScreenHelper;
import com.pierfrancescosoffritti.youtubeplayer.ui.PlayerUIController;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import tcking.github.com.giraffeplayer2.VideoView;

public class VideoViewActivity extends BaseActivity {
    private static final String TAG = "VideoViewActivity";
    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    public String SubjectID = "", ChapterID = "";
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    @BindView(R.id.bvp)
    VideoView bvp;
    @BindView(R.id.mVideoWeb)
    WebView mVideoWeb;
    @BindView(R.id.rel_main)
    RelativeLayout relMain;
    @BindView(R.id.mBack)
    ImageView mBack;

    @BindView(R.id.youtube_player_view)
    YouTubePlayerView youtubePlayerView;

    private final Runnable mShowPart2Runnable = () -> {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
    };

    private boolean mVisible;
    public String video_id, Type = "", videoType = "", personal_Path = "", FileNAME = "";
    public int pos = 0;
    private final Runnable mHideRunnable = () -> hide();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        ButterKnife.bind(this);

        Type = getIntent().getStringExtra("Type") == null ? "" : getIntent().getStringExtra("Type");
        videoType = getIntent().getStringExtra("videoType") == null ? "" : getIntent().getStringExtra("videoType");
        FileNAME = getIntent().getStringExtra("FILENAME") == null ? "" : getIntent().getStringExtra("FILENAME");

        Log.d(TAG, "Type: " + Type);
        Log.d(TAG, "videoType: " + videoType);
        FullScreenHelper fullScreenHelper = new FullScreenHelper();

        //youtubePlayerView = new YouTubePlayerView(this);

        if (!TextUtils.isEmpty(Type) && !videoType.equalsIgnoreCase("L")) {
            mBack.setVisibility(View.GONE);
            if (Type.equalsIgnoreCase("personal")) {
                mVisible = true;
                video_id = getIntent().getStringExtra("VideoID");
                personal_Path = getIntent().getStringExtra("Path");
                Log.d(TAG, "Path: " + personal_Path);
                for (int i = 0; i < App.perrsonal_video_list.size(); i++) {
                    if (video_id.equalsIgnoreCase(App.perrsonal_video_list.get(i).getFileID())) {
                        bvp.setVideoPath(personal_Path + App.perrsonal_video_list.get(i).getFileName()).getPlayer().start();
                        pos = i;
                    }
                }
            } else {
                mVisible = true;
                video_id = getIntent().getStringExtra("LFID");
                SubjectID = getIntent().getStringExtra("SubjectID");
                ChapterID = getIntent().getStringExtra("ChapterID");
                Log.d(TAG, "video_id:>> " + video_id + "subject id>>>>" + SubjectID + "ChapterID>>" + ChapterID);
                Log.d(TAG, "video_list: " + App.video_list.size());

                for (int i = 0; i < App.video_list.size(); i++) {
                    Log.d(TAG, "video_list>> " + App.video_list.get(i).getLFID());
                    if (video_id.equalsIgnoreCase(App.video_list.get(i).getLFID())) {
                        bvp.setVideoPath(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + App.video_list.get(i).getFileName()).getPlayer().start();
                        pos = i;
                        Log.d(TAG, "video path: " + Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + App.video_list.get(i).getFileName());
                    }
                }
            }
        } else {
            mBack.setVisibility(View.GONE);
            bvp.setVisibility(View.GONE);

            if(FileNAME.startsWith("https://www.youtube.com")){
               // youtubePlayerView.enterFullScreen();

                mVideoWeb.setVisibility(View.GONE);
                youtubePlayerView.setVisibility(View.VISIBLE);

                PlayerUIController uiController = youtubePlayerView.getPlayerUIController();
                uiController.showVideoTitle(false);
                uiController.setVideoTitle("");
                uiController.showMenuButton(false);
                uiController.showYouTubeButton(false);
                uiController.showUI(true);

                youtubePlayerView.initialize(new YouTubePlayerInitListener() {
                    @Override
                    public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                        initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {
                                String videoId = FileNAME.substring(FileNAME.lastIndexOf("/")+1);
                                initializedYouTubePlayer.loadVideo(videoId, 0);
                            }
                        });
                    }
                }, true);

                youtubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
                    @Override
                    public void onYouTubePlayerEnterFullScreen() {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        fullScreenHelper.enterFullScreen(youtubePlayerView);

                        addCustomActionsToPlayer();
                    }

                    @Override
                    public void onYouTubePlayerExitFullScreen() {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        fullScreenHelper.exitFullScreen(youtubePlayerView);

                        removeCustomActionsFromPlayer();
                    }
                });

            }else{
                youtubePlayerView.setVisibility(View.GONE);
                mVideoWeb.setVisibility(View.VISIBLE);

                String vId = getIntent().getStringExtra("VideoId");
                Log.d(TAG, "VideoID: " + vId);
                // String frameVideo = "<html><body><iframe width=\"350\" height=\"315\" src=\"https://www.youtube.com/embed/HdroHYazvRc/preview\"frameborder=\"0\" allowfullscreen=\"true\"></iframe></body></html>";
                //<embed type=\"video/webm\" src="+FileNAME+" width=\"350\" height=\"300\" allowfullscreen/>\n" +

                String htmlData = "<html>\n" +
                        "               <body>\n" +
                        "                <iframe src="+FileNAME+" width=\"100%\" height=\"100%\" frameborder=\"0\" scrolling=\"no\" allowfullscreen='true' webkitallowfullscreen='true' mozallowfullscreen='true'></iframe> \n" +
                        "                </body>\n" +
                        "            </html>";
//            getResources().getString(R.string.videoData, vId);
                Log.d(TAG, "htmlData: " + htmlData);
                CookieManager.getInstance().setAcceptCookie(true);
                if (Build.VERSION.SDK_INT >= 21) {
                    CookieManager.getInstance().setAcceptThirdPartyCookies(mVideoWeb, true);
                }
                WebSettings webSettings = mVideoWeb.getSettings();
                mVideoWeb.requestFocus();
                mVideoWeb.getSettings().setLightTouchEnabled(true);
                mVideoWeb.getSettings().setJavaScriptEnabled(true);
                mVideoWeb.getSettings().setGeolocationEnabled(true);
                mVideoWeb.setSoundEffectsEnabled(true);
                mVideoWeb.setWebViewClient(new WebViewClient());
                mVideoWeb.setWebChromeClient(new WebChromeClient());
                webSettings.setJavaScriptEnabled(true);
                webSettings.setDomStorageEnabled(true);
                //mVideoWeb.getSettings().setBuiltInZoomControls(true);
                webSettings.setAllowFileAccessFromFileURLs(true);
                webSettings.setAllowUniversalAccessFromFileURLs(true);
                webSettings.setMediaPlaybackRequiresUserGesture(false);
                webSettings.setPluginState(WebSettings.PluginState.ON);
                mVideoWeb.loadData(htmlData, "text/html", "utf-8");
                // mVideoWeb.loadUrl("https://www.youtube.com/watch?v=HdroHYazvRc");
            }

        }
        relMain.setOnClickListener(view -> toggle());
        mBack.setOnClickListener(view -> {
            if (youtubePlayerView.isFullScreen())
                youtubePlayerView.exitFullScreen();
            else
                super.onBackPressed();
        });
    }

    private void addCustomActionsToPlayer() {
        Drawable customAction1Icon = ContextCompat.getDrawable(this, R.drawable.exomedia_ic_rewind_white);
        Drawable customAction2Icon = ContextCompat.getDrawable(this, R.drawable.exomedia_ic_fast_forward_white);
        assert customAction1Icon != null;
        assert customAction2Icon != null;

        youtubePlayerView.getPlayerUIController().setCustomAction1(customAction1Icon, view ->
                Toast.makeText(this, "custom action1 clicked", Toast.LENGTH_SHORT).show());

        youtubePlayerView.getPlayerUIController().setCustomAction2(customAction2Icon, view ->
                Toast.makeText(this, "custom action1 clicked", Toast.LENGTH_SHORT).show());
    }

    private void removeCustomActionsFromPlayer() {
        youtubePlayerView.getPlayerUIController().showCustomAction1(false);
        youtubePlayerView.getPlayerUIController().showCustomAction2(false);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfiguration) {
        super.onConfigurationChanged(newConfiguration);
        Objects.requireNonNull(youtubePlayerView.getPlayerUIController().getMenu()).dismiss();
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        youtubePlayerView.release();
    }

    private class WebAppInterface {
        Context mContext;

        WebAppInterface(Context c) {
            mContext = c;
        }

        // This function can be called in our JS script now
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_LONG).show();
        }
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;
        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
//        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        bvp.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;
        // Schedule a runnable to display UI elements after a delay
//        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

   /* *
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.*/

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}

