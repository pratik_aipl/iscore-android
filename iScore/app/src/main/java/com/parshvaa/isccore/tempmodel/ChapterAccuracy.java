package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 2/4/2017.
 */

public class ChapterAccuracy implements Serializable {

    private static final long serialVersionUID = 1L;

    public static int isChecked = -1;

    private String name;
    public String ChapterID;
    public String ChapterNumber;
    public String ParentChapterID;
    public String BoardID;
    public String MediumID;
    public String StandardID;
    public String SubjectID;
    public String ChapterName;
    public String DisplayOrder;
    public String isMCQ;
    public String isDemo = "";
    public int acuracy = 0;

    public int getAcuracy() {
        return acuracy;
    }

    public void setAcuracy(int acuracy) {
        this.acuracy = acuracy;
    }

    public String getIsDemo() {
        return isDemo;
    }

    public void setIsDemo(String isDemo) {
        this.isDemo = isDemo;
    }

    public String getIsMCQ() {
        return isMCQ;
    }

    public void setIsMCQ(String isMCQ) {
        this.isMCQ = isMCQ;
    }

    public String getDisplayOrder() {
        return DisplayOrder;
    }

    public void setDisplayOrder(String displayOrder) {
        DisplayOrder = displayOrder;
    }

    public String CreatedBy;
    public String CreatedOn;
    public String ModifiedBy;
    public String ModifiedOn;

    public String isGeneratePaper;
    public ArrayList<ChapterAccuracy> subChapters = new ArrayList<>();


    public String getIsGeneratePaper() {
        return isGeneratePaper;
    }

    public void setIsGeneratePaper(String isGeneratePaper) {
        this.isGeneratePaper = isGeneratePaper;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getChapterNumber() {
        return ChapterNumber;
    }

    public void setChapterNumber(String chapterNumber) {
        ChapterNumber = chapterNumber;
    }

    public String getParentChapterID() {
        return ParentChapterID;
    }

    public void setParentChapterID(String parentChapterID) {
        ParentChapterID = parentChapterID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }


    public boolean isSelected;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

}