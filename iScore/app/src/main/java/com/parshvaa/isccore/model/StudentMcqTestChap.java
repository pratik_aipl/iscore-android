package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/4/2017.
 */


@JsonObject
public class StudentMcqTestChap implements Serializable {
    public int getStudentMCQTestchapterid() {
        return StudentMCQTestChapterID;
    }

    public void setStudentMCQTestchapterid(int studentMCQTestchapterid) {
        StudentMCQTestChapterID = studentMCQTestchapterid;
    }

    public String getStudentMCQTestHDRID() {
        return StudentMCQTestHDRID;
    }

    public void setStudentMCQTestHDRID(String studentMCQTestHDRID) {
        StudentMCQTestHDRID = studentMCQTestHDRID;
    }

    public String getChapterid() {
        return ChapterID;
    }

    public void setChapterid(String chapterid) {
        ChapterID = chapterid;
    }

    @JsonField
    public String StudentMCQTestHDRID = "", ChapterID = "";
    @JsonField
    public int StudentMCQTestChapterID = -1;
}
