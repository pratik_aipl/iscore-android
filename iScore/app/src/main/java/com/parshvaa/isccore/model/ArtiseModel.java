package com.parshvaa.isccore.model;

import java.io.Serializable;

public class ArtiseModel implements Serializable{

    public String ArtistID;
    public String ArtistName;
    public String Image;

    public String getArtistID() {
        return ArtistID;
    }

    public void setArtistID(String artistID) {
        ArtistID = artistID;
    }

    public String getArtistName() {
        return ArtistName;
    }

    public void setArtistName(String artistName) {
        ArtistName = artistName;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
