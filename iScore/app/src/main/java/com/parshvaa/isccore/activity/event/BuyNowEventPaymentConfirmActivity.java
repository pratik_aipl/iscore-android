package com.parshvaa.isccore.activity.event;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.MobiLoginActivity;
import com.parshvaa.isccore.activity.library.videoPleyar.utility.Util;
import com.parshvaa.isccore.activity.paytm.Api;
import com.parshvaa.isccore.activity.paytm.Checksum;
import com.parshvaa.isccore.activity.paytm.Paytm;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.KeyData;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.RobotoEditTextView;
import com.parshvaa.isccore.utils.Utils;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BuyNowEventPaymentConfirmActivity extends BaseActivity implements AsynchTaskListner, PaytmPaymentTransactionCallback {
    private static final String TAG = "PaymentConfirmActivity";
    //    public PaymentConfirmActivity instance;
    public Button btn_confirm;
    public LinearLayout lin_locataion, lin_wrong, lin_right;
    public ImageView img_back, img_next, img_close;
    public TextView tv_standard, tv_medium, tv_board, tv_name, tv_amount,tv_location,
            tv_wrong_text, tv_right_text, tv_have_referal;
    public App app;
    public JsonParserUniversal jParser;
    public CircleImageView img_profile;
    public String first_name, last_name, emailID="", mobileNo, boardName, meduimName, standardName,
            boardId, mediumId, standardId, version = "", address = "", Guid = "", PleyaerID = "", amount = "", EventName = "",EventID = "", StartDate = "", EndDate = "", Artist = "";
    String Response, TXTID, TXNAMOUNT, ORDERID, CHECKSUMHASH;
    String actualAmount = "", codeOwner = "", payableAmount = "";
    public MyDBManager mDb;
    public EditText et_referal;
    public RobotoEditTextView etEmail,et_mobile;

    public AQuery aQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.confirm_event_paayment);
        Utils.logUser();
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        mySharedPref = new MySharedPref(this);
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        aQuery = new AQuery(BuyNowEventPaymentConfirmActivity.this);

        int verCode = Utils.currentAppVersionCode(this);
        version = String.valueOf(verCode);

        amount = getIntent().getExtras().getString("amount");
        EventName = getIntent().getExtras().getString("EventName");
        EventID = getIntent().getExtras().getString("EventID");
        StartDate = getIntent().getExtras().getString("StartDate");
        EndDate = getIntent().getExtras().getString("EndDate");
        Artist = getIntent().getExtras().getString("Artist");


        img_back = findViewById(R.id.img_back);
        img_next = findViewById(R.id.img_next);
        img_close = findViewById(R.id.img_close);
        lin_locataion = findViewById(R.id.lin_locataion);
        lin_wrong = findViewById(R.id.lin_wrong);
        lin_right = findViewById(R.id.lin_right);
        img_profile = findViewById(R.id.img_profile);
        tv_wrong_text = findViewById(R.id.tv_wrong_text);
        tv_right_text = findViewById(R.id.tv_right_text);
        tv_have_referal = findViewById(R.id.tv_have_referal);
        tv_name = findViewById(R.id.tv_name);
        tv_board = findViewById(R.id.tv_board);
        tv_medium = findViewById(R.id.tv_medium);
        tv_standard = findViewById(R.id.tv_standard);
        tv_location = findViewById(R.id.tv_location);
        tv_amount = findViewById(R.id.tv_amount);
        et_referal = findViewById(R.id.et_referal);
        etEmail = findViewById(R.id.et_event_email);
        et_mobile = findViewById(R.id.et_mobile);

        btn_confirm = findViewById(R.id.btn_confirm);


        tv_amount.setText("Rs. " + amount);
        emailID = MainUser.getEmailID();
        mobileNo = MainUser.getRegMobNo();
        first_name = MainUser.getFirstName();
        last_name = MainUser.getLastName();

        tv_name.setText(first_name + " " + last_name);
        et_mobile.setText(mobileNo);
        etEmail.setText(emailID);
        tv_board.setText(EventName);
        tv_medium.setText(StartDate);
        tv_standard.setText(EndDate);
        tv_location.setText(Artist);

    /*
        boardName = MainUser.getBoardName();
        meduimName = MainUser.getMediumName();
        standardName = MainUser.getStanderdName();
        mediumId = MainUser.getMediumID();
        boardId = MainUser.getBoardID();
        standardId = MainUser.getStandardID();

        tv_name.setText(first_name + " " + last_name);
        tv_board.setText(boardName);
        tv_medium.setText(meduimName);
        tv_standard.setText(Html.fromHtml(standardName + "th standard - <span style='color:#66bb6a !important;'> Rs. " + amount));
*/
        String ProfileImage = mySharedPref.getProf_image().substring(mySharedPref.getProf_image().lastIndexOf("/") + 1);
        File imageDirProfileImage = new File(Constant.LOCAL_IMAGE_PATH + "/profile/");
        File imageFile = new File(imageDirProfileImage.getAbsolutePath() + "/" + ProfileImage);
        if (!TextUtils.isEmpty(ProfileImage)) {
            if (imageFile.exists()) {
                Utils.setImage(this, imageFile, img_profile,R.drawable.profile);
            } else {
                if (Utils.isNetworkAvailable(this)) {
                    Util.downloadClassIcon(this, aQuery, mySharedPref.getProf_image(), img_profile, imageFile, R.drawable.profile);
                } else {
                    img_profile.setImageResource(R.drawable.profile);
                }
            }
        } else {
            img_profile.setImageResource(R.drawable.profile);
        }

        /**********************promo code********************/
        tv_have_referal.setTag("Have a Promo Code?");
        tv_have_referal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv_have_referal.getTag().toString().equalsIgnoreCase("Have a Promo Code?")) {
                    alertReferralCode();

                } else if (tv_have_referal.getTag().toString().equalsIgnoreCase("Remove Referral code")) {
                    lin_right.setVisibility(View.GONE);
                    lin_wrong.setVisibility(View.GONE);
                    amount = getIntent().getExtras().getString("amount");
                    tv_have_referal.setTag("Have a Promo Code?");
                    tv_have_referal.setText("Have a Promo Code?");
                    tv_have_referal.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tv_amount.setText("Rs. " + amount);
                }
            }
        });
        et_referal.addTextChangedListener(mTextEditorWatcher);
        img_next.setOnClickListener(view -> {
            if (Utils.isNetworkAvailable(this)) {
                showProgress(true, true);
                new CallRequest(BuyNowEventPaymentConfirmActivity.this).varify_Promo_code(et_referal.getText().toString(), amount);
            }
        });
        img_close.setOnClickListener(view -> et_referal.getText().clear());
        /**********************promo code********************/


        img_back.setOnClickListener(v -> onBackPressed());

        btn_confirm.setOnClickListener(v ->
        {
            if (TextUtils.isEmpty(et_mobile.getText().toString())) {
                et_mobile.setError("Please Enter Mobile No.");
            } else if (!et_mobile.getText().toString().matches("^[0-9]{10}$")) {
                et_mobile.setError("Enter appropriate mobile number.");
            } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
                etEmail.setError("Please Enter Email Id");
            } else if (!etEmail.getText().toString().matches("^(.+)@(.+)$")) {
                etEmail.setError("Enter appropriate Email ID");
            } else {
                if (Utils.isNetworkAvailable(this)) {
                    generateCheckSum(amount);
                /*showProgress(true, true);
                new CallRequest(BuyNowEventPaymentConfirmActivity.this).verify_bord_medium_standard(MainUser.getBoardID(),
                        MainUser.getMediumID(),
                        MainUser.getStandardID(),
                        mySharedPref.getClassID());*/
                }
            }

        });


    }

    private void alertReferralCode() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final AlertDialog alertTarge = dialogBuilder.create();
        LayoutInflater inflater = getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.alert_referral_code, null);
        alertTarge.setView(alertDialogView);
        alertTarge.setCancelable(true);
        TextView tv_code_name = alertDialogView.findViewById(R.id.tv_code_name);
        EditText et_referal1 = alertDialogView.findViewById(R.id.et_referal);
        ImageView rel_submit = alertDialogView.findViewById(R.id.rel_submit);
        tv_code_name.setText("Enter Promo Code");
        rel_submit.setOnClickListener(view -> {
            if (et_referal1.getText().toString().equals("")) {
                et_referal1.setError("Please Enter Referral code");
            } else {
                showProgress(true, true);
                new CallRequest(this).varify_Promo_code(et_referal1.getText().toString(), amount);
                alertTarge.dismiss();
                tv_have_referal.setText("Remove Referral code");
                tv_have_referal.setTag("Remove Referral code");
                tv_have_referal.setTextColor(getResources().getColor(R.color.wrong));
            }
        });

        alertTarge.show();
    }

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //This sets a textview to the current length
            if (s.length() >= 6) {
                img_close.setVisibility(View.GONE);
                img_next.setVisibility(View.VISIBLE);
            } else {
                img_close.setVisibility(View.VISIBLE);
                img_next.setVisibility(View.GONE);
                lin_wrong.setVisibility(View.GONE);
                lin_right.setVisibility(View.GONE);
            }

        }

        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case EventSubscribe:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                           /* Utils.deleteAllTableRecord(mDb);
                            App.deleteCache(this);
                            mySharedPref.setIsLoggedIn(true);
                            mySharedPref.setDownloadStatus("y");
                            JSONObject jData = jObj.getJSONObject("data");
                            if (jData != null && jData.length() > 0) {
                                DataSetInSherdPref(jData);
                            }*/
                         /*   Intent intent = new Intent(this, EventListActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);*/
                           onBackPressed();

                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Please try again later", this);
                        e.printStackTrace();
                    }
                    break;
                case GetPromoCode:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            actualAmount = (jObj.getString("Discount"));
                            String amountAc = String.valueOf(Integer.parseInt(amount) - Integer.parseInt(actualAmount));
                            codeOwner = jObj.getJSONObject("data").getString("Code");
                            amount = jObj.getString("Payable_Amount");
                            lin_wrong.setVisibility(View.GONE);
                            lin_right.setVisibility(View.VISIBLE);
                            tv_right_text.setText(jObj.getString("message"));
                            tv_amount.setText("Rs. " + amount);

                            Bundle bundle1 = new Bundle();
                            bundle1.putString("CodeOwner", codeOwner);
                            bundle1.putString("Amount", amount);
                            mFirebaseAnalytics.logEvent("REFERCODE", bundle1);

                        } else {
                            lin_wrong.setVisibility(View.VISIBLE);
                            lin_right.setVisibility(View.GONE);
                            tv_wrong_text.setText(jObj.getString("message"));
                            tv_have_referal.setTag("Have a Promo Code?");
                            tv_have_referal.setText("Have a Promo Code?");
                            tv_have_referal.setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                    } catch (Exception e) {
                        Utils.showToast("message", this);
                        e.printStackTrace();
                    }
                    break;

                case getTxnStatus:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("STATUS").equals("TXN_SUCCESS")) {
                            Response = jObj.toString();
                            TXTID = jObj.getString("TXNID");
                            TXNAMOUNT = jObj.getString("TXNAMOUNT");
                            ORDERID = jObj.getString("ORDERID");
                            Utils.Log("TAG", "Response json :-> " + Response);
                            Utils.Log("TAG", "go to Register :-> ");
                            new CallRequest(BuyNowEventPaymentConfirmActivity.this).
                                    paymentEvent(
                                            et_mobile.getText().toString(),
                                            etEmail.getText().toString(),
                                            EventID,
                                            TXTID,
                                            ORDERID,
                                            amount,
                                            actualAmount,
                                            TXNAMOUNT,
                                            Response,
                                            codeOwner);
                        } else {
                            Utils.showToast("some error occurred", this);
                        }
                    } catch (Exception e) {
                        Utils.showToast("message", this);
                        e.printStackTrace();
                    }
                    break;
                case Verifychecksum:
                    showProgress(false, true);
                    break;
            }
        }
    }

    public void DataSetInSherdPref(JSONObject jData) {
        try {
            MainUser.setStudentId(jData.getString("StudentID"));
            MainUser.setFirstName(jData.getString("FirstName"));
            MainUser.setLastName(jData.getString("LastName"));
            MainUser.setGuid(jData.getString("IMEINo"));
            MainUser.setEmailID(jData.getString("EmailID"));
            MainUser.setRegMobNo(jData.getString("RegMobNo"));
            MainUser.setSchoolName(jData.getString("SchoolName"));
            MainUser.setStudentCode(jData.getString("StudentCode"));

            mySharedPref.setStudentCode(jData.getString("StudentCode"));
            mySharedPref.setStudent_id(jData.getString("StudentID"));
            mySharedPref.setFirst_name(jData.getString("FirstName"));
            mySharedPref.setLast_name(jData.getString("LastName"));
            mySharedPref.setGuuid(jData.getString("IMEINo"));
            mySharedPref.setEmailID(jData.getString("EmailID"));
            mySharedPref.setRegMobNo(jData.getString("RegMobNo"));
            mySharedPref.setProfileImage(jData.getString("ProfileImage"));
            mySharedPref.setVersioCode(version);
            mySharedPref.setIsLoggedIn(true);
            mySharedPref.setDownloadStatus("y");

//            String fileName = App.Image.substring(App.Image.lastIndexOf("/") + 1);
//            String inputpath = App.Image.substring(0, App.Image.lastIndexOf("/") + 1);
//            copyFile(inputpath, fileName, Constant.LOCAL_IMAGE_PATH + "/profile/");
            JSONArray jsonArray = jData.getJSONArray("KeyData");
            KeyData keydata;
            MainUser.keyDataArray.clear();
            for (int i = 0; i < jsonArray.length(); i++) {
                keydata = (KeyData) jParser.parseJson(jsonArray.getJSONObject(i), new KeyData());
                MainUser.keyDataArray.add(keydata);
                MainUser.setRegKey(MainUser.keyDataArray.get(i).getStudentKey());
                mySharedPref.setStudent_id(MainUser.keyDataArray.get(i).getStudentID());
                mySharedPref.setBoardID(MainUser.keyDataArray.get(i).getBoardID());
                mySharedPref.setMediumID(MainUser.keyDataArray.get(i).getMediumID());
                mySharedPref.setStandardID(MainUser.keyDataArray.get(i).getStandardID());
                mySharedPref.setClassID(MainUser.keyDataArray.get(i).getClassID());
                mySharedPref.setBranchID(MainUser.keyDataArray.get(i).getBranchID());
                mySharedPref.setBatchID(MainUser.keyDataArray.get(i).getBatchID());
                mySharedPref.setReg_key(MainUser.keyDataArray.get(i).getStudentKey());
                mySharedPref.setVersion(MainUser.keyDataArray.get(i).getIsDemo());
                mySharedPref.setStandardName(MainUser.keyDataArray.get(i).getStandardName());
                mySharedPref.setClassName(MainUser.keyDataArray.get(i).getClassName());
                mySharedPref.setBoardName(MainUser.keyDataArray.get(i).getBoardName());
                mySharedPref.setMediumName(MainUser.keyDataArray.get(i).getMediumName());
                mySharedPref.setClassLogo(MainUser.keyDataArray.get(i).getClassLogo());
                mySharedPref.setClassLogo2(MainUser.keyDataArray.get(i).getSplashLogo());
                mySharedPref.setClassRoundLogo(MainUser.keyDataArray.get(i).getClassRoundLogo());
                mySharedPref.setlblPrviousTarget(MainUser.keyDataArray.get(i).getPreviousYearScore());
                mySharedPref.setCreatedOn(MainUser.keyDataArray.get(i).getCreatedOn());
            }


            Bundle bundle = new Bundle();
            mFirebaseAnalytics.logEvent("SUBSCRIBE", bundle);

            startActivity(new Intent(BuyNowEventPaymentConfirmActivity.this, DashBoardActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generateCheckSum(String amount) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //creating the retrofit api service
        Api apiService = retrofit.create(Api.class);
        // Live Credentials
        final Paytm paytm = new Paytm(
                Constant.M_ID,
                Constant.CHANNEL_ID,
                amount,
                Constant.WEBSITE,
                Constant.CALLBACK_URL,
                Constant.INDUSTRY_TYPE_ID, etEmail.getText().toString(), et_mobile.getText().toString()
        );

        //creating a call object from the apiService
        Call<Checksum> call = apiService.getChecksum(
                paytm.getmId(),
                paytm.getOrderId(),
                paytm.getCustId(),
                paytm.getIndustryTypeId(),
                paytm.getChannelId(),
                paytm.getTxnAmount(),
                paytm.getWebsite(),
                paytm.getEMAIL(),
                paytm.getMOBILE_NO(),
                paytm.getCallBackUrl() + paytm.getOrderId()
        );

        if (BuildConfig.DEBUG)
            Log.d("TAG => ", "generateCheckSum: paytm.getmId() " + paytm.getmId() + "  paytm.getOrderId() " +
                    paytm.getOrderId() + "  paytm.getCustId()" +
                    paytm.getCustId() + " paytm.getIndustryTypeId()" +
                    paytm.getIndustryTypeId() + " paytm.getChannelId()" +
                    paytm.getChannelId() + " paytm.getTxnAmount() " +
                    paytm.getTxnAmount() + " paytm.getWebsite() " +
                    paytm.getWebsite() + " paytm.getEMAIL()" +
                    paytm.getEMAIL() + " paytm.getMOBILE_NO() " +
                    paytm.getMOBILE_NO() + " paytm.getCallBackUrl() " +
                    paytm.getCallBackUrl() + paytm.getOrderId());
        //making the call to generate checksum
        call.enqueue(new Callback<Checksum>() {
            @Override
            public void onResponse(Call<Checksum> call, Response<Checksum> response) {

                Log.d("TAG", "onResponse: " + ((response.body()) != null));
                if (response.body() != null) {
                    if (response.body().getChecksumHash() != null) {
                        initializePaytmPayment(response.body().getChecksumHash(), paytm);
                    }
                }
            }

            @Override
            public void onFailure(Call<Checksum> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void initializePaytmPayment(String checksumHash, final Paytm paytm) {

        final PaytmPGService Service = PaytmPGService.getProductionService();
        //creating a hashmap and adding all the values required
        final Map<String, String> paramMap = new HashMap<>();
        paramMap.put("MID", paytm.getmId());
        paramMap.put("ORDER_ID", paytm.getOrderId());
        paramMap.put("CUST_ID", paytm.getCustId());
        paramMap.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());
        paramMap.put("CHANNEL_ID", paytm.getChannelId());
        paramMap.put("TXN_AMOUNT", paytm.getTxnAmount());
        paramMap.put("WEBSITE", paytm.getWebsite());
        paramMap.put("EMAIL", paytm.getEMAIL());
        paramMap.put("MOBILE_NO", paytm.getMOBILE_NO());
        paramMap.put("CALLBACK_URL", paytm.getCallBackUrl() + paytm.getOrderId());
        paramMap.put("CHECKSUMHASH", checksumHash);
        PaytmOrder order = new PaytmOrder(paramMap);
        //intializing the paytm service
        Service.initialize(order, null);
        //finally starting the payment transaction
        Service.startPaymentTransaction(this, true, false, this);

    }

    @Override
    public void onTransactionResponse(Bundle bundle) {
        // Toast.makeText(this, bundle.toString(), Toast.LENGTH_LONG).show();
        try {
            Response = bundle.toString();
            Response = Response.substring(7, Response.length() - 1);
            TXTID = bundle.getString("TXNID");
            TXNAMOUNT = bundle.getString("TXNAMOUNT");
            ORDERID = bundle.getString("ORDERID");
            CHECKSUMHASH = bundle.getString("CHECKSUMHASH");

            Log.d("TAG", "onTransactionResponse: " + TXTID);
            Log.d("TAG", "onTransactionResponse: " + TXNAMOUNT);
            Log.d("TAG", "onTransactionResponse: " + ORDERID);
            Log.d("TAG", "onTransactionResponse: " + CHECKSUMHASH);

        } catch (Exception e) {
            e.printStackTrace();
        }
        String Json = "{\"MID\":\"" + Constant.M_ID + "\",\"ORDERID\":\"" + ORDERID + "\",\"CHECKSUMHASH\":\"" + CHECKSUMHASH + "\"}";
        Log.d("TAG", "onTransactionResponse: " + URLEncoder.encode(Json));

        Bundle bundle1 = new Bundle();
        bundle1.putString(FirebaseAnalytics.Param.METHOD, "CONFIRM-PAYMENT");
        bundle1.putString(FirebaseAnalytics.Param.TRANSACTION_ID, ORDERID);
        bundle1.putString(FirebaseAnalytics.Param.PRICE, TXNAMOUNT);
        mFirebaseAnalytics.logEvent("ADDTOCART", bundle);

        new CallRequest(BuyNowEventPaymentConfirmActivity.this).getTxnStatus(Constant.TXN_STATUS_URL, URLEncoder.encode(Json));
    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        //   Toast.makeText(this, "Back Pressed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
    }

}
