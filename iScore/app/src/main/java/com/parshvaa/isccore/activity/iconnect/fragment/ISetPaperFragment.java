package com.parshvaa.isccore.activity.iconnect.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;

import com.parshvaa.isccore.activity.iconnect.activity.ISetPaperActivity;
import com.parshvaa.isccore.activity.iconnect.adapter.GeneratePaperSolutionAdapter;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.SetPaperQuestionAndTypes;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.nineoldandroids.view.ViewPropertyAnimator;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 1/31/2018.
 */

public class ISetPaperFragment extends Fragment {
    private static final String TAG = "ISetPaperFragment";
    public View v;
    private static final String ARG_PAGE_NUMBER = "page_number";
    private static final String isAns = "ans";
    private boolean isShowAns = false;

    private RecyclerView recyclerView;
    private GeneratePaperSolutionAdapter mAdapter;

    private ArrayList<SetPaperQuestionAndTypes> typesArrayList = new ArrayList<>();
    private boolean mFabIsShown;
    public ArrayList<SetPaperQuestionAndTypes> tempArray = new ArrayList<>();
    public static int pageNumber = 0;

    public Fragment newInstance(int page, boolean ANS) {
        ISetPaperFragment fragment = new ISetPaperFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        args.putBoolean(isAns, ANS);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_paper_solution, container, false);
        recyclerView = v.findViewById(R.id.recycler_paper);
        int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        isShowAns = getArguments().getBoolean(isAns, false);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));

        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
        if (ISetPaperActivity.questionsList != null && ISetPaperActivity.questionsList.size() > pageNumber) {
            typesArrayList = ISetPaperActivity.questionsList.get(pageNumber).prelimSetList;
        }


        setupRecyclerView();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    hideFab();
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    hideFab();

                } else {
                    showFab();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }
        });

        return v;

    }

    private void showFab() {
        if (!mFabIsShown) {
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).scaleX(1).scaleY(1).setDuration(200).start();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).scaleX(1).scaleY(1).setDuration(200).start();
            mFabIsShown = true;
        }
    }

    private void hideFab() {
        if (mFabIsShown) {
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).scaleX(0).scaleY(0).setDuration(200).start();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).scaleX(0).scaleY(0).setDuration(200).start();
            mFabIsShown = false;
        }
    }


    private void setupRecyclerView() {
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Log.d(TAG, "setupRecyclerView: " + isShowAns);
        mAdapter = new GeneratePaperSolutionAdapter(typesArrayList, getActivity(), isShowAns);
        recyclerView.setAdapter(mAdapter);
    }
}

