package com.parshvaa.isccore.activity.library.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.parshvaa.isccore.activity.library.activity.LibrarySystemFilesActivity;
import com.parshvaa.isccore.activity.library.Library;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.util.List;

/**
 * Created by empiere-vaibhav on 2/27/2018.
 */

public class LibraryChapterAdapter extends RecyclerView.Adapter<LibraryChapterAdapter.MyViewHolder> {

    private List<Library> subjectList;
    public Context context;
    public AQuery aQuery;
    public String SubjectID = "", Subject = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name;
        public ImageView img_sub;
        public View lineview;
        public ProgressBar pBar;


        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.lbl_level_one);
            img_sub = view.findViewById(R.id.img_sub);
            lineview = view.findViewById(R.id.view);

            pBar = view.findViewById(R.id.pBar);
        }
    }


    public LibraryChapterAdapter(List<Library> moviesList, Context context, String subID, String Subject) {
        this.subjectList = moviesList;
        this.context = context;
        aQuery = new AQuery(context);
        this.Subject = Subject;
        this.SubjectID = subID;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subject_revision_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_subject_name.setText(subjectList.get(position).getFolderName());
        Utils.setImage(context, subjectList.get(position).getFolderIcon(), holder.img_sub, R.drawable.sub_english);

        holder.lineview.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(v -> context.startActivity(new Intent(context, LibrarySystemFilesActivity.class)
                .putExtra("FolderID", subjectList.get(position).getFolderID())
                .putExtra("SubjectID", SubjectID)
                .putExtra("Subject", Subject)
                .putExtra("ChapterID", subjectList.get(position).getChapterID())
                .putExtra("Chapter", subjectList.get(position).getFolderName())));

    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }
}




