package com.parshvaa.isccore.activity.iconnect.activity;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.iconnect.adapter.IMcqTestAdapter;
import com.parshvaa.isccore.activity.iconnect.EMcqPaper;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CctActivity extends BaseActivity implements AsynchTaskListner {

    private static final String TAG = "CctActivity";
    public RecyclerView recycler_mcq_sub;
    private IMcqTestAdapter adapter;
    public JsonParserUniversal jParser;
    public ArrayList<Subject> EvaSubjectArray = new ArrayList<>();
    public EMcqPaper emp;
    public ArrayList<EMcqPaper> paperArray = new ArrayList<>();
    public ImageView img_back, img_search;
    public TextView tv_title;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public MyDBManager mDb;
    public ArrayList<String> NOTIFICATION_ID = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_cct);
        Utils.logUser();

        mDb = MyDBManager.getInstance(this);
        mDb.open(this);


        jParser = new JsonParserUniversal();
        mDb.dbQuery("UPDATE notification SET isTypeOpen = 1,isOpen=1 WHERE ModuleType = 'cct'");
        Cursor c = mDb.getAllRows("select android_notification_id from notification where ModuleType = 'cct'");
        if (c != null && c.moveToFirst()) {
            do {
                NOTIFICATION_ID.add(c.getString(c.getColumnIndex("android_notification_id")));
            } while (c.moveToNext());
            c.close();
        }
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        for (int i = 0; i < NOTIFICATION_ID.size(); i++) {
            try {
                int ID = Integer.parseInt(NOTIFICATION_ID.get(i));
                Utils.Log("TAG", "NOTIFICATION_ID :-> " + ID);
                notificationManager.cancel(ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        img_back = findViewById(R.id.img_back);
        img_search = findViewById(R.id.img_search);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("CCT");
        img_back.setOnClickListener(v -> onBackPressed());
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);

        emptyView.setVisibility(View.GONE);
        recycler_mcq_sub = findViewById(R.id.recycler_mcq_sub);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recycler_mcq_sub.addItemDecoration(new ItemOffsetDecoration(spacing));
        //    new CallRequest(CctActivity.this).get_recent_mcq_paper_activity();
        img_search.setOnClickListener(v -> {
            startActivity(new Intent(CctActivity.this, ISearchActivity.class));
            finish();

        });
    }

    private void setupRecyclerView(ArrayList<EMcqPaper> paperArray) {
        final Context context = recycler_mcq_sub.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_mcq_sub.setLayoutAnimation(controller);
        recycler_mcq_sub.scheduleLayoutAnimation();
        recycler_mcq_sub.setLayoutManager(new LinearLayoutManager(context));
        adapter = new IMcqTestAdapter(paperArray, context,"1");
        recycler_mcq_sub.setAdapter(adapter);
        Log.i("TAG", "paperArray :-> " + paperArray.size());

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case get_recent_mcq_paper:
                    Log.d(TAG, "CCt Result: "+ result);

                    paperArray.clear();
                   /* if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }*/
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {

                                for (int i = 0; i < jDataArray.length(); i++) {
                                    JSONObject jpaper = jDataArray.getJSONObject(i);
                                    emp = (EMcqPaper) jParser.parseJson(jpaper, new EMcqPaper());
                                    paperArray.add(emp);
                                }
                                setupRecyclerView(paperArray);
                                showProgress(false, true);

                            }

                        } else {
                            showProgress(false, true);
                            tv_empty.setText(jObj.getString("message"));
                             recycler_mcq_sub.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        tv_empty.setText("message");
                        //recycler_mcq_sub.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        paperArray.clear();
        showProgress(true, true);
        new CallRequest(CctActivity.this).get_recent_mcq_paper_activity();
    }
}
