package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 11/4/2017.
 */

@JsonObject
public class CctReport implements Serializable {
    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    /*SubjectID: "1",
    SubjectName: "English",

    COUNT(CEMCQID): "1"*/
    @JsonField
    public String SubjectID = "";
    @JsonField
    public String SubjectName = "";
    @JsonField
    public String Total = "";
    @JsonField
    public String Acuuracy="";

    public String getAcuuracy() {
        return Acuuracy;
    }

    public void setAcuuracy(String acuuracy) {
        Acuuracy = acuuracy;
    }
}
