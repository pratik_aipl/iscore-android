package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/9/2018.
 */

public class Level implements Serializable {
    public int accuracy = 0, Right_Answer_level = 0, Total_attempt_level = 0;

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getRight_Answer_level() {
        return Right_Answer_level;
    }

    public void setRight_Answer_level(int right_Answer_level) {
        Right_Answer_level = right_Answer_level;
    }

    public int getTotal_attempt_level() {
        return Total_attempt_level;
    }

    public void setTotal_attempt_level(int total_attempt_level) {
        Total_attempt_level = total_attempt_level;
    }


}
