package com.parshvaa.isccore.activity.changestandard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.adapter.MediumAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.MediumModel;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 3/21/2018.
 */

public class MedumActivity extends BaseActivity implements AsynchTaskListner {

    public MedumActivity instance;
    public RecyclerView recycler_board;
    public TextView lbl_title;
    public String[] list_name;
    public FloatingActionButton btn_next, btn_prev;
    public String email;
    public MediumModel mediumModel;
    public ArrayList<MediumModel> mediumArray = new ArrayList<>();
    public ArrayList<String> strMediumArray = new ArrayList<>();
    public MediumAdapter mediumAdapter;
    public App app;
    private LinearLayout emptyView;
    public TextView tv_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_board);
        Utils.logUser();
        instance = this;

        recycler_board = findViewById(R.id.recycler_board);
        lbl_title = findViewById(R.id.lbl_title);
        btn_next = findViewById(R.id.btn_next);
        btn_prev = findViewById(R.id.btn_prev);
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);
        tv_empty.setText("No Medium Available");


        lbl_title.setText("SELECT MEDIUM");

        new CallRequest(MedumActivity.this).getMedium(MainUser.getBoardID());

        btn_next.setOnClickListener(v -> {
            if (Utils.isNetworkAvailable(instance)) {
                if (mediumAdapter != null) {
                    if (TextUtils.isEmpty(mediumAdapter.getSelectedItem())) {
                        Utils.showToast("Please Select Medium", MedumActivity.this);

                    } else {
                        MainUser.setMediumID(mediumAdapter.getSelectedItem());
                        MainUser.setMediumName(mediumAdapter.getMediumNamee());
                        startActivity(new Intent(instance, StandardActivity.class));

                    }
                } else {
                    Utils.showToast(getString(R.string.no_internet_msg), instance);

                }

            }
        });
        btn_prev.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case getMedium:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            emptyView.setVisibility(View.GONE);
                            JSONArray jBoardArray = jObj.getJSONArray("data");
                            if (jBoardArray != null && jBoardArray.length() > 0) {
                                for (int i = 0; i < jBoardArray.length(); i++) {
                                    JSONObject jBoard = jBoardArray.getJSONObject(i);
                                    mediumModel = new MediumModel();
                                    mediumModel.setMediumID(jBoard.getString("MediumID"));
                                    mediumModel.setMediumName(jBoard.getString("MediumName"));
                                    strMediumArray.add(mediumModel.getMediumName());
                                    mediumArray.add(mediumModel);
                                }
                                setupRecyclerView(mediumArray);
                            } else {
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        } else {
                            btn_next.setAlpha(0.5f);
                            btn_next.setClickable(false);
                            recycler_board.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Medium", this);
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private void setupRecyclerView(ArrayList<MediumModel> mediumArray) {
        Context context = recycler_board.getContext();
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);
        recycler_board.setLayoutAnimation(controller);
        recycler_board.scheduleLayoutAnimation();
        recycler_board.setLayoutManager(new LinearLayoutManager(context));
        recycler_board.setAdapter(new MediumAdapter(mediumArray, context));
        mediumAdapter = new MediumAdapter(mediumArray, context);
        recycler_board.setAdapter(mediumAdapter);
    }

}
