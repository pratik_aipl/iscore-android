package com.parshvaa.isccore.activity.iconnect.fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.parshvaa.isccore.activity.iconnect.activity.CctSummaryActivity;
import com.parshvaa.isccore.activity.iconnect.adapter.ICorrectAnswerAdapter;
import com.parshvaa.isccore.observscroll.BaseFragment;
import com.parshvaa.isccore.observscroll.ObservableRecyclerView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.R;

/**
 * Created by empiere-vaibhav on 2/3/2018.
 */

public class IIncorrectFragment extends BaseFragment {
    private LinearLayout emptyView;
    public ObservableRecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (CctSummaryActivity.incorectAnsArray != null && CctSummaryActivity.incorectAnsArray.size() > 0) {

                View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
                Activity parentActivity = getActivity();
                recyclerView = view.findViewById(R.id.scroll);
                recyclerView.setLayoutManager(new LinearLayoutManager(parentActivity));
                recyclerView.setHasFixedSize(false);
                recyclerView.setVisibility(View.VISIBLE);

                recyclerView.setAdapter(new ICorrectAnswerAdapter(getActivity(), CctSummaryActivity.incorectAnsArray));
                recyclerView.setTouchInterceptionViewGroup(view.findViewById(R.id.container));
                if (parentActivity instanceof ObservableScrollViewCallbacks) {
                    recyclerView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
                }
                return view;
            } else {
                View view = inflater.inflate(R.layout.fragment_empty_view, container, false);
                emptyView = view.findViewById(R.id.empty_view);
                emptyView.setVisibility(View.VISIBLE);
                return view;
            }
        }

    }

