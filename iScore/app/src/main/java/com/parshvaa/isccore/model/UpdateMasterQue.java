package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 12/7/2017.
 */

@JsonObject
public class UpdateMasterQue implements Serializable {


    @JsonField
    public String MQuestionID = "", old_question_id = "";

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public String getOld_question_id() {
        return old_question_id;
    }

    public void setOld_question_id(String old_question_id) {
        this.old_question_id = old_question_id;
    }
}
