package com.parshvaa.isccore.model;

/**
 * Created by Karan - Empiere on 2/28/2017.
 */

public class DBfile {
    public String rootpath;
    public String file_array;

    public String getRootpath() {
        return rootpath;
    }

    public void setRootpath(String rootpath) {
        this.rootpath = rootpath;
    }

    public String getFile_array() {
        return file_array;
    }

    public void setFile_array(String file_array) {
        this.file_array = file_array;
    }

    public String getLast_version() {
        return last_version;
    }

    public void setLast_version(String last_version) {
        this.last_version = last_version;
    }

    public String last_version;
}
