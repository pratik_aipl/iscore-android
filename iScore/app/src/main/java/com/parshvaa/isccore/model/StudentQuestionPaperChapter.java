package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/20/2017.
 */

@JsonObject
public class StudentQuestionPaperChapter implements Serializable {


    @JsonField
    public int StudentQuestionPaperChapterID = -1,
               StudentQuestionPaperID ;
    @JsonField
    public String ChapterID = "";

    public int getStudentQuestionPaperChapterID() {
        return StudentQuestionPaperChapterID;
    }

    public void setStudentQuestionPaperChapterID(int studentQuestionPaperChapterID) {
        StudentQuestionPaperChapterID = studentQuestionPaperChapterID;
    }

    public int getStudentQuestionPaperID() {
        return StudentQuestionPaperID;
    }

    public void setStudentQuestionPaperID(int studentQuestionPaperID) {
        StudentQuestionPaperID = studentQuestionPaperID;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }
}
