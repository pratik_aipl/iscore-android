package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Dipesh on 3/10/2017.
 */
@JsonObject
public class ExamTypePattern implements Serializable {
    @JsonField
    public String ExamTypePatternID = "", BoardID = "", MediumID = "", StandardID = "", SubjectID = "", ExamTypeID = "", TotalMarks = "", Duration = "", Instruction = "", PatternNo = "", CreatedBy = "", CreatedOn = "", ModifiedBy = "", ModifiedOn = "";


    public ExamTypePattern() {

    }

    public String getExamTypePatternID() {
        return ExamTypePatternID;
    }

    public void setExamTypePatternID(String examTypePatternID) {
        ExamTypePatternID = examTypePatternID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(String examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }

    public String getPatternNo() {
        return PatternNo;
    }

    public void setPatternNo(String patternNo) {
        PatternNo = patternNo;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
