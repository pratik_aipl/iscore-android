package com.parshvaa.isccore.activity.boardpaper;

import java.io.Serializable;

public class BoardSubject implements Serializable {
    public String SubjectID = "", SubjectName = "";

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }
}