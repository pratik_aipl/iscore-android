package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 5/28/2018.
 */

@JsonObject
public class ClassQuestionPaperSubQuestion implements Serializable {

    @JsonField
    public int ClassQuestionPaperSubQuestionID = -1;
    @JsonField
    public String PaperID = "";
    @JsonField
    public String DetailID = "";
    @JsonField
    public String MPSQID = "";




    public int getClassQuestionPaperSubQuestionID() {
        return ClassQuestionPaperSubQuestionID;
    }

    public void setClassQuestionPaperSubQuestionID(int classQuestionPaperSubQuestionID) {
        ClassQuestionPaperSubQuestionID = classQuestionPaperSubQuestionID;
    }

    public String getPaperID() {
        return PaperID;
    }

    public void setPaperID(String paperID) {
        PaperID = paperID;
    }

    public String getDetailID() {
        return DetailID;
    }

    public void setDetailID(String detailID) {
        DetailID = detailID;
    }

    public String getMPSQID() {
        return MPSQID;
    }

    public void setMPSQID(String MPSQID) {
        this.MPSQID = MPSQID;
    }

}
