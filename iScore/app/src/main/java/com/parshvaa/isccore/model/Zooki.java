package com.parshvaa.isccore.model;

import java.io.Serializable;

public class Zooki implements Serializable {

    public String AdvertisementID ;
    public String StartDate = "";
    public String EndDate = "";
    public String Image = "";
    public String Link = "";
    public String Order = "";
    public String EventID = "";
    public String CreatedOn = "";
    public String CreatedBy = "";

    public String getAdvertisementID() {
        return AdvertisementID;
    }

    public void setAdvertisementID(String advertisementID) {
        AdvertisementID = advertisementID;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getOrder() {
        return Order;
    }

    public void setOrder(String order) {
        Order = order;
    }

    public String getEventID() {
        return EventID;
    }

    public void setEventID(String eventID) {
        EventID = eventID;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }
}
