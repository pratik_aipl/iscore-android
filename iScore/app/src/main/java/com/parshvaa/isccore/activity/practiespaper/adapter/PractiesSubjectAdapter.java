package com.parshvaa.isccore.activity.practiespaper.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.library.videoPleyar.utility.Util;
import com.parshvaa.isccore.activity.practiespaper.activity.PaperTypeActivity;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.referal.ReferalActivity;
import com.parshvaa.isccore.tempmodel.PractiesPaper;
import com.parshvaa.isccore.tempmodel.SubjectAccuracy;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.util.List;

import static com.parshvaa.isccore.App.bannerImage;
import static com.parshvaa.isccore.App.categoryImage;
import static com.parshvaa.isccore.utils.Constant.IMAGEURL;
import static com.parshvaa.isccore.utils.LongThread.TAG;


public class PractiesSubjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int CONTENT = 0;
    private static final int AD = 1;

    AQuery aQuery;
    private List<SubjectAccuracy> subjectList;
    public Context context;
    public FirebaseAnalytics mFirebaseAnalytics;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_lastAttempt, tv_totalTeat;
        public ImageView img_sub;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_lastAttempt = view.findViewById(R.id.tv_lastAttempt);
            tv_totalTeat = view.findViewById(R.id.tv_totalTeat);
            img_sub = view.findViewById(R.id.img_sub);
        }
    }


    public PractiesSubjectAdapter(List<SubjectAccuracy> moviesList, Context context) {
        this.subjectList = moviesList;
        Log.d(TAG, " this.subjectList >>: "+moviesList);
        this.context = context;
        aQuery = new AQuery(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout mBannerView;
        public ImageView mBanner;

        public AdViewHolder(View view) {
            super(view);
            mBannerView = view.findViewById(R.id.mBannerView);
            mBanner = view.findViewById(R.id.mBanner);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == CONTENT) {
            return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.gen_subject_list_row, viewGroup, false));
        } else {
            return new AdViewHolder(LayoutInflater.from(context).inflate(R.layout.banner_raw_item_subject, viewGroup, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (Utils.isNetworkAvailable(context) && !TextUtils.isEmpty(bannerImage) && !TextUtils.isEmpty(categoryImage)) {
            if (position == 2) {
                return AD;
            } else {
                return CONTENT;
            }
        } else {
            return CONTENT;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {
            case 0:
                MyViewHolder holder = (MyViewHolder) holder1;
                int newPos = position;
                if (position > 2 && Utils.isNetworkAvailable(context) && !TextUtils.isEmpty(bannerImage) && !TextUtils.isEmpty(categoryImage)) {
                    newPos = position - 1;
                }
                if (subjectList.size() > newPos) {
                    SubjectAccuracy subjectAccuracy = subjectList.get(newPos);
                    holder.tv_subject_name.setText(subjectAccuracy.getSubjectName());
                    holder.tv_totalTeat.setText(Html.fromHtml("<font color=\"#4e4e4e\">TOTAL TEST : </font><font color=\"#0d4568\">" + subjectAccuracy.getTotalSubjectTest() + "</font>"));
                    if (subjectAccuracy.getLast_Date() == null || subjectAccuracy.getLast_Date().equals(""))
                        holder.tv_lastAttempt.setText(Html.fromHtml("<font color=\"#4e4e4e\">LAST ATTEMPT : </font><font color=\"#0d4568\">N/A</font>"));
                    else {
                        String date = subjectAccuracy.getLast_Date();
                        date = date.substring(0, 10);

                        holder.tv_lastAttempt.setText(Html.fromHtml("<font color=\"#4e4e4e\">LAST ATTEMPT : </font><font color=\"#0d4568\">" + Utils.changeDateToDDMMYYYY(date) + "</font>"));

                    }

                    File imageDir = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/");
                    File image = new File(imageDir.getAbsolutePath() + "/" + subjectAccuracy.getSubjectIcon());
                    String imgUrl = IMAGEURL + "subject_icon/" + subjectAccuracy.getSubjectIcon();
                    if (image.exists()) {
                        Utils.setImageSubject(context, image, holder.img_sub);
                    } else {
                        if (Utils.isNetworkAvailable(context)) {
                            Util.downloadSubjectIcon(context, aQuery, imgUrl, holder.img_sub, image, R.drawable.sub_english);
                        }
                    }
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            App.practiesPaperObj = new PractiesPaper();
                            if (subjectAccuracy.getIsMapped().equals("1")) {
                                App.practiesPaperObj.setSubjectID(subjectAccuracy.getMappedSubjectID());
                            } else {
                                App.practiesPaperObj.setSubjectID(subjectAccuracy.getSubjectID());
                            }

                            App.practiesPaperObj.setIsMapped(subjectAccuracy.getIsMapped());
                            App.practiesPaperObj.setOldSubjectID(subjectAccuracy.getSubjectID());
                            App.practiesPaperObj.setSubjectName(subjectAccuracy.getSubjectName());
                            App.practiesPaperObj.setSubjectIcon(subjectAccuracy.getSubjectIcon());
                            context.startActivity(new Intent(context, PaperTypeActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
                    });
                }

                break;
            case 1:
                AdViewHolder adViewHolder = (AdViewHolder) holder1;
                adViewHolder.mBannerView.setVisibility(View.VISIBLE);
                Utils.setImage(context, bannerImage, adViewHolder.mBanner, R.drawable.warning);

                if (categoryImage.equalsIgnoreCase(Constant.ReferAndEarn)) {
                    adViewHolder.mBannerView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Bundle bundle = new Bundle();
                            mFirebaseAnalytics.logEvent("BANNERCLICK", bundle);
                            context.startActivity(new Intent(context, ReferalActivity.class));
                        }
                    });
                } else if (categoryImage.equalsIgnoreCase(Constant.Subscribe)) {
                    adViewHolder.mBannerView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Bundle bundle = new Bundle();
                            mFirebaseAnalytics.logEvent("BANNERCLICK", bundle);
                            context.startActivity(new Intent(context, ChooseOptionActivity.class));
                        }
                    });
                }
                break;
        }

    }


    @Override
    public int getItemCount() {
        if (Utils.isNetworkAvailable(context) && !TextUtils.isEmpty(bannerImage) && !TextUtils.isEmpty(categoryImage))
            return subjectList.size() + 1;
        else
            return subjectList.size();

    }
}
