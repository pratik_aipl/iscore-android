package com.parshvaa.isccore.activity.practiespaper.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.model.SetPaperQuestionTypes;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;

import static androidx.recyclerview.widget.RecyclerView.ViewHolder;

/**
 * Created by empiere-vaibhav on 1/8/2018.
 */

public class SetPaperTypeAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<SetPaperQuestionTypes> setPaperTypeList;
    public Context context;
    public RecyclerView recyclerView;
    public int lastSelectedPosition = -1;
    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_FOOTER = 1;
    private LayoutInflater mInflater;


    public SetPaperTypeAdapter(ArrayList<SetPaperQuestionTypes> setPaperTypeList, Context context, RecyclerView recyclerView) {
        this.setPaperTypeList = setPaperTypeList;
        this.context = context;
        this.recyclerView = recyclerView;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_set_paaper_raw, parent, false);

            return new ViewHolderiItem(itemView, new CustomEtListener(), new ToAskWatcher(), new ToAnswerWatcher());
        } else
            return new ViewHolderFooter(mInflater.inflate(R.layout.expand_footer, parent, false));
    }

    //return new MyViewHolder(itemView, new CustomEtListener(), new ToAskWatcher(), new ToAnswerWatcher());
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderiItem offerHolder = (ViewHolderiItem) holder;
                bindHeaderItem(offerHolder, position);
                break;
            case 1:
                ViewHolderFooter addrHolder = (ViewHolderFooter) holder;
                bindFooter(addrHolder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).getType().equals("footer")) {
            return VIEW_TYPE_FOOTER;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    public SetPaperQuestionTypes getItem(int position) {

        return setPaperTypeList.get(position);
    }

    class ViewHolderiItem extends ViewHolder {
        public TextView tv_total_que, tv_marks, tv_total_marks;
        public EditText et_to_ask, et_to_ans;
        public CustomEtListener customEtListener;
        public CheckBox chkSelected;
        public ToAskWatcher askWatcher;
        public ToAnswerWatcher answerWatcher;

        public ViewHolderiItem(View view, CustomEtListener customEtListener, ToAskWatcher toAskWatcher, ToAnswerWatcher toAnswerWatcher) {
            super(view);
            this.customEtListener = customEtListener;

            this.askWatcher = toAskWatcher;
            this.answerWatcher = toAnswerWatcher;
            tv_total_que = view.findViewById(R.id.tv_total_que);
            tv_marks = view.findViewById(R.id.tv_marks);
            et_to_ask = view.findViewById(R.id.et_to_ask);
            tv_total_marks = view.findViewById(R.id.tv_total_marks);
            et_to_ans = view.findViewById(R.id.et_to_ans);
            chkSelected = view.findViewById(R.id.chk_que);
            //   et_to_ask.addTextChangedListener(customEtListener);


            askWatcher.ToAskWatcher(et_to_ask,
                    et_to_ans, tv_marks,
                    tv_total_marks, tv_total_que);

            answerWatcher.ToAnswerWatcher(et_to_ask,
                    et_to_ans, tv_marks,
                    tv_total_marks, tv_total_que);

            et_to_ask.addTextChangedListener(askWatcher);
            et_to_ans.addTextChangedListener(answerWatcher);
        }
    }


    public void bindHeaderItem(final ViewHolderiItem holder, final int position) {
        final SetPaperQuestionTypes setPaperType = setPaperTypeList.get(position);

        holder.tv_total_que.setText(setPaperType.getTotalQuestion());
        holder.tv_marks.setText(setPaperType.getMarks());
        holder.chkSelected.setText(setPaperType.getQuestionType());
        holder.chkSelected.setTag(setPaperType);
        holder.askWatcher.updatePosition(position);
        holder.answerWatcher.updatePosition(position);
        if (setPaperType.isSelected == -2) {
            holder.chkSelected.setChecked(true);
            if (setPaperType.getNoOfQuestion() != 0) {
                holder.et_to_ask.setText(setPaperType.getNoOfQuestion() + "");
            } else {
                holder.et_to_ask.setText("");
            }
            if (setPaperType.getNoOfAnswer() != 0) {
                holder.et_to_ans.setText(setPaperType.getNoOfAnswer() + "");
            } else {
                holder.et_to_ans.setText("");
            }

        } else {
            holder.et_to_ask.setText("");
            holder.chkSelected.setChecked(false);
            holder.et_to_ans.setText("");
        }
        //  holder.chkSelected.setChecked(setPaperType.isSelected == -2 ? true : false);
        holder.chkSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                CheckBox cb = (CheckBox) buttonView;
                SetPaperQuestionTypes questionTypes = (SetPaperQuestionTypes) cb.getTag();
                if (cb.isChecked()) {
                    questionTypes.isSelected = -2;
                    holder.et_to_ask.requestFocus();

                    holder.et_to_ask.setEnabled(true);
                    holder.et_to_ans.setEnabled(true);
                    holder.chkSelected.setTextColor(Color.parseColor("#01579B"));
                    holder.chkSelected.setTextSize(19);
                } else {
                    questionTypes.isSelected = -1;
                    holder.et_to_ask.clearFocus();

                    holder.et_to_ask.setEnabled(false);
                    holder.et_to_ans.setEnabled(false);
                    holder.chkSelected.setTextColor(Color.parseColor("#4e4e4e"));
                    holder.chkSelected.setTextSize(17);
                }


                if (isChecked) {
                    holder.chkSelected.setTextColor(Color.parseColor("#01579B"));
                    holder.chkSelected.setTextSize(19);

                } else {
                    holder.chkSelected.setTextColor(Color.parseColor("#4e4e4e"));
                    holder.chkSelected.setTextSize(17);
                }
            }
        });
       /* holder.chkSelected.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                SetPaperQuestionTypes questionTypes = (SetPaperQuestionTypes) cb.getTag();
                if (cb.isChecked()) {
                    questionTypes.isSelected = -2;
                    holder.et_to_ask.requestFocus();

                    holder.et_to_ask.setEnabled(true);
                    holder.et_to_ans.setEnabled(true);
                } else {
                    questionTypes.isSelected = -1;
                    holder.et_to_ask.clearFocus();

                    holder.et_to_ask.setEnabled(false);
                    holder.et_to_ans.setEnabled(false);
                }


            }
        });*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chkSelected.setChecked(true);

            }
        });
    }

    public ArrayList<Integer> getTempsArray() {
        ArrayList<Integer> tempArray = new ArrayList<>();
        SetPaperQuestionTypes types = null;
        for (int i = 0; i < setPaperTypeList.size(); i++) {
            types = setPaperTypeList.get(i);

            if (types.isSelected == -2) {
                tempArray.add(i);

            }
        }
        return tempArray;
    }

    class ViewHolderFooter extends ViewHolder {
        public ViewHolderFooter(View v1) {
            super(v1);
        }
    }

    public void bindFooter(final ViewHolderFooter itemHolder, final int pos) {

    }


    @Override
    public int getItemCount() {
        return setPaperTypeList.size();
    }

    private class CustomEtListener implements TextWatcher {
        private int position;
        public EditText et_to_ask;


        public CustomEtListener() {

        }

        public void CustomEtListener(EditText et_to_ask) {
            this.et_to_ask = et_to_ask;

        }

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence s, int i, int i2, int i3) {
            // Change the value of array according to the position


        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    }

    public ArrayList<SetPaperQuestionTypes> getSelQuestionTypesArray() {

        ArrayList<SetPaperQuestionTypes> array = new ArrayList<>();
        SetPaperQuestionTypes types = null;
        for (int i = 0; i < setPaperTypeList.size(); i++) {
            types = setPaperTypeList.get(i);

            if (types.isSelected == -2) {
                ////Utils.Log("TAG", "In IF -1 isSelected:-> " + types.isSelected);
                if (types.noOfMarks == 0) {
                    ////Utils.Log("TAG", "In IF -2 types.noOfMarks:-> " + types.noOfMarks);
                    if (types.noOfQuestion == 0) {
                        ////Utils.Log("TAG", "In IF -3 types.noOfQuestion :-> " + types.noOfQuestion);
                        recyclerView.getLayoutManager().scrollToPosition(i);

                        try {
                            setToAskAlert(recyclerView.findViewHolderForAdapterPosition(i).itemView, i);
                        } catch (Exception e) {

                            Utils.showToast("Please Enter Questions To ASK ", context);
                            e.printStackTrace();

                        }


                    } else if (types.noOfAnswer == 0) {
                        // Utils.Log("TAG", "In else IF -4 types.noOfAnswer :-> " + types.noOfAnswer);

                        try {

                            setToAnswerAlert(recyclerView.findViewHolderForAdapterPosition(i).itemView, i);
                        } catch (NullPointerException e) {
                            Utils.showToast("Please Enter Questions To Answer ", this.context);
                            recyclerView.getLayoutManager().scrollToPosition(i);
                            e.printStackTrace();

                        }

                    }
                    return null;
                } else {

                    array.add(types);
                    ////Utils.Log("TAG", "In ELSE " + array.size());
                }
            }

        }
        return array;
    }

    public void setToAskAlert(View view, int i) {
        CardView card = (CardView) view;
        RelativeLayout rel = (RelativeLayout) card.getChildAt(0);
        LinearLayout lin_one = (LinearLayout) rel.getChildAt(2);
        LinearLayout lin_two = (LinearLayout) lin_one.getChildAt(0);
        LinearLayout lin_three = (LinearLayout) lin_two.getChildAt(1);

        EditText et = (EditText) lin_three.getChildAt(0);

        //EditText et = (EditText) lin_two.getChildAt(0);
        et.setError("Please Enter Questions To ASK ");
        et.requestFocus();


    }

    public void setToAnswerAlert(View view, int i) {
        CardView card = (CardView) view;
        RelativeLayout rel = (RelativeLayout) card.getChildAt(0);
        LinearLayout lin_one = (LinearLayout) rel.getChildAt(2);
        LinearLayout lin_two = (LinearLayout) lin_one.getChildAt(0);
        LinearLayout lin_three = (LinearLayout) lin_two.getChildAt(2);
        EditText et = (EditText) lin_three.getChildAt(0);
        et.setError("Please Enter Questions To Answer ");
        et.requestFocus();
        recyclerView.getLayoutManager().scrollToPosition(i);
    }


    private class ToAskWatcher implements TextWatcher {


        public TextView tv_marks_show, tv_total_marks_show, tv_total_Ques;
        public EditText et_toAsk, et_answer;

        public SetPaperQuestionTypes questionTypes;
        public int pos;


        public ToAskWatcher() {

        }

        public void ToAskWatcher(EditText et, EditText et2, TextView tv_marks_show,
                                 TextView tv_total_marks_show, TextView tv_total_Ques) {
            this.et_toAsk = et;

            //  this.questionTypes = questionTypes;
            this.et_answer = et2;
            //    this.questionTypes = questionTypesArrayList.get(pos);
            this.tv_marks_show = tv_marks_show;
            this.tv_total_marks_show = tv_total_marks_show;
            this.tv_total_Ques = tv_total_Ques;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void updatePosition(int i) {
            this.pos = i;
            this.questionTypes = setPaperTypeList.get(pos);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                Utils.Log("Position of Watcher :::: ->", pos + "  ::: total Question  ->" + this.questionTypes.getTotalQuestion() + " -> " + s);
                if (s.toString().isEmpty()) {
                    this.et_answer.setText("");
                    this.questionTypes.setNoOfQuestion(0);
                    return;
                } else if (s.toString().equals("0")) {
                    this.et_toAsk.setText("");
                    this.questionTypes.setNoOfQuestion(0);
                } else {
                    int totalQ = Integer.parseInt(this.questionTypes.getTotalQuestion());
                    int q = Integer.parseInt(s.toString());
                    if (q > totalQ) {
                        this.et_toAsk.setText(s.toString().substring(0, s.length() - 1));
                        this.et_toAsk.setSelection(et_toAsk.length());
                        this.questionTypes.setNoOfQuestion(Integer.parseInt(et_toAsk.getText().toString()));
                        Utils.showToast("Should not be more than " + totalQ, context);
                    } else {
                        this.questionTypes.setNoOfQuestion(q);
                    }

                    if (!this.et_answer.getText().toString().equals("")) {
                        int ToAns = Integer.parseInt(this.et_answer.getText().toString());
                        if (ToAns > q) {
                            this.et_answer.setText("");

                        }
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    }


    private class ToAnswerWatcher implements TextWatcher {


        public TextView tv_marks_show, tv_total_marks_show, tv_total_Ques;
        public EditText et_toAsk, et_answer;

        public SetPaperQuestionTypes questionTypes;
        public int pos;

        public ToAnswerWatcher() {

        }

        public void ToAnswerWatcher(EditText et, EditText et2, TextView tv_marks_show,
                                    TextView tv_total_marks_show, TextView tv_total_Ques) {
            this.et_toAsk = et;

            //this.questionTypes = questionTypes;
            this.et_answer = et2;
            this.tv_marks_show = tv_marks_show;
            this.tv_total_marks_show = tv_total_marks_show;
            this.tv_total_Ques = tv_total_Ques;
        }

        public void updatePosition(int i) {
            this.pos = i;
            this.questionTypes = setPaperTypeList.get(pos);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (s.toString().isEmpty()) {
                    this.questionTypes.setNoOfMarks(0);
                    this.questionTypes.setNoOfAnswer(0);
                    this.tv_total_marks_show.setText(String.valueOf(0));
                    return;
                } else if (s.toString().equals("0")) {
                    this.et_answer.setText("");
                    this.questionTypes.setNoOfMarks(0);
                    this.questionTypes.setNoOfAnswer(Integer.parseInt(s.toString()));
                    this.tv_total_marks_show.setText(String.valueOf(0));
                } else {
                    int totalAQ = 0;
                    if (!this.et_toAsk.getText().toString().isEmpty()) {
                        totalAQ = Integer.parseInt(this.et_toAsk.getText().toString());
                    }
                    int q = Integer.parseInt(s.toString());
                    if (q > totalAQ) {
                        this.et_answer.setText(s.toString().substring(0, s.length() - 1));
                        this.et_answer.setSelection(et_answer.length());
                        //  this.questionTypes.setNoOfQuestion(q);
                        Utils.showToast("Should not be more than " + totalAQ, context);
                        s = et_answer.getText().toString();
                        int TotalMarks = 0;

                        TotalMarks = (int) (Double.parseDouble(s.toString()) * Double.parseDouble(this.tv_marks_show.getText().toString()));
                        this.tv_total_marks_show.setText(String.valueOf(TotalMarks));
                        this.questionTypes.setNoOfMarks(TotalMarks);
                        this.questionTypes.setNoOfAnswer(Integer.parseInt(s.toString()));

                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().isEmpty()) {
                return;
            } else if (s.toString().equals("0")) {
                this.et_answer.setText("");
            } else if (this.et_toAsk.getText().toString().equals("")) {
                return;
            } else {
                int TotalMarks = 0;
                int totalAQ = Integer.parseInt(this.et_toAsk.getText().toString());
                int q = Integer.parseInt(s.toString());
                if (!s.toString().isEmpty() && !s.toString().equals("0") && q <= totalAQ) {
                    TotalMarks = (int) (Double.parseDouble(s.toString()) * Double.parseDouble(this.tv_marks_show.getText().toString()));
                    this.tv_total_marks_show.setText(String.valueOf(TotalMarks));
                    questionTypes.setNoOfMarks(TotalMarks);
                    questionTypes.setNoOfAnswer(Integer.parseInt(s.toString()));
                } else {
                    this.tv_total_marks_show.setText("");
                }
            }
        }

    }
}
