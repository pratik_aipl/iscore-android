package com.parshvaa.isccore.activity.boardquestion;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.SubjectAccuracy;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.util.List;

/**
 * Created by empiere-vaibhav on 1/20/2018.
 */

public class BoardSubjectAdapter extends RecyclerView.Adapter<BoardSubjectAdapter.MyViewHolder> {

    private List<Subject> subjectList;
    public Context context;
    SubjectAccuracy subObj;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name;
        public ImageView img_sub;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.lbl_level_one);
            img_sub = view.findViewById(R.id.img_sub);
        }
    }


    public BoardSubjectAdapter(List<Subject> moviesList, Context context) {
        this.subjectList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subject_revision_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_subject_name.setText(subjectList.get(position).getSubjectName());
        File image = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + subjectList.get(position).getSubjectIcon());
        if (image.exists()) {
            Utils.setImage(context,image,holder.img_sub,R.drawable.sub_english);
//            Picasso.with(context).load(image).into(holder.img_sub);
//            try {
//                holder.img_sub.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
//            } catch (NullPointerException e) {
//            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.subObj= subjectList.get(position);

                context.startActivity(new Intent(context, BoardChapterActivity.class)
                        //  .putExtra("suObj", mcqdata)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }
}


