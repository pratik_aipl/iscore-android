package com.parshvaa.isccore.activity.iconnect.adapter;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.model.EvaluterMcqTestMain;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.TestSummary;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.ISccoreWebView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by empiere-vaibhav on 2/3/2018.
 */

public class INotAppearedAnswerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    private LayoutInflater mInflater;
    private ArrayList<EvaluterMcqTestMain> mItems;
    private View mHeaderView;
    McqQuestion mcqQueObj;
    private List<TestSummary> moviesList;
    public boolean isPosition = true;
    public static Context mContext;
    public static boolean isSolution = true;

    public INotAppearedAnswerAdapter(Context context, ArrayList<EvaluterMcqTestMain> items) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public EvaluterMcqTestMain getItem(int position) {

        return mItems.get(position);
    }

    @Override
    public int getItemViewType(int position) {


        return VIEW_TYPE_ITEM;


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(mInflater.inflate(R.layout.custom_correct_question_raw, parent, false));

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {

            case 1:
                ItemViewHolder addrHolder = (ItemViewHolder) holder;
                bindItemHolder(mItems.get(position), addrHolder, position);
                break;
        }

    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView tvQueNO, tv_solution,
                tv_question, tv_a, tv_b, tv_c, tv_d, ans_a, ans_b, ans_c, ans_d, tv_level;
        public ISccoreWebView qWbeView, aWebView, bWebView, cWebView, dWebView;
        public LinearLayout lin_a, lin_b, lin_c, lin_d;
        public LinearLayout lin_solution;

        public ItemViewHolder(View view) {
            super(view);
            tvQueNO = view.findViewById(R.id.tvQueNO);
            tv_question = view.findViewById(R.id.tv_q);
            tv_solution = view.findViewById(R.id.tv_solution);
            lin_solution = view.findViewById(R.id.lin_solution);

            qWbeView = view.findViewById(R.id.qWebView);
            aWebView = view.findViewById(R.id.aWebView);
            bWebView = view.findViewById(R.id.bWebView);
            cWebView = view.findViewById(R.id.cWebView);
            dWebView = view.findViewById(R.id.dWebView);
            lin_a = view.findViewById(R.id.lin_a);
            lin_b = view.findViewById(R.id.lin_b);
            lin_c = view.findViewById(R.id.lin_c);
            lin_d = view.findViewById(R.id.lin_d);
            tv_question = view.findViewById(R.id.tv_q);
            tv_a = view.findViewById(R.id.tv_a);
            tv_b = view.findViewById(R.id.tv_b);
            tv_c = view.findViewById(R.id.tv_c);
            tv_d = view.findViewById(R.id.tv_d);
            ans_a = view.findViewById(R.id.a_ans);
            ans_b = view.findViewById(R.id.b_ans);
            ans_c = view.findViewById(R.id.c_ans);
            ans_d = view.findViewById(R.id.d_ans);
            tv_level = view.findViewById(R.id.tv_level);

            for (int i = 0; i < mItems.size(); i++) {
                try {
                    if (mItems.get(i).optionArray.get(0).getOptions().contains("<img") || mItems.get(i).optionArray.get(0).getOptions().contains("<math") ||
                            mItems.get(i).optionArray.get(0).getOptions().contains("<table")) {
                        aWebView.setVisibility(View.VISIBLE);
                        ans_a.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (mItems.get(i).optionArray.get(1).getOptions().contains("<img") || mItems.get(i).optionArray.get(1).getOptions().contains("<math") ||
                            mItems.get(i).optionArray.get(1).getOptions().contains("<table")) {
                        bWebView.setVisibility(View.VISIBLE);
                        ans_b.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (mItems.get(i).optionArray.get(2).getOptions().contains("<img") || mItems.get(i).optionArray.get(2).getOptions().contains("<math") || mItems.get(i).optionArray.get(2).getOptions().contains("<table")) {
                        cWebView.setVisibility(View.VISIBLE);
                        ans_c.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (mItems.get(i).optionArray.get(3).getOptions().contains("<img") || mItems.get(i).optionArray.get(3).getOptions().contains("<math") || mItems.get(i).optionArray.get(3).getOptions().contains("<table")) {
                        dWebView.setVisibility(View.VISIBLE);
                        ans_d.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    void bindItemHolder(EvaluterMcqTestMain mcqQueObj, final ItemViewHolder itemHolder, int position) {

        itemHolder.tv_solution.setVisibility(View.GONE);

        itemHolder.tvQueNO.setText((position + 1) + "");
        if (mcqQueObj.getQuestion().contains("<img") || mcqQueObj.getQuestion().contains("<math") || mcqQueObj.getQuestion().contains("<table")) {
            itemHolder.qWbeView.setVisibility(View.VISIBLE);
            itemHolder.tv_question.setVisibility(View.GONE);
            itemHolder.qWbeView.loadHtmlFromLocal(mcqQueObj.getQuestion());
        } else {
            itemHolder.tv_question.setText(Html.fromHtml(mcqQueObj.getQuestion()));

        }
        itemHolder.tv_level.setText("(Level " + mcqQueObj.getLevel() + ")");

        itemHolder.tv_a.setBackgroundResource(0);
        itemHolder.tv_b.setBackgroundResource(0);
        itemHolder.tv_c.setBackgroundResource(0);
        itemHolder.tv_d.setBackgroundResource(0);

        isSolution = false;
        itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_line));

        try {
            if (mItems.get(position).optionArray.size() != 0) {
                if (!mItems.get(position).optionArray.get(0).getOptions().isEmpty() && !mItems.get(position).optionArray.get(0).getOptions().equalsIgnoreCase("")) {
                    itemHolder.lin_a.setVisibility(View.VISIBLE);
                    itemHolder.aWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(0).getOptions());
                    itemHolder.ans_a.setText(Html.fromHtml(mItems.get(position).optionArray.get(0).getOptions()).toString().trim());


                    if (mItems.get(position).optionArray.get(0).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {
                        if (mItems.get(position).optionArray.get(0).isRightAns == 1) {
                            itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_right);

                        } else {
                            itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_wrong);

                        }
                    }
                    if (mItems.get(position).optionArray.size() > 0) {
                        if (mItems.get(position).optionArray.get(0).getIsCorrect().equals("1"))
                            itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_right);
                    }
                }
            }

            if (mItems.get(position).optionArray.size() > 1) {
                if (!mItems.get(position).optionArray.get(1).getOptions().isEmpty() && !mItems.get(position).optionArray.get(1).getOptions().equalsIgnoreCase("")) {
                    itemHolder.lin_b.setVisibility(View.VISIBLE);

                    itemHolder.bWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(1).getOptions());

                    itemHolder.ans_b.setText(Html.fromHtml(mItems.get(position).optionArray.get(1).getOptions()).toString().trim());

                    if (mItems.get(position).optionArray.get(1).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {

                        if (mItems.get(position).optionArray.get(1).isRightAns == 1) {
                            itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_right);
                        } else {
                            itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_wrong);
                        }
                    }
                    if (mItems.get(position).optionArray.get(1).getIsCorrect().equalsIgnoreCase("1")) {
                        itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_right);
                    }
                }
            }
            if (mItems.get(position).optionArray.size() > 2) {

                if (!mItems.get(position).optionArray.get(2).getOptions().isEmpty() && !mItems.get(position).optionArray.get(2).getOptions().equalsIgnoreCase("")) {
                    itemHolder.lin_c.setVisibility(View.VISIBLE);

                    itemHolder.cWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(2).getOptions());

                    itemHolder.ans_c.setText(Html.fromHtml(mItems.get(position).optionArray.get(2).getOptions()).toString().trim());

                    if (mItems.get(position).optionArray.get(2).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {

                        if (mItems.get(position).optionArray.get(2).isRightAns == 1) {
                            itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_right);
                        } else {
                            itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_wrong);
                        }
                    }
                    if (mItems.get(position).optionArray.get(2).getIsCorrect().equalsIgnoreCase("1")) {
                        itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_right);
                    }
                }
            }

            if (mItems.get(position).optionArray.size() > 3) {

                if (!mItems.get(position).optionArray.get(3).getOptions().isEmpty() && !mItems.get(position).optionArray.get(3).getOptions().equalsIgnoreCase("")) {
                    itemHolder.lin_d.setVisibility(View.VISIBLE);
                    itemHolder.dWebView.loadHtmlFromLocal(mItems.get(position).optionArray.get(3).getOptions());
                    itemHolder.ans_d.setText(Html.fromHtml(mItems.get(position).optionArray.get(3).getOptions()).toString().trim());
                    if (mItems.get(position).optionArray.get(3).getMCQOPtionID().equalsIgnoreCase(mItems.get(position).selectedAns + "")) {
                        if (mItems.get(position).optionArray.get(3).isRightAns == 1) {
                            itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_right);
                        } else {
                            itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_wrong);
                        }
                    }
                    if (mItems.get(position).optionArray.get(3).isCorrect.equalsIgnoreCase("1")) {
                        itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_right);
                    }

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        itemHolder.tv_solution.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!isSolution) {
                    isSolution = true;

                    itemHolder.lin_solution.setVisibility(View.VISIBLE);
                    itemHolder.tv_solution.setText("    Back    ");
                    itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
                    itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_button));


                } else {
                    isSolution = false;

                    itemHolder.lin_solution.setVisibility(View.GONE);
                    itemHolder.tv_solution.setText("  Solution  ");
                    itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_line));

                }
                // notifyDataSetChanged();
            }
        });
    }


}

