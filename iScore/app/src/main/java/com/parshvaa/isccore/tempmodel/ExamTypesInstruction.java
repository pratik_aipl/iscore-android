package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/20/2018.
 */

public class ExamTypesInstruction implements Serializable {

    public String ExamTypeID;
    public String ExamTypeName;
    public String PaperType;
    public String BoardID;
    public String MediumID;
    public String StandardID;
    public String TotalMarks;
    public String Duration;
    public String Instruction;
    public String CreatedBy;
    public String CreatedOn;
    public String MainInstruction;

    public String getMainInstruction() {
        return MainInstruction;
    }

    public void setMainInstruction(String mainInstruction) {
        MainInstruction = mainInstruction;
    }

    public String getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(String examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getExamTypeName() {
        return ExamTypeName;
    }

    public void setExamTypeName(String examTypeName) {
        ExamTypeName = examTypeName;
    }

    public String getPaperType() {
        return PaperType;
    }

    public void setPaperType(String paperType) {
        PaperType = paperType;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String ModifiedBy;
    public String ModifiedOn;


}
