package com.parshvaa.isccore.observscroll;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.TypedValue;

import com.androidquery.AQuery;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.TableRecords;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.Prefs;
import com.tonyodev.fetch2.Fetch;

public abstract class BaseActivity extends AppCompatActivity {

    public TableRecords tableRecords = null;
    public Prefs prefs;
    public Fetch fetch;
    public Gson gson;
    public AQuery aQuery;
    public MySharedPref mySharedPref;

    ProgressDialog progressDialog;
    public static AlertDialog.Builder alertBuilder;
    public FirebaseAnalytics mFirebaseAnalytics;

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    public void showProgress(boolean isShow, boolean isCancelable) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(isCancelable);
        }
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //android O fix bug orientation
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        aQuery = new AQuery(this);
        fetch = Fetch.Impl.getInstance(App.fetchConfiguration);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        prefs = Prefs.with(this);
        mySharedPref = new MySharedPref(this);
        gson = new Gson();
        tableRecords = gson.fromJson(prefs.getString(Constant.tableRecords, ""), TableRecords.class);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    public static void showInternetPopup(Context context) {

        if (alertBuilder == null) {
            alertBuilder = new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("oops..!")
                    .setCancelable(false)
                    .setMessage("We need internet connection to update your database, kindly switch on internet connection and restart app!")
                    .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertBuilder = null;
                            Intent i = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(i);
                        }
                    });
            alertBuilder.show();
        }

    }

}
