package com.parshvaa.isccore.activity.changestandard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.adapter.BoardAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.BoardModel;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 3/21/2018.
 */

public class BoardActivity extends BaseActivity implements AsynchTaskListner {
    public BoardActivity instance;
    public RecyclerView recycler_board;
    public String[] list_name;
    public TextView lbl_title;
    public FloatingActionButton btn_next, btn_prev;
    public ArrayList<BoardModel> boardArray = new ArrayList<>();
    public ArrayList<String> strBoardArray = new ArrayList<>();
    public BoardModel boardModel;
    public BoardAdapter boardAdapter;
    public String email;
    public App app;
    private LinearLayout emptyView;
    public TextView tv_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_board);
        Utils.logUser();

        instance = this;
        app = App.getInstance();

        recycler_board = findViewById(R.id.recycler_board);
        lbl_title = findViewById(R.id.lbl_title);
        btn_next = findViewById(R.id.btn_next);
        btn_prev = findViewById(R.id.btn_prev);
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);
        tv_empty.setText("No Board Available");

        new CallRequest(BoardActivity.this).getBoard();

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(instance)) {
                    if (boardAdapter != null)
                        if (TextUtils.isEmpty(boardAdapter.getSelectedItem())) {
                            Utils.showToast("Please Select Education Board", BoardActivity.this);

                        } else {
                            MainUser.setBoardID(boardAdapter.getSelectedItem());
                            MainUser.setBoardName(boardAdapter.getBordeename());
                            startActivity(new Intent(instance, MedumActivity.class));
                        }
                } else {
                    Utils.showToast(getString(R.string.no_internet_msg), instance);

                }
            }
        });
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case getBoard:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            strBoardArray = new ArrayList<>();
                            emptyView.setVisibility(View.GONE);
                            JSONArray jBoardArray = jObj.getJSONArray("data");
                            if (jBoardArray != null && jBoardArray.length() > 0) {
                                for (int i = 0; i < jBoardArray.length(); i++) {
                                    JSONObject jBoard = jBoardArray.getJSONObject(i);
                                    boardModel = new BoardModel();
                                    boardModel.setBoardID(jBoard.getString("BoardID"));
                                    boardModel.setBoardName(jBoard.getString("BoardName"));
                                    boardModel.setBoardFullName(jBoard.getString("BoardFullName"));
                                    strBoardArray.add(boardModel.getBoardName() + "(" + boardModel.getBoardFullName() + ")");
                                    boardArray.add(boardModel);
                                }
                                setupRecyclerView(boardArray);
                            } else {
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        } else {
                            btn_next.setAlpha(0.5f);
                            btn_next.setClickable(false);
                            recycler_board.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private void setupRecyclerView(ArrayList<BoardModel> boardArray) {
        final Context context = recycler_board.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);

        recycler_board.setLayoutAnimation(controller);
        recycler_board.scheduleLayoutAnimation();
        recycler_board.setLayoutManager(new LinearLayoutManager(context));
        boardAdapter = new BoardAdapter(boardArray, context);
        recycler_board.setAdapter(boardAdapter);
        //  recycler_board.addItemDecoration(new ItemOffsetDecoration(spacing));
    }
}
