package com.parshvaa.isccore.activity.boardpaper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.BoardPaperDownload;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBConstant;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.MyCustomTypeface;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BoardPaperActivity extends BaseActivity implements AsynchTaskListner {
    public BoardSubject boardSubObj;
    public ArrayList<BoardSubject> boardSubArray = new ArrayList<>();
    public ArrayList<String> strBoardSubArray = new ArrayList<>();
    public JsonParserUniversal jParser;
    int selcted = 0;
    public Spinner spSubject;
    public Button btn_search;
    public String subjectSelected;
    public Subject subjectObj;
    public ArrayList<Subject> subArray = new ArrayList<>();
    public ArrayList<String> strsubArray = new ArrayList<>();
    public ArrayList<BoardPaperPreview> boardPaperArray = new ArrayList<>();
    public BoardPaperPreview boardPaperPreview;
    public BoardPaperAdapter boardAdapter;
    public RecyclerView recycler_board_paper;
    public ImageView img_back;
    public TextView tv_title;
    public BoardPaperActivity instance;
    public MyDBManager mDb;
    private LinearLayout emptyView;
    public TextView tv_empty;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_board_paper);
        Utils.logUser();

        instance = this;
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);
        tv_empty.setText("No Paper Available");
        spSubject = findViewById(R.id.spSubject);
        btn_search = findViewById(R.id.btn_search);
        recycler_board_paper = findViewById(R.id.recycler_board_paper);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Board Paper");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recycler_board_paper.addItemDecoration(new ItemOffsetDecoration(spacing));
        jParser = new JsonParserUniversal();
        showProgress(true, true);
        new CallRequest(instance).get_subject_board();
        subjectObj = new Subject();
        subjectObj.setSubjectName("Select Subject");
        strsubArray.add(subjectObj.getSubjectName());
        subArray.add(subjectObj);

        spSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));


                if (subArray != null) {
                    int pos = spSubject.getSelectedItemPosition();

                    if (pos != 0) {
                        subjectSelected = boardSubArray.get(position).getSubjectID();

                    } else {
                        // Utils.showToast("Not any yeart/month Found", getActivity());
                    }
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spSubject.getSelectedItemPosition() == 0) {
                    Utils.showToast("Select Subject", instance);
                } else {
                    showProgress(true, true);
                    new CallRequest(instance).get_boardpaper(subjectSelected);
                }
            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false, true);
        if (result != null && !result.isEmpty()) {
            try {

                switch (request) {
                    case get_subject_board:
                        JSONObject jObj = new JSONObject(result);
                        boardSubObj = new BoardSubject();
                        boardSubObj.setSubjectName("Select Subject");
                        strBoardSubArray.add(boardSubObj.getSubjectName());
                        boardSubArray.add(boardSubObj);
                        if (jObj.getBoolean("status")) {

                            if (jObj.getJSONArray("boardpaper") != null) {

                                JSONArray jData = jObj.getJSONArray("boardpaper");
                                for (int i = 0; i < jData.length(); i++) {
                                    boardSubObj = (BoardSubject) jParser.parseJson(jData.getJSONObject(i), new BoardSubject());
                                    boardSubArray.add(boardSubObj);
                                    strBoardSubArray.add(boardSubObj.getSubjectName());
                                }
                                spSubject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strBoardSubArray));
                                spSubject.setSelection(selcted);
                            } else {
                                spSubject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strBoardSubArray));
                                spSubject.setSelection(selcted);
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            spSubject.setAdapter(new ArrayAdapter<String>(instance, R.layout.custom_spinner_row, strBoardSubArray));
                            spSubject.setSelection(selcted);
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(instance);
                            builder1.setMessage(jObj.getString("message"));
                            builder1.setCancelable(false);
                            builder1.setTitle("Alert");
                            builder1.setIcon(android.R.drawable.ic_dialog_alert);
                            builder1.setPositiveButton(
                                    "Okay",
                                    (dialog, id) -> onBackPressed());

                            AlertDialog alert11 = builder1.create();
                            if (BoardPaperActivity.this != null)
                                alert11.show();
                        }

                        break;
                    case get_boardpaper:
                        showProgress(false, true);
                        jObj = new JSONObject(result);
                        boardPaperArray.clear();
                        if (jObj.getBoolean("status")) {
                            if (jObj.getJSONArray("boardpaper") != null) {
                                JSONArray jData = jObj.getJSONArray("boardpaper");
                                for (int i = 0; i < jData.length(); i++) {
                                    JSONObject jBoard = jData.getJSONObject(i);
                                    boardPaperPreview = new BoardPaperPreview();
                                    boardPaperPreview.setQFile(jBoard.getString("QFile"));
                                    boardPaperPreview.setQAFile(jBoard.getString("QAFile"));
                                    boardPaperPreview.setPaperName(jBoard.getString("PaperName"));
                                    boardPaperPreview.setAnswerPaperName(jBoard.getString("AnswerPaperName"));

                                    boardPaperPreview.setBoardPaperName(jBoard.getString("BoardPaperName"));
                                    boardPaperPreview.setSubjectName(jBoard.getString("SubjectName"));
                                    boardPaperArray.add(boardPaperPreview);
                                }
                                BoardPaperDownload cpd = new BoardPaperDownload();
                                cpd.setStudentID(MainUser.getStudentId());
                                cpd.setSubjectID(subjectSelected);
                                cpd.setBoardPaperAnswers(boardPaperPreview.getAnswerPaperName());
                                mDb.insertRow(cpd, DBConstant.board_paper_download);

                                if (boardPaperArray != null || boardPaperArray.size() > 0) {
                                    setupRecyclerView(boardPaperArray);
                                    emptyView.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.GONE);
                                } else {
                                    recycler_board_paper.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                }


                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(instance);
                            builder1.setMessage(jObj.getString("message"));
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setupRecyclerView(ArrayList<BoardPaperPreview> boardPaperArray) {
        final Context context = recycler_board_paper.getContext();

        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);
        recycler_board_paper.setLayoutAnimation(controller);
        recycler_board_paper.scheduleLayoutAnimation();
        recycler_board_paper.setLayoutManager(new LinearLayoutManager(context));
        boardAdapter = new BoardPaperAdapter(boardPaperArray, context);
        recycler_board_paper.setAdapter(boardAdapter);


    }
}
