package com.parshvaa.isccore.activity.practiespaper.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.ExamTypesInstruction;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 1/8/2018.
 */

public class ExamTypeAdapter extends RecyclerView.Adapter<ExamTypeAdapter.MyViewHolder> {

    private ArrayList<ExamTypesInstruction> examTypesArray;
    public Context context;
    public int row_index = 0;
    private int lastSelectedPosition = 0;
    public String ExamTypeID = "";
    private RadioButton lastCheckedRB = null;

    public ExamTypeAdapter(ArrayList<ExamTypesInstruction> examTypesArray, Context context) {
        this.examTypesArray = examTypesArray;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_ready_paper_raw, parent, false);
        MyViewHolder viewHolder =
                new MyViewHolder(itemView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ExamTypesInstruction testType = examTypesArray.get(position);

        holder.rb_test_type.setText(testType.getExamTypeName());
        holder.rb_test_type.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    holder.lin_instruction.setVisibility(View.VISIBLE);
                    ExamTypeID = examTypesArray.get(lastSelectedPosition).getExamTypeID();
                } else {
                    holder.lin_instruction.setVisibility(View.GONE);

                }
            }
        });
        holder.tv_instruction.setText(testType.getMainInstruction());
        holder.rb_test_type.setChecked(lastSelectedPosition == position);
        holder.tv_marks.setText("(" + testType.getTotalMarks() + " Marks)");

        if (position == 0) {

        }

    }


    @Override
    public int getItemCount() {
        return examTypesArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RadioButton rb_test_type;
        public LinearLayout lin_instruction;
        public TextView tv_instruction, tv_marks;

        public MyViewHolder(View view) {
            super(view);
            rb_test_type = view.findViewById(R.id.rb_test_type);
            tv_instruction = view.findViewById(R.id.tv_instruction);
            tv_marks = view.findViewById(R.id.tv_marks);
            lin_instruction = view.findViewById(R.id.lin_instruction);
            rb_test_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lin_instruction.setVisibility(View.VISIBLE);
                    lastSelectedPosition = getAdapterPosition();
                    ExamTypeID = examTypesArray.get(lastSelectedPosition).getExamTypeID();
                    notifyDataSetChanged();
                }
            });

        }
    }

    public String getSelectedItem() {
        return ExamTypeID;
    }
}

