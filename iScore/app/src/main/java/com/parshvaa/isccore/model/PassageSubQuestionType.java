package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 5/23/2018.
 */

@JsonObject
public class PassageSubQuestionType implements Serializable {

    @JsonField
    public String PassageSubQuestionTypeID = "", QuestionTypeID = "", Label = "", Mark = "", CreatedBy = "", CreatedOn = "",
            ModifiedBy = "", ModifiedOn = "";

    public String getPassageSubQuestionTypeID() {
        return PassageSubQuestionTypeID;
    }

    public void setPassageSubQuestionTypeID(String passageSubQuestionTypeID) {
        PassageSubQuestionTypeID = passageSubQuestionTypeID;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getMark() {
        return Mark;
    }

    public void setMark(String mark) {
        Mark = mark;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
