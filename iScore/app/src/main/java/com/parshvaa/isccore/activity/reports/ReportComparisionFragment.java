package com.parshvaa.isccore.activity.reports;

import android.database.Cursor;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;


import com.parshvaa.isccore.activity.reports.adapter.SubjectComparisionAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.ComparisionReport;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportComparisionFragment extends Fragment {
    public GridView gridView;

    public CursorParserUniversal cParse;
    public Subject subjectObj;
    public MyDBManager mDb;
    public SubjectComparisionAdapter adapter;

    public View v;
    public ArrayList<ComparisionReport> ComparisionArray = new ArrayList<>();
    public ComparisionReport ComparisionObj;

    public TextView tv_total_mcq, tv_accuracy_mcq, tv_total_cct, tv_accuracy_cct, tv_genratepaper_total;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_comarision, container, false);
        cParse = new CursorParserUniversal();

        mDb = MyDBManager.getInstance(getActivity());
        mDb.open(getActivity());
        ComparisionArray.clear();
        tv_total_mcq = v.findViewById(R.id.tv_total_mcq);
        tv_accuracy_mcq = v.findViewById(R.id.tv_accuracy_mcq);
        tv_total_cct = v.findViewById(R.id.tv_total_cct);
        tv_accuracy_cct = v.findViewById(R.id.tv_accuracy_cct);
        tv_genratepaper_total = v.findViewById(R.id.tv_genratepaper_total);


        tv_total_mcq.setText(App.McqTotal+"");
        tv_accuracy_mcq.setText(App.mcqTotalAccuracy+"%");
        tv_total_cct.setText(App.cctTotal+"");
        tv_accuracy_cct.setText(App.cctTotalAccuracy+"%");
        tv_genratepaper_total.setText(App.PractisePaperTotal+"");


        Cursor c = mDb.getAllRows(DBQueries.getAllSubjects());
        if (c != null && c.moveToFirst()) {
            do {
                ComparisionObj = new ComparisionReport();
                ComparisionObj = (ComparisionReport) cParse.parseCursor(c, new ComparisionReport());
                ComparisionArray.add(ComparisionObj);
            } while (c.moveToNext());
            c.close();
        } else {

        }
        for (ComparisionReport header : ComparisionArray) {
            String hdr = String.valueOf(header.getSubjectID());
            // header.setMcqHdrID(String.valueOf(mDb.getAllReports("select StudentMCQTestHDRID from student_mcq_test_hdr where SubjectID = " + hdr)));
            header.setTotalMcq(String.valueOf(mDb.getAllReports("select * from student_mcq_test_hdr where SubjectID = " + hdr)));
            header.setToatalGenratPaper(String.valueOf(mDb.getAllReports("select * from student_question_paper where SubjectID = " + hdr)));
            header.setMcqHdrID(getTestHeaderID(hdr));
            for (int i = 0; i < App.cctArray.size(); i++) {
                if (App.cctArray.get(i).getSubjectID().equals(hdr)) {
                    Utils.Log("TAG", "SUB ID APP::->" + App.cctArray.get(i).getSubjectName() + "SUb ID " + header.getSubjectName());
                    header.setAccuracy_cct(App.cctArray.get(i).getAcuuracy());
                    header.setTotalCct(App.cctArray.get(i).getTotal());
                }
            }

            int totalAttQue = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(header.getMcqHdrID()));
            int totalRightAns = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(header.getMcqHdrID()));
            header.setAccuracy_MCQ((Utils.getAccuraccy(totalAttQue, totalRightAns)));
        }


        adapter = new SubjectComparisionAdapter(this, ComparisionArray);
        gridView = v.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);

        return v;
    }

    public String getTestHeaderID(String subID) {
        String IDS = "";
        StudentMcqTestHdr stdntHdr;
        Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllSujectviseHdrID(subID));
        if (MCqHdr != null && MCqHdr.moveToFirst()) {
            do {
                stdntHdr = (StudentMcqTestHdr) cParse.parseCursor(MCqHdr, new StudentMcqTestHdr());
                IDS += stdntHdr.getStudentMCQTestHDRID() + ",";
            } while (MCqHdr.moveToNext());
            MCqHdr.close();
        } else {
            //    Utils.showToast("Not any Mcq hader", getActivity());
        }


        return Utils.removeLastComma(IDS);
    }
}
