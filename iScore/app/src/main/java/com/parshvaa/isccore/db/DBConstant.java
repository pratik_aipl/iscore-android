package com.parshvaa.isccore.db;

import android.os.Environment;

import java.io.File;


public class DBConstant {


    public static final int DATABASE_VERSION = 9;
    public static String IMAGE_FOLDER = ".iScore";
    public static final String DB_NAME = "iScoreDbNew.db";
    public static final String DB_OneSignal = "OneSignal.db";
    public static final String DB_FILE = Environment.getExternalStorageDirectory()
            + "/" + DBConstant.IMAGE_FOLDER + File.separator + "databases" + File.separator + DB_NAME;


    public static final String STUDENT_TABLE = "student";
    public static String boards = "boards";
    public static String chapters = "chapters";
    public static String ci_sessions = "ci_sessions";
    public static String classes = "classes";
    public static String examtypes = "examtypes";
    public static String login_attempts = "login_attempts";
    public static String masterquestion = "masterquestion";
    public static String mediums = "mediums";
    public static String question_types = "question_types";
    public static String standards = "standards";
    public static String subjects = "subjects";
    public static String users = "users";
    public static String user_autologin = "user_autologin";
    public static String user_profiles = "user_profiles";
    public static String examtype_subject = "examtype_subject";
    public static String mcqquestion = "mcqquestion";
    public static String mcqoption = "mcqoption";
    public static String board_paper_files = "board_paper_files";
    public static String boards_paper_data = "boards_paper_data";
    public static String boards_paper = "boards_paper";
    public static String temp_mcq_test_hdr = "temp_mcq_test_hdr";
    public static String temp_mcq_test_dtl = "temp_mcq_test_dtl";
    public static String temp_mcq_test_chapter = "temp_mcq_test_chapter";
    public static String student_mcq_test_hdr = "student_mcq_test_hdr";
    public static String student_mcq_test_dtl = "student_mcq_test_dtl";
    public static String student_mcq_test_chapter = "student_mcq_test_chapter";
    public static String exam_type_pattern = "exam_type_pattern";
    public static String paper_type = "paper_type";
    public static String exam_type_pattern_detail = "exam_type_pattern_detail";
    public static String student_question_paper = "student_question_paper";
    public static String student_question_paper_chapter = "student_question_paper_chapter";
    public static String student_question_paper_detail = "student_question_paper_detail";

    public static String board_paper_download = "board_paper_download";
    public static String student_set_paper_question_type = "student_set_paper_question_type";
    public static String student_set_paper_detail = "student_set_paper_detail";
    public static String notification = "notification";
    public static String class_evaluater_mcq_test_temp = "class_evaluater_mcq_test_temp";
    public static String class_evaluater_cct_count = "class_evaluater_cct_count";
    public static String updated_masterquetion = "updated_masterquetion";

    public static String temp_mcq_test_level = "temp_mcq_test_level";
    public static String student_mcq_test_level = "student_mcq_test_level";
    public static String zooki = "zooki";
    public static String student_incorrect_question = "student_incorrect_question";
    public static String student_not_appeared_question = "student_not_appeared_question";
    public static String class_question_paper_sub_question = "class_question_paper_sub_question";
    public static String master_passage_sub_question = "master_passage_sub_question";
    public static String passage_sub_question_type = "passage_sub_question_type";
    public static String sub_question_types = "sub_question_types";
}
