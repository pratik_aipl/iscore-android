package com.parshvaa.isccore.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.parshvaa.isccore.model.BoardModel;
import com.parshvaa.isccore.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 12/25/2017.
 */

public class BoardAdapter extends RecyclerView.Adapter<BoardAdapter.ViewHolder> {
    private ArrayList<BoardModel> boardArrayList;
    private Context context;

    private int lastSelectedPosition = -1;
    public String boardId = "";
    public String boardNamee = "";

    public BoardAdapter(ArrayList<BoardModel> boardList, Context ctx) {
        boardArrayList = boardList;
        context = ctx;
    }

    @Override
    public BoardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.board_item, parent, false);

        BoardAdapter.ViewHolder viewHolder =
                new BoardAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BoardAdapter.ViewHolder holder,
                                 int position) {
        BoardModel boardObj = boardArrayList.get(position);
        holder.selectionState.setText(boardObj.getBoardFullName() + " (" + boardObj.getBoardName() + ")");

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
        holder.selectionState.setChecked(lastSelectedPosition == position);

    }

    @Override
    public int getItemCount() {
        return boardArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView boardName;
        public RadioButton selectionState;

        public ViewHolder(View view) {
            super(view);
            selectionState = view.findViewById(R.id.redio_board);
            selectionState.setOnClickListener(v -> {
                lastSelectedPosition = getAdapterPosition();
                boardId = boardArrayList.get(lastSelectedPosition).getBoardID();
                boardNamee = boardArrayList.get(lastSelectedPosition).getBoardFullName() + "(" + boardArrayList.get(lastSelectedPosition).getBoardName() + ")";
                notifyDataSetChanged();
            });

        }
    }

    public String getSelectedItem() {
        return boardId;
    }

    public String getBordeename() {
        return boardNamee;
    }
}