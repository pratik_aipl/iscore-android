package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by admin on 2/28/2017.
 */
@JsonObject
public class ExamTypes implements Serializable {

    @JsonField
    public String ExamTypeID;
    @JsonField
    public String ExamTypeName;
    @JsonField
    public String PaperType;
    @JsonField
    public String BoardID;
    @JsonField
    public String MediumID;
    @JsonField
    public String StandardID;
    @JsonField
    public String TotalMarks;
    @JsonField
    public String Duration;
    @JsonField
    public String Instruction;
    @JsonField
    public String CreatedBy;
    @JsonField
    public String CreatedOn;
    @JsonField
    public String ModifiedBy;
    @JsonField
    public String ModifiedOn;

    public ExamTypes() {

    }

    public ExamTypes(String ExamTypeID, String ExamTypeName, String PaperType, String BoardID,
                     String MediumID, String StandaradID, String TotalMarks, String Duration,
                     String Instruction, String CreatedBy,
                     String CreatedOn, String ModifiedBy, String ModifiedOn) {

        this.ExamTypeID = ExamTypeID;
        this.ExamTypeName = ExamTypeName;
        this.PaperType = PaperType;
        this.BoardID = BoardID;
        this.MediumID = MediumID;
        this.StandardID = StandaradID;
        this.TotalMarks = TotalMarks;
        this.Duration = Duration;
        this.Instruction = Instruction;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;

    }

    public String getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(String examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getExamTypeName() {
        return ExamTypeName;
    }

    public void setExamTypeName(String examTypeName) {
        ExamTypeName = examTypeName;
    }

    public String getPaperType() {
        return PaperType;
    }

    public void setPaperType(String paperType) {
        PaperType = paperType;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
