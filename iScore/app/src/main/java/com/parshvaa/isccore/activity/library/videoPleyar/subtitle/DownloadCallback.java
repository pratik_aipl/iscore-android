package com.parshvaa.isccore.activity.library.videoPleyar.subtitle;

import java.io.File;

public interface DownloadCallback {
    void onDownload(File file);
    void onFail(Exception e);
}
