package com.parshvaa.isccore.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

public class IsccoreWebActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_isccore_web);
        Utils.logUser();


        WebView webView= findViewById(R.id.aWebView);
        webView.loadUrl("file:///android_asset/loader.html");

    }


}
