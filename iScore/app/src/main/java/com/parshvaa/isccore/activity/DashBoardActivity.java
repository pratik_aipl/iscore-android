package com.parshvaa.isccore.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.boardpaper.BoardPaperActivity;
import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.event.EventListActivity;
import com.parshvaa.isccore.activity.iconnect.activity.IDashboardActivity;
import com.parshvaa.isccore.activity.library.videoPleyar.utility.Util;
import com.parshvaa.isccore.activity.mcq.SubjectMcqActivity;
import com.parshvaa.isccore.activity.moderatepaper.ModeratePaperActivity;
import com.parshvaa.isccore.activity.practiespaper.activity.PractiesSubjectActivity;
import com.parshvaa.isccore.activity.profile.MyProfileActivity;
import com.parshvaa.isccore.activity.referal.ReferalActivity;
import com.parshvaa.isccore.activity.reports.ReportsActivity;
import com.parshvaa.isccore.activity.revisionnote.activity.RevisionNoteSubjectActivity;
import com.parshvaa.isccore.activity.searchpaper.SearchPaperActivity;
import com.parshvaa.isccore.activity.sureshot.SureShotActivity;
import com.parshvaa.isccore.activity.textbook.TextBookSubjectActivity;
import com.parshvaa.isccore.activity.weeklydise.WeeklyDiseActivity;
import com.parshvaa.isccore.adapter.ZookiAdapter;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.custominterface.OnTaskCompletedDashboard;
import com.parshvaa.isccore.db.DBConstant;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.db.MyDBManagerOneSignal;
import com.parshvaa.isccore.model.BoardPaperDownload;
import com.parshvaa.isccore.model.KeyData;
import com.parshvaa.isccore.model.MainData;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.NotificationOneSignal;
import com.parshvaa.isccore.model.NotificationType;
import com.parshvaa.isccore.model.StudentIncorrectQuestion;
import com.parshvaa.isccore.model.StudentMcqTestChap;
import com.parshvaa.isccore.model.StudentMcqTestDtl;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.model.StudentMcqTestLevel;
import com.parshvaa.isccore.model.StudentQuestionPaper;
import com.parshvaa.isccore.model.StudentQuestionPaperChapter;
import com.parshvaa.isccore.model.StudentQuestionPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperQuestionType;
import com.parshvaa.isccore.model.StudentnotAppearednQuestion;
import com.parshvaa.isccore.model.Zooki;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.ObservableScrollView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.observscroll.ViewHelper;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.FragmentDrawer;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.MessageEvent;
import com.parshvaa.isccore.utils.TableReload;
import com.parshvaa.isccore.utils.Utils;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.RetryPolicy;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.parshvaa.isccore.App.bannerImage;
import static com.parshvaa.isccore.App.categoryImage;

public class DashBoardActivity extends BaseActivity implements ObservableScrollViewCallbacks, OnTaskCompletedDashboard, FragmentDrawer.FragmentDrawerListener, AsynchTaskListner {
    private static final String TAG = "DashBoardActivity";
    public static boolean isAlive = false;
    public static App app;
    public JsonParserUniversal jParser;
    public String PleyaerID = "";
    private View mFlexibleSpaceView, mToolbarView, mFab;
    private int mFlexibleSpaceHeight, flexibleSpaceAndToolbarHeight,
            mFabMargin, mActionBarSize, mFlexibleSpaceImageHeight;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    public boolean zookiIsFirstTie = false;
    public ImageView drawerButton;
    ImageView mRefreshZooki;
    ProgressBar mProgressZooki;

    private TextView mTitleView, btn_subscrib, tv_userName, tv_visit_site, tv_studID,
            tv_medium, tv_board, tv_standard, tv_licence_status;
    Button mExport, mRemoveAll, mLogout, mReferFriend;
    public LinearLayout lin_text_book,lin_mcq, lin_practies_paper, mBannerView, lin_indtitute_connect, lin_my_report,lin_event, lin_board_question, lin_board_paper, lin_search_paper, lin_revision_tool, lin_sure_shot, lin_moderate_paper;
    ImageView mBanner;
    public RecyclerView rvZooki;
    public ObservableScrollView scrollView;
    private FragmentDrawer drawerFragment;
    public RecyclerView.LayoutManager mLayoutManager1;
    public CircleImageView fab_profile;
    public RelativeLayout mainLay;
    IntentFilter intentFilter;
    public static MyDBManager mDb;
    public static StudentMcqTestHdr StudentHdrObj;
    public static StudentMcqTestChap StudentChapObj;
    public static StudentMcqTestDtl mcqTestDtl;
    public static StudentMcqTestLevel mcqTestLevel;
    public static StudentnotAppearednQuestion notAppeardObj;
    public static StudentIncorrectQuestion incorrectObj;
    public Zooki zooki;
    public ArrayList<Zooki> zookiList = new ArrayList<>();
    public static ArrayList<StudentMcqTestHdr> StuHdrArray = new ArrayList<>();
    public ArrayList<StudentQuestionPaper> StuQuePaperArray = new ArrayList<>();
    public ArrayList<BoardPaperDownload> BoardpaperArray = new ArrayList<>();
    public static ArrayList<StudentnotAppearednQuestion> StuNotAppearArray = new ArrayList<>();
    public static ArrayList<StudentIncorrectQuestion> StuIncorrectArray = new ArrayList<>();
    public static LinkedHashMap<String, String> mapImages = new LinkedHashMap<>();
    public static JsonArray jStudQuePaperArray;
    public static JsonArray jStudHdrArray;
    public static JsonArray jBoardPaperArray;
    public static JsonArray jIncorrect;
    public static JsonArray jNotAppeared;

    public static JSONArray ParseArrayMCQ, ParseArrayPracties, ParseArrayBoard;
    public ZookiAdapter zookiAdapter;
    public RetryPolicy retryPolicy;

    public static ArrayList<NotificationOneSignal> OneSignalArray = new ArrayList<>();
    public MyDBManagerOneSignal mDbOneSignal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        Utils.logUser();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);

        mDbOneSignal = MyDBManagerOneSignal.getInstance(this);
        mDbOneSignal.open(this);

        retryPolicy = new DefaultRetryPolicy();
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.CONNECTIVITY_ACTION);
        PleyaerID = Utils.OneSignalPlearID();
        Log.d(TAG, "Dashboard: "+PleyaerID);

        if (Utils.isNetworkAvailable(this)){
            //new CallRequest(DashBoardActivity.this).check_login_user(PleyaerID);
            new CallRequest(this).getDashPopupData();

        }
        DataSherdToAppClass();
        FindElements();
        ScrollXYPotion();

  /*      if (mySharedPref.getIsFirstTime()) {
            if (!mySharedPref.getlblTarget().isEmpty()) {
                App.Target = mySharedPref.getTarget();
            } else {
                //alertSetTarget();
            }
        } else {
            mySharedPref.setIsFirstTime(true);
            //alertSetTarget();
        }
*/
        Bundle bundle = new Bundle();
        mFirebaseAnalytics.logEvent("DASHBOARD", bundle);
    }


    //-------------------  Data Shard To App Class  ------------------- //
    public void DataSherdToAppClass() {
        App.mainUser = new MainUser();
        MainUser.setFirstName(mySharedPref.getFirstName());
        MainUser.setLastName(mySharedPref.getLastName());
        MainUser.setRegKey(mySharedPref.getReg_key());
        MainUser.setGuid(mySharedPref.getGuuid());
        MainUser.setClassID(mySharedPref.getClassID());
        MainUser.setBoardID(mySharedPref.getBoardID());
        MainUser.setMediumID(mySharedPref.getMediumID());
        MainUser.setStandardID(mySharedPref.getStandardID());
        MainUser.setEmailID(mySharedPref.getEmailID());
        MainUser.setRegMobNo(mySharedPref.getRegMobNo());
        MainUser.setSubjects(mySharedPref.getSubjects());
        MainUser.setStudentId(mySharedPref.getStudentID());
        MainUser.setStanderdName(mySharedPref.getStandardName());
        MainUser.setClassName(mySharedPref.getClassName());
        MainUser.setKeyStatus(mySharedPref.getKeyStatus());
        MainUser.setBranchID(mySharedPref.getBranchID());
        MainUser.setBatchID(mySharedPref.getBatchID());
        MainUser.setSchoolName(mySharedPref.getSchoolName());
        MainUser.setVersion(mySharedPref.getVersion());
        MainUser.setBoardName(mySharedPref.getBoardName());
        MainUser.setMediumName(mySharedPref.getMediumName());
        MainUser.setStudentCode(mySharedPref.getStudentCode());
    }

    //-------------------  Cast Variables  ------------------- //

    public void FindElements() {

        mBannerView = findViewById(R.id.mBannerView);
        mBanner = findViewById(R.id.mBanner);
        mTitleView = findViewById(R.id.title);
        tv_userName = findViewById(R.id.tv_userName);
        fab_profile = findViewById(R.id.img_profile);
        tv_studID = findViewById(R.id.tv_studID);
        tv_medium = findViewById(R.id.tv_medium);
        tv_board = findViewById(R.id.tv_board);
        tv_standard = findViewById(R.id.tv_standard);
        tv_visit_site = findViewById(R.id.tv_visit_site);
        tv_licence_status = findViewById(R.id.tv_licence_status);
        rvZooki = findViewById(R.id.rvRelated);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        drawerButton = findViewById(R.id.drawerButton);
        mReferFriend = findViewById(R.id.mReferFriend);
        mExport = findViewById(R.id.mExport);
        mRemoveAll = findViewById(R.id.mRemoveAll);
        mLogout = findViewById(R.id.mLogout);

        mToolbarView = findViewById(R.id.toolbar);
        mainLay = findViewById(R.id.mainLay);
        drawerFragment = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        scrollView = findViewById(R.id.scroll);
        findViewById(R.id.body).setPadding(0, getResources().getDimensionPixelSize(R.dimen._20sdp), 0, 0);
        mFab = findViewById(R.id.fab);
        lin_mcq = findViewById(R.id.lin_mcq);
        lin_text_book = findViewById(R.id.lin_text_book);
        lin_practies_paper = findViewById(R.id.lin_practies_paper);
        lin_board_paper = findViewById(R.id.lin_board_paper);
        lin_revision_tool = findViewById(R.id.lin_revision_tool);
        lin_search_paper = findViewById(R.id.lin_search_paper);
        lin_indtitute_connect = findViewById(R.id.lin_indtitute_connect);
        lin_board_question = findViewById(R.id.lin_board_question);
        lin_my_report = findViewById(R.id.lin_my_report);
        lin_event = findViewById(R.id.lin_event);
        lin_moderate_paper = findViewById(R.id.lin_moderate_paper);
        lin_sure_shot = findViewById(R.id.lin_sure_shot);

        mRefreshZooki = findViewById(R.id.mRefreshZooki);
        mProgressZooki = findViewById(R.id.mProgressZooki);

        mProgressZooki.setVisibility(View.VISIBLE);
        mRefreshZooki.setVisibility(View.GONE);


        lin_sure_shot.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, SureShotActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        lin_moderate_paper.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, ModeratePaperActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        btn_subscrib = findViewById(R.id.btn_subscrib);

        if (BuildConfig.DEBUG) {
            mLogout.setVisibility(View.VISIBLE);
            mExport.setOnClickListener(view -> {
                File sd = Environment.getExternalStorageDirectory();
                File data = Environment.getDataDirectory();
                FileChannel source = null;
                FileChannel destination = null;
                String currentDBPath = "/data/" + getPackageName() + "/databases/" + DBConstant.DB_NAME;
                String backupDBPath = DBConstant.DB_NAME;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);
                try {
                    source = new FileInputStream(currentDB).getChannel();
                    destination = new FileOutputStream(backupDB).getChannel();
                    destination.transferFrom(source, 0, source.size());
                    source.close();
                    destination.close();
                    Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            mRemoveAll.setOnClickListener(view -> {
                Utils.deleteAllTableRecord(mDb);
            });
            mLogout.setOnClickListener(view -> {
                trimCache(DashBoardActivity.this);
            });
        } else {
            mExport.setVisibility(View.GONE);
        }


        mReferFriend.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            mFirebaseAnalytics.logEvent("REFERANDEARN", bundle);
            startActivity(new Intent(DashBoardActivity.this, ReferalActivity.class));
        });

        if (mySharedPref.getVersion().equals("0")) {
            tv_licence_status.setText("Licence : Purchased");
            btn_subscrib.setVisibility(View.GONE);
        } else {
            tv_licence_status.setText("Licence : Demo");
            btn_subscrib.setVisibility(View.VISIBLE);
            btn_subscrib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(DashBoardActivity.this, ChooseOptionActivity.class));
                }
            });
        }

//onesourcewebs.com/iScore-New-APK/isccore app.docx
        lin_mcq.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, SubjectMcqActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        lin_practies_paper.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, PractiesSubjectActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        lin_board_paper.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, WeeklyDiseActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        lin_revision_tool.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, RevisionNoteSubjectActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        lin_text_book.setOnClickListener(v ->
                startActivity(new Intent(DashBoardActivity.this, TextBookSubjectActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        lin_my_report.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, ReportsActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
        lin_event.setOnClickListener(v -> {
            showProgress(true, false);
            new CallRequest(this).ClassEventCheck();
        });
        lin_search_paper.setOnClickListener(v -> startActivity(new Intent(DashBoardActivity.this, SearchPaperActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));


        lin_board_question.setOnClickListener(v -> {

            if (mDb != null && mDb.isDbOpen()) {
//                if (mDb.getTableCount(Constant.subjects)>0){
//                    startActivity(new Intent(DashBoardActivity.this, BoardSubjectActivity.class)
//                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    startActivity(new Intent(DashBoardActivity.this, BoardPaperActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                }

            }else{
                Toast.makeText(this, "Subject Not Found", Toast.LENGTH_SHORT).show();
            }

        });
        lin_indtitute_connect.setOnClickListener(v -> {
            if (Utils.isNetworkAvailable(DashBoardActivity.this)) {
                showProgress(true, false);
                new CallRequest(this).CheckClass();

            } else {
                Utils.showToast(getResources().getString(R.string.conect_internet), DashBoardActivity.this);
            }
        });

        tv_userName.setText(MainUser.getFirstName() + " " + MainUser.getLastName());
        tv_board.setText(MainUser.getBoardName());
        tv_medium.setText(MainUser.getMediumName() + " Medium");
        tv_studID.setText(MainUser.getStudentCode());
        tv_standard.setText("CLASS " + MainUser.getStanderdName());

        Log.d(TAG, "FindElements: " + mySharedPref.getClassLogo());
        Log.d(TAG, "FindElements: " + mySharedPref.getProf_image());
        Log.d(TAG, "FindElements: " + mySharedPref.getClassRoundLogo());


        setImageLogo();

        SyncData();
        if (Utils.isNetworkAvailable(this)) {
            new CallRequest(this).getTableRecords();
        }

        mRefreshZooki.setOnClickListener(view -> {
            mProgressZooki.setVisibility(View.VISIBLE);
            mRefreshZooki.setVisibility(View.GONE);
            zookiList.clear();
            SyncData();
        });


    }

    private void setImageLogo() {
        String ClassLogoName = mySharedPref.getClassRoundLogo().substring(mySharedPref.getClassRoundLogo().lastIndexOf("/") + 1);
        File imageDirClassLogoName = new File(Constant.LOCAL_IMAGE_PATH + "/class/");
        File imageFile = new File(imageDirClassLogoName.getAbsolutePath() + "/" + ClassLogoName);
        if (!TextUtils.isEmpty(ClassLogoName)) {
            if (imageFile.exists()) {
                Utils.setImage(this, imageFile, fab_profile, R.drawable.profile);
            } else {
                if (Utils.isNetworkAvailable(this)) {
                    Util.downloadClassIcon(this, aQuery, mySharedPref.getClassRoundLogo(), fab_profile, imageFile, R.drawable.profile);
                } else {
                    fab_profile.setImageResource(R.drawable.profile);
                }
            }
        } else {
            fab_profile.setImageResource(R.drawable.profile);
        }
    }

    public void trimCache(Context context) {
        try {
            Utils.deleteAllTableRecord(mDb);
            mySharedPref.setIsLoggedIn(false);
            mySharedPref.setDownloadStatus("y");
            mySharedPref.setIsProperLogin("false");
            File dir = context.getCacheDir();
            deleteDir(dir);
            Intent i = new Intent(this, SplashActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else {
            return false;
        }
    }

    public void SyncData() {
        if (Utils.isNetworkAvailable(this)) {
            new CallRequest(this).get_zooki();
        } else {
            RecyclerViewZooki();
        }
    }

    //-------------------  Set Target  ------------------- //
    public void alertSetTarget() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        AlertDialog alertTarge = dialogBuilder.create();
        LayoutInflater inflater = getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.alert_set_goal, null);
        alertTarge.setView(alertDialogView);
        alertTarge.setCancelable(false);
        EditText et_target = alertDialogView.findViewById(R.id.et_target);
        EditText et_previous_goal = alertDialogView.findViewById(R.id.et_previous_goal);
        if (!mySharedPref.getlblTarget().equals("") || mySharedPref.getlblTarget() != null) {
            et_target.setText(mySharedPref.getlblTarget());
        }
        if (!mySharedPref.getlblPrviousTarget().equals("") || mySharedPref.getlblPrviousTarget() != null) {
            et_previous_goal.setText(mySharedPref.getlblPrviousTarget());
        }
        et_target.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {

                    String n = et_target.getText().toString();
                    if (n.length() > 0) {
                        int number = Integer.parseInt(n);
                        if (number > 100) {
                            et_target.setText(n.substring(0, n.length() - 1));
                            Utils.showToast("Target should be only Lessthen or equal 100", DashBoardActivity.this);
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_previous_goal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {

                    String n = et_previous_goal.getText().toString();

                    if (n.length() > 0) {
                        int number = Integer.parseInt(n);
                        if (number > 100) {
                            et_previous_goal.setText(n.substring(0, n.length() - 1));
                            Utils.showToast("Target should be only Lessthen or equal 100", DashBoardActivity.this);
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ImageView rel_img = alertDialogView.findViewById(R.id.rel_submit);
        rel_img.setOnClickListener(view -> {
            if (et_target.getText().toString().equals("")) {
                et_target.setError("Please Enter Target");
            } else if (et_previous_goal.getText().toString().equals("")) {
                et_previous_goal.setError("Please Enter Target");
            } else {
                App.Target = et_target.getText().toString();
                mySharedPref.setlblTarget(et_target.getText().toString());
                mySharedPref.setlblPrviousTarget(et_previous_goal.getText().toString());
                MainUser.setPreviousYearScore(mySharedPref.getlblPrviousTarget());

                mySharedPref.setIsFirstTime(true);
                if (Utils.isNetworkAvailable(DashBoardActivity.this)) {
                    new CallRequest(DashBoardActivity.this).upadateProfile(MainUser.getFirstName(), MainUser.getLastName(), TextUtils.isEmpty(MainUser.getSchoolName()) ? "" : MainUser.getSchoolName(),
                            et_target.getText().toString(),
                            et_previous_goal.getText().toString(),
                            "", PleyaerID);
                }
                Utils.hideKeyboard(DashBoardActivity.this, et_target);
                alertTarge.dismiss();
            }
        });

        alertTarge.show();
    }

    /*Show Dashboard PopUp*/
    public void SowDashPopUp(String title, String desc, String img,String Type) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        AlertDialog popalertTarge = dialogBuilder.create();
        LayoutInflater inflater = getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.show_dash_popup, null);
        popalertTarge.setView(alertDialogView);
        popalertTarge.setCancelable(false);
        Log.d(TAG, "imagepath: "+img);

        ImageView img_close = alertDialogView.findViewById(R.id.img_pop_close);
        ImageView img_dashpop = alertDialogView.findViewById(R.id.img_dashpop);

        TextView tv_tittle = alertDialogView.findViewById(R.id.tv_tittle);
        TextView tv_pop_description = alertDialogView.findViewById(R.id.tv_pop_description);

        Button rel_img = alertDialogView.findViewById(R.id.rel_pop_submit);

        tv_tittle.setText(title);
        tv_pop_description.setText(desc);
        Glide.with(this)
                .load(img)
                .placeholder(R.drawable.iscore_final_logo)
                .into(img_dashpop);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popalertTarge.dismiss();
            }
        });

        rel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Type.equalsIgnoreCase(Constant.ReferAndEarn)) {
                    startActivity(new Intent(DashBoardActivity.this, ReferalActivity.class));
                } else if (Type.equalsIgnoreCase(Constant.Subscribe)) {
                    startActivity(new Intent(DashBoardActivity.this, ChooseOptionActivity.class));
                } else if (Type.equalsIgnoreCase("Event")) {
                    startActivity(new Intent(DashBoardActivity.this, EventListActivity.class));
                }
                popalertTarge.dismiss();
            }
        });


        popalertTarge.show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        new CallRequest(this).getFlashData();
        new CallRequest(this).getFlashSubjectData();

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onBackPressed() {
        if (drawerFragment.mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerFragment.mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setMessage("Do you want to exit iSccore App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", null)
                .show();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
    //-------------------  Side Panel  Start -------------------//

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {

        switch (position) {
            case 0:

                break;
            case 1:
                startActivity(new Intent(DashBoardActivity.this, MyProfileActivity.class));

                break;
            case 2:
                startActivity(new Intent(DashBoardActivity.this, QueryCallUsActivity.class));
                //startActivity(new Intent(DashBoardActivity.this, IsccoreWebActivity.class));
                break;

            case 3:
                startActivity(new Intent(DashBoardActivity.this, ReferalActivity.class));
                break;

            case 4:
                launchMarket();
                break;

            case 5:
                startActivity(new Intent(DashBoardActivity.this, LegalActivity.class));
                break;

            case 6:
                startActivity(new Intent(DashBoardActivity.this, ChooseOptionActivity.class));
                break;
            case 7:
                startActivity(new Intent(DashBoardActivity.this, NotificationActivity.class));
                break;

            default:
                break;
        }

    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Utils.Log("TAG url ::--> ", String.valueOf(uri));
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    public void ReferFriend() {
        //"market://details?id=" + getPackageName()
        Uri uri = Uri.parse("http://bit.ly/iScore_App");

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "iSocre");
        i.putExtra(Intent.EXTRA_TEXT, "Terrified of Tests? Be confident with iSccore! Get unlimited set of tests with model answers for Maharashtra Board Std 8-10. Install NOW " + uri);
        startActivity(Intent.createChooser(i, "Share via"));
    }

    @Override
    public void onGetRecorComplite() {
        getMCQData();
    }

    @Override
    public void onInsertRecorComplite() {
//        pDialog.dismiss();
       /* if (PleyaerID != null || !PleyaerID.equals("")) {
            new CallRequest(DashBoardActivity.this).check_login_token_silent(PleyaerID);
        }*/

    }

    //-------------------  Scrooling  Start -------------------//
    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        updateFlexibleSpaceText(scrollY);

    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    public void ScrollXYPotion() {

        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._130sdp);
        mFabMargin = getResources().getDimensionPixelSize(R.dimen._17sdp);
        mActionBarSize = getActionBarSize();
        flexibleSpaceAndToolbarHeight = mFlexibleSpaceHeight + getActionBarSize();
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen._70sdp);


        mTitleView.setText("Dashboard");
        ViewHelper.setScaleX(mFab, 0);
        ViewHelper.setScaleY(mFab, 0);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, findViewById(R.id.drawer_layout), drawerButton, mainLay);
        drawerFragment.setDrawerListener(this);
        scrollView.setScrollViewCallbacks(this);
        mFlexibleSpaceView.getLayoutParams().height = flexibleSpaceAndToolbarHeight;
        Log.d(TAG, "ScrollXYPotion: " + mFabMargin);
        ScrollUtils.addOnGlobalLayoutListener(mFab, new Runnable() {
            @Override
            public void run() {
                updateFlexibleSpaceText(scrollView.getCurrentScrollY());
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void updateFlexibleSpaceText(final int scrollY) {

        ViewHelper.setTranslationY(mFlexibleSpaceView, -scrollY);
        int adjustedScrollY = (int) ScrollUtils.getFloat(scrollY, 0, mFlexibleSpaceHeight);
        float maxScale = (float) (mFlexibleSpaceHeight - mToolbarView.getHeight()) / mToolbarView.getHeight();
        float scale = maxScale * ((float) mFlexibleSpaceHeight - adjustedScrollY) / mFlexibleSpaceHeight;

        Log.d(TAG, "updateFlexibleSpaceText: " + adjustedScrollY);
        if (!BuildConfig.DEBUG)
            if (adjustedScrollY < 100) {
                mReferFriend.setVisibility(View.VISIBLE);
            } else {
                mReferFriend.setVisibility(View.GONE);
            }
        ViewHelper.setPivotX(mFab, 0);
        ViewHelper.setPivotY(mFab, 0);
        ViewHelper.setScaleX(mFab, 1 + scale);
        ViewHelper.setScaleY(mFab, 1 + scale);

        int maxTitleTranslationX = mToolbarView.getWidth() - mFab.getWidth() - 10;
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 180);
        int maxFabTranslationY = mFlexibleSpaceImageHeight - mFab.getHeight() / 2;
        float fabTranslationY = ScrollUtils.getFloat(-scrollY + mFlexibleSpaceImageHeight - mFab.getHeight() / 2, mActionBarSize - mFab.getHeight() / 2, maxFabTranslationY);
        float fabTranslationX = ScrollUtils.getFloat((int) ((scrollY - scale) * ((scale + 1.5))), mFabMargin, maxTitleTranslationX);

        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFab.getLayoutParams();
            lp.leftMargin = mFlexibleSpaceView.getWidth() - mFabMargin;
            lp.topMargin = (int) fabTranslationY;

            mFab.requestLayout();
        } else {
            ViewHelper.setTranslationX(mFab, fabTranslationX);
            ViewHelper.setTranslationY(mFab, fabTranslationY);
        }

    }

    //-------------------  Scrooling  END-------------------//

    //-------------------  Zooki   Start-------------------//

    public void RecyclerViewZooki() {
        /*try {
            zookiList = (ArrayList) new CustomDatabaseQuery(this, new Zooki()).execute(new String[]{DBNewQuery.zooki()}).get();
            Zooki obj = new Zooki();
            obj.setTitle("View More");
            zookiList.add(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvZooki.setLayoutManager(mLayoutManager1);
        rvZooki.setHasFixedSize(true);
        if (!zookiIsFirstTie) {
            zookiIsFirstTie = true;
            final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
            rvZooki.addItemDecoration(new ItemOffsetDecoration(spacing));
        }
        RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                rvZooki.removeOnScrollListener(this);

                rvZooki.addOnScrollListener(this);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        rvZooki.addOnScrollListener(scrollListener);
        zookiAdapter = new ZookiAdapter(DashBoardActivity.this, zookiList);
        rvZooki.setAdapter(zookiAdapter);
        tv_visit_site.setOnClickListener(view ->
                startActivity(new Intent(DashBoardActivity.this, ZookiWebActivity.class)));
        if (Utils.isNetworkAvailable(DashBoardActivity.this)) {
            new getRecorrd(DashBoardActivity.this).execute();
        }
    }
    //-------------------  Zooki  END-------------------//

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        mProgressZooki.setVisibility(View.GONE);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case upadateProfile:
                        break;
                        case ClassEventCheck:
                            showProgress(false, true);
                            Log.d(TAG, "onTaskCompleted: User Details " + result);
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                startActivity(new Intent(DashBoardActivity.this, EventListActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            }else{
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        break;
                            case CheckClass:
                            showProgress(false, true);
                            Log.d(TAG, "onTaskCompleted: User Details " + result);
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                     startActivity(new Intent(DashBoardActivity.this, IDashboardActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            }else{
                                new AlertDialog.Builder(this)
                                        .setTitle("")
                                        .setMessage(jObj.getString("message"))
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .setCancelable(false)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }
                        break;
                    case getStudentDetails:
                        Log.d(TAG, "onTaskCompleted: User Details " + result);
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");
                            MainUser.setStudentId(jData.getString("StudentID"));
                            MainUser.setFirstName(jData.getString("FirstName"));
                            MainUser.setLastName(jData.getString("LastName"));
                            MainUser.setGuid(jData.getString("IMEINo"));
                            MainUser.setEmailID(jData.getString("EmailID"));
                            MainUser.setRegMobNo(jData.getString("RegMobNo"));
                            MainUser.setSchoolName(jData.getString("SchoolName"));
                            MainUser.setProfile_image(jData.getString("ProfileImage"));
                            MainUser.setStudentCode(jData.getString("StudentCode"));

                            mySharedPref.setStudent_id(MainUser.getStudentId());
                            mySharedPref.setFirst_name(MainUser.getFirstName());
                            mySharedPref.setLast_name(MainUser.getLastName());
                            mySharedPref.setGuuid(MainUser.getGuid());
                            mySharedPref.setEmailID(MainUser.getEmailID());
                            mySharedPref.setRegMobNo(MainUser.getRegMobNo());
                            mySharedPref.setStudentCode(MainUser.getStudentCode());
                            mySharedPref.setProfileImage(MainUser.getProfile_image());
                            mySharedPref.setVersioCode(String.valueOf(BuildConfig.VERSION_CODE));
                            KeyData keydata;
                            JSONArray jsonArray = jData.getJSONArray("KeyData");
                            String checkUser = "";
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jKey = jsonArray.getJSONObject(i);
                                keydata = LoganSquare.parse(jKey.toString(), KeyData.class);
                                MainUser.keyDataArray.add(keydata);

                                MainUser.setRegKey(MainUser.keyDataArray.get(i).getStudentKey());
                                MainUser.setPreviousYearScore(MainUser.keyDataArray.get(i).getPreviousYearScore());

                                mySharedPref.setStudent_id(MainUser.keyDataArray.get(i).getStudentID());
                                mySharedPref.setBoardID(MainUser.keyDataArray.get(i).getBoardID());
                                mySharedPref.setMediumID(MainUser.keyDataArray.get(i).getMediumID());
                                mySharedPref.setStandardID(MainUser.keyDataArray.get(i).getStandardID());
                                mySharedPref.setClassID(MainUser.keyDataArray.get(i).getClassID());
                                mySharedPref.setBranchID(MainUser.keyDataArray.get(i).getBranchID());
                                mySharedPref.setBatchID(MainUser.keyDataArray.get(i).getBatchID());
                                mySharedPref.setReg_key(MainUser.keyDataArray.get(i).getStudentKey());
                                checkUser = MainUser.keyDataArray.get(i).getIsDemo();
                                mySharedPref.setStandardName(MainUser.keyDataArray.get(i).getStandardName());
                                mySharedPref.setClassName(MainUser.keyDataArray.get(i).getClassName());
                                mySharedPref.setBoardName(MainUser.keyDataArray.get(i).getBoardName());
                                mySharedPref.setMediumName(MainUser.keyDataArray.get(i).getMediumName());
                                mySharedPref.setClassLogo(MainUser.keyDataArray.get(i).getClassLogo());
                                mySharedPref.setClassLogo2(MainUser.keyDataArray.get(i).getSplashLogo());
                                mySharedPref.setClassRoundLogo(MainUser.keyDataArray.get(i).getClassRoundLogo());
                                mySharedPref.setCreatedOn(MainUser.keyDataArray.get(i).getCreatedOn());
                                mySharedPref.setlblTarget(MainUser.keyDataArray.get(i).getSetTarget());
                                mySharedPref.setlblPrviousTarget(MainUser.keyDataArray.get(i).getPreviousYearScore());

                            }
                            setImageLogo();
                            if (!checkUser.equalsIgnoreCase(mySharedPref.getVersion())) {
                                new AlertDialog.Builder(this)
                                        .setTitle("")
                                        .setMessage("Your iSccore Product has upgrade, Please login again")
                                        .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                Utils.deleteAllTableRecord(mDb);
                                                trimCache(DashBoardActivity.this);
                                            }
                                        })
                                        .setCancelable(false)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }

                        } else {
                            showProgress(false, true);
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                    case getDashPopup:
                       jObj = new JSONObject(result);
                        Log.d(TAG, "getDashPopup: "+result);
                    /*{"status":true,"message":"","data":[{"Title":"mkn","Desc":"acnk","Type":"2",
                            "Category":"Subscribe","Image":"8686cf6829e44b8bad38ee8df74e7181.png"}],
                        "path":"https:\/\/evaluater.parshvaa.com\/banner_images\/app"}*/
                         if (jObj.getBoolean("status")) {

                             JSONArray jsonArray = jObj.getJSONArray(Constant.data);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                             SowDashPopUp(jsonObject.getString("Title"),jsonObject.getString("Desc"),
                                     jObj.getString("path")+"/"+jsonObject.getString("Image"),jsonObject.getString("Category"));

                        }
                        break;

                        case getFlashBanner:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jsonArray = jObj.getJSONArray(Constant.data);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            if (jsonObject.has(Constant.Image) && !TextUtils.isEmpty(jsonObject.getString(Constant.Image))) {
                                mBannerView.setVisibility(View.VISIBLE);
                                Log.d(TAG, "onTaskCompleted: " + jsonObject.getString(Constant.Category));
                                mBannerView.setOnClickListener(view -> {
                                    Bundle bundle = new Bundle();
                                    mFirebaseAnalytics.logEvent("BANNERCLICK", bundle);
                                    try {
                                        if (jsonObject.getString(Constant.Category).equalsIgnoreCase(Constant.ReferAndEarn)) {
                                            startActivity(new Intent(DashBoardActivity.this, ReferalActivity.class));
                                        } else if (jsonObject.getString(Constant.Category).equalsIgnoreCase(Constant.Subscribe)) {
                                            startActivity(new Intent(DashBoardActivity.this, ChooseOptionActivity.class));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                });
                                Utils.setImage(DashBoardActivity.this, jObj.getString(Constant.path) + "/" + jsonObject.getString(Constant.Image), mBanner, R.drawable.warning);
                            } else
                                mBannerView.setVisibility(View.GONE);

                        } else {
                            mBannerView.setVisibility(View.GONE);
                        }
                        break;
                    case getFlashBannerSubject:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jsonArray = jObj.getJSONArray(Constant.data);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            if (jsonObject.has(Constant.Image) && !TextUtils.isEmpty(jsonObject.getString(Constant.Image))) {
                                categoryImage = jsonObject.getString(Constant.Category);
                                bannerImage = jObj.getString(Constant.path) + "/" + jsonObject.getString(Constant.Image);
                            } else
                                bannerImage = "";
                        } else {
                            bannerImage = "";
                        }
                        break;
                    case get_zooki:
                        mRefreshZooki.setVisibility(View.VISIBLE);
                        showProgress(false, true);
                        jObj = new JSONObject(result);
                        Log.d(TAG, "onTaskCompleted: " + jObj.toString());
                        if (jObj.getBoolean("status")) {
                           /* if (mDb.isDbOpen())
                                mDb.emptyTable("zooki");*/
                            //String folderPath = jObj.getString("folder_path");
                            JSONArray jData = jObj.getJSONArray("data");
                            if (jData != null) {
                                for (int i = 0; i < jData.length(); i++) {
                                    zooki = (Zooki) jParser.parseJson(jData.getJSONObject(i), new Zooki());
                                    //mDb.insertRow(zooki, "zooki");
                                    //String url = folderPath + zooki.getImage();
                                   // String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/zooki/" + zooki.getImage();
                               /*     File imageFile = new File(SDCardPath);
                                    if (Utils.isNetworkAvailable(DashBoardActivity.this)) {
                                        File dir = new File(imageFile.getParent());
                                        if (!dir.exists()) {
                                            dir.mkdirs();
                                        }
                                        downloadFile(url, SDCardPath);
                                    }*/
                                    zookiList.add(zooki);
                                }
                                RecyclerViewZooki();
                            }
                        } else {
                            if (Utils.isNetworkAvailable(DashBoardActivity.this)) {
                                new getRecorrd(DashBoardActivity.this).execute();
                            }
                        }
                        break;
                     /*case check_login_token:
                       jObj = new JSONObject(result);
                        if (!jObj.getBoolean("status")) {
                            Utils.deleteAllTableRecord(mDb);
                            App.deleteCache(DashBoardActivity.this);
                            mySharedPref.clearApp();
                            Intent intent = new Intent(DashBoardActivity.this, WelcomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        break;*/
                      /*  case check_login_user:
                        jObj = new JSONObject(result);
                            Log.d(TAG, "check_login_token: "+result);
                            *//*FALSE=Old USER,TRUE=New USER*//*
                            if (!jObj.getBoolean("status")) {
                            Utils.deleteAllTableRecord(mDb);
                            App.deleteCache(DashBoardActivity.this);
                            mySharedPref.clearApp();
                            Intent intent = new Intent(DashBoardActivity.this, WelcomeActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            new CallRequest(this).getStudentDetails();
                        }
                        break;*/
                    case getTableRecords:
                        showProgress(false, true);
                        Log.d(TAG, "getTableRecords: "+result.toString());
                        try {
                            MainData mainData = LoganSquare.parse(result, MainData.class);
                            if (mainData.isStatus()) {
                                prefs.save(Constant.tableRecords, gson.toJson(mainData.getData()));
                                tableRecords = mainData.getData();
                                Log.d(TAG, "tableRecords: "+ gson.toJson(tableRecords));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case sendMcqData:
                        showProgress(false, true);
                        jStudHdrArray = null;
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("MCQLastSync")) {
                                String lastMCQSyncTime = jObj.getString("MCQLastSync");
                                mySharedPref.setLastMCQlastSync(lastMCQSyncTime);
                                mySharedPref.setLastSyncTime(lastMCQSyncTime);
                            }
                            if (jObj.has("data") && jObj.getJSONArray("data") != null) {
                                JSONArray jData = jObj.getJSONArray("data");
                                ParseArrayMCQ = jData;
                            }
                        }
                        getNotAppeared();
                        break;

                    case sendNotAppearedQuestion:
                        showProgress(false, true);
                        jNotAppeared = null;
                        getIncorrect();
                        break;
                    case sendIncorrectQuestion:
                        jIncorrect = null;
                        showProgress(false, true);
                        getGeneratePaperData();
                        break;
                    case sendGanreatPaper:
                        showProgress(false, true);
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("GeneratePaperLastSync")) {
                                String lastGeneratePaperSyncTime = jObj.getString("GeneratePaperLastSync");
                                mySharedPref.setLastGeneratePaperlastSync(lastGeneratePaperSyncTime);
                                mySharedPref.setLastSyncTime(lastGeneratePaperSyncTime);
                            }
                            if (jObj.has("data") && jObj.getJSONArray("data") != null) {
                                JSONArray jData = jObj.getJSONArray("data");
                                ParseArrayPracties = jData;
                            }

                        }
                        getBoardPaperData();
                        break;
                    case sendBoardPaperDownload:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("BoardPaperLastSync")) {
                                String setLastBoardPaperDownloadSyncTime = jObj.getString("BoardPaperLastSync");
                                mySharedPref.setLastBoardPaperDownloadSyncTime(setLastBoardPaperDownloadSyncTime);
                                mySharedPref.setLastSyncTime(setLastBoardPaperDownloadSyncTime);
                            }
                            if (jObj.has("data") && jObj.getJSONArray("data") != null) {
                                JSONArray jData = jObj.getJSONArray("data");
                                ParseArrayBoard = jData;
                            }
                        }
                        new insertRecorrd(DashBoardActivity.this).execute();
                        break;
                }
            } catch (Exception e) {
                showProgress(false, true);
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        Utils.closeDb(mDb);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        if (fetch != null && !fetch.isClosed())
            fetch.close();
        super.onDestroy();
        App.deleteCache(this);
    }

    private void downloadFile(String url, String path) {
        File qLocalFile = new File(path);
        Request request = null;
        if (!qLocalFile.exists()) {
            request = new Request(url, path);
            request.setPriority(Priority.HIGH);
            request.setNetworkType(NetworkType.ALL);
            fetch.enqueue(request, updatedRequest -> {
                Log.d(TAG, "manageImagesDownload: ");
            }, error -> {
                Toast.makeText(this, "error enqueue download!!!", Toast.LENGTH_SHORT).show();
            });

        }

    }

    //----------mcq Start-------------//

    public void getMCQData() {
        if (jStudHdrArray != null && jStudHdrArray.size() > 0) {
            new CallRequest(DashBoardActivity.this).sendMcqDataLogin(jStudHdrArray.toString());
        } else {
            new CallRequest(DashBoardActivity.this).sendMcqDataLogin("[]");
        }
    }

    public void getNotAppeared() {
        if (jNotAppeared != null && jNotAppeared.size() > 0)
            new CallRequest(DashBoardActivity.this).sendNotAppearedQuestion(jNotAppeared.toString());
    }

    public void getIncorrect() {
        if (jIncorrect != null && jIncorrect.size() > 0) {
            //  showProgress(true, true);
            new CallRequest(DashBoardActivity.this).sendIncorrectQuestion(jIncorrect.toString());
        }

    }

    public void getGeneratePaperData() {
        //   showProgress(true, true);
        if (jStudQuePaperArray != null && jStudQuePaperArray.size() > 0) {
            new CallRequest(DashBoardActivity.this).sendGanreatPaperLogin(String.valueOf(jStudQuePaperArray));
        } else {
            new CallRequest(DashBoardActivity.this).sendGanreatPaperLogin("[]");
        }
    }

    public void getBoardPaperData() {
        if (jBoardPaperArray != null && jBoardPaperArray.size() > 0) {
            new CallRequest(DashBoardActivity.this).sendBoardPaperDownloadLogin(String.valueOf(jBoardPaperArray));
        } else {
            new CallRequest(DashBoardActivity.this).sendBoardPaperDownloadLogin("[]");
        }
    }

    //-----------------------------------------
    public void StudentQuePaperDetail(StudentQuestionPaper header, String hdr, CursorParserUniversal cParse) {
        if (mDb.isDbOpen()) {
            Cursor dtl = mDb.getAllRows(DBQueries.getStudentQuePaperDetail(hdr));
            if (dtl != null && dtl.moveToFirst()) {
                do {
                    StudentQuestionPaperDetail dtlObj = (StudentQuestionPaperDetail) cParse.parseCursor(dtl, new StudentQuestionPaperDetail());
                    header.detailArray.add(dtlObj);
                } while (dtl.moveToNext());
                dtl.close();
            }
        }


    }

    public void StudentQuePaperChap(StudentQuestionPaper header, String hdr, CursorParserUniversal cParse) {
        if (mDb.isDbOpen()) {
            Cursor chap = mDb.getAllRows(DBQueries.getStudentQuePaperChap(hdr));
            if (chap != null && chap.moveToFirst()) {
                do {
                    StudentQuestionPaperChapter chapObj = (StudentQuestionPaperChapter) cParse.parseCursor(chap, new StudentQuestionPaperChapter());
                    header.chapterArray.add(chapObj);
                } while (chap.moveToNext());
                chap.close();
            }
        }
        ////Utils.Log("Ganrate Paper ::-->", String.valueOf(jStudQuePaperArray));
    }

    //------------------- Data  GET and  Parse  End-----------------------//
    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Utils.Log("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Utils.Log("TAG " + tag + " -->", str);
        }
    }

    public class getRecorrd extends AsyncTask<String, String, String> {
        Context context;
        private MyDBManager mDb;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public OnTaskCompletedDashboard listener;

        private getRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);
            this.listener = (OnTaskCompletedDashboard) context;

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            StuHdrArray.clear();
            jStudHdrArray = null;
            StuNotAppearArray.clear();
            jNotAppeared = null;
            StuIncorrectArray.clear();
            jIncorrect = null;
            StuQuePaperArray.clear();
            jStudQuePaperArray = null;
            BoardpaperArray.clear();
            jBoardPaperArray = null;
            OneSignalArray.clear();
        }

        @Override
        protected String doInBackground(String... id) {
            if (mDb.isTableExists(Constant.notification)) {
                int N_ID = mDb.getLastRecordIDForRequest("notification", "OneSignalID");
                Log.d(TAG, "doInBackground: " + N_ID);
                if (N_ID != -1) {
                    Cursor noti = mDbOneSignal.getAllRows(DBNewQuery.getNotificatonDash(N_ID));
                    if (noti != null && noti.moveToFirst()) {
                        Log.d(TAG, "doInBackground: " + noti.getCount());
                        do {
                            NotificationOneSignal notiObj = (NotificationOneSignal) cParse.parseCursor(noti, new NotificationOneSignal());
                            OneSignalArray.add(notiObj);
                        } while (noti.moveToNext());
                        noti.close();
                    } else {
                        if (noti != null)
                            noti.close();
                        Log.d(TAG, "doInBackground: cursor null or blank");
                    }
                }
            }
            //------------------------ get mcq Record Start-----------------------//

            Cursor MCQhdr = mDb.getAllRows(DBQueries.getAllStudentHdr(mySharedPref.getLastMCQSyncTime()));
            if (MCQhdr != null && MCQhdr.moveToFirst()) {
                do {
                    StudentHdrObj = (StudentMcqTestHdr) cParse.parseCursor(MCQhdr, new StudentMcqTestHdr());
                    StuHdrArray.add(StudentHdrObj);
                } while (MCQhdr.moveToNext());
                MCQhdr.close();
            }

            for (StudentMcqTestHdr header : StuHdrArray) {
                String hdr = String.valueOf(header.getStudentMCQTestHDRID());

                Cursor MCQchap = mDb.getAllRows(DBQueries.getStudentMcqChap(hdr));
                if (MCQchap != null && MCQchap.moveToFirst()) {
                    do {
                        StudentChapObj = (StudentMcqTestChap) cParse.parseCursor(MCQchap, new StudentMcqTestChap());
                        header.chapterArray.add(StudentChapObj);
                    } while (MCQchap.moveToNext());
                    MCQchap.close();
                }
                Cursor MCQdetl = mDb.getAllRows(DBQueries.getStudentMcqChap(hdr));
                if (MCQdetl != null && MCQdetl.moveToFirst()) {
                    do {
                        mcqTestDtl = (StudentMcqTestDtl) cParse.parseCursor(MCQdetl, new StudentMcqTestDtl());
                        header.detailArray.add(mcqTestDtl);
                    } while (MCQdetl.moveToNext());
                    MCQdetl.close();
                }
                Cursor MCQlvl = mDb.getAllRows(DBQueries.getStudentMcqChap(hdr));
                if (MCQlvl != null && MCQlvl.moveToFirst()) {
                    do {
                        mcqTestLevel = (StudentMcqTestLevel) cParse.parseCursor(MCQlvl, new StudentMcqTestLevel());
                        header.levelArray.add(mcqTestLevel);
                    } while (MCQlvl.moveToNext());
                    MCQlvl.close();
                }

            }
            Gson gson = new GsonBuilder().create();
            jStudHdrArray = gson.toJsonTree(StuHdrArray).getAsJsonArray();


            Cursor notAppeard = mDb.getAllRows(DBQueries.getNotAppeared());
            if (notAppeard != null && notAppeard.moveToFirst()) {
                do {
                    notAppeardObj = (StudentnotAppearednQuestion) cParse.parseCursor(notAppeard, new StudentnotAppearednQuestion());
                    StuNotAppearArray.add(notAppeardObj);
                } while (notAppeard.moveToNext());
                notAppeard.close();
            }

            gson = new GsonBuilder().create();
            if (StuNotAppearArray != null)
                jNotAppeared = gson.toJsonTree(StuNotAppearArray).getAsJsonArray();


            Cursor incorrect = mDb.getAllRows(DBQueries.getIncorrect());
            if (incorrect != null && incorrect.moveToFirst()) {
                do {
                    incorrectObj = (StudentIncorrectQuestion) cParse.parseCursor(incorrect, new StudentIncorrectQuestion());
                    StuIncorrectArray.add(incorrectObj);
                } while (incorrect.moveToNext());
                incorrect.close();
            }

            gson = new GsonBuilder().create();
            if (StuIncorrectArray != null)
                jIncorrect = gson.toJsonTree(StuIncorrectArray).getAsJsonArray();

            //------------------------ get mcq Record ENd-----------------------//

            //------------------------ get Practies Paper Record Start-----------------------//


            Cursor paper = mDb.getAllRows(DBQueries.getAllStudentQuestionPaper(mySharedPref.getLastGeneratePaperSyncTime()));
            if (paper != null && paper.moveToFirst()) {
                do {
                    StudentQuestionPaper paperObj = (StudentQuestionPaper) cParse.parseCursor(paper, new StudentQuestionPaper());
                    StuQuePaperArray.add(paperObj);
                } while (paper.moveToNext());
                paper.close();
            }


            for (StudentQuestionPaper header : StuQuePaperArray) {
                String hdr = String.valueOf(header.getStudentQuestionPaperID());


                if (header.getPaperTypeID().equals("1") || header.getPaperTypeID().equals(1)) {

                    StudentQuePaperDetail(header, hdr, cParse);

                } else if (header.getPaperTypeID().equals("2") || header.getPaperTypeID().equals(2)) {

                    StudentQuePaperChap(header, hdr, cParse);
                    StudentQuePaperDetail(header, hdr, cParse);
                } else {
                    // paper type id 3

                    StudentQuePaperChap(header, hdr, cParse);

                    Cursor Qtype = mDb.getAllRows(DBQueries.getStudentSetPaperQueType(hdr));
                    if (Qtype != null && Qtype.moveToFirst()) {
                        do {
                            StudentSetPaperQuestionType typeObj = (StudentSetPaperQuestionType) cParse.parseCursor(Qtype, new StudentSetPaperQuestionType());
                            header.queTypeArray.add(typeObj);
                        } while (Qtype.moveToNext());
                        Qtype.close();
                    }


                    for (StudentSetPaperQuestionType Quetypeid : header.queTypeArray) {
                        String dtl_id = String.valueOf(Quetypeid.getStudentSetPaperQuestionTypeID());

                        Cursor dtl = mDb.getAllRows(DBQueries.getStudentSetPaperDtl(dtl_id));
                        if (dtl != null && dtl.moveToFirst()) {
                            do {
                                StudentSetPaperDetail typeObj = (StudentSetPaperDetail) cParse.parseCursor(dtl, new StudentSetPaperDetail());
                                Quetypeid.setdetailArray.add(typeObj);
                            } while (dtl.moveToNext());
                            dtl.close();
                        }

                    }
                }
            }
            gson = new GsonBuilder().create();
            jStudQuePaperArray = gson.toJsonTree(StuQuePaperArray).getAsJsonArray();


            //------------------------ get Practies Paper Record ENd-----------------------//

            //------------------------ get Board Paper Record Start-----------------------//

            Cursor board = mDb.getAllRows(DBQueries.getAllBoardPapereDownload(mySharedPref.getLastBoardPaperDownloadSyncTime()));
            if (board != null && board.moveToFirst()) {
                do {
                    BoardPaperDownload typeObj = (BoardPaperDownload) cParse.parseCursor(board, new BoardPaperDownload());
                    BoardpaperArray.add(typeObj);
                } while (board.moveToNext());
                board.close();
            }

            gson = new GsonBuilder().create();
            jBoardPaperArray = gson.toJsonTree(BoardpaperArray).getAsJsonArray();


            //------------------------ get Board Paper Record ENd-----------------------//


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showProgress(false, false);
            listener.onGetRecorComplite();
        }
    }

    public class insertRecorrd extends AsyncTask<String, String, String> {
        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        public OnTaskCompletedDashboard listener;

        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";

        private insertRecorrd(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            this.listener = (OnTaskCompletedDashboard) context;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {
            if (OneSignalArray != null && OneSignalArray.size() > 0) {
                for (int i = 0; i < OneSignalArray.size(); i++) {
                    try {
                        NotificationType typeObj = new NotificationType();

                        JSONObject jObj = new JSONObject(OneSignalArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        notification_type_id = jtype.getString("notification_type_id");
                        created_on = jtype.getString("created_on");
                        SubjectName = jtype.getString("subject_name");
                        paper_type = jtype.getString("paper_type");
                        typeObj.setOneSignalID(Integer.parseInt(OneSignalArray.get(i).get_id()));
                        typeObj.setCreatedOn(created_on);
                        typeObj.setAndroid_notification_id(OneSignalArray.get(i).getAndroid_notification_id());
                        typeObj.setIsOpen(OneSignalArray.get(i).getOpened());
                        typeObj.setIsTypeOpen("0");
                        typeObj.setSubjectName(SubjectName);
                        typeObj.setNotification_type_id(notification_type_id);
                        typeObj.setModuleType(type);
                        typeObj.setPaperType(paper_type);
                        typeObj.setMessage(OneSignalArray.get(i).getMessage());
                        typeObj.setTitle(OneSignalArray.get(i).getTitle());
                        if (mDb.isDbOpen())
                            mDb.insertWebRow(typeObj, "notification");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            if (ParseArrayMCQ != null && ParseArrayMCQ.length() > 0) {
                for (int i = 0; i < ParseArrayMCQ.length(); i++) {
                    try {

                        JSONObject obj = ParseArrayMCQ.getJSONObject(i);
                        StudentHdrObj = new StudentMcqTestHdr();
                        StudentHdrObj.setStudentID(obj.getString("StudentID"));
                        StudentHdrObj.setBoardID(obj.getString("BoardID"));
                        StudentHdrObj.setMediumID(obj.getString("MediumID"));
                        StudentHdrObj.setSubjectID(obj.getString("SubjectID"));

                        StudentHdrObj.setTimeleft(obj.getString("Timeleft"));
                        StudentHdrObj.setAddType(obj.getString("AddType"));
                        StudentHdrObj.setCreatedBy(obj.getString("CreatedBy"));
                        StudentHdrObj.setCreatedOn(obj.getString("CreatedOn"));
                        StudentHdrObj.setModifiedBy(obj.getString("ModifiedBy"));
                        StudentHdrObj.setModifiedOn(obj.getString("ModifiedOn"));

                        long ID = mDb.insertWebRow(StudentHdrObj, "student_mcq_test_hdr");
                        int inserID = Integer.parseInt(ID + "");

                        if (obj.has("chapterArray")) {
                            JSONArray chapterArray = obj.getJSONArray("chapterArray");

                            if (chapterArray != null && chapterArray.length() > 0) {
                                for (int k = 0; k < chapterArray.length(); k++) {
                                    JSONObject chapObj = chapterArray.getJSONObject(k);
                                    StudentChapObj = new StudentMcqTestChap();

                                    StudentChapObj.setStudentMCQTestHDRID((inserID + ""));
                                    StudentChapObj.setChapterid(chapObj.getString("ChapterID"));
                                    if (mDb.isDbOpen())
                                        mDb.insertWebRow(StudentChapObj, "student_mcq_test_chapter");
                                }
                            }
                        }


                        if (obj.has("detailArray")) {
                            JSONArray detailArray = obj.getJSONArray("detailArray");

                            if (detailArray != null && detailArray.length() > 0) {
                                for (int k = 0; k < detailArray.length(); k++) {
                                    JSONObject detailObj = detailArray.getJSONObject(k);
                                    mcqTestDtl = new StudentMcqTestDtl();
                                    mcqTestDtl.setQuestionID(detailObj.getString("QuestionID"));
                                    mcqTestDtl.setAnswerID(detailObj.getString("AnswerID"));
                                    mcqTestDtl.setIsAttempt(detailObj.getString("IsAttempt"));
                                    mcqTestDtl.setSrNo(detailObj.getString("srNo"));
                                    mcqTestDtl.setCreatedBy(detailObj.getString("CreatedBy"));
                                    mcqTestDtl.setCreatedOn(detailObj.getString("CreatedOn"));
                                    mcqTestDtl.setModifiedBy(detailObj.getString("ModifiedBy"));
                                    mcqTestDtl.setModifiedOn(detailObj.getString("ModifiedOn"));
                                    mcqTestDtl.setStudentMCQTestHDRID(inserID);
                                    if (mDb.isDbOpen())
                                        mDb.insertWebRow(mcqTestDtl, "student_mcq_test_dtl");
                                }
                            }
                        }
                        if (obj.has("levelArray")) {
                            JSONArray levelArray = obj.getJSONArray("levelArray");

                            if (levelArray != null && levelArray.length() > 0) {
                                for (int k = 0; k < levelArray.length(); k++) {
                                    JSONObject levelObj = levelArray.getJSONObject(k);
                                    mcqTestLevel = new StudentMcqTestLevel();
                                    mcqTestLevel.setStudentMCQTestHDRID(inserID + "");
                                    mcqTestLevel.setLevelID(levelObj.getString("LevelID"));
                                    if (mDb.isDbOpen())
                                        mDb.insertWebRow(mcqTestLevel, "student_mcq_test_level");
                                }
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }


            if (ParseArrayPracties != null && ParseArrayPracties.length() > 0) {
                try {
                    StudentQuestionPaper StudentQuePaeprObj;
                    for (int i = 0; i < ParseArrayPracties.length(); i++) {
                        JSONObject obj = ParseArrayPracties.getJSONObject(i);

                        StudentQuePaeprObj = new StudentQuestionPaper();
                        StudentQuePaeprObj.setExamTypePatternID(obj.getString("ExamTypePatternID"));
                        StudentQuePaeprObj.setStudentID(obj.getString("StudentID"));
                        StudentQuePaeprObj.setPaperTypeID(obj.getString("PaperTypeID"));
                        StudentQuePaeprObj.setSubjectID(obj.getString("SubjectID"));
                        StudentQuePaeprObj.setTotalMarks(obj.getString("TotalMarks"));
                        StudentQuePaeprObj.setDuration(obj.getString("Duration"));
                        StudentQuePaeprObj.setCreatedBy(obj.getString("CreatedBy"));
                        StudentQuePaeprObj.setCreatedOn(obj.getString("CreatedOn"));
                        StudentQuePaeprObj.setModifiedBy(obj.getString("ModifiedBy"));
                        StudentQuePaeprObj.setModifiedOn(obj.getString("ModifiedOn"));
                        long ID = mDb.insertWebRow(StudentQuePaeprObj, "student_question_paper");
                        StuQuePaperArray.add(StudentQuePaeprObj);
                        int inserID = Integer.parseInt(ID + "");


                        if (obj.has("chapterArray")) {
                            JSONArray chapterArray = obj.getJSONArray("chapterArray");

                            if (chapterArray != null && chapterArray.length() > 0) {
                                StudentQuestionPaperChapter StudentQuePaeprChpObj;
                                for (int k = 0; k < chapterArray.length(); k++) {
                                    JSONObject chapObj = chapterArray.getJSONObject(k);
                                    StudentQuePaeprChpObj = new StudentQuestionPaperChapter();

                                    StudentQuePaeprChpObj.setStudentQuestionPaperID((inserID));
                                    StudentQuePaeprChpObj.setChapterID(chapObj.getString("ChapterID"));
                                    if (mDb.isDbOpen())
                                        mDb.insertWebRow(StudentQuePaeprChpObj, "student_question_paper_chapter");
                                }
                            }
                        }
                        if (obj.has("detailArray")) {
                            JSONArray detailArray = obj.getJSONArray("detailArray");

                            if (detailArray != null && detailArray.length() > 0) {
                                StudentQuestionPaperDetail StudentQuePaeprDtlObj;
                                for (int k = 0; k < detailArray.length(); k++) {
                                    JSONObject detailObj = detailArray.getJSONObject(k);
                                    StudentQuePaeprDtlObj = new StudentQuestionPaperDetail();
                                    StudentQuePaeprDtlObj.setExamTypePatternDetailID(detailObj.getString("ExamTypePatternDetailID"));
                                    StudentQuePaeprDtlObj.setMasterorCustomized(Integer.parseInt(detailObj.getString("MasterorCustomized")));
                                    StudentQuePaeprDtlObj.setMQuestionID(detailObj.getString("MQuestionID"));
                                    StudentQuePaeprDtlObj.setCQuestionID(Integer.parseInt(detailObj.getString("CQuestionID")));

                                    StudentQuePaeprDtlObj.setStudentQuestionPaperID(inserID);
                                    if (mDb.isDbOpen())
                                        mDb.insertWebRow(StudentQuePaeprDtlObj, "student_question_paper_detail");
                                }
                            }
                        }

                        if (obj.has("queTypeArray")) {
                            JSONArray queTypeArray = obj.getJSONArray("queTypeArray");

                            if (queTypeArray != null && queTypeArray.length() > 0) {
                                StudentSetPaperQuestionType StudentSetPaperQueTypeObj;
                                for (int k = 0; k < queTypeArray.length(); k++) {
                                    JSONObject queTypeObj = queTypeArray.getJSONObject(k);
                                    StudentSetPaperQueTypeObj = new StudentSetPaperQuestionType();
                                    StudentSetPaperQueTypeObj.setQuestionTypeID(queTypeObj.getString("QuestionTypeID"));
                                    StudentSetPaperQueTypeObj.setTotalAsk(queTypeObj.getString("TotalAsk"));
                                    StudentSetPaperQueTypeObj.setToAnswer(queTypeObj.getString("ToAnswer"));
                                    StudentSetPaperQueTypeObj.setTotalMark(queTypeObj.getString("TotalMark"));

                                    StudentSetPaperQueTypeObj.setStudentQuestionPaperID(inserID + "");
                                    long QuTypeID = mDb.insertWebRow(StudentSetPaperQueTypeObj, "student_set_paper_question_type");
                                    int inserQuTypeID = Integer.parseInt(QuTypeID + "");

                                    if (queTypeObj.has("setdetailArray")) {
                                        Utils.Log("TAG ", "in SET detail::->");
                                        JSONArray setdetailArray = queTypeObj.getJSONArray("setdetailArray");

                                        if (setdetailArray != null && setdetailArray.length() > 0) {
                                            StudentSetPaperDetail StudentSetPaperDtlObj;
                                            for (int j = 0; j < setdetailArray.length(); j++) {
                                                Utils.Log("TAG ", "in SET detail  for setdetailArray ::->");
                                                JSONObject setdetailObj = setdetailArray.getJSONObject(j);
                                                StudentSetPaperDtlObj = new StudentSetPaperDetail();
                                                StudentSetPaperDtlObj.setStudentSetPaperQuestionTypeID(inserQuTypeID);
                                                try {
                                                    StudentSetPaperDtlObj.setMQuestionID(setdetailObj.getString("MQuestionID"));
                                                } catch (NumberFormatException e) {
                                                    e.printStackTrace();
                                                    StudentSetPaperDtlObj.setMQuestionID("-2");
                                                }
                                                if (mDb.isDbOpen())
                                                    mDb.insertWebRow(StudentSetPaperDtlObj, "student_set_paper_detail");

                                            }
                                        }
                                    } else {
                                        Utils.Log("TAG ", "else SET detail::->");

                                    }

                                }
                            }
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (ParseArrayBoard != null && ParseArrayBoard.length() > 0) {
                try {
                    BoardPaperDownload boardPaperObj;

                    for (int i = 0; i < ParseArrayBoard.length(); i++) {
                        JSONObject obj = ParseArrayBoard.getJSONObject(i);

                        boardPaperObj = new BoardPaperDownload();
                        boardPaperObj.setBoardPaperAnswers(obj.getString("BoardPaperAnswers"));
                        boardPaperObj.setBoardPaperID(obj.getString("BoardPaperID"));
                        boardPaperObj.setStudentID(obj.getString("StudentID"));
                        boardPaperObj.setSubjectID(obj.getString("SubjectID"));
                        boardPaperObj.setCreatedOn(obj.getString("CreatedOn"));
                        mDb.insertWebRow(boardPaperObj, "board_paper_download");
                        BoardpaperArray.add(boardPaperObj);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showProgress(false, true);
            listener.onInsertRecorComplite();
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, "onExceptionsEventEvent: " + event.isFaild());
        showProgress(false, false);
        mProgressZooki.setVisibility(View.GONE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TableReload event) {
        showProgress(false, false);
        if (Utils.isNetworkAvailable(this)) {
            new CallRequest(this).getTableRecords();
        }
    }


    public static void onInterntDisconnect() {
        showInternetPopup(App.mContext);
        if (app != null && App.mySharedPref != null)
            App.mySharedPref.setDownloadStatus("n");
    }

}
