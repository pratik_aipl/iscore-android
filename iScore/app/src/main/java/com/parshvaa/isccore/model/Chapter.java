package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 2/4/2017.
 */

@JsonObject
public class Chapter implements Serializable {

    private static final long serialVersionUID = 1L;

    public static int isChecked = -1;

//    @JsonField
//    public String name;
    @JsonField
    public String ChapterID;
    @JsonField
    public String ChapterNumber;
    @JsonField
    public String ParentChapterID;
    @JsonField
    public String BoardID;
    @JsonField
    public String MediumID;
    @JsonField
    public String StandardID;
    @JsonField
    public String SubjectID;
    @JsonField
    public String ChapterName;
    @JsonField
    public String DisplayOrder;
    @JsonField
    public String isMCQ;
    @JsonField
    public String isDemo="";
    @JsonField
    public String CreatedBy;
    @JsonField
    public String CreatedOn;
    @JsonField
    public String ModifiedBy;
    @JsonField
    public String ModifiedOn;
    @JsonField
    public String isGeneratePaper;


    public String getIsDemo() {
        return isDemo;
    }

    public void setIsDemo(String isDemo) {
        this.isDemo = isDemo;
    }

    public String getIsMCQ() {
        return isMCQ;
    }

    public void setIsMCQ(String isMCQ) {
        this.isMCQ = isMCQ;
    }

    public String getDisplayOrder() {
        return DisplayOrder;
    }

    public void setDisplayOrder(String displayOrder) {
        DisplayOrder = displayOrder;
    }


    public ArrayList<Chapter> subChapters = new ArrayList<>();

    public Chapter() {

    }

    public Chapter(String name) {
        this.ChapterName = name;
    }

    public Chapter(String name, boolean isSelected) {

        this.ChapterName = name;

        this.isSelected = isSelected;
    }
    public String getIsGeneratePaper() {
        return isGeneratePaper;
    }

    public void setIsGeneratePaper(String isGeneratePaper) {
        this.isGeneratePaper = isGeneratePaper;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getChapterNumber() {
        return ChapterNumber;
    }

    public void setChapterNumber(String chapterNumber) {
        ChapterNumber = chapterNumber;
    }

    public String getParentChapterID() {
        return ParentChapterID;
    }

    public void setParentChapterID(String parentChapterID) {
        ParentChapterID = parentChapterID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }


    public boolean isSelected;


//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }



    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

}