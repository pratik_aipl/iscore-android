package com.parshvaa.isccore.activity.textbook;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class TextBook implements Serializable {
    @JsonField
    public String SubjectID = "";
    @JsonField
    public String SubjectName = "";
    @JsonField
    public String SubjectIcon = "";

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectIcon() {
        return SubjectIcon;
    }

    public void setSubjectIcon(String subjectIcon) {
        SubjectIcon = subjectIcon;
    }
}
