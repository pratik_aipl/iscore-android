package com.parshvaa.isccore.activity.mcq.adapter;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.TestSummary;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.ISccoreWebView;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NotAppearedAnswerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    private LayoutInflater mInflater;
    private ArrayList<McqQuestion> mItems;
    private View mHeaderView;
    McqQuestion mcqQueObj;
    private List<TestSummary> moviesList;
    public boolean isPosition = true;
    public static Context mContext;
    public static boolean isSolution = true;

    public NotAppearedAnswerAdapter(Context context, ArrayList<McqQuestion> items) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public McqQuestion getItem(int position) {

        return mItems.get(position);
    }

    @Override
    public int getItemViewType(int position) {


        return VIEW_TYPE_ITEM;


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ItemViewHolder(mInflater.inflate(R.layout.custom_correct_question_raw, parent, false));

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {

            case 1:
                ItemViewHolder addrHolder = (ItemViewHolder) holder;
                bindItemHolder(mItems.get(position), addrHolder, position);
                break;
        }

    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView tvQueNO, btn_solution, tv_solution,
                tv_question, tv_a, tv_b, tv_c, tv_d, ans_a, ans_b, ans_c, ans_d, tv_level;
        public ISccoreWebView qWbeView, aWebView, bWebView, cWebView, dWebView, web_solution;
        public LinearLayout lin_a, lin_b, lin_c, lin_d;
        public LinearLayout lin_solution;

        public ItemViewHolder(View view) {
            super(view);
            tvQueNO = view.findViewById(R.id.tvQueNO);
            tv_question = view.findViewById(R.id.tv_q);
            btn_solution = view.findViewById(R.id.btn_solution);
            tv_solution = view.findViewById(R.id.tv_solution);
            lin_solution = view.findViewById(R.id.lin_solution);

            qWbeView = view.findViewById(R.id.qWebView);
            aWebView = view.findViewById(R.id.aWebView);
            bWebView = view.findViewById(R.id.bWebView);
            cWebView = view.findViewById(R.id.cWebView);
            dWebView = view.findViewById(R.id.dWebView);
            lin_a = view.findViewById(R.id.lin_a);
            lin_b = view.findViewById(R.id.lin_b);
            lin_c = view.findViewById(R.id.lin_c);
            lin_d = view.findViewById(R.id.lin_d);
            tv_question = view.findViewById(R.id.tv_q);
            tv_a = view.findViewById(R.id.tv_a);
            tv_b = view.findViewById(R.id.tv_b);
            tv_c = view.findViewById(R.id.tv_c);
            tv_d = view.findViewById(R.id.tv_d);
            ans_a = view.findViewById(R.id.a_ans);
            ans_b = view.findViewById(R.id.b_ans);
            ans_c = view.findViewById(R.id.c_ans);
            ans_d = view.findViewById(R.id.d_ans);
            tv_level = view.findViewById(R.id.tv_level);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    void bindItemHolder(McqQuestion mcqQueObj, final ItemViewHolder itemHolder, final int position) {
        itemHolder.tvQueNO.setText((position + 1) + "");
        if (mcqQueObj.getQuestion().contains("<img") || mcqQueObj.getQuestion().contains("<math") || mcqQueObj.getQuestion().contains("<table")) {
            itemHolder.qWbeView.setVisibility(View.VISIBLE);
            itemHolder.tv_question.setVisibility(View.GONE);
            itemHolder.qWbeView.loadHtmlFromLocal(mcqQueObj.getQuestion());
        } else {
            itemHolder.tv_question.setText(Html.fromHtml(mcqQueObj.getQuestion()));

        }
        itemHolder.tv_level.setText("(Level " + mcqQueObj.getQuestionLevelID() + ")");

        itemHolder.tv_a.setBackgroundResource(0);
        itemHolder.tv_b.setBackgroundResource(0);
        itemHolder.tv_c.setBackgroundResource(0);
        itemHolder.tv_d.setBackgroundResource(0);

        isSolution = false;
        itemHolder.btn_solution.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        itemHolder.btn_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_line));
        if (mcqQueObj != null && mcqQueObj.getSolution().equals(null) || mcqQueObj.getSolution().equals("")) {
            itemHolder.btn_solution.setVisibility(View.GONE);
        } else {
            itemHolder.btn_solution.setVisibility(View.VISIBLE);
            if (mcqQueObj != null && !TextUtils.isEmpty(mcqQueObj.getSolution()) && mcqQueObj.optionsArray != null && mcqQueObj.optionsArray.size() > 0) {
                if (mcqQueObj.getSolution().contains("<img") || mcqQueObj.optionsArray.get(0).getOptions().contains("<math") || mcqQueObj.optionsArray.get(0).getOptions().contains("<table")) {
                        itemHolder.web_solution.loadHtmlFromLocal(mcqQueObj.getSolution());
                } else {
                    itemHolder.tv_solution.setText(Html.fromHtml(mcqQueObj.getSolution()).toString().trim());
                }
            }
        }
        if (mcqQueObj != null && mcqQueObj.optionsArray != null && mcqQueObj.optionsArray.size() > 0) {
            if (!mcqQueObj.optionsArray.get(0).getOptions().isEmpty() && !mcqQueObj.optionsArray.get(0).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_a.setVisibility(View.VISIBLE);
                if (mcqQueObj.optionsArray.get(0).getOptions().contains("<img") || mcqQueObj.optionsArray.get(0).getOptions().contains("<math") || mcqQueObj.optionsArray.get(0).getOptions().contains("<table")) {
                    itemHolder.aWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_a.setVisibility(View.GONE);
                    itemHolder.aWebView.loadHtmlFromLocal(!TextUtils.isEmpty(mcqQueObj.optionsArray.get(0).getOptions()) ? mcqQueObj.optionsArray.get(0).getOptions() : "");
                } else {
                    itemHolder.ans_a.setText(Html.fromHtml(mcqQueObj.optionsArray.get(0).getOptions()).toString().trim());
                }

                if (mcqQueObj.optionsArray.get(0).isCorrect.equalsIgnoreCase("1")) {
                    itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_right);
                }
            }


            if (mcqQueObj.optionsArray.size() > 1) {
                if (!mcqQueObj.optionsArray.get(1).getOptions().isEmpty() && !mcqQueObj.optionsArray.get(1).getOptions().equalsIgnoreCase("")) {
                    itemHolder.lin_b.setVisibility(View.VISIBLE);
                    if (mcqQueObj.optionsArray.get(1).getOptions().contains("<img") || mcqQueObj.optionsArray.get(1).getOptions().contains("<math") || mcqQueObj.optionsArray.get(1).getOptions().contains("<table")) {
                        itemHolder.bWebView.setVisibility(View.VISIBLE);
                        itemHolder.ans_b.setVisibility(View.GONE);
                        itemHolder.aWebView.loadHtmlFromLocal(!TextUtils.isEmpty(mcqQueObj.optionsArray.get(1).getOptions()) ? mcqQueObj.optionsArray.get(1).getOptions() : "");
                    } else {
                        itemHolder.ans_b.setText(Html.fromHtml(mcqQueObj.optionsArray.get(1).getOptions()).toString().trim());
                    }

                    if (mcqQueObj.optionsArray.get(1).isCorrect.equalsIgnoreCase("1")) {
                        itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_right);
                    }
                }
            }


            if (mcqQueObj.optionsArray.size() > 2) {
                if (!mcqQueObj.optionsArray.get(2).getOptions().isEmpty() && !mcqQueObj.optionsArray.get(2).getOptions().equalsIgnoreCase("")) {
                    itemHolder.lin_c.setVisibility(View.VISIBLE);
                    if (mcqQueObj.optionsArray.get(2).getOptions().contains("<img") || mcqQueObj.optionsArray.get(2).getOptions().contains("<math") || mcqQueObj.optionsArray.get(2).getOptions().contains("<table")) {
                        itemHolder.cWebView.setVisibility(View.VISIBLE);
                        itemHolder.ans_c.setVisibility(View.GONE);
                        itemHolder.aWebView.loadHtmlFromLocal(!TextUtils.isEmpty(mcqQueObj.optionsArray.get(2).getOptions()) ? mcqQueObj.optionsArray.get(2).getOptions() : "");
                    } else {
                        itemHolder.ans_c.setText(Html.fromHtml(mcqQueObj.optionsArray.get(2).getOptions()).toString().trim());
                    }

                    if (mcqQueObj.optionsArray.get(2).isCorrect.equalsIgnoreCase("1")) {
                        Utils.Log("TAG", "Position :->" + position + "  -> 2");
                        itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_right);
                    }
                }
            }


            if (mcqQueObj.optionsArray.size() > 3) {
                if (!mcqQueObj.optionsArray.get(3).getOptions().isEmpty() && !mcqQueObj.optionsArray.get(3).getOptions().equalsIgnoreCase("")) {
                    itemHolder.lin_d.setVisibility(View.VISIBLE);
                    if (mcqQueObj.optionsArray.get(3).getOptions().contains("<img") || mcqQueObj.optionsArray.get(3).getOptions().contains("<math") || mcqQueObj.optionsArray.get(3).getOptions().contains("<table")) {
                        itemHolder.dWebView.setVisibility(View.VISIBLE);
                        itemHolder.ans_d.setVisibility(View.GONE);
                        itemHolder.aWebView.loadHtmlFromLocal(!TextUtils.isEmpty(mcqQueObj.optionsArray.get(3).getOptions()) ? mcqQueObj.optionsArray.get(3).getOptions() : "");
                    } else {
                        itemHolder.ans_d.setText(Html.fromHtml(mcqQueObj.optionsArray.get(3).getOptions()).toString().trim());
                    }
                    if (mcqQueObj.optionsArray.get(3).isCorrect.equalsIgnoreCase("1")) {
                        itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_right);
                    }
                }

            }
        }


        itemHolder.btn_solution.setOnClickListener(v -> {
            if (!isSolution) {
                isSolution = true;
                itemHolder.lin_solution.setVisibility(View.VISIBLE);
                itemHolder.btn_solution.setText("    Back    ");
                itemHolder.btn_solution.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
                itemHolder.btn_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_button));
            } else {
                isSolution = false;
                itemHolder.lin_solution.setVisibility(View.GONE);
                itemHolder.btn_solution.setText("  Explanation  ");
                itemHolder.btn_solution.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                itemHolder.btn_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_line));
            }
        });
    }


}
