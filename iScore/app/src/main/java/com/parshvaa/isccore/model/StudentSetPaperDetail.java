package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Dipesh on 3/25/2017.
 */
@JsonObject
public class StudentSetPaperDetail implements Serializable {
    @JsonField
    public int StudentSetPaperDetailID = -1,StudentSetPaperQuestionTypeID=0;
    @JsonField
    public String MQuestionID="";
    //public String StudentSetPaperQuestionTypeID = "", MQuestionID = "";

    public int getStudentSetPaperDetailID() {
        return StudentSetPaperDetailID;
    }

    public void setStudentSetPaperDetailID(int studentSetPaperDetailID) {
        this.StudentSetPaperDetailID = studentSetPaperDetailID;
    }

    public int getStudentSetPaperQuestionTypeID() {
        return StudentSetPaperQuestionTypeID;
    }

    public void setStudentSetPaperQuestionTypeID(int studentSetPaperQuestionTypeID) {
        this.StudentSetPaperQuestionTypeID = studentSetPaperQuestionTypeID;
    }

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }
    @JsonField(name = "setdetailArray")
    public ArrayList<ClassQuestionPaperSubQuestion> subQuesArray = new ArrayList<>();

    public ArrayList<ClassQuestionPaperSubQuestion> getSubQuesArray() {
        return subQuesArray;
    }

    public void setSubQuesArray(ArrayList<ClassQuestionPaperSubQuestion> subQuesArray) {
        this.subQuesArray = subQuesArray;
    }
}
