package com.parshvaa.isccore.activity.reports.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parshvaa.isccore.model.TestReport;
import com.parshvaa.isccore.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 11/2/2017.
 */

public class SubjectTestlAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<TestReport> testReportsArray;
    public LayoutInflater inflater;

    public SubjectTestlAdapter(Fragment ft, ArrayList<TestReport> testReportsArray) {
        mContext = ft.getActivity();
        this.testReportsArray = testReportsArray;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return testReportsArray.size();
    }

    @Override
    public TestReport getItem(int position) {
        // TODO Auto-generated method stub
        return testReportsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private static class ViewHolder {

        public TextView tv_subj;
        public TextView tv_total,tv_ready,tv_prelim,tv_set;
        public ProgressBar prgBar;

        public ViewHolder(View v) {
            tv_subj = v.findViewById(R.id.tv_sub_name);

            tv_total = v.findViewById(R.id.tv_total);
            tv_ready = v.findViewById(R.id.tv_ready);
            tv_prelim = v.findViewById(R.id.tv_prelim);
            tv_set = v.findViewById(R.id.tv_set);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_subject_test_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_subj.setText(testReportsArray.get(position).getSubjectName()+" (Total Test)");
        holder.tv_total.setText(getItem(position).getTotalTest());
        holder.tv_prelim.setText(getItem(position).getPrelim());
        holder.tv_ready.setText(getItem(position).getReady());
        holder.tv_set.setText(getItem(position).getSetPaper());

        return convertView;
    }

}
