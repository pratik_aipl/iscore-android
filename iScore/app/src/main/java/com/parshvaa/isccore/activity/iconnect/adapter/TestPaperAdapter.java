package com.parshvaa.isccore.activity.iconnect.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.iconnect.AddPaperStartTime;
import com.parshvaa.isccore.activity.iconnect.ITestPaper;
import com.parshvaa.isccore.activity.iconnect.activity.IGeneratePaperActivity;
import com.parshvaa.isccore.activity.iconnect.activity.ISetPaperActivity;
import com.parshvaa.isccore.activity.iconnect.activity.PaperPDFPreviewActivity;
import com.parshvaa.isccore.activity.iconnect.activity.UploadPaperImagesActivity;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ProgressBarAnimation;
import com.parshvaa.isccore.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class TestPaperAdapter extends RecyclerView.Adapter<TestPaperAdapter.MyViewHolder> {

    private static final String TAG = "TestPaperAdapter";
    private List<ITestPaper> paperTypeList;
    public Context context;
    public int SubjectID;
    public String SubjectName;
    AddPaperStartTime addPaperStartTime;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_month, tv_marks, tv_date,tv_time,tv_hours;
        public ImageView img_watch,img_play,img_upload;
        public ProgressBar pbar_accuracy;
        public RelativeLayout rel_img;
        View vieww;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_month = view.findViewById(R.id.tv_month);
            tv_date = view.findViewById(R.id.tv_date);
            tv_marks = view.findViewById(R.id.tv_marks);
            img_watch = view.findViewById(R.id.img_watch);
            tv_time = view.findViewById(R.id.tv_time);
            tv_hours = view.findViewById(R.id.tv_hours);
            img_play = view.findViewById(R.id.img_play);
            img_upload = view.findViewById(R.id.img_upload);
            vieww = view.findViewById(R.id.vieww);

            pbar_accuracy = view.findViewById(R.id.pbar_accuracy);
            rel_img = view.findViewById(R.id.rel_img);
        }
    }


    public TestPaperAdapter(List<ITestPaper> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;

        this.context = context;
        this.addPaperStartTime = (AddPaperStartTime) context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_serach_i_practies_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ITestPaper cons = paperTypeList.get(position);
        holder.tv_subject_name.setText(cons.getSubjectName());
        String startDate = "";
        String endDate = "";
        try {
            startDate = cons.getPaperDate();
            startDate = startDate.substring(0, 10);
            endDate = cons.getPaperDate();
            endDate = endDate.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startDate = Utils.changeDateToMMDDYYYY(startDate);
        holder.tv_month.setText(startDate.substring(3, 6));
        holder.tv_date.setText(startDate.substring(0, 2));
        try {
            if (cons.getIsStudentMarkUploaded().equals("0") || cons.getIsStudentMarkUploaded().equals("")) {
                holder.pbar_accuracy.setVisibility(View.GONE);
                holder.rel_img.setVisibility(View.GONE);
                holder.tv_marks.setText(cons.getTotalMarks() + " Marks");
            } else {
                holder.pbar_accuracy.setMax(Integer.parseInt(cons.getTotalMarks()));
                ProgressBarAnimation anim = new ProgressBarAnimation(holder.pbar_accuracy, 0, Float
                        .parseFloat(cons.getStudentMark()));
                anim.setDuration(1000);
                holder.pbar_accuracy.startAnimation(anim);
                holder.tv_marks.setText(cons.getStudentMark() + "/" + cons.getTotalMarks() + " Marks");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        holder.tv_marks.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        holder.tv_time.setText(cons.getOnlyStartTime());
        holder.tv_hours.setText(cons.getOnlyEndTime());
       /* int totalMinutesInt = Integer.parseInt(cons.getDuration());
        int hours = totalMinutesInt / 60;
        int hoursToDisplay = hours;
        if (hours > 12) {
            hoursToDisplay = hoursToDisplay - 12;
        }
        int minutesToDisplay = totalMinutesInt - (hours * 60);
        String minToDisplay = null;
        if(minutesToDisplay == 0 ) minToDisplay = "00";
        else if( minutesToDisplay < 10 ) minToDisplay = "00" + minutesToDisplay ;
        else minToDisplay = "" + minutesToDisplay ;
        String displayValue = hoursToDisplay + ":" + minToDisplay;

        holder.tv_hours.setText(displayValue);*/

        DateFormat formatt = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
        try {
            Date Startdate = formatt.parse(cons.getStartTime());
            Date Enddate = formatt.parse(cons.getEndTime());

            Date uploadStartdate = formatt.parse(cons.getUploadStartTime());
            Date uploadEnddate = formatt.parse(cons.getUploadEndTime());

            long Currentmillisecond = new Date().getTime();

            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String CurrentDate = df.format(c);

            Log.d(TAG, "uploadstartmilli: "+uploadStartdate.getTime());
            Log.d(TAG, "uploadEndmilli: "+uploadEnddate.getTime());

            Log.d(TAG, "startmilli: "+Startdate.getTime());
            Log.d(TAG, "Endmilli: "+Enddate.getTime());
            Log.d(TAG, "currentmilli: "+Currentmillisecond);

            if(CurrentDate.equals(cons.getQuestionPaperDate()) && Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                holder.img_play.setVisibility(View.VISIBLE);
                holder.img_upload.setVisibility(View.GONE);
                holder.vieww.setVisibility(View.VISIBLE);
                Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
                animation.setDuration(1000); //1 second duration for each animation cycle
                animation.setInterpolator(new LinearInterpolator());
                animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
                animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
                holder.img_play.startAnimation(animation); //to start animation
            }
            if(CurrentDate.equals(cons.getQuestionPaperDate()) && Currentmillisecond>uploadStartdate.getTime() && Currentmillisecond<uploadEnddate.getTime()){
                holder.img_upload.setVisibility(View.VISIBLE);
                holder.img_play.setVisibility(View.GONE);
                holder.vieww.setVisibility(View.VISIBLE);
                Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
                animation.setDuration(1000); //1 second duration for each animation cycle
                animation.setInterpolator(new LinearInterpolator());
                animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
                animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
                holder.img_upload.startAnimation(animation); //to start animation
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }




        holder.img_play.setOnClickListener(view -> {
                DateFormat dff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             //   DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                String formattedDate= dff.format(new Date());

            addPaperStartTime.onPaperStartTime( MainUser.getStudentId(),cons.getHistoryID(),cons.Type,formattedDate);
            try {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
                Date Startdate = format.parse(cons.getStartTime());
                Date Enddate = format.parse(cons.getEndTime());
                long Currentmillisecond = new Date().getTime();

                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String CurrentDate = df.format(c);

                Log.d(TAG, "now: "+CurrentDate);

                Log.d(TAG, "startmilli: "+Startdate.getTime());
                Log.d(TAG, "Endmilli: "+Enddate.getTime());
                Log.d(TAG, "currentmilli: "+Currentmillisecond);

                if(CurrentDate.equals(cons.getQuestionPaperDate())){
                    if(Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                        if (cons.getType().equalsIgnoreCase("generate")) {
                            if (cons.getPaperTypeName().equalsIgnoreCase("Ready Paper")) {
                                Intent intent = new Intent(context, IGeneratePaperActivity.class);
                                intent.putExtra("id", cons.getPaperID());
                                intent.putExtra(Constant.TYPE, "TESTPAPER");
                                intent.putExtra(Constant.UPLOADSTARTTIME, cons.getUploadStartTime());
                                intent.putExtra(Constant.UPLOADENDTIME, cons.getUploadEndTime());
                                intent.putExtra(Constant.STARTTIME, cons.getStartTime());
                                intent.putExtra(Constant.ENDTIME, cons.getEndTime());
                                intent.putExtra(Constant.isOnlyQuestionPaper, cons.getIsOnlyQuestionPaper());
                                intent.putExtra(Constant.AnswerPaperDate, cons.getAnswerPaperDate() != null ? cons.getAnswerPaperDate() : "");
                                context.startActivity(intent);

                            } else if (cons.getPaperTypeName().equalsIgnoreCase("Prelim Paper")) {
                                Intent intent = new Intent(context, IGeneratePaperActivity.class);
                                intent.putExtra("id", cons.getPaperID());
                                intent.putExtra(Constant.TYPE, "TESTPAPER");
                                intent.putExtra(Constant.UPLOADSTARTTIME, cons.getUploadStartTime());
                                intent.putExtra(Constant.UPLOADENDTIME, cons.getUploadEndTime());
                                intent.putExtra(Constant.STARTTIME, cons.getStartTime());
                                intent.putExtra(Constant.ENDTIME, cons.getEndTime());
                                intent.putExtra(Constant.isOnlyQuestionPaper, cons.getIsOnlyQuestionPaper());
                                intent.putExtra(Constant.AnswerPaperDate, cons.getAnswerPaperDate() != null ? cons.getAnswerPaperDate() : "");
                                context.startActivity(intent);

                            } else {
                                Intent intent = new Intent(context, ISetPaperActivity.class);
                                intent.putExtra("id", cons.getPaperID());
                                intent.putExtra(Constant.TYPE, "TESTPAPER");
                                intent.putExtra(Constant.UPLOADSTARTTIME, cons.getUploadStartTime());
                                intent.putExtra(Constant.UPLOADENDTIME, cons.getUploadEndTime());
                                intent.putExtra(Constant.STARTTIME, cons.getStartTime());
                                intent.putExtra(Constant.ENDTIME, cons.getEndTime());
                                intent.putExtra(Constant.isOnlyQuestionPaper, cons.getIsOnlyQuestionPaper());
                                intent.putExtra(Constant.AnswerPaperDate, cons.getAnswerPaperDate() != null ? cons.getAnswerPaperDate() : "");
                                context.startActivity(intent);
                            }
                        }else{
                            Intent intent = new Intent(context, PaperPDFPreviewActivity.class);
                            intent.putExtra("id", cons.getPaperID());
                            intent.putExtra(Constant.PAPERNAME, cons.getSubjectName());
                            intent.putExtra(Constant.PAPERLINK, cons.getQuestionPaperFile());
                            intent.putExtra(Constant.STARTTIME, cons.getStartTime());
                            intent.putExtra(Constant.ENDTIME, cons.getEndTime());
                            intent.putExtra(Constant.isOnlyQuestionPaper, cons.getIsOnlyQuestionPaper());
                            intent.putExtra(Constant.AnswerPaperDate, cons.getAnswerPaperDate() != null ? cons.getAnswerPaperDate() : "");
                            context.startActivity(intent);
                        }

                    }else{
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                        builder1.setMessage("Alert! To view this Not In a Time");
                        builder1.setCancelable(true);
                        builder1.setNegativeButton("OK", (dialog, i) -> {
                            dialog.cancel();
                        });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                }else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Alert! Please Select Today Date...!");
                    builder1.setCancelable(true);
                    builder1.setNegativeButton("OK", (dialog, i) -> {
                        dialog.cancel();
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


        });

        holder.img_upload
                .setOnClickListener(view ->
                context.startActivity(new Intent(context, UploadPaperImagesActivity.class)
                        .putExtra(Constant.PAPERID,cons.getPaperID())
                        .putExtra(Constant.TYPE,cons.getType().equalsIgnoreCase("generate")?"GENRATE":"UPLOAD")));

    }

    @Override
    public int getItemCount() {
        return paperTypeList.size();
    }


}



