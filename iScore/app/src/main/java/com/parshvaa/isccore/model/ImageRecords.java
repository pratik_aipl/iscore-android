package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class ImageRecords {

    @JsonField(name = "question_images")
    List<String> questionImages;

    @JsonField(name = "subject_icon")
    SubjectIcon subjectIcon;

    public List<String> getQuestionImages() {
        return questionImages;
    }

    public void setQuestionImages(List<String> questionImages) {
        this.questionImages = questionImages;
    }

    public SubjectIcon getSubjectIcon() {
        return subjectIcon;
    }

    public void setSubjectIcon(SubjectIcon subjectIcon) {
        this.subjectIcon = subjectIcon;
    }
}
