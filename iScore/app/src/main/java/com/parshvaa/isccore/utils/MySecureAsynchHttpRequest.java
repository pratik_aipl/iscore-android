package com.parshvaa.isccore.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.parshvaa.isccore.custominterface.AsynchTaskListner;
//import com.parshvaa.isccore.services.MySSLSocketFactoryNew;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sagar Sojitra on 3/8/2016.
 */
public class MySecureAsynchHttpRequest {

    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public Constant.POST_TYPE post_type;

    public MySecureAsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (map.containsKey("show")) {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
//            Utils.showToast("No Internet, Please try again later", ft.getActivity());
            EventBus.getDefault().post(new MessageEvent(null, true));
        }
    }

    public Context ct;

    public MySecureAsynchHttpRequest(Context ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ct)) {
            if (map.containsKey("show")) {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
//            Utils.showToast("No Internet, Please try again later", ct);
            EventBus.getDefault().post(new MessageEvent(null, true));
        }
    }


    class MyRequest extends AsyncTask<Map<String, String>, Void, String> {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(Map<String, String>... map) {
            Utils.SystemOutPrintln("::urls ::", map[0].get("url"));

            String responseBody = "";
            switch (post_type) {
                case GET:
                    DefaultHttpClient httpClient = getNewHttpClient();
                    String query = map[0].get("url");
                    map[0].remove("url");
                    List<String> values = new ArrayList<String>(map[0].values());
                    List<String> keys = new ArrayList<String>(map[0].keySet());
                    for (int i = 0; i < values.size(); i++) {
                        Utils.SystemOutPrintln(keys.get(i), "====>" + values.get(i));
                        query = query + keys.get(i) + "=" + values.get(i).replace(" ", "%20");
                        if (i < values.size() - 1) {
                            query += "&";
                        }
                    }
                    // URLEncoder.encode(query, "UTF-8");
                    Utils.SystemOutPrintln("URL", "====>" + query);
                    HttpGet httpGet = new HttpGet(query);

                    HttpResponse httpResponse = null;
                    HttpEntity httpEntity = null;
                    try {
                        httpResponse = httpClient.execute(httpGet);
                        httpEntity = httpResponse.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                    } catch (IOException e) {
                        e.printStackTrace();
                        EventBus.getDefault().post(new MessageEvent(e, true));
                    }
                    return responseBody;

                case POST:
                    Utils.SystemOutPrintln("new Requested URL >> ", map[0].get("url"));
                    HttpPost postRequest = new HttpPost(map[0].get("url"));
                    DefaultHttpClient httpclient = getNewHttpClient();
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    for (String key : map[0].keySet()) {
                        Utils.SystemOutPrintln(key, "====>" + map[0].get(key));
                        nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                    }
                    try {
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    Utils.SystemOutPrintln("Final URI::", postRequest.getEntity().toString());
                    HttpResponse response = null;
                    try {
                        response = httpclient.execute(postRequest);
                        httpEntity = response.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                    } catch (IOException e) {
                        EventBus.getDefault().post(new MessageEvent(e, true));
                    }
                    return responseBody;

                case POST_WITH_IMAGE:
                    httpclient = getNewHttpClient();
                    postRequest = new HttpPost(map[0].get("url"));

                    map[0].remove("url");
                    reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                        }
                    });
                    BasicHttpContext localContext = new BasicHttpContext();
                    for (String key : map[0].keySet()) {
                        //Utils.SystemOutPrintln();
                        Utils.SystemOutPrintln(key, "====>" + map[0].get(key));
                        if (key.equals("image")) {
                            if (map[0].get(key) != null) {
                                if (map[0].get(key).length() > 1) {
                                    String type = "image/png";
                                    File f = new File(map[0].get(key));
                                   // new Compressor(this).compressToFile(f);
                                    FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                    reqEntity.addPart("image", fbody);
                                }
                            }
                        } else {
                            try {
                                reqEntity.addPart(key, new StringBody(map[0].get(key).replace(" ", "%20")));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    postRequest.setEntity(reqEntity);
                    HttpResponse responses = null;
                    try {
                        responses = httpclient.execute(postRequest, localContext);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(), StandardCharsets.UTF_8));
                        String sResponse;
                        while ((sResponse = reader.readLine()) != null) {
                            responseBody = responseBody + sResponse;
                        }
                    } catch (IOException e) {
                        EventBus.getDefault().post(new MessageEvent(e, true));
                    }
                    Utils.SystemOutPrintln("Responce ::", responseBody);
                    return responseBody;


                case Eval_POST_WITH_JSON:
                    String results = "";
                    httpClient = getNewHttpClient();
                    HttpPost httpPost = new HttpPost(map[0].get("url"));

                    nameValuePairs = new ArrayList<NameValuePair>();

                    for (String key : map[0].keySet()) {
                        Utils.SystemOutPrintln(key + "====>", map[0].get(key));
                        nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                    }
                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Utils.SystemOutPrintln("Final URI::", httpPost.getEntity().toString());
                    try {
                        response = httpClient.execute(httpPost);
                        httpEntity = response.getEntity();
                        results = EntityUtils.toString(httpEntity, "UTF-8");
                    } catch (IOException e) {
                        EventBus.getDefault().post(new MessageEvent(e, true));
                    }
                    Utils.SystemOutPrintln("Responce", "====>" + results);
                    return results;

                case POST_WITH_JSON:
                    String result = "";
                    httpClient = getNewHttpClient();
                    httpPost = new HttpPost(map[0].get("url"));
                    nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("StudID", map[0].get("StudID")));
                    nameValuePairs.add(new BasicNameValuePair("action", map[0].get("action")));
                    nameValuePairs.add(new BasicNameValuePair("data", map[0].get("json")));
                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    try {
                        httpResponse = httpClient.execute(httpPost);
                        httpEntity = httpResponse.getEntity();
                        result = EntityUtils.toString(httpEntity, "UTF-8");
                        Utils.SystemOutPrintln("Responce ::", result);
                    } catch (IOException e) {
                        EventBus.getDefault().post(new MessageEvent(e, true));
                    }
                    return result;
            }


            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            if (aListner != null && request !=null)
                aListner.onTaskCompleted(result, request);
            super.onPostExecute(result);
        }
    }

    public DefaultHttpClient HostVerifier() {
        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        DefaultHttpClient client = new DefaultHttpClient();

        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

// Set verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        return httpClient;
    }

    ///Change by Brainvire
    public DefaultHttpClient getNewHttpClient() {
        try {
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            return new DefaultHttpClient(params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

//    public DefaultHttpClient getNewHttpClient() {
//        try {
////            Code commented by Brainvire
//            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            trustStore.load(null, null);
//
//            MySSLSocketFactoryNew sf = new MySSLSocketFactoryNew(trustStore);
//            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//
//            HttpParams params = new BasicHttpParams();
//            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
//
//            SchemeRegistry registry = new SchemeRegistry();
//            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
//            registry.register(new Scheme("https", sf, 443));
//
//            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
//
//            return new DefaultHttpClient(ccm, params);
//        } catch (Exception e) {
//            return new DefaultHttpClient();
//        }
//    }
}
