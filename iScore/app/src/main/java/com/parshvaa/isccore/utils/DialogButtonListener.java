package com.parshvaa.isccore.utils;

public interface DialogButtonListener {
    void onPositiveButtonClicked();
    void onNegativButtonClicked();
}