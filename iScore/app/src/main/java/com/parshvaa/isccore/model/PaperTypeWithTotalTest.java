package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/20/2018.
 */

public class PaperTypeWithTotalTest implements Serializable{
    public String PaperTypeName="";
    public String PaperTypeID="";
    public String TotalTest="";
    public int Icone;
    public String Containt="";

    public String getContaint() {
        return Containt;
    }

    public void setContaint(String containt) {
        Containt = containt;
    }

    public int getIcone() {
        return Icone;
    }

    public void setIcone(int icone) {
        Icone = icone;
    }

    public String getPaperTypeName() {
        return PaperTypeName;
    }

    public void setPaperTypeName(String paperTypeName) {
        PaperTypeName = paperTypeName;
    }

    public String getPaperTypeID() {
        return PaperTypeID;
    }

    public void setPaperTypeID(String paperTypeID) {
        PaperTypeID = paperTypeID;
    }

    public String getTotalTest() {
        return TotalTest;
    }

    public void setTotalTest(String totalTest) {
        TotalTest = totalTest;
    }

    public String getLast_attempt() {
        return last_attempt;
    }

    public void setLast_attempt(String last_attempt) {
        this.last_attempt = last_attempt;
    }

    public String last_attempt="";

}
