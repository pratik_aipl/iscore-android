package com.parshvaa.isccore.db;

import android.util.Log;

import com.parshvaa.isccore.utils.Utils;

/**
 * Created by Karan - Empiere on 1/9/2018.
 */

public class DBNewQuery {
//    static String query = "";
private static final String TAG = "DBNewQuery";
    public static String get_subject_for_mcq_with_accuracy() {
        return "select s.*,(select CreatedOn  from student_mcq_test_hdr as sthd where sthd.SubjectID=s.SubjectID" +
                " order by CreatedOn desc limit 1) as Last_Date, count(smth.StudentMCQTestHDRID) as TotalSubjectTest" +
                " , ((  (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as" +
                " opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1 and opt.isCorrect=1 and EXISTS" +
                "  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID" +
                " and hdr.SubjectID=s.SubjectID ) )    *100) / (select count (StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl dtl where EXISTS  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where" +
                " hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=s.SubjectID )) ) as accuracy1" +
                " from subjects as s left join student_mcq_test_hdr as smth on s.SubjectID=smth.SubjectID" +
                " WHERE s.isMCQ = 1 " +
                " group by s.SubjectID order by s.SubjectOrder " +
                "";
    }

    public static String get_chapter_for_mcq_with_accuracy(String subID) {

        return "select main_c.*, (((" +
                " select count(smtd.StudentMCQTestDTLID) from student_mcq_test_dtl smtd " +
                " join mcqoption as mo on mo.MCQOPtionID=smtd.AnswerID " +
                " join mcqquestion as mq on mq.MCQQuestionID=mo.MCQQuestionID " +
                " where ((mq.ChapterID=main_c.ChapterID) or (mq.ChapterID in (select c.ChapterID from chapters as c where c.ParentChapterID=main_c.ChapterID))) AND mo.isCorrect=1 " +
                ")*100)/(select count(smtd.StudentMCQTestDTLID) from student_mcq_test_dtl smtd " +
                " join mcqoption as mo on mo.MCQOPtionID=smtd.AnswerID " +
                " join mcqquestion as mq on mq.MCQQuestionID=mo.MCQQuestionID " +
                " where ((mq.ChapterID=main_c.ChapterID) or (mq.ChapterID in (select c.ChapterID from chapters as c where c.ParentChapterID=main_c.ChapterID))))) " +
                "as acuracy" +
                " from chapters as main_c where main_c.ParentChapterID=0 AND main_c.SubjectID=" + subID + " AND main_c.isMCQ=1 ORDER BY main_c.DisplayOrder ASC ";

    }

    public static String get_child_chapter_for_mcq(String ChapterID) {
        return "select  * from chapters where ParentChapterID = " + ChapterID + " AND isMCQ = 1 " +
                "ORDER BY DisplayOrder ASC ";
    }

    public static String get_level_wise_accuraccy(String Level, String SubjectID, String chapterID) {
       /* return "select count(smth.StudentMCQTestHDRID) as TotalSubjectTest,(select count (StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl dtl where EXISTS  (select StudentMCQTestHDRID" +
                " from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID" +
                " and hdr.SubjectID=" + SubjectID + " and LevelID=" + Level + ")) as Total_attempt_level, (select count(StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID" +
                "  where dtl.IsAttempt=1 and opt.isCorrect=1 and EXISTS  (select StudentMCQTestHDRID" +
                " from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID" +
                " and hdr.SubjectID=" + SubjectID + " and LevelID=" + Level + ") ) as Right_Answer_level , ((  (select count(StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID" +
                "  where dtl.IsAttempt=1 and opt.isCorrect=1 and EXISTS  (select StudentMCQTestHDRID from" +
                " student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and" +
                " hdr.SubjectID=" + SubjectID + " and LevelID=" + Level + ") )    *100) / (select count (StudentMCQTestDTLID) from" +
                " student_mcq_test_dtl dtl where EXISTS  (select StudentMCQTestHDRID from student_mcq_test_hdr" +
                " hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + SubjectID + " and LevelID=" + Level + ")) )" +
                " as accuracy from student_mcq_test_hdr as smth where smth.SubjectID=" + SubjectID + "";*/
        String Query = "select (select count (StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqquestion as mq" +
                " on mq.MCQQuestionID=dtl.QuestionID  where mq.QuestionLevelID=" + Level + " AND mq.ChapterID IN(" + chapterID + ") and EXISTS" +
                "  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID" +
                " and hdr.SubjectID=" + SubjectID + ")) as Total_attempt_level, (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl" +
                " left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID" +
                " where dtl.IsAttempt=1 and opt.isCorrect=1  and mq.QuestionLevelID=" + Level + " AND mq.ChapterID IN(" + chapterID + ") and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr" +
                " where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + SubjectID + ")) as Right_Answer_level , (((select count(StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on " +
                "mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1 and mq.QuestionLevelID=" + Level + " AND mq.ChapterID IN(" + chapterID + ") and EXISTS (select StudentMCQTestHDRID" +
                " from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + SubjectID + "))    *100) / (select count (StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where mq.QuestionLevelID=" + Level + " AND mq.ChapterID IN(" + chapterID + ") and EXISTS" +
                "  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + SubjectID + ")))" +
                " as accuracy from student_mcq_test_hdr as smth where smth.SubjectID=" + SubjectID + " limit 1";

        Utils.Log("TAg ", "Digf LEVEL :-> " + Query);
        return Query;
    }


    public static String get_chapter_wise_accuraccy(String Level, String SubjectID, String chapterID) {
        String Query = "select (((select count(StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on " +
                "mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1  AND mq.ChapterID IN(" + chapterID + ") and EXISTS (select StudentMCQTestHDRID" +
                " from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + SubjectID + "))    *100) / (select count (StudentMCQTestDTLID)" +
                " from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where  mq.ChapterID IN(" + chapterID + ") and EXISTS" +
                "  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + SubjectID + ")))" +
                " as accuracy from student_mcq_test_hdr as smth where smth.SubjectID=" + SubjectID + " limit 1";


        return Query;
    }


    public static String get_mcq_option(String MCQQuestionID) {
        return " select  * from  mcqoption  where MCQQuestionID=" + MCQQuestionID;
    }


    //---------------- diff level START ----------------------//
    public static String get_mcq_notAppeard_diff(String boardID,String mediumId,String standardID,String SubID, String Level, String ChapterID) {
        String query = "select  * from  mcqquestion  where BoardID=" +boardID + "  AND MediumID=" + mediumId + "" +
                "  AND StandardID=" + standardID + "  AND SubjectID=" + SubID +
                "   AND ChapterID IN (" + ChapterID + ")" +
                " and MCQQuestionID in " +
                "(select QuestionID from student_not_appeared_question)  ORDER BY Random() LIMIT 20 ";
        Utils.Log("TAG", "student_not_appeared_question   " + query);
        return query;
    }

    public static String get_mcq_incorect_diff(String boardID,String mediumId,String standardID,String SubID, String Level, String ChapterID) {
        String query = "select  * from  mcqquestion  where BoardID=" +boardID + "  AND MediumID=" + mediumId + "" +
                "  AND StandardID=" + standardID + "  AND SubjectID=" + SubID +
                " AND ChapterID IN (" + ChapterID + ")" +
                " and MCQQuestionID in (select QuestionID from student_incorrect_question)  ORDER BY Random() LIMIT 20 ";

        Utils.Log("TAG", "student_incorrect_question   " + query);
        return query;
    }
    //---------------- diff level  END ----------------------//


    public static String get_mcq_random(String boardID,String mediumId,String standardID,String isMaped, String SubID, String Level, String ChapterID) {
        String query = "select  * from  mcqquestion  where SubjectID=" + SubID + "  AND QuestionLevelID in (" + Level + ")  AND ChapterID IN (" + ChapterID + ")";
        if (isMaped.equals("0")) {
            query += " AND BoardID=" + boardID + "  AND MediumID=" +mediumId+ "" + "  AND StandardID=" + standardID + "  AND SubjectID=" + SubID;
        }
        query += " ORDER BY Random() LIMIT 20 ";
        return query;
    }

    public static String get_mcq_notAppeard(String boardID,String mediumId,String standardID,String isMaped, String SubID, String Level, String ChapterID) {
        String query = "select  * from  mcqquestion  where SubjectID=" + SubID + "  AND QuestionLevelID in (" + Level + ")  AND ChapterID IN (" + ChapterID + ")";
        if (isMaped.equals("0")) {
            query += " AND BoardID=" + boardID+ "  AND MediumID=" + mediumId + "" + "  AND StandardID=" + standardID + "  AND SubjectID=" + SubID;

        }
        query += " and MCQQuestionID in (select QuestionID from student_not_appeared_question) ORDER BY Random() LIMIT 20 ";
        Utils.Log("TAG", "student_not_appeared_question :-> " + query);
        return query;

    }

    public static String get_mcq_incorect(String boardID,String mediumId,String standardID,String isMaped, String SubID, String Level, String ChapterID) {
//        String  query += "select  * from  mcqquestion  where SubjectID=" + SubID + "  AND QuestionLevelID in (" + Level + ")  AND ChapterID IN (" + ChapterID + ")";
        String query = "select  * from  mcqquestion  where SubjectID=" + SubID + "  AND QuestionLevelID in (" + Level + ")  AND ChapterID IN (" + ChapterID + ")";
        if (isMaped.equals("0")) {
            query += " AND BoardID=" + boardID+ "  AND MediumID=" + mediumId + "" + "  AND StandardID=" +standardID + "  AND SubjectID=" + SubID;
        }
        query += " and MCQQuestionID in (select QuestionID from student_incorrect_question) ORDER BY Random() LIMIT 20 ";
        Utils.Log("TAG", "student_incorrect_question :-> " + query);
        return query;
    }

    public static String get_mcq_incorect_and_notAppeard(String boardID,String mediumId,String standardID,String isMaped, String SubID, String Level, String ChapterID) {
        String query = "select  * from  mcqquestion  where SubjectID=" + SubID + "  AND QuestionLevelID in (" + Level + ")  AND ChapterID IN (" + ChapterID + ")";
        if (isMaped.equals("0")) {
            query += " AND BoardID=" + boardID + "  AND MediumID=" + mediumId + "" + "  AND StandardID=" + standardID + "  AND SubjectID=" + SubID;
        }
        query += " and MCQQuestionID in (select QuestionID from student_incorrect_question) OR (MCQQuestionID in (select QuestionID from student_not_appeared_question)) ORDER BY Random() LIMIT 20 ";
        return query;
    }

    public static String get_level_wise_report(String HdrID, String LevelID) {
        return "select count (StudentMCQTestDTLID) as Total_MCQ_Que,(select count(StudentMCQTestDTLID) as Total_Right  " +
                "  from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID " +
                "  left join mcqquestion as mq1 on mq1.MCQQuestionID=dtl.QuestionID  where dtl.IsAttempt=1   " +
                "  and opt.isCorrect=1 and StudentMCQTestHDRID==" + HdrID + " and mq1.QuestionLevelID=" + LevelID + ") " +
                "   as Total_right from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID   " +
                "   left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where  StudentMCQTestHDRID=" + HdrID + " and mq.QuestionLevelID=" + LevelID;

    }

    public static String getQuestions(String QuesTypeIDs, String Chapters) {
        return "SELECT  distinct qt.isPassage, `q`.`MQuestionID`, `q`.`QuestionTypeID`, `q`.`Question`, `q`.`Answer`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `q` JOIN `question_types` `qt` ON `q`.`QuestionTypeID` = `qt`.`QuestionTypeID` WHERE q.QuestionTypeID IN (" + QuesTypeIDs + ") AND q.ChapterID IN (" + Chapters + ") ORDER BY `qt`.`RevisionNo`,`q`.`QuestionTypeID` ";
    }

    public static String getSubject() {
        return "select * from subjects where isMCQ = 1 order by SubjectOrder";
    }

    public static String getPractiesSubject() {
        return "select * from subjects where isGeneratePaper = 1 order by SubjectOrder";
    }

    public static String getNoOfLastMCQTest() {
        return "select s.SubjectID,s.SubjectName, ((  (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1 and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID)*  100) / (select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl where dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) ) as accuracy1,(select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1 and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) as Total_Right,(select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl where dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) as Total_que, smth.StudentMCQTestHDRID, smth.CreatedOn from student_mcq_test_hdr as smth left join subjects as s on s.SubjectID=smth.SubjectID group by smth.StudentMCQTestHDRID order by smth.CreatedOn DESC";
    }

    public static String getFilterSubjectTest(String SubjectID, String LevelID) {

        String sql_query = "";

        sql_query = "select s.SubjectID,s.SubjectName,  ((  (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as" +
                " dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1" +
                " and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID)*  100) / " +
                "(select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl where" +
                " dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) ) " +
                "as accuracy1,(select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl " +
                "left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1 " +
                "and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) " +
                "as Total_Right,(select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl " +
                "where dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) as Total_que, smth.StudentMCQTestHDRID, smth.CreatedOn " +
                "from student_mcq_test_hdr as smth left join subjects as s on s.SubjectID=smth.SubjectID " +
                " where 1 ";
        if (!SubjectID.equals("")) {
            sql_query += " and s.SubjectID in(" + SubjectID + ") ";
        }

        if (!LevelID.equals("")) {
            sql_query += " and smth.StudentMCQTestHDRID IN (select StudentMCQTestHDRID from student_mcq_test_level where LevelID IN (" + LevelID + ")) ";
        }
        sql_query += " order by smth.CreatedOn DESC ";


        Utils.Log("TAg ", "Query " + sql_query);


        return sql_query;
    }


    public static String getAllSubjectPractiesPaper(String standardId) {
        return "SELECT s.*,(select count(StudentQuestionPaperID) from student_question_paper as" +
                " sqp where sqp.SubjectID = s.SubjectID) as TotalSubjectTest, (select CreatedOn from student_question_paper as" +
                " lsqp where lsqp.SubjectID = s.SubjectID order by lsqp.CreatedOn desc limit 1) as Last_Date" +
                " FROM subjects as s where s.StandardID=" + standardId + " AND s.isGeneratePaper='1'" +
                " ORDER BY s.SubjectOrder ASC";
    }

    public static String getPractiesPaperType(String SubjectID) {
        return "select pt.*, (select count(StudentQuestionPaperID) from student_question_paper as sqp " +
                "left  join Subjects as s on sqp.SubjectID=s.SubjectID where sqp.PaperTypeID = pt.PaperTypeID AND s.SubjectID= " + SubjectID + ")" +
                " as TotalTest, \n" +
                " (select lsqp.CreatedOn from student_question_paper as lsqp left\n" +
                " join Subjects as s on lsqp.SubjectID=s.SubjectID where lsqp.PaperTypeID = pt.PaperTypeID AND s.SubjectID=" + SubjectID +
                "                 order by lsqp.CreatedOn desc limit 1) as last_attempt  from paper_type as pt";


    }


    public static String getExamTypeTest(String examTypeID, String paperTypeId, String SubjectID) {
        return "select  et.*,(SELECT `Instruction` FROM `exam_type_pattern` WHERE `SubjectID` = " + SubjectID +
                " AND `ExamTypeID` = et.ExamTypeID LIMIT 1) as MainInstruction from  examtypes as et where ExamTypeID IN (" +
                examTypeID + ") AND (PaperType LIKE  '" + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + "' OR PaperType = '" + paperTypeId + "')";
    }

    public static String getChapterPractiesPaper(String SubjectID) {
        return "select  * from chapters where SubjectID = " + SubjectID + " AND ParentChapterID = 0 AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC ";

    }

    public static String getSubChapterPractiesPaper(String ParentChapterID) {
        return "select  * from chapters where ParentChapterID = " + ParentChapterID + " AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC ";
    }

    public static String getSubjectWiseQuesTypesSetpaper(String SubjectId, String ChapterIds) {
        String sql_query = "SELECT COUNT(qm.MQuestionID) as TotalQuestion, `qt`.`QuestionTypeID`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `qm` " +
                "JOIN `question_types` `qt` ON `qt`.`QuestionTypeID` = `qm`.`QuestionTypeID` " +
                "WHERE `qm`.`SubjectID` = '" + SubjectId + "' AND `qm`.`ChapterID` IN (" + ChapterIds + ")" +
                "GROUP BY `qt`.`QuestionTypeID`  ORDER BY `RevisionNo` ASC ";
//        Utils.Log("TAg", "Query :-> " + sql_query);
        return sql_query;


    }

    public static String getSetPaperQuestions(String StudentQuestionTypeID) {
        return "SELECT sspd.StudentSetPaperDetailID, qt.isPassage, mq.Question, mq.Answer, sspd.MQuestionID, qt.QuestionType, sspq.QuestionTypeID, sspq.TotalAsk, sspq.ToAnswer, sspq.TotalMark " +
                "FROM student_set_paper_question_type sspq " +
                "LEFT JOIN question_types qt ON qt.QuestionTypeID = sspq.QuestionTypeID " +
                "LEFT JOIN student_set_paper_detail sspd ON sspd.StudentSetPaperQuestionTypeID = sspq.StudentSetPaperQuestionTypeID " +
                "LEFT JOIN masterquestion mq on mq.MQuestionID=sspd.MQuestionID " +
                "WHERE sspq.StudentSetPaperQuestionTypeID " +
                "IN (" + StudentQuestionTypeID + ")  ORDER BY sspd.StudentSetPaperQuestionTypeID  ";
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT 1";
    }

    public static String getPrelimPaperQuestions(String StudentQuestionTypeID) {
        String Query = "select etpd." +
                "        ExamTypePatternDetailID, etpd.QuestionNo, etpd.SubQuestionNo, etpd.QuestionTypeText," +
                " etpd.isQuestion, etpd.PageNo, etpd.QuestionMarks,etpd.QuestionTypeID," +
                "                (select mq.Question from student_question_paper_detail as sqpd" +
                "        Left join masterquestion as mq on mq.MQuestionID = sqpd.MQuestionID" +
                "        where StudentQuestionPaperID = " + StudentQuestionTypeID + " AND sqpd." +
                "        ExamTypePatternDetailID = etpd.ExamTypePatternDetailID)as Question," +
                "        (select mq.Answer from student_question_paper_detail as sqpd" +
                "        Left join masterquestion as mq on mq.MQuestionID = sqpd.MQuestionID" +
                "        where StudentQuestionPaperID = " + StudentQuestionTypeID + " AND sqpd." +
                "        ExamTypePatternDetailID = etpd.ExamTypePatternDetailID)as Answer" +
                "        from exam_type_pattern_detail as etpd" +
                "        where etpd.ExamTypePatternID = (select ExamTypePatternID from student_question_paper where" +
                "        StudentQuestionPaperID = " + StudentQuestionTypeID + ")" +
                "        Order by etpd.DisplayOrder";
//        Utils.Log("TAG QUERY ::-->", Query);
        return Query;

    }

    public static String getPrelimPaperQuestionsIsPassage(String StudentQuestionTypeID) {
        String Query = "select etpd.ExamTypePatternDetailID, etpd.QuestionNo, etpd.SubQuestionNo, etpd.QuestionTypeText,\n" +
                "etpd.isQuestion, etpd.PageNo, etpd.QuestionMarks,etpd.QuestionTypeID,sqpd.StudentQuestionPaperDetailID,mq.MQuestionID," +
                " mq.Question, mq.Answer, qt.isPassage\n" +
                "from exam_type_pattern_detail as etpd\n" +
                "left join student_question_paper_detail as sqpd on (sqpd.StudentQuestionPaperID = " + StudentQuestionTypeID + " AND sqpd.\n" +
                "ExamTypePatternDetailID = etpd.ExamTypePatternDetailID)\n" +
                "Left join masterquestion as mq on mq.MQuestionID = sqpd.MQuestionID\n" +
                "Left join question_types as qt on qt.QuestionTypeID = mq.QuestionTypeID\n" +
                "where etpd.ExamTypePatternID = (select ExamTypePatternID from student_question_paper where\n" +
                "StudentQuestionPaperID = " + StudentQuestionTypeID + ")\n" +
                "Order by etpd.DisplayOrder";
//        Utils.Log("TAG QUERY ::-->", Query);
        return Query;

    }

    public static String getChapterGenrate(String SubjectID) {
        //    ////Utils.Log("TAG QUERY ::-->", "select  * from chapters where SubjectID = " + SubjectID + " AND ParentChapterID = 0 ORDER BY DisplayOrder ASC ");
        return "select  * from chapters where SubjectID = " + SubjectID + " AND ParentChapterID = 0 AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC ";

    }

    public static String getSubChapterGenrate(String ParentChapterID) {
        return "select  * from chapters where ParentChapterID = " + ParentChapterID + " AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC ";
    }

    public static String getAllSubjectGenrate(String bordId, String mediumId, String standardId) {

        Log.d(TAG, "getAllSubjectGenrate() called with: bordId = [" + bordId + "], mediumId = [" + mediumId + "], standardId = [" + standardId + "]");
        return "SELECT * FROM subjects where BoardID=" + bordId + " AND MediumID=" + mediumId + " AND StandardID=" + standardId+ " AND isGeneratePaper='1' ORDER BY SubjectOrder ASC";
    }

    public static String getSubjectWiseQuesTypes(String SubjectId, String ChapterIds) {
        //return "SELECT * FROM `question_types` where SubjectID='"+SubjectId+"' ORDER BY RevisionNo";
        return "SELECT `qt`.`QuestionTypeID`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `qm` " +
                "JOIN `question_types` `qt` ON `qt`.`QuestionTypeID` = `qm`.`QuestionTypeID` " +
                "WHERE `qm`.`SubjectID` = '" + SubjectId + "' AND `qm`.`ChapterID` IN (" + ChapterIds + ") " +
                "GROUP BY `qt`.`QuestionTypeID` ORDER BY `qt`.`RevisionNo` ASC";

    }

    //  --------------------------------- Board Question --------------------------------------------
    public static String getSubjectWiseBoardQuesTypes(String SubjectId, String ChapterIds) {
        //return "SELECT * FROM `question_types` where SubjectID='"+SubjectId+"' ORDER BY RevisionNo";
        return "SELECT `qt`.`QuestionTypeID`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `qm`" +
                " JOIN `question_types` `qt` ON `qt`.`QuestionTypeID` = `qm`.`QuestionTypeID`" +
                " WHERE `qm`.`SubjectID` = " + SubjectId + "  AND `qm`.`ChapterID` IN (" + ChapterIds + ") AND `qm`.isBoard=1 " +
                " GROUP BY `qt`.`QuestionTypeID` ORDER BY `qt`.`RevisionNo` ASC";

    }

    public static String getBoardQuestions(String QuesTypeIDs, String Chapters) {
        return "SELECT  distinct `q`.`MQuestionID`, `q`.`QuestionTypeID`, `q`.`Question`, `q`.`Answer`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `q` JOIN `question_types` `qt` ON `q`.`QuestionTypeID` = `qt`.`QuestionTypeID` WHERE q.QuestionTypeID IN (" + QuesTypeIDs + ") AND q.ChapterID IN (" + Chapters + ") AND q.isBoard=1 ORDER BY `qt`.`RevisionNo`,`q`.`QuestionTypeID` ";
    }
    //  --------------------------------- Board Question --------------------------------------------

    public static String getAllRows(String tableName) {
        return "select * from paper_type";
    }

    public static String getRecentPractisePaper() {

        return "SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn, s.SubjectName, et.ExamTypeName, et.Duration, et.TotalMarks,sqp.ExamTypePatternID,pt.PaperTypeName,sqp.PaperTypeID,sqp.TotalMarks AS SetPaperMarks,sqp.Duration AS SetPaperDuration" +
                " FROM student_question_paper sqp" +
                " LEFT JOIN exam_type_pattern etp ON sqp.ExamTypePatternID = etp.ExamTypePatternID" +
                " LEFT JOIN examtypes et ON etp.ExamTypeID = et.ExamTypeID" +
                " LEFT JOIN subjects s ON sqp.SubjectID = s.SubjectID" +
                " LEFT JOIN paper_type pt ON sqp.PaperTypeID=pt.PaperTypeID" +
                " ORDER BY sqp.StudentQuestionPaperID DESC ";
    }

    public static String getFilterPractisePaper(String studentId,String subjectId, String paperTypeID) {


        String sql_query = "";

        sql_query = "SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn as CreatedOn, s.SubjectName,et.Duration" +
                ", et.ExamTypeName, et.TotalMarks, sqp.PaperTypeID, pt.PaperTypeName, pt.PaperTypeID,sqp.TotalMarks AS SetPaperMarks,sqp.Duration AS SetPaperDuration,sqp.ExamTypePatternID " +
                "FROM `student_question_paper` `sqp` " +
                "LEFT JOIN `exam_type_pattern` `etp` ON `sqp`.`ExamTypePatternID` = `etp`.`ExamTypePatternID` " +
                "LEFT JOIN `examtypes` `et` ON `etp`.`ExamTypeID` = `et`.`ExamTypeID` " +
                "LEFT JOIN `paper_type` `pt` ON `sqp`.`PaperTypeID` = `pt`.`PaperTypeID` " +
                "LEFT JOIN `subjects` `s` ON `sqp`.`SubjectID` = `s`.`SubjectID` " +
                "WHERE `sqp`.`StudentID` = " + studentId;
        if (!subjectId.equals("")) {
            sql_query += " AND `sqp`.`SubjectID` in(" + subjectId + ") ";
        }

        if (!paperTypeID.equals("")) {
            sql_query += " AND `sqp`.PaperTypeID in(" + paperTypeID + ") ";
        }

        sql_query += " ORDER BY sqp.StudentQuestionPaperID DESC ";
        Utils.Log("TAg ", "Query " + sql_query);


        return sql_query;

    }

    public static String getSetPaperQuestionTypeID(String StudentQuestionPaperID) {
        return "select StudentSetPaperQuestionTypeID from student_set_paper_question_type where StudentQuestionPaperID=" + StudentQuestionPaperID;
    }

    public static String getLevelviseTestCount(String levelID) {
        //return "select * from student_mcq_test_level where LevelID =" + levelID;
        return "select StudentMCQTestHDRID from student_mcq_test_hdr where StudentMCQTestHDRID IN (select StudentMCQTestHDRID from  student_mcq_test_level  where\n" +
                "LevelID=" + levelID + ")";
    }

    public static String zooki() {
        return "select * from zooki";
    }

    public static String CheckQueIncorrect(String ID) {
        return "select * from student_incorrect_question where QuestionID = " + ID;
    }

    public static String CheckQueNotAppeared(String ID) {
        return "select * from student_not_appeared_question where QuestionID = " + ID;
    }

    public static String DeleteQueIncorrect(String ID) {
        return "DELETE from student_incorrect_question where QuestionID = " + ID;
    }

    public static String DelteQueNotAppeared(String ID) {
        return "DELETE  from student_not_appeared_question where QuestionID = " + ID;
    }

    public static String DelteTabelRecord(String name) {
        return "DELETE  from " + name;
    }

    public static String get_total_mcq_test_accuracy_by_student(String StartDate, String EndDate) {
        String QUERY = "SELECT ROUND(TotalRight/TotalQuestion) as Accuracy from (SELECT COUNT(`dtl`.`StudentMCQTestDTLID`)*100 as TotalRight FROM `student_mcq_test_dtl` `dtl` JOIN `mcqoption` `op`ON `dtl`.`AnswerID` = `op`.`MCQOPtionID` WHERE `dtl`.`IsAttempt` = 1 AND `op`.`isCorrect` = 1 AND dtl.StudentMCQTestHDRID IN(SELECT DISTINCT smth.StudentMCQTestHDRID as StudentMCQTestHDRID FROM `student_mcq_test_hdr` `smth` WHERE  `smth`.`CreatedOn` BETWEEN '" + StartDate + "' and '" + EndDate + "') ) x, (SELECT COUNT(`dtl`.`StudentMCQTestDTLID`) as TotalQuestion FROM `student_mcq_test_dtl` `dtl` WHERE StudentMCQTestHDRID IN(SELECT DISTINCT smth.StudentMCQTestHDRID as StudentMCQTestHDRID FROM `student_mcq_test_hdr` `smth` WHERE `smth`.`CreatedOn` BETWEEN '" + StartDate + "' and '" + EndDate + "')) y";
        Utils.Log("QUERY", "==>" + QUERY);
        return QUERY;
    }

    public static String get_mcq_subject_accuracy_by_student(String StartDate, String EndDate) {
        String QUERY = "select s.SubjectID, s.SubjectName, (SELECT ROUND(TotalRight/TotalQuestion)" +
                " from ( SELECT COUNT(`dtl`.`StudentMCQTestDTLID`)*100 as TotalRight FROM student_mcq_test_dtl dtl " +
                "JOIN `mcqoption` `op` ON `dtl`.`AnswerID` = `op`.`MCQOPtionID` WHERE `dtl`.`IsAttempt` = 1 AND " +
                "`op`.`isCorrect` = 1 AND dtl.StudentMCQTestHDRID IN(SELECT DISTINCT smth.StudentMCQTestHDRID as StudentMCQTestHDRID " +
                "FROM `student_mcq_test_hdr` `smth` WHERE `smth`.`SubjectID` = s.SubjectID AND " +
                "`smth`.`CreatedOn` BETWEEN '" + StartDate + "' and '" + EndDate + "') ) x, " +
                "(SELECT COUNT(`dtl`.`StudentMCQTestDTLID`) as TotalQuestion FROM student_mcq_test_dtl dtl WHERE " +
                "StudentMCQTestHDRID IN(SELECT DISTINCT smth.StudentMCQTestHDRID as StudentMCQTestHDRID " +
                "FROM `student_mcq_test_hdr` `smth` WHERE `smth`.`SubjectID` = s.SubjectID AND `smth`.`CreatedOn` " +
                "BETWEEN '" + StartDate + "' and '" + EndDate + "') )) as Accuracy, " +
                "(select count(StudentQuestionPaperID) as Total from student_question_paper where SubjectID=s.SubjectID and " +
                "CreatedOn BETWEEN '" + StartDate + "' and '" + EndDate + "') as TotalPaper from subjects as s";
        return QUERY;

    }

    public static String get_paper_test_details(String StartDate, String EndDate) {

        return "select count(StudentQuestionPaperID) as TotalPaper from student_question_paper where CreatedOn between '" + StartDate + "' and '" + EndDate + "'";
    }

    public static String getNotificaton() {

        return "Select * from notification";
    }

    public static String getNotificatonDash(int id) {
        Utils.Log("TAG", "query:-> " + "Select * from notification where _id > " + id);
        return "Select * from notification where _id > " + id;
    }

    public static String DeleteStudent_question_paper(int ID) {
        return "DELETE from student_question_paper where StudentQuestionPaperID = " + ID;
    }

    public static String DeleteStudent_set_question_paper_detal(int ID) {
        return "DELETE from student_set_paper_detail where StudentSetPaperQuestionTypeID = " + ID;
    }

    public static String DeleteStudent_set_question_paper_Chapter(int ID) {
        return "DELETE from student_question_paper_chapter where StudentQuestionPaperID = " + ID;
    }

    public static String DeleteStudent_set_question_type(int ID) {
        return "DELETE from student_set_paper_question_type where StudentQuestionPaperID = " + ID;
    }

}