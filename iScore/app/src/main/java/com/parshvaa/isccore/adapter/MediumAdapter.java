package com.parshvaa.isccore.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.parshvaa.isccore.model.MediumModel;
import com.parshvaa.isccore.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 12/25/2017.
 */

public class MediumAdapter extends RecyclerView.Adapter<MediumAdapter.ViewHolder> {
    private ArrayList<MediumModel> mediumArrayList;
    private Context context;

    private int lastSelectedPosition = -1;
    public String mediumId = "";
    public String mediumNamee = "";

    public MediumAdapter(ArrayList<MediumModel> offersListIn, Context ctx) {
        mediumArrayList = offersListIn;
        context = ctx;
    }

    @Override
    public MediumAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.board_item, parent, false);

        MediumAdapter.ViewHolder viewHolder =
                new MediumAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MediumAdapter.ViewHolder holder,
                                 int position) {
        MediumModel mediumObj = mediumArrayList.get(position);
        holder.selectionState.setText(mediumObj.getMediumName());

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
        holder.selectionState.setChecked(lastSelectedPosition == position);
    }

    @Override
    public int getItemCount() {
        return mediumArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RadioButton selectionState;

        public ViewHolder(View view) {
            super(view);
            selectionState = view.findViewById(R.id.redio_board);

            selectionState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    mediumId = mediumArrayList.get(lastSelectedPosition).getMediumID();
                    mediumNamee = mediumArrayList.get(lastSelectedPosition).getMediumName();

                    notifyDataSetChanged();


                }
            });
        }
    }
    public String getSelectedItem() {
        return mediumId;
    }
    public String getMediumNamee() {
        return mediumNamee;
    }
}