package com.parshvaa.isccore.activity.practiespaper.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.model.SetPaperQuestionTypes;
import com.parshvaa.isccore.R;

import java.util.ArrayList;

import static androidx.recyclerview.widget.RecyclerView.ViewHolder;

/**
 * Created by empiere-vaibhav on 1/3/2018.
 */

public class SetPaperTypePrivewAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<SetPaperQuestionTypes> setPaperQuestionTypesArray;
    public Context context;
    public int row_index = 0;
    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_FOOTER = 1;
    private LayoutInflater mInflater;

    public SetPaperTypePrivewAdapter(ArrayList<SetPaperQuestionTypes> setPaperQuestionTypesArray, Context context) {
        this.setPaperQuestionTypesArray = setPaperQuestionTypesArray;
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new ViewHolderiItem(mInflater.inflate(R.layout.set_paper_privew_raw, parent, false));
        } else
            return new ViewHolderFooter(mInflater.inflate(R.layout.set_paper_privew_footer_raw, parent, false));
    }

    //return new MyViewHolder(itemView, new CustomEtListener(), new ToAskWatcher(), new ToAnswerWatcher());
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderiItem offerHolder = (ViewHolderiItem) holder;
                bindHeaderItem(offerHolder, position);
                break;
            case 1:
                ViewHolderFooter addrHolder = (ViewHolderFooter) holder;
                bindFooter(addrHolder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == (setPaperQuestionTypesArray.size() - 1)) {
            return VIEW_TYPE_FOOTER;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    public SetPaperQuestionTypes getItem(int position) {

        return setPaperQuestionTypesArray.get(position);
    }

    class ViewHolderiItem extends ViewHolder {
        public TextView tv_question, tv_marks, tv_total_ask, tv_total_ans, tv_total_marks;
        public ImageView img_icon;
        public RelativeLayout rel_std;

        public ViewHolderiItem(View view) {
            super(view);
            tv_question = view.findViewById(R.id.tv_que);
            tv_marks = view.findViewById(R.id.tv_marks);
            tv_total_ask = view.findViewById(R.id.tv_total_ask);
            tv_total_ans = view.findViewById(R.id.tv_total_ans);
            tv_total_marks = view.findViewById(R.id.tv_total_marks);
        }
    }

    public void bindHeaderItem(final ViewHolderiItem holder, final int position) {
        SetPaperQuestionTypes movie = setPaperQuestionTypesArray.get(position);

        holder.tv_question.setText(Html.fromHtml("<strong>" + (position + 1) + ".</strong> " + movie.getQuestionType()));
        holder.tv_marks.setText(movie.getMarks() + "");
        holder.tv_total_ask.setText(movie.getNoOfQuestion() + "");
        holder.tv_total_ans.setText(movie.getNoOfAnswer() + "");
        holder.tv_total_marks.setText(movie.getNoOfMarks() + "");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
            }
        });
    }

    class ViewHolderFooter extends ViewHolder {
        public TextView tv_question, tv_marks, tv_total_ask, tv_total_ans, tv_total_marks;
        public ImageView img_icon;
        public RelativeLayout rel_std;
        public ViewHolderFooter(View view) {
            super(view);
            tv_question = view.findViewById(R.id.tv_que);
            tv_marks = view.findViewById(R.id.tv_marks);
            tv_total_ask = view.findViewById(R.id.tv_total_ask);
            tv_total_ans = view.findViewById(R.id.tv_total_ans);
            tv_total_marks = view.findViewById(R.id.tv_total_marks);
        }
    }

    public void bindFooter(final ViewHolderFooter holder, final int position) {
        SetPaperQuestionTypes movie = setPaperQuestionTypesArray.get(position);

        holder.tv_question.setText(Html.fromHtml("<strong>" + (position + 1) + ".</strong> " + movie.getQuestionType()));
        holder.tv_marks.setText(movie.getMarks() + "");
        holder.tv_total_ask.setText(movie.getNoOfQuestion() + "");
        holder.tv_total_ans.setText(movie.getNoOfAnswer() + "");
        holder.tv_total_marks.setText(movie.getNoOfMarks() + "");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
            }
        });

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View view) {
            super(view);


        }
    }




    public String getTotalMarks() {

        double TotalMark = 0.0;
        SetPaperQuestionTypes types = null;
        for (int i = 0; i < setPaperQuestionTypesArray.size(); i++) {
            types = setPaperQuestionTypesArray.get(i);
            if (types.isSelected == -2)
                TotalMark = TotalMark + Double.parseDouble(String.valueOf(types.getNoOfMarks()));
        }
        return String.valueOf((int) TotalMark);
    }

    @Override
    public int getItemCount() {
        return setPaperQuestionTypesArray.size();
    }
}
