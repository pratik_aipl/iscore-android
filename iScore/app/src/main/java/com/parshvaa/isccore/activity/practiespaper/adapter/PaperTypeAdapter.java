package com.parshvaa.isccore.activity.practiespaper.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.PaperTypeInterface;
import com.parshvaa.isccore.model.PaperTypeWithTotalTest;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 1/3/2018.
 */

public class PaperTypeAdapter extends RecyclerView.Adapter<PaperTypeAdapter.MyViewHolder> {

    private static final String TAG = "PaperTypeAdapter";
    private ArrayList<PaperTypeWithTotalTest> paperTypeList;
    public Context context;
    public int row_index = -1;
    public PaperTypeInterface paperTypeInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_total_test, tv_last_attempt, tv_containt;
        public ImageView img_icon;
        public RelativeLayout rel_std;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_total_test = view.findViewById(R.id.tv_total_test);
            tv_last_attempt = view.findViewById(R.id.tv_last_attempt);
            tv_containt = view.findViewById(R.id.tv_containt);
            img_icon = view.findViewById(R.id.img_icon);
            rel_std = view.findViewById(R.id.rel_std);

        }
    }


    public PaperTypeAdapter(ArrayList<PaperTypeWithTotalTest> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;
        this.context = context;
        paperTypeInterface = (PaperTypeInterface) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_set_paper_type_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final PaperTypeWithTotalTest paperType = paperTypeList.get(position);
        holder.tv_subject_name.setText(paperType.getPaperTypeName());


        if (row_index == position) {
            holder.rel_std.setBackgroundResource(R.drawable.squar_thik_border_red);

        } else {
            holder.rel_std.setBackgroundColor(Color.parseColor("#ffffff"));

        }
        holder.img_icon.setImageResource(paperType.getIcone());
        holder.tv_containt.setText(paperType.getContaint());
        holder.tv_total_test.setText(Html.fromHtml("<font color=\"#4e4e4e\">TOTAL TEST : </font><font color=\"#0d4568\">" + paperType.getTotalTest() + "</font>"));

        Log.d(TAG, "onBindViewHolder: "+paperType.getLast_attempt());
        if (paperType.getLast_attempt() == null || paperType.getLast_attempt().equals(""))
            holder.tv_last_attempt.setText(Html.fromHtml("<font color=\"#4e4e4e\">LAST ATTEMPT : </font><font color=\"#0d4568\">N/A</font>"));
        else {
            String date = paperType.getLast_attempt();
            date = date.substring(0, 10);

            holder.tv_last_attempt.setText(Html.fromHtml("<font color=\"#4e4e4e\">LAST ATTEMPT : </font><font color=\"#0d4568\">" + Utils.changeDateToDDMMYYYY(date) + "</font>"));

        }


        // paperTypeInterface.getPosition(0);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
                if (position == 0) {
                    paperTypeInterface.getPosition(0);

                } else if (position == 1) {
                    paperTypeInterface.getPosition(1);

                } else if (position == 2) {
                    paperTypeInterface.getPosition(2);

                }

                App.practiesPaperObj.setTotalTest(paperType.getTotalTest());

            }
        });


    }


    @Override
    public int getItemCount() {
        return paperTypeList.size();
    }
}
