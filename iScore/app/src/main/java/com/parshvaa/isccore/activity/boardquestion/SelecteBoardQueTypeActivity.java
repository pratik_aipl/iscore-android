package com.parshvaa.isccore.activity.boardquestion;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.revisionnote.adapter.SelectQueTypeAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.ChapterInterface;
import com.parshvaa.isccore.model.QuestionTypes;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;

public class SelecteBoardQueTypeActivity extends BaseActivity implements ChapterInterface {
    public MyDBManager mDb;
    public Subject SelsubjectObj = null;
    public String chapterIds;
    public String chapterNumbers;


    public ArrayList<String> srrQuesTypes = new ArrayList<>();
    public ArrayList<String> srrQuesTypesIds = new ArrayList<>();
    public QuestionTypes questionTypesObj = null;
    public CursorParserUniversal cParse;
    public ArrayList<QuestionTypes> questionTypes = new ArrayList<>();
    public RecyclerView recycle_que_type;
    public SelectQueTypeAdapter selectQueTypeAdapter;
    public Button btn_generate;
    public CheckBox cbSelectAll;
    public ImageView img_back;
    public TextView tv_title;
    public SelecteBoardQueTypeActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_select_que_type);
        Utils.logUser();
        instance = this;
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        cParse = new CursorParserUniversal();
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Select Question Type");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recycle_que_type = findViewById(R.id.recycle_que_type);
        btn_generate = findViewById(R.id.btn_generate);
        cbSelectAll = findViewById(R.id.cbSelectAll);

        chapterIds = getIntent().getExtras().getString("ChapterIds");
        chapterNumbers = getIntent().getExtras().getString("ChapterNumber");

        getQuesTypes();


    }


    public void getQuesTypes() {

        try {
            questionTypes = (ArrayList) new CustomDatabaseQuery(this, new QuestionTypes())
                    .execute(new String[]{DBNewQuery.getSubjectWiseBoardQuesTypes(App.subObj.getSubjectID(), chapterIds)}).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (questionTypes.size() > 0) {
            setupRecyclerView();
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(instance);
            builder1.setMessage("No Board Questions found");
            builder1.setCancelable(true);
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "Okay",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                            finish();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }


        btn_generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectQueTypeAdapter.getSelectedQuestionType().equals("") || selectQueTypeAdapter.getSelectedQuestionType().isEmpty()) {
                    Utils.showToast("Please select the question type.", instance);
                    return;
                }
                startActivity(new Intent(instance, BoardPreviewRevisionNoteActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("ChapterIds", chapterIds)
                        .putExtra("ChapterNumber", chapterNumbers)
                        .putExtra("selectedQuesTypeIDs", selectQueTypeAdapter.getSelectedQuestionType())

                );
            }
        });
    }


    private void setupRecyclerView() {
        final Context context = recycle_que_type.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycle_que_type.setLayoutAnimation(controller);
        recycle_que_type.scheduleLayoutAnimation();
        recycle_que_type.setLayoutManager(new LinearLayoutManager(context));
        selectQueTypeAdapter = new SelectQueTypeAdapter(questionTypes, context, cbSelectAll);
        recycle_que_type.setAdapter(selectQueTypeAdapter);
        recycle_que_type.addItemDecoration(new ItemOffsetDecoration(spacing));
        cbSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cbSelectAll.isChecked()) {
                    selectQueTypeAdapter.selectAll();
                    selectQueTypeAdapter.notifyDataSetChanged();
                } else {
                    selectQueTypeAdapter.deSelectAll();
                    selectQueTypeAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onCheckBoxDeselect(boolean b) {
        cbSelectAll.setChecked(b);

    }
}
