package com.parshvaa.isccore.model;

public class TestType {
    public String testName;

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public TestType(String testName) {
        this.testName = testName;

    }
}
