package com.parshvaa.isccore.activity.searchpaper.practisepaper.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.parshvaa.isccore.activity.searchpaper.feftdrawer.LeftSubjectPractiesDrawerFragment;
import com.parshvaa.isccore.activity.searchpaper.practisepaper.adapter.PractcePaperAdapter;
import com.parshvaa.isccore.activity.searchpaper.practisepaper.model.PractisePaperModel;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.FilterArrayListListener;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseFragment;
import com.parshvaa.isccore.tempmodel.PractiesPaper;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 1/10/2018.
 */

public class PracticePaperFragment extends BaseFragment implements FilterArrayListListener {
    public View v;
    RelativeLayout left_button, right_button;
    private LeftSubjectPractiesDrawerFragment left_drawer;
    public RightPaperTypeDrawer right_drawer;

    public RelativeLayout mainLay, mainLayout;
    public DrawerLayout drawerLayout;
    public PractisePaperModel practiseObj;
    private ArrayList<PractisePaperModel> practiseArray = new ArrayList<>();
    public PractcePaperAdapter practiseAdapter;

    public RecyclerView recycler_practise_paper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_practice_paper, container, false);

        mainLay = v.findViewById(R.id.mainLay);
        mainLayout = v.findViewById(R.id.mainLayout);
        left_button = v.findViewById(R.id.leftt_button);
        right_button = v.findViewById(R.id.rightt_button);
        drawerLayout = v.findViewById(R.id.drawer_layout);
        recycler_practise_paper = v.findViewById(R.id.recycler_practise_paper);
        left_drawer = (LeftSubjectPractiesDrawerFragment) getActivity().getFragmentManager().findFragmentById(R.id.fragment_lleft_drawer);
        right_drawer = (RightPaperTypeDrawer) getActivity().getFragmentManager().findFragmentById(R.id.fragment_rright_drawer);
        right_drawer.setUpRightButton(R.id.fragment_right_drawer, drawerLayout, right_button, mainLay);
        left_drawer.setUpButton(R.id.fragment_left_drawer, drawerLayout, left_button, mainLayout);
        left_drawer.setDrawerListener(PracticePaperFragment.this);
        right_drawer.setDrawerListener(PracticePaperFragment.this);

        App.practiesPaperObj = new PractiesPaper();
        App.checked_subject.clear();
        App.checked_paper_type.clear();
        new getRecent(getActivity()).execute();

        return v;
    }


    @Override
    public void onLeftDrawerItemSelected() {
        FilterSubjectTest(android.text.TextUtils.join(",", App.checked_subject), android.text.TextUtils.join(",", App.checked_paper_type));
    }

    @Override
    public void onRightDrawerItemSelected() {
        //new getFilterData(getActivity(), android.text.TextUtils.join(",", App.checked_subject), android.text.TextUtils.join(",", App.checked_paper_type)).execute();
        FilterSubjectTest(android.text.TextUtils.join(",", App.checked_subject), android.text.TextUtils.join(",", App.checked_paper_type));
    }

    private void FilterSubjectTest(String subjectId, String paperTypeId) {
        try {
/*

            practiseArray.clear();

            practiseArray = (ArrayList) new CustomDatabaseQuery(getActivity(), new PractisePaperModel())
                    .execute(new String[]{DBNewQuery.getRecentPractisePaper()}).get();
            practiseAdapter = new PractcePaperAdapter(practiseArray, getActivity());

            recycler_practise_paper.setAdapter(practiseAdapter);
            practiseAdapter.notifyDataSetChanged();
*/


            new getFilterData(getActivity(), subjectId, paperTypeId).execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class getRecent extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;


        private getRecent(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {


            practiseArray.clear();

            Cursor c = mDb.getAllRows(DBNewQuery.getRecentPractisePaper());

            if (c != null && c.moveToFirst()) {
                int i = c.getCount();
                do {
                    if (c.getString(c.getColumnIndex("PaperTypeID")).equals("3") ||
                            c.getString(c.getColumnIndex("PaperTypeID")).equals(3)) {
                        practiseObj = new PractisePaperModel();
                        practiseObj.setHeaderID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));
                        practiseObj.setSubjectName(c.getString(c.getColumnIndex("SubjectName")));
                        practiseObj.setCreatedOn(c.getString(c.getColumnIndex("CreatedOn")));
                        practiseObj.setExamTypeName(c.getString(c.getColumnIndex("PaperTypeName")));
                        practiseObj.setTotalMarks(c.getString(c.getColumnIndex("SetPaperMarks")));
                        practiseObj.setExamTypePatternID(c.getString(c.getColumnIndex("ExamTypePatternID")));
                        practiseObj.setDuration(c.getString(c.getColumnIndex("SetPaperDuration")));
                        practiseObj.setPaperTypeID("3");
                        practiseObj.setTestNO(String.valueOf(i));
                        practiseObj.setStudentQuestionPaperID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));
                       
                        practiseArray.add(practiseObj);
                    } else {
                        practiseObj = new PractisePaperModel();
                        practiseObj.setHeaderID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));
                        practiseObj.setSubjectName(c.getString(c.getColumnIndex("SubjectName")));
                        practiseObj.setCreatedOn(c.getString(c.getColumnIndex("CreatedOn")));
                        practiseObj.setExamTypeName(c.getString(c.getColumnIndex("ExamTypeName")));
                        practiseObj.setTotalMarks(c.getString(c.getColumnIndex("TotalMarks")));
                        practiseObj.setExamTypePatternID(c.getString(c.getColumnIndex("ExamTypePatternID")));
                        practiseObj.setDuration(c.getString(c.getColumnIndex("Duration")));
                        practiseObj.setTestNO(String.valueOf(i));
                        practiseObj.setStudentQuestionPaperID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));


                        practiseArray.add(practiseObj);
                    }
                    i--;
                } while
                        (c.moveToNext());
                c.close();
            } else {


            }

            setupRecyclerView();
            return "";
        }
    }

    public class getFilterData extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectID, paperTypeId;


        private getFilterData(Context context, String SubjectID, String paperTypeId) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            this.paperTypeId = paperTypeId;
            this.SubjectID = SubjectID;
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {
            showProgress(true);//Utils.showProgressDialog(getActivity());
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {


            practiseArray.clear();
            if ( mySharedPref !=null && !TextUtils.isEmpty(mySharedPref.getStudent_id())){
            Cursor c = mDb.getAllRows(DBNewQuery.getFilterPractisePaper(mySharedPref.getStudent_id(),SubjectID, paperTypeId));
            if (c != null && c.moveToFirst()) {
                int i = c.getCount();
                do {

                    if (c.getString(c.getColumnIndex("PaperTypeID")).equals("3")) {
                        practiseObj = new PractisePaperModel();
                        practiseObj.setHeaderID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));
                        practiseObj.setSubjectName(c.getString(c.getColumnIndex("SubjectName")));
                        practiseObj.setCreatedOn(c.getString(c.getColumnIndex("CreatedOn")));
                        practiseObj.setExamTypeName(c.getString(c.getColumnIndex("PaperTypeName")));
                        practiseObj.setTotalMarks(c.getString(c.getColumnIndex("SetPaperMarks")));
                        practiseObj.setExamTypePatternID(c.getString(c.getColumnIndex("ExamTypePatternID")));
                        practiseObj.setDuration(c.getString(c.getColumnIndex("SetPaperDuration")));
                        practiseObj.setPaperTypeID("3");
                        practiseObj.setTestNO(String.valueOf(i));
                        practiseObj.setStudentQuestionPaperID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));
                        Utils.Log("TAG Duration PAPER:: ", "SET PAPER");
                        Utils.Log("TAG Duration:: ", practiseObj.getDuration());
                        practiseArray.add(practiseObj);
                    } else {
                        Utils.Log("TAG IN ", "PAPER Id 1");
                        practiseObj = new PractisePaperModel();
                        practiseObj.setHeaderID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));
                        practiseObj.setSubjectName(c.getString(c.getColumnIndex("SubjectName")));
                        practiseObj.setCreatedOn(c.getString(c.getColumnIndex("CreatedOn")));
                        practiseObj.setExamTypeName(c.getString(c.getColumnIndex("ExamTypeName")));
                        practiseObj.setTotalMarks(c.getString(c.getColumnIndex("TotalMarks")));
                        practiseObj.setExamTypePatternID(c.getString(c.getColumnIndex("ExamTypePatternID")));
                        practiseObj.setDuration(c.getString(c.getColumnIndex("Duration")));
                        practiseObj.setTestNO(String.valueOf(i));
                        practiseObj.setStudentQuestionPaperID(c.getString(c.getColumnIndex("StudentQuestionPaperID")));

                        Utils.Log("TAG Duration:: ", practiseObj.getDuration());
                        practiseArray.add(practiseObj);
                    }
                    i--;
                } while
                        (c.moveToNext());

                c.close();
            } else {
                new Runnable() {

                    @Override
                    public void run() {
                        Utils.showToast("No Paper Found", getActivity());
                    }
                };

            }
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showProgress(false);
            practiseAdapter = new PractcePaperAdapter(practiseArray, getActivity());
            recycler_practise_paper.setAdapter(practiseAdapter);
            practiseAdapter.notifyDataSetChanged();
        }
    }

    private void setupRecyclerView() {
        final Context context = recycler_practise_paper.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_practise_paper.setLayoutAnimation(controller);
        recycler_practise_paper.scheduleLayoutAnimation();
        recycler_practise_paper.setLayoutManager(new LinearLayoutManager(context));
        practiseAdapter = new PractcePaperAdapter(practiseArray, context);
        recycler_practise_paper.setAdapter(practiseAdapter);
    }


}
