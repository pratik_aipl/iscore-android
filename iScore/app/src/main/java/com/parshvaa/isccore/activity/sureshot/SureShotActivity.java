package com.parshvaa.isccore.activity.sureshot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;


import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SureShotActivity extends BaseActivity implements AsynchTaskListner {
    public View v;
    public MyDBManager mDb;
    public JsonParserUniversal jParser;
    public App app;
    public CursorParserUniversal cParse;
    public ArrayList<SureShot> sureShotArrayList = new ArrayList<>();
    public SureShot sureShot;
    public RecyclerView recycler_sure_shot;
    public SureShotAdapter adapter;
    public SureShotActivity instance;
    public ImageView img_back;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_sure_shot);
        Utils.logUser();

        instance=this;
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        recycler_sure_shot = findViewById(R.id.recycler_sure_shot);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Sure Shot");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        jParser = new JsonParserUniversal();
        showProgress(true, true);
        new CallRequest(instance).get_sure_shot();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {

                switch (request) {

                    case get_sure_shot:
                        showProgress(false, true);
                        JSONObject jObj = new JSONObject(result);
                        sureShotArrayList.clear();
                        if (jObj.getBoolean("status")) {
                            if (jObj.getJSONArray("SureShot") != null) {
                                JSONArray jData = jObj.getJSONArray("SureShot");
                                for (int i = 0; i < jData.length(); i++) {
                                    JSONObject jBoard = jData.getJSONObject(i);
                                    sureShot = new SureShot();
                                    sureShot.setQFile(jBoard.getString("QFile"));
                                    sureShot.setQAFile(jBoard.getString("QAFile"));
                                    sureShot.setSureShotPaperName(jBoard.getString("SureShotPaperName"));
                                    sureShot.setAnswerPaperName(jBoard.getString("AnswerPaperName"));
                                    sureShot.setPaperName(jBoard.getString("PaperName"));
                                    sureShot.setSureShotPaperID(jBoard.getString("SureShotPaperID"));
                                    sureShotArrayList.add(sureShot);
                                }   setupRecyclerView(sureShotArrayList);
                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(instance);
                            builder1.setMessage(jObj.getString("message"));
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            onBackPressed();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private void setupRecyclerView(ArrayList<SureShot> sureShotArrayList) {
        final Context context = recycler_sure_shot.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);
        recycler_sure_shot.setLayoutAnimation(controller);
        recycler_sure_shot.scheduleLayoutAnimation();
        recycler_sure_shot.setLayoutManager(new LinearLayoutManager(context));
        adapter= new SureShotAdapter(sureShotArrayList, context);
        recycler_sure_shot.setAdapter(adapter);
    }
}
