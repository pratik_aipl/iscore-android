package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 6/4/2018.
 */

public class SubQueLabel implements Serializable {
    public String Label="";

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public ArrayList<SubQueKey> keyArray = new ArrayList<>();
}
