package com.parshvaa.isccore.custominterface;

/**
 * Created by Karan - Empiere on 3/14/2017.
 */

public interface ChapterInterface {

    void onCheckBoxDeselect(boolean b);
}
