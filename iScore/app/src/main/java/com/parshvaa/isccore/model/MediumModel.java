package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 12/25/2017.
 */

public class MediumModel implements Serializable{
    public String MediumID;
    public String BoardID;
    public String MediumName;
    public String CreatedBy;
    public String CreatedOn;
    public String ModifiedBy;
    public String ModifiedOn;


    public MediumModel() {

    }

    public MediumModel(String MediumID, String BoardID, String MediumName, String CreatedBy,
                  String CreatedOn, String ModifiedBy, String ModifiedOn) {

        this.MediumID = MediumID;
        this.BoardID = BoardID;
        this.MediumName = MediumName;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;

    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
