package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/3/2017.
 */

public class TempMcqTestDtl implements Serializable {


    public int getTempMCQTestDTLID() {
        return TempMCQTestDTLID;
    }

    public void setTempMCQTestDTLID(int tempMCQTestDTLID) {
        TempMCQTestDTLID = tempMCQTestDTLID;
    }

    public String getTempMCQTestHDRID() {
        return TempMCQTestHDRID;
    }

    public void setTempMCQTestHDRID(String tempMCQTestHDRID) {
        TempMCQTestHDRID = tempMCQTestHDRID;
    }
    public String QuestionTime="";

    public String getQuestionTime() {
        return QuestionTime;
    }

    public void setQuestionTime(String questionTime) {
        QuestionTime = questionTime;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getIsAttempt() {
        return IsAttempt;
    }

    public void setIsAttempt(String isAttempt) {
        IsAttempt = isAttempt;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String TempMCQTestHDRID = "",
            QuestionID = "", AnswerID = "", IsAttempt = "",
            srNo = "", CreatedBy = "", CreatedOn = "", ModifiedBy = "", ModifiedOn = "";
    public int TempMCQTestDTLID = -1;
}
