package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 1/16/2018.
 */

public class TempQuestionTypes implements Serializable {
    public String MQuestionID = "";
    public String QuestionTypeID = "";
    public String Question = "";
    public String Answer = "";
    public String QuestionType = "";
    public boolean isSolution = false;
    public boolean isQuestionType = false;
    public String Type = "";

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(String questionType) {
        QuestionType = questionType;
    }

    public String getMarks() {
        return Marks;
    }

    public void setMarks(String marks) {
        Marks = marks;
    }

    public String Marks = "";
    public String isPassage = "";

    public String getIsPassage() {
        return isPassage;
    }

    public void setIsPassage(String isPassage) {
        this.isPassage = isPassage;
    }

    public ArrayList<TempQuestionTypes> tempQuestionTypesArrayList = new ArrayList<>();


}
