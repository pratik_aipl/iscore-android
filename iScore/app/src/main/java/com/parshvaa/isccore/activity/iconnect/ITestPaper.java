package com.parshvaa.isccore.activity.iconnect;

import java.io.Serializable;

public class ITestPaper implements Serializable {
    public String PaperID = "";
    public String SubjectName = "";
    public String PaperDate = "";
    public String isOnlyQuestionPaper = "";
    public String AnswerPaperDate = "";
    public String StartTime = "";
    public String EndTime = "";
    public String UploadStartTime = "";
    public String UploadEndTime = "";
    public String QuestionPaperDate = "";
    public String Duration = "";
    public String OnlyStartTime = "";
    public String OnlyEndTime = "";
    public String Type = "";
    public String QuestionPaperFile = "";
    public String HistoryID = "";

    public String getHistoryID() {
        return HistoryID;
    }

    public void setHistoryID(String historyID) {
        HistoryID = historyID;
    }

    public String getQuestionPaperFile() {
        return QuestionPaperFile;
    }

    public void setQuestionPaperFile(String questionPaperFile) {
        QuestionPaperFile = questionPaperFile;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getOnlyEndTime() {
        return OnlyEndTime;
    }

    public void setOnlyEndTime(String onlyEndTime) {
        OnlyEndTime = onlyEndTime;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getOnlyStartTime() {
        return OnlyStartTime;
    }

    public void setOnlyStartTime(String onlyStartTime) {
        OnlyStartTime = onlyStartTime;
    }

    public String getUploadStartTime() {
        return UploadStartTime;
    }

    public void setUploadStartTime(String uploadStartTime) {
        UploadStartTime = uploadStartTime;
    }

    public String getUploadEndTime() {
        return UploadEndTime;
    }

    public void setUploadEndTime(String uploadEndTime) {
        UploadEndTime = uploadEndTime;
    }

    public String getQuestionPaperDate() {
        return QuestionPaperDate;
    }

    public void setQuestionPaperDate(String questionPaperDate) {
        QuestionPaperDate = questionPaperDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getPaperID() {
        return PaperID;
    }

    public void setPaperID(String paperID) {
        PaperID = paperID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getPaperDate() {
        return PaperDate;
    }

    public void setPaperDate(String paperDate) {
        PaperDate = paperDate;
    }

    public String getPaperTypeName() {
        return PaperTypeName;
    }

    public void setPaperTypeName(String paperTypeName) {
        PaperTypeName = paperTypeName;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String PaperTypeName = "";
    public String TotalMarks = "";

    public String IsStudentMarkUploaded = "";
    public String StudentMark = "";

    public String getStudentMark() {
        return StudentMark;
    }

    public void setStudentMark(String studentMark) {
        StudentMark = studentMark;
    }

    public String getIsStudentMarkUploaded() {
        return IsStudentMarkUploaded;
    }

    public void setIsStudentMarkUploaded(String isStudentMarkUploaded) {
        IsStudentMarkUploaded = isStudentMarkUploaded;
    }

    public String getIsOnlyQuestionPaper() {
        return isOnlyQuestionPaper;
    }

    public void setIsOnlyQuestionPaper(String isOnlyQuestionPaper) {
        this.isOnlyQuestionPaper = isOnlyQuestionPaper;
    }

    public String getAnswerPaperDate() {
        return AnswerPaperDate;
    }

    public void setAnswerPaperDate(String answerPaperDate) {
        AnswerPaperDate = answerPaperDate;
    }
}
