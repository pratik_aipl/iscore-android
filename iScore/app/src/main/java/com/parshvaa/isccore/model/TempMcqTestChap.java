package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/4/2017.
 */

public class TempMcqTestChap implements Serializable {
    public String tempmcqtesthdrid="",Chapterid="";

    public int getTempmcqtestchapterid() {
        return tempmcqtestchapterid;
    }

    public void setTempmcqtestchapterid(int tempmcqtestchapterid) {
        this.tempmcqtestchapterid = tempmcqtestchapterid;
    }

    public String getTempmcqtesthdrid() {
        return tempmcqtesthdrid;
    }

    public void setTempmcqtesthdrid(String tempmcqtesthdrid) {
        this.tempmcqtesthdrid = tempmcqtesthdrid;
    }

    public String getChapterid() {
        return Chapterid;
    }

    public void setChapterid(String chapterid) {
        Chapterid = chapterid;
    }

    public int tempmcqtestchapterid = -1;
}
