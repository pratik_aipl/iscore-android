package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class TableRecords {
    /*
    subjects: 9,
    chapters: 241,
    mcqquestion: 6572,
    mcqoption: 22002,
    paper_type: 3,
    examtype_subject: 34,
    examtypes: 16,
    exam_type_pattern: 34,
    exam_type_pattern_detail: 1131,
    masterquestion: 9513,
    question_types: 254,
    sub_question_types: 249,
    passage_sub_question_type: 43,
    master_passage_sub_question: 5014
     */
    @JsonField(name = "subjects")
    int subjects;
    @JsonField(name = "chapters")
    int chapters;
    @JsonField(name = "mcqquestion")
    int mcqquestion;
    @JsonField(name = "mcqoption")
    int mcqoption;
    @JsonField(name = "paper_type")
    int paper_type;
    @JsonField(name = "examtype_subject")
    int examtype_subject;
    @JsonField(name = "examtypes")
    int examtypes;
    @JsonField(name = "exam_type_pattern")
    int exam_type_pattern;
    @JsonField(name = "exam_type_pattern_detail")
    int exam_type_pattern_detail;
    @JsonField(name = "masterquestion")
    int masterquestion;
    @JsonField(name = "question_types")
    int question_types;
    @JsonField(name = "sub_question_types")
    int sub_question_types;
    @JsonField(name = "passage_sub_question_type")
    int passage_sub_question_type;
    @JsonField(name = "master_passage_sub_question")
    int master_passage_sub_question;
    @JsonField(name = "master_question_images")
    int master_question_images;
    @JsonField(name = "mcq_question_images")
    int mcq_question_images;

    public int getSubjects() {
        return subjects;
    }

    public void setSubjects(int subjects) {
        this.subjects = subjects;
    }

    public int getChapters() {
        return chapters;
    }

    public void setChapters(int chapters) {
        this.chapters = chapters;
    }

    public int getMcqquestion() {
        return mcqquestion;
    }

    public void setMcqquestion(int mcqquestion) {
        this.mcqquestion = mcqquestion;
    }

    public int getMcqoption() {
        return mcqoption;
    }

    public void setMcqoption(int mcqoption) {
        this.mcqoption = mcqoption;
    }

    public int getPaper_type() {
        return paper_type;
    }

    public void setPaper_type(int paper_type) {
        this.paper_type = paper_type;
    }

    public int getExamtype_subject() {
        return examtype_subject;
    }

    public void setExamtype_subject(int examtype_subject) {
        this.examtype_subject = examtype_subject;
    }

    public int getExamtypes() {
        return examtypes;
    }

    public void setExamtypes(int examtypes) {
        this.examtypes = examtypes;
    }

    public int getExam_type_pattern() {
        return exam_type_pattern;
    }

    public void setExam_type_pattern(int exam_type_pattern) {
        this.exam_type_pattern = exam_type_pattern;
    }

    public int getExam_type_pattern_detail() {
        return exam_type_pattern_detail;
    }

    public void setExam_type_pattern_detail(int exam_type_pattern_detail) {
        this.exam_type_pattern_detail = exam_type_pattern_detail;
    }

    public int getMasterquestion() {
        return masterquestion;
    }

    public void setMasterquestion(int masterquestion) {
        this.masterquestion = masterquestion;
    }

    public int getQuestion_types() {
        return question_types;
    }

    public void setQuestion_types(int question_types) {
        this.question_types = question_types;
    }

    public int getSub_question_types() {
        return sub_question_types;
    }

    public void setSub_question_types(int sub_question_types) {
        this.sub_question_types = sub_question_types;
    }

    public int getPassage_sub_question_type() {
        return passage_sub_question_type;
    }

    public void setPassage_sub_question_type(int passage_sub_question_type) {
        this.passage_sub_question_type = passage_sub_question_type;
    }

    public int getMaster_passage_sub_question() {
        return master_passage_sub_question;
    }

    public void setMaster_passage_sub_question(int master_passage_sub_question) {
        this.master_passage_sub_question = master_passage_sub_question;
    }

    public int getMaster_question_images() {
        return master_question_images;
    }

    public void setMaster_question_images(int master_question_images) {
        this.master_question_images = master_question_images;
    }

    public int getMcq_question_images() {
        return mcq_question_images;
    }

    public void setMcq_question_images(int mcq_question_images) {
        this.mcq_question_images = mcq_question_images;
    }
}
