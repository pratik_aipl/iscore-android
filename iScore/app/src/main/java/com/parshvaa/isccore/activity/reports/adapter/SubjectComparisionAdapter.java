package com.parshvaa.isccore.activity.reports.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parshvaa.isccore.model.ComparisionReport;
import com.parshvaa.isccore.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Karan - Empiere on 2/25/2017.
 */

public class SubjectComparisionAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<ComparisionReport> comparisionArray;

    private LinkedHashMap<String, String> array;

    private LinkedHashMap<String, String> tempGenratepaperArray;
    private String[] mKeys;
    private String[] gKeys;
    public LayoutInflater inflater;

    public SubjectComparisionAdapter(Fragment ft, ArrayList<ComparisionReport> comparisionArray) {
        mContext = ft.getActivity();
        this.comparisionArray = comparisionArray;

        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        /*mKeys = array.keySet().toArray(new String[array.size()]);
        gKeys = tempGenratepaperArray.keySet().toArray(new String[tempGenratepaperArray.size()]);*/
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return comparisionArray.size();
    }

    @Override
    public ComparisionReport getItem(int position) {
        // TODO Auto-generated method stub
        return comparisionArray.get(position);

    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private static class ViewHolder {

        public TextView tv_subj;
        public TextView tv_total_mcq, tv_accuracy_mcq, tv_total_cct, tv_accuracy_cct, tv_genratepaper_total;


        public ViewHolder(View v) {
            tv_subj = v.findViewById(R.id.tv_sub_name);

            tv_total_mcq = v.findViewById(R.id.tv_total_mcq);
            tv_accuracy_mcq = v.findViewById(R.id.tv_accuracy_mcq);
            tv_total_cct = v.findViewById(R.id.tv_total_cct);
            tv_accuracy_cct = v.findViewById(R.id.tv_accuracy_cct);
            tv_genratepaper_total = v.findViewById(R.id.tv_genratepaper_total);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated meth++++++++++++++++od stub

        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_subject_comparision_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_subj.setText(comparisionArray.get(position).getSubjectName()+" (Attempt/Accuracy)");
        holder.tv_total_mcq.setText(comparisionArray.get(position).getTotalMcq());
        holder.tv_accuracy_mcq.setText(comparisionArray.get(position).getAccuracy_MCQ() + "%");
        if(!comparisionArray.get(position).getTotalCct().equals(""))
        holder.tv_total_cct.setText(comparisionArray.get(position).getTotalCct());
        else
            holder.tv_total_cct.setText("0");
        if(!comparisionArray.get(position).getAccuracy_cct().equals(""))
        holder.tv_accuracy_cct.setText(comparisionArray.get(position).getAccuracy_cct() + "%");
        else
            holder.tv_accuracy_cct.setText("0%");
        holder.tv_genratepaper_total.setText(comparisionArray.get(position).getToatalGenratPaper());
        return convertView;
    }

}
