package com.parshvaa.isccore.activity.boardquestion;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.TempQuestionTypes;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class BoardPreviewRevisionNoteActivity extends BaseActivity {
    public String ChapterID = "", chapterNumber = "", QuestionTypeIDs = "";
    public Button btn_preview_paper;
    public BoardPreviewRevisionNoteActivity instance;
    public ArrayList<TempQuestionTypes> tempQuestionTypesArray;
    public ArrayList<String> questionType = new ArrayList<>();
    public JsonArray jSendQue;
    public ViewPager viewPager;
    public MyViewPagerAdapter myViewPagerAdapter;
    public FloatingActionButton float_right_button, float_left_button;
    public TextView tv_pager_count;
    public ImageView img_back, img_right, img_left;
    public TextView tv_title;
    public HashMap<String, ArrayList<TempQuestionTypes>> stringArrayListHashMap = new HashMap<>();
    ArrayList<TempQuestionTypes> values = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_revision_paper_solution);
        Utils.logUser();
        instance = this;
        btn_preview_paper = findViewById(R.id.btn_preview_paper);
        ChapterID = getIntent().getExtras().getString("ChapterIds");
        chapterNumber = getIntent().getExtras().getString("ChapterNumber");
        QuestionTypeIDs = getIntent().getExtras().getString("selectedQuesTypeIDs");
        viewPager = findViewById(R.id.pager);
        tv_pager_count = findViewById(R.id.tv_pager_count);
        img_back = findViewById(R.id.img_back);
        img_right = findViewById(R.id.img_right);
        img_left = findViewById(R.id.img_left);
        tv_title = findViewById(R.id.tv_title);
        float_right_button = findViewById(R.id.float_right_button);
        float_left_button = findViewById(R.id.float_left_button);


        List<String> items = Arrays.asList(QuestionTypeIDs.split("\\s*,\\s*"));
        stringArrayListHashMap.clear();
        for (int i = 0; i < items.size(); i++) {
            try {
                tempQuestionTypesArray = (ArrayList) new CustomDatabaseQuery(this, new TempQuestionTypes())
                        .execute(new String[]{DBNewQuery.getBoardQuestions(items.get(i), ChapterID)}).get();
                stringArrayListHashMap.put(items.get(i), tempQuestionTypesArray);


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }


        myViewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager(), stringArrayListHashMap.size(), values);
        viewPager.setAdapter(myViewPagerAdapter);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        myViewPagerAdapter.notifyDataSetChanged();
        tv_pager_count.setText(viewPager.getCurrentItem() + 1 + "/" + stringArrayListHashMap.size());
        tv_title.setText(App.subObj.getSubjectName()+" Paper");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageScrollStateChanged(int state) {}

            @Override
            public void onPageSelected(int position) {
                tv_pager_count.setText(position + 1 + "/" + stringArrayListHashMap.size());
            }
        });
        float_left_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        float_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < 10) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });
        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < 10) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });

        btn_preview_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, BoardPreviewActivity.class)
                         .putExtra("ChapterIds", ChapterID)
                        .putExtra("ChapterNumber", chapterNumber)
                        .putExtra("selectedQuesTypeIDs", QuestionTypeIDs)
                );
            }
        });
    }

    public class MyViewPagerAdapter extends FragmentPagerAdapter {
        int mNumOfTabs;
        ArrayList<TempQuestionTypes> arrayList = new ArrayList<>();

        public MyViewPagerAdapter(FragmentManager fm, int i, ArrayList<TempQuestionTypes> questionTypesArrayList) {
            super(fm);
            this.mNumOfTabs = i;
            this.arrayList = questionTypesArrayList;
        }

        @Override
        public Fragment getItem(int position) {
            return new BoardPreviewFragment().newInstance(position, arrayList,stringArrayListHashMap);
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return String.valueOf(position + 1);
        }
    }


}
