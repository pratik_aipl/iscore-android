package com.parshvaa.isccore.activity.boardpaper;

/**
 * Created by empiere-vaibhav on 1/10/2018.
 */

public class BoardPaperPreview {
    public String QFile = "";
    public String QAFile = "";


    public String PaperName = "";

    public String getPaperName() {
        return PaperName;
    }

    public void setPaperName(String paperName) {
        PaperName = paperName;
    }

    public String getAnswerPaperName() {
        return AnswerPaperName;
    }

    public void setAnswerPaperName(String answerPaperName) {
        AnswerPaperName = answerPaperName;
    }

    public String AnswerPaperName = "";

    public String BoardPaperName = "";

    public String getQFile() {
        return QFile;
    }

    public void setQFile(String QFile) {
        this.QFile = QFile;
    }

    public String getQAFile() {
        return QAFile;
    }

    public void setQAFile(String QAFile) {
        this.QAFile = QAFile;
    }

    public String getBoardPaperName() {
        return BoardPaperName;
    }

    public void setBoardPaperName(String boardPaperName) {
        BoardPaperName = boardPaperName;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String SubjectName = "";
}
