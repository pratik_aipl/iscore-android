package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by admin on 2/28/2017.
 */
@JsonObject
public class QuestionTypes implements Serializable {
    @JsonField
    public String QuestionTypeID;
    @JsonField
    public String QuestionType;
    @JsonField
    public String BoardID;
    @JsonField
    public String MediumID;
    @JsonField
    public String ClassID;
    @JsonField
    public String StandardID;
    @JsonField
    public String SubjectID;
    @JsonField
    public String Marks;
    @JsonField
    public String Type="";
    @JsonField
    public String RevisionNo;
    @JsonField
    public String CreatedBy;
    @JsonField
    public String CreatedOn;
    @JsonField
    public String ModifiedBy;
    @JsonField
    public String ModifiedOn;
    @JsonField
    public String isMTC;
    @JsonField
    public String isPassage;

    public String getIsPassage() {
        return isPassage;
    }

    public void setIsPassage(String isPassage) {
        this.isPassage = isPassage;
    }

    public QuestionTypes() {

    }

    public QuestionTypes(String QuestionTypeID, String QuestionType, String BoardID, String MediumID, String ClassID, String StandardID,
                         String SubjectID, String Marks, String Type, String CreatedBy,
                         String CreatedOn, String ModifiedBy, String ModifiedOn, String RevisionNo, String isMTC) {

        this.QuestionTypeID = QuestionTypeID;
        this.QuestionType = QuestionType;
        this.BoardID = BoardID;
        this.MediumID = MediumID;
        this.ClassID = ClassID;
        this.StandardID = StandardID;
        this.SubjectID = SubjectID;
        this.Marks = Marks;
        this.Type = Type;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;
        this.RevisionNo = RevisionNo;
        this.isMTC = isMTC;

    }

    public String getIsMTC() {
        return isMTC;
    }

    public void setIsMTC(String isMTC) {
        this.isMTC = isMTC;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(String questionType) {
        QuestionType = questionType;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getMarks() {
        return Marks;
    }

    public void setMarks(String marks) {
        Marks = marks;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getRevisionNo() {
        return RevisionNo;
    }

    public void setRevisionNo(String revisionNo) {
        RevisionNo = revisionNo;
    }

}
