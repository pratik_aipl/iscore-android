package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 12/21/2017.
 */

@JsonObject
public class Subject implements Serializable {

    @JsonField
    public String SubjectID;
    @JsonField
    public String SubjectName;
    @JsonField
    public String BoardID;
    @JsonField
    public String MediumID;
    @JsonField
    public String StandardID;
    @JsonField
    public String CreatedBy;
    @JsonField
    public String CreatedOn;
    @JsonField
    public String ModifiedBy;
    @JsonField
    public String ModifiedOn;
    @JsonField
    public String SubjectIcon;
    @JsonField
    public String SubjectOrder;
    @JsonField
    public String isMCQ;
    @JsonField
    public String isGeneratePaper;
    @JsonField
    public String IsMapped;
    @JsonField
    public String MappedSubjectID;

    public String getIsMapped() {
        return IsMapped;
    }

    public void setIsMapped(String isMapped) {
        IsMapped = isMapped;
    }

    public String getMappedSubjectID() {
        return MappedSubjectID;
    }


    public Subject() {
    }

    public void setMappedSubjectID(String mappedSubjectID) {
        MappedSubjectID = mappedSubjectID;
    }

    public String getIsMCQ() {
        return isMCQ;
    }

    public void setIsMCQ(String isMCQ) {
        this.isMCQ = isMCQ;
    }

    public String getIsGeneratePaper() {
        return isGeneratePaper;
    }

    public void setIsGeneratePaper(String isGeneratePaper) {
        this.isGeneratePaper = isGeneratePaper;
    }

    public String getSubjectOrder() {
        return SubjectOrder;
    }

    public void setSubjectOrder(String subjectOrder) {
        SubjectOrder = subjectOrder;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String Price;   //tables.put("Price","integer (11)");

    public String getSubjectIcon() {
        return SubjectIcon;
    }

    public void setSubjectIcon(String subjectIcon) {
        SubjectIcon = subjectIcon;
    }


    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}