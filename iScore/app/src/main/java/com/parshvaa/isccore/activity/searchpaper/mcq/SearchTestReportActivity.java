package com.parshvaa.isccore.activity.searchpaper.mcq;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nineoldandroids.view.ViewHelper;
import com.parshvaa.isccore.activity.mcq.SubjectMcqActivity;
import com.parshvaa.isccore.activity.mcq.TestSummaryActivity;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.ObservableScrollView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.LevelTestReportMCQ;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class SearchTestReportActivity extends BaseActivity implements ObservableScrollViewCallbacks {
    private static final String TAG = "SearchTestReportActivit";
    public SearchTestReportActivity instance;
    public Toolbar mToolbar;

    private View mFlexibleSpaceView, mOverlayView, mImageView;
    private int mActionBarSize, mFlexibleSpaceImageHeight, HeaderId, SubjectID;
    public String SubjectName;
    public ObservableScrollView mScrollView;
    public WebView GoalStaticsProgress, SubjectStaticsProgress;
    public ImageView img_back;

//    public ArrayList<LevelTestReportMCQ> levelArrayList = new ArrayList<>();
    public Button btn_start_new;
    public TextView tv_title, tv_testName, tv_prev_goal, tv_all_sub_accuracy, tv_sub_statics_subName, tv_subName, tv_total_three, tv_total_two, tv_total_one, tv_right_three, tv_right_two, tv_right_one, tv_right, tv_total_que, tv_non_attempted, tv_incorrect, tv_correct, tv_viewSummary;
    public int TotalQue = 0, TotalDuration = 0, AccuracyAllSubject = 0, TotalRight = 0, GoalStatics = 0, SubjectStatics = 0, AvgTimePerQuestion = 0, AvgTimePerQuestionSubjectWise = 0, AvgTimePerQuestionAllSubject = 0;

    public TextView tv_totla_time, tv_time_subName, tv_avg_per_que, tv_sub_vise_per_que, tv_all_sub_wise_per_que;
    public ArrayList<McqQuestion> rightAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> nonAttemptAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> incorectAnsArray = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_test_report);
        Utils.logUser();

        instance = this;

        showProgress(true, true);//Utils.showProgressDialog(instance);
        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mActionBarSize = getActionBarSize();
        mToolbar = findViewById(R.id.toolbar);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        mOverlayView = findViewById(R.id.overlay);
        GoalStaticsProgress = findViewById(R.id.progress);
        SubjectStaticsProgress = findViewById(R.id.progress_two);


        tv_title = findViewById(R.id.tv_title);
        tv_testName = findViewById(R.id.tv_testName);
        img_back = findViewById(R.id.img_back);
        tv_subName = findViewById(R.id.tv_subName);
        tv_total_three = findViewById(R.id.tv_total_three);
        tv_total_two = findViewById(R.id.tv_total_two);
        tv_total_one = findViewById(R.id.tv_total_one);
        tv_right_three = findViewById(R.id.tv_right_three);
        tv_right_two = findViewById(R.id.tv_right_two);
        tv_right_one = findViewById(R.id.tv_right_one);
        tv_total_que = findViewById(R.id.tv_total_que);
        tv_right = findViewById(R.id.tv_right);
        tv_sub_statics_subName = findViewById(R.id.tv_sub_statics_subName);
        tv_all_sub_accuracy = findViewById(R.id.tv_all_sub_accuracy);
        tv_totla_time = findViewById(R.id.tv_totla_time);
        tv_avg_per_que = findViewById(R.id.tv_avg_per_que);
        tv_sub_vise_per_que = findViewById(R.id.tv_sub_vise_per_que);
        tv_all_sub_wise_per_que = findViewById(R.id.tv_all_sub_wise_per_que);

        tv_non_attempted = findViewById(R.id.tv_non_attempted);
        tv_incorrect = findViewById(R.id.tv_incorrect);
        tv_correct = findViewById(R.id.tv_correct);
        tv_viewSummary = findViewById(R.id.tv_viewSummary);
        btn_start_new = findViewById(R.id.btn_start_new);
        tv_time_subName = findViewById(R.id.tv_time_subName);
        tv_prev_goal = findViewById(R.id.tv_prev_goal);
        HeaderId = getIntent().getExtras().getInt("HeaderId");
        SubjectID = getIntent().getExtras().getInt("SubjectID");
        SubjectName = getIntent().getExtras().getString("SubjectName");
        tv_title.setText("Test Report");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_subName.setText(SubjectName + " Subject");
        tv_testName.setText("Score of Test " + (HeaderId + 1));


        rightAnsArray = (ArrayList<McqQuestion>) getIntent().getExtras().getSerializable("rightAnsArray");
        incorectAnsArray = (ArrayList<McqQuestion>) getIntent().getExtras().getSerializable("incorectAnsArray");
        nonAttemptAnsArray = (ArrayList<McqQuestion>) getIntent().getExtras().getSerializable("nonAttemptAnsArray");
        // TotalDuration = getIntent().getExtras().getInt("TotalDuration");
        tv_correct.setText(rightAnsArray.size() + " Correct");
        tv_incorrect.setText(incorectAnsArray.size() + " Incorrect");
        tv_non_attempted.setText(nonAttemptAnsArray.size() + " Not answerred");

        mImageView = findViewById(R.id.image);
        mScrollView = findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) 0 / mFlexibleSpaceImageHeight, 0, 1));


        getLevelOne(String.valueOf(HeaderId), "1");
        getLevelTwo(String.valueOf(HeaderId), "2");
        getLevelThree(String.valueOf(HeaderId), "3");

        tv_right.setText(TotalRight + "");
        tv_total_que.setText(TotalQue + "");

        tv_viewSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, TestSummaryActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("temp", HeaderId)
                        .putExtra("rightAnsArray", rightAnsArray)
                        .putExtra("incorectAnsArray", incorectAnsArray)
                        .putExtra("nonAttemptAnsArray", nonAttemptAnsArray));
            }
        });
        btn_start_new.setVisibility(View.GONE);
        btn_start_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, SubjectMcqActivity.class)
                        //  .putExtra("suObj", mcqdata)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
        tv_prev_goal.setText(Html.fromHtml("Your Previous Year School Result : <b>" + mySharedPref.getlblPrviousTarget() + "% </b>"));

        new getRecorrd(instance).execute();

    }

    public void getLevelOne(String HdrID, String LevelID) {
        ArrayList<LevelTestReportMCQ> levelArrayList = new ArrayList<>();

        try {
            Log.d(TAG, "getLevelOne: "+DBNewQuery.get_level_wise_report(HdrID, LevelID));
            levelArrayList = (ArrayList) new CustomDatabaseQuery(this, new LevelTestReportMCQ())
                    .execute(new String[]{DBNewQuery.get_level_wise_report(HdrID, LevelID)}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (levelArrayList != null && levelArrayList.size() > 0) {
            TotalQue = levelArrayList.get(0).getTotal_MCQ_Que();
            TotalRight = levelArrayList.get(0).getTotal_right();
            tv_total_one.setText(levelArrayList.get(0).getTotal_MCQ_Que() + "");
            tv_right_one.setText(levelArrayList.get(0).getTotal_right() + "");
        } else {
            TotalQue = 0;
            TotalRight = 0;
            tv_total_one.setText("0");
            tv_right_one.setText("0");
        }

    }

    public void getLevelTwo(String HdrID, String LevelID) {

        ArrayList<LevelTestReportMCQ> levelArrayList = new ArrayList<>();
        try {
            levelArrayList = (ArrayList) new CustomDatabaseQuery(this, new LevelTestReportMCQ())
                    .execute(new String[]{DBNewQuery.get_level_wise_report(HdrID, LevelID)}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (levelArrayList != null && levelArrayList.size() > 0) {
            TotalQue += levelArrayList.get(0).getTotal_MCQ_Que();
            TotalRight += levelArrayList.get(0).getTotal_right();
            tv_total_two.setText(levelArrayList.get(0).getTotal_MCQ_Que() + "");
            tv_right_two.setText(levelArrayList.get(0).getTotal_right() + "");
        } else {
            TotalQue += 0;
            TotalRight += 0;
            tv_total_two.setText("0");
            tv_right_two.setText("0");
        }

    }

    public void getLevelThree(String HdrID, String LevelID) {

        ArrayList<LevelTestReportMCQ> levelArrayList = new ArrayList<>();
        try {
            levelArrayList = (ArrayList) new CustomDatabaseQuery(this, new LevelTestReportMCQ())
                    .execute(new String[]{DBNewQuery.get_level_wise_report(HdrID, LevelID)}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (levelArrayList != null && levelArrayList.size() > 0) {
            TotalQue += levelArrayList.get(0).getTotal_MCQ_Que();
            TotalRight += levelArrayList.get(0).getTotal_right();
            tv_total_three.setText(levelArrayList.get(0).getTotal_MCQ_Que() + "");
            tv_right_three.setText(levelArrayList.get(0).getTotal_right() + "");
        } else {
            TotalQue += 0;
            TotalRight += 0;
            tv_total_three.setText("0");
            tv_right_three.setText("0");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void WebViewSetting(WebView webview, String Process, String SecondProgress, String title) {
        WebSettings settings = webview.getSettings();
        //settings.setMinimumFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        webview.setWebViewClient(new myWebClient());
        //webview.loadUrl("file:///android_asset/progressbar/progress.html");
        webview.loadDataWithBaseURL("", ProgressBarHtmL(Process, SecondProgress, title), "text/html", "UTF-8", "");

    }

    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                // showProgress(false)();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        float flexibleRange = mFlexibleSpaceImageHeight - mActionBarSize;
        int minOverlayTransitionY = mActionBarSize - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));
        // Change alpha of overlay
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    public class getRecorrd extends AsyncTask<String, String, String> {
        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;

        private getRecorrd(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {
            Cursor c = mDb.getAllRows("SELECT ((count(smtd.StudentMCQTestDTLID)*100)/" +
                    "(select count(sub_smtd.StudentMCQTestDTLID) from student_mcq_test_dtl as sub_smtd where sub_smtd.StudentMCQTestHDRID = " + HeaderId + "))" +
                    " as accuracy  FROM student_mcq_test_dtl as smtd JOIN mcqoption as mo on smtd.AnswerID=mo.MCQOPtionID " +
                    "WHERE smtd.StudentMCQTestHDRID = " + HeaderId + " AND mo.isCorrect=1");

            if (c != null && c.moveToFirst()) {
                GoalStatics = c.getInt(0);
                c.close();
            }

            c = mDb.getAllRows("SELECT (" +
                    "(count(smtd.StudentMCQTestDTLID)*100)/" +
                    "(select count(sub_smtd.StudentMCQTestDTLID) from student_mcq_test_dtl as sub_smtd " +
                    "where sub_smtd.`StudentMCQTestHDRID` in (select sub.StudentMCQTestHDRID from student_mcq_test_hdr as sub where sub.SubjectID = " + SubjectID + "))" +
                    ") as accuracy  FROM `student_mcq_test_dtl` as smtd " +
                    "JOIN mcqoption as mo on smtd.AnswerID=mo.MCQOPtionID " +
                    "WHERE smtd.`StudentMCQTestHDRID` in (select sub.StudentMCQTestHDRID from student_mcq_test_hdr as sub where sub.SubjectID = " + SubjectID + ") AND mo.isCorrect=1");
            if (c != null && c.moveToFirst()) {
                SubjectStatics = c.getInt(0);
                c.close();
            }


            c = mDb.getAllRows(" SELECT((sum(smtd.QuestionTime)) /" +
                    " (select count(sub_smtd.StudentMCQTestDTLID)from student_mcq_test_dtl as sub_smtd where sub_smtd.StudentMCQTestHDRID = " + HeaderId + "))" +
                    " as per_question FROM student_mcq_test_dtl as smtd WHERE smtd.StudentMCQTestHDRID =" + HeaderId);
            if (c != null && c.moveToFirst()) {
                AvgTimePerQuestion = c.getInt(0);
                c.close();
            }

            c = mDb.getAllRows(" SELECT ((sum(smtd.QuestionTime))/(select count(sub_smtd.StudentMCQTestDTLID) from student_mcq_test_dtl" +
                    " as sub_smtd where sub_smtd.StudentMCQTestHDRID in (select sub.StudentMCQTestHDRID from" +
                    " student_mcq_test_hdr as sub where sub.SubjectID = " + SubjectID + "))) as accuracy  FROM student_mcq_test_dtl" +
                    " as smtd WHERE smtd.StudentMCQTestHDRID in (select sub.StudentMCQTestHDRID from student_mcq_test_hdr" +
                    " as sub where sub.SubjectID = " + SubjectID + ")");

            if (c != null && c.moveToFirst()) {
                AvgTimePerQuestionSubjectWise = c.getInt(0);
                c.close();
            }
            c = mDb.getAllRows("SELECT((sum(smtd.QuestionTime)) /(select count(sub_smtd.StudentMCQTestDTLID)from" +
                    " student_mcq_test_dtl as sub_smtd))as accuracy FROM student_mcq_test_dtl as smtd");
            if (c != null && c.moveToFirst()) {
                AvgTimePerQuestionAllSubject = c.getInt(0);
                c.close();
            }
            c = mDb.getAllRows("select TestTime from student_mcq_test_hdr where StudentMCQTestHDRID=" + HeaderId);
            if (c != null && c.moveToFirst()) {
                TotalDuration = c.getInt(0);
                c.close();
            }

            c = mDb.getAllRows("SELECT (\n" +
                    "(count(smtd.StudentMCQTestDTLID)*100)/\n" +
                    "(select count(sub_smtd.StudentMCQTestDTLID) from student_mcq_test_dtl as sub_smtd \n" +
                    "where sub_smtd.`StudentMCQTestHDRID` in (select sub.StudentMCQTestHDRID from student_mcq_test_hdr as sub))\n" +
                    ") as accuracy  FROM `student_mcq_test_dtl` as smtd \n" +
                    "JOIN mcqoption as mo on smtd.AnswerID=mo.MCQOPtionID \n" +
                    "WHERE smtd.`StudentMCQTestHDRID` in (select sub.StudentMCQTestHDRID from student_mcq_test_hdr as sub) AND mo.isCorrect=1");

            if (c != null && c.moveToFirst()) {
                AccuracyAllSubject = c.getInt(0);
                c.close();
            }
            return "";
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showProgress(false, true);
            WebViewSetting(GoalStaticsProgress, String.valueOf(GoalStatics), String.valueOf(mySharedPref.getTarget()), "MY GOAL");
            WebViewSetting(SubjectStaticsProgress, String.valueOf(GoalStatics), String.valueOf(SubjectStatics), SubjectName);


            String mins = String.valueOf(TotalDuration / 60);
            String sec = String.valueOf(TotalDuration % 60);
            try {
                if (mins.length() <= 1) {
                    mins = "0" + mins;
                }
                if (sec.length() <= 1) {
                    sec = "0" + sec;
                }
                tv_totla_time.setText(mins + ":" + sec);


            } catch (Exception e) {
                tv_totla_time.setText("--:--");
                e.printStackTrace();

            }
            tv_sub_statics_subName.setText("Comparison with " + SubjectName + " Subject");
            tv_avg_per_que.setText(MinuteTotal(AvgTimePerQuestion) + ":" + SecondTotal(AvgTimePerQuestion));
            tv_sub_vise_per_que.setText("" + MinuteTotal(AvgTimePerQuestionSubjectWise) + ":" + SecondTotal(AvgTimePerQuestionSubjectWise) + "");
            tv_all_sub_wise_per_que.setText("" + MinuteTotal(AvgTimePerQuestionAllSubject) + ":" + SecondTotal(AvgTimePerQuestionAllSubject) + "");
            tv_time_subName.setText("(" + SubjectName + ")");

            tv_all_sub_accuracy.setText(Html.fromHtml("Your Accuracy For All Subject: <b>" + AccuracyAllSubject + "%</b>"));

        }
    }

    public String SecondTotal(int second) {
        String sec = String.valueOf(second % 60);
        if (sec.length() <= 1) {
            sec = "0" + sec;
        }
        return sec;
    }

    public String MinuteTotal(int sec) {

        String mins = String.valueOf(sec / 60);
        if (mins.length() <= 1) {
            mins = "0" + mins;
        }
        return mins;
    }
    public String ProgressBarHtmL(String Progress, String SecondProgress, String title) {
        return "<!DOCTYPE html>" +
                "<head>  " +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
                "<meta charset=\"utf-8\" />" +
                "  <link rel='stylesheet' type='text/css' href='file:///android_asset/bootstrap.min.css'/>" +
/*
                "  <link rel=\"stylesheet\" type=\"text/css\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\" />" +
               */ "  <script type=\"text/javascript\" src=\"file:///android_asset/jquery.min.js\"></script>" +
                "  <script type=\"text/javascript\" src=\"file:///android_asset/bootstrap.min.js\"></script>" +

                "<Script>" +
                "  $(document).ready(function() {" +
                "    $(function () {" +
                "       $('[data-toggle=\"tooltip\"]').tooltip({trigger: 'manual'}).tooltip('show');\n" +
                "  });" +
                "  $(\".progress-bar\").each(function(){" +
                "    each_bar_width = $(this).attr('aria-valuenow');" +
                "    $(this).width(each_bar_width + '%');" +
                "  });" +
                "   });" +
                "  </Script>" +
                "<style>" +
                ".barWrapper{height: 60px;margin-top: 30px;width:100%}" +
                "" +
                ".tooltip{ " +
                "  position:relative;" +
                "  float:right;" +
                "} " +
                ".tooltip > .tooltip-inner {" +
                "  padding: 3px 6px;" +
                "  color: #fff;" +
                "  font-weight: bold;" +
                "  font-size: 11px;" +
                "  width: auto;" +
                "  height: auto;" +
                "  border-radius: 4px;" +
                "  line-height: 14px;" +
                "  display: block;" +
                "  vertical-align: middle;" +
                "}" +
                ".tooltip.top{top:-34px !important; opacity: 1; margin-top: 0; width:auto;}" +
                "" +
                ".tooltip.top > .tooltip-inner {  background-color: #f49738; }" +
                "" +
                ".popOver + .tooltip.top > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-top: 6px solid #f49738; left:50% !important}" +
                "" +
                "" +
                "" +
                ".tooltip.bottom > .tooltip-inner {  background-color: #206e9e; }" +
                ".tooltip.bottom{top:12px !important; opacity: 1; margin-top: 0;}" +
                ".popOver + .tooltip.bottom > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-bottom: 6px solid #206e9e;  left:50% !important}" +
                "" +
                "section{" +
                "  margin:100px auto; " +
                "  height:1000px;" +
                "}" +
                ".progress{" +
                "  border-radius:0;" +
                "  overflow:visible;" +
                "  height: 10px;" +
                "  margin: 0 auto;" +
                " width:82%;"+
                "}" +
                ".progress-bar{ " +
                "  -webkit-transition: width 1.5s ease-in-out;" +
                "  transition: width 1.5s ease-in-out;" +
                "}" +
                "" +
                ".me-at.progress{ position: relative; z-index: 11; background: rgba(0,0,0,0);}" +
                ".me-at.progress .progress-bar{   background:#f49738; opacity: 1; z-index: 11}" +
                "" +
                ".mygoal.progress{position: relative;top: -10px; z-index: 1}" +
                "" +
                ".mygoal.progress .progress-bar{   background:#206e9e; opacity: .9; z-index: 11}" +
                "" +
                "</style>" +
                "" +
                "</head>" +
                "<body>" +
                "     " +
                "  <!--<h2 class=\"text-center\">Scroll down the page a bit</h2><br><br> -->" +
                "<div class=\"container\">" +
                "  " +
                "    " +
                "    " +
                "       " +
                "      <div class=\"barWrapper\"> " +
                "        <div class=\"progress me-at\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=" + Progress + " aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ME&nbsp;(" + Progress + "%)\"> </span>     " +
                "          </div> " +
                "        </div>" +
                "        <div class=\"progress mygoal\">" +
                "         <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=" + SecondProgress + " aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" + title + "(" + SecondProgress + "%)\"> </span>     " +
                "          </div>" +
                "        </div>" +
                "      </div>" +
                " " +
                "</div>   " +


                "</body>" +

                "</html>";
    }
}
