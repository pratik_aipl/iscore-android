package com.parshvaa.isccore.activity.event;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import tcking.github.com.giraffeplayer2.VideoView;

public class EventVideoViewActivity extends BaseActivity {
    private static final String TAG = "VideoViewActivity";
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    @BindView(R.id.bvp)
    VideoView bvp;
    @BindView(R.id.mVideoWeb)
    WebView mVideoWeb;
    @BindView(R.id.rel_main)
    RelativeLayout relMain;
    @BindView(R.id.mBack)
    ImageView mBack;

    private final Runnable mShowPart2Runnable = () -> {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
    };
    private boolean mVisible;
    public String FileNAME="";
    public int pos = 0;
    private final Runnable mHideRunnable = () -> hide();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        ButterKnife.bind(this);
        FileNAME = getIntent().getStringExtra("FILENAME") == null ? "" : getIntent().getStringExtra("FILENAME");

        if (!TextUtils.isEmpty(FileNAME)){
            mVisible = true;
            bvp.setVideoPath(FileNAME).getPlayer().start();
        }

        relMain.setOnClickListener(view -> toggle());
        mBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;
        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
//        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        bvp.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;
        // Schedule a runnable to display UI elements after a delay
//        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}

