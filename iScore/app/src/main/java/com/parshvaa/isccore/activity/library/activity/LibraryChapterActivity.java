package com.parshvaa.isccore.activity.library.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.activity.library.adapter.LibraryChapterAdapter;
import com.parshvaa.isccore.activity.library.Library;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class LibraryChapterActivity extends BaseActivity implements AsynchTaskListner {
    private RecyclerView recyclerView;
    private LibraryChapterAdapter mAdapter;
    public ImageView img_back;
    public TextView tv_title;
    ArrayList<Library> libraryArrayList = new ArrayList<>();
    public static MyDBManager mDb;
    public Library library;
    public JsonParserUniversal jParser;
    public String FolderID = "", SubjectID = "", Subject = "";

    public RelativeLayout rel_library, rel_subject, rel_chapter;
    public TextView tv_lib, tv_subject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_subject);
        Utils.logUser();

        jParser = new JsonParserUniversal();

        FolderID = getIntent().getStringExtra("FolderID");
        SubjectID = getIntent().getStringExtra("SubjectID");
        Subject = getIntent().getStringExtra("Subject");

        recyclerView = findViewById(R.id.recycler_view);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);

        rel_library = findViewById(R.id.rel_library);
        rel_subject = findViewById(R.id.rel_subject);
        rel_chapter = findViewById(R.id.rel_chapter);

        tv_lib = findViewById(R.id.tv_lib);
        tv_subject = findViewById(R.id.tv_subject);

        rel_chapter.setVisibility(View.GONE);
        tv_subject.setText(Subject);
        tv_lib.setOnClickListener(view -> onBackPressed());

        tv_title.setText("library");
        img_back.setOnClickListener(v -> onBackPressed());
        showProgress(true, true);
        new CallRequest(LibraryChapterActivity.this).get_library_chapter_list(FolderID);
    }


    private void setupRecyclerView() {
        final Context context = recyclerView.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        mAdapter = new LibraryChapterAdapter(libraryArrayList, context, SubjectID, Subject);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case get_library_chapter_list:

                    libraryArrayList.clear();
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jDataArray = jObj.getJSONArray("library");
                            libraryArrayList.addAll(LoganSquare.parseList(jDataArray.toString(),Library.class));
                                setupRecyclerView();
                                showProgress(false, true);

                        } else {
                            showProgress(false, true);
                            showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

