package com.parshvaa.isccore.activity.reports;

import android.database.Cursor;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.reports.adapter.SubjectTestlAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.TestReport;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportTestFragment extends Fragment {
    public TextView tv_total, tv_PrelimsPaperDownload, tv_SetPaperDownload, tv_ReadyPaperDownload;
    public int readyPaper;
    public int setPaper;
    public int prelimPaper;
    public int total_ganrate;
    public MyDBManager mDb;
    public GridView gridView;
    public SubjectTestlAdapter adapter;
    public CursorParserUniversal cParse;
    public String testHeaderIDS = "";
    private LinkedHashMap<String, String> array, tempArray;
    public String[] tempSujectID = new String[]{};
    public App app;
    public TestReport testReport;
    public ArrayList<TestReport> testReportArray = new ArrayList<>();


    public static ReportTestFragment newInstance(String param1, String param2) {
        ReportTestFragment fragment = new ReportTestFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }

    }

    public View v;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        v = inflater.inflate(R.layout.fragment_test, container, false);

        mDb = MyDBManager.getInstance(getActivity());
        mDb.open(getActivity());
        cParse = new CursorParserUniversal();
        app = App.getInstance();
        total_ganrate = mDb.getAllRecordsUsingQuery("select * from student_question_paper");
        readyPaper = mDb.getAllRecordsUsingQuery("select * from student_question_paper where PaperTypeID = '2'");
        setPaper = mDb.getAllRecordsUsingQuery("select * from student_question_paper where PaperTypeID = '3'");
        prelimPaper = mDb.getAllRecordsUsingQuery("select * from student_question_paper where PaperTypeID = '1'");

        tv_total = v.findViewById(R.id.tv_total);
        tv_PrelimsPaperDownload = v.findViewById(R.id.tv_PrelimsPaperDownload);
        tv_SetPaperDownload = v.findViewById(R.id.tv_SetPaperDownload);
        tv_ReadyPaperDownload = v.findViewById(R.id.tv_ReadyPaperDownload);

        tv_total.setText(total_ganrate + "");
        tv_PrelimsPaperDownload.setText(prelimPaper + "");
        tv_SetPaperDownload.setText(setPaper + "");
        tv_ReadyPaperDownload.setText(readyPaper + "");
        testReportArray.clear();


        Cursor c = mDb.getAllRows(DBQueries.getTotalTestSubjectsWithName());
        if (c != null && c.moveToFirst()) {
            do {

                testReport = new TestReport();
                testReport = (TestReport) cParse.parseCursor(c, new TestReport());
                testReportArray.add(testReport);

            } while (c.moveToNext());
            c.close();
        } else {
            //   Utils.showToast("Not any Subject Found from : ", getActivity());
        }
        for (TestReport obj : testReportArray) {
            obj.setTotalTest(String.valueOf(mDb.getAllReports("select * from student_question_paper where SubjectID = " +  obj.getSubjectID())));

            obj.setPrelim(String.valueOf(mDb.getAllReports("select * from student_question_paper where SubjectID = " + obj.getSubjectID()+" AND PaperTypeID=1")));
            obj.setReady(String.valueOf(mDb.getAllReports("select * from student_question_paper where SubjectID = " +  obj.getSubjectID()+" AND PaperTypeID=2")));
            obj.setSetPaper(String.valueOf(mDb.getAllReports("select * from student_question_paper where SubjectID = " +  obj.getSubjectID()+" AND PaperTypeID=3")));

        }
        adapter = new SubjectTestlAdapter(this, testReportArray);
        gridView = v.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        return v;
    }
}
