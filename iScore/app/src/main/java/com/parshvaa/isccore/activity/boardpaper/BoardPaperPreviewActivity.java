package com.parshvaa.isccore.activity.boardpaper;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.parshvaa.isccore.activity.boardpaper.pdfview.PDFView;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.BoardPaperDownload;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.FileDownloader;
import com.parshvaa.isccore.utils.Utils;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class BoardPaperPreviewActivity extends BaseActivity implements AsynchTaskListner {
    public String QFile, PaperName;
    public PDFView pdfview;
    public RetryPolicy retryPolicy;
    public static ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    public FloatingActionButton img_share, imgprint;
    public PrintDocumentAdapter pda;
    public File pdf;
    public ImageView img_back;
    public TextView tv_title;
    public BoardPaperPreviewActivity instance;
    public BoardPaperPreviewActivity Staticinstance;
    public static MyDBManager mDb;

    public ArrayList<BoardPaperDownload> BoardpaperArray = new ArrayList<>();
    public static JsonArray jBoardPaperArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_board_paper_preview);
        Utils.logUser();

        instance = this;
        Staticinstance = this;
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);

        if (getIntent().hasExtra("ViewPaper")) {
            showProgress(true, true);//Utils.showProgressDialog(instance);
            QFile = getIntent().getStringExtra("QFile");
            PaperName = getIntent().getStringExtra("PaperName");
            new DownloadFile().execute(QFile, PaperName);


        }

        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Board Paper");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        retryPolicy = new DefaultRetryPolicy();
        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);

        pdfview = findViewById(R.id.pdfView);

        img_share = findViewById(R.id.img_share);
        imgprint = findViewById(R.id.imgprint);
        imgprint.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                pda = new PrintDocumentAdapter() {

                    @Override
                    public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {
                        InputStream input = null;
                        OutputStream output = null;

                        try {

                            input = new FileInputStream(pdf);
                            output = new FileOutputStream(destination.getFileDescriptor());

                            byte[] buf = new byte[1024];
                            int bytesRead;

                            while ((bytesRead = input.read(buf)) > 0) {
                                output.write(buf, 0, bytesRead);
                            }

                            callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

                        } catch (FileNotFoundException ee) {
                            //Catch exception
                        } catch (Exception e) {
                            //Catch exception
                        } finally {
                            try {
                                input.close();
                                output.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {

                        if (cancellationSignal.isCanceled()) {
                            callback.onLayoutCancelled();
                            return;
                        }


                        PrintDocumentInfo pdi = new PrintDocumentInfo.Builder(PaperName).setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                        callback.onLayoutFinished(pdi, true);
                    }


                };
                PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
                String jobName = getString(R.string.app_name) + " Document";
                printManager.print(jobName, pda, null);
            }
        });
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentShareFile = new Intent(Intent.ACTION_SEND);
                intentShareFile.setType("application/pdf");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + pdf));
                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        "Sharing File...");
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");
                startActivity(Intent.createChooser(intentShareFile, "Share File"));
            }
        });


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case sendBoardPaperDownload:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {


                            if (jObj.has("BoardPaperLastSync")) {
                                String setLastBoardPaperDownloadSyncTime = jObj.getString("BoardPaperLastSync");
                                mySharedPref.setLastBoardPaperDownloadSyncTime(setLastBoardPaperDownloadSyncTime);
                                mySharedPref.setLastSyncTime(setLastBoardPaperDownloadSyncTime);
                            }
                            try {
                                if (jObj.getJSONArray("data") != null) {
                                    JSONArray jData = jObj.getJSONArray("data");
                                    parseWebBoardPaperData(jData);
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }

                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Void doInBackground(String... strings) {
            QFile = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            PaperName = strings[1];  // -> maven.pdf
            String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + "board-paper-file";

            File qDirectory = new File(SDCardPath);
            if (!qDirectory.exists()) qDirectory.mkdirs();

            String qLocalUrl = SDCardPath + "/" + PaperName;
            File qLocalFile = new File(qLocalUrl);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(QFile, qLocalFile, getApplicationContext());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pdf = new File(Constant.LOCAL_IMAGE_PATH + "/board-paper-file/" + PaperName);
            if (pdf.isFile()) {
                Utils.Log("TAG", " if POST EXECUTE ::-> " + Constant.LOCAL_IMAGE_PATH + "/board-paper-file/" + PaperName);
                pdfview.fromFile(pdf).load();
            } else {
                new AlertDialog.Builder(instance)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Alert")
                        .setCancelable(false)
                        .setMessage("Paper not available ")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                                dialog.dismiss();


                            }
                        })
                        .show();
                Utils.Log("TAG", " else POST EXECUTE ::-> " + Constant.LOCAL_IMAGE_PATH + "/board-paper-file/" + PaperName);

            }
            getBoardPaperData();
        }
    }

    public void getBoardPaperData() {
        BoardpaperArray.clear();
        jBoardPaperArray = null;
        try {
            BoardpaperArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new BoardPaperDownload())
                    .execute(new String[]{DBQueries.getAllBoardPapereDownload(mySharedPref.getLastBoardPaperDownloadSyncTime())}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Gson gson = new GsonBuilder().create();
        jBoardPaperArray = gson.toJsonTree(BoardpaperArray).getAsJsonArray();
        if (jBoardPaperArray.size() > 0) {
            new CallRequest(instance).sendBoardPaperDownloadLogin(String.valueOf(jBoardPaperArray));
        } else {
            new CallRequest(instance).sendBoardPaperDownloadLogin("[]");
        }
    }

    public void parseWebBoardPaperData(JSONArray jArray) {
        if (jArray != null && jArray.length() > 0) {
            try {
                BoardPaperDownload boardPaperObj;

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject obj = jArray.getJSONObject(i);

                    boardPaperObj = new BoardPaperDownload();
                    boardPaperObj.setBoardPaperAnswers(obj.getString("BoardPaperAnswers"));
                    boardPaperObj.setBoardPaperID(obj.getString("BoardPaperID"));
                    boardPaperObj.setStudentID(obj.getString("StudentID"));
                    boardPaperObj.setSubjectID(obj.getString("SubjectID"));
                    boardPaperObj.setCreatedOn(obj.getString("CreatedOn"));
                    mDb.insertWebRow(boardPaperObj, "board_paper_download");
                    BoardpaperArray.add(boardPaperObj);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
