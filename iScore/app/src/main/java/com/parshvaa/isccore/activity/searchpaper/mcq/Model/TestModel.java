package com.parshvaa.isccore.activity.searchpaper.mcq.model;

import java.io.Serializable;

public class TestModel implements Serializable {

    public String SubjectID;
    public String SubjectName;
    public String CreatedOn;
    public int StudentMCQTestHDRID;
    public int accuracy1;
    public int Total_Right;
    public int Total_que;


    public int getStudentMCQTestHDRID() {
        return StudentMCQTestHDRID;
    }

    public void setStudentMCQTestHDRID(int studentMCQTestHDRID) {
        StudentMCQTestHDRID = studentMCQTestHDRID;
    }


    public int getAccuracy1() {
        return accuracy1;
    }

    public void setAccuracy1(int accuracy1) {
        this.accuracy1 = accuracy1;
    }

    public int getTotal_Right() {
        return Total_Right;
    }

    public void setTotal_Right(int total_Right) {
        Total_Right = total_Right;
    }

    public int getTotal_que() {
        return Total_que;
    }

    public void setTotal_que(int total_que) {
        Total_que = total_que;
    }


    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }


    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }


}