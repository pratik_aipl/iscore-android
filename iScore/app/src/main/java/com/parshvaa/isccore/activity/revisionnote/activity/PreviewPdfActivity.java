package com.parshvaa.isccore.activity.revisionnote.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.revisionnote.HTMLUtils;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.SubQueKey;
import com.parshvaa.isccore.tempmodel.SubQueLabel;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.RomanNumber;
import com.parshvaa.isccore.utils.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

public class PreviewPdfActivity extends BaseActivity {
    public String HTML = "", SubQuestionTypeID = "";
    public int subQue_count = 1, label_count = 1, subQue_type_count = 1;
    public WebView pdfview;
    public MyDBManager mDb;
    public PreviewPdfActivity instance;
    public CursorParserUniversal cParse;
    public String subjectID = "0", ChapterID = "", chapterNumber = "", QuestionTypeIDs = "";
    ArrayList<String> mtcArray = new ArrayList<>();
    ArrayList<String> mtcQArray = new ArrayList<>();
    boolean isLastPrinted = true;
    public FloatingActionButton imgprint;
    public ImageView img_back, img_rotation, img_home;
    public TextView tv_title;
    public SubQueLabel subQuesLabelObj;
    public SubQueKey subQueskeyObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_preview_pdf);
        Utils.logUser();
        instance = this;
        cParse = new CursorParserUniversal();
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);

        showProgress(true, true);//Utils.showProgressDialog(instance);
        imgprint = findViewById(R.id.imgprint);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Preview Q & A");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_home = findViewById(R.id.img_home);
        pdfview = findViewById(R.id.pdfView);
        ChapterID = getIntent().getExtras().getString("ChapterIds");
        chapterNumber = getIntent().getExtras().getString("ChapterNumber");
        QuestionTypeIDs = getIntent().getExtras().getString("selectedQuesTypeIDs");


        pdfview.getSettings().setAllowFileAccess(true);
        pdfview.getSettings().setSupportZoom(true);
        pdfview.getSettings().setDisplayZoomControls(false);
        pdfview.getSettings().setAllowContentAccess(true);
        pdfview.getSettings().setLoadWithOverviewMode(true);
        pdfview.getSettings().setUseWideViewPort(true);
        pdfview.getSettings().setJavaScriptEnabled(true);
        pdfview.getSettings().setDomStorageEnabled(true);
        imgprint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                printPDF();
            }
        });

        new WorkCursor().execute();
        img_rotation = findViewById(R.id.img_rotation);
        img_rotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                }
            }
        });
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });


    }
    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState) {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }
    public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("Sure about visiting the dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        showProgress(true, true);//Utils.showProgressDialog(instance);

                        startActivity(new Intent(instance, DashBoardActivity.class));
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void printPDF() {
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = pdfview.createPrintDocumentAdapter();
        String jobName = getString(R.string.app_name) + " Report "
                + System.currentTimeMillis();
        PrintAttributes printAttrs = new PrintAttributes.Builder().
                setColorMode(PrintAttributes.COLOR_MODE_MONOCHROME).
                setMediaSize(PrintAttributes.MediaSize.NA_LETTER.asLandscape()).
                setMinMargins(PrintAttributes.Margins.NO_MARGINS).
                build();
        PrintJob printJob = printManager.print(jobName, printAdapter,
                printAttrs);
    }

    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                showProgress(false, true);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }


    @SuppressLint("LongLogTag")
    public void mtcLastAnsPrint() {
        if (mtcArray.size() > 0 && isLastPrinted) {
            String HTML1 = "";

            HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                    "<td align=\"right\"><b> ANS.&nbsp; </b></td>";
            int reminder = mtcArray.size() % 3;
            Utils.Log("Final Answer : reminder :: ", reminder + "");

            if (reminder == 0) {
                reminder = 3;
            }
            int startIndex = mtcArray.size() - reminder;

            for (int m = 1; m <= reminder; m++) {
                if (m != 1) {
                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                            "<td width=\"30\"><b></b></td>\n";
                }

                HTML1 = HTML1 + "<td class=\"question\" ><strong><div style=\"width:40%; float:left; \">"
                        + m + ") " + mtcQArray.get(startIndex) + " </div>" +
                        "<div style=\"width:5%; float:left;\" >Ans) &nbsp; </div> "
                        + "<div  style=\"width:50%; float:left;\">" + mtcArray.get(startIndex) + "</div></strong></td></tr>\n";
                startIndex++;
            }

            HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                    "<td width=\"30\"><b></b></td></br></tr>\n";
            HTML = HTML + HTML1;
            isLastPrinted = false;
        }
    }


    public class WorkCursor extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            Cursor c = mDb.getAllRows(DBQueries.getQuestions(subjectID, QuestionTypeIDs, ChapterID));
            // Utils.Log("QuestionTypeIDs", "==>" + QuestionTypeIDs);
            // Utils.Log("ChapterID", "==>" + ChapterID);

            // Utils.Log("Cursor isTableExists", DatabaseUtils.dumpCursorToString(c));

            //longInfo(DatabaseUtils.dumpCursorToString(c), "Cursor");

            if (c != null && c.moveToFirst()) {
                Date d = new Date();
                CharSequence currDate = DateFormat.format("dd-MMM-yyyy", d.getTime());
                HTML = new HTMLUtils(instance).getPrelimTestHeader((String) currDate, App.subObj.getSubjectName(), chapterNumber);
                int i = 1;
                int k = 1;
                int n = 65;
                int p = 0;
                int j = 1;
                int outerIndex = 1;
                float mark = 0;
                String MPSQID = "0";
                String LastQuetiionTypeID = "";
                do {
                    String IsQuesType = "";
                    String QuestionTypeID = c.getString(c.getColumnIndex("QuestionTypeID"));
                    int isMTC = mDb.getAllRecordsUsingQuery(DBQueries.checkQusTypeisMTC(QuestionTypeID + ""));
                    int isPessage = c.getInt(c.getColumnIndex("isPassage"));

                    Utils.Log("TAG", "CAURSOR::-->" + isMTC);
                    if (!QuestionTypeID.equals(LastQuetiionTypeID)) {
                        mtcLastAnsPrint();
                        i = 1;
                        QuestionTypeID = c.getString(c.getColumnIndex("QuestionTypeID"));
                        LastQuetiionTypeID = QuestionTypeID;
                        HTML = HTML + "<tr>\n" +
                                "<td width=\"40\"><b>Q." + k++ + ")</b></td>\n" +
                                "<td width=\"auto\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + "</b></td>\n" +
                                "<td align=\"right\" width=\"40\"><br/></td>\n" +
                                "</tr>";
                    }
                    if (!c.getString(c.getColumnIndex("QuestionTypeID")).equals(QuestionTypeID)) {
                        mtcLastAnsPrint();
                        i = 1;
                        QuestionTypeID = c.getString(c.getColumnIndex("QuestionTypeID"));
                        LastQuetiionTypeID = QuestionTypeID;

                        HTML = HTML + "<tr>\n" +
                                "<td width=\"40\"><b>Q." + k++ + ")</b></td>\n" +
                                "<td colspan=\"2\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + "</b></td>" +
                                "<td align=\"right\"><b>" + Integer.parseInt(c.getString(c.getColumnIndex("Marks"))) + "</b></td>\n" +
                                "</tr>";

                    } else if (isMTC == 1) {
                        Utils.Log("TAG", "CAURSOR::-->" + isMTC);

                        String finalHTML = Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question")));
                        String finalANS = Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer")));

                        if (IsQuesType.equals("")) {
                            IsQuesType = QuestionTypeID;
                            Utils.Log("Going in if  :->  ", "J  = " + j);
                            String HTML1 = "";

                            if ((mtcArray.size() % 3) == 0 && mtcArray.size() > 0) {
                                j = 1;
                                n = 65;
                                HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                        "<td align=\"right\"><b> ANS.&nbsp; </b></td>";
                                int startIndex = mtcArray.size() - 3;
                                for (int m = 1; m <= 3; m++) {
                                    if (m != 1) {
                                        HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                                "<td width=\"30\"><b></b></td>\n";
                                    }
                                    HTML1 = HTML1 + "<td class=\"question\" ><strong><div style=\"width:40%; float:left; \">"
                                            + m + ") " + mtcQArray.get(startIndex) + " </div>" +
                                            "<div style=\"width:5%; float:left;\" >Ans) &nbsp; </div> "
                                            + "<div  style=\"width:50%; float:left;\">" + mtcArray.get(startIndex) + "</div></strong></td></tr>\n";
                                    startIndex++;
                                }
                                HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                        "<td colspan='2'><br></td></tr>\n";
                            }


                            if (((mtcArray.size() % 3) == 0 && mtcArray.size() > 0) || mtcArray.size() == 0) {
                                HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                        "<td align=\"right\"><b>(" + (outerIndex++) + ")&nbsp;</b></td>\n";
                            } else {
                                HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                        "<td width=\"30\"><b></b></td>\n";
                            }
                            HTML1 = HTML1 + "<td class=\"question\" ><div style=\"width:40%; float:left; \">"
                                    + j++ + ") " + finalHTML + " </div>" +
                                    "<div style=\"width:auto; float:left;\" >" + (char) n++ + ")&nbsp;</div> " + "<div id='mtc" + (p) + "' style=\"width:50%; float:left;\">" + finalANS + "</div></td>\n";
                            mtcQArray.add(finalHTML);
                            mtcArray.add(finalANS);
                            p++;
                            HTML1 = HTML1 + "<td align=\"right\" width=\"10\"><b></b></td>\n" + "</tr>";
                            HTML = HTML + HTML1;
                        }
                    } else if (isPessage == 1) {


                        HTML = HTML +
                                "<table>" +
                                "   <tr class=\"margin_top\">\n" +
                                "       <td valign=\"top\"><b>" + i + ")</b></td>" +
                                "       <td colspan=\"2\" class=\"question\">" +
                                "           <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                                "               <tbody>" +
                                "                    <tr>" +
                                "                        <td style=\"text-align:justify\">" +
                                                                Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))) +
                                "                        </td>" +
                                "                    </tr>" +
                                "               </tbody>" +
                                "           </table>" +
                                "       </td>" +
                                "   </tr>" +
                                "   <tr>\n" +
                                "       <td valign=\"top\"><b>Ans</b></td>" +
                                "       <td colspan=\"2\" class=\"answer\">" + Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer"))) + "</td>\n" +
                                "   </tr>";

                        Cursor clabel = mDb.getAllRows(DBQueries.getLabel(c.getString(c.getColumnIndexOrThrow("MQuestionID"))));
                        //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                        if (clabel != null && clabel.moveToFirst()) {

                            do {
                                label_count = 1;
                                subQue_type_count = 1;
                                subQuesLabelObj = new SubQueLabel();
                                subQuesLabelObj.setLabel(clabel.getString(clabel.getColumnIndexOrThrow("Label")));

                                mark = clabel.getInt(clabel.getColumnIndexOrThrow("Mark"));

                                subQueskeyObj = new SubQueKey();

                                SubQuestionTypeID = "";
                                Cursor cQue = mDb.getAllRows(DBQueries.getPassageSubquetionsRivistion(MPSQID,
                                        c.getString(c.getColumnIndexOrThrow("MQuestionID")),
                                        c.getString(c.getColumnIndexOrThrow("QuestionTypeID")), String.valueOf(mark), clabel.getString(clabel.getColumnIndexOrThrow("PassageSubQuestionTypeID"))));
                                //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                                if (cQue != null && cQue.moveToFirst()) {
                                    subQue_count = 1;

                                    do {
                                        if (!SubQuestionTypeID.equals(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestionTypeID")))) {

                                            HTML = HTML +
                                                    "<tr class=\"margin_top\">" +
                                                    "   <td width=\"30\"></td>" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"><b>";
                                            if (label_count == 1) {
                                                HTML = HTML + subQuesLabelObj.getLabel() + ")</b>" +
                                                        "</td>";
                                                label_count++;
                                            }

                                            HTML = HTML +
                                                    "   <td width=\"660\">" +
                                                    "       <table width=\"100%\">" +
                                                    "           <tr>" +
                                                    "               <td width=\"630\">";
                                            HTML = HTML + RomanNumber.toRoman(subQue_type_count) + ") " + cQue.getString(cQue.getColumnIndexOrThrow("SubQuestionType"));
                                            HTML = HTML + "         </td>\n";
                                            double marks = Float.parseFloat(cQue.getString(cQue.getColumnIndexOrThrow("Marks")));

                                            DecimalFormat format = new DecimalFormat();
                                            format.setDecimalSeparatorAlwaysShown(false);

                                            HTML = HTML + "        <td align=\"right\" width=\"30\">" + format.format(marks) + "</td>" +
                                                    "          </tr>\n" +
                                                    "       </table>\n" +
                                                    "  </td>" +
                                                    "</tr>" +
                                                    "<tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 3%;\" class=\"sub\">\n" +
                                                    "       <table width=\"100%\">" +
                                                    "          <tr>" +
                                                    "               <td width=\"30\" style=\"vertical-align: top\">" + subQue_count + ")  </td>" +
                                                    "               <td width=\"630\">";
                                            HTML = HTML + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestion")));
                                            HTML = HTML + "         </td>\n" +
                                                    "          </tr>" +
                                                    "          <tr>" +
                                                    "            <td width=\"30\" style=\"vertical-align: top\"><b>Ans .</b></td>" +
                                                    "            <td width=\"630\">";
                                            HTML = HTML + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubAnswer")));
                                            HTML = HTML + "      </td>\n" +
                                                    "          </tr>" +
                                                    "       </table>" +
                                                    "    </td>" +
                                                    "   <td align=\"right\" width=\"30\"></td>" +
                                                    "</tr>";

                                            SubQuestionTypeID = cQue.getString(cQue.getColumnIndexOrThrow("SubQuestionTypeID"));
                                            subQue_count++;
                                            subQue_type_count++;
                                        } else {
                                            HTML = HTML + /*"1</tr>" +
                                                    " <tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";

                                            HTML = HTML + subQue_count + ") " + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestion")));

                                            HTML = HTML + "   </td>\n" +
                                                    "   <td align=\"right\" width=\"30\"></td>\n" +
                                                    " </tr>      ";*/
                                                    "<tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 3%;\" class=\"sub\">\n" +
                                                    "       <table width=\"100%\">" +
                                                    "          <tr>" +
                                                    "               <td width=\"30\" style=\"vertical-align: top\">" + subQue_count + ")  </td>" +
                                                    "               <td width=\"630\">";
                                            HTML = HTML + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubQuestion")));
                                            HTML = HTML + "         </td>\n" +
                                                    "          </tr>" +
                                                    "          <tr>" +
                                                    "            <td width=\"30\" style=\"vertical-align: top\"><b>Ans .</b></td>" +
                                                    "            <td width=\"630\">";
                                            HTML = HTML + Utils.replaceImagesPath(cQue.getString(cQue.getColumnIndexOrThrow("SubAnswer")));
                                            HTML = HTML + "      </td>\n" +
                                                    "          </tr>" +
                                                    "       </table>" +
                                                    "    </td>" +
                                                    "   <td align=\"right\" width=\"30\"></td>" +
                                                    "</tr>";
                                            subQue_count++;
                                        }
                                    } while (cQue.moveToNext());
                                    cQue.close();
                                }


                            } while (clabel.moveToNext());
                            clabel.close();

                            HTML = HTML + "   </table>\n" ;
                        }

                    } else {
                        mtcLastAnsPrint();
                        HTML = HTML + "<tr>\n" +
                                "<td valign=\"top\"><b>" + i + ")</b></td>" +
                                "<td colspan=\"2\" class=\"question\">" + Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))) + "</td>\n" +
                                "</tr>" +
                                "<tr>\n" +
                                "<td valign=\"top\"><b>Ans</b></td>" +
                                "<td colspan=\"2\" class=\"answer\">" + Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer"))) + "</td>\n" +
                                "</tr>";
                    }
                    i++;
                }
                while (c.moveToNext());
                c.close();
                mtcLastAnsPrint();
                HTML = HTML + "</table>" +
                        "\n" +
                        "<div style=\"height:150px !important; \"></div>     " +
                        "           </div>    \n" +
                        "            </div>\n" +

                        "       </div> \n" +
                        "" + HTMLUtils.getRevisionNoteShuffleScript();

                int increment = 1;
                HTML = HTML + " var obj = {};";
                HTML = HTML + " obj.product" + increment + " = [];";
                for (int a = 1; a < mtcArray.size() + 1; a++) {
                    if (a > 1 && (a % 3 == 1)) {
                        increment++;
                        HTML = HTML + " obj.product" + increment + " = [];";

                    }

                    String s = mtcArray.get(a - 1).replaceAll(System.getProperty("line.separator"), "");
                    HTML = HTML + " \n obj.product" + increment + ".push(\"" + s + "\");";
                }


                HTML = HTML + "\n" +
                        "            var $i=0;\n" +
                        "            $.each(obj, function (idx, product) {\n" +
                        "                \n" +
                        "                var shuffledArray = shuffleArray(product);\n" +
                        "                \n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[0];\n" +
                        "                $i++;\n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[1];\n" +
                        "                $i++;\n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[2];\n" +
                        "                $i++;\n" +
                        "                \n" +
                        "            });";


                HTML = HTML + "    </script> </body>\n" +
                        "</html>";

                longInfo(HTML, " RIVISE HTML :-> ");
            } else {
                new AlertDialog.Builder(instance)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("oops..!")
                        .setCancelable(false)
                        .setMessage("No Questions Found ...!")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .show();
            }
            return HTML;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String HTML) {
            WebSettings settings = pdfview.getSettings();
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setBuiltInZoomControls(true);
            settings.setJavaScriptEnabled(true);
            settings.setSupportZoom(true);
            settings.setDomStorageEnabled(true);
            pdfview.setWebViewClient(new myWebClient());

            pdfview.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");

        }
    }

    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }
}
