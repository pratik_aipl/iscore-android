package com.parshvaa.isccore.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.OtpView;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.MainData;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


public class VerifyOtpActivity extends BaseActivity implements AsynchTaskListner {
    public Button btn_resend, btn_confirm, btn_cancel;
    private OtpView mOtpView;

    private static final String TAG = "VerifyOtpActivity";
    public String name, str_amount = "", OTP = "", surname, mobile, email, boardID, mediumID,
            standardID, packageID,SchoolName, version, RegKey, Guid, PleyaerID = "";
    public App app;
    public boolean isFromMobileNoLogin = false;
    public Handler customHandler = new Handler();
    boolean isDemo = false, isKeyRegister = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        Utils.logUser();
        app = App.getInstance();
        isDemo = getIntent().getBooleanExtra(Constant.isDemo, false);

        isKeyRegister = getIntent().getBooleanExtra(Constant.isKeyRegiste, false);
        mySharedPref = new MySharedPref(VerifyOtpActivity.this);

        if (!mySharedPref.getGuuid().isEmpty()) {
            Guid = mySharedPref.getGuuid();
        } else {
            Guid = UUID.randomUUID().toString();
        }
        mOtpView = findViewById(R.id.otp_view);
        btn_resend = findViewById(R.id.btn_resend);
        btn_confirm = findViewById(R.id.btn_confirm);
        btn_cancel = findViewById(R.id.btn_cancel);
        if (BuildConfig.DEBUG && !TextUtils.isEmpty(App.Otp)) {
            mOtpView.setOTP(App.Otp);
        }
        if (getIntent().hasExtra(Constant.MobLogin)) {
            mobile = MainUser.getRegMobNo();
            email = MainUser.getEmailID();
            OTP = App.Otp;
            isFromMobileNoLogin = true;
        } else {
            try {
                IsFromLOginSendOTP();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        btn_cancel.setOnClickListener(view -> onBackPressed());
        new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                SimpleDateFormat tempDf = new SimpleDateFormat("mm:ss", Locale.ENGLISH);
                btn_resend.setText("" + String.format("%02d : %02d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

            }

            public void onFinish() {
                btn_resend.setOnClickListener(view -> {
                    start();
                    OTP = "";
                    new CallRequest(VerifyOtpActivity.this).resend_otp(mobile);
                });
                btn_resend.setText("Resend");
            }
        }.start();


        btn_confirm.setOnClickListener(v -> {
            if (Utils.isNetworkAvailable(this)) {
                btn_confirm.setEnabled(false);
                Bundle bundle = new Bundle();
                if (isDemo) {
                    bundle.putString(FirebaseAnalytics.Param.METHOD, "Demo");
                    mFirebaseAnalytics.logEvent("DEMO", bundle);
                } else if (isKeyRegister) {
                    bundle.putString(FirebaseAnalytics.Param.METHOD, "Register");
                    mFirebaseAnalytics.logEvent("KEYLOGIN", bundle);
                } else if (!isFromMobileNoLogin) {
                    bundle.putString(FirebaseAnalytics.Param.METHOD, "Register");
                    mFirebaseAnalytics.logEvent("SIGNUP", bundle);
                } else {
                    mFirebaseAnalytics.logEvent("MOBILELOGIN", bundle);
                }

                verfyOTPandFrowedNext();
            } else {
                Utils.showToast("Please connect internet to Verify OTP", this);
            }
        });
    }

    public void verfyOTPandFrowedNext() {

        if (OTP.equalsIgnoreCase(mOtpView.getOTP())) {

            if (isFromMobileNoLogin) {
                if (PleyaerID.equalsIgnoreCase("")) {
                    PleyaerID = Utils.OneSignalPlearID();
                    showProgress(true, true);//Utils.showProgressDialog(instance);
                    customHandler.postDelayed(updateTimerThread, 0);
                } else {
                    new CallRequest(this).update_last_sync_time(PleyaerID);
                }
            } else {
                if (App.isFromNewRegister) {
                    startActivity(new Intent(this, BoardActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(this, RegistrationConfirmActivity.class).putExtra("VerifyOtp", "true"));
                    finish();

                }
            }
        } else {
            btn_confirm.setEnabled(true);
            Utils.showToast("INVALID OTP", VerifyOtpActivity.this);
        }

    }

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            if (!PleyaerID.equalsIgnoreCase("")) {
                new CallRequest(VerifyOtpActivity.this).update_last_sync_time(PleyaerID);
            } else {
                PleyaerID = Utils.OneSignalPlearID();
                customHandler.postDelayed(this, 1000);
            }

        }
    };

    @Override
    public void onBackPressed() {

        this.finish();
    }

    public void IsFromLOginSendOTP() {
        name = getIntent().getExtras().getString("name");
        OTP = getIntent().getExtras().getString("OTP");
        surname = getIntent().getExtras().getString("surname");
        mobile = getIntent().getExtras().getString("mobile");
        email = getIntent().getExtras().getString("email");
        boardID = getIntent().getExtras().getString("boardID");
        mediumID = getIntent().getExtras().getString("mediumID");
        standardID = getIntent().getExtras().getString("standardID");
        SchoolName = getIntent().getExtras().getString("SchoolName");
        version = getIntent().getExtras().getString("version");
        RegKey = getIntent().getExtras().getString("RegKey");
        Guid = getIntent().getExtras().getString("Guid");
        packageID = getIntent().getExtras().getString("packageID");
        str_amount = getIntent().getExtras().getString("str_amount");

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case resend_otp:
                    btn_confirm.setEnabled(true);
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            OTP = jObj.getString("otp");
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getTableRecords:
                    showProgress(false, true);
                    try {
                        MainData mainData = LoganSquare.parse(result, MainData.class);
                        if (mainData.isStatus()) {
                            tableRecords = mainData.getData();
                            prefs.save(Constant.tableRecords, gson.toJson(mainData.getData()));
                            Log.d(TAG, "onTaskCompleted: " + prefs.getString(Constant.tableRecords, ""));
                        }
                        callPage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case update_last_sync_time:
                    customHandler.removeCallbacks(updateTimerThread);
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (Utils.isNetworkAvailable(VerifyOtpActivity.this)) {
                                showProgress(true, false);
                                new CallRequest(this).getTableRecords();
                            } else {
                                callPage();
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                            onBackPressed();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void callPage() {
        mySharedPref.setIsLoggedIn(true);
        mySharedPref.setDownloadStatus("n");
        mySharedPref.setDownloadStatus("y");
        mySharedPref.setIsProperLogin("true");

        if (!TextUtils.isEmpty(RegKey)) {

        }
        Intent i = new Intent(this, DashBoardActivity.class);
        i.putExtra("sync", "sync");
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

}
