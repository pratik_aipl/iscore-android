package com.parshvaa.isccore.activity.revisionnote.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.TextView;

import com.nineoldandroids.view.ViewPropertyAnimator;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.revisionnote.activity.PreviewRevisionNoteActivity;
import com.parshvaa.isccore.activity.revisionnote.adapter.PreviewRevisionNoteAdapter;
import com.parshvaa.isccore.tempmodel.TempQuestionTypes;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by empiere-vaibhav on 1/17/2018.
 */

public class PreviewRevisionNoteFragment extends Fragment {
    public View v;
    private final String ARG_PAGE_NUMBER = "page_number";
    private ArrayList<String> mKeys=new ArrayList<>();

    @BindView(R.id.recycler_paper)
    RecyclerView recyclerPaper;

    Unbinder unbinder;
    List<TempQuestionTypes> typesArrayList = new ArrayList<>();
    private PreviewRevisionNoteAdapter mAdapter;
    private boolean mFabIsShown;
    HashMap<String, ArrayList<TempQuestionTypes>> stringArrayListHashMap = new HashMap<>();
    public ArrayList<TempQuestionTypes> tempArray = new ArrayList<>();
    public TextView tv_que_no;


    int pageNumber;
    public Fragment newInstance(int page, List<TempQuestionTypes> arrayList, HashMap<String, ArrayList<TempQuestionTypes>> stringArrayListHashMap) {
        PreviewRevisionNoteFragment fragment = new PreviewRevisionNoteFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        args.putSerializable("list", (Serializable) arrayList);
        args.putSerializable("hasList", (Serializable) stringArrayListHashMap);
        fragment.setArguments(args);
        return fragment;

    }

//    @Override
//    public void setMenuVisibility(final boolean visible) {
//
//        if (visible) {
////            pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
//            typesArrayList.clear();
//            typesArrayList = (List<TempQuestionTypes>) getArguments().getSerializable("page");
//            if (mAdapter != null)
//                mAdapter.notifyDataSetChanged();
//        }
//
//    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_paper_solution, container, false);
        unbinder = ButterKnife.bind(this, v);

        recyclerPaper.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
        recyclerPaper.scheduleLayoutAnimation();
        recyclerPaper.setLayoutManager(new LinearLayoutManager(getActivity()));
        typesArrayList.clear();

        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
        stringArrayListHashMap = (HashMap<String, ArrayList<TempQuestionTypes>>) getArguments().getSerializable("hasList");
        mKeys = new ArrayList<>(stringArrayListHashMap.keySet());
        typesArrayList = stringArrayListHashMap.get(getKey(pageNumber));

//        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
//        stringArrayListHashMap = (HashMap<String, ArrayList<TempQuestionTypes>>) getArguments().getSerializable("hasList");
//        typesArrayList = stringArrayListHashMap.keySet().get(pageNumber);

        mAdapter = new PreviewRevisionNoteAdapter(typesArrayList, getActivity(), recyclerPaper);
        recyclerPaper.setAdapter(mAdapter);

        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recyclerPaper.addItemDecoration(new ItemOffsetDecoration(spacing));
        recyclerPaper.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    hideFab();
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    hideFab();
                } else {
                    showFab();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        return v;

    }

    public String getKey(int position) {
        return new ArrayList<>(stringArrayListHashMap.keySet()).get(position);
    }

    private void showFab() {
        if (!mFabIsShown) {
            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_left_button).scaleX(1).scaleY(1).setDuration(200).start();
            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_right_button).scaleX(1).scaleY(1).setDuration(200).start();
            mFabIsShown = true;
        }
    }

    private void hideFab() {
        if (mFabIsShown) {
            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_right_button).scaleX(0).scaleY(0).setDuration(200).start();

            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((PreviewRevisionNoteActivity) getActivity()).float_left_button).scaleX(0).scaleY(0).setDuration(200).start();
            mFabIsShown = false;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

