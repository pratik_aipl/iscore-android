package com.parshvaa.isccore.activity.mcq;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.adapter.SelectSubjectAdapter;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.Chapter;
import com.parshvaa.isccore.model.ImageData;
import com.parshvaa.isccore.model.ImageRecords;
import com.parshvaa.isccore.model.McqOption;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.Paper_type;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.McqData;
import com.parshvaa.isccore.tempmodel.SubjectAccuracy;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.MessageEvent;
import com.parshvaa.isccore.utils.RobotoTextView;
import com.parshvaa.isccore.utils.Utils;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.parshvaa.isccore.utils.Utils.closeDb;

public class SubjectMcqActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "SubjectMcqActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_refresh)
    ImageView img_refresh;
    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.mDownloadMcqTest)
    ImageView mDownloadMcqTest;
    @BindView(R.id.mProgressMcqTest)
    ProgressBar mProgressMcqTest;
    @BindView(R.id.mProgressMcqTestValue)
    TextView mProgressMcqTestValue;
    @BindView(R.id.mErrorMsg)
    TextView mErrorMsg;
    @BindView(R.id.mTransViewMcqTest)
    RelativeLayout mTransViewMcqTest;

    public MyDBManager mDb;
    public static App app;


    public int myImgTempCounter = 0;
    public LinkedHashMap<String, String> mapImages = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_select_subject);
        ButterKnife.bind(this);
        img_refresh.setVisibility(View.VISIBLE);
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        Utils.logUser();
        tvTitle.setText("Select Subject");
        app = App.getInstance();
        App.mcqdata = new McqData();
        setupRecyclerView();
        setProgressMaxValue();
        mProgressMcqTestValue.setText("Click on the icon to download MCQ.");
        callProgressUpdate(false);
    }

    private void setProgressMaxValue() {
        Log.d(TAG, "tableRecords>> "+tableRecords);
        if (tableRecords != null) {
            int mcqTotal = tableRecords.getSubjects() + tableRecords.getChapters() + tableRecords.getPaper_type()
                    + tableRecords.getMcqquestion() + tableRecords.getMcqoption();
            mProgressMcqTest.setMax(mcqTotal);
            Log.d(TAG, "callProgressUpdate>> "+mcqTotal);
        }

    }

    private void callProgressUpdate(boolean isFirst) {
        Log.d(TAG, "mDb>> "+mDb);
        Log.d(TAG, "mDbisDbOpen>> "+mDb.isDbOpen());

        if (mDb != null && mDb.isDbOpen()) {
            int mcqTotal = mDb.getTableCount(Constant.subjects)
                    + mDb.getTableCount(Constant.chapters)
                    + mDb.getTableCount(Constant.paper_type)
                    + mDb.getTableCount(Constant.mcqquestion)
                    + mDb.getTableCount(Constant.mcqoption);
            Log.d(TAG, "callProgressUpdate: "+mcqTotal);
            Log.d(TAG, "mProgressMcqTest: "+mProgressMcqTest);
            if (mProgressMcqTest != null) {
                mProgressMcqTest.setProgress(mcqTotal);
                Log.d(TAG, "getMax: "+mProgressMcqTest.getMax());
                Log.d(TAG, "mProgressMcqTest.getProgress(): "+mProgressMcqTest.getProgress());

         /*       int max;
                if(mProgressMcqTest.getMax()!=0){
                    max=mProgressMcqTest.getMax();
                }else{
                    max=1;
                }*/
                int mcqPer = (mProgressMcqTest.getProgress() * 100) / mProgressMcqTest.getMax();
                Log.d(TAG, "mcqPer>>"+mcqPer);

                if (isFirst) {
                    mProgressMcqTest.setVisibility(View.VISIBLE);
                    if (mcqPer < 99)
                        mProgressMcqTestValue.setText(String.format(getString(R.string.progress_per), mcqPer) + "%");
                } else {
                    mProgressMcqTest.setVisibility(View.GONE);
                }
                Log.d(TAG, "callProgressUpdate: 11 -->  " + mcqPer);
                if (mcqPer >= 99) {
                    if (mySharedPref.getMcqImageDownloadStatus())
                        viewUpdate();
                    else {
                        callDownloadImage(true);
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    mTransViewMcqTest.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    private void setupRecyclerView() {
        int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.mcqdata = new McqData();
    }

    @OnClick({R.id.img_back, R.id.mDownloadMcqTest, R.id.img_refresh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.mDownloadMcqTest:
                if (Utils.isNetworkAvailable(this)) {
                    callDownloadEvent();
                } else {
                    Utils.showToast(getString(R.string.no_internet_msg), this);
                }
                break;
                case R.id.img_refresh:
                if (Utils.isNetworkAvailable(this)) {
                    recyclerView.setVisibility(View.GONE);
                    mTransViewMcqTest.setVisibility(View.VISIBLE);
                    callDownloadEvent();
                } else {
                    Utils.showToast(getString(R.string.no_internet_msg), this);
                }
                break;
        }
    }

    private void callDownloadEvent() {
        mProgressMcqTest.setVisibility(View.VISIBLE);
        mProgressMcqTestValue.setVisibility(View.VISIBLE);
        mErrorMsg.setText("");
        if (Utils.isNetworkAvailable(this))
            subjectDataDownloadNew();
        else {
            Utils.showToast(getString(R.string.no_internet_msg), this);
        }
    }

    // TODO: 8/3/2019 -> subject
    public void subjectDataDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.subjects);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getSubjects()) {
                    mDb.emptyTable(Constant.subjects);
                    new CallRequest(this).getSubjectTable();
                } else {
                    chapterDataDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter
    public void chapterDataDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.chapters);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getChapters()) {
                    mDb.emptyTable(Constant.chapters);
                    new CallRequest(this).getChapterTable();
                } else {
                    paperTypesDownloadNew();
                }
            }
        }
    }

    // TODO: 8/3/2019 sget_question_typesubject -> Chapter -> PaperType
    public void paperTypesDownloadNew() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.paper_type);
            if (tableRecords != null) {
                if (total == 0 || total < tableRecords.getPaper_type()) {
                    mDb.emptyTable(Constant.paper_type);
                    new CallRequest(this).getPaper_Type();
                } else {
                    mcqQuestionsDownload();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question -> passage_sub_question_type -> sub_question_types -> McqQuestions
    public void mcqQuestionsDownload() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.mcqquestion);
            int id = mDb.getLastRecordIDForRequest(Constant.mcqquestion, "MCQQuestionID");
            if (tableRecords != null) {
                if (total >= 0 && total < tableRecords.getMcqquestion()) {
                    new CallRequest(this).getMcqQuetions(id);
                } else {
                    mcqOptionsDownload();
                }
            }
        }
    }

    // TODO: 8/3/2019 subject -> Chapter -> PaperType -> ExamtypeSubject -> ExamType -> ExamTypePattern -> ExamTypePatternDetail -> master_passage_sub_question -> passage_sub_question_type -> sub_question_types -> McqQuestions -> McqOptions
    public void mcqOptionsDownload() {
        if (mDb.isDbOpen()) {
            int total = mDb.getTableCount(Constant.mcqoption);
            int id = mDb.getLastRecordIDForRequest(Constant.mcqoption, "MCQOPtionID");
            if (tableRecords != null) {
                Log.d(TAG, "mcqOptionsDownload: "+tableRecords.getMcqoption());
                if (total >= 0 && total < tableRecords.getMcqoption()) {
                    new CallRequest(this).getMCQOptions(id);
                } else {
                    callDownloadImage(true);
                }
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            callProgressUpdate(true);
            switch (request) {
                // TODO: 8/3/2019  1.Subject download
                case getSubjectTable:
                    new insertSubjectRow().execute(result);
                    break;
                // TODO: 8/3/2019 2. Chapter download
                case getChapterTable:
                    new insertChapterRow().execute(result);
                    break;
                // TODO: 8/3/2019 3. Paper Type Download
                case getPaper_Type:
                    new insertPaperTypesRow().execute(result);
                    break;
                case getMcqQuetions:
                    new insertMcqQuetionsRow().execute(result);
                    break;
                case getMcqOptions:
                    new insertMcqOptionsRow().execute(result);
                    break;
                // TODO: 8/3/2019  McqQuestionImages
                case getMcqQuestionImages:
                    new downloadMCQImages().execute(result);
                    break;
            }
        }
    }

    public class insertSubjectRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<Subject> subjectList = LoganSquare.parseList(jArray.toString(), Subject.class);
                if (mDb.isDbOpen()) {
                    //();
                    for (int i = 0; i < subjectList.size(); i++) {
                        mDb.insertRow(subjectList.get(i), Constant.subjects);
                    }
                    Utils.setTransaction(mDb);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            chapterDataDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertChapterRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<Chapter> chapterList = LoganSquare.parseList(jArray.toString(), Chapter.class);
                if (mDb.isDbOpen()) {
                    for (int i = 0; i < chapterList.size(); i++) {
                        mDb.insertRow(chapterList.get(i), Constant.chapters);
                    }
                    Utils.setTransaction(mDb);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            paperTypesDownloadNew();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertPaperTypesRow extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONArray jArray = new JSONArray(params[0]);
                List<Paper_type> paper_types = LoganSquare.parseList(jArray.toString(), Paper_type.class);
                if (paper_types != null && paper_types.size() > 0) {
                    if (mDb.isDbOpen()) {
                        //();
                        for (int i = 0; i < paper_types.size(); i++) {
                            mDb.insertRow(paper_types.get(i), Constant.paper_type);
                        }
                        Utils.setTransaction(mDb);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            mcqQuestionsDownload();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertMcqQuetionsRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<McqQuestion> mcqQuestionList = LoganSquare.parseList(new JSONArray(params[0]).toString(), McqQuestion.class);
                if (mcqQuestionList != null && mcqQuestionList.size() > 0) {
                    if (mDb.isDbOpen()) {
                        //();
                        for (int i = 0; i < mcqQuestionList.size(); i++) {
                            mDb.insertRow(mcqQuestionList.get(i), Constant.mcqquestion);
                        }
                        Utils.setTransaction(mDb);
                    }
                    isNext = false;
                } else {
                    isNext = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            if (isNext) {
                mcqOptionsDownload();
            } else {
                mcqQuestionsDownload();
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public class insertMcqOptionsRow extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isNext = false;
            try {
                List<McqOption> mcqOptionList = LoganSquare.parseList(new JSONArray(params[0]).toString(), McqOption.class);
                Log.d(TAG, "mcqOptionList: "+mcqOptionList.size());
                if (mcqOptionList.size() > 0) {
                    if (mDb.isDbOpen()) {
                        for (int i = 0; i < mcqOptionList.size(); i++) {
                            mDb.insertRow(mcqOptionList.get(i), Constant.mcqoption);
                            Log.d(TAG, "mcqoption: "+mcqOptionList.get(i));

                        }
                        Utils.setTransaction(mDb);
                    }
                    isNext = false;
                } else {
                    isNext = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return isNext;
        }

        @Override
        protected void onPostExecute(Boolean isNext) {
            if (isNext) {
                callDownloadImage(true);
            } else {
                mcqOptionsDownload();
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void callDownloadImage(boolean b) {
        mProgressMcqTestValue.setText(String.format(getString(R.string.progress_per), 99) + "%");
        new CallRequest(SubjectMcqActivity.this).getMcqQuestionImages();
    }

    public class downloadMCQImages extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                ImageData imageData = LoganSquare.parse(params[0], ImageData.class);
                if (imageData.isStatus()) {
                    ImageRecords imageRecords = imageData.getImageRecordsList();

//                    if (imageRecords.getSubjectIcon() != null) {
//                        SubjectIcon subjectIcon = imageRecords.getSubjectIcon();
//                        File imageDir = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/");
//                        if (subjectIcon != null && subjectIcon.getImages().size() > 0) {
//                            for (int i = 0; i < subjectIcon.getImages().size(); i++) {
//                                if (Utils.isNetworkAvailable(SubjectMcqActivity.this)) {
//                                    if (!imageDir.exists()) {
//                                        imageDir.mkdirs();
//                                    }
//                                    String imgUrl = IMAGEURL + "subject_icon/" + subjectIcon.getImages().get(i);
//                                    downloadFile(imgUrl, Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + subjectIcon.getImages().get(i));
//                                }
//                            }
//                        }
//                    }
                    if (imageRecords.getQuestionImages() != null) {
                        for (int k = 0; k < imageRecords.getQuestionImages().size(); k++) {
                            String folderPath = String.valueOf(imageRecords.getQuestionImages().get(k));
                            if (folderPath.contains("http:")) {
                              //  folderPath = folderPath.substring(26);
                                folderPath = folderPath.substring(24);
                                folderPath = folderPath.substring(0, folderPath.lastIndexOf("/"));
                            } else {
                                //folderPath = folderPath.substring(27);
                                folderPath = folderPath.substring(25);
                                folderPath = folderPath.substring(0, folderPath.lastIndexOf("/"));
                            }
                            if (folderPath.contains("../")) {
                                folderPath = folderPath.replace("../", "");
                            }
                            if (folderPath.contains("&amp;")) {
                                folderPath = folderPath.replaceAll("&amp;", "&");
                            }
                            String tempPath = imageRecords.getQuestionImages().get(k);
                            String fileName = tempPath.substring(tempPath.lastIndexOf("/") + 1);
                            String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + folderPath + "/" + fileName;
                            File imageFile = new File(SDCardPath);
                            if (Utils.isNetworkAvailable(SubjectMcqActivity.this)) {
                                File dir = new File(imageFile.getParent());
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                downloadFile(imageRecords.getQuestionImages().get(k), SDCardPath);
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            mySharedPref.setMcqImageDownloadStatus(true);
//            new CountDownTimer(2000, 1000) { // adjust the milli seconds here
//                public void onTick(long millisUntilFinished) {
//                    mProgressMcqTestValue.setText(""+String.format("Image Downloading.. %d min, %d sec",
//                            TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
//                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
//                }
//
//                public void onFinish() {
            mProgressMcqTestValue.setText("done!");
            callProgressUpdate(false);
//                }
//            }.start();

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void downloadFile(String url, String path) {
        Log.d(TAG, "downloadFile() called with: url = [" + url + "], path = [" + path + "]");

        File qLocalFile = new File(path);
        Request request = null;
        if (!qLocalFile.exists()) {
            request = new Request(url, path);
            request.setPriority(Priority.HIGH);
            request.setNetworkType(NetworkType.ALL);
            fetch.enqueue(request, updatedRequest -> {
                Log.d(TAG, "manageImagesDownload: ");
            }, error -> {
                Toast.makeText(this, "error enqueue download!!!", Toast.LENGTH_SHORT).show();
            });

        }

    }

    private void viewUpdate() {
        try {
            recyclerView.setAdapter(new SelectSubjectAdapter((ArrayList) new CustomDatabaseQuery(this, new SubjectAccuracy()).execute(new String[]{DBNewQuery.get_subject_for_mcq_with_accuracy()}).get(), this));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        recyclerView.setVisibility(View.VISIBLE);
        mTransViewMcqTest.setVisibility(View.GONE);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        if (fetch !=null)
        fetch.close();
        closeDb(mDb);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        mProgressMcqTestValue.setText("Click on the icon to download MCQ.");
        callProgressUpdate(false);
        if (event.isFaild())
            mErrorMsg.setText("Please retry.");
    }

}
