package com.parshvaa.isccore.utils;

public class TableReload {
    boolean isReload;

    public TableReload(boolean isReload) {
        this.isReload = isReload;
    }

    public boolean isReload() {
        return isReload;
    }

    public void setReload(boolean reload) {
        isReload = reload;
    }
}
