package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karan - Empiere on 3/4/2017.
 */

@JsonObject
public class StudentMcqTestHdr implements Serializable {

    @JsonField
    public String TestTime = "",StudentID = "", AddType = "A", BoardID = "", MediumID = "", SubjectID = "",  Timeleft = "", CreatedBy = "", CreatedOn = "", ModifiedBy = "", ModifiedOn = "";
    @JsonField
    public int StudentMCQTestHDRID = -1, TimerStatus;

    @JsonField(name = "chapterArray")
    public List<StudentMcqTestChap> chapterArray = new ArrayList<>();
    @JsonField(name = "detailArray")
    public List<StudentMcqTestDtl> detailArray = new ArrayList<>();
    @JsonField(name = "levelArray")
    public List<StudentMcqTestLevel> levelArray = new ArrayList<>();


    public int getStudentMCQTestHDRID() {
        return StudentMCQTestHDRID;
    }

    public void setStudentMCQTestHDRID(int studentMCQTestHDRID) {
        StudentMCQTestHDRID = studentMCQTestHDRID;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }



    public String getTimeleft() {
        return Timeleft;
    }

    public void setTimeleft(String timeleft) {
        Timeleft = timeleft;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public String getAddType() {
        return AddType;
    }

    public void setAddType(String addType) {
        AddType = addType;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }


    public String getTestTime() {
        return TestTime;
    }

    public void setTestTime(String testTime) {
        TestTime = testTime;
    }

    public int getTimerStatus() {
        return TimerStatus;
    }

    public void setTimerStatus(int timerStatus) {
        TimerStatus = timerStatus;
    }

    public List<StudentMcqTestChap> getChapterArray() {
        return chapterArray;
    }

    public void setChapterArray(List<StudentMcqTestChap> chapterArray) {
        this.chapterArray = chapterArray;
    }

    public List<StudentMcqTestDtl> getDetailArray() {
        return detailArray;
    }

    public void setDetailArray(List<StudentMcqTestDtl> detailArray) {
        this.detailArray = detailArray;
    }

    public List<StudentMcqTestLevel> getLevelArray() {
        return levelArray;
    }

    public void setLevelArray(List<StudentMcqTestLevel> levelArray) {
        this.levelArray = levelArray;
    }
}
