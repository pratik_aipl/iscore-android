package com.parshvaa.isccore.custominterface;

import com.parshvaa.isccore.activity.library.PersonalFiles;

public interface PersonalFolderCallback {

        void onItemClick(PersonalFiles file);

    }
