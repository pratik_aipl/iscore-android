package com.parshvaa.isccore.db;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Sagar Sojitra on 1/30/2017.
 */

public class DBTables {
    public HashMap<String, String> tables = new HashMap<>();
    public DBConstant dbConstant;

    public DBTables() {
        dbConstant = new DBConstant();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    public void createAll(MyDBManager db) {
        //createStudentTable(db);
        boards(db);
        boards_paper(db);
        boards_paper_data(db);
        board_paper_files(db);
        chapters(db);
        //ci_sessions(db);
        //classes(db);
        examtypes(db);
        examtype_subject(db);
        //login_attempts(db);
        masterquestion(db);
        mcqoption(db);
        mcqquestion(db);
        mediums(db);
        question_types(db);
        standards(db);
        subjects(db);
        //users(db);
        //user_autologin(db);
        //user_profiles(db);
        temp_mcq_test_hdr(db);
        temp_mcq_test_dtl(db);
        temp_mcq_test_chapter(db);
        student_mcq_test_hdr(db);
        student_mcq_test_dtl(db);
        student_mcq_test_chapter(db);
        paper_type(db);
        exam_type_pattern(db);
        exam_type_pattern_detail(db);
        student_question_paper(db);
        student_question_paper_detail(db);
        student_question_paper_chapter(db);
        board_paper_download(db);
        student_set_paper_question_type(db);
        student_set_paper_detail(db);
        notification(db);
        class_evaluater_mcq_test_temp(db);
        class_evaluater_cct_count(db);
        updated_masterquetion(db);
        student_mcq_test_level(db);
        temp_mcq_test_level(db);
        student_not_appeared_question(db);
        student_incorrect_question(db);
        class_question_paper_sub_question(db);
        sub_question_types(db);
        master_passage_sub_question(db);
        passage_sub_question_type(db);
        zooki(db);
    }


    public void createStudentTable(MyDBManager db) {
        tables.clear();
        tables.put("id", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("name", "String");
        db.createTable(tables, DBConstant.STUDENT_TABLE);
    }

    public void boards(MyDBManager db) {
        tables.clear();
        tables.put("BoardID", "integer  PRIMARY KEY AUTOINCREMENT");
        tables.put("BoardName", "text");
        tables.put("BoardFullName", "text");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.boards);
    }

    public void boards_paper(MyDBManager db) {
        tables.clear();
        tables.put("BoardPaperID", "integer (11)");
        tables.put("BoardPaperName", "text");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.boards_paper);
    }

    public void boards_paper_data(MyDBManager db) {
        tables.clear();
        tables.put("BPDataID", "integer (11)");
        tables.put("QuestionID", "integer (11)");
        tables.put("BoardPaperID", "integer (11)");

        db.createTable(tables, DBConstant.boards_paper_data);
    }

    public void board_paper_files(MyDBManager db) {
        tables.clear();
        tables.put("BoardPaperFileID", "integer (11)");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("StandardID", "integer (11)");
        tables.put("SubjectID", "integer (11)");
        tables.put("BoardPaperID", "integer (11)");
        tables.put("QFile", "varchar(255)");
        tables.put("QAFile", "varchar(255)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");
        tables.put("Rootpath", "varchar(255)");

        db.createTable(tables, DBConstant.board_paper_files);
    }

    public void chapters(MyDBManager db) {
        tables.clear();
        tables.put("ChapterID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("ChapterNumber", "integer(11)");
        tables.put("ParentChapterID", "integer(11)");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("StandardID", "integer (11)");
        tables.put("SubjectID", "integer (11)");
        tables.put("ChapterName", "text");
        tables.put("isMCQ", "integer(11)");
        tables.put("DisplayOrder", "integer (11)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");
        tables.put("isGeneratePaper", "integer (11)");
        tables.put("isDemo", "varchar (11)");
        db.createTable(tables, DBConstant.chapters);
    }

    public void ci_sessions(MyDBManager db) {
        tables.clear();
        tables.put("sessionID", "varchar(40)");
        tables.put("ip_address", "varchar(16)");
        tables.put("user_agent", "varchar(150)");
        tables.put("last_activity", "integer (10)");
        tables.put("user_data", "text");

        db.createTable(tables, DBConstant.ci_sessions);
    }

    public void classes(MyDBManager db) {
        tables.clear();
        tables.put("ClassID", "integer (11)");
        tables.put("ClassName", "varchar(250)");
        tables.put("Address1", "text");
        tables.put("Area", "varchar(200)");
        tables.put("Zipcode", "varchar(10)");
        tables.put("City", "varchar(50)");
        tables.put("State", "varchar(50)");
        tables.put("Country", "varchar(50)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.classes);
    }

    public void examtypes(MyDBManager db) {
        tables.clear();
        tables.put("ExamTypeID", "integer (11)");
        tables.put("ExamTypeName", "text");
        tables.put("PaperType", "varchar (255) NOT NULL");
        tables.put("BoardID", "varchar (16)");
        tables.put("MediumID", "varchar (16)");
        tables.put("StandardID", "varchar (16)");
        tables.put("TotalMarks", "decimal (5,2)");
        tables.put("Duration", "text");
        tables.put("Instruction", "text");
        tables.put("CreatedBy", "varchar (255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar (200)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.examtypes);
    }

    public void examtype_subject(MyDBManager db) {
        tables.clear();
        tables.put("ExamTypeSubjectID", "integer (11)");
        tables.put("ExamTypeID", "text");
        tables.put("SubjectID", "varchar(255)");

        db.createTable(tables, DBConstant.examtype_subject);
    }

    public void login_attempts(MyDBManager db) {
        tables.clear();
        tables.put("id", "integer (11)");
        tables.put("ip_address", "varchar(40)");
        tables.put("login", "varchar(50)");
        tables.put("time", "timestamp");

        db.createTable(tables, DBConstant.login_attempts);
    }

    public void masterquestion(MyDBManager db) {
        tables.clear();
        tables.put("MQuestionID", "INTEGER");
        tables.put("Question", "longtext");
        tables.put("ImageURL", "varchar(100)");
        tables.put("Answer", "longtext");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("ClassID", "integer (11)");
        tables.put("StandardID", "integer (11)");
        tables.put("subjectID", "integer (11)");
        tables.put("ChapterID", "integer (11)");
        tables.put("QuestionTypeID", "integer (11)");
        tables.put("isBoard", "tinyint(1)");
        tables.put("ExerciseNo", "varchar (255)");
        tables.put("QuestionNo", "varchar (255)");
        tables.put("Question_Year", "longtext (255)");
        tables.put("is_demo", "varchar (11)");
        tables.put("CreatedBy", "varchar (255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar (255)");
        tables.put("ModifiedOn", "timestamp");
        db.createTable(tables, DBConstant.masterquestion);
    }

    public void mcqoption(MyDBManager db) {
        tables.clear();
        tables.put("MCQOPtionID", "integer (11)");
        tables.put("MCQQuestionID", "varchar(100)");
        tables.put("Options", "text");
        tables.put("ImageURL", "varchar(100)");
        tables.put("OptionNo", "integer (11)");
        tables.put("isCorrect", "tinyint(1)");
        tables.put("CreatedBy", "varchar(100)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");
        db.createTable(tables, DBConstant.mcqoption);
    }

    public void mcqquestion(MyDBManager db) {
        tables.clear();
        tables.put("MCQQuestionID", "integer (11)");
        tables.put("Question", "text");
        tables.put("ImageURL", "varchar(100)");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("ClassID", "integer (11)");
        tables.put("StandardID", "integer (11)");
        tables.put("SubjectID", "integer (11)");
        tables.put("ChapterID", "integer (11)");
        tables.put("Solution", "longtext");
        tables.put("QuestionTypeID", "integer (11)");
        tables.put("QuestionLevelID", "integer (11)");
        tables.put("Duration", "integer (11)");
        tables.put("CreatedBy", "varchar(100)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");
        db.createTable(tables, DBConstant.mcqquestion);
    }

    public void mediums(MyDBManager db) {
        tables.clear();
        tables.put("MediumID", "integer (11)");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumName", "text");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.mediums);
    }

    public void question_types(MyDBManager db) {
        tables.clear();
        tables.put("QuestionTypeID", "integer (11)");
        tables.put("QuestionType", "varchar(255)");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("ClassID", "integer (11)");
        tables.put("StandardID", "integer (11)");
        tables.put("SubjectID", "integer (11)");
        tables.put("Marks", "decimal(5,2)");
        tables.put("Type", "varchar(100)");
        tables.put("RevisionNo", "varchar(5)");
        tables.put("CreatedBy", "varchar(100)");
        tables.put("CreatedOn", "integer (11)");
        tables.put("ModifiedBy", "varchar(100)");
        tables.put("ModifiedOn", "timestamp");
        tables.put("isMTC", "integer (11)");
        tables.put("isPassage", "integer (11)");


        db.createTable(tables, DBConstant.question_types);
    }

    public void standards(MyDBManager db) {
        tables.clear();
        tables.put("StandardID", "integer (11)");
        tables.put("StandardName", "text");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.standards);
    }

    public void subjects(MyDBManager db) {
        tables.clear();
        tables.put("SubjectID", "integer (11)");
        tables.put("SubjectName", "text");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("StandardID", "integer (11)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("SubjectIcon", "varchar(128)");
        tables.put("ModifiedOn", "timestamp");
        tables.put("Price", "integer (11)");
        tables.put("SubjectOrder", "integer (11)");
        tables.put("isMCQ", "integer (11)");
        tables.put("isGeneratePaper", "integer (11)");
        tables.put("IsMapped", "integer (11)");
        tables.put("MappedSubjectID", "integer (11)");

        db.createTable(tables, DBConstant.subjects);
    }

    public void users(MyDBManager db) {
        tables.clear();
        tables.put("id", "integer (11)");
        tables.put("username", "varchar(50)");
        tables.put("password", "varchar(255)");
        tables.put("email", "varchar(100)");
        tables.put("activated", "tinyint(1)");
        tables.put("banned", "tinyint(1)");
        tables.put("ben_reason", "varchar(255)");
        tables.put("new_password_key", "varchar(50)");
        tables.put("new_password_requested", "datetime");
        tables.put("new_email", "varchar(100)");
        tables.put("new_email_key", "varchar(50)");
        tables.put("last_ip", "varchar(40)");
        tables.put("last_login", "datetime");
        tables.put("crested", "datetime");
        tables.put("modified", "timestamp");


        db.createTable(tables, DBConstant.users);
    }

    public void user_autologin(MyDBManager db) {
        tables.clear();
        tables.put("key_id", "char(32)");
        tables.put("user_id ", "integer (11)");
        tables.put("user_agent", "varchar(150)");
        tables.put("last_ip", "varchar(40)");
        tables.put("last_login", "timestamp");

        db.createTable(tables, DBConstant.user_autologin);
    }

    public void user_profiles(MyDBManager db) {
        tables.clear();
        tables.put("id", "integer (11)");
        tables.put("user_id ", "integer (11)");
        tables.put("country", "varchar(20)");
        tables.put("website", "varchar(255)");

        db.createTable(tables, DBConstant.user_profiles);
    }

    public void temp_mcq_test_hdr(MyDBManager db) {
        tables.clear();
        tables.put("TempMCQTestHDRID ", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentID", "integer (11)");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("SubjectID", "integer (11)");
        tables.put("AddType", "CHAR(1) NOT NULL DEFAULT ('A')");
        tables.put("Timeleft", "varchar (255)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("TimerStatus", "integer (11)");
        tables.put("TestTime", "varchar(255)");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.temp_mcq_test_hdr);
    }

    public void temp_mcq_test_level(MyDBManager db) {
        tables.clear();
        tables.put("StudentMCQTestLevelID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentMCQTestHDRID", "varchar (16)");
        tables.put("LevelID", "varchar(255)");

        db.createTable(tables, DBConstant.temp_mcq_test_level);
    }

    public void zooki(MyDBManager db) {
        tables.clear();
        tables.put("ZookiID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("Title", "varchar (255)");
        tables.put("Desc", "longtext");
        tables.put("image", "varchar (255)");
        tables.put("CreatedOn", "varchar (255)");
        db.createTable(tables, DBConstant.zooki);
    }

    public void temp_mcq_test_dtl(MyDBManager db) {
        tables.clear();
        tables.put("TempMCQTestDTLID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("TempMCQTestHDRID ", "varchar (16)");
        tables.put("QuestionID", "varchar (16)");
        tables.put("AnswerID", "varchar (16)");
        tables.put("IsAttempt ", "varchar (16)");
        tables.put("srNo", "varchar (16)");
        tables.put("CreatedBy", "varchar (16)");
        tables.put("QuestionTime", "varchar(255)");
        tables.put("CreatedOn ", "datetime");
        tables.put("ModifiedBy", "varchar (16)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.temp_mcq_test_dtl);
    }

    public void temp_mcq_test_chapter(MyDBManager db) {
        tables.clear();
        tables.put("TempMCQTestChapterID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("TempMCQTestHDRID ", "integer (11)");
        tables.put("ChapterID", "integer (11)");

        db.createTable(tables, DBConstant.temp_mcq_test_chapter);
    }

    public void student_mcq_test_hdr(MyDBManager db) {
        tables.clear();
        tables.put("StudentMCQTestHDRID ", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentID", "integer (11)");
        tables.put("BoardID", "integer (11)");
        tables.put("MediumID", "integer (11)");
        tables.put("SubjectID", "integer (11)");
        tables.put("Timeleft", "varchar (255)");
        tables.put("AddType", "CHAR(1) NOT NULL DEFAULT ('A')");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "datetime");
        tables.put("TimerStatus", "integer (11)");
        tables.put("TestTime", "varchar(255)");

        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "TIMESTAMP");


        db.createTable(tables, DBConstant.student_mcq_test_hdr);
    }

    public void student_mcq_test_dtl(MyDBManager db) {
        tables.clear();
        tables.put("StudentMCQTestDTLID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentMCQTestHDRID  ", "integer (11)");
        tables.put("QuestionID", "integer (11)");
        tables.put("AnswerID", "integer (11)");
        tables.put("IsAttempt ", "integer (11)");
        tables.put("srNo", "integer (11)");
        tables.put("QuestionTime", "varchar(255)");
        tables.put("CreatedBy", "varchar (255)");
        tables.put("CreatedOn ", "datetime");
        tables.put("ModifiedBy", "varchar (255)");
        tables.put("ModifiedOn", "default current_timestamp");

        db.createTable(tables, DBConstant.student_mcq_test_dtl);
    }

    public void student_mcq_test_chapter(MyDBManager db) {
        tables.clear();
        tables.put("StudentMCQTestChapterID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentMCQTestHDRID ", "integer (11)");
        tables.put("ChapterID", "integer (11)");

        db.createTable(tables, DBConstant.student_mcq_test_chapter);
    }

    public void student_mcq_test_level(MyDBManager db) {
        tables.clear();
        tables.put("StudentMCQTestLevelID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentMCQTestHDRID", "varchar (16)");
        tables.put("LevelID", "varchar(255)");

        db.createTable(tables, DBConstant.student_mcq_test_level);
    }

    public void paper_type(MyDBManager db) {
        tables.clear();
        tables.put("PaperTypeID", "varchar (15)");
        tables.put("PaperTypeName ", "varchar (225)");

        db.createTable(tables, DBConstant.paper_type);
    }


    public void exam_type_pattern(MyDBManager db) {
        tables.clear();
        tables.put("ExamTypePatternID", "varchar (15)");
        tables.put("BoardID", "varchar (15)");
        tables.put("MediumID", "varchar (15)");
        tables.put("StandardID", "varchar (15)");
        tables.put("SubjectID", "varchar (15)");
        tables.put("ExamTypeID", "varchar (15)");
        tables.put("TotalMarks", "decimal(5,2)");
        tables.put("Duration", "text");
        tables.put("Instruction ", "text");
        tables.put("PatternNo ", "varchar (15)");
        tables.put("CreatedBy ", "varchar (225)");
        tables.put("CreatedOn ", "varchar (225)");
        tables.put("ModifiedBy ", "varchar (225)");
        tables.put("ModifiedOn ", "varchar (225)");

        db.createTable(tables, DBConstant.exam_type_pattern);
    }

    public void exam_type_pattern_detail(MyDBManager db) {
        tables.clear();
        tables.put("ExamTypePatternDetailID", "varchar (15)");
        tables.put("ExamTypePatternID", "integer (11)");
        tables.put("ExamTypeID", "integer (11)");
        tables.put("QuestionTypeID", "integer (11)");
        tables.put("QuestionNO", "text");
        tables.put("SubQuestionNO", "text");
        tables.put("QuestionTypeText", "text");
        tables.put("QuestionMarks", "varchar (255)");
        tables.put("NoOfQuestion", "integer (11)");
        tables.put("DisplayOrder", "integer (11)");
        tables.put("isQuestion", "integer (11)");
        tables.put("isQuestionVisible", "integer (11)");
        tables.put("ChapterID ", "integer (15)");
        tables.put("PageNo ", "integer (15)");
        tables.put("CreatedBy ", "varchar (225)");
        tables.put("CreatedOn ", "datetime");
        tables.put("ModifiedBy ", "varchar (225)");
        tables.put("ModifiedOn ", "timestamp");

        db.createTable(tables, DBConstant.exam_type_pattern_detail);
    }

    public void student_question_paper(MyDBManager db) {
        tables.clear();

        tables.put("StudentQuestionPaperID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("ExamTypePatternID", "varchar (32)");
        tables.put("StudentID", "varchar (32)");
        tables.put("PaperTypeID", "varchar (32)");
        tables.put("SubjectID", "varchar (32)");
        tables.put("TotalMarks", "varchar (32)");
        tables.put("Duration", "varchar (32)");
        tables.put("CreatedBy", "varchar (32)");
        tables.put("CreatedOn", "datetime");
        tables.put("ModifiedBy", "varchar (32)");
        tables.put("ModifiedOn", "timestamp");

        db.createTable(tables, DBConstant.student_question_paper);
    }

    public void student_question_paper_chapter(MyDBManager db) {
        tables.clear();

        tables.put("StudentQuestionPaperChapterID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentQuestionPaperID", "integer (11)");
        tables.put("ChapterID", "integer (11)");

        db.createTable(tables, DBConstant.student_question_paper_chapter);
    }

    public void student_question_paper_detail(MyDBManager db) {
        tables.clear();

        tables.put("StudentQuestionPaperDetailID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentQuestionPaperID", "integer (11)");
        tables.put("ExamTypePatternDetailID", "integer (11)");
        tables.put("MasterorCustomized", "integer (11)");
        tables.put("MQuestionID", "integer(11)");
        tables.put("CQuestionID", "integer (11)");

        db.createTable(tables, DBConstant.student_question_paper_detail);
    }

    public void board_paper_download(MyDBManager db) {
        tables.clear();

        tables.put("BoardPaperDownloadID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentID", "integer (11)");
        tables.put("SubjectID", "integer (11)");
        tables.put("BoardPaperID", "integer (11)");
        tables.put("BoardPaperAnswers", "integer (11)");
        tables.put("CreatedOn", "DATETIME");

        db.createTable(tables, DBConstant.board_paper_download);
    }

    public void student_set_paper_question_type(MyDBManager db) {
        tables.clear();

        tables.put("StudentSetPaperQuestionTypeID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentQuestionPaperID", "varchar (11)");
        tables.put("QuestionTypeID", "varchar (11)");
        tables.put("TotalAsk", "varchar (11)");
        tables.put("ToAnswer", "varchar (11)");
        tables.put("TotalMark", "varchar (11)");

        db.createTable(tables, DBConstant.student_set_paper_question_type);
    }

    public void student_set_paper_detail(MyDBManager db) {
        tables.clear();

        tables.put("StudentSetPaperDetailID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("StudentSetPaperQuestionTypeID", "integer (11)");
        tables.put("MQuestionID", "varchar(100)");

        db.createTable(tables, DBConstant.student_set_paper_detail);
    }

    public void notification(MyDBManager db) {
        tables.clear();
        tables.put("NID", "integer PRIMARY KEY AUTOINCREMENT");
            tables.put("OneSignalID", "integer");
        tables.put("notification_type_id", "varchar (225)");
        tables.put("android_notification_id", "varchar (225)");
        tables.put("Title", "varchar (225)");
        tables.put("Message", "varchar (225)");
        tables.put("CreatedOn", "varchar (225)");
        tables.put("ModuleType", "varchar (225)");
        tables.put("isOpen", "varchar (225)");
        tables.put("isTypeOpen", "varchar (225)");
        tables.put("CctSubmit", "varchar (225)");
        tables.put("paper_type", "varchar (225)");
        tables.put("SubjectName", "varchar (225)");
        db.createTable(tables, DBConstant.notification);

    }

    public void class_evaluater_mcq_test_temp(MyDBManager db) {
        tables.clear();
        tables.put("ClassMCQTestDTLID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("ClassMCQTestHDRID  ", "varchar (32)");
        tables.put("QuestionID", "varchar (32)");
        tables.put("AnswerID", "varchar (32)");
        tables.put("IsAttempt", "varchar (32)");
        tables.put("MasterorCustomized", "varchar (255)");
        tables.put("Question", "text");
        db.createTable(tables, DBConstant.class_evaluater_mcq_test_temp);
    }

    public void class_evaluater_cct_count(MyDBManager db) {
        tables.clear();
        tables.put("ClassCctID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("SubjectID", "varchar (32)");
        tables.put("SubjectName", "varchar (32)");
        tables.put("Total", "varchar (32)");
        tables.put("Acuuracy", "varchar (32)");
        db.createTable(tables, DBConstant.class_evaluater_cct_count);
    }


    public void updated_masterquetion(MyDBManager db) {
        tables.clear();
        tables.put("MQuestionID", "integer PRIMARY KEY");
        tables.put("old_question_id", "integer (11)");

        db.createTable(tables, DBConstant.updated_masterquetion);
    }

    public void student_incorrect_question(MyDBManager db) {
        tables.clear();
        tables.put("StudentID", "integer (11)");
        tables.put("QuestionID ", "integer (11)");
        tables.put("CreatedOn", "datetime");

        db.createTable(tables, DBConstant.student_incorrect_question);

    }

    public void student_not_appeared_question(MyDBManager db) {
        tables.clear();
        tables.put("StudentID", "integer (11)");
        tables.put("QuestionID ", "integer (11)");
        tables.put("CreatedOn", "datetime");

        db.createTable(tables, DBConstant.student_not_appeared_question);

    }

    public void class_question_paper_sub_question(MyDBManager db) {
        tables.clear();
        tables.put("ClassQuestionPaperSubQuestionID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("PaperID ", "integer (11)");
        tables.put("DetailID", "integer (11)");
        tables.put("MPSQID", "integer (11)");

        db.createTable(tables, DBConstant.class_question_paper_sub_question);

    }

    public void passage_sub_question_type(MyDBManager db) {
        tables.clear();
        tables.put("PassageSubQuestionTypeID", "integer (11)");
        tables.put("QuestionTypeID ", "integer (11)");
        tables.put("Label", "varchar(500)");
        tables.put("Mark", "varchar(50)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "varchar(255)");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "varchar(255)");
        db.createTable(tables, DBConstant.passage_sub_question_type);

    }

    public void master_passage_sub_question(MyDBManager db) {
        tables.clear();
        tables.put("MPSQID", "integer (11)");
        tables.put("MQuestionID", "integer (11)");
        tables.put("BetaMQuestionID", "integer (11)");
        tables.put("SubQuestionTypeID ", "integer (11)");
        tables.put("PassageSubQuestionTypeID", "integer (11)");
        tables.put("Question", "longtext");
        tables.put("Answer", "longtext");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "varchar(255)");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "varchar(255)");
        db.createTable(tables, DBConstant.master_passage_sub_question);

    }

    public void sub_question_types(MyDBManager db) {
        tables.clear();


        tables.put("SubQuestionTypeID", "integer (11)");
        tables.put("QuestionType ", "integer (11)");
        tables.put("BoardID", "varchar(500)");
        tables.put("MediumID", "varchar(50)");
        tables.put("ClassID", "varchar(500)");
        tables.put("StandardID", "varchar(50)");
        tables.put("SubjectID", "varchar(50)");
        tables.put("Marks", "decimal(5,2)");
        tables.put("CreatedBy", "varchar(255)");
        tables.put("CreatedOn", "varchar(255)");
        tables.put("ModifiedBy", "varchar(255)");
        tables.put("ModifiedOn", "varchar(255)");
        db.createTable(tables, DBConstant.sub_question_types);

    }

}
