package com.parshvaa.isccore.model;

/**
 * Created by empiere-vaibhav on 1/8/2018.
 */

public class SetPaperType {
    public String totalQue;

    public SetPaperType() {

    }

    public String getTotalQue() {
        return totalQue;
    }

    public void setTotalQue(String totalQue) {
        this.totalQue = totalQue;
    }

    public SetPaperType(String totalQue) {
        this.totalQue = totalQue;
    }

}
