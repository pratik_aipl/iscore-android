package com.parshvaa.isccore.activity.weeklydise;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ISccoreWebView;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class WeeklyReportPreviewActivity extends BaseActivity {
    public ImageView img_back;
    public TextView tv_title;
    public ISccoreWebView pdfview;
    public String start_date = "", end_date = "", old_end_date = "", old_start_date = "";
    public int mcq_accu = 0, paper_accu = 0;
    public int past_mcq_accu = 0, past_paper_accu = 0;
    public String testHeaderIDS = "";

    public WeeklyReportPreviewActivity instance;
    public ArrayList<WeeklyDise> weeklyDiseArray = new ArrayList<>();
    public ArrayList<WeeklyDise> weeklyDiseArray1 = new ArrayList<>();

    public static MyDBManager mDb;
    public CursorParserUniversal cParse;

    int diff_mcq_accu = 0, diff_paper_accu = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_weekly_report_preview);
        Utils.logUser();


        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        cParse = new CursorParserUniversal();

        instance = this;
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Weekly Digest");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        start_date = getIntent().getStringExtra("start_date");
        end_date = getIntent().getStringExtra("end_date");

        old_end_date = getIntent().getStringExtra("old_end_date");
        old_start_date = getIntent().getStringExtra("old_start_date");
        Utils.Log("old_end_date", "==>" + old_end_date);
        Utils.Log("old_start_date", "==>" + old_start_date);

        pdfview = findViewById(R.id.pdfView);
        pdfview.getSettings().setJavaScriptEnabled(true);
        pdfview.getSettings().setAllowFileAccess(true);
        pdfview.getSettings().setDomStorageEnabled(true);

        try {
            weeklyDiseArray.clear();
            weeklyDiseArray = (ArrayList) new CustomDatabaseQuery(WeeklyReportPreviewActivity.this, new WeeklyDise())
                    .execute(new String[]{DBNewQuery.get_mcq_subject_accuracy_by_student(start_date, end_date)}).get();
            weeklyDiseArray1.clear();
            weeklyDiseArray1 = (ArrayList) new CustomDatabaseQuery(WeeklyReportPreviewActivity.this, new WeeklyDise())
                    .execute(new String[]{DBNewQuery.get_mcq_subject_accuracy_by_student(old_start_date, old_end_date)}).get();

            //  Utils.Log("Weekly Array", "==>" + weeklyDiseArray.size());
            new getRecorrd(instance).execute();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class getRecorrd extends AsyncTask<String, String, String> {
        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;

        private getRecorrd(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {

            testHeaderIDS = getTestHeaderID();
            int totalAttQue = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(testHeaderIDS));
            int totalRightAns = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(testHeaderIDS));

            App.mcqTotalAccuracy = 0;
            App.mcqTotalAccuracy = Integer.parseInt(Utils.getAccuraccy(totalAttQue, totalRightAns));

            Cursor c = mDb.getAllRows(DBNewQuery.get_total_mcq_test_accuracy_by_student(start_date, end_date));

            if (c != null && c.moveToFirst()) {
                mcq_accu = c.getInt(0);
                c.close();
            }
            c = mDb.getAllRows(DBNewQuery.get_paper_test_details(start_date, end_date));

            if (c != null && c.moveToFirst()) {
                paper_accu = c.getInt(0);
                c.close();
            }
            c = mDb.getAllRows(DBNewQuery.get_total_mcq_test_accuracy_by_student(old_start_date, old_end_date));

            if (c != null && c.moveToFirst()) {
                past_mcq_accu = c.getInt(0);
                c.close();
            }
            c = mDb.getAllRows(DBNewQuery.get_paper_test_details(old_start_date, old_end_date));

            if (c != null && c.moveToFirst()) {
                past_paper_accu = c.getInt(0);
                c.close();
            }
            return "";
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showProgress(false, true);
            Utils.Log("ACcu", "==>" + paper_accu);
            diff_mcq_accu = (mcq_accu - past_mcq_accu);
            diff_paper_accu = (paper_accu - past_paper_accu);
            pdfview.loadHtmlFromLocal(getWeeklyReportHtml());
        }
    }
    public String getTestHeaderID() {
        String IDS = "";
        StudentMcqTestHdr stdntHdr;
        Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllRowQuery("student_mcq_test_hdr"));
        if (MCqHdr != null && MCqHdr.moveToFirst()) {
            do {
                stdntHdr = (StudentMcqTestHdr) cParse.parseCursor(MCqHdr, new StudentMcqTestHdr());
                IDS += stdntHdr.getStudentMCQTestHDRID() + ",";
            } while (MCqHdr.moveToNext());
            MCqHdr.close();
        }

        return Utils.removeLastComma(IDS);
    }

    public String getWeeklyReportHtml() {
        int current_sub_mcq_accu = 0;
        int past_sub_mcq_accu = 0;
        int diff_sub_mcq_accu = 0;

        int current_sub_paper_accu = 0;
        int past_sub_paper_accu = 0;
        int diff_sub_paper_accu = 0;

        String HTML = "";
        HTML = "<!DOCTYPE HTML>\n" +
                "<html>\n" +
                "<head>\n" +
                "<meta charset=\"utf-8\">\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "<title> Home </title>  \n" +
                "<body>\n" +
                "\n" +
                "\n" +
                "<table style=\"border:1px solid #f1f1f1; max-width:600px; margin:0 auto; width:100%; font-family: arial;\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "  <tr>\n" +
                "    <td colspan=\"2\" style=\"text-align:center; padding:10px;\">\n" +
                "      <h2><span style=\"color: #f47d35; font-weight: bold;\">" + mySharedPref.getClassName() + "</span></h2>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td colspan=\"2\" style=\"background:#575757; text-align:center; color:#fff;  padding:12px 10px; font-size:24px; font-weight: bold; text-transform:uppercase\"> WEEKLY DIGEST REPORT \n" +
                "    </td>\n" +
                "  </tr>\n" +
                "\n" +
                "\n" +
                "  <tr>\n" +
                "    <td style=\"text-align:left;padding:12px 10px; width: 50%\">\n" +
                "      <label style=\"color: #232323; font-size: 15px;\"><span style=\"color: #f47d35; font-weight: bold;\">" + MainUser.getFirstName() + " " + MainUser.getLastName() + "</span></label>\n" +
                "\n" +
                "    </td>\n" +
                "    <td style=\"text-align:right;padding:12px 10px;width: 50%\">\n" +
                "        <label style=\"color: #232323; font-size: 15px;\">From : <span style=\"color: #f47d35; font-weight: bold;\"> "+start_date+"</span></label>\n" +
                "         \n" +
                "           <br/>\n" +
                "             <label style=\"color: #232323; font-size: 15px;\">To : <span style=\"color: #f47d35; font-weight: bold;\"> " + end_date + "</span></label>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "\n" +
                "  <tr>\n" +
                "    <td style=\"text-align:center;padding:15px; width: 50%; background-color: #f1583b;\">\n" +
                "      <label style=\"color: #fff; font-size: 15px;\">Your Set Goal </label><br/>\n" +
                "      <span style=\"color: #fff;font-size: 33px;\">" + mySharedPref.getTarget() + "%</span>\n" +
                "\n" +
                "    </td>\n" +
                "    <td style=\"text-align:center;padding:15px; width: 50%; background-color: #1c4d7f\">\n" +
                "          <label style=\"color: #fff; font-size: 15px;\">Your Accuracy  </label><br/>\n" +
                "          <span style=\"color: #fff; font-size: 33px;\">"+App.mcqTotalAccuracy+"%<span style=\"font-size: 15px\"> \n" +
                "    </td>\n" +
                "  </tr>\n" +
                "\n" +
                "\n" +
                "  <tr>\n" +
                "    <td colspan=\"2\" style=\"text-align:left;padding:5px;\"> \n" +
                "        <table style=\"border:1px solid #eee; width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "          <tr>\n" +
                "              <td style=\"width:33.3%; text-align: center; border-right:1px solid #eee; padding: 20px 0\">\n" +
                "                  <label style=\"color: #252525; font-size: 15px;\">mcq </label><br/>\n" +
                "                  <span style=\"color: #252525; font-size: 30px;font-weight: bold;\">" + mcq_accu + "%" + "<span style=\"font-size: 15px\">";
        if (mcq_accu > 0) {
            HTML += "<img src=\"file:///android_asset/img/up.png\"/> " + diff_mcq_accu + "%" + "</span></span> \n";

        }else if (mcq_accu == 0) {
            HTML += "<img src=\"file:///android_asset/img/nutal.png\"/> " + diff_mcq_accu + "%" + "</span></span> \n";

        } else {
            HTML += "<img src=\"file:///android_asset/img/down.png\"/> -" + diff_mcq_accu + "%" + "</span></span> \n";

        }
        HTML +=
                "               </td>\n" +
                        "\n" +
                        "                <td style=\"width:33.3%;text-align: center;padding: 20px 0\">\n" +
                        "                  <label style=\"color: #252525; font-size: 15px;\">PRACTISE PAPER </label><br/>\n" +
                        "                  <span style=\"color: #252525; font-size: 30px;font-weight: bold;\">" + paper_accu + "%" + "<span style=\"font-size: 15px;font-weight: normal;\">";
        if (paper_accu > 0) {
            HTML += " <img src=\"file:///android_asset/img/up.png\"/> " + diff_paper_accu + "%" + "</span></span> \n";
        }else if (paper_accu == 0) {
            HTML += " <img src=\"file:///android_asset/img/nutal.png\"/> " + diff_paper_accu + "%" + "</span></span> \n";
        }else {
            HTML += "<img src=\"file:///android_asset/img/down.png\"/> " + diff_paper_accu + "%" + "</span></span> \n";
        }
        HTML +=
                "               </td>\n" +
                        "          </tr>\n" +
                        "        </table>\n" +
                        "       \n" +
                        "    </td>\n" +
                        "   </tr>\n" +
                        "\n" +
                        "\n" +
                        "   <tr>\n" +
                        "    <td colspan=\"2\" style=\"text-align:center;padding:10px;color: #252525; font-weight: bold; text-transform: uppercase;\"> \n" +
                        "        Subject wise Weekly Report \n" +
                        "    </td>\n" +
                        "  </tr>\n" +
                        "\n" +
                        "\n" +
                        "  <tr>\n" +
                        "    <td colspan=\"2\" style=\"text-align:left;padding:5px;\"> \n" +
                        "        <table style=\"border:1px solid #f1f1f1; width: 100%; font-size: 15px; background:#eee\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                        "          <tr>\n" +
                        "              <th style=\"width:30%; text-align: left; color: #fff; background:#1c4d7f; padding: 10px;\"> SUBJECTS </th>\n" +
                        "              <th style=\"width:23.3%; text-align: center; color: #fff; background:#1c4d7f; padding: 10px;\"> mcq </th>\n" +
                        "              <th style=\"width:23.3%; text-align: center;  color: #fff;background:#1c4d7f; padding: 10px;\"> PRACTISE </th>\n" +
                        "          </tr>\n";

        for (int i = 0; i < weeklyDiseArray.size(); i++) {
            try {
                if (weeklyDiseArray.get(i).getAccuracy() == null || weeklyDiseArray.get(i).getAccuracy().equals("") ||
                        weeklyDiseArray.get(i).getTotalPaper() == null || weeklyDiseArray.get(i).getAccuracy().equals("")) {
                    current_sub_mcq_accu = 0;
                    current_sub_paper_accu = 0;
                } else {
                    current_sub_mcq_accu = Integer.parseInt(weeklyDiseArray.get(i).Accuracy);
                    current_sub_paper_accu = Integer.parseInt(weeklyDiseArray.get(i).getTotalPaper());
                }
                if (weeklyDiseArray1.get(i).getAccuracy() == null || weeklyDiseArray1.get(i).getAccuracy().equals("")
                        || weeklyDiseArray1.get(i).getTotalPaper() == null || weeklyDiseArray1.get(i).getTotalPaper().equals("")) {
                    past_sub_mcq_accu = 0;
                    past_sub_paper_accu = 0;
                } else {
                    past_sub_mcq_accu = Integer.parseInt(weeklyDiseArray1.get(i).Accuracy);
                    past_sub_paper_accu = Integer.parseInt(weeklyDiseArray1.get(i).TotalPaper);

                }
                diff_sub_mcq_accu = current_sub_mcq_accu - past_sub_mcq_accu;
                diff_sub_paper_accu = current_sub_paper_accu - past_sub_paper_accu;
            } catch (Exception e) {
                e.printStackTrace();
            }
            HTML += "          <tr>\n" +
                    "              <td style=\"text-align: left; color: #252525;  padding: 8px;\"> " + weeklyDiseArray.get(i).getSubjectName() + " </td>\n" +
                    "              <td style=\"text-align: center; color: #252525;  padding: 8px; font-weight: bold;\"> \n";
            if (weeklyDiseArray.get(i).getAccuracy() == null || weeklyDiseArray.get(i).getAccuracy().equals("")) {
                HTML += "                  " + 0 + "% <span style=\"font-size: 10px;font-weight: normal;\">";
                if (diff_sub_mcq_accu > 0) {
                    HTML += "<img src=\"file:///android_asset/img/up_s.png\"/> " + diff_sub_mcq_accu + "%</span> \n";

                } else  if (diff_sub_mcq_accu == 0) {
                    HTML += "<img src=\"file:///android_asset/img/nutal_s.png\"/> " + diff_sub_mcq_accu + "%</span> \n";

                } else {
                    HTML += "<img src=\"file:///android_asset/img/down_s.png\"/> " + diff_sub_mcq_accu + "%</span> \n";

                }
                HTML +=
                        "              </td>\n";

            } else {
                HTML += "                  " + weeklyDiseArray.get(i).getAccuracy() + "% <span style=\"font-size: 10px;font-weight: normal;\">";
                if (diff_sub_mcq_accu > 0) {
                    HTML += "<img src=\"file:///android_asset/img/up_s.png\"/> " + diff_sub_mcq_accu + "%</span> \n";

                }else if (diff_sub_mcq_accu == 0) {
                    HTML += "<img src=\"file:///android_asset/img/nutal_s.png\"/> " + diff_sub_mcq_accu + "%</span> \n";

                } else {
                    HTML += "<img src=\"file:///android_asset/img/down_s.png\"/> " + diff_sub_mcq_accu + "%</span> \n";

                }
                HTML +=
                        "              </td>\n";

            }
            HTML +=
                    "               <td style=\"text-align: center; color: #252525;  padding: 8px; font-weight: bold;\"> \n" +
                            "                  " + weeklyDiseArray.get(i).getTotalPaper() + "% <span style=\"font-size: 10px;font-weight: normal;\">";
            if (diff_sub_paper_accu > 0) {
                HTML += "<img src=\"file:///android_asset/img/up_s.png\"/> " + diff_sub_paper_accu + "%</span> \n";

            }else  if (diff_sub_paper_accu == 0) {
                HTML += "<img src=\"file:///android_asset/img/nutal_s.png\"/> " + diff_sub_paper_accu + "%</span> \n";

            }else {
                HTML += "<img src=\"file:///android_asset/img/down_s.png\"/> " + diff_sub_paper_accu + "%</span> \n";

            }
            HTML +=
                    "              </td> \n" +
                            "          </tr>\n";
        }
        HTML +=
                "        </table>\n" +
                        "       \n" +
                        "    </td>\n" +
                        "   </tr>\n" +
                        "\n" +
                        "\n" +
                        "  <tr>\n" +
                        "\n" +
                        "  </tr>\n" +
                        "</table>\n" +
                        "</body>\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "</html>\n";


        return HTML;
    }


}
