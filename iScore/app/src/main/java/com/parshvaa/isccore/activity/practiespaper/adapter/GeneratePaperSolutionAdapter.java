package com.parshvaa.isccore.activity.practiespaper.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.practiespaper.fragment.GeneratePaperSolutionFragment;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.SetPaperQuestionAndTypes;
import com.parshvaa.isccore.utils.ISccoreWebView;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;

import static androidx.recyclerview.widget.RecyclerView.ViewHolder;

/**
 * Created by empiere-vaibhav on 1/8/2018.
 */

public class GeneratePaperSolutionAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<SetPaperQuestionAndTypes> stringArrayListHashMap = new ArrayList<>();
    public Context context;
    int clickedPos = -1;
    private LayoutInflater mInflater;
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_FOOTER = 2;
    public RecyclerView recyclerView;

    public GeneratePaperSolutionAdapter(ArrayList<SetPaperQuestionAndTypes> moviesList, Context context, RecyclerView recyclerView) {
        this.stringArrayListHashMap = moviesList;
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.recyclerView = recyclerView;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            return new ViewHolderHeader(mInflater.inflate(R.layout.custom_generate_paper_raw, parent, false));
        } else if (viewType == VIEW_TYPE_FOOTER) {
            return new ViewHolderFooter(mInflater.inflate(R.layout.custom_generate_paper_footer, parent, false));
        } else {
            return new ViewHolderContent(mInflater.inflate(R.layout.custom_generate_paper_child_raw, parent, false));
        }
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return stringArrayListHashMap.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderHeader offerHolder = (ViewHolderHeader) holder;
                bindHeaderHolder(offerHolder, position);
                break;
            case 1:
                ViewHolderContent addrHolder = (ViewHolderContent) holder;
                bindContentHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView tv_question, tvQueNO, btn_solution, tv_que_type;
        public LinearLayout lin_solution;
        public ISccoreWebView qWebView, sWebView;

        public ViewHolderHeader(View v2) {
            super(v2);
            btn_solution = v2.findViewById(R.id.btn_solution);
            lin_solution = v2.findViewById(R.id.lin_solution);

            tv_question = v2.findViewById(R.id.tv_question);
            tv_que_type = v2.findViewById(R.id.tv_que_type);
            tvQueNO = v2.findViewById(R.id.tvQueNO);
            qWebView = v2.findViewById(R.id.qWebView);
            sWebView = v2.findViewById(R.id.sWebView);
        }

    }

    class ViewHolderContent extends RecyclerView.ViewHolder {
        TextView tv_question, tvQueNO, btn_solution;
        public LinearLayout lin_solution;
        public ISccoreWebView qWebView, sWebView;

        public ViewHolderContent(View v1) {
            super(v1);
            btn_solution = v1.findViewById(R.id.btn_solution);
            lin_solution = v1.findViewById(R.id.lin_solution);

            tv_question = v1.findViewById(R.id.tv_question);
            tvQueNO = v1.findViewById(R.id.tvQueNO);
            qWebView = v1.findViewById(R.id.qWebView);
            sWebView = v1.findViewById(R.id.sWebView);
        }
    }

    class ViewHolderFooter extends RecyclerView.ViewHolder {
        TextView tv_question, tvQueNO, btn_solution;
        public LinearLayout lin_solution;
        public ISccoreWebView qWebView, sWebView;

        public ViewHolderFooter(View v1) {
            super(v1);
            btn_solution = v1.findViewById(R.id.btn_solution);
            lin_solution = v1.findViewById(R.id.lin_solution);

            tv_question = v1.findViewById(R.id.tv_question);
            tvQueNO = v1.findViewById(R.id.tvQueNO);
            qWebView = v1.findViewById(R.id.qWebView);
            sWebView = v1.findViewById(R.id.sWebView);
        }
    }


    @SuppressLint("NewApi")
    public void bindHeaderHolder(final ViewHolderHeader headerHolder, final int pos) {
        SetPaperQuestionAndTypes types = getItem(pos);

        int queNo = pos + 1;

        if (!stringArrayListHashMap.get(pos).isSolution) {
            hideSolutionHeader(pos, headerHolder);
        }

        headerHolder.tvQueNO.setText((queNo++) + ".");

        headerHolder.tv_que_type.setText(Html.fromHtml("Q-" + (GeneratePaperSolutionFragment.pageNumber + 1) + " " + stringArrayListHashMap.get(pos).getQuestionType()));
        if ((stringArrayListHashMap.get(pos).getQuestion().contains("<img")) || (stringArrayListHashMap.get(pos).getQuestion().contains("<math") || (stringArrayListHashMap.get(pos).getQuestion().contains("<table")))) {
            headerHolder.qWebView.setVisibility(View.VISIBLE);
            headerHolder.qWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getQuestion())));
            headerHolder.tv_question.setVisibility(View.GONE);
        } else {
            headerHolder.tv_question.setText(Html.fromHtml(stringArrayListHashMap.get(pos).getQuestion()));
        }
        if(stringArrayListHashMap.get(pos).getIsPassage().equals("1")){
            headerHolder.btn_solution.setVisibility(View.GONE);
        }

        headerHolder.btn_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!stringArrayListHashMap.get(pos).isSolution) {
                    stringArrayListHashMap.get(pos).isSolution = true;
                    scrollRecycler(pos);
                    showSolutionHeader(pos, headerHolder);
                    headerHolder.sWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getAnswer())));
                } else {
                    stringArrayListHashMap.get(pos).isSolution = false;
                    hideSolutionHeader(pos, headerHolder);
                }
            }
        });
        webViewSettingHeader(headerHolder);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void bindContentHolder(final ViewHolderContent holder, final int pos) {
        final SetPaperQuestionAndTypes types = getItem(pos);
        int queNo = pos + 1;
        if (!stringArrayListHashMap.get(pos).isSolution) {
            hideSolutionContent(pos, holder);
        }

        holder.tvQueNO.setText((queNo++) + ".");

        if ((stringArrayListHashMap.get(pos).getQuestion().contains("<img")) || (stringArrayListHashMap.get(pos).getQuestion().contains("<math") || (stringArrayListHashMap.get(pos).getQuestion().contains("<table")))) {
            holder.qWebView.setVisibility(View.VISIBLE);
            holder.qWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getQuestion())));
            holder.tv_question.setVisibility(View.GONE);
        } else {
            holder.tv_question.setText(Html.fromHtml(stringArrayListHashMap.get(pos).getQuestion()));
        }
        if(stringArrayListHashMap.get(pos).getIsPassage().equals("1")){
            holder.btn_solution.setVisibility(View.GONE);
        }

        holder.btn_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!stringArrayListHashMap.get(pos).isSolution) {
                    stringArrayListHashMap.get(pos).isSolution = true;
                    scrollRecycler(pos);
                    showSolutionContent(pos, holder);
                    holder.sWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getAnswer())));
                } else {
                    stringArrayListHashMap.get(pos).isSolution = false;
                    hideSolutionContent(pos, holder);
                }
            }
        });

        webViewSettingContent(holder);


    }


    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {
        final SetPaperQuestionAndTypes types = getItem(pos);
        int queNo = pos + 1;
        if (!stringArrayListHashMap.get(pos).isSolution) {
            hideSolutionFooter(pos, holder);
        }

        holder.tvQueNO.setText((queNo++) + ".");

        if ((stringArrayListHashMap.get(pos).getQuestion().contains("<img")) || (stringArrayListHashMap.get(pos).getQuestion().contains("<math") || (stringArrayListHashMap.get(pos).getQuestion().contains("<table")))) {
            holder.qWebView.setVisibility(View.VISIBLE);
            holder.qWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getQuestion())));
            holder.tv_question.setVisibility(View.GONE);
        } else {
            holder.tv_question.setText(Html.fromHtml(stringArrayListHashMap.get(pos).getQuestion()));
        }
        if(stringArrayListHashMap.get(pos).getIsPassage().equals("1")){
            holder.btn_solution.setVisibility(View.GONE);
        }
        holder.btn_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!stringArrayListHashMap.get(pos).isSolution) {
                    stringArrayListHashMap.get(pos).isSolution = true;
                    showSolutionFooter(pos, holder);
                    scrollRecycler(pos);
                    holder.sWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getAnswer())));
                } else {
                    stringArrayListHashMap.get(pos).isSolution = false;
                    hideSolutionFooter(pos, holder);
                }
            }
        });
        webViewSettingFooter(holder);
    }

    public SetPaperQuestionAndTypes getItem(int position) {

        return stringArrayListHashMap.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).isHeader) {
            return VIEW_TYPE_HEADER;

        } else {
            if (position == (stringArrayListHashMap.size() - 1)) {
                return VIEW_TYPE_FOOTER;
            } else {
                return VIEW_TYPE_ITEM;
            }
        }
    }


    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Utils.Log("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Utils.Log("TAG " + tag + " -->", str);
        }
    }

    public static String getPrelimTestHeader(String html) {

        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "    <head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "        <title>Question List</title>\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
                "        <style type=\"text/css\">\n" +
                "            body { width: 100% !important } \n" +
                "            table td{ width: auto !important } \n" +
                "            table th{ width: auto !important } \n" +
                "            table{ width: 100% !important }\n" +
                "            .table {\n" +
                "                width: 100% !important \n" +
                "            }\n" +
                "            .table tr td {\n" +
                "                vertical-align: top;\n" +
                "            }\n" +
                "            .question p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +
                " .sub p {\n" +
                "\tdisplay:inline;\n" +
                " }" +
                "            body * {\n" +
                "                font-size: 12px;\n" +
                "            }\n" +

                "            .answer p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +
                "            body {\n" +
                //  "                width: 100%;\n" +
                "                height: auto;\n" +
                "                margin: 0;\n" +
                "                padding: 0;\n" +
                "                font: 12pt \"Tahoma\";\n" +
                "            }\n" +
                "            * {\n" +
                "                box-sizing: border-box;\n" +
                "                -moz-box-sizing: border-box;\n" +
                "            }\n" +
                "            .page {\n" +
                "                width: 210mm;\n" +
                "                min-height: 297mm;\n" +
                "                padding: 0mm;\n" +
                "                margin: 10mm auto;\n" +
                "                /*border: 1px #D3D3D3 solid;*/\n" +
                "                /*border-radius: 5px;*/\n" +
                "                background: white;\n" +
                "                /*box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);*/\n" +
                "            }\n" +
                "            .subpage {\n" +
                "                padding: 1cm;\n" +
                "                /*border: 5px red solid;*/\n" +
                "                height: 257mm;\n" +
                "                /*outline: 2cm #FFEAEA solid;*/\n" +
                "            }\n" +
                "\n" +
                "            @page {\n" +
                "                size: A4;\n" +
                "                margin: 0;\n" +
                "            }\n" +
                "            @media print {\n" +
                "                html, body {\n" +
                "                    width: 210mm;\n" +
                "                    height: 297mm;        \n" +
                "                }\n" +
                "                .page {\n" +
                "                    margin: 0;\n" +
                "                    border: initial;\n" +
                "                    border-radius: initial;\n" +
                "                    width: initial;\n" +
                "                    min-height: initial;\n" +
                "                    /*box-shadow: initial;*/\n" +
                "                    background: initial;\n" +
                "                    /*page-break-after: always;*/\n" +
                "                }\n" +
                "            }\n" +
                "      .tablenew tr td {\n" +

                "                 vertical-align: top; \n" +
                "              } " +
                "      .table tr.mtc td p{\n" +
                "                    margin: 0;\n" +
                "                 display: inline-block; \n" +
                "              } " +
                "table img{\n" +
                "width:100% !important;\n" +
                "max-width:100% !important;\n" +
                "}" +
                "        </style>\n" +
                "\n" +
                "<link rel=\"stylesheet\" href=\"file:///android_asset/mathscribe/jqmath-0.4.3.css\">" +
                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jquery-1.4.3.min.js\"></script>\n" +
                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jqmath-etc-0.4.6.min.js\"></script>\n" +
                "    </head>\n" +
                "    <body>\n" + html +
                "     </body></html>";
    }

    public void scrollRecycler(int pos) {
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(context) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
        smoothScroller.setTargetPosition(pos + 1);
        recyclerView.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    public void hideSolutionHeader(int pos, ViewHolderHeader holder) {


        holder.lin_solution.setVisibility(View.GONE);
        holder.btn_solution.setText("   View Solution   ");
        holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_line));

    }

    public void hideSolutionFooter(int pos, ViewHolderFooter holder) {


        holder.lin_solution.setVisibility(View.GONE);
        holder.btn_solution.setText("   View Solution   ");
        holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_line));

    }

    public void hideSolutionContent(int pos, ViewHolderContent holder) {


        holder.lin_solution.setVisibility(View.GONE);
        holder.btn_solution.setText("   View Solution   ");
        holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_line));

    }

    public void showSolutionHeader(int pos, ViewHolderHeader holder) {


        holder.lin_solution.setVisibility(View.VISIBLE);
        holder.btn_solution.setText("    Hide Solution    ");
        holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorWhite));
        holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_button));
        holder.sWebView.setVisibility(View.VISIBLE);
    }

    public void showSolutionFooter(int pos, ViewHolderFooter holder) {


        holder.lin_solution.setVisibility(View.VISIBLE);
        holder.btn_solution.setText("    Hide Solution    ");
        holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorWhite));
        holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_button));
        holder.sWebView.setVisibility(View.VISIBLE);
    }

    public void showSolutionContent(int pos, ViewHolderContent holder) {


        holder.lin_solution.setVisibility(View.VISIBLE);
        holder.btn_solution.setText("    Hide Solution    ");
        holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorWhite));
        holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_button));
        holder.sWebView.setVisibility(View.VISIBLE);
    }

    public void webViewSettingHeader(ViewHolderHeader holder) {

        holder.qWebView.getSettings().setJavaScriptEnabled(true);

        holder.qWebView.getSettings().setAllowFileAccess(true);
        holder.qWebView.getSettings().setJavaScriptEnabled(true);
        holder.qWebView.getSettings().setDomStorageEnabled(true);
        holder.qWebView.getSettings().setUseWideViewPort(true);

        holder.qWebView.getSettings().setBuiltInZoomControls(false);
        holder.qWebView.getSettings().setDisplayZoomControls(false);
        holder.qWebView.getSettings().setLoadWithOverviewMode(true);
        holder.qWebView.setInitialScale(1);

    }

    public void webViewSettingFooter(ViewHolderFooter holder) {

        holder.qWebView.getSettings().setJavaScriptEnabled(true);

        holder.qWebView.getSettings().setAllowFileAccess(true);
        holder.qWebView.getSettings().setJavaScriptEnabled(true);
        holder.qWebView.getSettings().setDomStorageEnabled(true);
        holder.qWebView.getSettings().setUseWideViewPort(true);

        holder.qWebView.getSettings().setBuiltInZoomControls(false);
        holder.qWebView.getSettings().setDisplayZoomControls(false);
        holder.qWebView.getSettings().setLoadWithOverviewMode(true);
        holder.qWebView.setInitialScale(1);

    }

    public void webViewSettingContent(ViewHolderContent holder) {


        holder.qWebView.getSettings().setJavaScriptEnabled(true);

        holder.qWebView.getSettings().setAllowFileAccess(true);
        holder.qWebView.getSettings().setJavaScriptEnabled(true);
        holder.qWebView.getSettings().setDomStorageEnabled(true);
        holder.qWebView.getSettings().setUseWideViewPort(true);

        holder.qWebView.getSettings().setBuiltInZoomControls(false);
        holder.qWebView.getSettings().setDisplayZoomControls(false);
        holder.qWebView.getSettings().setLoadWithOverviewMode(true);
        holder.qWebView.setInitialScale(1);
    }


}
