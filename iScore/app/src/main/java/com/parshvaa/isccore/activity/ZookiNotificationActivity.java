package com.parshvaa.isccore.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.ObservableScrollView;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

public class ZookiNotificationActivity extends BaseActivity {
    private ImageView mImageView;
    private TextView tv_FullDesc, tv_title, tv_date, tv_header_title;
    public App app;
    public ImageView img_back;

    private ObservableScrollView mScrollView;
    public int mParallaxImageHeight;
    public FrameLayout frame_content, frame_viewMore;
    ZookiNotificationActivity instance;
    public AQuery aQuery;
    public ProgressBar pbar;
    public String title = "", bigPicture = "", created_on = "", body = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_zooki_notification);
        Utils.logUser();
        aQuery = new AQuery(this);
        title = getIntent().getExtras().getString("title");
        bigPicture = getIntent().getExtras().getString("bigPicture");
        created_on = getIntent().getExtras().getString("created_on");
        body = getIntent().getExtras().getString("body");


        tv_title = findViewById(R.id.tv_title);
        tv_FullDesc = findViewById(R.id.tv_FullDesc);
        tv_date = findViewById(R.id.tv_date);
        frame_content = findViewById(R.id.frame_content);
        frame_viewMore = findViewById(R.id.frame_viewMore);
        mImageView = findViewById(R.id.image);
        pbar = findViewById(R.id.pbar);
        Utils.Log("TAG", "URL :- " + bigPicture);
        aQuery.id(mImageView).progress(pbar).image(bigPicture, true, true, 0, R.drawable.zooki_image, null, 0, 0.0f);
        img_back= findViewById(R.id.img_back);
        tv_title.setText(title);
        tv_FullDesc.setText(body);

        try {
            created_on = created_on.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_date.setText(Utils.changeDateToDDMMYYYY(created_on));
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

    }
}
