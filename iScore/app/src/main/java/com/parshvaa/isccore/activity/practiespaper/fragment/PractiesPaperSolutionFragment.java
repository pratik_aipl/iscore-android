package com.parshvaa.isccore.activity.practiespaper.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;

import com.parshvaa.isccore.activity.practiespaper.activity.PractiesPaperSolutionActivity;
import com.parshvaa.isccore.activity.practiespaper.adapter.PrectisPaperSolutionAdapter;
import com.parshvaa.isccore.model.GeneratePaper;
import com.parshvaa.isccore.model.PrelimTestRecord;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.PrelimQuestionListModel;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.nineoldandroids.view.ViewPropertyAnimator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by empiere-vaibhav on 1/8/2018.
 */

public class PractiesPaperSolutionFragment extends Fragment {
    public View v;
    private static final String ARG_PAGE_NUMBER = "page_number";
    private static final String MODEL = "questionModel";
    private static final String list = "list";
    private List<GeneratePaper> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PrectisPaperSolutionAdapter mAdapter;
    private ArrayList<String> mKeys;

    private List<PrelimTestRecord> typesArrayList = new ArrayList<>();
    List<PrelimQuestionListModel> questionsList = new ArrayList<>();
    private boolean mFabIsShown;
    public int pageNumber = 0;


    public Fragment newInstance(int page, ArrayList<PrelimQuestionListModel> questionsList, PrelimQuestionListModel questionModel) {
        PractiesPaperSolutionFragment fragment = new PractiesPaperSolutionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        args.putSerializable(MODEL, questionModel);
        args.putSerializable(list, questionsList);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_paper_solution, container, false);
        recyclerView = v.findViewById(R.id.recycler_paper);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
        questionsList = (List<PrelimQuestionListModel>) getArguments().getSerializable(list);
        typesArrayList = questionsList.get(pageNumber).prelimList;
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));

        LayoutAnimationController controller = null;
        int anim = R.anim.layout_animation_fall_down;
        controller = AnimationUtils.loadLayoutAnimation(getActivity(), anim);
        if (controller != null)
            recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new PrectisPaperSolutionAdapter(typesArrayList, getActivity(), recyclerView);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    hideFab();
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    hideFab();

                } else {
                    showFab();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }
        });

        return v;

    }

    @Override
    public void setMenuVisibility(final boolean visible) {
//        if (visible) {
//            typesArrayList = PractiesPaperSolutionActivity.questionsList.get(pageNumber).prelimList;
//        }
    }

    public String getKey(int position) {
        return mKeys.get(position);
    }

    private void showFab() {
        if (!mFabIsShown) {
            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_left_button).scaleX(1).scaleY(1).setDuration(200).start();
            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_right_button).scaleX(1).scaleY(1).setDuration(200).start();
            mFabIsShown = true;
        }
    }

    private void hideFab() {
        if (mFabIsShown) {
            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_right_button).scaleX(0).scaleY(0).setDuration(200).start();

            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((PractiesPaperSolutionActivity) getActivity()).float_left_button).scaleX(0).scaleY(0).setDuration(200).start();
            mFabIsShown = false;
        }
    }


}
