package com.parshvaa.isccore.activity.iconnect.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.activity.iconnect.adapter.PracticePaperAdapter;
import com.parshvaa.isccore.activity.iconnect.ITestPaper;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseFragment;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class IPracticePaperFragment extends BaseFragment implements SelectSubjectDrawer.SubjectArrayListListener, AsynchTaskListner {
    //    public View v;
    private static final String TAG = "IPracticePaperFragment";
    Button left_button;
    private SelectSubjectDrawer left_drawer;
    public JsonParserUniversal jParser;
    public RecyclerView recycler_mcq_sub;
    public ArrayList<ITestPaper> paperArray = new ArrayList<>();
    private PracticePaperAdapter adapter;
    public ITestPaper emp;
    public ArrayList<Subject> EvaSubjectArray = new ArrayList<>();
    public RelativeLayout mainLay, mainLayout;
    public DrawerLayout drawerLayout;
    private LinearLayout emptyView;
    public TextView tv_empty;

    boolean IsDrawer=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_ipractice_test, container, false);
        recycler_mcq_sub = v.findViewById(R.id.recycler_mcq_sub);
        mainLayout = v.findViewById(R.id.mainLayout);
        left_button = v.findViewById(R.id.subject_button);
        jParser = new JsonParserUniversal();
        emptyView = v.findViewById(R.id.empty_view);
        tv_empty = v.findViewById(R.id.tv_empty);

        emptyView.setVisibility(View.GONE);

        drawerLayout = v.findViewById(R.id.drawer_layout1);
        left_drawer = (SelectSubjectDrawer) getActivity().getFragmentManager().findFragmentById(R.id.fragment_subject_practice_drawer);
        showProgress(true);
       // new CallRequest(IPracticePaperFragment.this).get_recent_question_paper();
        new CallRequest(IPracticePaperFragment.this).get_paper("");
        IsDrawer=false;
        new CallRequest(IPracticePaperFragment.this).get_evaluator_subjects("generate");

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        showProgress(true);
       // new CallRequest(IPracticePaperFragment.this).get_recent_question_paper();
        new CallRequest(IPracticePaperFragment.this).get_paper("");
        IsDrawer=false;

    }

    private void setupRecyclerView() {
        final Context context = recycler_mcq_sub.getContext();

        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_mcq_sub.setLayoutAnimation(controller);
        recycler_mcq_sub.scheduleLayoutAnimation();
        recycler_mcq_sub.setLayoutManager(new LinearLayoutManager(context));
        adapter = new PracticePaperAdapter(paperArray, context);
        recycler_mcq_sub.setAdapter(adapter);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.d(TAG, "onTaskCompleted: " + result);
            switch (request) {
                case get_evaluator_subjects:
                    showProgress(false);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        Subject subject = new Subject();
                        if (jObj.getBoolean("status")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                EvaSubjectArray.clear();
                                EvaSubjectArray.addAll(LoganSquare.parseList(jBranchArray.toString(), Subject.class));
//                                for (int i = 0; i < jBranchArray.length(); i++) {
//                                    JSONObject jSubject = jBranchArray.getJSONObject(i);
//                                    subject = (Subject) jParser.parseJson(jSubject, new Subject());
//                                    EvaSubjectArray.add(subject);
//                                }

                                left_drawer.setUpButton(R.id.fragment_subject_practice_drawer, drawerLayout, left_button, mainLayout, EvaSubjectArray);
                                left_drawer.setDDrawerListener(IPracticePaperFragment.this);

                            } else {
                                Utils.showToast(jObj.getString("message"), getActivity());
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case get_recent_question_paper:
                    showProgress(false);
                    paperArray.clear();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            recycler_mcq_sub.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);

                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {
                                for (int i = 0; i < jDataArray.length(); i++) {
                                    JSONObject jpaper = jDataArray.getJSONObject(i);
                                    emp = (ITestPaper) jParser.parseJson(jpaper, new ITestPaper());
                                    paperArray.add(emp);
                                }
                                setupRecyclerView();
                            }

                        } else {
                            showProgress(false);
                            tv_empty.setText(jObj.getString("message"));
                            recycler_mcq_sub.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        showProgress(false);
                        tv_empty.setText("message");
                        recycler_mcq_sub.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                    break;
                case get_paper:
                    showProgress(false);
                    paperArray.clear();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "get papper result>>: "+result.toString());
                        if (jObj.getBoolean("status")) {
                            recycler_mcq_sub.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);

                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {

                                for (int i = 0; i < jDataArray.length(); i++) {
                                    JSONObject jpaper = jDataArray.getJSONObject(i);
                                    emp = (ITestPaper) jParser.parseJson(jpaper, new ITestPaper());
                                    paperArray.add(emp);
                                }
                                if(IsDrawer){
                                    adapter = new PracticePaperAdapter(paperArray, recycler_mcq_sub.getContext());
                                    recycler_mcq_sub.setAdapter(adapter);
                                }else{
                                    setupRecyclerView();
                                }


                            }

                        } else {
                            showProgress(false);
                            tv_empty.setText(jObj.getString("message"));
                            recycler_mcq_sub.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        showProgress(false);
                        tv_empty.setText("message");
                        recycler_mcq_sub.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().finish();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public void displayView(int position) {

        switch (position) {
            case 0:

                break;
            case 1:

                break;
            case 2:
                break;

            case 3:
                break;

            case 4:
                break;

            case 5:
                break;

            case 6:
                break;

            case 7:

                break;
            default:
                break;
        }

    }

    @Override
    public void onDrawerItemSelected(ArrayList<String> subjectId) {
        String SubjectId = android.text.TextUtils.join(",", subjectId);
        showProgress(true);
        new CallRequest(IPracticePaperFragment.this).get_paper(SubjectId);
        IsDrawer=true;

    }
}
