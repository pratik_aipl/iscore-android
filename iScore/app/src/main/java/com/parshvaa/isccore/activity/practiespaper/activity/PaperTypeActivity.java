package com.parshvaa.isccore.activity.practiespaper.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.isccore.activity.practiespaper.adapter.PaperTypeAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.OnTaskCompleted;
import com.parshvaa.isccore.custominterface.PaperTypeInterface;
import com.parshvaa.isccore.model.ClassQuestionPaperSubQuestion;
import com.parshvaa.isccore.model.ExamTypePatternDetail;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.PaperTypeWithTotalTest;
import com.parshvaa.isccore.model.PrelimTestRecord;
import com.parshvaa.isccore.model.StudentQuestionPaper;
import com.parshvaa.isccore.model.StudentQuestionPaperDetail;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.MasterQuestionWithSubQue;
import com.parshvaa.isccore.tempmodel.PrelimQuestionListModel;
import com.parshvaa.isccore.tempmodel.SubQueKey;
import com.parshvaa.isccore.tempmodel.SubQueLabel;
import com.parshvaa.isccore.db.DBConstant;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.RomanNumber;
import com.parshvaa.isccore.utils.Utils;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class PaperTypeActivity extends BaseActivity implements PaperTypeInterface, OnTaskCompleted {
    private static final String TAG = "PaperTypeActivity";
    public ArrayList<PaperTypeWithTotalTest> paperTypeList = new ArrayList<>();
    private RecyclerView recycler_paper_type;
    private PaperTypeAdapter setPaperTypeAdapter;
    public ImageView img_back;
    public TextView tv_title;
    public Button btn_next;
    public int pos = -1;
    public String quesIds = "", chapterIds = "";
    public boolean checkcheckPattenId = false, onClick = false;
    public boolean checkPatternDetail = false;
    public ExamTypePatternDetail examTypePatternDetail;
    public String PageNo = "";

    public int count = 0;

    public long StudentQuestionPaperID = -1, DetailID = -1;
    public PrelimTestRecord prelimObj;

    public ArrayList<PrelimQuestionListModel> questionsList = new ArrayList<>();
    public ClassQuestionPaperSubQuestion subQuesObj;
    public SubQueLabel subQuesLabelObj;
    public SubQueKey subQueskeyObj;

    public ArrayList<ClassQuestionPaperSubQuestion> subQuesArray = new ArrayList<>();
    //    public ArrayList<SubQueLabel> subQuesLabelArray = new ArrayList<>();
    public int sub_questions_total, sub_question_type_total, SubQuestionTypeID = -1, sss = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_paper_type);
        Utils.logUser();

        recycler_paper_type = findViewById(R.id.recycler_paper_type);
        img_back = findViewById(R.id.img_back);
        btn_next = findViewById(R.id.btn_next);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Select Paper Type");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recycler_paper_type.addItemDecoration(new ItemOffsetDecoration(spacing));

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pos == -1) {
                    Utils.showToastinCenter("Please select paper type", PaperTypeActivity.this);
                    return;
                } else {
                    if (!onClick) {
                        onClick = true;
                        if (pos == 0) {
                            App.practiesPaperObj.setPaperTypeID(paperTypeList.get(pos).getPaperTypeID());
                            new getPrelimTest(PaperTypeActivity.this).execute();
                        } else if (pos == 1) {
                            App.practiesPaperObj.setPaperTypeID(paperTypeList.get(pos).getPaperTypeID());
                            startActivity(new Intent(PaperTypeActivity.this, ReadyPaperTypeActivity.class)
                                    .putExtra("PaperTypeId", paperTypeList.get(pos).getPaperTypeID())
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                        } else if (pos == 2) {
                            App.practiesPaperObj.setPaperTypeID(paperTypeList.get(pos).getPaperTypeID());
                            startActivity(new Intent(PaperTypeActivity.this, PractiesChapterActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    .putExtra("SetPaper", "true"));
                        }
                    }
                }
            }
        });
    }


    private void setupRecyclerView() {
        final Context context = recycler_paper_type.getContext();

        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_paper_type.setLayoutAnimation(controller);
        recycler_paper_type.scheduleLayoutAnimation();
        recycler_paper_type.setLayoutManager(new LinearLayoutManager(context));
        setPaperTypeAdapter = new PaperTypeAdapter(paperTypeList, context);
        recycler_paper_type.setAdapter(setPaperTypeAdapter);

    }

    @Override
    public void getPosition(final int pos) {
        this.pos = pos;
    }

    //For Prelim Paper

    public class getPrelimTest extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        public OnTaskCompleted listener;
        public String messagee = "";
        String examTypesID = "";
        public float mark = 0;
        public String MPSQID = "0";
        String msg = "";

        private getPrelimTest(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            this.listener = (OnTaskCompleted) context;

        }

        @Override
        protected void onPreExecute() {
            examTypesID = "";
            PageNo = "";
            showProgress(true, true);//Utils.showProgressDialog(instance);
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {
            Cursor cExamTypes = mDb.getAllRows(DBQueries.getExamtypesID(App.practiesPaperObj.getSubjectID()));

            if (cExamTypes != null && cExamTypes.moveToFirst()) {
                do {
                    examTypesID += cExamTypes.getString(0) + ",";
                } while (cExamTypes.moveToNext());
                if (examTypesID.length() > 0) {
                    examTypesID = examTypesID.substring(0, examTypesID.length() - 1);
                }
                cExamTypes.close();
            }

            Cursor c = mDb.getAllRows(DBQueries.getRandomExamTypeTest(examTypesID, App.practiesPaperObj.getPaperTypeID()));
            if (c != null && c.moveToFirst()) {
                do {
                    App.practiesPaperObj.setExamTypeID(c.getString(c.getColumnIndex("ExamTypeID")));
                } while (c.moveToNext());
                c.close();
            }

            c = mDb.getAllRows(DBQueries.getExamTypePatternId(App.practiesPaperObj.getSubjectID(), App.practiesPaperObj.getExamTypeID()));
            if (c != null && c.moveToFirst()) {
                App.practiesPaperObj.setExamTypePatternId(getExamPatternID(c));
            }
            c.close();


            if (!App.practiesPaperObj.getExamTypePatternId().isEmpty()) {
                StudentQuestionPaper sPaper = new StudentQuestionPaper();
                sPaper.setExamTypePatternID(App.practiesPaperObj.getExamTypePatternId());
                sPaper.setSubjectID(App.practiesPaperObj.getOldSubjectID());
                sPaper.setStudentID(MainUser.getStudentId());
                sPaper.setPaperTypeID(App.practiesPaperObj.getPaperTypeID());
                StudentQuestionPaperID = mDb.insertRow(sPaper, DBConstant.student_question_paper);
            } else {
                showProgress(false, true);
                if (mySharedPref.getVersion().equals("1")) {
                    messagee = "Alert! To view this please purchase the complete version.";

                } else {
                    if (App.practiesPaperObj.getExamTypePatternId().equals("1")) {
                        messagee = "Please carefully read the instruction.";
                    } else {
                        messagee = "Please read the instructions and selects chapters accordingly.";
                    }
                }
                return messagee;
            }
            Cursor cExamTypePatrernDetail = mDb.getAllRows(DBQueries.getExamTypePatternDetail(App.practiesPaperObj.getExamTypePatternId()));

            if (cExamTypePatrernDetail != null && cExamTypePatrernDetail.moveToFirst()) {

                String CheckQuestionID = "";
                do {
                    examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());
                    if (examTypePatternDetail.getIsQuestion().equals("0")) {
                        examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());


                    } else {
                        Cursor Ques;
                        MasterQuestionWithSubQue masterQuestion = null;
                        if (examTypePatternDetail.getChapterID().equals("0")) {
                            Ques = mDb.getAllRows(DBQueries.getRandomSubQuestionSubjectWise(App.practiesPaperObj.getSubjectID(), examTypePatternDetail.getQuestionTypeID(), CheckQuestionID));
                        } else {
                            Ques = mDb.getAllRows(DBQueries.getRandomSubQuestionWithID(examTypePatternDetail.getChapterID(), examTypePatternDetail.getQuestionTypeID(), CheckQuestionID));
                        }
                        outerloop:
                        if (Ques != null && Ques.moveToFirst()) {

                            do {
                                ArrayList<SubQueLabel> subQuesLabelArray = new ArrayList<>();
                                masterQuestion = (MasterQuestionWithSubQue) cParse.parseCursor(Ques, new MasterQuestionWithSubQue());
                                quesIds = quesIds + masterQuestion.getMQuestionID() + ",";
                                chapterIds = chapterIds + masterQuestion.getChapterID() + ",";
                                String QuesTypeId = masterQuestion.getQuestionTypeID();
                                String IsQuesType = "";
                                int i = 1;
                                int isMTC = mDb.getAllRecordsUsingQuery(DBQueries.checkQusTypeisMTC(QuesTypeId + ""));


                                CheckQuestionID = CheckQuestionID + Ques.getString(Ques.getColumnIndex("MQuestionID")) + ",";
                                StudentQuestionPaperDetail cpd = new StudentQuestionPaperDetail();
                                cpd.setStudentQuestionPaperID((int) StudentQuestionPaperID);
                                cpd.setMQuestionID(masterQuestion.getMQuestionID());
                                cpd.setExamTypePatternDetailID(examTypePatternDetail.getExamTypePatternDetailID());
                                cpd.setCQuestionID(0);
                                cpd.setMasterorCustomized(0);


                                DetailID = mDb.insertRow(cpd, DBConstant.student_question_paper_detail);
                                Cursor cIsPassage = mDb.getAllRows(DBQueries.checkQusTypeisPassage(cpd.getMQuestionID()));

                                int isPassage = 0;
                                if (cIsPassage != null && cIsPassage.moveToFirst()) {
                                    isPassage = cIsPassage.getInt(0);
                                    cIsPassage.close();
                                }

                                if (isPassage == 1) {
                                    Cursor clabel = mDb.getAllRows(DBQueries.getLabel(Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID"))));
                                    if (clabel != null && clabel.moveToFirst()) {
                                        do {
                                            subQuesLabelObj = new SubQueLabel();
                                            subQuesLabelObj.setLabel(clabel.getString(clabel.getColumnIndexOrThrow("Label")));

                                            mark = clabel.getInt(clabel.getColumnIndexOrThrow("Mark"));
                                            subQueskeyObj = new SubQueKey();
                                            while (0 < mark) {
                                                Cursor csqi = mDb.getAllRows(DBQueries.getPassageSubquetions(MPSQID,
                                                        Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID")),
                                                        QuesTypeId, String.valueOf(mark), clabel.getString(clabel.getColumnIndexOrThrow("PassageSubQuestionTypeID"))));
                                                if (csqi != null && csqi.moveToFirst()) {
                                                    boolean isExistes = false;
                                                    try {

                                                        if (MPSQID.equals("0")) {
                                                            MPSQID = csqi.getString(csqi.getColumnIndex("MPSQID"));
                                                        } else {
                                                            MPSQID = MPSQID + "," + csqi.getString(csqi.getColumnIndex("MPSQID"));
                                                        }
                                                        //Utils.Log("ID :->", csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")));
                                                        mark = mark - csqi.getFloat(csqi.getColumnIndexOrThrow("Marks"));
                                                        //  Utils.Log("In IF :-> ", mark + "");
                                                        subQuesObj = new ClassQuestionPaperSubQuestion();
                                                        subQuesObj.setMPSQID(csqi.getString(csqi.getColumnIndex("MPSQID")));
                                                        subQuesObj.setDetailID(String.valueOf(DetailID));
                                                        subQuesObj.setPaperID(String.valueOf(StudentQuestionPaperID));
                                                        for (SubQueLabel lblObj : subQuesLabelArray) {

                                                            if (lblObj.getLabel().equals(clabel.getString(clabel.getColumnIndexOrThrow("Label")))) {
                                                                for (SubQueKey keyObj : lblObj.keyArray) {
                                                                    if (keyObj.getKey().equals(csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")))) {
                                                                        keyObj.internalArray.add(subQuesObj);
                                                                        // lblObj.keyArray.add(keyObj);
                                                                        isExistes = true;
                                                                        break;
                                                                    } else {
                                                                        isExistes = false;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (!isExistes) {
                                                            subQueskeyObj.setKey(csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")));
                                                            subQueskeyObj.internalArray.add(subQuesObj);
                                                        }
                                                        if (subQuesLabelArray.size() <= 0) {
                                                            subQuesLabelObj.keyArray.add(subQueskeyObj);
                                                            subQuesLabelArray.add(subQuesLabelObj);
                                                        }

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                        Utils.Log("In CATCH :-> ", mark + "");
                                                        mDb.dbQuery(DBNewQuery.DeleteStudent_question_paper((int) StudentQuestionPaperID));
                                                        csqi.close();
                                                        msg = "Sub Question Not Avaliable.!";
                                                        break outerloop;
                                                    }
                                                } else {
                                                    Utils.Log("In ELSE :-> ", mark + "");

                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_paper_detal((int) StudentQuestionPaperID));
                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_paper_Chapter((int) StudentQuestionPaperID));
                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_type((int) StudentQuestionPaperID));
                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_question_paper((int) StudentQuestionPaperID));

                                                    csqi.close();
                                                    msg = "Sub Question Not Avaliable.!";
                                                    break outerloop;
                                                }

                                            }

                                            boolean islblExistes = false;
                                            for (SubQueLabel lblObj : subQuesLabelArray) {
                                                if (lblObj.getLabel().equals(subQuesLabelObj.getLabel())) {
                                                    islblExistes = true;
                                                    break;
                                                } else {
                                                    islblExistes = false;
                                                }
                                            }
                                            if (!islblExistes) {
                                                subQuesLabelObj.keyArray.add(subQueskeyObj);
                                                subQuesLabelArray.add(subQuesLabelObj);
                                            }


                                        } while (clabel.moveToNext());
                                        clabel.close();

                                        for (SubQueLabel lblObj : subQuesLabelArray) {
                                            for (SubQueKey keyObj : lblObj.keyArray) {
                                                for (ClassQuestionPaperSubQuestion obj : keyObj.internalArray) {
                                                    mDb.insertRow(obj, DBConstant.class_question_paper_sub_question);
                                                }


                                            }

                                        }

                                    }

                                } else {

                                }


                            } while (Ques.moveToNext());
                        }
                    }

                } while (cExamTypePatrernDetail.moveToNext());
            }
            return msg;
        }

        public String getExamPatternID(Cursor c) {
            String tempExamTypePatternId = "";

            do {
                Cursor cExamTypePatrernDetail = mDb.getAllRows(DBQueries.getExamTypePatternDetail(c.getString(c.getColumnIndex("ExamTypePatternID"))));

                if (cExamTypePatrernDetail != null && cExamTypePatrernDetail.moveToFirst()) {
                    boolean checkQuestions = false;
                    String CheckQuestionID = "";
                    do {
                        examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());
                        if (examTypePatternDetail.getIsQuestion().equals("0")) {

                        } else {
                            Cursor Ques;
                            if (examTypePatternDetail.getChapterID().equals("0")) {
                                Ques = mDb.getAllRows(DBQueries.getRandomSubQuestionSubjectWise(App.practiesPaperObj.getSubjectID(), examTypePatternDetail.getQuestionTypeID(), CheckQuestionID));
                            } else {
                                Ques = mDb.getAllRows(DBQueries.getRandomSubQuestionWithID(examTypePatternDetail.getChapterID(), examTypePatternDetail.getQuestionTypeID(), CheckQuestionID));
                            }
                            if (Ques != null && Ques.moveToFirst()) {
                                checkQuestions = true;
                            } else {
                                checkQuestions = false;
                                break;
                            }
                            CheckQuestionID = CheckQuestionID + Ques.getString(Ques.getColumnIndex("MQuestionID")) + ",";
                        }

                    } while (cExamTypePatrernDetail.moveToNext());
                    cExamTypePatrernDetail.close();

                    if (checkQuestions) {
                        checkPatternDetail = true;
                        tempExamTypePatternId = c.getString(0);
                        break;
                    } else {
                        checkPatternDetail = false;
                    }
                }
                if (checkPatternDetail) {
                    checkcheckPattenId = true;
                    break;
                }
            } while (c.moveToNext());
            //c.close();
            // Utils.Log("TAG Final checkcheckPattenId ==> ", String.valueOf(checkcheckPattenId));
            return tempExamTypePatternId;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            ArrayList<PrelimTestRecord> tempPrelimTestRecordArrayList = new ArrayList<>();
            String HTML = "";
            try {
                tempPrelimTestRecordArrayList = (ArrayList) new CustomDatabaseQuery(PaperTypeActivity.this, new PrelimTestRecord())
                        .execute(new String[]{DBNewQuery.getPrelimPaperQuestionsIsPassage(String.valueOf(StudentQuestionPaperID))}).get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            App.practiesPaperObj.setStudentQuestionPaperID(String.valueOf(StudentQuestionPaperID));
            questionsList.clear();
            ArrayList<PrelimTestRecord> prelimTestRecordArrayList = new ArrayList<>();
            for (int i = 0; i < tempPrelimTestRecordArrayList.size(); i++) {
                if (tempPrelimTestRecordArrayList.get(i).getIsPassage() != null) {
                    if (tempPrelimTestRecordArrayList.get(i).getIsPassage().equals("1")) {
                        HTML = "";
                       int sub_qt = 0;
                        Cursor clabel = mDb.getAllRows(DBQueries.getLabel(tempPrelimTestRecordArrayList.get(i).getMQuestionID()));
                        if (clabel != null && clabel.moveToFirst()) {

                            int k = 1;
                            do {
                                Cursor csqi = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id(App.practiesPaperObj.getStudentQuestionPaperID(),
                                        tempPrelimTestRecordArrayList.get(i).getStudentQuestionPaperDetailID(),
                                        clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID"))));
                                if (csqi != null && csqi.moveToFirst()) {
                                    int sq = 0;
                                    int n = 1;
                                    do {
                                        Cursor totla = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id_total(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                tempPrelimTestRecordArrayList.get(i).getStudentQuestionPaperDetailID(),
                                                clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")),
                                                csqi.getString(csqi.getColumnIndex("SubQuestionTypeID"))));
                                        sub_questions_total = totla.getCount();
                                        totla.close();
                                        if (n == 1) {
                                            Cursor c = mDb.getAllRows(" SELECT DISTINCT(sqt.SubQuestionTypeID)\n" +
                                                    "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                                                    "INNER JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                                                    "INNER JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                                                    "INNER JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                                                    "WHERE `cqpsq`.`PaperID` = '" + App.practiesPaperObj.getStudentQuestionPaperID() + "'\n" +
                                                    "AND `cqpsq`.`DetailID` = '" + tempPrelimTestRecordArrayList.get(i).getStudentQuestionPaperDetailID() + "'\n" +
                                                    "AND `psqt`.`PassageSubQuestionTypeID` = '" + clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")) + "'");

                                            if (c != null && c.moveToFirst()) {
                                                sub_question_type_total = c.getCount();
                                                c.close();
                                            }


                                        }
                                        HTML = HTML + "<table> <tr class=\"margin_top\">\n" +
                                                "<td width=\"30\"></td>\n" +
                                                "<td width=\"30\" style=\"vertical-align: top;\"><b>";
                                        if (sub_qt == 0 && n == 1) {
                                            HTML = HTML + k + " </b></td>";
                                        }
                                        if (SubQuestionTypeID != csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"))) {
                                            SubQuestionTypeID = csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"));
                                            sq++;
                                            int sss = 1;
                                            if (n == 1) {
                                                HTML = HTML + "  <td width=\"660\">\n" +
                                                        "<table width=\"100%\">\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "           <b>";
                                                HTML = HTML + csqi.getString(csqi.getColumnIndex("Label")) + ")" +
                                                        "           </b>\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"630\">\n";

                                                if (sub_question_type_total == 1) {
                                                    HTML = HTML + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                } else {
                                                    HTML = HTML + RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                }

                                                HTML = HTML + "        </td>\n";

                                                double marks = sub_questions_total * csqi.getFloat(csqi.getColumnIndex("QuestionTypeMarks"));
                                                DecimalFormat format = new DecimalFormat();
                                                format.setDecimalSeparatorAlwaysShown(false);


                                                //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + format.format(marks) + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>";
                                            } else {
                                                HTML = HTML + "<td width=\"660\">\n" +
                                                        "<table>\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"630\">\n" +
                                                        RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType")) +
                                                        "       </td>\n";
                                                double marks = sub_questions_total * csqi.getFloat(csqi.getColumnIndex("QuestionTypeMarks"));


                                                DecimalFormat format = new DecimalFormat();
                                                format.setDecimalSeparatorAlwaysShown(false);

                                                //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + format.format(marks) + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>    ";
                                            }

                                           /* int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>";*/
                                        }

                                        HTML = HTML + "</tr>" +
                                                " <tr class=\"margin_top\">\n" +
                                                "   <td width=\"30\"></td>\n" +
                                                "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";
                                        
                                        
                                        if (sub_questions_total == 1) {
                                            HTML = HTML + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                          //  HTML = HTML + Utils.replaceImagesPathtemp(csqi.getString(csqi.getColumnIndex("Question")),fetch);
                                            Log.d(TAG, "imageName=1: "+csqi.getString(csqi.getColumnIndex("Question")));
                                        } else {
                                            HTML = HTML + sss + ") " + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                         //   HTML = HTML + sss + ") " + Utils.replaceImagesPathtemp(csqi.getString(csqi.getColumnIndex("Question")),fetch);
                                            Log.d(TAG, "imageName=2: "+csqi.getString(csqi.getColumnIndex("Question")));

                                        }

                                        HTML = HTML + "   </td>\n" +
                                                "   <td align=\"right\" width=\"30\"></td>\n" +
                                                "</tr>      ";
                                        n++;
                                        sss++;
                                    }
                                    while (csqi.moveToNext());
                                    csqi.close();
                                }
                                if (sub_qt == 0) {
                                    HTML = HTML + "<tr class=\"margin_top\">\n" +
                                            "<td width=\"30\"></td>\n" +
                                            "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                            "<td width=\"660\">" +
                                            tempPrelimTestRecordArrayList.get(i).getQuestion() +
                                            "</td>\n" +
                                            "<td align=\"right\" width=\"30\"></td>\n" +
                                            "</tr>     \n";
                                }

                                sub_qt++;
                            } while (clabel.moveToNext());
                            clabel.close();
                            k++;

                        } else {
                        }
                        tempPrelimTestRecordArrayList.get(i).setQuestion(HTML + "</table>");
                        // tempPrelimTestRecordArrayList.get(i).setQuestion("test");

                    }
                }

                if (tempPrelimTestRecordArrayList.get(i).getPageNo().equals(PageNo)) {


                    prelimObj = new PrelimTestRecord();
                    prelimObj.setAnswer(tempPrelimTestRecordArrayList.get(i).getAnswer());
                    prelimObj.setExamTypePatternDetailID(tempPrelimTestRecordArrayList.get(i).getExamTypePatternDetailID());
                    prelimObj.setIsQuestion(tempPrelimTestRecordArrayList.get(i).getIsQuestion());
                    prelimObj.setPageNo(tempPrelimTestRecordArrayList.get(i).getPageNo());
                    prelimObj.setQuestionNo(tempPrelimTestRecordArrayList.get(i).getQuestionNo());
                    prelimObj.setQuestionTypeText(tempPrelimTestRecordArrayList.get(i).getQuestionTypeText());
                    prelimObj.setSubQuestionNo(tempPrelimTestRecordArrayList.get(i).getSubQuestionNo());
                    prelimObj.setIsPassage(tempPrelimTestRecordArrayList.get(i).getIsPassage());

                    prelimObj.setQuestionMarks(tempPrelimTestRecordArrayList.get(i).getQuestionMarks());
                    prelimObj.setQuestionTypeID(tempPrelimTestRecordArrayList.get(i).getQuestionTypeID());


                    prelimObj.setQuestion(tempPrelimTestRecordArrayList.get(i).getQuestion());
                    if (tempPrelimTestRecordArrayList.get(i).getIsPassage() != null) {
                        if (tempPrelimTestRecordArrayList.get(i).getIsPassage().equals("1")) {
                            longInfo(tempPrelimTestRecordArrayList.get(i).getQuestion(), "IS PAssage Que IF:-> ");
                        }

                    }
                    prelimTestRecordArrayList.add(prelimObj);


                    //  stringArrayListHashMap.put(PageNo, prelimTestRecordArrayList);
                } else {

                    questionsList.add(new PrelimQuestionListModel(PageNo, prelimTestRecordArrayList));
                    PageNo = tempPrelimTestRecordArrayList.get(i).getPageNo();


                    prelimTestRecordArrayList = new ArrayList<>();
                    prelimObj = new PrelimTestRecord();


                    prelimObj.setAnswer(tempPrelimTestRecordArrayList.get(i).getAnswer());
                    prelimObj.setExamTypePatternDetailID(tempPrelimTestRecordArrayList.get(i).getExamTypePatternDetailID());
                    prelimObj.setIsQuestion(tempPrelimTestRecordArrayList.get(i).getIsQuestion());
                    prelimObj.setPageNo(tempPrelimTestRecordArrayList.get(i).getPageNo());
                    prelimObj.setQuestionNo(tempPrelimTestRecordArrayList.get(i).getQuestionNo());
                    prelimObj.setQuestionTypeText(tempPrelimTestRecordArrayList.get(i).getQuestionTypeText());
                    prelimObj.setSubQuestionNo(tempPrelimTestRecordArrayList.get(i).getSubQuestionNo());
                    prelimObj.setQuestion(tempPrelimTestRecordArrayList.get(i).getQuestion());
                    prelimObj.setIsPassage(tempPrelimTestRecordArrayList.get(i).getIsPassage());
                    prelimObj.setQuestionMarks(tempPrelimTestRecordArrayList.get(i).getQuestionMarks());
                    prelimObj.setQuestionTypeID(tempPrelimTestRecordArrayList.get(i).getQuestionTypeID());

                    if (tempPrelimTestRecordArrayList.get(i).getIsPassage() != null) {
                        if (tempPrelimTestRecordArrayList.get(i).getIsPassage().equals("1")) {
                            longInfo(tempPrelimTestRecordArrayList.get(i).getQuestion(), "IS PAssage Que ELSE:-> ");
                        }

                    }
                    prelimTestRecordArrayList.add(prelimObj);

                    //    stringArrayListHashMap.put(PageNo, prelimTestRecordArrayList);
                }


            }
            questionsList.add(new PrelimQuestionListModel(PageNo, prelimTestRecordArrayList));
            questionsList.remove(0);


            listener.onCompleted(s);
        }
    }


    @Override
    public void onCompleted() {
        showProgress(false, true);

    }

    @Override
    public void onCompleted(String msg) {
        showProgress(false, true);

        if (questionsList.size() > 0) {
            startActivity(new Intent(PaperTypeActivity.this, PractiesPaperSolutionActivity.class)
                    .putExtra("questionsList", questionsList));
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(PaperTypeActivity.this);
            builder1.setMessage(msg);
            builder1.setCancelable(true);

            builder1.setNegativeButton("Close", (dialog, i) -> {
                dialog.cancel();
                finish();
            });

//            builder1.setPositiveButton(
//                    "Subscribe",
//                    (dialog, id) -> {
//                        dialog.cancel();
//                        startActivity(new Intent(PaperTypeActivity.this, ChooseOptionActivity.class));
//                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Utils.Log("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Utils.Log("TAG " + tag + " -->", str);
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        pos = -1;
        onClick = false;
        Setdata();


    }

    public void Setdata() {
        try {

            //  System.out.println("Total Rows on try");

            paperTypeList = (ArrayList) new CustomDatabaseQuery(this, new PaperTypeWithTotalTest())
                    .execute(new String[]{DBNewQuery.getPractiesPaperType(App.practiesPaperObj.getOldSubjectID())}).get();

        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (paperTypeList.size() > 0) {
            paperTypeList.get(0).setIcone(R.drawable.icon_prelim);
            paperTypeList.get(0).setContaint("(User can auto generate all chapters test as per board pattern prescribed by Maharashtra State Board.)");
        }
        if (paperTypeList.size() > 1) {
            paperTypeList.get(1).setIcone(R.drawable.icon_ready);
            paperTypeList.get(1).setContaint("(User can auto generate question papers for single or multiple chapters from various available paper patterns for different marks.)");
        }
        if (paperTypeList.size() > 2) {
            paperTypeList.get(2).setIcone(R.drawable.icon_set);
            paperTypeList.get(2).setContaint("(User can select question types and frame their own paper pattern for single or multiple chapters to generate a test paper.)");
        }

        setupRecyclerView();

    }

    private void downloadFile(String url, String path) {
        Log.d(TAG, "downloadFile() called with: url = [" + url + "], path = [" + path + "]");
        File qLocalFile = new File(path);
        Request request = null;
        if (!qLocalFile.exists()) {
            request = new Request(url, path);
            request.setPriority(Priority.HIGH);
            request.setNetworkType(NetworkType.ALL);
            fetch.enqueue(request, updatedRequest -> {
                Log.d(TAG, "manageImagesDownload: ");
            }, error -> {
                Toast.makeText(this, "error enqueue download!!!", Toast.LENGTH_SHORT).show();
            });

        }

    }

}
