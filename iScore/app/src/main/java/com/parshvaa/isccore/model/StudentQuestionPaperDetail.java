package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 3/20/2017.
 */

@JsonObject
public class StudentQuestionPaperDetail implements Serializable {


    @JsonField
    public int StudentQuestionPaperDetailID = -1;
    @JsonField
    public int StudentQuestionPaperID = -1;
    @JsonField
    public int MasterorCustomized=0,CQuestionID=0;
    @JsonField
    public String MQuestionID = "", ExamTypePatternDetailID="";

    public int getMasterorCustomized() {
        return MasterorCustomized;
    }

    public void setMasterorCustomized(int masterorCustomized) {
        MasterorCustomized = masterorCustomized;
    }


    public int getStudentQuestionPaperDetailID() {
        return StudentQuestionPaperDetailID;
    }

    public void setStudentQuestionPaperDetailID(int studentQuestionPaperDetailID) {
        StudentQuestionPaperDetailID = studentQuestionPaperDetailID;
    }

    public int getStudentQuestionPaperID() {
        return StudentQuestionPaperID;
    }

    public void setStudentQuestionPaperID(int studentQuestionPaperID) {
        StudentQuestionPaperID = studentQuestionPaperID;
    }

    public String getExamTypePatternDetailID() {
        return ExamTypePatternDetailID;
    }

    public void setExamTypePatternDetailID(String examTypePatternDetailID) {
        ExamTypePatternDetailID = examTypePatternDetailID;
    }

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public int getCQuestionID() {
        return CQuestionID;
    }

    public void setCQuestionID(int CQuestionID) {
        this.CQuestionID = CQuestionID;
    }
    @JsonField(name = "subQuesArray")
    public ArrayList<ClassQuestionPaperSubQuestion> subQuesArray = new ArrayList<>();

    public ArrayList<ClassQuestionPaperSubQuestion> getSubQuesArray() {
        return subQuesArray;
    }

    public void setSubQuesArray(ArrayList<ClassQuestionPaperSubQuestion> subQuesArray) {
        this.subQuesArray = subQuesArray;
    }
}
