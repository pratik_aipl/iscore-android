package com.parshvaa.isccore.custominterface;

/**
 * Created by empiere-vaibhav on 1/10/2018.
 */

public interface PaperTypeInterface {
    void getPosition(int pos);
}
