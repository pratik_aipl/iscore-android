package com.parshvaa.isccore.activity.mcq;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nineoldandroids.view.ViewHelper;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.ObservableScrollView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.Level;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.FABToolbarLayout;
import com.parshvaa.isccore.utils.ProgressBarAnimation;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DifficultyLevelActivity extends BaseActivity implements ObservableScrollViewCallbacks {
    public DifficultyLevelActivity instance;
    public TextView tv_total_attempt_one, tv_total_attempt_two, tv_total_attempt_three, mTitleView,
            tv_accuracy, tv_subName, tv_accuracy_one, tv_accuracy_two, tv_accuracy_three;
    private Toolbar mToolbar;
    public ImageView img_sub;
    public CheckBox chk_not_appeard, chk_incorrcet, chk_random, chk_level_one, chk_level_two, chk_level_three;
    private View mFlexibleSpaceView, mOverlayView, mImageView;
    public ProgressBar pbar_total_accuracy, pbar_level_three, pbar_level_two, pbar_level_one;
    private int mActionBarSize, mFlexibleSpaceImageHeight;
    public ObservableScrollView mScrollView;
    public ImageView img_back;
    public TextView tv_title;
    public Button btn_next;
    public String SelctedChapID = "", QueAppear = "";
    public ArrayList<Level> levelArray = new ArrayList<>();
    public List<String> LeveliDS = new ArrayList<>();
    public FloatingActionButton fab;
    FABToolbarLayout mFabToolbar;
    public LinearLayout lin_notappeared, lin_incorrect, lin_random;
    public RelativeLayout mainLay;
    private MyDBManager mDb;
    public CursorParserUniversal cParse;
    public int chapterAccuracy = 0;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulty_level);
        Utils.logUser();
        instance = this;
        cParse = new CursorParserUniversal();
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mActionBarSize = getActionBarSize();
        CastVariabels();

        if (App.mcqdata != null && !TextUtils.isEmpty(App.mcqdata.getSelctedChapID())) {

            String Query = "select (((select count(StudentMCQTestDTLID)" +
                    " from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on " +
                    "mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1  AND mq.ChapterID IN(" + App.mcqdata.getSelctedChapID() + ") and EXISTS (select StudentMCQTestHDRID" +
                    " from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + App.mcqdata.subObj.getSubjectID() + "))    *100) / (select count (StudentMCQTestDTLID)" +
                    " from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where  mq.ChapterID IN(" + App.mcqdata.getSelctedChapID() + ") and EXISTS" +
                    "  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=" + App.mcqdata.subObj.getSubjectID() + ")))" +
                    " as accuracy from student_mcq_test_hdr as smth where smth.SubjectID=" + App.mcqdata.subObj.getSubjectID() + " limit 1";
            Cursor c = mDb.getAllRows(Query);
            if (c != null && c.moveToFirst()) {
                chapterAccuracy = c.getInt(0);
                c.close();
            }
        }
        fab.setOnClickListener(v -> {
            mScrollView.fullScroll(View.FOCUS_DOWN);
            mFabToolbar.show();
        });

        mTitleView.setText("Select Difficulty Level");
        if (App.mcqdata != null && App.mcqdata.subObj != null && !TextUtils.isEmpty(App.mcqdata.subObj.getSubjectName())) {
            tv_subName.setText(App.mcqdata.subObj.getSubjectName());
            File image = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + App.mcqdata.subObj.getSubjectIcon());
            if (image.exists()) {
                img_sub.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
            }
        }
        tv_accuracy.setText("Accuracy " + chapterAccuracy + "%");
        ProgressBarAnimation anim = new ProgressBarAnimation(pbar_total_accuracy, 0, chapterAccuracy);
        anim.setDuration(1000);
        pbar_total_accuracy.startAnimation(anim);

        getLevelOne();
        getLevelTwo();
        getLevelThree();


        if (notAppear() == null || notAppear().size() == 0) {
            Utils.Log("TAG", "NOT APPEAR  :-> " + notAppear().size() + "");
            chk_not_appeard.setFocusable(false);
            chk_not_appeard.setClickable(false);
            lin_notappeared.setAlpha(0.5f);
        }
        if (inCorect() == null || inCorect().size() == 0) {
            Utils.Log("TAG", " inCorect  :-> " + inCorect().size() + "");
            chk_incorrcet.setFocusable(false);
            chk_incorrcet.setClickable(false);
            lin_incorrect.setAlpha(0.5f);
        }

        mScrollView.setScrollViewCallbacks(this);
        mScrollView.fullScroll(View.FOCUS_DOWN);
        img_back.setOnClickListener(v -> onBackPressed());
        btn_next.setOnClickListener(v -> {
            if (!chk_level_one.isChecked() && !chk_level_two.isChecked() && !chk_level_three.isChecked()) {
                Utils.showSnakBar("Please Select Level", mainLay, instance);
                return;
            } else if (!chk_random.isChecked() && !chk_incorrcet.isChecked() && !chk_not_appeard.isChecked()) {
                Utils.showSnakBar("Please Select any Option  ", mainLay, instance);
                return;
            } else {
                if (App.mcqdata != null) {
                    if (chk_random.isChecked()) {
                        App.mcqdata.setQueAppear("isRandom");
                    } else {
                        if (chk_not_appeard.isChecked() && chk_incorrcet.isChecked()) {
                            App.mcqdata.setQueAppear("inCorect_and_NotAppear");
                        } else {
                            if (chk_not_appeard.isChecked()) {
                                App.mcqdata.setQueAppear("notAppear");
                            } else {
                                App.mcqdata.setQueAppear("inCorect");
                            }
                        }
                    }
                    App.mcqdata.setLevel(getSelectedLevel());
                }
                startActivity(new Intent(DifficultyLevelActivity.this, StartTestActivity.class));
            }
        });
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) 0 / mFlexibleSpaceImageHeight, 0, 1));
    }

    public static ArrayList<McqQuestion> notAppearArray = new ArrayList<>();
    public static ArrayList<McqQuestion> inCorectArray = new ArrayList<>();

    public ArrayList<McqQuestion> notAppear() {
        notAppearArray.clear();
        try {
            if (App.mcqdata != null && !TextUtils.isEmpty(App.mcqdata.getSelctedChapID())) {
                String bordId=mySharedPref.getBoardID();
                String mediumId=mySharedPref.getMediumID();
                String standardId=mySharedPref.getStandardID();

                notAppearArray = (ArrayList) new CustomDatabaseQuery(this, new McqQuestion())
                        .execute(new String[]{DBNewQuery.get_mcq_notAppeard_diff(bordId,mediumId,standardId, App.mcqdata.subObj.getSubjectID(), App.mcqdata.getLevel(), App.mcqdata.getSelctedChapID())}).get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notAppearArray;
    }

    public ArrayList<McqQuestion> inCorect() {
        inCorectArray.clear();
        try {
            if (App.mcqdata != null && !TextUtils.isEmpty(App.mcqdata.getSelctedChapID())) {

                String bordId=mySharedPref.getBoardID();
                String mediumId=mySharedPref.getMediumID();
                String standardId=mySharedPref.getStandardID();

                inCorectArray = (ArrayList) new CustomDatabaseQuery(this, new McqQuestion())
                        .execute(new String[]{DBNewQuery.get_mcq_incorect_diff(bordId,mediumId,standardId,App.mcqdata.subObj.getSubjectID(), App.mcqdata.getLevel(), App.mcqdata.getSelctedChapID())}).get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inCorectArray;
    }

    public String getSelectedLevel() {
        return android.text.TextUtils.join(",", LeveliDS);
    }

    public void getLevelOne() {
        try {
            if (App.mcqdata != null && !TextUtils.isEmpty(App.mcqdata.getSelctedChapID())) {

                levelArray = (ArrayList) new CustomDatabaseQuery(this, new Level())
                        .execute(new String[]{DBNewQuery.get_level_wise_accuraccy("1", App.mcqdata.subObj.getSubjectID(), App.mcqdata.getSelctedChapID())}).get();
                if (levelArray.size() > 0) {

                    tv_total_attempt_one.setText(Html.fromHtml(totalAttemptHtml(String.valueOf(levelArray.get(0).getTotal_attempt_level()), String.valueOf(levelArray.get(0).getRight_Answer_level()))));
                    ProgressBarAnimation anim = new ProgressBarAnimation(pbar_level_one, 0, levelArray.get(0).getAccuracy());
                    anim.setDuration(1000);
                    pbar_level_one.startAnimation(anim);
                    tv_accuracy_one.setText("Accuracy " + levelArray.get(0).getAccuracy() + "%");
                } else {
                    tv_total_attempt_one.setText(Html.fromHtml(totalAttemptHtml("0", "0")));
                    ProgressBarAnimation anim = new ProgressBarAnimation(pbar_level_one, 0, 0);
                    anim.setDuration(1000);
                    pbar_level_one.startAnimation(anim);
                    tv_accuracy_one.setText("Accuracy " + "0" + "%");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void getLevelTwo() {
        try {
            if (App.mcqdata != null && !TextUtils.isEmpty(App.mcqdata.getSelctedChapID())) {
                levelArray = (ArrayList) new CustomDatabaseQuery(this, new Level())
                        .execute(new String[]{DBNewQuery.get_level_wise_accuraccy("2", App.mcqdata.subObj.getSubjectID(), App.mcqdata.getSelctedChapID())}).get();
                if (levelArray.size() > 0) {

                    tv_total_attempt_two.setText(Html.fromHtml(totalAttemptHtml(String.valueOf(levelArray.get(0).getTotal_attempt_level()), String.valueOf(levelArray.get(0).getRight_Answer_level()))));
                    ProgressBarAnimation anim = new ProgressBarAnimation(pbar_level_two, 0, levelArray.get(0).getAccuracy());
                    anim.setDuration(1000);
                    pbar_level_two.startAnimation(anim);
                    tv_accuracy_two.setText("Accuracy " + levelArray.get(0).getAccuracy() + "%");
                } else {
                    tv_total_attempt_two.setText(Html.fromHtml(totalAttemptHtml("0", "0")));
                    ProgressBarAnimation anim = new ProgressBarAnimation(pbar_level_two, 0, 0);
                    anim.setDuration(1000);
                    pbar_level_two.startAnimation(anim);
                    tv_accuracy_two.setText("Accuracy " + "0" + "%");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void getLevelThree() {
        try {
            if (App.mcqdata != null && !TextUtils.isEmpty(App.mcqdata.getSelctedChapID())) {
                levelArray = (ArrayList) new CustomDatabaseQuery(this, new Level()).execute(new String[]{DBNewQuery.get_level_wise_accuraccy("3", App.mcqdata.subObj.getSubjectID(), App.mcqdata.getSelctedChapID())}).get();
                if (levelArray.size() > 0) {
                    tv_total_attempt_three.setText(Html.fromHtml(totalAttemptHtml(String.valueOf(levelArray.get(0).getTotal_attempt_level()), String.valueOf(levelArray.get(0).getRight_Answer_level()))));
                    ProgressBarAnimation anim = new ProgressBarAnimation(pbar_level_three, 0, levelArray.get(0).getAccuracy());
                    anim.setDuration(1000);
                    pbar_level_three.startAnimation(anim);
                    tv_accuracy_three.setText("Accuracy " + levelArray.get(0).getAccuracy() + "%");
                } else {
                    tv_total_attempt_three.setText(Html.fromHtml(totalAttemptHtml("0", "0")));
                    ProgressBarAnimation anim = new ProgressBarAnimation(pbar_level_three, 0, 0);
                    anim.setDuration(1000);
                    pbar_level_three.startAnimation(anim);
                    tv_accuracy_three.setText("Accuracy " + "0" + "%");
                }
            }
        } catch (Exception e) {

            e.printStackTrace();

        }

    }


    public void CastVariabels() {
        mToolbar = findViewById(R.id.toolbar);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        mOverlayView = findViewById(R.id.overlay);
        mImageView = findViewById(R.id.image);
        mScrollView = findViewById(R.id.scroll);
        btn_next = findViewById(R.id.btn_next);
        mFabToolbar = findViewById(R.id.fabtoolbar);
        mainLay = findViewById(R.id.mainLay);
        tv_total_attempt_one = findViewById(R.id.tv_total_attempt_one);
        tv_total_attempt_two = findViewById(R.id.tv_total_attempt_two);
        tv_total_attempt_three = findViewById(R.id.tv_total_attempt_three);
        tv_accuracy_one = findViewById(R.id.tv_accuracy_one);
        tv_accuracy_two = findViewById(R.id.tv_accuracy_two);
        tv_accuracy_three = findViewById(R.id.tv_accuracy_three);
        pbar_level_three = findViewById(R.id.pbar_level_three);
        pbar_level_two = findViewById(R.id.pbar_level_two);
        pbar_level_one = findViewById(R.id.pbar_level_one);
        lin_notappeared = findViewById(R.id.lin_notappeared);
        lin_incorrect = findViewById(R.id.lin_incorrect);
        lin_random = findViewById(R.id.lin_random);


        img_back = findViewById(R.id.img_back);
        mTitleView = findViewById(R.id.tv_title);
        tv_accuracy = findViewById(R.id.tv_accuracy);
        pbar_total_accuracy = findViewById(R.id.pbar_total_accuracy);
        tv_subName = findViewById(R.id.tv_subName);
        chk_not_appeard = findViewById(R.id.chk_not_appeard);
        chk_incorrcet = findViewById(R.id.chk_incorrcet);
        chk_random = findViewById(R.id.chk_random);
        chk_level_one = findViewById(R.id.chk_level_one);
        chk_level_two = findViewById(R.id.chk_level_two);
        chk_level_three = findViewById(R.id.chk_level_three);

        img_sub = findViewById(R.id.img_sub);


        chk_level_one.setOnCheckedChangeListener(onCheckedChangeListener);
        chk_level_two.setOnCheckedChangeListener(onCheckedChangeListener);
        chk_level_three.setOnCheckedChangeListener(onCheckedChangeListener);

        chk_random.setOnCheckedChangeListener(onCheckedChangeListener);
        chk_incorrcet.setOnCheckedChangeListener(onCheckedChangeListener);
        chk_not_appeard.setOnCheckedChangeListener(onCheckedChangeListener);

        fab = findViewById(R.id.fab);
        didTapButton(fab);
    }

    public void didTapButton(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.5, 20);
        myAnim.setInterpolator(interpolator);

        fab.startAnimation(myAnim);
    }

    public String totalAttemptHtml(String attempts, String correct) {
        return "<html>" +
                "<head>" +
                "</head>" +
                "<body>" +
                "Total Appeared Question <strong>" + attempts + "</strong> | Correct  " + correct + "</span> " +
                "</body>" +
                "</html>";
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        if (mScrollView.getCurrentScrollY() == mScrollView.getHeight()) {
            fab.show();
        } else {
            fab.hide();
            mFabToolbar.show();
        }
        float flexibleRange = mFlexibleSpaceImageHeight - mActionBarSize;
        int minOverlayTransitionY = mActionBarSize - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));
    }

    class MyBounceInterpolator implements android.view.animation.Interpolator {
        private double mAmplitude = 1;
        private double mFrequency = 10;

        MyBounceInterpolator(double amplitude, double frequency) {
            mAmplitude = amplitude;
            mFrequency = frequency;
        }

        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) *
                    Math.cos(mFrequency * time) + 1);
        }
    }

    @Override
    public void onDownMotionEvent() {


    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    private CheckBox.OnCheckedChangeListener onCheckedChangeListener
            = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.chk_random:
                    if (isChecked) {
                        chk_incorrcet.setChecked(false);
                        chk_not_appeard.setChecked(false);
                    }

                    break;
                case R.id.chk_incorrcet:
                    if (isChecked)
                        chk_random.setChecked(false);
                    break;
                case R.id.chk_not_appeard:
                    if (isChecked)
                        chk_random.setChecked(false);

                    break;
                case R.id.chk_level_one:
                    if (isChecked) {
                        if (!LeveliDS.contains("1")) {
                            LeveliDS.add("1");
                        }
                    } else {
                        LeveliDS.remove("1");
                    }
                    break;
                case R.id.chk_level_two:
                    if (isChecked) {
                        if (!LeveliDS.contains("2")) {
                            LeveliDS.add("2");
                        }
                    } else {
                        LeveliDS.remove("2");
                    }
                    break;
                case R.id.chk_level_three:
                    if (isChecked) {
                        if (!LeveliDS.contains("3")) {
                            LeveliDS.add("3");
                        }
                    } else {
                        LeveliDS.remove("3");
                    }
                    break;
            }

        }
    };
}