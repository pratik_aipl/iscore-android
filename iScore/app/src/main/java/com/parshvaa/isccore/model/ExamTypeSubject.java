package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by admin on 2/28/2017.
 */
@JsonObject
public class ExamTypeSubject implements Serializable {

    @JsonField
    public String ExamTypeSubjectID;
    @JsonField
    public String ExamTypeID;
    @JsonField
    public String SubjectID;

    public ExamTypeSubject() {

    }

    public ExamTypeSubject(String ExamTypeSubjectID, String ExamTypeID, String SubjectID) {

        this.ExamTypeSubjectID = ExamTypeSubjectID;
        this.ExamTypeID = ExamTypeID;
        this.SubjectID = SubjectID;

    }

    public String getExamTypeSubjectID() {
        return ExamTypeSubjectID;
    }

    public void setExamTypeSubjectID(String examTypeSubjectID) {
        ExamTypeSubjectID = examTypeSubjectID;
    }

    public String getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(String examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }
}
