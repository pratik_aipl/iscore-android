package com.parshvaa.isccore.activity.iconnect.activity;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.iconnect.adapter.CctTestAdapter;
import com.parshvaa.isccore.activity.mcq.PagerListner;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.CctReport;
import com.parshvaa.isccore.model.EvalCctDetail;
import com.parshvaa.isccore.model.EvalSendCCT;
import com.parshvaa.isccore.model.EvaluterMcqTestMain;
import com.parshvaa.isccore.model.EvaluterQuestionOption;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CountDownTimerPausable;
import com.parshvaa.isccore.utils.DialogButtonListener;
import com.parshvaa.isccore.utils.PagerSlidingTabStrip;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.parshvaa.isccore.utils.Utils.getDurationBreakdown;

public class CctTestActivity extends BaseActivity implements PagerListner, AsynchTaskListner {
    public CctTestActivity instance;
    private static final String TAG = "CctTestActivity";
    public static ViewPager viewPager;
    public static PagerSlidingTabStrip pagerTabStri;
    public ImageView img_back;
    public TextView tv_title, tv_attempt, tv_time;
    public Button btn_submit;
    public MyDBManager mDb;
    public CctTestAdapter cctTestAdapter;
    public static ArrayList<Long> timerArray = new ArrayList<>();
    public static ArrayList<McqQuestion> mcqArray = new ArrayList<>();
    public List<String> iDS = new ArrayList<>();
    public CountDownTimer timer;
    public static ArrayList<EvaluterMcqTestMain> EvalMainArray = new ArrayList<>();
    public String start = "", paper_id = "", subject_name = "", StartTime = "", EndTime = "",PublishType;
    long TIME=0;
    public EvaluterMcqTestMain evaMcqTestMain = new EvaluterMcqTestMain();
    public EvaluterQuestionOption option = new EvaluterQuestionOption();
    public int attempted, rightAns, toal_que;
    public EvalSendCCT evalSendCCT;
    public static JsonArray jCctArray;
    public EvalCctDetail evalCctDetail;
    public ArrayList<EvalSendCCT> sendCctArray = new ArrayList<>();
    public String start_time = "";

    CountDownTimerPausable countDownTimerPausable;
    boolean isAlertShow = false;
    boolean isAutoEndTest = false;

    @BindView(R.id.mDisableView)
    View mDisableView;
    @BindView(R.id.lin_time)
    LinearLayout lin_time;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_cct_test);
        Utils.logUser();
        ButterKnife.bind(this);

        instance = this;
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        viewPager = findViewById(R.id.pager);
        pagerTabStri = findViewById(R.id.pager_header);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_attempt = findViewById(R.id.tv_attempt);
        tv_time = findViewById(R.id.tv_time);
        btn_submit = findViewById(R.id.btn_submit);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        EvalMainArray.clear();
        paper_id = getIntent().getExtras().getString("id");
        try {
            int NotificationID = getIntent().getExtras().getInt("NotificationID");
            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NotificationID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        showProgress(true,true);
        new CallRequest(CctTestActivity.this).mcq_view_paper_activity(paper_id);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDb.dbQuery("UPDATE notification SET CctSubmit = 1,isTypeOpen = 1 WHERE ModuleType = 'cct' AND notification_type_id = " + paper_id);
                new AlertDialog.Builder(CctTestActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Alert")
                        .setMessage("Do you really want to submit the test?")
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                submitTest();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
            }
        });


    }

    @Override
    public void onPageVisible(int pos) {
        if (iDS == null) {
            iDS.add(String.valueOf(pos));
            tv_attempt.setText(Html.fromHtml("Attempt<font color=\"#fabb72\"> " + "0" + " </font><font color=\"#0d4568\"> /" + EvalMainArray.size() + "</font>"));
        }
        if (!iDS.contains(pos + "")) {
            iDS.add(String.valueOf(pos));
        }
        tv_attempt.setText(Html.fromHtml("Attempt<font color=\"#fabb72\"> " + iDS.size() + " </font><font color=\"#0d4568\"> /" + EvalMainArray.size() + "</font>"));

    }

    public void setPagerListner() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                pagerTabStri.currentPosition = position;
                pagerTabStri.currentPositionOffset = positionOffset;

                pagerTabStri.scrollToChild(position, (int) (positionOffset * PagerSlidingTabStrip.tabsContainer.getChildAt(position).getWidth()));

                pagerTabStri.invalidate();

                if (pagerTabStri.delegatePageListener != null) {
                    pagerTabStri.delegatePageListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    pagerTabStri.scrollToChild(pagerTabStri.pager.getCurrentItem(), 0);
                }

                if (pagerTabStri.delegatePageListener != null) {
                    pagerTabStri.delegatePageListener.onPageScrollStateChanged(state);
                }
            }

            @Override
            public void onPageSelected(int position) {

                if (pagerTabStri.delegatePageListener != null) {
                    pagerTabStri.delegatePageListener.onPageSelected(position);
                }
            }
        });


        if(PublishType.equalsIgnoreCase("1")){

            lin_time.setVisibility(View.VISIBLE);

            DateFormat formatt = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
            try {
                Date Startdate = formatt.parse(StartTime);
                Date Enddate = formatt.parse(EndTime);
                Log.d(TAG, "startmilli: "+Startdate.getTime());
                Log.d(TAG, "Endmilli: "+Enddate.getTime());
                long Currentmillisecond = new Date().getTime();
                Log.d(TAG, "currentmilli: "+Currentmillisecond);

                TIME = ((Enddate.getTime())-(Currentmillisecond));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            //  if (!TextUtils.isEmpty(paperBean.getTime())) {
            countDownTimerPausable = new CountDownTimerPausable(TIME, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    tv_time.setText("" + getDurationBreakdown(millisUntilFinished));
                }

                @Override
                public void onFinish() {
                    isAutoEndTest = true;
                    mDisableView.setVisibility(View.VISIBLE);
                    if (Utils.isNetworkAvailable(CctTestActivity.this)) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CctTestActivity.this);
                        alertDialogBuilder.setTitle("");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("Your time should be complete.\nPlease click on OK to see your reports.");
                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                submitTest();
                            }
                        }).show();
                    }
                }
            };
            if (countDownTimerPausable.isPaused())
                if (!isAlertShow)
                    countDownTimerPausable.start();

            //}
        }else{
            lin_time.setVisibility(View.INVISIBLE);
        }



    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                showProgress(false, true);

                switch (request) {

                    case mcq_view_paper:
                        JSONObject jObj = new JSONObject(result);

                        Log.d(TAG, "resultsss>>>: "+jObj.toString());
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("data")) {
                                JSONObject jData = jObj.getJSONObject("data");
                                start_time = jObj.getString("start_time");

                                EvalMainArray.clear();
                                if (jData.has("header_data")) {
                                    JSONObject jheaderData = jData.getJSONObject("header_data");
                                    Utils.Log("TAG", "HEADR_DATA::-->" + jheaderData.getString("ClassMCQTestHDRID"));
                                    tv_title.setText(jheaderData.getString("SubjectName"));
                                    subject_name = jheaderData.getString("SubjectName");
                                    StartTime = jheaderData.getString("StartTime");
                                    EndTime = jheaderData.getString("EndTime");
                                    PublishType = jheaderData.getString("PublishType");

                                    JSONArray question_list = jData.getJSONArray("question_list");
                                    if (question_list != null && question_list.length() > 0) {
                                        try {
                                          //  ();
                                            for (int i = 0; i < question_list.length(); i++) {
                                                JSONObject jpaper = question_list.getJSONObject(i);
                                                // MasterorCustomized

                                                evaMcqTestMain = new EvaluterMcqTestMain();
                                                evaMcqTestMain.setClassMCQTestHDRID(jheaderData.getString("ClassMCQTestHDRID"));

                                                evaMcqTestMain.setQuestion(jpaper.getString("Question"));
                                                evaMcqTestMain.setQuestionID(jpaper.getString("QuestionID"));
                                                evaMcqTestMain.setMasterorCustomized(jpaper.getString("MasterorCustomized"));
                                                evaMcqTestMain.setIsAttempt(jpaper.getString("IsAttempt"));
                                                evaMcqTestMain.setClassMCQTestDTLID(jpaper.getString("ClassMCQTestDTLID"));
                                                evaMcqTestMain.setAnswerID(jpaper.getString("AnswerID"));
                                                Utils.Log("TAG", "QUE ID==::-->" + evaMcqTestMain.getQuestionID());


                                                JSONArray jQuestion_optionArray = jpaper.getJSONArray("Question_option");

                                                if (jQuestion_optionArray != null && jQuestion_optionArray.length() > 0) {

                                                    for (int j = 0; j < jQuestion_optionArray.length(); j++) {
                                                        JSONObject jQuestion_option = jQuestion_optionArray.getJSONObject(j);

                                                        option = new EvaluterQuestionOption();
                                                        option.setIsCorrect(jQuestion_option.getString("isCorrect"));
                                                        option.setMCQOPtionID(jQuestion_option.getString("MCQOPtionID"));
                                                        option.setOptions(jQuestion_option.getString("Options"));
                                                        evaMcqTestMain.optionArray.add(option);

                                                        Utils.Log("TAG", "OPTIONS::-->" + evaMcqTestMain.optionArray.get(j).getOptions());
                                                    }

                                                }
                                                EvalMainArray.add(evaMcqTestMain);
                                                Utils.Log("TAG", "QUE::-->" + EvalMainArray.get(i).getQuestion());
                                                //  mDb.insertRow(evaMcqTestMain, "class_evaluater_mcq_test_temp");
                                            }
                                            Utils.Log("TAG", "QUE size::-->" + EvalMainArray.size());
                                            cctTestAdapter = new CctTestAdapter(getSupportFragmentManager(), EvalMainArray.size());

                                            viewPager.setAdapter(cctTestAdapter);
                                            int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
                                            viewPager.setPageMargin(pageMargin);
                                            pagerTabStri.setViewPager(viewPager);

                                            setPagerListner();
                                            cctTestAdapter.notifyDataSetChanged();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                }


                            }
                        }
                        break;

                    case submit_mcq_test:
                        jObj = new JSONObject(result);
                        showProgress(false, true);
                        if (jObj.getBoolean("status") == true) {
                            Utils.showToast("Test submitted successfully", this);
                            App.flags.clear();

                            try {
                                mDb.removeTable("class_evaluater_cct_count");
                                new CallRequest(this).getIcctReport();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Utils.showToast("try again", this);
                            //switchFragment(new EvalTestResultFragment());
                        }
                        break;
                    case getCctReport:
                        showProgress(false, true);
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            try {
                                if (jObj.getJSONArray("data") != null) {
                                    JSONArray jData = jObj.getJSONArray("data");
                                    if (jData != null && jData.length() > 0) {
                                        try {
                                            int total = 0;
                                            mDb.removeTable("class_evaluater_cct_count");
                                            for (int i = 0; i < jData.length(); i++) {
                                                JSONObject obj = jData.getJSONObject(i);
                                                total = total + Integer.parseInt(obj.getString("Total"));
                                                CctReport cctReportObj = new CctReport();
                                                cctReportObj.setSubjectID(obj.getString("SubjectID"));
                                                cctReportObj.setSubjectName(obj.getString("SubjectName"));
                                                cctReportObj.setTotal(obj.getString("Total"));
                                                cctReportObj.setAcuuracy(obj.getString("Acuuracy"));
                                                ////Utils.Log("TAG","IN CCT::->"+cctReportObj.getSubjectID());
                                                mDb.insertRow(cctReportObj, "class_evaluater_cct_count");
                                                switchFragment();
                                            }
                                        } finally {
                                        }
                                    } else {

                                    }
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();

                            }
                        }


                        break;
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void switchFragment() {
        String accuraacy = "";
        try {
            accuraacy = String.valueOf(rightAns * 100 / EvalMainArray.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivity(new Intent(CctTestActivity.this, CctTestResultActivity.class)
                .putExtra("total_q", String.valueOf(EvalMainArray.size()))
                .putExtra("attempted_q", String.valueOf(attempted))
                .putExtra("right_a", String.valueOf(rightAns))
                .putExtra("type", "0")
                .putExtra("accuracy_level", accuraacy)
                .putExtra("subject_name", subject_name)
                .putExtra("wrong_a", String.valueOf(attempted - rightAns))
                .putExtra("start", "start")
                .putExtra("taken_id", paper_id).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();

    }


    public int getRightAnswers() {
        int i = 0;
        for (EvaluterMcqTestMain que : EvalMainArray) {
            if (que.isRightt) ++i;
        }
        return i;
    }

    public int getAttempted() {
        int i = 0;
        for (EvaluterMcqTestMain que : EvalMainArray) {
            if (que.isAttemptedd != -1) ++i;
        }
        return i;
    }

    public void submitTest() {
        showProgress(true, true);//Utils.showProgressDialog(CctTestActivity.this);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }


        sendCctData();
        attempted = getAttempted();
        rightAns = getRightAnswers();
        showProgress(true,true);
        new CallRequest(CctTestActivity.this).submit_mcq_test_activity
                (paper_id,
                        String.valueOf(attempted),
                        String.valueOf(EvalMainArray.size()),
                        String.valueOf(rightAns),
                        jCctArray.toString(), start_time);

    }

    public void sendCctData() {
        evalSendCCT = new EvalSendCCT();
        evalSendCCT.setHeaderID(evaMcqTestMain.getClassMCQTestHDRID());
        for (int i = 0; i < EvalMainArray.size(); i++) {
            //for (EvaluterMcqTestMain que : EvalMainArray) {
            evalCctDetail = new EvalCctDetail();
            evalCctDetail.setDetailID(EvalMainArray.get(i).getClassMCQTestDTLID());
            if (EvalMainArray.get(i).isAttemptedd != -1) {
                evalCctDetail.setIsAttempt("1");
                evalCctDetail.setSelectedAnswerID(EvalMainArray.get(i).getAnswerID());
            } else {
                evalCctDetail.setIsAttempt("0");
                evalCctDetail.setSelectedAnswerID("0");
            }
            evalSendCCT.detailArray.add(evalCctDetail);
        }
        sendCctArray.add(evalSendCCT);

        Gson gson = new GsonBuilder().create();
        jCctArray = gson.toJsonTree(evalSendCCT.detailArray).getAsJsonArray();
        // Utils.Log("mcq ::-->", String.valueOf(jStudHdrArray));
    }

    @Override
    public void onBackPressed() {
        if (countDownTimerPausable != null && !countDownTimerPausable.isPaused())
            countDownTimerPausable.pause();
        Utils.showTwoButtonDialog(this, "Leave Test", "Do you really want to leave test?", "Yes", "No", new DialogButtonListener() {
            @Override
            public void onPositiveButtonClicked() {
                if (App.isNoti) {
                    App.isNoti = false;
                    Intent i = new Intent(getApplicationContext(), DashBoardActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }else{
                    CctTestActivity.super.onBackPressed();
                }
            }

            @Override
            public void onNegativButtonClicked() {
                isAlertShow = false;
                if (countDownTimerPausable != null && countDownTimerPausable.isPaused())
                    countDownTimerPausable.start();
            }
        });

    }

    @Override
    protected void onPause() {
        if (countDownTimerPausable != null && !countDownTimerPausable.isPaused())
            countDownTimerPausable.pause();
        super.onPause();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (countDownTimerPausable != null && countDownTimerPausable.isPaused())
            if (!isAlertShow)
                countDownTimerPausable.start();
    }

}
