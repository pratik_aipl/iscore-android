package com.parshvaa.isccore.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Utils;

public class LegalActivity extends BaseActivity {
    public LegalActivity indtance;
    public ImageView img_back;
    public TextView tv_title, tv_terms, tv_privacy, tv_linces,tv_refund;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_legal);
        Utils.logUser();
        indtance = this;
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_terms = findViewById(R.id.tv_terms);
        tv_privacy = findViewById(R.id.tv_privacy);
        tv_linces = findViewById(R.id.tv_linces);
        tv_refund = findViewById(R.id.tv_refund);
        tv_title.setText("Legal");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        WebView webView = findViewById(R.id.web_legal);
        webView.loadUrl("file:///android_asset/Licenses&Copyrights.html");


        tv_linces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                .putExtra("action","l"));
            }
        });
        tv_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                        .putExtra("action","p"));
            }
        });
        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                        .putExtra("action","t"));
            }
        });
        tv_refund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                        .putExtra("action","r"));
            }
        });
    }
}
