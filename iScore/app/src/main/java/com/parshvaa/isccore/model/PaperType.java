package com.parshvaa.isccore.model;

import java.io.Serializable;

public class PaperType implements Serializable {
    public String paperName ;
    public int paperIcon;
    public PaperType() {

    }
    public PaperType(String paperName,int paperIcon) {
        this.paperName = paperName;
        this.paperIcon = paperIcon;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public int getPaperIcon() {
        return paperIcon;
    }

    public void setPaperIcon(int paperIcon) {
        this.paperIcon = paperIcon;
    }
}
