package com.parshvaa.isccore.activity.iconnect.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.iconnect.adapter.IMcqTestAdapter;
import com.parshvaa.isccore.activity.iconnect.EMcqPaper;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.observscroll.BaseFragment;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */

public class IMcqTestFragment extends BaseFragment implements SelectSubjectDrawer.SubjectArrayListListener, AsynchTaskListner {

    public View v;
    Button left_button;
    public SelectSubjectDrawer left_drawer;
    public RelativeLayout mainLay, mainLayout;
    public DrawerLayout drawerLayout;
    public RecyclerView recycler_mcq_sub;
    private IMcqTestAdapter adapter;
    public JsonParserUniversal jParser;
    public ArrayList<Subject> EvaSubjectArray = new ArrayList<>();
    public EMcqPaper emp;
    public ArrayList<EMcqPaper> paperArray = new ArrayList<>();
    private LinearLayout emptyView;
    public TextView tv_empty;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_imcq_test, container, false);
        jParser = new JsonParserUniversal();

        mainLay = v.findViewById(R.id.mainLay);
        mainLayout = v.findViewById(R.id.mainLayout);
        left_button = v.findViewById(R.id.left_button);
        drawerLayout = v.findViewById(R.id.drawer_layout);
        recycler_mcq_sub = v.findViewById(R.id.recycler_mcq_sub);
        emptyView = v.findViewById(R.id.empty_view);
        tv_empty = v.findViewById(R.id.tv_empty);

        emptyView.setVisibility(View.GONE);
        left_drawer = (SelectSubjectDrawer) getActivity().getFragmentManager().findFragmentById(R.id.fragment_subject_drawer);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recycler_mcq_sub.addItemDecoration(new ItemOffsetDecoration(spacing));
        //   new CallRequest(IMcqTestFragment.this).get_recent_mcq_paper();
        showProgress(true);
        new CallRequest(IMcqTestFragment.this).get_evaluator_subjects("mcq");

        return v;

    }

    @Override
    public void onResume() {
        super.onResume();
        showProgress(true);
        new CallRequest(IMcqTestFragment.this).get_recent_mcq_paper();
    }


    private void setupRecyclerView() {
        final Context context = recycler_mcq_sub.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_mcq_sub.setLayoutAnimation(controller);
        recycler_mcq_sub.scheduleLayoutAnimation();
        recycler_mcq_sub.setLayoutManager(new LinearLayoutManager(context));
        adapter = new IMcqTestAdapter(paperArray, context, "0");
        recycler_mcq_sub.setAdapter(adapter);

    }


    @Override
    public void onDrawerItemSelected(ArrayList<String> subjectId) {
        showProgress(true);
        String SubjectId = android.text.TextUtils.join(",", subjectId);
        new CallRequest(IMcqTestFragment.this).get_subject_mcq_pape(SubjectId);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case get_evaluator_subjects:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                EvaSubjectArray.clear();
                                EvaSubjectArray.addAll(LoganSquare.parseList(jObj.getJSONArray("data").toString(), Subject.class));
                                if (drawerLayout != null && left_button != null && mainLayout != null && left_drawer != null) {
                                    left_drawer.setUpButton(R.id.fragment_left_drawer, drawerLayout, left_button, mainLayout, EvaSubjectArray);
                                    left_drawer.setDDrawerListener(IMcqTestFragment.this);
                                }
                                showProgress(false);
                            } else {
                                Utils.showToast(jObj.getString("message"), getActivity());
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getActivity());
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case get_recent_mcq_paper:

                    paperArray.clear();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            recycler_mcq_sub.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);

                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {
                                paperArray.clear();
                                paperArray.addAll(LoganSquare.parseList(jDataArray.toString(), EMcqPaper.class));
                                setupRecyclerView();
                                showProgress(false);
                            }

                        } else {
                            showProgress(false);
                            tv_empty.setText(jObj.getString("message"));
                            recycler_mcq_sub.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        showProgress(false);
                        tv_empty.setText("message");
                        recycler_mcq_sub.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case get_subject_mcq_pape:
                    showProgress(false);
                    paperArray.clear();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            recycler_mcq_sub.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);


                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {
                                paperArray.addAll(LoganSquare.parseList(jDataArray.toString(), EMcqPaper.class));
                                adapter = new IMcqTestAdapter(paperArray, recycler_mcq_sub.getContext(), "0");
                                recycler_mcq_sub.setAdapter(adapter);
                            }
                        } else {
                            tv_empty.setText(jObj.getString("message"));
                            recycler_mcq_sub.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        showProgress(false);
                        tv_empty.setText("message");
                        recycler_mcq_sub.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().finish();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}

