package com.parshvaa.isccore.model;

import java.io.Serializable;

public class EvalCctDetail implements Serializable {
        public String DetailID = "", SelectedAnswerID = "", isAttempt = "";

    public String getDetailID() {
        return DetailID;
    }

    public void setDetailID(String detailID) {
        DetailID = detailID;
    }

    public String getSelectedAnswerID() {
        return SelectedAnswerID;
    }

    public void setSelectedAnswerID(String selectedAnswerID) {
        SelectedAnswerID = selectedAnswerID;
    }

    public String getIsAttempt() {
        return isAttempt;
    }

    public void setIsAttempt(String isAttempt) {
        this.isAttempt = isAttempt;
    }
}