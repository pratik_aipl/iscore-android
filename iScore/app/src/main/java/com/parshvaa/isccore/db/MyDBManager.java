package com.parshvaa.isccore.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.parshvaa.isccore.db.DBConstant.DATABASE_VERSION;


public class MyDBManager extends SQLiteOpenHelper {

    private static final String TAG = "DATABASE";
    static MyDBManager sInstance;

    public SQLiteDatabase myDb;
    public Context myContext;


    public MyDBManager(Context context) {
        super(context, DBConstant.DB_NAME, null, DATABASE_VERSION);
        this.myContext = context;
    }


    public static synchronized MyDBManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MyDBManager(context);
        }
        return sInstance;
    }


    public boolean isDbOpen() {
        if (myDb == null)
            return false;
        return myDb.isOpen();
    }

    public MyDBManager open(Context context) throws SQLException {
        myDb = getWritableDatabase();
        return this;
    }

    private boolean checkDataBase() {

        try {
            if (myDb != null && isDbOpen()) {
                return true;
            }
            myDb = SQLiteDatabase.openOrCreateDatabase(getDatabasePath(DBConstant.DB_NAME), null);
            return true;
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        }


    }

    public void close() {
        if (myDb != null && myDb.isOpen())
            myDb.close();
    }

    public String getDatabasePath(String name) {

        String dbFile = myContext.getPackageCodePath() + File.separator + name;
        //   String dbfile = Constant.LOCAL_IMAGE_PATH + File.separator + name;
        File result = new File(dbFile);
        if (!result.getParentFile().exists()) {
            result.getParentFile().mkdirs();
        }
        if (Log.isLoggable(TAG, Log.WARN)) {
            Log.w(TAG, "getDatabasePath(" + name + ") = " + result.getAbsolutePath());
        }

        return result.getAbsolutePath();
    }

    public void createTable(HashMap<String, String> columns, String tableName) {
        String query = "create table if not EXISTS " + tableName + " (";
        List<String> dataType = new ArrayList<String>(columns.values());
        List<String> names = new ArrayList<String>(columns.keySet());
        for (int i = 0; i < names.size(); i++) {
            query += names.get(i) + " " + dataType.get(i);
            if (i < names.size() - 1) {
                query += ",";
            }
        }
        query += ")";
        dbPreparedQuery(query);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.myDb = sqLiteDatabase;

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean dbQuery(String query) {
        try {
            //   Utils.Log(" Query :  ==>", query);
            query = query.replaceAll("\\p{M}", "");
            myDb.execSQL(query);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean shouldInclude(String name) {

        return !name.equalsIgnoreCase("isRight")
                && !name.equalsIgnoreCase("selectedAns");

    }

    public String replaceName(String name) {

        if (name.equalsIgnoreCase("TempMCQTestHDRID")) {
            return "StudentMCQTestHDRID";
        } else {
            return name;
        }

    }


    public long insertRow(Object o, String tableName) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();
                    if (filed.getName().equalsIgnoreCase("CreatedOn")) {
                        values.put(replaceName(filed.getName()), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                    } else {
                        if (filed.getType().isAssignableFrom(String.class)) {
                            if (filed.get(o) == null) {
                                values.put(replaceName(filed.getName()), "");
                            } else {
                                values.put(replaceName(filed.getName()), (String) filed.get(o));
                            }
                        }
                        if (type == int.class) {
                            if ((int) filed.get(o) != -1) {
                                values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                            }
                        }
                    }

                }
                if (myDb.isOpen())
                    return myDb.insert(tableName, null, values);
            }

            if (tableName.equals(Constant.class_evaluater_cct_count)) {
                DBTables dbTables = new DBTables();
                dbTables.class_evaluater_cct_count(this);
                if (isTableExists(tableName)) {
                    ContentValues values = new ContentValues();
                    c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                    Field[] fields = c.getFields();
                    for (Field filed : fields) {
                        Object type = filed.getType();
                        if (filed.getName().equalsIgnoreCase("CreatedOn")) {
                            values.put(replaceName(filed.getName()), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        } else {
                            if (filed.getType().isAssignableFrom(String.class)) {
                                if (filed.get(o) == null) {
                                    values.put(replaceName(filed.getName()), "");
                                } else {
                                    values.put(replaceName(filed.getName()), (String) filed.get(o));
                                }
                            }
                            if (type == int.class) {
                                if ((int) filed.get(o) != -1) {
                                    values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                                }
                            }
                        }

                    }
                    if (myDb.isOpen())
                        return myDb.insert(tableName, null, values);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return -1;
    }

    public long insertWebRow(Object o, String tableName) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();
                    if (filed.getType().isAssignableFrom(String.class)) {
                        if (filed.get(o) == null) {
                            values.put(replaceName(filed.getName()), "");
                        } else {
                            values.put(replaceName(filed.getName()), (String) filed.get(o));
                        }
                    }
                    if (type == int.class) {
                        if ((int) filed.get(o) != -1) {
                            values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                        }
                    }
                }
                String query = "Select * From " + tableName + " where " + fields[2].getName() + " = '" + fields[2].get(o) + "'";
                Log.d(TAG, "insertWebRow: " + query);
//                if (myDb.rawQuery("Select * From " + tableName + " where " + fields[2].getName() + "=?", new String[]{String.valueOf(fields[2].get(o))}).getCount() > 0) {
////                    Toast.makeText(myContext, "Already Exist!", Toast.LENGTH_SHORT).show();
//                } else {
                return myDb.insert(tableName, null, values);
//                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return -1;
    }

    public boolean CheckIsDataAlreadyInDBorNot(String TableName,
                                               String dbfield, String fieldValue) {
        String Query = "Select * from " + TableName + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = myDb.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public long insertRowWithHelper(Object o, String tableName) {
        Class c;
        try {
            ContentValues values = new ContentValues();
            c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
            Field[] fields = c.getFields();

            String statement = " insert into " + tableName + " (";


            for (Field f : fields) {
                if (f.getType().isAssignableFrom(String.class)) {

                    statement += f.getName() + ",";
                } else if (f.getType() == int.class) {

                    statement += f.getName() + ",";
                }
            }
            statement = statement.substring(0, statement.length() - 1);
            statement += ") VALUES (";
            for (int i = 0; i < fields.length; i++) {
                statement += "?,";
            }
            statement = statement.substring(0, statement.length() - 1);
            //Utils.Log("TABLE  QUERY :", statement);

            for (Field filed : fields) {
                Object type = filed.getType();


                if (filed.getName().equalsIgnoreCase("CreatedOn")) {
                    values.put(replaceName(filed.getName()), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                } else {
                    if (filed.getType().isAssignableFrom(String.class)) {
                        if (filed.get(o) == null) {
                            values.put(replaceName(filed.getName()), "");
                        } else {
                            values.put(replaceName(filed.getName()), (String) filed.get(o));
                        }
                    }
                    if (type == int.class) {
                        if ((int) filed.get(o) != -1) {
                            //  Utils.Log("TAG  " + tableName + " inserted integer  :: ", filed.getName() + " :-> " + (Integer) filed.get(o));
                            values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                        }
                    }
                }
            }
            return myDb.insert(tableName, null, values);

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }


    }

    public void insertTempRow(Object o, String tableName) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();
                    if (filed.getType().isAssignableFrom(String.class)) {
                        values.put(filed.getName(), (String) filed.get(o));
                    }
                    if (filed.getType().isAssignableFrom(Integer.class)) {
                        if ((Integer) filed.get(o) != -1)
                            values.put(filed.getName(), (Integer) filed.get(o));
                    }
                }
                myDb.insert(tableName, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void updateRow(Object o, String tableName, String tableQuery) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();
                    if (filed.getType().isAssignableFrom(String.class)) {
                        values.put(filed.getName(), (String) filed.get(o));
                    }
                    if (filed.getType().isAssignableFrom(Integer.class)) {
                        values.put(filed.getName(), (Integer) filed.get(o));
                    }
                }
                myDb.update(tableName, values, tableQuery, null);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }


    public void emptyTable(String tableName) {
        Log.d(TAG, "emptyTable:" + tableName + "--> " + isTableExists(tableName));
        if (isTableExists(tableName))
            myDb.delete(tableName, null, null);
//            removeTable(tableName);
    }

    public void removeTable(String tableName) {
        if (isTableExists(tableName)) {
            String query = "DELETE FROM " + tableName;
            myDb.execSQL(query);
        }
    }

    public boolean dbPreparedQuery(String query) {
        try {
            myDb.execSQL(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    private boolean checkForTableExists(SQLiteDatabase db, String table) {
        String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + table + "'";
        Cursor mCursor = db.rawQuery(sql, null);
        if (mCursor.getCount() > 0) {
            return true;
        }
        mCursor.close();
        return false;
    }

    private boolean fieldExists(String p_query) {
        Cursor mCursor = myDb.rawQuery(p_query, null);
        if ((mCursor != null) && (mCursor.moveToFirst())) {
            mCursor.close();
            return true;
        }
        mCursor.close();
        return false;
    }

    public Cursor getAllRows(String query) {
        if (myDb != null && !myDb.isOpen()) {
            open(myContext);
        }
        return myDb.rawQuery(query, null);
    }

    public int getLastRecordID(String tablename, String columnName) {
        String countQuery = "SELECT  " + columnName + " FROM " + tablename + "  ORDER BY " + columnName + " DESC Limit 1";
        Cursor cursor = myDb.rawQuery(countQuery, null);
        if (cursor != null && cursor.moveToNext()) {
            int data = cursor.getInt(0);
            cursor.close();
            return data;
        } else {
            cursor.close();
            return -1;
        }
    }

    public int getLastRecordIDForRequest(String tabelname, String columnName) {
        String countQuery = "SELECT  " + columnName + " FROM " + tabelname + "  ORDER BY " + columnName + " DESC Limit 1";
        Log.d(TAG, "getLastRecordIDForRequest: " + isTableExists(tabelname));
        if (!isTableExists(tabelname)) {
            return -1;
        }
        Log.d(TAG, "getLastRecordIDForRequest: " + countQuery);
        Cursor cursor = myDb.rawQuery(countQuery, null);
        if (cursor != null && cursor.moveToNext()) {
            int data = cursor.getInt(0);
            cursor.close();
            return data;
        } else {
            Log.d(TAG, "getLastRecordIDForRequest: else ");
            if (cursor != null)
                cursor.close();
            return -1;
        }
    }


    public int getTotalUsingJoinQuery(String countQuery) {
        Cursor cursor = myDb.rawQuery(countQuery, null);
        if (cursor != null && cursor.moveToNext()) {
            int data = cursor.getInt(0);
            cursor.close();
            return data;
        } else {
            if (cursor != null)
                cursor.close();
            return -1;
        }
    }

    public int getAllRecordsUsingQuery(String query) {
        try {
            Cursor cursor = myDb.rawQuery(query, null);
            int cnt = cursor.getCount();
            cursor.close();
            return cnt;
        } catch (Exception e) {
            e.printStackTrace();

            return -1;
        }
    }

    public int getAllReports(String query) {
        try {
            Cursor cursor = myDb.rawQuery(query, null);
            int cnt = cursor.getCount();
            cursor.close();
            return cnt;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }


    public int getAllRecords(String TABLE_NAME) {
        if (isTableExists(TABLE_NAME)) {
            String countQuery = "SELECT  * FROM " + TABLE_NAME;
            Cursor cursor = myDb.rawQuery(countQuery, null);
            int cnt = cursor.getCount();
            cursor.close();
            return cnt;
        } else {
            return 0;
        }

    }

    public int getTableCount(String TABLE_NAME) {
        Cursor c = myDb.rawQuery("select * from " + TABLE_NAME, null);
        return c.getCount();
    }

    public boolean isTableExists(String tableName) {
//        myDb.rawQuery("DROP TABLE IF EXISTS " + tableName, null);
        if (tableName == null || myDb == null || !myDb.isOpen()) {
            return false;
        }
        Cursor cursor = myDb.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
        if (!cursor.moveToFirst()) {
            cursor.close();
            return false;
        }
        Log.d(TAG, "isTableExists: " + tableName);
        int count = cursor.getCount();
        Log.d(TAG, "isTableExists: " + count);
        cursor.close();
        return count > 0;
    }

}

