package com.parshvaa.isccore.activity.library.videoPleyar.utility;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import androidx.annotation.AttrRes;
import android.util.Log;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author Aidan Follestad (halilibo)
 */
public class Util {
    private static final String TAG = "Util";

    public static int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static String getDurationString(long durationMs, boolean negativePrefix) {
        long hours = TimeUnit.MILLISECONDS.toHours(durationMs);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(durationMs);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(durationMs);
        if (hours > 0) {
            return String.format(Locale.getDefault(), "%s%02d:%02d:%02d",
                    negativePrefix ? "-" : "",
                    hours,
                    minutes - TimeUnit.HOURS.toMinutes(hours),
                    seconds - TimeUnit.MINUTES.toSeconds(minutes));
        }
        return String.format(Locale.getDefault(), "%s%02d:%02d",
                negativePrefix ? "-" : "",
                minutes,
                seconds - TimeUnit.MINUTES.toSeconds(minutes)
        );
    }

    public static boolean isColorDark(int color) {
        double darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
        return darkness >= 0.5;
    }

    public static int adjustAlpha(int color, @SuppressWarnings("SameParameterValue") float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public static int resolveColor(Context context, @AttrRes int attr) {
        return resolveColor(context, attr, 0);
    }

    public static int resolveColor(Context context, @AttrRes int attr, int fallback) {
        TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{attr});
        try {
            return a.getColor(0, fallback);
        } finally {
            a.recycle();
        }
    }

    public static Drawable resolveDrawable(Context context, @AttrRes int attr) {
        return resolveDrawable(context, attr, null);
    }

    private static Drawable resolveDrawable(Context context, @AttrRes int attr, @SuppressWarnings("SameParameterValue") Drawable fallback) {
        TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{attr});
        try {
            Drawable d = a.getDrawable(0);
            if (d == null && fallback != null)
                d = fallback;
            return d;
        } finally {
            a.recycle();
        }
    }

    public static void downloadSubjectIcon(Context mContext, AQuery aQuery, String imgUrl, ImageView img_sub, File image, int error_icon) {

        if (Utils.isNetworkAvailable(mContext))
            Utils.setImageSubject(mContext, imgUrl, img_sub);

        aQuery.ajax(imgUrl, Bitmap.class, 0, new AjaxCallback<Bitmap>() {
            @Override
            public void callback(String url, Bitmap bitmap, AjaxStatus status) {
                super.callback(url, bitmap, status);
                try {
                    File dir = new File(image.getParent());
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    image.createNewFile();
                    String filePath = image.getAbsolutePath();
                    FileOutputStream ostream = new FileOutputStream(image);
                    String imgName = image.getName();
                    if (imgName.contains(".jpg") || imgName.contains(".JPG") || imgName.contains(".jpeg") || imgName.contains(".JPEG")) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                    } else {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                    }
                    ostream.close();
                    if (bitmap != null)
                        bitmap.recycle();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                    } else {
                        MediaScannerConnection.scanFile(mContext, new String[]{filePath}, null, new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                Log.i("ExternalStorage", "-> uri=" + uri);
                            }
                        });

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (img_sub != null && image.exists())
                    Utils.setImage(mContext, image, img_sub,error_icon);


//                    img_sub.setImageBitmap(bitmap);
            }
        });
    }

    public static void downloadClassIcon(Context mContext, AQuery aQuery, String imgUrl, ImageView img_sub, File image, int error_icon) {

        if (Utils.isNetworkAvailable(mContext))
            Utils.setImage(mContext, imgUrl, img_sub,error_icon);

        aQuery.ajax(imgUrl, Bitmap.class, 0, new AjaxCallback<Bitmap>() {
            @Override
            public void callback(String url, Bitmap bitmap, AjaxStatus status) {
                super.callback(url, bitmap, status);
                try {
                    File dir = new File(image.getParent());
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    image.createNewFile();
                    String filePath = image.getAbsolutePath();
                    FileOutputStream ostream = new FileOutputStream(image);
                    String imgName = image.getName();
                    if (imgName.contains(".jpg") || imgName.contains(".JPG") || imgName.contains(".jpeg") || imgName.contains(".JPEG")) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                    } else {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                    }
                    ostream.close();
                    if (bitmap != null)
                        bitmap.recycle();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                    } else {
                        MediaScannerConnection.scanFile(mContext, new String[]{filePath}, null, new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                Log.i("ExternalStorage", "-> uri=" + uri);
                            }
                        });

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (img_sub != null && image.exists())
                    Utils.setImage(mContext, image, img_sub,error_icon);


//                    img_sub.setImageBitmap(bitmap);
            }
        });
    }
}