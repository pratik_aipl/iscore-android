package com.parshvaa.isccore.activity.iconnect.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class IPreviewPaperActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "IPreviewPaperActivity";
    public String HTML = "";
    public WebView webpdf;
    public String paper_id = "",UPLOADSTARTTIME,UPLOADENDTIME,STARTTIME,TYPE,FROM,
    ENDTIME;
    public ImageView img_back,img_home,img_rotation;
    public TextView tv_title;
    public FloatingActionButton imgprint;
    long milli;
    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_preview_pdf);
        Utils.logUser();

        imgprint = findViewById(R.id.imgprint);
        img_rotation = findViewById(R.id.img_rotation);
        img_home = findViewById(R.id.img_home);
        img_home.setImageResource(R.drawable.addimg);
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });

        webpdf = findViewById(R.id.pdfView);
        webpdf.getSettings().setJavaScriptEnabled(true);
        webpdf.setWebViewClient(new myWebClient());
        webpdf.setBackgroundColor(Color.TRANSPARENT);
        WebSettings settings = webpdf.getSettings();
        //settings.setMinimumFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setDomStorageEnabled(true);
        tv_title = findViewById(R.id.tv_title);
        img_back = findViewById(R.id.img_back);

        tv_title.setText("Preview");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (getIntent().hasExtra("privew")) {
          //  imgprint.setBackgroundResource(R.drawable.addimg);
            imgprint.setVisibility(View.GONE);
            paper_id = getIntent().getExtras().getString("paper_id");
            FROM = getIntent().getExtras().getString(Constant.FROM);

            showProgress(true, true);
            new CallRequest(IPreviewPaperActivity.this).view_paper_activity(paper_id);


            if(FROM.equalsIgnoreCase("TESTPAPER")){
                UPLOADSTARTTIME = getIntent().getExtras().getString(Constant.UPLOADSTARTTIME);
                UPLOADENDTIME = getIntent().getExtras().getString(Constant.UPLOADENDTIME);
                STARTTIME = getIntent().getExtras().getString(Constant.STARTTIME);
                ENDTIME = getIntent().getExtras().getString(Constant.ENDTIME);
                TYPE = getIntent().getExtras().getString(Constant.TYPE);
                Log.d(TAG, "TYPE: "+TYPE);
                DateFormat formatt = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
                try {
                    Date Startdate = formatt.parse(STARTTIME);
                    Date Enddate = formatt.parse(ENDTIME);
                    long Currentmillisecond = new Date().getTime();

                    Log.d(TAG, "startmilli: "+Startdate.getTime());
                    Log.d(TAG, "Endmilli: "+Enddate.getTime());
                    Log.d(TAG, "currentmilli: "+Currentmillisecond);
                    milli=Enddate.getTime()-Currentmillisecond;

              /*  if(Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                    imgprint.setVisibility(View.VISIBLE);
                }else{
                    imgprint.setVisibility(View.GONE);
                }*/
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                new CountDownTimer(milli,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }
                    @Override
                    public void onFinish() {
                        showAlertback("Time Finished...!");
                    }
                }.start();
                img_home.setVisibility(View.VISIBLE);
            }else{
                img_home.setVisibility(View.GONE);

            }

        } else {
            paper_id = getIntent().getExtras().getString("paper_id");
            showProgress(true, true);
            imgprint.setVisibility(View.VISIBLE);
            new CallRequest(IPreviewPaperActivity.this).view_model_answer(paper_id);
        }
        img_rotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });
        imgprint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if (getIntent().hasExtra("privew")) {
                    Toast.makeText(IPreviewPaperActivity.this, "Call", Toast.LENGTH_SHORT).show();
                }else{
                    printPDF();
                }

            }
        });

    }
    public void showAlertback(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(IPreviewPaperActivity.this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        startActivity(new Intent(IPreviewPaperActivity.this, TestPaperActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public void gotoDashboard() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("Are You Sure You want To Upload Paper ?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    showProgress(true, true);//Utils.showProgressDialog(instance);
                    startActivity(new Intent(this, UploadPaperImagesActivity.class)
                            .putExtra(Constant.PAPERID,paper_id)
                            .putExtra(Constant.TYPE,TYPE));
                    finish();


                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                showProgress(false, true);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.d(TAG, "pdfview: "+result);
            switch (request) {
                case view_paper:
                    //showProgress(false)();
                    showProgress(false, true);
                    try {

                        if (result.contains("true")) {
                            HTML = result.substring(result.indexOf(",\"data\":\"") + 9, result.length() - 2);
                            HTML = HTML.replace("\\t", "");
                            HTML = HTML.replace("\\r", "");
                            HTML = HTML.replace("\\n", "");
                            HTML = HTML.replace("\\\"", "\"");
                            /*HTML = HTML.replace("\\t", "");
                            HTML = HTML.replace("\\r", "");
                            HTML = HTML.replace("\\n", "");
                            HTML = HTML.replace("\\/", "/");
                            HTML = HTML.replace("\\\"","&quot;");*/


                            //settings.setDefaultTextEncodingName("UTF-8");
                            webpdf.setWebViewClient(new myWebClient());
                            webpdf.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
                        } else {
                            //result.substring(result.indexOf("\"message\":\""), result.indexOf("\""));
                            Utils.showToast(result.substring(result.indexOf("\"message\":\"") + 12, result.indexOf("\",\"data\"")), this);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;

                case view_model_answer:
                    showProgress(false, true);
                    try {
                        if (result.contains("true")) {
                            HTML = result.substring(result.indexOf(",\"data\":\"") + 9, result.length() - 2);
                            HTML = HTML.replace("\\t", "");
                            HTML = HTML.replace("\\r", "");
                            HTML = HTML.replace("\\n", "");
                            HTML = HTML.replace("\\\"", "\"");
                            Utils.Log("TAG", "HTML::-->" + HTML);
                            webpdf.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
                        } else {
                            //result.substring(result.indexOf("\"message\":\""), result.indexOf("\""));
                            Utils.showToast(result.substring(result.indexOf("\"message\":\"") + 12, result.indexOf("\",\"data\"")), this);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void printPDF() {
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = webpdf.createPrintDocumentAdapter();
        String jobName = getString(R.string.app_name) + " Report "
                + System.currentTimeMillis();
        PrintAttributes printAttrs = new PrintAttributes.Builder().
                setColorMode(PrintAttributes.COLOR_MODE_MONOCHROME).
                setMediaSize(PrintAttributes.MediaSize.NA_LETTER.asLandscape()).
                setMinMargins(new PrintAttributes.Margins(50, 0, 50, 0)).
                build();
        PrintJob printJob = printManager.print(jobName, printAdapter,
                printAttrs);
        //7mPrintJobs.add(printJob);
    }

}
