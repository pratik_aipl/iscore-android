package com.parshvaa.isccore.utils;

import android.content.Context;
import androidx.fragment.app.Fragment;
import android.util.Log;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.ImageDataModel;
import com.parshvaa.isccore.model.MainUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class CallRequest extends Exception {
    private static final String TAG = "CallRequest";
    public static String URL = Constant.BASIC_URL + Constant.URL_VERSION + "web_services/";
    public static String VIEW_PAPER_EVALUTER_URL = Constant.BASIC_VIEW_PAPER_EVA_URL + "class-admin/web-services-paper/";
    public static String IURL = Constant.BASIC_URL + Constant.URL_VERSION + "evaluator_web_services/";


    public App app;
    public Context ct;
    public Fragment ft;

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public void forgot_pass(String type, String email) {


        System.out.println("In forgot Pass");
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL);
        map.put("type", type);
        map.put("action", "forgotPassword");
        map.put("email", email);
        Log.d(TAG, "forgot_pass: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.forgot_pw, Constant.POST_TYPE.GET, map);
    }

    public void get_updated_masterquetion() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_updated_masterquetion/?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_updated_masterquetion: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_updated_masterquetion, Constant.POST_TYPE.GET, map);
    }


    //------------------  Login,  Register, OTP   Start------------------//

    public void getLogin(String register_mobile, String guid, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "login?");
        map.put("register_mobile", register_mobile);
        map.put("imei_number", guid);
        map.put("device_id", "");
        map.put("player_id", PleyaerID);
        map.put("device_type", "android");
        Log.d(TAG, "getLogin: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getLogin, Constant.POST_TYPE.GET, map);
    }

    public void checkKey(String key, String guid, String device_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "check_key?");
        map.put("app_key", key);
        map.put("imei_number", guid);
        map.put("device_id", "");
        map.put("device_type", "android");
        Log.d(TAG, "checkKey: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.checkKey, Constant.POST_TYPE.GET, map);
    }

    public void change_class_key(String key) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "change_class_key?");
        map.put("app_key", key);
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "change_class_key: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.change_class_key, Constant.POST_TYPE.GET, map);
    }

    public void send_otp(String mobile, String email) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "send-otp?");
        map.put("mobile_number", mobile);
        map.put("email", email);
        Log.d(TAG, "send_otp: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.send_otp, Constant.POST_TYPE.GET, map);
    }

    public void regKeySendOtp(String mobile, String email) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "send-otp?");
        map.put("StudID", MainUser.getStudentId());
        map.put("mobile_number", mobile);
        map.put("email", email);
        Log.d(TAG, "regKeySendOtp: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.send_otp, Constant.POST_TYPE.GET, map);
    }

    public void resend_otp(String mobile) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "resend_otp?");
        map.put("mobile_number", mobile);
        Log.d(TAG, "resend_otp: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.resend_otp, Constant.POST_TYPE.GET, map);
    }

    public void register(String Name, String surname, String mobile, String email, String board, String medium, String standaerd, String bachID, String ClassID, String branchID, String RegKey,String SchoolName, String isDemo, String imei_number, String image, String UserType, String LocationDetails, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "register?");
        map.put("StudID", MainUser.getStudentId());
        map.put("first_name", Name);
        map.put("last_name", surname);
        map.put("register_mobile", mobile);
        map.put("email", email);
        map.put("board_id", board);
        map.put("medium_id", medium);
        map.put("standard_id", standaerd);
        map.put("batch_id", bachID);
        map.put("class_id", ClassID);
        map.put("branch_id", branchID);
        map.put("RegKey", RegKey);
        map.put("SchoolName", SchoolName);
        map.put("KeyType", isDemo);
        map.put("imei_number", imei_number);
        map.put("image", image);
        map.put("UserType", UserType);
        map.put("LocationDetails", LocationDetails);
        map.put("player_id", PleyaerID);
        Log.d(TAG, "register: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.register, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void directRegister(String Name, String surname, String mobile, String SchoolName, String email, String board, String medium, String standaerd, String bachID, String ClassID, String branchID, String RegKey, String imei_number, String image, String UserType, String LocationDetails, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "direct_register?");
        map.put("first_name", Name);
        map.put("last_name", surname);
        map.put("register_mobile", mobile);
        map.put("SchoolName", SchoolName);
        map.put("email", email);
        map.put("board_id", board);
        map.put("medium_id", medium);
        map.put("standard_id", standaerd);
        map.put("batch_id", bachID);
        map.put("class_id", ClassID);
        map.put("branch_id", branchID);
        map.put("StudID", RegKey);
        map.put("imei_number", imei_number);
        map.put("image", image);
        map.put("UserType", UserType);
        map.put("LocationDetails", LocationDetails);
        map.put("player_id", PleyaerID);
        Log.d(TAG, "directRegister: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.direct_register, Constant.POST_TYPE.POST_WITH_IMAGE
                , map);
    }

    public void paymentRegister(String board, String medium, String standaerd,
                                String ClassID, String TransactionID, String OrderID, String Amount, String Discount, String TotalAmount, String Response,
                                String codeOwner,String ReferalCode) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "subscribe?");
        map.put("StudID", MainUser.getStudentId());
        map.put("BoardID", board);
        map.put("MediumID", medium);
        map.put("StandardID", standaerd);
        map.put("ClassID", ClassID);
        map.put("TransactionID", TransactionID);
        map.put("OrderID", OrderID);
        map.put("Amount", Amount);
        map.put("Discount", Discount);
        map.put("TotalAmount", TotalAmount);
        map.put("Response", Response);
        map.put("CodeOwner", codeOwner);
        map.put("RefralCode", ReferalCode);
        Log.d(TAG, "paymentRegister: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.subscribe, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }
    public void paymentEvent(String mobile_no, String email, String EventID,
                             String TransactionID, String OrderID,
                             String Amount, String Discount, String TotalAmount, String Response,String PromoCode) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "EventSubscribe");
        map.put("mobile_no", mobile_no);
        map.put("email", email);
        map.put("EventID", EventID);
        map.put("StudentID", MainUser.getStudentId());
        map.put("TransactionID", TransactionID);
        map.put("OrderID", OrderID);
        map.put("Amount", Amount);
        map.put("Discount", Discount);
        map.put("TotalAmount", TotalAmount);
        map.put("Response", Response);
        map.put("PromoCode", PromoCode);
        map.put("Type", "iScore");
        Log.d(TAG, "paymentRegister: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.EventSubscribe, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void update_last_sync_time(String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "update_last_sync_time?");
        map.put("StudID", MainUser.getStudentId());
        map.put("player_id", PleyaerID);
        Log.d(TAG, "update_last_sync_time: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.update_last_sync_time, Constant.POST_TYPE.GET, map);
    }


    public void verify_bord_medium_standard(String board, String medium, String standaerd, String ClassID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "verify_bord_medium_standard?");
        map.put("StudID", MainUser.getStudentId());
        map.put("BoardID", board);
        map.put("MediumID", medium);
        map.put("StandardID", standaerd);
        map.put("ClassID", ClassID);
        Log.d(TAG, "verify_bord_medium_standard: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.verify_bord_medium_standard, Constant.POST_TYPE.POST_WITH_IMAGE
                , map);
    }

    public void getTxnStatus(String PAYTM_URL, String JsonData) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", PAYTM_URL);
        map.put("JsonData", JsonData);
        Log.d(TAG, "getTxnStatus: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getTxnStatus, Constant.POST_TYPE.GET, map);
    }
    //------------------  Login,  Register, OTP   End------------------//

    public void getSilentLogin(String key, String guid, String device_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "login?");
        map.put("show", "");
        map.put("app_key", key);
        map.put("imei_number", guid);
        map.put("device_id", "");
        map.put("device_type", "android");
        Log.d(TAG, "getSilentLogin: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getSilentLogin, Constant.POST_TYPE.GET, map);
    }


    public void getImages() {
        //   http://onesourcewebs.com/iscore/web-services/get_images
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_images?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getImages: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getImages, Constant.POST_TYPE.GET, map);
    }


    public void getMasterQuestionImages() {
        //   http://onesourcewebs.com/iscore/web-services/get_images
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_master_question_images?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getMasterQuestionImages: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getMasterQuestionImages, Constant.POST_TYPE.GET, map);
    }


    public void getMcqQuestionImages() {
        //   http://onesourcewebs.com/iscore/web-services/get_images
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_mcq_question_images?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getMcqQuestionImages: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getMcqQuestionImages, Constant.POST_TYPE.GET, map);
    }


    public void getBoard() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "getBoards");
        Log.d(TAG, "getBoard: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getBoard, Constant.POST_TYPE.GET, map);
    }

    public void getMedium(String board_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "/getMediums?");
        map.put("board_id", board_id);
        Log.d(TAG, "getMedium: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getMedium, Constant.POST_TYPE.GET, map);
    }

    public void getStandeard(String medium_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "getStandards?");
        map.put("medium_id", medium_id);
        Log.d(TAG, "getStandeard: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getStandeard, Constant.POST_TYPE.GET, map);
    }

    public void getSubject(String standard_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "getSubjects?");
        map.put("standard_id", standard_id);
        map.put("show", "");
        Log.d(TAG, "getSubject: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getSubject, Constant.POST_TYPE.GET, map);
    }

    public void getPackage(String standard_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "getPackage?");
        map.put("standard_id", standard_id);
        map.put("show", "");
        Log.d(TAG, "getPackage: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getPackage, Constant.POST_TYPE.GET, map);
    }

    public void webLogin(String qrcode_key, String app_key, String guid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "qr_login?");
        map.put("imei_number", guid);
        map.put("qrcode_key", qrcode_key);
        map.put("app_key", app_key);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.webLogin, Constant.POST_TYPE.GET, map);
    }

    public void querySendMail(String from, String QueryMessage) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "query_message?");
        map.put("from", from);
        map.put("QueryMessage", QueryMessage);
        Log.d(TAG, "querySendMail: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.qurySendMail, Constant.POST_TYPE.POST, map);

    }

    public void getSubjectTable() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_subjects?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getSubjectTable: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getSubjectTable, Constant.POST_TYPE.GET, map);

    }
    public void getFlashData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", "https://staff.iccc.co.in/web_services/version_58/evaluator_web_services/get_class_banner_image?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getFlashBanner, Constant.POST_TYPE.GET, map);
    }
    public void getDashPopupData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", "https://staff.iccc.co.in/web_services/version_58/evaluator_web_services/get_adds_banner_image?");
        map.put("StudID", MainUser.getStudentId());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getDashPopup, Constant.POST_TYPE.GET, map);
    }
    public void getFlashSubjectData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", "https://staff.iccc.co.in/web_services/version_58/evaluator_web_services/get_class_subjective_banner_image?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getFlashBannerSubject, Constant.POST_TYPE.GET, map);
    }

    public void getChapterTable() {
        Map<String, String> map = new HashMap<String, String>();
        // map.put("url", URL_TABLE);

        // map.put("table", "chapters");
        map.put("url", URL + "get_chapter?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getChapterTable: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getChapterTable, Constant.POST_TYPE.GET, map);

    }


    public void getPaper_Type() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_paper_type?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getPaper_Type: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getPaper_Type, Constant.POST_TYPE.GET, map);

    }

    public void getExamTypeSubject() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_examtype_subject?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getExamTypeSubject: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getExamtype_subject, Constant.POST_TYPE.GET, map);

    }

    public void getExamTypes() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_examtypes?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getExamTypes: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getExamtypes, Constant.POST_TYPE.GET, map);
    }

    public void getExamTypePattern() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_exam_type_pattern?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getExamTypePattern: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getExamTypePattern, Constant.POST_TYPE.GET, map);

    }

    public void getExamTypePatternDetail() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_exam_type_pattern_detail?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getExamTypePatternDetail: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getExamTypePatternDetail, Constant.POST_TYPE.GET, map);

    }

    public void get_master_passage_sub_question() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_master_passage_sub_question?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "get_master_passage_sub_question: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_master_passage_sub_question, Constant.POST_TYPE.GET, map);

    }

    public void get_passage_sub_question_types() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_passage_sub_question_types?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "get_passage_sub_question_types: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_passage_sub_question_types, Constant.POST_TYPE.GET, map);

    }

    public void get_sub_question_types() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_sub_question_types?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "get_sub_question_types: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_sub_question_types, Constant.POST_TYPE.GET, map);

    }

    public void getTableRecords() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get-table-count?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        map.put("PleyaerID", Utils.OneSignalPlearID());
        Log.d(TAG, "getTableRecords: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getTableRecords, Constant.POST_TYPE.GET, map);
    }
    public void getStudentDetails() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_student_details?");
        map.put("StudID", MainUser.getStudentId());
        map.put("PleyaerID", Utils.OneSignalPlearID());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getStudentDetails, Constant.POST_TYPE.GET, map);
    }

    public void getMasterQuestions(int id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get-question?");
        map.put("q_id", id + "");
        map.put("show", "");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "getMasterQuestions: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getMasterQuestionTable, Constant.POST_TYPE.GET, map);
        //   http://www.onesourcewebs.com/iscore/web-services/get-question?
    }

    public void getMcqQuetions(int id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_mcqquestions?");
        map.put("q_id", id + "");
        map.put("show", "");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "getMcqQuetions: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getMcqQuetions, Constant.POST_TYPE.GET, map);

    }

    public void getMCQOptions(int id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_mcqoptions?");
        map.put("q_id", id + "");
        map.put("show", "");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "getMCQOptions: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getMcqOptions, Constant.POST_TYPE.GET, map);

    }

    public void getQuesTypeTable() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_question_types?");
        map.put("StudID", MainUser.getStudentId());
        map.put("show", "");
        Log.d(TAG, "getQuesTypeTable: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getQuesTypeTable, Constant.POST_TYPE.GET, map);
    }

    public void notification(String Notification) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "notification");
        map.put("json", Notification);
        map.put("show", "");
        Log.d(TAG, "notification: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.notification, Constant.POST_TYPE.POST_WITH_JSON, map);

    }


    //--------------------- Data Sync And profile Upadate  Start -----------------------//


    public void sendMcqDataLogin(String McqDtl) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "mcq");
        map.put("StudID", MainUser.getStudentId());
        map.put("json", McqDtl);
        map.put("show", "");
        Log.d(TAG, "sendMcqDataLogin: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendMcqData, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendMcqDataLoginNew(String McqDtl) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "mcq");
        map.put("StudID", MainUser.getStudentId());
        map.put("json", McqDtl);
        map.put("show", "");
        Log.d(TAG, "sendMcqDataLoginNew: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendMcqDataNew, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendIncorrectQuestion(String Data) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "IncorrectQuestion");
        map.put("StudID", MainUser.getStudentId());
        map.put("json", Data);
        map.put("show", "");
        Log.d(TAG, "sendIncorrectQuestion: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendIncorrectQuestion, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendNotAppearedQuestion(String Data) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "NotAppearedQuestion");
        map.put("StudID", MainUser.getStudentId());
        map.put("json", Data);
        map.put("show", "");
        Log.d(TAG, "sendNotAppearedQuestion: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendNotAppearedQuestion, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendGanreatPaperLogin(String McqDtl) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "GeneratePaper");
        map.put("StudID", MainUser.getStudentId());
        map.put("json", McqDtl);
        map.put("show", "");
        Log.d(TAG, "sendGanreatPaperLogin: "+map.toString());
        // new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendGanreatPaper, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendGanreatPaperLoginNew(String McqDtl) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "GeneratePaper");
        map.put("StudID", MainUser.getStudentId());
        map.put("json", McqDtl);
        map.put("show", "");
        Log.d(TAG, "sendGanreatPaperLoginNew: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendGanreatPaperNew, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendBoardPaperDownloadLogin(String BoardPaperDtl) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "BoardPaperDownload");
        map.put("StudID", MainUser.getStudentId());
        map.put("json", BoardPaperDtl);
        map.put("show", "");
        Log.d(TAG, "sendBoardPaperDownloadLogin: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendBoardPaperDownload, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendGanreatPaperVersion(String McqDtl, String Version) {
        Map<String, String> map = new HashMap<String, String>();

        map.put("url", URL + "datasync?");
        map.put("action", "GeneratePaper" + Version);
        map.put("StudID", MainUser.getStudentId());
        map.put("json", McqDtl);
        map.put("show", "");
        Log.d(TAG, "sendGanreatPaperVersion: "+map.toString());
        //   new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendGanreatPaper, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void sendGanreatPaperVersionNew(String McqDtl, String Version) {
        Map<String, String> map = new HashMap<String, String>();

        map.put("url", URL + "datasync?");
        map.put("action", "GeneratePaper" + Version);
        map.put("StudID", MainUser.getStudentId());
        map.put("json", McqDtl);
        map.put("show", "");
        Log.d(TAG, "sendGanreatPaperVersionNew: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendGanreatPaperNew, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void upadateProfile(String firstname, String lastname, String schoolname, String target, String previousTarget, String profile_image, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "UpdateProfile");
        map.put("Firstname", firstname);
        map.put("Lastname", lastname);
        map.put("SchoolName", schoolname);
        map.put("SetTarget", target);
        map.put("PreviousYearScore", previousTarget);
        map.put("image", profile_image);

        map.put("StudID", MainUser.getStudentId());
        map.put("player_id", PleyaerID);
        Log.d(TAG, "upadateProfile: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.upadateProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void check_login_token_silent(String token) {
        // http://onesourcewebs.com/iscore/web-services/get_images
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "check_login_token?");
        map.put("RegMob", MainUser.getRegMobNo());
        map.put("token", token);
        map.put("show", "");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "check_login_token_silent: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.check_login_token, Constant.POST_TYPE.GET, map);
    }

   /* public void check_login_user(String token) {
        Map<String, String> map = new HashMap<>();
        map.put("url", URL + "check_login_token?");
        map.put("RegMob", MainUser.getRegMobNo());
        map.put("token", token);
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "check_login_user: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.check_login_user, Constant.POST_TYPE.GET, map);
    }*/

    public void get_zooki() {
        Map<String, String> map = new HashMap<String, String>();
//        map.put("url", URL + "get-zooki?");
        map.put("url", URL + "getAdvertisement");
        map.put("StudID", MainUser.getStudentId());
//        map.put("show", "");
        Log.d(TAG, "get_zooki: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_zooki, Constant.POST_TYPE.POST, map);
    }
    ////-------------- CCT STart  ---------//

    public void get_recent_mcq_paper() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-recent-mcq-paper?");
        map.put("StudID", MainUser.getStudentId());
        map.put("taken_test", "1");
        Log.d(TAG, "get_recent_mcq_paper: "+map.toString());
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.get_recent_mcq_paper, Constant.POST_TYPE.GET, map);

    }

    public void get_recent_mcq_paper_activity() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-recent-mcq-paper?");
        map.put("StudID", MainUser.getStudentId());
        map.put("taken_test", "0");
        Log.d(TAG, "get_recent_mcq_paper_activity: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_recent_mcq_paper, Constant.POST_TYPE.GET, map);

    }

    public void mcq_view_paper_activity(String paper_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "mcq-view-paper?");
        map.put("id", paper_id);
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "mcq_view_paper_activity: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.mcq_view_paper, Constant.POST_TYPE.GET, map);

    }

    public void submit_mcq_test_activity(String paper_id, String total_attempt,
                                         String toal_que, String rightAns, String detail_array,
                                         String start_time) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "submit_mcq_test?");
        map.put("StudID", MainUser.getStudentId());
        map.put("paper_id", paper_id);
        map.put("total_attempt", total_attempt);
        map.put("total_question", toal_que);
        map.put("total_right", rightAns);
        map.put("detail_array", detail_array);
        map.put("start_time", start_time);
        Log.d(TAG, "submit_mcq_test_activity: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.submit_mcq_test, Constant.POST_TYPE.Eval_POST_WITH_JSON, map);

    }

    public void getIcctReport() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get_subject_cct_paper_report/?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "getIcctReport: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getCctReport, Constant.POST_TYPE.GET, map);
    }


    public void get_mcq_test_result_activity(String TakenTest) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-mcq-test-result?");
        map.put("StudID", MainUser.getStudentId());
        map.put("PaperID", TakenTest);
        Log.d(TAG, "get_mcq_test_result_activity: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_mcq_test_result, Constant.POST_TYPE.GET, map);

    }

    public void mcq_view_summary(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "mcq_view_summary?");
        map.put("id", id);
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "mcq_view_summary: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.mcq_view_summary, Constant.POST_TYPE.GET, map);

    }

    ////-------------- CCT END   ---------//


    ////-------------- Practies paper STart  ---------//

    public void get_paper(String subject_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-subject-paper/?");
        map.put("StudID", MainUser.getStudentId());
        map.put("subject_id", subject_id);
        map.put("class_id", MainUser.getClassID());
        Log.d(TAG, "get_paper: "+map.toString());
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.get_paper, Constant.POST_TYPE.GET, map);
    }

    public void view_paper_activity(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", VIEW_PAPER_EVALUTER_URL + "view-paper?");
        map.put("id", id);
        Log.d(TAG, "view_paper_activity: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.view_paper, Constant.POST_TYPE.GET, map);
    }

    public void view_model_answer(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", VIEW_PAPER_EVALUTER_URL + "view-model-answer?");
        map.put("id", id);
        Log.d(TAG, "view_model_answer: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.view_model_answer, Constant.POST_TYPE.GET, map);

    }

    ////-------------- Practies paper End  ---------//


    public void get_subject_mcq_pape(String subject_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-subject-mcq-paper/?");
        map.put("StudID", MainUser.getStudentId());
        map.put("subject_id", subject_id);
        map.put("class_id", MainUser.getClassID());
        Log.d(TAG, "get_subject_mcq_pape: "+map.toString());
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.get_subject_mcq_pape, Constant.POST_TYPE.GET, map);
    }


    public void get_evaluator_subjects(String section_type) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-evaluator-subjects?");
        map.put("StudID", MainUser.getStudentId());
        map.put("section_type", section_type);
        Log.d(TAG, "get_evaluator_subjects: "+map.toString());
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.get_evaluator_subjects, Constant.POST_TYPE.GET, map);

    }


    public void get_recent_question_paper() {
        Map<String, String> map = new HashMap<>();
        map.put("url", IURL + "get-recent-question-paper?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_recent_question_paper: "+map.toString());
        new MySecureAsynchHttpRequest(ft, Constant.REQUESTS.get_recent_question_paper, Constant.POST_TYPE.GET, map);

    }


    public void get_recent_question_paper_activity() {
        Map<String, String> map = new HashMap<>();
        map.put("url", IURL + "get-recent-question-paper?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_recent_question_paper_activity: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_recent_question_paper, Constant.POST_TYPE.GET, map);
    }


    public void get_single_question_paper(String PaperID) {
        Map<String, String> map = new HashMap<>();
        map.put("url", IURL + "get_single_question_paper/?");
        map.put("StudID", MainUser.getStudentId());
        map.put("PaperID", PaperID);
        Log.d(TAG, "get_single_question_paper: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_single_question_paper, Constant.POST_TYPE.GET, map);

    }


    ////--------------------------- Evaluter Connect   END -------------------------//


    //------------  Board, Moderate, And  Sure-Shot  Start-----------------//

    public void get_subject_board() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_boardpaper_subject?");
        map.put("StudID", MainUser.getStudentId());
        map.put("StandardID", MainUser.getStandardID());
        Log.d(TAG, "get_subject_board: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_subject_board, Constant.POST_TYPE.GET, map);
    }

    public void get_boardpaper(String SubjectID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_boardpaper?");
        map.put("StudID", MainUser.getStudentId());
        map.put("SubjectID", SubjectID);
        Log.d(TAG, "get_boardpaper: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_boardpaper, Constant.POST_TYPE.GET, map);
    }

    public void get_moderatepaper_subject() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_moderatepaper_subject?");
        map.put("StudID", MainUser.getStudentId());
        map.put("StandardID", MainUser.getStandardID());
        Log.d(TAG, "get_moderatepaper_subject: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_moderatepaper_subject, Constant.POST_TYPE.GET, map);
    }

    public void get_moderatepaper(String SubjectID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_moderatepaper?");
        map.put("StudID", MainUser.getStudentId());
        map.put("SubjectID", SubjectID);
        Log.d(TAG, "get_moderatepaper: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_moderatepaper, Constant.POST_TYPE.GET, map);
    }

    public void get_sure_shot() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_sure_shot?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_sure_shot: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_sure_shot, Constant.POST_TYPE.GET, map);
    }

    public void get_notice_board() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_notice_board?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_notice_board: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_notice_board, Constant.POST_TYPE.GET, map);
    }


    //------------  Board, Moderate, And  Sure-Shot  End-----------------//


    ////--------------------------- TextBook Start-------------------------//
    public void get_textbook_list() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "textbook_subjects?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_textbook_list: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_textbook_list, Constant.POST_TYPE.GET, map);
    }
    public void get_textbook_chapter_list(String subid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "textbook_chapters?");
        map.put("StudID", MainUser.getStudentId());
        map.put("SubjectID", subid);
        Log.d(TAG, "get_textbook_chapter_list: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_textbook_chapter_list, Constant.POST_TYPE.GET, map);
    }

    ////--------------------------- TextBook End-------------------------//



    ////--------------------------- library Start-------------------------//

    public void get_library_parent_list() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_library_parent_list?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_library_parent_list: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_library_parent_list, Constant.POST_TYPE.GET, map);
    }

    public void get_library_chapter_list(String FolderID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_library_chapter_list?");
        map.put("StudID", MainUser.getStudentId());
        map.put("FolderID", FolderID);
        Log.d(TAG, "get_library_chapter_list: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_library_chapter_list, Constant.POST_TYPE.GET, map);
    }

    public void get_system_library_files(String FolderID, String FileType) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_system_library_files?");
        map.put("StudID", MainUser.getStudentId());
        map.put("FolderID", FolderID);
        map.put("FileType", FileType);
        Log.d(TAG, "get_system_library_files: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_system_library_files, Constant.POST_TYPE.GET, map);
    }

    public void get_personal_library_sub_folder_files_list(String FolderID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_personal_library_sub_folder_files_list?");
        map.put("StudID", MainUser.getStudentId());
        map.put("FolderID", FolderID);
        Log.d(TAG, "get_personal_library_sub_folder_files_list: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_personal_library_sub_folder_files_list, Constant.POST_TYPE.GET, map);
    }

    ////--------------------------- library End-------------------------//

/*
    public void check_login_token(String token) {
        //   http://onesourcewebs.com/iscore/web-services/get_images
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "check_login_token?");
        map.put("RegMob", MainUser.getRegMobNo());
        map.put("token", token);
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "check_login_token: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.check_login_token, Constant.POST_TYPE.GET, map);
    }
*/

    public void get_student_class_dtl() {
        //   http://onesourcewebs.com/iscore/web-services/get_images
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get_student_class_dtl?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_student_class_dtl: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_student_class_dtl, Constant.POST_TYPE.GET, map);
    }

    public void Verifychecksum(String MID, String ORDER_ID, String CUST_ID, String INDUSTRY_TYPE_ID,
                               String CHANNEL_ID, String TXN_AMOUNT, String WEBSITE, String EMAIL,
                               String MOBILE_NO, String CALLBACK_URL, String CHECKSUMHASH) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.BASIC_URL + Constant.URL_VERSION + "paytm/verifyChecksum.php");
        map.put("MID", MID);
        map.put("ORDER_ID", ORDER_ID);
        map.put("CUST_ID", CUST_ID);
        map.put("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);
        map.put("CHANNEL_ID", CHANNEL_ID);
        map.put("TXN_AMOUNT", TXN_AMOUNT);
        map.put("WEBSITE", WEBSITE);
        map.put("EMAIL", EMAIL);
        map.put("MOBILE_NO", MOBILE_NO);

        map.put("CALLBACK_URL", CALLBACK_URL);
        map.put("CHECKSUMHASH", CHECKSUMHASH);
        Log.d(TAG, "Verifychecksum: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.Verifychecksum, Constant.POST_TYPE.POST, map);

    }

    public void get_student_wallate_blance() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_student_wallate_blance?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "get_student_wallate_blance: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_student_wallate_blance, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void withdraw_money(String Amount, String mNumber) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "withdraw_money?");
        map.put("StudID", MainUser.getStudentId());
        map.put("Amount", Amount);
        map.put("MobileNo", mNumber);
        Log.d(TAG, "withdraw_money: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.withdraw_money, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void varify_refral_code(String refralCode, String Amount) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "varify_refral_code?");
        map.put("StudID", MainUser.getStudentId());
        map.put("RefralCode", refralCode);
        map.put("Amount", Amount);
        map.put("BoardID", MainUser.getBoardID());
        Log.d(TAG, "varify_refral_code: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.varify_refral_code, Constant.POST_TYPE.POST_WITH_IMAGE
                , map);
    }
    public void varify_Promo_code(String refralCode, String Amount) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "GetPromoCode");
        map.put("StudID", MainUser.getStudentId());
        map.put("PromoCode", refralCode);
        map.put("Amount", Amount);
        Log.d(TAG, "varify_refral_code: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.GetPromoCode, Constant.POST_TYPE.POST_WITH_IMAGE
                , map);
    }

    public void getDates(boolean isShow, String SID, String date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "lesson_planner_dates");
        map.put("StudID", SID);
        map.put("Month", date);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getDate, Constant.POST_TYPE.POST, map);
    }
    public void getDataByDate(boolean isShow, String date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "lesson_planner_data");
        map.put("Date", date);
        map.put("StudID", MainUser.getStudentId());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getDataByDate, Constant.POST_TYPE.POST, map);
    }

    public void getOnlineClassURL(String class_id,String isTeacher,String lessonName,String ourseName) {
        Map<String, String> map = new HashMap<String, String>();
        //  map.put("url", URL + "lesson_planner_data");
        map.put("url", "https://api.braincert.com/v2/getclasslaunch?apikey=yOYjUZ9bX4LkuWthDlHg");
        map.put("class_id", class_id);
        map.put("userId", MainUser.getStudentId());
        map.put("userName", MainUser.getFirstName()+" "+MainUser.getLastName());
        map.put("isTeacher", isTeacher);
        map.put("lessonName", lessonName);
        map.put("courseName", ourseName);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getOnlineClassURL, Constant.POST_TYPE.POST, map);
    }

    //FOR GENERATE PAPER TYPE
    public void UploadPaperImages(List<ImageDataModel> images, String PaperID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "submit_student_anwser?");
        map.put("StudID", MainUser.getStudentId());
        map.put("PaperID", PaperID);
        images.remove(0);
        if (images.size() > 0) {
            for (int i = 0; i < images.size(); i++) {
                Log.d(TAG, "UploadPaperImages: "+images.get(i).getFile().getAbsolutePath());
                String key = "image[" + i + "]";
                map.put(key,images.get(i).getFile().getAbsolutePath());
            }
        }
        Log.d(TAG, "uploadIamges: "+map.toString());
        new AsyncHttpRequest(ct, Constant.REQUESTS.UploadPaperImages, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    //FOR UPLOAD PAPER TYPE
    public void SubmitUploadPaperImages(List<ImageDataModel> images, String PaperID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "submit_upload_paper_student_anwser");
        map.put("StudID", MainUser.getStudentId());
        map.put("PaperID", PaperID);
        images.remove(0);
        if (images.size() > 0) {
            for (int i = 0; i < images.size(); i++) {
                Log.d(TAG, "UploadPaperImages: "+images.get(i).getFile().getAbsolutePath());
                String key = "image[" + i + "]";
                map.put(key,images.get(i).getFile().getAbsolutePath());
            }
        }
        Log.d(TAG, "uploadIamges: "+map.toString());
        new AsyncHttpRequest(ct, Constant.REQUESTS.UploadPaperImages, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void SendPaperEnterTime(String StudID, String HistoryID,String Type, String Time) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "submit_paper_view_start_time");
        map.put("StudID", StudID);
        map.put("HistoryID", HistoryID);
        map.put("Type", Type);
        map.put("Time", Time);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.SendPaperEnterTime, Constant.POST_TYPE.POST, map);
    }
    public void SendPaperStartTime(String StudID, String ClassLessonPlannerID, String Time) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "submit_lecture_view_start_time");
        map.put("StudID", StudID);
        map.put("ClassLessonPlannerID", ClassLessonPlannerID);
        map.put("Time", Time);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.SendPaperEnterTime, Constant.POST_TYPE.POST, map);
    }

    public void GetEvents() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "getEvents");
        map.put("StudID", MainUser.getStudentId());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.GetEvent, Constant.POST_TYPE.POST, map);
    }
    public void EventRegister(String mobile_no, String email, String EventID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "EventRegister");
        map.put("StudentID", MainUser.getStudentId());
        map.put("mobile_no", mobile_no);
        map.put("email", email);
        map.put("EventID", EventID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.EventRegister, Constant.POST_TYPE.POST, map);
    }
    public void ClassEventCheck() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "ClassEventCheck");
        map.put("StudID", MainUser.getStudentId());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.ClassEventCheck, Constant.POST_TYPE.POST, map);
    }

    public void CheckClass() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "check_class?");
        map.put("StudID", MainUser.getStudentId());
        Log.d(TAG, "CheckClass: "+map.toString());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.CheckClass, Constant.POST_TYPE.GET, map);
    }
}
