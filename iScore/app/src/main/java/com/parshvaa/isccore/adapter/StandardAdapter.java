package com.parshvaa.isccore.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.model.StandardModel;
import com.parshvaa.isccore.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 12/25/2017.
 */

public class StandardAdapter extends RecyclerView.Adapter<StandardAdapter.ViewHolder> {
    private ArrayList<StandardModel> offersList;
    private Context context;
    public int row_index = 0;
    private int lastSelectedPosition = -1;
    public String standardName, standardId;

    public StandardAdapter(ArrayList<StandardModel> offersListIn, Context ctx) {
        offersList = offersListIn;
        context = ctx;
    }

    @Override
    public StandardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.standard_item, parent, false);

        StandardAdapter.ViewHolder viewHolder =
                new StandardAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StandardAdapter.ViewHolder holder,
                                 final int position) {
        StandardModel StandardObj = offersList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index=position;
                notifyDataSetChanged();
            }
        });
        holder.tv_stdName.setText(StandardObj.getStandardName());

        if (row_index == position) {
            holder.rel_std.setBackgroundResource(R.drawable.squar_border_red);
            standardName = StandardObj.getStandardName();
            standardId = StandardObj.getStandardID();
        } else {
            holder.rel_std.setBackgroundColor(Color.parseColor("#ffffff"));
        }


    //since only one radio button is allowed to be selected,
    // this condition un-checks previous selections
}

    @Override
    public int getItemCount() {
        return offersList.size();
    }

public class ViewHolder extends RecyclerView.ViewHolder {


    public TextView tv_stdName;
    public RelativeLayout rel_std;

    public ViewHolder(View view) {
        super(view);
        tv_stdName = view.findViewById(R.id.tv_stdName);
        rel_std = view.findViewById(R.id.rel_std);

    }

}

    public String getStandardName() {
        return standardName;
    }

    public String getStandardId() {
        return standardId;
    }
}