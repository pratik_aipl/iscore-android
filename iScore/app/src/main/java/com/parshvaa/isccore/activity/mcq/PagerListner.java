package com.parshvaa.isccore.activity.mcq;

/**
 * Created by Karan - Empiere on 1/15/2018.
 */

public interface PagerListner {
   void onPageVisible(int pos);
}
