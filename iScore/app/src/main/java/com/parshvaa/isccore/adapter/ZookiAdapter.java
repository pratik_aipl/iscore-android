package com.parshvaa.isccore.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.bumptech.glide.Glide;
import com.parshvaa.isccore.activity.event.EventListActivity;
import com.parshvaa.isccore.model.Zooki;
import com.parshvaa.isccore.R;

import java.util.ArrayList;


public class ZookiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context mContext;
    LayoutInflater inflater;
    Zooki zookiObj;
    private ArrayList<Zooki> spaArray;
    public int type = 0;

    private static final int VIEW_TYPE_DEFULT = 0;
    //private static final int VIEW_TYPE_VIEW_MORE = 1;
    AQuery aQuery;

    public ZookiAdapter(Activity context, ArrayList<Zooki> spaArray) {

        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.spaArray = spaArray;
        aQuery = new AQuery(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       /* if (viewType == VIEW_TYPE_DEFULT) {
        } *//*else {
            return new ViewHolderViewMore(inflater.inflate(R.layout.custom_zooki_viewmore_row, parent, false));
        }*/
        return new ViewHolderMenu(inflater.inflate(R.layout.custom_zooki_row, parent, false));
    }


    public Zooki getItem(int position) {
        return spaArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return spaArray.size();
    }


    public class ViewHolderMenu extends RecyclerView.ViewHolder {
        public TextView tv_videoTitle;
        public ImageView img_video;
        public ProgressBar pBar;

        int pos;

        public ViewHolderMenu(View v) {
            super(v);
            tv_videoTitle = v.findViewById(R.id.tv_videoTitle);
            img_video = v.findViewById(R.id.img_video);

        }
    }

   /* public class ViewHolderViewMore extends RecyclerView.ViewHolder {
        public Button btn_viewmore;


        public ViewHolderViewMore(View v) {
            super(v);
            btn_viewmore = v.findViewById(R.id.btn_viewmore);

        }
    }*/

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderMenu offerHolder = (ViewHolderMenu) holder;
                bindSpaHolder(offerHolder, position);
                break;
           /* case 1:
                ViewHolderViewMore addrHolder = (ViewHolderViewMore) holder;
                bindViewMoreHolder(addrHolder, position);
                break;*/

        }

    }

/*    @Override
    public int getItemViewType(int position) {

      *//*  if (position == (spaArray.size() - 1)) {
            return VIEW_TYPE_VIEW_MORE;
        } else {
        }
*//*
        return 0;

    }*/


  /*  public void bindViewMoreHolder(final ViewHolderViewMore holder, final int pos) {
        holder.btn_viewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(mContext)) {
                    mContext.startActivity(new Intent(mContext, ZookiWebActivity.class));
                } else {
                    Utils.showToast(mContext.getResources().getString(R.string.conect_internet), mContext);


                }
            }
        });

    }*/

    public void bindSpaHolder(final ViewHolderMenu holder, final int pos) {
        zookiObj = getItem(pos);
       // holder.tv_videoTitle.setText(zookiObj.getTitle());

        holder.img_video.setOnClickListener(view -> {
            if(!TextUtils.isEmpty(getItem(pos).getEventID())){
                mContext.startActivity(new Intent(mContext, EventListActivity.class));
            }
        });

        if (!TextUtils.isEmpty(zookiObj.getImage()))
            Glide.with(mContext)
                    .load(zookiObj.getImage())
                    .placeholder(R.drawable.iscore_final_logo)
                    .into(holder.img_video);

     /*   String imgName = zookiObj.getImage();

        String imagePathUrl = imgName;
        File image = new File(Constant.LOCAL_IMAGE_PATH + "/zooki/" + imgName);
        if (image.exists()) {
            Utils.setImage(mContext, image, holder.img_video,R.drawable.warning);
        } else {
            if (Utils.isNetworkAvailable(mContext)) {
                Util.downloadClassIcon(mContext, aQuery, imagePathUrl, holder.img_video, image, R.drawable.warning);
            } else {
                holder.img_video.setImageResource(R.drawable.profile);
            }
        }*/
    }


}


