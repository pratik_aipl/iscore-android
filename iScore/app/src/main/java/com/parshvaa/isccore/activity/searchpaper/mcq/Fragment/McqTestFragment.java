package com.parshvaa.isccore.activity.searchpaper.mcq.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.parshvaa.isccore.activity.searchpaper.feftdrawer.LeftSubjectDrawerFragment;
import com.parshvaa.isccore.activity.searchpaper.mcq.adapter.McqTestAdapter;
import com.parshvaa.isccore.activity.searchpaper.mcq.model.TestModel;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.FilterArrayListListener;
import com.parshvaa.isccore.model.McqOption;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.StudentMcqTestChap;
import com.parshvaa.isccore.model.StudentMcqTestDtl;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseFragment;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by empiere-vaibhav on 1/10/2018.
 */

public class McqTestFragment extends BaseFragment implements FilterArrayListListener {
    private static final String TAG = "McqTestFragment";
    public View v;
    RelativeLayout left_button, right_button;
    private LeftSubjectDrawerFragment left_drawer;
    public RightLevelDrawerFragment right_drawer;
    public RelativeLayout mainLay, mainLayout;
    public DrawerLayout drawerLayout;
    public RecyclerView recycler_mcq_sub;
    private McqTestAdapter setPaperTypeAdapter;
    private List<TestModel> testModelArray = new ArrayList<>();
    public int TotalDuration = 0, tempTotalDuration, milliseconds, attempted, headerId;
    public ArrayList<McqQuestion> rightAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> nonAttemptAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> incorectAnsArray = new ArrayList<>();

    public String SubjectId = "", LevelId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_mcq_test, container, false);


        mainLay = v.findViewById(R.id.mainLay);
        mainLayout = v.findViewById(R.id.mainLayout);
        left_button = v.findViewById(R.id.left_button);
        right_button = v.findViewById(R.id.right_button);
        drawerLayout = v.findViewById(R.id.drawer_layout);

        recycler_mcq_sub = v.findViewById(R.id.recycler_mcq_sub);

        left_drawer = (LeftSubjectDrawerFragment) getActivity().getFragmentManager().findFragmentById(R.id.fragment_left_drawer);
        right_drawer = (RightLevelDrawerFragment)  getActivity().getFragmentManager().findFragmentById(R.id.fragment_right_drawer);

        right_drawer.setUpRightButton(R.id.fragment_right_drawer, drawerLayout, right_button, mainLay);
        left_drawer.setUpButton(R.id.fragment_left_drawer, drawerLayout, left_button, mainLayout);
        left_drawer.setDrawerListener(McqTestFragment.this);
        right_drawer.setDrawerListener(McqTestFragment.this);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        recycler_mcq_sub.setLayoutAnimation(controller);
        recycler_mcq_sub.scheduleLayoutAnimation();
        recycler_mcq_sub.setLayoutManager(new LinearLayoutManager(getActivity()));
        setPaperTypeAdapter = new McqTestAdapter(testModelArray, getActivity());
        recycler_mcq_sub.setAdapter(setPaperTypeAdapter);

        RecentTest();
        App.checked_subject.clear();
        App.checked_level.clear();
        new getRecorrd(getActivity()).execute();
        return v;
    }


    public void RecentTest() {
        try {
            //  System.out.println("Total Rows on try");

            Log.d(TAG, "RecentTest: "+DBNewQuery.getNoOfLastMCQTest());
            testModelArray.clear();
            testModelArray = (ArrayList) new CustomDatabaseQuery(getActivity(), new TestModel()).execute(new String[]{DBNewQuery.getNoOfLastMCQTest()}).get();
            setPaperTypeAdapter.notifyDataSetChanged();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void FilterSubjectTest(String subjectId, String levelId) {

        Utils.Log("TAG", "Selected  Subject :-> " + subjectId);
        Utils.Log("TAG", "Selected  levelId :-> " + levelId);


        try {

            testModelArray.clear();

            testModelArray = (ArrayList) new CustomDatabaseQuery(getActivity(), new TestModel())
                    .execute(new String[]{DBNewQuery.getFilterSubjectTest(subjectId, levelId)}).get();
            setPaperTypeAdapter = new McqTestAdapter(testModelArray, getActivity());

            recycler_mcq_sub.setAdapter(setPaperTypeAdapter);
            setPaperTypeAdapter.notifyDataSetChanged();


        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLeftDrawerItemSelected() {

        FilterSubjectTest(android.text.TextUtils.join(",", App.checked_subject), android.text.TextUtils.join(",", App.checked_level));
    }

    @Override
    public void onRightDrawerItemSelected() {
        FilterSubjectTest(android.text.TextUtils.join(",", App.checked_subject), android.text.TextUtils.join(",", App.checked_level));
    }


    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;


        public McqQuestion mcqQuestion;
        public McqOption option;
        public StudentMcqTestChap testChapter;
        public StudentMcqTestDtl testDetail;

        public ArrayList<McqQuestion> mcqArray = new ArrayList<>();
        public ArrayList<McqQuestion> tempArray = new ArrayList<>();
        public ArrayList<StudentMcqTestChap> chapterArray = new ArrayList<>();
        public ArrayList<StudentMcqTestDtl> testDetailArray = new ArrayList<>();
        public int totalRight = 0, totalWrong = 0, totalAttempted = 0, totalNonAttempted = 0;


        private getRecorrd(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {

            Cursor MCQchapter = mDb.getAllRows(DBQueries.getStudentMCQChapters(headerId + ""));
            if (MCQchapter != null && MCQchapter.moveToFirst()) {
                do {
                    testChapter = (StudentMcqTestChap) cParse.parseCursor(MCQchapter, new StudentMcqTestChap());
                    chapterArray.add(testChapter);
                } while (MCQchapter.moveToNext());
                MCQchapter.close();
            } else {
            }
            Cursor MCQDetail = mDb.getAllRows(DBQueries.getStudentMCQDetai(headerId + ""));

            if (MCQDetail != null && MCQDetail.moveToFirst()) {
                do {
                    testDetail = (StudentMcqTestDtl) cParse.parseCursor(MCQDetail, new StudentMcqTestDtl());
                    testDetailArray.add(testDetail);
                } while (MCQDetail.moveToNext());
                MCQDetail.close();
            } else {
            }
            try {
                String queIDS = "";
                for (StudentMcqTestDtl detail : testDetailArray) {
                    queIDS += detail.getQuestionID() + ",";
                }
                queIDS = queIDS.substring(0, queIDS.length() - 1);
                Cursor MCQQuetions = mDb.getAllRows(DBQueries.getAttemptedQuetion(queIDS));
                MCQQuetions.getCount();
                if (MCQQuetions != null && MCQQuetions.moveToFirst()) {
                    do {
                        mcqQuestion = (McqQuestion) cParse.parseCursor(MCQQuetions, new McqQuestion());
                        mcqArray.add(mcqQuestion);
                    } while (MCQQuetions.moveToNext());
                    MCQQuetions.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            int i = 0;
            for (Iterator<StudentMcqTestDtl> it = testDetailArray.iterator(); it.hasNext(); ) {
                StudentMcqTestDtl detail = it.next();
                //  Utils.Log("TAG ", " TAG : FIRST LOOP  :->" + ++i);
                int j = 0;
                for (Iterator<McqQuestion> mcqIT = mcqArray.iterator(); mcqIT.hasNext(); ) {
                    McqQuestion que = mcqIT.next();
                    if (que.getMCQQuestionID().equalsIgnoreCase(detail.getQuestionID())) {
                        try {
                            tempArray.add((McqQuestion) que.clone());
                            break;
                        } catch (CloneNotSupportedException e) {
                        }

                    }


                }
            }
            mcqArray.clear();
            mcqArray = (ArrayList<McqQuestion>) tempArray.clone();
            // Utils.Log("TAG ", " TAG : mcqArray " + mcqArray.size());
            Cursor oc = null;
            totalAttempted = 0;
            for (McqQuestion que : mcqArray) {
                String rightAnswerId = "";
                for (StudentMcqTestDtl dtl : testDetailArray) {
                    if (dtl.getQuestionID().equalsIgnoreCase(que.getMCQQuestionID())) {
                        rightAnswerId = dtl.getAnswerID();
                        if (!TextUtils.isEmpty(rightAnswerId))
                            que.selectedAns = Integer.parseInt(rightAnswerId);

                        if (!dtl.getIsAttempt().isEmpty()) {
                            if (dtl.getIsAttempt().equalsIgnoreCase("1")) {
                                que.isAttempted = 1;
                                ++totalAttempted;
                            } else if (dtl.getIsAttempt().equalsIgnoreCase("0")) {
                                que.isAttempted = 0;
                            }
                        }

                    }
                }
                oc = mDb.getAllRows(DBQueries.getMCQAnswers(que.MCQQuestionID));
                if (oc != null && oc.moveToFirst()) {
                    que.optionsArray.clear();
                    do {
                        option = (McqOption) cParse.parseCursor(oc, new McqOption());
                        // Utils.Log("TAg", "is OPtin:->" + option.getMCQOPtionID() + " = " + rightAnswerId + " && " + option.isCorrect + " 1 ");
                        if (option.getMCQOPtionID().equalsIgnoreCase(rightAnswerId) && option.isCorrect.equalsIgnoreCase("1")) {
                            option.isRightAns = 1;
                            que.isRight = true;
                        }
                        que.optionsArray.add(option);
                    } while (oc.moveToNext());
                } else {
                }
            }

            if (oc != null)
                oc.close();

            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    ++i;
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            nonAttemptAnsArray.clear();
            incorectAnsArray.clear();
            rightAnsArray.clear();


            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    rightAnsArray.add(que);
                }
                if (que.isAttempted == 1 && !que.isRight) {
                    incorectAnsArray.add(que);
                }
                if (que.isAttempted == 0) {
                    nonAttemptAnsArray.add(que);
                }


            }

        }
    }

}
