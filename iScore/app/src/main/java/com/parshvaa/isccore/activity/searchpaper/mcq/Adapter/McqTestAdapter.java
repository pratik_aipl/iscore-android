package com.parshvaa.isccore.activity.searchpaper.mcq.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parshvaa.isccore.activity.searchpaper.mcq.model.TestModel;
import com.parshvaa.isccore.activity.searchpaper.mcq.SearchTestReportActivity;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.PaperTypeInterface;
import com.parshvaa.isccore.model.McqOption;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.StudentMcqTestChap;
import com.parshvaa.isccore.model.StudentMcqTestDtl;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.ProgressBarAnimation;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by empiere-vaibhav on 1/12/2018.
 */

public class McqTestAdapter extends RecyclerView.Adapter<McqTestAdapter.MyViewHolder> {

    private List<TestModel> paperTypeList;
    public Context context;
    public int row_index = 0;
    public PaperTypeInterface paperTypeInterface;
    public ArrayList<McqQuestion> rightAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> nonAttemptAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> incorectAnsArray = new ArrayList<>();
    public int headerId, SubjectID;
    public String SubjectName;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_accuracy, tv_total_que, tv_right_ans, tv_date;
        public ProgressBar pbar_accuracy;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_accuracy = view.findViewById(R.id.tv_accuracy);
            tv_total_que = view.findViewById(R.id.tv_total_que);
            tv_right_ans = view.findViewById(R.id.tv_right_ans);
            tv_date = view.findViewById(R.id.tv_date);

            pbar_accuracy = view.findViewById(R.id.pbar_accuracy);

        }
    }


    public McqTestAdapter(List<TestModel> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_serach_mcq_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final TestModel testModel = paperTypeList.get(position);
        holder.tv_subject_name.setText(testModel.getSubjectName());
        holder.tv_accuracy.setText("Accuracy " + testModel.getAccuracy1() + "%");
        holder.tv_total_que.setText(String.valueOf(testModel.getTotal_que()));
        holder.tv_right_ans.setText(String.valueOf(testModel.getTotal_Right()));

        String date = testModel.getCreatedOn();
        date = date.substring(0, 10);

        holder.tv_date.setText(Utils.changeDateToDDMMYYYY(date));


        ProgressBarAnimation anim = new ProgressBarAnimation(holder.pbar_accuracy, 0, testModel.getAccuracy1());
        anim.setDuration(1000);
        holder.pbar_accuracy.startAnimation(anim);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    headerId = testModel.getStudentMCQTestHDRID();
                    SubjectID = Integer.parseInt(testModel.getSubjectID());
                    SubjectName = testModel.getSubjectName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new getRecorrd(context, headerId).execute();
            }
        });

    }


    @Override
    public int getItemCount() {
        return paperTypeList.size();
    }


    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;


        public McqQuestion mcqQuestion;
        public McqOption option;
        public StudentMcqTestChap testChapter;
        public StudentMcqTestDtl testDetail;

        public ArrayList<McqQuestion> mcqArray = new ArrayList<>();
        public ArrayList<McqQuestion> tempArray = new ArrayList<>();
        public ArrayList<StudentMcqTestChap> chapterArray = new ArrayList<>();
        public ArrayList<StudentMcqTestDtl> testDetailArray = new ArrayList<>();
        public int totalRight = 0, totalWrong = 0, totalAttempted = 0, totalNonAttempted = 0;
        int headerId;


        private getRecorrd(Context context, int headerId) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            this.headerId = headerId;

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {

            Cursor MCQchapter = mDb.getAllRows(DBQueries.getStudentMCQChapters(headerId + ""));
            if (MCQchapter != null && MCQchapter.moveToFirst()) {
                do {
                    testChapter = (StudentMcqTestChap) cParse.parseCursor(MCQchapter, new StudentMcqTestChap());
                    chapterArray.add(testChapter);
                } while (MCQchapter.moveToNext());
                MCQchapter.close();
            } else {
            }
            Cursor MCQDetail = mDb.getAllRows(DBQueries.getStudentMCQDetai(headerId + ""));

            if (MCQDetail != null && MCQDetail.moveToFirst()) {
                do {
                    testDetail = (StudentMcqTestDtl) cParse.parseCursor(MCQDetail, new StudentMcqTestDtl());
                    testDetailArray.add(testDetail);
                } while (MCQDetail.moveToNext());
                MCQDetail.close();
            } else {
            }
            try {
                String queIDS = "";
                for (StudentMcqTestDtl detail : testDetailArray) {
                    queIDS += detail.getQuestionID() + ",";
                }
                queIDS = queIDS.substring(0, queIDS.length() - 1);
                Cursor MCQQuetions = mDb.getAllRows(DBQueries.getAttemptedQuetion(queIDS));
                MCQQuetions.getCount();
                if (MCQQuetions != null && MCQQuetions.moveToFirst()) {
                    do {
                        mcqQuestion = (McqQuestion) cParse.parseCursor(MCQQuetions, new McqQuestion());
                        mcqArray.add(mcqQuestion);
                    } while (MCQQuetions.moveToNext());
                    MCQQuetions.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            int i = 0;
            for (Iterator<StudentMcqTestDtl> it = testDetailArray.iterator(); it.hasNext(); ) {
                StudentMcqTestDtl detail = it.next();
                //  Utils.Log("TAG ", " TAG : FIRST LOOP  :->" + ++i);
                int j = 0;
                for (Iterator<McqQuestion> mcqIT = mcqArray.iterator(); mcqIT.hasNext(); ) {
                    McqQuestion que = mcqIT.next();
                    //    Utils.Log("TAG ", " TAG : Second LOOP  :->" + i + " : " + ++j);

                    if (que.getMCQQuestionID().equalsIgnoreCase(detail.getQuestionID())) {
                        try {
                            //  Utils.Log("TAG ", " TAG : Second LOOP  try:->" + i + " : " + j);
                            tempArray.add((McqQuestion) que.clone());

                            System.out.print("");
                            break;
                        } catch (CloneNotSupportedException e) {
                        }

                    }


                }
            }
            mcqArray.clear();
            mcqArray = (ArrayList<McqQuestion>) tempArray.clone();
            // Utils.Log("TAG ", " TAG : mcqArray " + mcqArray.size());
            Cursor oc = null;
            totalAttempted = 0;
            for (McqQuestion que : mcqArray) {
                String rightAnswerId = "";
                for (StudentMcqTestDtl dtl : testDetailArray) {
                    if (dtl.getQuestionID().equalsIgnoreCase(que.getMCQQuestionID())) {
                        rightAnswerId = dtl.getAnswerID();
                        if (!TextUtils.isEmpty(rightAnswerId))
                            que.selectedAns = Integer.parseInt(rightAnswerId);
                        if (!dtl.getIsAttempt().isEmpty()) {
                            if (dtl.getIsAttempt().equalsIgnoreCase("1")) {
                                que.isAttempted = 1;
                                ++totalAttempted;
                            } else if (dtl.getIsAttempt().equalsIgnoreCase("0")) {
                                que.isAttempted = 0;
                            }
                        }

                    }
                }
                oc = mDb.getAllRows(DBQueries.getMCQAnswers(que.MCQQuestionID));
                if (oc != null && oc.moveToFirst()) {
                    que.optionsArray.clear();
                    do {
                        option = (McqOption) cParse.parseCursor(oc, new McqOption());
                        if (option.getMCQOPtionID().equalsIgnoreCase(rightAnswerId) && option.isCorrect.equalsIgnoreCase("1")) {
                            option.isRightAns = 1;
                            que.isRight = true;
                        }
                        que.optionsArray.add(option);
                    } while (oc.moveToNext());
                } else {
                }
            }

            if (oc != null)
                oc.close();

            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    ++i;
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            nonAttemptAnsArray.clear();
            incorectAnsArray.clear();
            rightAnsArray.clear();


            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    rightAnsArray.add(que);
                }
                if (que.isAttempted == 1 && !que.isRight) {
                    incorectAnsArray.add(que);
                }
                if (que.isAttempted == 0) {
                    nonAttemptAnsArray.add(que);
                }


            }
            context.startActivity(new Intent(context, SearchTestReportActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra("HeaderId", headerId)
                    .putExtra("rightAnsArray", rightAnsArray)
                    .putExtra("incorectAnsArray", incorectAnsArray)
                    .putExtra("nonAttemptAnsArray", nonAttemptAnsArray)
                    .putExtra("SubjectID", SubjectID)
                    .putExtra("SubjectName", SubjectName));


        }
    }

}

