package com.parshvaa.isccore.activity.revisionnote.adapter;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.parshvaa.isccore.custominterface.ChapterInterface;
import com.parshvaa.isccore.model.QuestionTypes;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.RecyclerView.ViewHolder;

/**
 * Created by empiere-vaibhav on 1/15/2018.
 */

public class SelectQueTypeAdapter extends RecyclerView.Adapter<ViewHolder> {
    private ArrayList<QuestionTypes> boardArrayList = new ArrayList<>();
    public Context context;
    public List<String> iDS;
    public List<String> ParentiDS;
    private List<QuestionTypes> mGroupCollection;
    public CheckBox cb;
    public ChapterInterface cInterface;
    private LayoutInflater mInflater;
    private int lastSelectedPosition = -1;
    public String boardId = "";
    public String boardNamee = "";
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;

    public SelectQueTypeAdapter(ArrayList<QuestionTypes> boardList, Context ctx, CheckBox cb) {
        boardArrayList = boardList;
        context = ctx;
        iDS = new ArrayList<>();
        ParentiDS = new ArrayList<>();
        mGroupCollection = boardList;
        this.cb = cb;
        mInflater = LayoutInflater.from(context);
        cInterface = (ChapterInterface) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_HEADER) {
            return new ViewHolderBlank(mInflater.inflate(R.layout.blank_row, parent, false));
        } else {
            return new ViewHolderContent(mInflater.inflate(R.layout.custom_que_type_raw, parent, false));
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                ViewHolderContent addrHolder = (ViewHolderContent) holder;
                bindContentHolder(addrHolder, position);
                break;


        }
    }


    @Override
    public int getItemCount() {
        return boardArrayList.size();
    }

    public QuestionTypes getItem(int position) {

        return boardArrayList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).getType().equals("blank")) {
            return VIEW_TYPE_HEADER;

        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    class ViewHolderContent extends ViewHolder {
        public TextView tv_marks;
        public CheckBox chkSelected;

        public ViewHolderContent(View view) {
            super(view);
            chkSelected = view.findViewById(R.id.chkSelected);
            tv_marks = view.findViewById(R.id.tv_marks);

        }
    }

    class ViewHolderBlank extends RecyclerView.ViewHolder {
        public ViewHolderBlank(View v2) {
            super(v2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void bindContentHolder(final ViewHolderContent holder, final int position) {
        QuestionTypes boardObj = boardArrayList.get(position);
        holder.chkSelected.setText(boardObj.getQuestionType());
        holder.tv_marks.setText("[" + boardObj.getMarks() + "]");
        holder.chkSelected.setOnCheckedChangeListener(null);
        if (iDS.contains(mGroupCollection.get(position).getQuestionTypeID())) {
            holder.chkSelected.setChecked(true);
        } else {
            holder.chkSelected.setChecked(false);
        }
        holder.chkSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                String id = mGroupCollection.get(position).getQuestionTypeID();
                if (b) {
                    if (!iDS.contains(id)) {
                        iDS.add(id);
                        boolean isSelected = checkAllSelected();
                        cInterface.onCheckBoxDeselect(isSelected);

                    }
                } else {
                    cInterface.onCheckBoxDeselect(false);
                    iDS.remove(id);
                }

            }
        });
    }

    public String getSelectedQuestionType() {
        Utils.Log("TAG", " Type IDS :-> " + android.text.TextUtils.join(",", iDS) + ":->");
        return android.text.TextUtils.join(",", iDS);
    }

    public boolean checkAllSelected() {
        for (int i = 0; i < (mGroupCollection.size() - 1); i++) {
            if (!iDS.contains(mGroupCollection.get(i).getQuestionTypeID())) {
                return false;
            }

        }
        return true;

    }

    public void selectAll() {
        iDS.clear();
        for (int i = 0; i < (mGroupCollection.size() - 1); i++) {
            iDS.add(mGroupCollection.get(i).getQuestionTypeID());
        }

        // iDS.remove(iDS.size() - 1);
        this.notifyDataSetChanged();
    }


    public void deSelectAll() {
        iDS.clear();
        ParentiDS.clear();
        this.notifyDataSetChanged();
    }

}
