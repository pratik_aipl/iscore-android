package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/22/2018.
 */

public class SetPaperQuestionAndTypes implements Serializable {

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public String getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(String questionType) {
        QuestionType = questionType;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getTotalAsk() {
        return TotalAsk;
    }

    public void setTotalAsk(String totalAsk) {
        TotalAsk = totalAsk;
    }

    public String getToAnswer() {
        return ToAnswer;
    }

    public void setToAnswer(String toAnswer) {
        ToAnswer = toAnswer;
    }

    public String getTotalMark() {
        return TotalMark;
    }

    public void setTotalMark(String totalMark) {
        TotalMark = totalMark;
    }

    public boolean isHeader = false;
    public boolean isSolution = false;
    public String Question = "";
    public String Answer = "";
    public String StudentSetPaperQuestionTypeID = "";
    public String isPassage = "";
    public String StudentSetPaperDetailID="";

    public String getStudentSetPaperDetailID() {
        return StudentSetPaperDetailID;
    }

    public void setStudentSetPaperDetailID(String studentSetPaperDetailID) {
        StudentSetPaperDetailID = studentSetPaperDetailID;
    }

    public String getStudentSetPaperQuestionTypeID() {
        return StudentSetPaperQuestionTypeID;
    }

    public void setStudentSetPaperQuestionTypeID(String studentSetPaperQuestionTypeID) {
        StudentSetPaperQuestionTypeID = studentSetPaperQuestionTypeID;
    }

    public String getIsPassage() {
        return isPassage;
    }

    public void setIsPassage(String isPassage) {
        this.isPassage = isPassage;
    }

    public String MQuestionID;
    public String QuestionType;
    public String QuestionTypeID;
    public String TotalAsk;
    public String ToAnswer;
    public String TotalMark;
}
