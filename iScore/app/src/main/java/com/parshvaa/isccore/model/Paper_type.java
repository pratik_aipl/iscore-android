package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Dipesh on 3/9/2017.
 */
@JsonObject
public class Paper_type implements Serializable {

    @JsonField
    public String PaperTypeName;
    @JsonField
    public String PaperTypeID;


    public String getPaperTypeID() {
        return PaperTypeID;
    }

    public void setPaperTypeID(String paperTypeID) {
        PaperTypeID = paperTypeID;
    }

    public String getPaperTypeName() {
        return PaperTypeName;
    }

    public void setPaperTypeName(String paperTypeName) {
        PaperTypeName = paperTypeName;
    }
}