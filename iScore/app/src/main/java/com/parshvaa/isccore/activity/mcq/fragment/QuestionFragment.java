package com.parshvaa.isccore.activity.mcq.fragment;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.mcq.PagerListner;
import com.parshvaa.isccore.activity.mcq.QuestionPagerActivity;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.McqOption;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.TempMcqTestDtl;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ISccoreWebView;
import com.parshvaa.isccore.utils.PagerSlidingTabStrip;
import com.parshvaa.isccore.utils.Utils;

/**
 * Created by empiere-vaibhav on 12/25/2017.
 */

public class QuestionFragment extends Fragment {
    public View v;
    private static final String ARG_PAGE_NUMBER = "page_number";
    public ISccoreWebView qWbeView, aWebView, bWebView, cWebView, dWebView;
    public LinearLayout lin_a, lin_b, lin_c, lin_d;
    public TextView tv_q, tv_time_subName, tv_a, tv_b, tv_c, tv_d, ans_a, ans_b, ans_c, ans_d, tv_time_quetion, tv_que_no;
    public CheckBox chk_flag;
    public int pageNumber = 0, pagerTabStriNumber = 0;
    public App app;
    private boolean isViewShown = false;
    public static Handler customHandler = new Handler();
    public long updatedTime = 0L;
    public McqQuestion myQuestion;
    public MyDBManager mDb;
    // int pos;
    public PagerListner pagerlistner;
    public static QuestionFragment fragment;
    public boolean visible = false;
    boolean mUserVisibleHint = true;

    public QuestionFragment newInstance(int page) {
        fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        fragment.setHasOptionsMenu(true);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_question, container, false);
        app = App.getInstance();
        mDb = MyDBManager.getInstance(getActivity());
        mDb.open(getActivity());
        pagerlistner = (PagerListner) getActivity();
        chk_flag = v.findViewById(R.id.chk_flag);

        pagerTabStriNumber = ((QuestionPagerActivity) getActivity()).pagerTabStri.pager.getCurrentItem();
        chk_flag.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                App.flags.put(pageNumber, true);
            } else {
                App.flags.put(pageNumber, false);
            }

            PagerSlidingTabStrip.setFlags();

        });

        qWbeView = v.findViewById(R.id.qWebView);
        aWebView = v.findViewById(R.id.aWebView);
        bWebView = v.findViewById(R.id.bWebView);
        cWebView = v.findViewById(R.id.cWebView);
        dWebView = v.findViewById(R.id.dWebView);
        lin_a = v.findViewById(R.id.lin_a);
        lin_b = v.findViewById(R.id.lin_b);
        lin_c = v.findViewById(R.id.lin_c);
        lin_d = v.findViewById(R.id.lin_d);
        tv_q = v.findViewById(R.id.tv_q);
        tv_a = v.findViewById(R.id.tv_a);
        tv_b = v.findViewById(R.id.tv_b);
        tv_c = v.findViewById(R.id.tv_c);
        tv_d = v.findViewById(R.id.tv_d);
        tv_que_no = v.findViewById(R.id.tv_que_no);
        tv_time_quetion = v.findViewById(R.id.tv_time_quetion);
        ans_a = v.findViewById(R.id.a_ans);
        ans_b = v.findViewById(R.id.b_ans);
        ans_c = v.findViewById(R.id.c_ans);
        ans_d = v.findViewById(R.id.d_ans);

        if (QuestionPagerActivity.mcqArray !=null && QuestionPagerActivity.mcqArray.size()>pageNumber){
            myQuestion = QuestionPagerActivity.mcqArray.get(pageNumber);

            if (!isViewShown) {
                setData();
            }
            setListners();
        }
        if (App.mcqdata != null && !App.mcqdata.isTimer()) {
            tv_time_quetion.setVisibility(View.INVISIBLE);
        }

        return v;
    }

    public void stopTimerOnBack() {

        Utils.Log("TAG ", "viewPager Pagenumber :-> " + pageNumber);
        customHandler.removeCallbacks(updateTimerThread);
        try {
            QuestionPagerActivity.timerArray.set(pageNumber, updatedTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void startTimer() {
        if (QuestionPagerActivity.timerArray.get(pageNumber) != null) {
            updatedTime = QuestionPagerActivity.timerArray.get(pageNumber);
        } else {
            updatedTime = 0L;
        }
        customHandler.postDelayed(updateTimerThread, 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        //  Utils.Log("TAG ", "onPause :-> " + pageNumber);
        try {
            QuestionPagerActivity.timerArray.set(pageNumber, updatedTime);
            //timeUpdate((updatedTime / 1000), pageNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onDestroy() {
        QuestionPagerActivity.timerArray.set(pageNumber, updatedTime);
        if (!App.MCQactive) {
            customHandler.removeCallbacks(updateTimerThread);
        }
        super.onDestroy();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);


    }

    // ...
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (App.mcqdata.isTimer()) {
            if (getView() != null) {
                isViewShown = true;
                setListners();
                setData();
            } else {
                isViewShown = false;
            }
            if (menuVisible) {
                visible = true;
                Utils.Log("TAG", "PAge no--> " + pageNumber + " = " + pageNumber + " pagerTabStriNumber : " + pagerTabStriNumber);
                if (QuestionPagerActivity.timerArray.get(QuestionPagerActivity.viewPager.getCurrentItem()) != null) {
                    updatedTime = QuestionPagerActivity.timerArray.get(QuestionPagerActivity.viewPager.getCurrentItem());
                } else {
                    updatedTime = 0L;
                }
                customHandler.postDelayed(updateTimerThread, 0);
            } else {
                visible = false;
                customHandler.removeCallbacks(updateTimerThread);
            }
        }

    }

    public void setTime() {
        if (QuestionPagerActivity.timerArray.size() > 0 && QuestionPagerActivity.timerArray != null) {
            updatedTime += 1000;
            try {
                QuestionPagerActivity.timerArray.set(pageNumber, updatedTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            try {
                tv_time_quetion.setText("" + String.format("%02d", mins) + ":" + String.format("%02d", secs));
            } catch (Exception e) {
                tv_time_quetion.setText("00:00");
                e.printStackTrace();
            }
        }
    }

    public Runnable updateTimerThread = new Runnable() {
        public void run() {
            customHandler.postDelayed(this, 1000);
            setTime();
        }
    };

    public void setData() {
        tv_que_no.setText((pageNumber + 1) + ".");
        if ((myQuestion.getQuestion().contains("<img")) || (myQuestion.getQuestion().contains("<math") || (myQuestion.getQuestion().contains("<table")))) {
            qWbeView.setVisibility(View.VISIBLE);
            qWbeView.loadHtmlFromLocal(myQuestion.getQuestion());
        } else {
            tv_q.setText(Html.fromHtml(myQuestion.getQuestion()));
        }
        for (McqOption op : myQuestion.optionsArray) {
            if (op.getOptionNo().equalsIgnoreCase("1")) {
                lin_a.setVisibility(View.VISIBLE);
                if (op.getOptions().contains("<img") || op.getOptions().contains("<sub") || op.getOptions().contains("<sup") || op.getOptions().contains("<math") || op.getOptions().contains("<table")) {
                    aWebView.setVisibility(View.VISIBLE);
                    aWebView.loadHtmlFromLocal(op.getOptions());
                } else {
                    ans_a.setText(Html.fromHtml(op.getOptions()).toString().trim());
                }
            } else if (op.getOptionNo().equalsIgnoreCase("2")) {
                lin_b.setVisibility(View.VISIBLE);
                if (op.getOptions().contains("<img") || op.getOptions().contains("<sub") || op.getOptions().contains("<sup") || op.getOptions().contains("<math") || op.getOptions().contains("<table")) {
                    bWebView.setVisibility(View.VISIBLE);
                    bWebView.loadHtmlFromLocal(op.getOptions());
                } else {
                    ans_b.setText(Html.fromHtml(op.getOptions()).toString().trim());
                }
            } else if (op.getOptionNo().equalsIgnoreCase("3") && !op.getOptionNo().equals("")) {
                lin_c.setVisibility(View.VISIBLE);
                if (op.getOptions().contains("<img") || op.getOptions().contains("<sub") || op.getOptions().contains("<sup") || op.getOptions().contains("<math") || op.getOptions().contains("<table")) {
                    cWebView.setVisibility(View.VISIBLE);
                    cWebView.loadHtmlFromLocal(op.getOptions());
                } else {
                    ans_c.setText(Html.fromHtml(op.getOptions()).toString().trim());
                }
            } else if (op.getOptionNo().equalsIgnoreCase("4")) {
                lin_d.setVisibility(View.VISIBLE);
                if (op.getOptions().contains("<img") || op.getOptions().contains("<sub") || op.getOptions().contains("<sup") || op.getOptions().contains("<math") || op.getOptions().contains("<table")) {
                    dWebView.setVisibility(View.VISIBLE);
                    ans_d.setVisibility(View.GONE);
                    dWebView.loadHtmlFromLocal(op.getOptions());
                } else {
                    ans_d.setGravity(Gravity.CENTER_VERTICAL);
                    ans_d.setText(Html.fromHtml(op.getOptions()).toString().trim());
                }
            }
        }

        if (myQuestion.selectedAns != -1) {
            switch (myQuestion.selectedAns) {
                case 1:
                    tv_a.setBackgroundResource(R.drawable.circle_button_blue);
                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_a.setTextColor(Color.WHITE);
                    tv_b.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    break;

                case 2:
                    tv_b.setBackgroundResource(R.drawable.circle_button_blue);
                    tv_a.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_b.setTextColor(Color.WHITE);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    break;
                case 3:
                    tv_c.setBackgroundResource(R.drawable.circle_button_blue);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_b.setTextColor(Color.BLACK);
                    tv_a.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.WHITE);
                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    break;
                case 4:
                    tv_d.setBackgroundResource(R.drawable.circle_button_blue);
                    tv_b.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_a.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.WHITE);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    break;
            }
        }

    }

    public void setListners() {
        lin_a.setOnClickListener(view -> {
            tv_a.setBackgroundResource(R.drawable.circle_button_blue);
            tv_b.setBackgroundColor(Color.TRANSPARENT);
            tv_a.setTextColor(Color.WHITE);
            tv_b.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.BLACK);
            tv_c.setBackgroundColor(Color.TRANSPARENT);
            tv_d.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 1;
            if (myQuestion.optionsArray.size() > 0)
                selectAns(myQuestion.optionsArray.get(0).getMCQOPtionID(), myQuestion.optionsArray.get(0).isCorrect);

        });
        ans_a.setOnClickListener(view -> {
            tv_a.setBackgroundResource(R.drawable.circle_button_blue);

            tv_b.setBackgroundColor(Color.TRANSPARENT);
            tv_a.setTextColor(Color.WHITE);
            tv_b.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.BLACK);
            tv_c.setBackgroundColor(Color.TRANSPARENT);
            tv_d.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 1;
            if (myQuestion.optionsArray.size() > 0)
                selectAns(myQuestion.optionsArray.get(0).getMCQOPtionID(), myQuestion.optionsArray.get(0).isCorrect);

        });
        aWebView.setOnTouchListener((view, motionEvent) -> {
            if (view.getId() == R.id.aWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                tv_a.setBackgroundResource(R.drawable.circle_button_blue);

                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_a.setTextColor(Color.WHITE);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 1;
                if (myQuestion.optionsArray.size() > 0)
                    selectAns(myQuestion.optionsArray.get(0).getMCQOPtionID(), myQuestion.optionsArray.get(0).isCorrect);

            }
            return false;
        });
        lin_b.setOnClickListener(view -> {
            tv_b.setBackgroundResource(R.drawable.circle_button_blue);
            tv_a.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.BLACK);
            tv_b.setTextColor(Color.WHITE);
            tv_a.setBackgroundColor(Color.TRANSPARENT);
            tv_c.setBackgroundColor(Color.TRANSPARENT);
            tv_d.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 2;
            if (myQuestion.optionsArray.size() > 1)
                selectAns(myQuestion.optionsArray.get(1).getMCQOPtionID(), myQuestion.optionsArray.get(1).isCorrect);

        });
        ans_b.setOnClickListener(view -> {
            tv_b.setBackgroundResource(R.drawable.circle_button_blue);
            tv_a.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.BLACK);
            tv_b.setTextColor(Color.WHITE);
            tv_a.setBackgroundColor(Color.TRANSPARENT);
            tv_c.setBackgroundColor(Color.TRANSPARENT);
            tv_d.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 2;
            if (myQuestion.optionsArray.size() > 1)
                selectAns(myQuestion.optionsArray.get(1).getMCQOPtionID(), myQuestion.optionsArray.get(1).isCorrect);

        });
        bWebView.setOnTouchListener((view, motionEvent) -> {
            if (view.getId() == R.id.bWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                tv_b.setBackgroundResource(R.drawable.circle_button_blue);
                tv_a.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_b.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 2;
                if (myQuestion.optionsArray.size() > 1)
                    selectAns(myQuestion.optionsArray.get(1).getMCQOPtionID(), myQuestion.optionsArray.get(1).isCorrect);

            }
            return false;
        });

        lin_c.setOnClickListener(view -> {
            tv_c.setBackgroundResource(R.drawable.circle_button_blue);
            tv_a.setBackgroundColor(Color.TRANSPARENT);
            tv_b.setTextColor(Color.BLACK);
            tv_a.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.WHITE);
            tv_b.setBackgroundColor(Color.TRANSPARENT);
            tv_d.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 3;
            if (myQuestion.optionsArray.size() > 2)
                selectAns(myQuestion.optionsArray.get(2).getMCQOPtionID(), myQuestion.optionsArray.get(2).isCorrect);

        });
        ans_c.setOnClickListener(view -> {
            tv_c.setBackgroundResource(R.drawable.circle_button_blue);
            tv_a.setBackgroundColor(Color.TRANSPARENT);
            tv_b.setTextColor(Color.BLACK);
            tv_a.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.WHITE);
            tv_b.setBackgroundColor(Color.TRANSPARENT);
            tv_d.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 3;
            if (myQuestion.optionsArray.size() > 2)
                selectAns(myQuestion.optionsArray.get(2).getMCQOPtionID(), myQuestion.optionsArray.get(2).isCorrect);

        });

        cWebView.setOnTouchListener((view, motionEvent) -> {
            if (view.getId() == R.id.cWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                tv_c.setBackgroundResource(R.drawable.circle_button_blue);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.WHITE);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 3;
                if (myQuestion.optionsArray.size() > 2)
                    selectAns(myQuestion.optionsArray.get(2).getMCQOPtionID(), myQuestion.optionsArray.get(2).isCorrect);

            }
            return false;
        });
        lin_d.setOnClickListener(view -> {
            tv_d.setBackgroundResource(R.drawable.circle_button_blue);
            tv_b.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.BLACK);
            tv_a.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.WHITE);
            tv_a.setBackgroundColor(Color.TRANSPARENT);
            tv_b.setBackgroundColor(Color.TRANSPARENT);
            tv_c.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 4;
            if (myQuestion.optionsArray.size() == 4)
                selectAns(myQuestion.optionsArray.get(3).getMCQOPtionID(), myQuestion.optionsArray.get(3).isCorrect);

        });

        ans_d.setOnClickListener(view -> {
            tv_d.setBackgroundResource(R.drawable.circle_button_blue);
            tv_b.setTextColor(Color.BLACK);
            tv_c.setTextColor(Color.BLACK);
            tv_a.setTextColor(Color.BLACK);
            tv_d.setTextColor(Color.WHITE);
            tv_a.setBackgroundColor(Color.TRANSPARENT);
            tv_b.setBackgroundColor(Color.TRANSPARENT);
            tv_c.setBackgroundColor(Color.TRANSPARENT);
            myQuestion.selectedAns = 4;
            if (myQuestion.optionsArray.size() == 4)
                selectAns(myQuestion.optionsArray.get(3).getMCQOPtionID(), myQuestion.optionsArray.get(3).isCorrect);

        });
        dWebView.setOnTouchListener((view, motionEvent) -> {
            if (view.getId() == R.id.dWebView && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                tv_d.setBackgroundResource(R.drawable.circle_button_blue);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);

                myQuestion.selectedAns = 4;
                if (myQuestion.optionsArray.size() == 4)
                    selectAns(myQuestion.optionsArray.get(3).getMCQOPtionID(), myQuestion.optionsArray.get(3).isCorrect);

            }
            return false;
        });
    }

    public void selectAns(String selectedAns, String isCorrect) {
        pagerlistner.onPageVisible(pageNumber);
        TempMcqTestDtl tempmcqdtl = new TempMcqTestDtl();
        tempmcqdtl.setQuestionID(myQuestion.MCQQuestionID);
        tempmcqdtl.setAnswerID(selectedAns);
        tempmcqdtl.setIsAttempt("1");
        myQuestion.isRight = isCorrect.equalsIgnoreCase("1");
        Cursor c = mDb.getAllRows(DBQueries.getAllMcqDtl(tempmcqdtl));
        if (c != null && c.moveToNext()) {
            mDb.updateRow(tempmcqdtl, Constant.temp_mcq_test_dtl, "QuestionID=" + myQuestion.MCQQuestionID);
        } else {
            mDb.insertTempRow(tempmcqdtl, Constant.temp_mcq_test_dtl);
        }

    }
}
