package com.parshvaa.isccore.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class QueryCallUsActivity extends BaseActivity implements AsynchTaskListner {

    public ImageView img_callNow;
    public Button btn_send;
    public EditText et_message;
    //    public QueryCallUsActivity instance;
    public App app;
    public ImageView img_back;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_query_call_us);
        Utils.logUser();

        app = App.getInstance();
        img_callNow = findViewById(R.id.img_callNow);
        btn_send = findViewById(R.id.btn_send);
        et_message = findViewById(R.id.et_message);
        img_callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = "+917710012112";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });

        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Query");

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_message.getText().toString().equals("")) {
                    Utils.showToast("Please fill the message", QueryCallUsActivity.this);
                } else {
                    showProgress(true, false);
                    new CallRequest(QueryCallUsActivity.this).querySendMail(MainUser.getEmailID(), et_message.getText().toString());
                }

            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false, true);
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case qurySendMail:
                    showProgress(false, true);
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            et_message.setText("");
                            Utils.showAlert("Thank you for contacting us. We will shortly get back to you.", this);
                        } else {
                            Utils.showAlert(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
