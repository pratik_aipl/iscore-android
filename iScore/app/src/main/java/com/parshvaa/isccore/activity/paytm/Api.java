package com.parshvaa.isccore.activity.paytm;

import com.parshvaa.isccore.utils.Constant;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Belal on 1/10/2018.
 */

public interface Api {

    //this is the URL of the paytm folder that we added in the server
    //make sure you are using your ip else it will not work

    //http://test.escore.parshvaa.com/web_services/version_39/Paytm/
    String BASE_URL = Constant.BASIC_URL + Constant.URL_VERSION + "Paytm/";


    @FormUrlEncoded
    @POST("generateChecksum.php")
    Call<Checksum> getChecksum(
            @Field("MID") String mId,
            @Field("ORDER_ID") String orderId,
            @Field("CUST_ID") String custId,
            @Field("INDUSTRY_TYPE_ID") String industryTypeId,
            @Field("CHANNEL_ID") String channelId,
            @Field("TXN_AMOUNT") String txnAmount,
            @Field("WEBSITE") String website,
            @Field("EMAIL") String EMAIL,
            @Field("MOBILE_NO") String MOBILE_NO,
            @Field("CALLBACK_URL") String callbackUrl

    );
    @FormUrlEncoded
    @POST("verifyChecksum.php")
    Call<VerifyChecksum> VerifyChecksum(
            @Field("MID") String mId,
            @Field("ORDER_ID") String orderId,
            @Field("CUST_ID") String custId,
            @Field("INDUSTRY_TYPE_ID") String industryTypeId,
            @Field("CHANNEL_ID") String channelId,
            @Field("TXN_AMOUNT") String txnAmount,
            @Field("WEBSITE") String website,
            @Field("EMAIL") String EMAIL,
            @Field("MOBILE_NO") String MOBILE_NO,
            @Field("CALLBACK_URL") String callbackUrl,
            @Field("CHECKSUMHASH") String checksum

    );
}
