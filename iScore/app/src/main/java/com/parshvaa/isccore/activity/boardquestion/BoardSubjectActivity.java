package com.parshvaa.isccore.activity.boardquestion;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class BoardSubjectActivity extends BaseActivity {
    private RecyclerView recyclerView;
    private BoardSubjectAdapter mAdapter;
    public ImageView img_back;
    public TextView tv_title;
    ArrayList<Subject> subjectArray;
    public static MyDBManager mDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_select_subject);
        Utils.logUser();
        App.subObj = new Subject();
        recyclerView = findViewById(R.id.recycler_view);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Select Subject");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        try {
            String bordId=mySharedPref.getBoardID();
            String mediumId=mySharedPref.getMediumID();
            String standardId=mySharedPref.getStandardID();

            subjectArray = (ArrayList) new CustomDatabaseQuery(this, new Subject())
                    .execute(new String[]{DBNewQuery.getAllSubjectGenrate(bordId, mediumId, standardId)}).get();
        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        setupRecyclerView();


    }


    private void setupRecyclerView() {
        final Context context = recyclerView.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new BoardSubjectAdapter(subjectArray, context);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));

        // prepareMovieData();
    }

}
