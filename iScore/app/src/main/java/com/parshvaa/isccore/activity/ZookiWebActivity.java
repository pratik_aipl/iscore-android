package com.parshvaa.isccore.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Utils;

public class ZookiWebActivity extends BaseActivity {
    public WebView web_newslastter;
    public App app;
    public SharedPreferences shared;
    String url = "";
    public ImageView img_back;
    public TextView tv_title;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_zooki);
        Utils.logUser();
        showProgress(true, true);//Utils.showProgressDialog(ZookiWebActivity.this);
        try {
            Intent i = getIntent();
            url = i.getExtras().getString("url");
            Utils.Log("TAG", "URL::->" + url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        web_newslastter = findViewById(R.id.web_newslastter);
        web_newslastter.setWebViewClient(new myWebClient());
        WebSettings settings = web_newslastter.getSettings();
        //settings.setMinimumFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);

        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setDomStorageEnabled(true);

        try {
            if (!url.equals("")) {
                Utils.Log("TAG", "URL::->" + url);
                web_newslastter.loadUrl(url);
            } else {
                web_newslastter.loadUrl("https://parshvaa.com/blog_copy/");

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            web_newslastter.loadUrl("https://parshvaa.com/blog_copy/");
        }
    }


    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                showProgress(false, true);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

}
