package com.parshvaa.isccore.activity.textbook;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class Chapter implements Serializable {
    @JsonField
    public String ChapterID = "";
    @JsonField
    public String ChapterName = "";
    @JsonField
    public String QFile = "";

    public String getQFile() {
        return QFile;
    }

    public void setQFile(String QFile) {
        this.QFile = QFile;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }
}
