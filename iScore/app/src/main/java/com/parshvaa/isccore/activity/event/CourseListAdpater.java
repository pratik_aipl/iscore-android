package com.parshvaa.isccore.activity.event;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.CourseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CourseListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<CourseModel> subCatList;



    public CourseListAdpater(Context context, List<CourseModel> subCatLists) {
        this.context = context;
        this.subCatList = subCatLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.event_course_row_item, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CourseModel listModel = subCatList.get(position);


        holder.tvCourseName.setText("> "+listModel.getTitle());
        holder.tvCourseDate.setText(listModel.getStartDateTime());
    }


    public void data() {

    }

    @Override
    public int getItemCount() {
        return subCatList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_course_name)
        TextView tvCourseName;
        @BindView(R.id.tv_course_date)
        TextView tvCourseDate;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}