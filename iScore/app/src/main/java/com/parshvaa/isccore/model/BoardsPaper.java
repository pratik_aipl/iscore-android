package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by admin on 2/28/2017.
 */
@JsonObject
public class BoardsPaper implements Serializable {

    @JsonField
    public String BoardPaperID;
    @JsonField
    public String BoardPaperName;
    @JsonField
    public String CreatedBy;
    @JsonField
    public String CreatedOn;
    @JsonField
    public String ModifiedBy;
    @JsonField
    public String ModifiedOn;

    public BoardsPaper() {

    }

    public BoardsPaper(String BoardPaperID, String BoardPaperName, String CreatedBy,
                       String CreatedOn, String ModifiedBy, String ModifiedOn) {

        this.BoardPaperID = BoardPaperID;
        this.BoardPaperName = BoardPaperName;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;

    }

    public String getBoardPaperID() {
        return BoardPaperID;
    }

    public void setBoardPaperID(String boardPaperID) {
        BoardPaperID = boardPaperID;
    }

    public String getBoardPaperName() {
        return BoardPaperName;
    }

    public void setBoardPaperName(String boardPaperName) {
        BoardPaperName = boardPaperName;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
