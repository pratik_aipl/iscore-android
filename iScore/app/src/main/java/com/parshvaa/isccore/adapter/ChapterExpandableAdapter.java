package com.parshvaa.isccore.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.mcq.ChapterMcqActivity;
import com.parshvaa.isccore.custominterface.ChapterInterface;
import com.parshvaa.isccore.model.Chapter;
import com.parshvaa.isccore.observscroll.ObservableExpandableListView;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.ChapterAccuracy;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.ProgressBarAnimation;

import java.util.ArrayList;
import java.util.List;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class ChapterExpandableAdapter extends BaseExpandableListAdapter implements ExpandableListView.OnGroupExpandListener {
    private static final String TAG = "ChapterExpandableAdapte";
    private Context mContext;
    public ObservableExpandableListView mExpandableListView;
    private List<ChapterAccuracy> mGroupCollection;
    private int[] groupStatus;
    public List<String> iDS;
    public List<String> ParentiDS;
    public List<String> numbers;
    Boolean isActive = false;
    public ChapterInterface cInterface;
    public CheckBox cb;
    public MySharedPref mySharedPref;
//    public ChildHolder childHolder;
//    public GroupHolder groupHolder;


    public ChapterExpandableAdapter(ChapterMcqActivity pContext, ObservableExpandableListView pExpandableListView,
                                    List<ChapterAccuracy> pGroupCollection, CheckBox cb) {
        mContext = pContext;
        mySharedPref = new MySharedPref(mContext);
        mGroupCollection = pGroupCollection;
        mExpandableListView = pExpandableListView;
        groupStatus = new int[mGroupCollection.size()];
        cInterface = pContext;
        this.cb = cb;
        iDS = new ArrayList<>();
        ParentiDS = new ArrayList<>();
        numbers = new ArrayList<>();
        setListEvent();
    }

    private void setListEvent() {
        mExpandableListView.setOnGroupExpandListener(new ObservableExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                groupStatus[groupPosition] = 1;

                long packedPosition = ExpandableListView.getPackedPositionForGroup(groupPosition);
                int flatPosition = mExpandableListView.getFlatListPosition(packedPosition);
                int first = mExpandableListView.getFirstVisiblePosition();
                try {

                    LinearLayout lin = (LinearLayout) mExpandableListView.getChildAt(flatPosition - first);
                    CardView v = (CardView) lin.getChildAt(1);
                    RelativeLayout rel = (RelativeLayout) v.getChildAt(0);
                    ImageView img = (ImageView) rel.getChildAt(1);
                    RotateAnimation rotate = new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
                    rotate.setDuration(300);
                    rotate.setFillAfter(true);
                    img.setAnimation(rotate);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        mExpandableListView.setOnGroupCollapseListener(groupPosition -> {
            groupStatus[groupPosition] = 0;
            long packedPosition = ExpandableListView.getPackedPositionForGroup(groupPosition);
            int flatPosition = mExpandableListView.getFlatListPosition(packedPosition);
            int first = mExpandableListView.getFirstVisiblePosition();
            try {
                LinearLayout lin = (LinearLayout) mExpandableListView.getChildAt(flatPosition - first);
                CardView v = (CardView) lin.getChildAt(1);
                RelativeLayout rel = (RelativeLayout) v.getChildAt(0);
                ImageView img = (ImageView) rel.getChildAt(1);
                RotateAnimation rotate = new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
                rotate.setDuration(300);
                rotate.setFillAfter(true);
                img.setAnimation(rotate);
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    @Override
    public Object getChild(int arg0, int arg1) {
        return mGroupCollection.get(arg0).subChapters.get(arg1);

    }

    @Override
    public long getChildId(int arg0, int arg1) {
        return arg1;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean arg2, View convertView, ViewGroup parent) {
        ChildHolder childHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.chapter_chiled_item, null);
            childHolder = new ChildHolder();
            childHolder.checkBox = convertView.findViewById(R.id.chkSelected);
            childHolder.tvName = convertView.findViewById(R.id.tvName);
            convertView.invalidate();
            convertView.setTag(R.layout.chapter_chiled_item, childHolder);
            View parentGroup = (View) convertView.getParent();
            if (parentGroup != null)
                parentGroup.setBackgroundColor(Color.CYAN);
        } else {
            childHolder = (ChildHolder) convertView.getTag(R.layout.chapter_chiled_item);

        }


        childHolder.checkBox.setText(mGroupCollection.get(groupPosition).subChapters.get(childPosition).getChapterName());
        childHolder.checkBox.setOnCheckedChangeListener(null);


        if (mGroupCollection.get(groupPosition).subChapters.get(childPosition).getIsDemo().equalsIgnoreCase("1") || mySharedPref.getVersion().equals("0")) {
            childHolder.checkBox.setEnabled(true);
            childHolder.tvName.setVisibility(View.GONE);
            childHolder.tvName.setOnClickListener(null);
            if (iDS.contains(mGroupCollection.get(groupPosition).subChapters.get(childPosition).getChapterID())) {
                childHolder.checkBox.setChecked(true);
            } else {
                childHolder.checkBox.setChecked(false);
            }
        } else {
            childHolder.checkBox.setEnabled(false);
            childHolder.checkBox.setChecked(false);
            childHolder.tvName.setVisibility(View.VISIBLE);
            childHolder.tvName.setOnClickListener(view -> {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Alert! To view this please purchase the complete version.");
                builder1.setCancelable(true);
                builder1.setNegativeButton("Close", (dialog, i) -> {
                    dialog.cancel();
                });
                builder1.setPositiveButton(
                        "Subscribe",
                        (dialog, id) -> {
                            dialog.cancel();
                            mContext.startActivity(new Intent(mContext, ChooseOptionActivity.class));
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            });

        }

        childHolder.checkBox.setOnCheckedChangeListener((compoundButton, b) -> {

            String Pid = mGroupCollection.get(groupPosition).getChapterID();
            String id = mGroupCollection.get(groupPosition).subChapters.get(childPosition).getChapterID();
            if (b) {
                if (!iDS.contains(id)) {
                    iDS.add(id);
                    boolean isSelected = checkAllSelected();
                    cInterface.onCheckBoxDeselect(isSelected);
                }
                if (checkParentSelected(groupPosition)) {
                    if (!ParentiDS.contains(Pid)) {
                        ParentiDS.add(Pid);
                        boolean isSelected = checkAllSelected();
                        cInterface.onCheckBoxDeselect(isSelected);
                    }
                }
            } else {
                cInterface.onCheckBoxDeselect(false);
                ParentiDS.remove(Pid);
                iDS.remove(id);
            }
            notifyDataSetChanged();

        });


        return convertView;
    }

    @Override
    public int getChildrenCount(int arg0) {
        return mGroupCollection.get(arg0).subChapters.size();
    }

    @Override
    public Object getGroup(int arg0) {
        return mGroupCollection.get(arg0);
    }

    @Override
    public int getGroupCount() {
        return mGroupCollection.size();
    }

    @Override
    public long getGroupId(int arg0) {
        return arg0;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View view, ViewGroup parent) {

        GroupHolder groupHolder;
        if (view == null) {

            LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.parent_chapter_item, null);

            //view = LayoutInflater.from(mContext).inflate(R.layout.parent_chapter_item, null);
            groupHolder = new GroupHolder();

            groupHolder.mViewClick = view.findViewById(R.id.mViewClick);
            groupHolder.chk_group = view.findViewById(R.id.chk_group);
            groupHolder.img_arrow = view.findViewById(R.id.img_arrow);
            groupHolder.tv_group_title = view.findViewById(R.id.tv_group_title);
            groupHolder.tv_accuracy = view.findViewById(R.id.tv_accuracy);
            groupHolder.pbr_accuracy = view.findViewById(R.id.pbr_accuracy);

            view.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) view.getTag();

        }

        groupHolder.tv_group_title.setText(mGroupCollection.get(groupPosition).getChapterName());
        groupHolder.chk_group.setOnCheckedChangeListener(null);

        groupHolder.tv_accuracy.setText("Accuracy " + mGroupCollection.get(groupPosition).getAcuracy() + "%");
        ProgressBarAnimation anim = new ProgressBarAnimation(groupHolder.pbr_accuracy, 0, mGroupCollection.get(groupPosition).getAcuracy());
        anim.setDuration(1000);
        groupHolder.pbr_accuracy.startAnimation(anim);

        if (mGroupCollection.get(groupPosition).getIsDemo().equalsIgnoreCase("1") || mySharedPref.getVersion().equals("0")) {
            groupHolder.chk_group.setEnabled(true);
            groupHolder.tv_group_title.setEnabled(true);
//            groupHolder.mViewClick.setOnClickListener(null);
            if (ParentiDS.contains(mGroupCollection.get(groupPosition).getChapterID())) {
                groupHolder.chk_group.setChecked(true);
            } else {
                groupHolder.chk_group.setChecked(false);
            }
        } else {
            groupHolder.chk_group.setEnabled(false);
            groupHolder.chk_group.setChecked(false);
            groupHolder.tv_group_title.setEnabled(false);
        }

        groupHolder.chk_group.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Chapter group = new Chapter();
            CheckBox chk = (CheckBox) buttonView;
            group.setChapterName(chk.getText().toString());

            String id = mGroupCollection.get(groupPosition).getChapterID();
            if (isChecked) {
                if (!ParentiDS.contains(id)) {
                    ParentiDS.add(id);
                    boolean isSelected = checkAllSelected();
                    cInterface.onCheckBoxDeselect(isSelected);
                }
                for (ChapterAccuracy c : mGroupCollection.get(groupPosition).subChapters) {
                    iDS.add(c.getChapterID());
                }
            } else {
                deSelectAll();
            }
            notifyDataSetChanged();
        });

        return view;
    }

    @Override
    public void onGroupExpand(int groupPosition) {

    }

    public String getSelectedChapters() {
        return android.text.TextUtils.join(",", iDS);
    }

    public int getSelectedChaptersCount() {
        return iDS.size();
    }

    class GroupHolder {
        CheckBox chk_group;
        RelativeLayout mViewClick;
        TextView tv_group_title, tv_accuracy;
        ProgressBar pbr_accuracy;
        ImageView img_arrow;
    }

    class ChildHolder {
        CheckBox checkBox;
        TextView tvName;
    }

    public void selectAll() {
        if (mySharedPref.getVersion().equals("0")) {
            iDS.clear();
            for (ChapterAccuracy c : mGroupCollection) {
                for (ChapterAccuracy s : c.subChapters) {
                    iDS.add(s.getChapterID());
                }
                ParentiDS.add(c.getChapterID());
            }
            this.notifyDataSetChanged();
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
            builder1.setMessage("Alert! To view this please purchase the complete version.");
            builder1.setCancelable(true);
            builder1.setNegativeButton("Close", (dialog, i) -> {
                dialog.cancel();
            });
            builder1.setPositiveButton(
                    "Subscribe",
                    (dialog, id) -> {
                        dialog.cancel();
                        mContext.startActivity(new Intent(mContext, ChooseOptionActivity.class));
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }


    }

    public void deSelectAll() {
        iDS.clear();
        ParentiDS.clear();
        this.notifyDataSetChanged();
    }

    public boolean checkAllSelected() {
        for (int i = 0; i < mGroupCollection.size(); i++) {
            if (!ParentiDS.contains(mGroupCollection.get(i).getChapterID())) {
                return false;
            }
        }
        return true;

    }

    public boolean checkParentSelected(int pos) {
        for (ChapterAccuracy s : mGroupCollection.get(pos).subChapters) {
            if (!iDS.contains(s.getChapterID())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return false;
    }


}