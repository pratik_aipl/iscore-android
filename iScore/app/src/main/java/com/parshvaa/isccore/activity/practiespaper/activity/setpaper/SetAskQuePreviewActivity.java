package com.parshvaa.isccore.activity.practiespaper.activity.setpaper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.isccore.activity.practiespaper.adapter.SetPaperTypePrivewAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.OnTaskCompleted;
import com.parshvaa.isccore.model.ClassQuestionPaperSubQuestion;
import com.parshvaa.isccore.model.GeneratePaper;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.SetPaperQuestionTypes;
import com.parshvaa.isccore.model.StudentQuestionPaper;
import com.parshvaa.isccore.model.StudentQuestionPaperChapter;
import com.parshvaa.isccore.model.StudentSetPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperQuestionType;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.SubQueKey;
import com.parshvaa.isccore.tempmodel.SubQueLabel;
import com.parshvaa.isccore.db.DBConstant;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SetAskQuePreviewActivity extends BaseActivity implements OnTaskCompleted {
    private List<GeneratePaper> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SetPaperTypePrivewAdapter mAdapter;
    public ImageView img_back;
    public TextView tv_title;
    public Button btn_next;
    public SetAskQuePreviewActivity instance;
    public long StudentQuestionPaperID = -1, DetailID = -1;
    public ArrayList<String> iDSArray = new ArrayList<String>();
    public Spinner etDuration;
    public boolean onClick = false;
    public ClassQuestionPaperSubQuestion subQuesObj;
    public SubQueLabel subQuesLabelObj;
    public SubQueKey subQueskeyObj;

    public ArrayList<ClassQuestionPaperSubQuestion> subQuesArray = new ArrayList<>();
    public ArrayList<SubQueLabel> subQuesLabelArray = new ArrayList<>();

    public static HashMap<String, ArrayList<ClassQuestionPaperSubQuestion>> stringArrayListHashMap = new HashMap<>();
    private ArrayList<String> mKeys;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_set_papertype_privew);
        Utils.logUser();


        instance = this;
        recyclerView = findViewById(R.id.recycler_view);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        etDuration = findViewById(R.id.etDuration);
        tv_title.setText("Confirm Question Types");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etDuration.getSelectedItem().toString().equals("Select Time")) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Please Select Time", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                } else {
                    if (!onClick) {
                        onClick = true;
//                        if (!mySharedPref.isFirstGeneratePaper()) {
                            mySharedPref.setIsGeneratePaper(true);
                            Bundle bundle = new Bundle();
                            mFirebaseAnalytics.logEvent("GENERATEPAPER", bundle);
//                        }
                        new InserRecord(instance).execute();
                    }
                }

            }
        });
        setupRecyclerView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        onClick = false;
    }

    private void prepareMovieData() {
        GeneratePaper movie = new GeneratePaper("10", true);
        movieList.add(movie);
        movie = new GeneratePaper("10", true);
        movieList.add(movie);

    }

    private void setupRecyclerView() {
        final Context context = recyclerView.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new SetPaperTypePrivewAdapter(App.practiesPaperObj.setPaperQuestionTypesArray, context);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));

    }

    //For Set Paper

    public class InserRecord extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        public OnTaskCompleted listener;
        public float mark = 0;
        public String MPSQID = "0";
        String examTypesID = "", msg = "";

        private InserRecord(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            this.listener = (OnTaskCompleted) context;

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {


            StudentQuestionPaper sPaper = new StudentQuestionPaper();
            sPaper.setPaperTypeID(App.practiesPaperObj.getPaperTypeID() + "");
            sPaper.setSubjectID(App.practiesPaperObj.getOldSubjectID() + "");
            sPaper.setDuration(etDuration.getSelectedItem().toString());
            sPaper.setStudentID(MainUser.getStudentId() + "");
            sPaper.setTotalMarks(mAdapter.getTotalMarks());


            StudentQuestionPaperID = mDb.insertRow(sPaper, DBConstant.student_question_paper);
            App.practiesPaperObj.setStudentQuestionPaperID(String.valueOf(StudentQuestionPaperID));
            List<String> list = new ArrayList<String>(Arrays.asList(App.practiesPaperObj.getChapterID().split(",")));
            for (int i = 0; i < list.size(); i++) {
                //for (String cID : list) {
                StudentQuestionPaperChapter cp = new StudentQuestionPaperChapter();
                cp.setChapterID(list.get(i));
                try {
                    cp.setStudentQuestionPaperID((int) StudentQuestionPaperID);
                } catch (NumberFormatException e) {
                    cp.setStudentQuestionPaperID(-1);
                    e.printStackTrace();
                }

                mDb.insertRow(cp, DBConstant.student_question_paper_chapter);
            }


            iDSArray.clear();
            long ids;
            for (SetPaperQuestionTypes types : App.practiesPaperObj.setPaperQuestionTypesArray) {

                StudentSetPaperQuestionType SetPaperTypes = new StudentSetPaperQuestionType();
                SetPaperTypes.setStudentQuestionPaperID(String.valueOf(StudentQuestionPaperID));
                SetPaperTypes.setQuestionTypeID(types.getQuestionTypeID());
                SetPaperTypes.setTotalAsk(String.valueOf(types.getNoOfQuestion()));
                SetPaperTypes.setToAnswer(String.valueOf(types.getNoOfAnswer()));
                SetPaperTypes.setTotalMark(String.valueOf(Double.parseDouble(types.getMarks()) * Double.parseDouble(String.valueOf(types.getNoOfAnswer()))));

                ids = mDb.insertRow(SetPaperTypes, DBConstant.student_set_paper_question_type);
                String TempId = String.valueOf(ids);
                iDSArray.add(TempId);


                Cursor Ques = mDb.getAllRows(DBQueries.getRandomSubQuestion(App.practiesPaperObj.getChapterID(), types.getQuestionTypeID(), String.valueOf(types.getNoOfQuestion())));
                outerloop:
                if (Ques != null && Ques.moveToFirst()) {

                    do {
                        subQuesLabelArray.clear();

                        StudentSetPaperDetail studentSetPaperDetail = new StudentSetPaperDetail();
                        studentSetPaperDetail.setStudentSetPaperQuestionTypeID((int) ids);
                        studentSetPaperDetail.setMQuestionID(Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID")));
                        // Utils.Log("Cursor studentSetPaperDetail", "StudentSetPaperQuestionTypeID --->" + studentSetPaperDetail.getStudentSetPaperQuestionTypeID());
                        //  Utils.Log("Cursor studentSetPaperDetail", "MQuestionID--->" + Ques.getInt(Ques.getColumnIndexOrThrow("MQuestionID")));

                        DetailID = mDb.insertRow(studentSetPaperDetail, DBConstant.student_set_paper_detail);
                        Cursor cIsPassage = mDb.getAllRows(DBQueries.checkQusTypeisPassage(Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID"))));

                        int isPassage = 0;
                        if (cIsPassage != null && cIsPassage.moveToFirst()) {
                            isPassage = cIsPassage.getInt(0);
                            cIsPassage.close();
                        }
                        if (isPassage == 1) {
                            Cursor clabel = mDb.getAllRows(DBQueries.getLabel(Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID"))));
                            //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                            if (clabel != null && clabel.moveToFirst()) {

                                do {

                                    subQuesLabelObj = new SubQueLabel();
                                    subQuesLabelObj.setLabel(clabel.getString(clabel.getColumnIndexOrThrow("Label")));

                                    mark = clabel.getInt(clabel.getColumnIndexOrThrow("Mark"));
                                    Log.i("TAg", "Label DO WHILE :-> " + clabel.getString(clabel.getColumnIndexOrThrow("Label")));
                                    subQueskeyObj = new SubQueKey();
                                    while (0 < mark) {

                                        Cursor csqi = mDb.getAllRows(DBQueries.getPassageSubquetions(MPSQID,
                                                Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID")),
                                                types.getQuestionTypeID(), String.valueOf(mark), clabel.getString(clabel.getColumnIndexOrThrow("PassageSubQuestionTypeID"))));
                                        if (csqi != null) {
                                            boolean isExistes = false;
                                            Log.i("TAg", "IN  WHILE csqi :-> " + mark);
                                            try {


                                                csqi.moveToFirst();
                                                if (MPSQID.equals("0")) {
                                                    MPSQID = csqi.getString(csqi.getColumnIndex("MPSQID"));
                                                } else {
                                                    MPSQID = MPSQID + "," + csqi.getString(csqi.getColumnIndex("MPSQID"));
                                                }
                                                //Utils.Log("ID :->", csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")));
                                                mark = mark - csqi.getFloat(csqi.getColumnIndexOrThrow("Marks"));
                                                //  Utils.Log("In IF :-> ", mark + "");
                                                subQuesObj = new ClassQuestionPaperSubQuestion();
                                                subQuesObj.setMPSQID(csqi.getString(csqi.getColumnIndex("MPSQID")));
                                                subQuesObj.setDetailID(String.valueOf(DetailID));
                                                subQuesObj.setPaperID(String.valueOf(StudentQuestionPaperID));
                                                for (SubQueLabel lblObj : subQuesLabelArray) {

                                                    if (lblObj.getLabel().equals(clabel.getString(clabel.getColumnIndexOrThrow("Label")))) {
                                                        for (SubQueKey keyObj : lblObj.keyArray) {
                                                            if (keyObj.getKey().equals(csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")))) {
                                                                keyObj.internalArray.add(subQuesObj);
                                                                // lblObj.keyArray.add(keyObj);
                                                                isExistes = true;
                                                                break;
                                                            } else {
                                                                isExistes = false;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (!isExistes) {
                                                    subQueskeyObj.setKey(csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")));
                                                    subQueskeyObj.internalArray.add(subQuesObj);
                                                }
                                                if (subQuesLabelArray.size() <= 0 || subQuesLabelArray == null) {
                                                    subQuesLabelObj.keyArray.add(subQueskeyObj);
                                                    subQuesLabelArray.add(subQuesLabelObj);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Utils.Log("In CATCH :-> ", mark + "");
                                                mDb.dbQuery(DBNewQuery.DeleteStudent_question_paper((int) StudentQuestionPaperID));
                                                csqi.close();
                                                msg = "Sub Question Not Avaliable.!";
                                                break outerloop;
                                            }
                                        } else {
                                            Utils.Log("In ELSE :-> ", mark + "");

                                            mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_paper_detal((int) StudentQuestionPaperID));
                                            mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_paper_Chapter((int) StudentQuestionPaperID));
                                            mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_type((int) StudentQuestionPaperID));
                                            mDb.dbQuery(DBNewQuery.DeleteStudent_question_paper((int) StudentQuestionPaperID));

                                            csqi.close();
                                            msg = "Sub Question Not Avaliable.!";
                                            break outerloop;
                                        }

                                    }

                                    boolean islblExistes = false;
                                    for (SubQueLabel lblObj : subQuesLabelArray) {
                                        if (lblObj.getLabel().equals(subQuesLabelObj.getLabel())) {
                                            islblExistes = true;
                                            break;
                                        } else {
                                            islblExistes = false;
                                        }
                                    }
                                    if (!islblExistes) {
                                        subQuesLabelObj.keyArray.add(subQueskeyObj);
                                        subQuesLabelArray.add(subQuesLabelObj);
                                    }


                                } while (clabel.moveToNext());
                                clabel.close();

                                for (SubQueLabel lblObj : subQuesLabelArray) {
                                    for (SubQueKey keyObj : lblObj.keyArray) {
                                        for (ClassQuestionPaperSubQuestion obj : keyObj.internalArray) {

                                            mDb.insertRow(obj, DBConstant.class_question_paper_sub_question);
                                        }
                                    }

                                }

                            }

                        } else {

                        }
                    } while (Ques.moveToNext());
                    Ques.close();
                }

            }
            Utils.Log("TAg", "IDS:-> " + Utils.convertArraytoCommaSeprated(iDSArray));
            App.practiesPaperObj.setStudentQuestionTypeID(Utils.convertArraytoCommaSeprated(iDSArray));
            return msg;
        }

        public String getKey(int position) {
            return mKeys.get(position);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.onCompleted(s);
        }
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onCompleted(String msg) {
        showProgress(false, true);
        if (msg.equals("")) {
            Utils.showToast("Paper Generate", instance);
            startActivity(new Intent(instance, SetPaperSolutionActivity.class));
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(instance);
            builder1.setMessage(msg);
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Okay",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                            onBackPressed();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();

        }

    }
}
