package com.parshvaa.isccore.activity.iconnect.fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.parshvaa.isccore.activity.iconnect.activity.CctSummaryActivity;
import com.parshvaa.isccore.activity.iconnect.adapter.ICorrectAnswerAdapter;
import com.parshvaa.isccore.observscroll.BaseFragment;
import com.parshvaa.isccore.R;

/**
 * Created by empiere-vaibhav on 1/27/2018.
 */

public class ICorrectSummaryFragment extends BaseFragment {
    private LinearLayout emptyView;
    public RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (CctSummaryActivity.rightAnsArray != null && CctSummaryActivity.rightAnsArray.size() > 0) {

            View view = inflater.inflate(R.layout.fragment_recyclerview_new, container, false);
            Activity parentActivity = getActivity();
            recyclerView = view.findViewById(R.id.scroll);
            recyclerView.setLayoutManager(new LinearLayoutManager(parentActivity));
            recyclerView.setHasFixedSize(false);
            recyclerView.setVisibility(View.VISIBLE);

            recyclerView.setAdapter(new ICorrectAnswerAdapter(getActivity(), CctSummaryActivity.rightAnsArray));


            return view;
        } else {
            View view = inflater.inflate(R.layout.fragment_empty_view, container, false);
            emptyView = view.findViewById(R.id.empty_view);
            emptyView.setVisibility(View.VISIBLE);
            return view;
        }

    }

}