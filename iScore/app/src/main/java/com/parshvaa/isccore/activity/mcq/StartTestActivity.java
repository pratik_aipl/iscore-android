package com.parshvaa.isccore.activity.mcq;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.custominterface.OnTaskCompleted;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.TempMcqTestChap;
import com.parshvaa.isccore.model.TempMcqTestHdr;
import com.parshvaa.isccore.model.TempMcqTestLevel;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.RobotoBoldButton;
import com.parshvaa.isccore.utils.RobotoBoldTextview;
import com.parshvaa.isccore.utils.RobotoTextView;
import com.parshvaa.isccore.utils.Utils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartTestActivity extends BaseActivity implements OnTaskCompleted {

    public MyDBManager mDb;
    public App app;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;
    @BindView(R.id.img3)
    ImageView img3;
    @BindView(R.id.img4)
    ImageView img4;
    @BindView(R.id.img5)
    ImageView img5;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.timer)
    ImageView timer;
    @BindView(R.id.tv_text)
    RobotoBoldTextview tvText;
    @BindView(R.id.toggle_timer)
    ToggleButton toggleTimer;
    @BindView(R.id.btn_start_test)
    RobotoBoldButton btnStartTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_start_test);
        ButterKnife.bind(this);
        Utils.logUser();
        app = App.getInstance();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);

        tvTitle.setText("Start Test");
        if (mDb.isDbOpen()) {
            mDb.emptyTable(Constant.temp_mcq_test_hdr);
            mDb.emptyTable(Constant.temp_mcq_test_chapter);
            mDb.emptyTable(Constant.temp_mcq_test_dtl);
            mDb.emptyTable(Constant.temp_mcq_test_level);
        }

        if (app != null && App.mcqdata != null)
            toggleTimer.setOnCheckedChangeListener((toggleButton, isChecked) -> {
                if (isChecked) {
                    App.mcqdata.setTimer(true);
                } else {
                    App.mcqdata.setTimer(false);
                }
            });
        btnStartTest.setOnClickListener(v -> {
            showProgress(true, true);
            if (app != null && App.flags != null)
                App.flags.clear();
            new insertRecord(StartTestActivity.this).execute();
        });
    }

    @Override
    public void onCompleted() {
        showProgress(false, true);
        startActivity(new Intent(this, QuestionPagerActivity.class));
    }

    @Override
    public void onCompleted(String msg) {

    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }


    public class insertRecord extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public TempMcqTestHdr tempmcq;
        public TempMcqTestChap tempmcqchap;
        public TempMcqTestLevel tempLevel;
        public App app;
        public OnTaskCompleted listener;
        int attempted;

        private insertRecord(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            this.listener = (OnTaskCompleted) context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {

            tempmcq = new TempMcqTestHdr();
            if (App.mcqdata != null && App.mcqdata.subObj != null) {
                tempmcq.setStudentID(MainUser.getStudentId());
                tempmcq.setBoardID(App.mcqdata.subObj.getBoardID());
                tempmcq.setSubjectID(App.mcqdata.subObj.getOldSubjectID());
                tempmcq.setMediumID(App.mcqdata.subObj.getMediumID());
                if (App.mcqdata != null && App.mcqdata.isTimer()) {
                    tempmcq.setTimerStatus(1);
                } else {
                    tempmcq.setTimerStatus(0);
                }
                if (mDb.isDbOpen() && mDb.isTableExists(Constant.temp_mcq_test_hdr))
                    mDb.insertTempRow(tempmcq, Constant.temp_mcq_test_hdr);

                attempted = mDb.getAllRecords(Constant.temp_mcq_test_hdr);
                List<String> chapterList = Arrays.asList(App.mcqdata.getSelctedChapID().split(","));
                for (int i = 0; i < chapterList.size(); i++) {
                    tempmcqchap = new TempMcqTestChap();
                    tempmcqchap.setTempmcqtesthdrid(attempted + "");
                    tempmcqchap.setChapterid(chapterList.get(i));
                    if (mDb.isDbOpen() && mDb.isTableExists(Constant.temp_mcq_test_chapter))
                        mDb.insertTempRow(tempmcqchap, Constant.temp_mcq_test_chapter);
                }
                List<String> LevelList = Arrays.asList(App.mcqdata.getLevel().split(","));
                for (int i = 0; i < LevelList.size(); i++) {
                    tempLevel = new TempMcqTestLevel();
                    tempLevel.setStudentMCQTestHDRID(attempted + "");
                    tempLevel.setLevelID(LevelList.get(i));
                    if (mDb.isDbOpen() && mDb.isTableExists(Constant.temp_mcq_test_level))
                        mDb.insertTempRow(tempLevel, Constant.temp_mcq_test_level);
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.onCompleted();

        }
    }

    @Override
    protected void onResume() {
        super.onResume(); }

}
