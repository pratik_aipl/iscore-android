package com.parshvaa.isccore.activity.textbook;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.moderatepaper.ModeratePreviewActivity;

import java.util.List;

public class TextBookChapterAdapter extends RecyclerView.Adapter<TextBookChapterAdapter.MyViewHolder> {

    private List<Chapter> subjectList;
    public Context context;
    public AQuery aQuery;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_folder;
        public ImageView img_folder;

        public MyViewHolder(View view) {
            super(view);
            tv_folder = view.findViewById(R.id.tv_folder);
            img_folder = view.findViewById(R.id.img_folder);
        }
    }


    public TextBookChapterAdapter(List<Chapter> moviesList, Context context) {
        this.subjectList = moviesList;
        this.context = context;
        aQuery = new AQuery(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.textbook_chapter_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_folder.setText(subjectList.get(position).getChapterName());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ModeratePreviewActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("QFile", subjectList.get(position).getQFile())
                        .putExtra("PaperName", subjectList.get(position).getChapterName())
                        .putExtra("page_name", subjectList.get(position).getChapterName())
                        .putExtra("ViewPaper", "true"));


            }
        });
    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }
}



