package com.parshvaa.isccore.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.adapter.StandardAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.StandardModel;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.Utils;
import com.parshvaa.isccore.utils.VerticalSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StandardActivity extends BaseActivity implements AsynchTaskListner {
    public StandardActivity instance;
    public RecyclerView recycler_standard;
    public String[] list_name;
    public TextView lbl_title;
    public FloatingActionButton btn_next, btn_prev;
    public ArrayList<StandardModel> stdList = new ArrayList<>();
    private StaggeredGridLayoutManager sgaggeredGridLayoutManager;
    public String mediumId;
    public StandardModel standardModel;
    public ArrayList<StandardModel> StandeardArray = new ArrayList<>();
    public ArrayList<String> strStandeardArray = new ArrayList<>();
    public String email;
    public StandardAdapter standardAdapter;
    public App app;
    private LinearLayout emptyView;
    public TextView tv_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_standard);
        Utils.logUser();

        instance = this;
        app = App.getInstance();
        recycler_standard = findViewById(R.id.recycler_standard);
        lbl_title = findViewById(R.id.lbl_title);
        btn_next = findViewById(R.id.btn_next);
        btn_prev = findViewById(R.id.btn_prev);
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);
        tv_empty.setText("No Standard Available");

        new CallRequest(StandardActivity.this).getStandeard(MainUser.getMediumID());

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(instance)) {
                    if (standardAdapter != null && !TextUtils.isEmpty(standardAdapter.getStandardId())){
                        if (TextUtils.isEmpty(standardAdapter.getStandardId())) {
                            Utils.showToast("Please Select Standard ", StandardActivity.this);
                        } else {
                            MainUser.setStandardID(standardAdapter.getStandardId());
                            MainUser.setStanderdName(standardAdapter.getStandardName());

                            startActivity(new Intent(instance, RegistrationConfirmActivity.class));
                        }
                    }else {
                        Utils.showToast("Please Select Standard ", StandardActivity.this);
                    }
                } else {
                    Utils.showToast(getString(R.string.no_internet_msg), instance);

                }
            }
        });
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setupRecyclerView(ArrayList<StandardModel> standeardArray) {
        final Context context = recycler_standard.getContext();
        sgaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        recycler_standard.setLayoutManager(sgaggeredGridLayoutManager);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);

        recycler_standard.setLayoutAnimation(controller);

        recycler_standard.setHasFixedSize(true);
        recycler_standard.addItemDecoration(new VerticalSpacingDecoration(2));
        recycler_standard.setItemViewCacheSize(20);
        recycler_standard.setDrawingCacheEnabled(true);
        recycler_standard.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        standardAdapter = new StandardAdapter(standeardArray, context);
        recycler_standard.setAdapter(standardAdapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case getStandeard:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            emptyView.setVisibility(View.GONE);


                            JSONArray jBoardArray = jObj.getJSONArray("data");

                            if (jBoardArray != null && jBoardArray.length() > 0) {

                                for (int i = 0; i < jBoardArray.length(); i++) {
                                    JSONObject jBoard = jBoardArray.getJSONObject(i);
                                    standardModel = new StandardModel();
                                    standardModel.setStandardID(jBoard.getString("StandardID"));
                                    standardModel.setStandardName(jBoard.getString("StandardName"));
                                    strStandeardArray.add(standardModel.getStandardName());
                                    StandeardArray.add(standardModel);
                                }
                                setupRecyclerView(StandeardArray);

                            } else {
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        } else {
                            btn_next.setAlpha(0.5f);
                            btn_next.setClickable(false);
                            recycler_standard.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
