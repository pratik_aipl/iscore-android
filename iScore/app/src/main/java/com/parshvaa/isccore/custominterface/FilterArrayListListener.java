package com.parshvaa.isccore.custominterface;

/**
 * Created by Karan - Empiere on 1/26/2018.
 */

public interface FilterArrayListListener {
    void onLeftDrawerItemSelected();

    void onRightDrawerItemSelected();
}
