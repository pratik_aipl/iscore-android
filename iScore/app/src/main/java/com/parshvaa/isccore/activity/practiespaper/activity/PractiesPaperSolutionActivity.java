package com.parshvaa.isccore.activity.practiespaper.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.practiespaper.fragment.PractiesPaperSolutionFragment;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.ClassQuestionPaperSubQuestion;
import com.parshvaa.isccore.model.StudentQuestionPaper;
import com.parshvaa.isccore.model.StudentQuestionPaperChapter;
import com.parshvaa.isccore.model.StudentQuestionPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperQuestionType;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.PrelimQuestionListModel;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class PractiesPaperSolutionActivity extends BaseActivity implements AsynchTaskListner {
    public ViewPager viewPager;
    public MyViewPagerAdapter myViewPagerAdapter;
    public FloatingActionButton float_right_button, float_left_button;
    public TextView tv_pager_count;
    public ImageView img_back, img_home, img_right, img_left;
    public TextView tv_title;
    public PractiesPaperSolutionActivity instance;
    public static PractiesPaperSolutionActivity Staticinstance;
    public Button btn_privew_paper, btn_model_paper;
    PrelimQuestionListModel questionListModel;
    public ArrayList<PrelimQuestionListModel> questionsList = new ArrayList<>();

    public ArrayList<StudentQuestionPaper> StuQuePaperArray = new ArrayList<>();
    public static JsonArray jStudQuePaperArray;
    public static MyDBManager mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_generate_paper_solution);
        Utils.logUser();



        instance = this;
        Staticinstance = this;
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        questionsList.clear();
        Intent intent = getIntent();
        questionsList = (ArrayList<PrelimQuestionListModel>) intent.getSerializableExtra("questionsList");
        viewPager = findViewById(R.id.pager);
        btn_privew_paper = findViewById(R.id.btn_privew_paper);
        btn_model_paper = findViewById(R.id.btn_model_paper);
        tv_pager_count = findViewById(R.id.tv_pager_count);
        img_back = findViewById(R.id.img_back);
        img_home = findViewById(R.id.img_home);
        img_right = findViewById(R.id.img_right);
        img_left = findViewById(R.id.img_left);
        tv_title = findViewById(R.id.tv_title);
        float_right_button = findViewById(R.id.float_right_button);
        float_left_button = findViewById(R.id.float_left_button);


        btn_privew_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, PractiesAndReadyPreviewPdfActivity.class)
                        .putExtra("showAns", false));
            }
        });
        btn_model_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, PractiesAndReadyPreviewPdfActivity.class)
                        .putExtra("showAns", true));
            }
        });
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });
        tv_pager_count.setText("1/" + questionsList.size());
        myViewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager(), questionsList.size(), questionListModel);
        viewPager.setAdapter(myViewPagerAdapter);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        myViewPagerAdapter.notifyDataSetChanged();
        tv_title.setText("Practise Paper");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                questionsList.clear();
            }
        });

        if (Utils.isNetworkAvailable(instance)) {
            getGeneratePaperData();

        }
        if (questionsList.size() == 1) {
            float_left_button.hide();
            float_right_button.hide();
        } else {
            float_left_button.hide();
        }
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }

            @Override
            public void onPageSelected(int position) {
                if ((questionsList.size() - 1) == position) {

                    float_right_button.hide();
                } else {
                    float_right_button.show();
                }
                if (position == 0) {
                    float_left_button.hide();
                } else {
                    float_left_button.show();
                }
                if (questionsList.size() == 1) {
                    float_left_button.hide();
                    float_right_button.hide();
                }
                tv_pager_count.setText(position + 1 + "/" + questionsList.size());

            }
        });
        float_left_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        float_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < questionsList.size()) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });
        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < questionsList.size()) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });


    }

    public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("ASure about visiting the dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        showProgress(true, true);//Utils.showProgressDialog(instance);

                        startActivity(new Intent(instance, DashBoardActivity.class));
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void getGeneratePaperData() {
        StuQuePaperArray.clear();
        jStudQuePaperArray = null;


        try {
            StuQuePaperArray = (ArrayList) new CustomDatabaseQuery(instance, new StudentQuestionPaper())
                    .execute(new String[]{DBQueries.getAllStudentQuestionPaper(mySharedPref.getLastGeneratePaperSyncTime())}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        for (StudentQuestionPaper header : StuQuePaperArray) {
            String hdr = String.valueOf(header.getStudentQuestionPaperID());


            if (header.getPaperTypeID().equals("1") || header.getPaperTypeID().equals(1)) {

                StudentQuePaperDetail(header, hdr);

            } else if (header.getPaperTypeID().equals("2") || header.getPaperTypeID().equals(2)) {

                StudentQuePaperChap(header, hdr);
                StudentQuePaperDetail(header, hdr);
            } else {
                // paper type id 3

                StudentQuePaperChap(header, hdr);

                try {
                    header.queTypeArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentSetPaperQuestionType())
                            .execute(new String[]{DBQueries.getStudentSetPaperQueType(hdr)}).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


                for (StudentSetPaperQuestionType Quetypeid : header.queTypeArray) {
                    String id = String.valueOf(Quetypeid.getStudentSetPaperQuestionTypeID());
                    try {
                        Quetypeid.setdetailArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentSetPaperDetail())
                                .execute(new String[]{DBQueries.getStudentSetPaperDtl(id)}).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }


                }
            }
        }
        Gson gson = new GsonBuilder().create();
        jStudQuePaperArray = gson.toJsonTree(StuQuePaperArray).getAsJsonArray();

        longInfo(String.valueOf(jStudQuePaperArray), "jStudQuePaperArray");

        if (jStudQuePaperArray.size() > 0) {
            new CallRequest(instance).sendGanreatPaperLogin(String.valueOf(jStudQuePaperArray));
        } else {
            new CallRequest(instance).sendGanreatPaperLogin("[]");
        }

    }


    public void StudentQuePaperDetail(StudentQuestionPaper header, String hdr) {


        try {
            header.detailArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentQuestionPaperDetail())
                    .execute(new String[]{DBQueries.getStudentQuePaperDetail(hdr)}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        for(StudentQuestionPaperDetail detaiObj: header.detailArray){
            String detaiId = String.valueOf(detaiObj.getStudentQuestionPaperDetailID());

            try {
                detaiObj.subQuesArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new ClassQuestionPaperSubQuestion())
                        .execute(new String[]{DBQueries.getClassSubQue(detaiId)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

    }

    public void StudentQuePaperChap(StudentQuestionPaper header, String hdr) {


        try {
            header.chapterArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentQuestionPaperChapter())
                    .execute(new String[]{DBQueries.getStudentQuePaperChap(hdr)}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Utils.Log("Ganrate Paper ::-->", String.valueOf(jStudQuePaperArray));
    }

    public class MyViewPagerAdapter extends FragmentPagerAdapter {
        int mNumOfTabs;
        PrelimQuestionListModel questionMod;

        public MyViewPagerAdapter(FragmentManager fm, int i, PrelimQuestionListModel questionMode) {
            super(fm);
            this.mNumOfTabs = i;
            this.questionMod = questionMode;
        }

        @Override
        public Fragment getItem(int position) {
            return new PractiesPaperSolutionFragment().newInstance(position,questionsList, questionMod);
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return String.valueOf(position + 1);
        }


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {

                    case sendGanreatPaper:
                        showProgress(false, true);

                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            if (jObj.has("GeneratePaperLastSync")) {
                                String lastGeneratePaperSyncTime = jObj.getString("GeneratePaperLastSync");
                                mySharedPref.setLastGeneratePaperlastSync(lastGeneratePaperSyncTime);
                                mySharedPref.setLastSyncTime(lastGeneratePaperSyncTime);

                            }
                            try {
                                if (jObj.getJSONArray("data") != null) {
                                    JSONArray jData = jObj.getJSONArray("data");
                                    parseWebGeneratePaperData(jData);
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }


                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void parseWebGeneratePaperData(JSONArray jArray) {
        if (jArray != null && jArray.length() > 0) {
            try {
                StudentQuestionPaper StudentQuePaeprObj;
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject obj = jArray.getJSONObject(i);

                    StudentQuePaeprObj = new StudentQuestionPaper();
                    StudentQuePaeprObj.setExamTypePatternID(obj.getString("ExamTypePatternID"));
                    StudentQuePaeprObj.setStudentID(obj.getString("StudentID"));
                    StudentQuePaeprObj.setPaperTypeID(obj.getString("PaperTypeID"));
                    StudentQuePaeprObj.setSubjectID(obj.getString("SubjectID"));
                    StudentQuePaeprObj.setTotalMarks(obj.getString("TotalMarks"));
                    StudentQuePaeprObj.setDuration(obj.getString("Duration"));
                    StudentQuePaeprObj.setCreatedBy(obj.getString("CreatedBy"));
                    StudentQuePaeprObj.setCreatedOn(obj.getString("CreatedOn"));
                    StudentQuePaeprObj.setModifiedBy(obj.getString("ModifiedBy"));
                    StudentQuePaeprObj.setModifiedOn(obj.getString("ModifiedOn"));
                    long ID = mDb.insertWebRow(StudentQuePaeprObj, "student_question_paper");
                    StuQuePaperArray.add(StudentQuePaeprObj);
                    int inserID = Integer.parseInt(ID + "");


                    if (obj.has("chapterArray")) {
                        JSONArray chapterArray = obj.getJSONArray("chapterArray");

                        if (chapterArray != null && chapterArray.length() > 0) {
                            StudentQuestionPaperChapter StudentQuePaeprChpObj;
                            for (int k = 0; k < chapterArray.length(); k++) {
                                JSONObject chapObj = chapterArray.getJSONObject(k);
                                StudentQuePaeprChpObj = new StudentQuestionPaperChapter();

                                StudentQuePaeprChpObj.setStudentQuestionPaperID((inserID));
                                StudentQuePaeprChpObj.setChapterID(chapObj.getString("ChapterID"));

                                mDb.insertWebRow(StudentQuePaeprChpObj, "student_question_paper_chapter");
                            }
                        }
                    }
                    if (obj.has("detailArray")) {
                        JSONArray detailArray = obj.getJSONArray("detailArray");

                        if (detailArray != null && detailArray.length() > 0) {
                            StudentQuestionPaperDetail StudentQuePaeprDtlObj;
                            for (int k = 0; k < detailArray.length(); k++) {
                                JSONObject detailObj = detailArray.getJSONObject(k);
                                StudentQuePaeprDtlObj = new StudentQuestionPaperDetail();
                                StudentQuePaeprDtlObj.setExamTypePatternDetailID(detailObj.getString("ExamTypePatternDetailID"));
                                StudentQuePaeprDtlObj.setMasterorCustomized(Integer.parseInt(detailObj.getString("MasterorCustomized")));
                                StudentQuePaeprDtlObj.setMQuestionID(detailObj.getString("MQuestionID"));
                                StudentQuePaeprDtlObj.setCQuestionID(Integer.parseInt(detailObj.getString("CQuestionID")));

                                StudentQuePaeprDtlObj.setStudentQuestionPaperID(inserID);
                                mDb.insertWebRow(StudentQuePaeprDtlObj, "student_question_paper_detail");
                            }
                        }
                    }

                    if (obj.has("queTypeArray")) {
                        JSONArray queTypeArray = obj.getJSONArray("queTypeArray");

                        if (queTypeArray != null && queTypeArray.length() > 0) {
                            StudentSetPaperQuestionType StudentSetPaperQueTypeObj;
                            for (int k = 0; k < queTypeArray.length(); k++) {
                                JSONObject queTypeObj = queTypeArray.getJSONObject(k);
                                StudentSetPaperQueTypeObj = new StudentSetPaperQuestionType();
                                StudentSetPaperQueTypeObj.setQuestionTypeID(queTypeObj.getString("QuestionTypeID"));
                                StudentSetPaperQueTypeObj.setTotalAsk(queTypeObj.getString("TotalAsk"));
                                StudentSetPaperQueTypeObj.setToAnswer(queTypeObj.getString("ToAnswer"));
                                StudentSetPaperQueTypeObj.setTotalMark(queTypeObj.getString("TotalMark"));

                                StudentSetPaperQueTypeObj.setStudentQuestionPaperID(inserID + "");
                                long QuTypeID = mDb.insertWebRow(StudentSetPaperQueTypeObj, "student_set_paper_question_type");
                                int inserQuTypeID = Integer.parseInt(QuTypeID + "");

                                if (queTypeObj.has("setdetailArray")) {
                                    Utils.Log("TAG ", "in SET detail::->");
                                    JSONArray setdetailArray = queTypeObj.getJSONArray("setdetailArray");

                                    if (setdetailArray != null && setdetailArray.length() > 0) {
                                        StudentSetPaperDetail StudentSetPaperDtlObj;
                                        for (int j = 0; j < setdetailArray.length(); j++) {
                                            Utils.Log("TAG ", "in SET detail  for setdetailArray ::->");
                                            JSONObject setdetailObj = setdetailArray.getJSONObject(j);
                                            StudentSetPaperDtlObj = new StudentSetPaperDetail();
                                            StudentSetPaperDtlObj.setStudentSetPaperQuestionTypeID(inserQuTypeID);
                                            try {
                                                StudentSetPaperDtlObj.setMQuestionID(setdetailObj.getString("MQuestionID"));
                                            } catch (NumberFormatException e) {
                                                e.printStackTrace();
                                                StudentSetPaperDtlObj.setMQuestionID("-2");
                                            }
                                            mDb.insertWebRow(StudentSetPaperDtlObj, "student_set_paper_detail");

                                        }
                                    }
                                } else {
                                    Utils.Log("TAG ", "else SET detail::->");

                                }

                            }
                        }
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Utils.Log("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else
            {
                Utils.Log("TAG " + tag + " -->", str);
            }
    }

}
