package com.parshvaa.isccore.activity.iconnect.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.iconnect.AddPaperStartTime;
import com.parshvaa.isccore.activity.iconnect.LaturesDataModel;
import com.parshvaa.isccore.activity.iconnect.adapter.LacturesRowDetailsAdpater;
import com.parshvaa.isccore.calendar.Utils;
import com.parshvaa.isccore.calendar.component.CalendarAttr;
import com.parshvaa.isccore.calendar.component.CalendarViewAdapter;
import com.parshvaa.isccore.calendar.interf.OnSelectDateListener;
import com.parshvaa.isccore.calendar.model.CalendarDate;
import com.parshvaa.isccore.calendar.model.PaperDates;
import com.parshvaa.isccore.calendar.view.Calendar;
import com.parshvaa.isccore.calendar.view.CustomDayView;
import com.parshvaa.isccore.calendar.view.MonthPager;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.custominterface.GenerateOnlineClassURL;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LaturesActivity extends BaseActivity implements AsynchTaskListner, GenerateOnlineClassURL, AddPaperStartTime {

    private static final String TAG = "DashBoardFragment";
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.mPrev)
    ImageView mPrev;
    @BindView(R.id.mMonthName)
    TextView mMonthName;
    @BindView(R.id.tv_notfound)
    TextView tv_notfound;
    @BindView(R.id.mNext)
    ImageView mNext;
    @BindView(R.id.calendar_view)
    MonthPager monthPager;
    @BindView(R.id.mDataList)
    RecyclerView mDataList;
    @BindView(R.id.content)
    CoordinatorLayout content;

    private int selectedMonth = 0;
    private RecyclerView.LayoutManager layoutManager;
    public JsonParserUniversal jParser;

    List<PaperDates> dates = new ArrayList<>();

    private OnSelectDateListener onSelectDateListener;
    private ArrayList<Calendar> currentCalendars = new ArrayList<>();
    private CalendarViewAdapter calendarAdapter;
    private CalendarDate currentDate;
    public App app;

    List<LaturesDataModel> laturesDataModels = new ArrayList<>();
    LacturesRowDetailsAdpater lacturesRowDetailsAdpater;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.latures_list);
        ButterKnife.bind(this);
        jParser = new JsonParserUniversal();
        app = App.getInstance();

        layoutManager = new LinearLayoutManager(this);
        mDataList.setLayoutManager(layoutManager);
        mDataList.setItemAnimator(new DefaultItemAnimator());


        monthPager.setViewHeight(Utils.dpi2px(this, 270));


        initCurrentDate();
        initCalendarView();
        initToolbarClickListener();
        Utils.scrollTo(content, mDataList, monthPager.getCellHeight(), 200);
        calendarAdapter.switchToWeek(monthPager.getRowIndex());
        monthPager.setCurrentItem(monthPager.getCurrentPosition() - 1);

        tv_title.setText("LECTURES");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void initCurrentDate() {
        currentDate = new CalendarDate();
        selectedMonth = currentDate.getMonth();
        mMonthName.setText(currentDate.getMonthName() + " " + currentDate.getYear());
        refreshClickDate(currentDate);
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.set(currentDate.getYear(), currentDate.getMonth() - 1, currentDate.getDay());
        new CallRequest(LaturesActivity.this).getDates(false, MainUser.getStudentId(), new SimpleDateFormat("MM/yyyy").format(cal.getTime()));


    }

    private void initListener() {
        onSelectDateListener = new OnSelectDateListener() {
            @Override
            public void onSelectDate(CalendarDate date) {
                refreshClickDate(date);
            }

            @Override
            public void onSelectOtherMonth(int offset) {
                //Offset -1 means refreshing to last month's data, 1 means refreshing to next month's data
                Log.d(TAG, "onSelectOtherMonth: 00 " + offset);
                Log.d(TAG, "onSelectOtherMonth: 11 " + monthPager.getRowIndex());
                monthPager.selectOtherMonth(offset);
            }
        };
    }

    private void initCalendarView() {
        initListener();
        CustomDayView customDayView = new CustomDayView(this, R.layout.custom_day);
        calendarAdapter = new CalendarViewAdapter(this,
                onSelectDateListener,
                CalendarAttr.WeekArrayType.Sunday,
                customDayView);
        initMonthPager();
    }

    private void initToolbarClickListener() {
        mPrev.setOnClickListener((View view) -> {
            Log.d(TAG, "initToolbarClickListener: pre " + monthPager.getCurrentPosition());
            Log.d(TAG, "initToolbarClickListener: pre " + (monthPager.getCurrentPosition() - 1));
            monthPager.setCurrentItem(monthPager.getCurrentPosition() - 1);
        });

        mNext.setOnClickListener((View view) -> {
            Log.d(TAG, "initToolbarClickListener: next " + monthPager.getCurrentPosition());
            Log.d(TAG, "initToolbarClickListener: next " + (monthPager.getCurrentPosition() + 1));
            monthPager.setCurrentItem(monthPager.getCurrentPosition() + 1);
        });

    }

    private void initMarkData() throws ParseException {
        HashMap<String, String> markData = new HashMap<>();
        for (int i = 0; i < dates.size(); i++) {
            markData.put(formateDateFromstring("yyyy-MM-dd", "yyyy-M-dd", dates.get(i).getDate()), "0");
            Log.d(TAG, "initMarkData: "+dates.get(i).getDate());
        }
        calendarAdapter.setMarkData(markData);
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) throws ParseException {
        return new SimpleDateFormat(outputFormat, java.util.Locale.getDefault()).format(new SimpleDateFormat(inputFormat, java.util.Locale.getDefault()).parse(inputDate));
    }

    private void initMonthPager() {
        monthPager.setAdapter(calendarAdapter);
        monthPager.setCurrentItem(MonthPager.CURRENT_DAY_INDEX);
        monthPager.setPageTransformer(false, (page, position) -> {
            position = (float) Math.sqrt(1 - Math.abs(position));
            page.setAlpha(position);
        });
        monthPager.addOnPageChangeListener(new MonthPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentCalendars = calendarAdapter.getPagers();
                if (currentCalendars.get(position % currentCalendars.size()) != null) {
                    CalendarDate date = currentCalendars.get(position % currentCalendars.size()).getSeedDate();
                    currentDate = date;
                    if (selectedMonth != date.getMonth()) {
                        java.util.Calendar cal = java.util.Calendar.getInstance();
                        cal.set(date.getYear(), date.getMonth() - 1, date.getDay());
                        new CallRequest(LaturesActivity.this).getDates(false, MainUser.getStudentId(), new SimpleDateFormat("MM/yyyy").format(cal.getTime()));

                    }
                    selectedMonth = date.getMonth();
                    mMonthName.setText(date.getMonthName() + " " + date.getYear());
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void refreshClickDate(CalendarDate date) {
        currentDate = date;
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.set(date.getYear(), date.getMonth() - 1, date.getDay());
       new CallRequest(LaturesActivity.this).getDataByDate(true, new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime()));
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
       // showProgress(false);
        JSONObject jObj;
        Log.d(TAG, "onTaskCompleted: "+result);
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case getDate:
                    try {
                        showProgress(false, true);
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            dates.clear();
                            JSONArray jData = jObj.getJSONArray("Dates");
                            Log.d(TAG, "onTaskCompleteddatta: " + jData.length());
                            if (jData.length() > 0) {
                                for (int i = 0; i < jData.length(); i++) {
                                    PaperDates pprdate=new PaperDates();
                                    pprdate.setDate(jData.getJSONObject(i).getString("Date"));
                                    dates.add(pprdate);
                                }
                            }
                            Log.d(TAG, "onTaskCompleteddate: "+dates.size());
                            if (dates.size() > 0)
                                initMarkData();
                        }
                    } catch (Exception e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    }
                    break;

                    case getDataByDate:
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Log.d(TAG, "onTaskCompleted: " +result);

                            JSONArray testassign = jObj.getJSONArray("Data");

                            laturesDataModels.clear();

                            if (testassign.length() > 0) {
                                for (int i = 0; i < testassign.length(); i++) {

                                    LaturesDataModel assignedData=new LaturesDataModel();
                                    assignedData.setClassLessonPlannerID(testassign.getJSONObject(i).getString("ClassLessonPlannerID"));
                                    assignedData.setClassID(testassign.getJSONObject(i).getString("ClassID"));
                                    assignedData.setTitle(testassign.getJSONObject(i).getString("Title"));
                                    assignedData.setClassName(testassign.getJSONObject(i).getString("ClassName"));
                                    assignedData.setIsLive(testassign.getJSONObject(i).getString("IsLive"));
                                    assignedData.setLiveClassID(testassign.getJSONObject(i).getString("LiveClassID"));
                                    assignedData.setTeacherID(testassign.getJSONObject(i).getString("TeacherID"));
                                    assignedData.setSubjectID(testassign.getJSONObject(i).getString("SubjectID"));
                                    assignedData.setBranchID(testassign.getJSONObject(i).getString("BranchID"));
                                    assignedData.setBatchID(testassign.getJSONObject(i).getString("BatchID"));
                                    assignedData.setDuration(testassign.getJSONObject(i).getString("Duration"));
                                    assignedData.setStatus(testassign.getJSONObject(i).getString("Status"));
                                    assignedData.setPaperDate(testassign.getJSONObject(i).getString("PaperDate"));
                                    assignedData.setCreatedBy(testassign.getJSONObject(i).getString("CreatedBy"));
                                    assignedData.setCreatedOn(testassign.getJSONObject(i).getString("CreatedOn"));
                                    assignedData.setModifiedBy(testassign.getJSONObject(i).getString("ModifiedBy"));
                                    assignedData.setModifiedOn(testassign.getJSONObject(i).getString("ModifiedOn"));
                                    assignedData.setTeacherName(testassign.getJSONObject(i).getString("TeacherName"));
                                    assignedData.setTime(testassign.getJSONObject(i).getString("Time"));
                                    assignedData.setSubjectName(testassign.getJSONObject(i).getString("SubjectName"));
                                    assignedData.setBoardName(testassign.getJSONObject(i).getString("BoardName"));
                                    assignedData.setMediumName(testassign.getJSONObject(i).getString("MediumName"));
                                    assignedData.setStandardName(testassign.getJSONObject(i).getString("StandardName"));
                                    assignedData.setStartTime(testassign.getJSONObject(i).getString("startTime"));
                                    assignedData.setEndTime(testassign.getJSONObject(i).getString("endTime"));
                                    assignedData.setJoinURL(testassign.getJSONObject(i).getString("JoinURL"));
                                    laturesDataModels.add(assignedData);
                                }
                            }

                            if(testassign.length()==0){
                                tv_notfound.setVisibility(View.VISIBLE);
                            }else{
                                tv_notfound.setVisibility(View.GONE);
                                mDataList.setVisibility(View.VISIBLE);

                                lacturesRowDetailsAdpater = new LacturesRowDetailsAdpater(LaturesActivity.this, laturesDataModels);
                                mDataList.setAdapter(lacturesRowDetailsAdpater);
                            }

                        }else {
                            tv_notfound.setVisibility(View.VISIBLE);
                            tv_notfound.setText("No assigned by institute");
                            mDataList.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case getOnlineClassURL:
                    try {
                        showProgress(false, true);
                        jObj = new JSONObject(result);

                        Log.d(TAG, "linkkk>>>>: "+result.toString());
                        if (jObj.getString("status").equalsIgnoreCase("ok")) {

                            Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                            httpIntent.setData(Uri.parse(""+jObj.getString("launchurl")));
                            startActivity(httpIntent);

                           /* startActivity(new Intent(LaturesActivity.this, OnlineClassActivity.class)
                                    .putExtra("CLASSLINK",jObj.getString("launchurl")));*/
                        }else{
                            Toast.makeText(app, ""+jObj.getString("error"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    }
                    break;
                case SendPaperEnterTime:
                    showProgress(false, true);
                    Log.d(TAG, "result: "+result);
                    try {
                        JSONObject jObjs = new JSONObject(result);
                        if (jObjs.getBoolean("status")) {
                           // JSONArray jDataArray = jObjs.getJSONArray("data");
                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    }
                    break;
            }

        }
    }

    @Override
    public void ongnrtURL(String class_id, String isTeacher, String lessonName, String ourseName) {
        Log.d(TAG, "ongnrtURL: "+class_id+
                isTeacher+
                lessonName+
                ourseName);
        showProgress(true, true);
        new CallRequest(LaturesActivity.this).getOnlineClassURL(class_id,isTeacher,lessonName,ourseName);
    }
    @Override
    public void onPaperStartTime(String StudID, String HistoryID, String Type, String Time) {
        showProgress(true,false);
        new CallRequest(LaturesActivity.this).SendPaperStartTime(StudID,HistoryID,Time);

    }
}
