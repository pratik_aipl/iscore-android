package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/25/2017.
 */
@JsonObject
public class BoardPaperDownload implements Serializable {


    @JsonField
    public int BoardPaperDownloadID = -1;
    @JsonField
    public String StudentID, SubjectID, BoardPaperID, BoardPaperAnswers, CreatedOn;

    public int getBoardPaperDownloadID() {
        return BoardPaperDownloadID;
    }

    public void setBoardPaperDownloadID(int boardPaperDownloadID) {
        BoardPaperDownloadID = boardPaperDownloadID;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getBoardPaperID() {
        return BoardPaperID;
    }

    public void setBoardPaperID(String boardPaperID) {
        BoardPaperID = boardPaperID;
    }

    public String getBoardPaperAnswers() {
        return BoardPaperAnswers;
    }

    public void setBoardPaperAnswers(String boardPaperAnswers) {
        BoardPaperAnswers = boardPaperAnswers;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }
}
