package com.parshvaa.isccore.activity;

import android.os.Bundle;
import android.webkit.WebView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Utils;

public class weeklyHtmlActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_weekly_html);
        Utils.logUser();
        WebView webView = findViewById(R.id.webview);
        webView.loadUrl("file:///android_asset/email2.html");
    }
}
