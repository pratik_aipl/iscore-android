package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/23/2018.
 */

@JsonObject
public class PrelimTestRecord implements Serializable {
    @JsonField
    public String ExamTypePatternDetailID = "";
    @JsonField
    public String QuestionNo = "";
    @JsonField
    public String SubQuestionNo = "";
    @JsonField
    public String QuestionTypeText = "";
    @JsonField
    public String isQuestion = "";
    @JsonField
    public String PageNo = "";
    @JsonField
    public String Question = "";
    @JsonField
    public String Answer = "";
    @JsonField
    public String QuestionMarks = "";
    @JsonField
    public String QuestionTypeID = "";
    @JsonField
    public String isPassage = "";
    @JsonField
    public String MQuestionID = "";
    @JsonField
    public String StudentQuestionPaperDetailID = "";
    @JsonField
    public String isQuestionVisible = "";
    @JsonField
    public boolean isSolution = false;

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public String getStudentQuestionPaperDetailID() {
        return StudentQuestionPaperDetailID;
    }

    public void setStudentQuestionPaperDetailID(String studentQuestionPaperDetailID) {
        StudentQuestionPaperDetailID = studentQuestionPaperDetailID;
    }

    public String getIsPassage() {
        return isPassage;
    }

    public void setIsPassage(String isPassage) {
        this.isPassage = isPassage;
    }

    public String getIsQuestionVisible() {
        return isQuestionVisible;
    }

    public void setIsQuestionVisible(String isQuestionVisible) {
        this.isQuestionVisible = isQuestionVisible;
    }

    public String getQuestionMarks() {
        return QuestionMarks;
    }

    public void setQuestionMarks(String questionMarks) {
        QuestionMarks = questionMarks;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getExamTypePatternDetailID() {
        return ExamTypePatternDetailID;
    }

    public void setExamTypePatternDetailID(String examTypePatternDetailID) {
        ExamTypePatternDetailID = examTypePatternDetailID;
    }

    public String getQuestionNo() {
        return QuestionNo;
    }

    public void setQuestionNo(String questionNo) {
        QuestionNo = questionNo;
    }

    public String getSubQuestionNo() {
        return SubQuestionNo;
    }

    public void setSubQuestionNo(String subQuestionNo) {
        SubQuestionNo = subQuestionNo;
    }

    public String getQuestionTypeText() {
        return QuestionTypeText;
    }

    public void setQuestionTypeText(String questionTypeText) {
        QuestionTypeText = questionTypeText;
    }

    public String getIsQuestion() {
        return isQuestion;
    }

    public void setIsQuestion(String isQuestion) {
        this.isQuestion = isQuestion;
    }

    public String getPageNo() {
        return PageNo;
    }

    public void setPageNo(String pageNo) {
        PageNo = pageNo;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }
}
