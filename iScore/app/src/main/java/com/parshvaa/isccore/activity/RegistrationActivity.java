package com.parshvaa.isccore.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.GPSTracker;
import com.parshvaa.isccore.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegistrationActivity extends BaseActivity implements AsynchTaskListner {
    public RegistrationActivity instance;
    public Button btn_next;
    public RelativeLayout rel_teacher, rel_student;
    public ImageView img_selector, image_stud_right, img_teach_right;
    public EditText et_firstName, et_lastName, et_email, et_mobileNo,et_schoolName;
    public LinearLayout lin_browse_picture;
    public boolean isFromRegKeyLogin = false;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public String DeviceID = "", OTP = "";
    public App app;
    public String packageSelected = "";
    public String Guid;
    public String str_amount;
    public String version = "";
    public CircleImageView img_student, img_teacher;
    Uri selectedUri, studentImageUri, teacherImageUri;
    public String profImagePath = "", UserType = "", LocationDetails = "";
    public int status = 0;
    public View view_teach_shadwo, view_stud_shadwo;

    boolean isDemo = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_registration);
        Utils.logUser();
        isDemo = getIntent().getBooleanExtra(Constant.isDemo, false);
        instance = this;
        app = App.getInstance();

        if (!mySharedPref.getGuuid().isEmpty()) {
            Guid = mySharedPref.getGuuid();
        } else {
            Guid = UUID.randomUUID().toString();
            ////Utils.Log("TAG GUID::-->", Guid);
        }
        view_teach_shadwo = findViewById(R.id.view_teach_shadwo);
        view_stud_shadwo = findViewById(R.id.view_stud_shadwo);
        img_student = findViewById(R.id.img_student);
        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_email = findViewById(R.id.et_email);
        et_mobileNo = findViewById(R.id.et_mobileNo);
        et_schoolName = findViewById(R.id.et_schoolName);
        lin_browse_picture = findViewById(R.id.lin_browse_picture);
        btn_next = findViewById(R.id.btn_next);
        rel_student = findViewById(R.id.rel_student);
        rel_teacher = findViewById(R.id.rel_teacher);
        img_selector = findViewById(R.id.img_selector);
        image_stud_right = findViewById(R.id.image_stud_right);
        img_teach_right = findViewById(R.id.img_teach_right);
        img_teacher = findViewById(R.id.img_teacher);
        rel_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (studentImageUri != null) {
                    img_student.setImageURI(studentImageUri);
                }
                view_stud_shadwo.setVisibility(View.VISIBLE);
                view_teach_shadwo.setVisibility(View.GONE);
                image_stud_right.setVisibility(View.VISIBLE);
                img_teach_right.setVisibility(View.GONE);
                img_teacher.setImageResource(R.drawable.default_teacher_image);
                img_selector.setImageResource(R.drawable.back_left);
                status = 0;
                App.UserType = "student";

            }
        });
        rel_teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teacherImageUri != null) {
                    img_teacher.setImageURI(teacherImageUri);
                }
                view_stud_shadwo.setVisibility(View.GONE);
                view_teach_shadwo.setVisibility(View.VISIBLE);
                image_stud_right.setVisibility(View.GONE);
                img_teach_right.setVisibility(View.VISIBLE);
                img_student.setImageResource(R.drawable.default_student_image);
                img_selector.setImageResource(R.drawable.back_right);
                status = 1;
                App.UserType = "teacher";
            }
        });
        lin_browse_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        if (getIntent().hasExtra("RegKeyLogin")) {
            isFromRegKeyLogin = true;
            et_firstName.setText(MainUser.getFirstName());
            et_lastName.setText(MainUser.getLastName());
            et_email.setText(MainUser.getEmailID());
            et_mobileNo.setText(MainUser.getRegMobNo());

        }

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_firstName.getText().toString().equals("")) {
                    et_firstName.setFocusable(true);
                    et_firstName.setError("Please Enter First Name");
                } else if (et_lastName.getText().toString().equals("")) {
                    et_lastName.setError("Please Enter Last Name");
                    et_lastName.setFocusable(true);
                } else if (et_mobileNo.getText().toString().length() != 10) {
                    et_mobileNo.setError("Enter appropriate mobile number.");
                    et_mobileNo.setFocusable(true);
                }else if (TextUtils.isEmpty(et_schoolName.getText().toString().trim())) {
                    et_mobileNo.setError("Please Enter School Name.");
                    et_mobileNo.setFocusable(true);
                } else if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
                    et_email.setError(" Please Enter Valid Email ID");
                    et_email.setFocusable(true);
                } else {
                    btn_next.setEnabled(false);
                    try {
                        if (status == 0) {
                            if (studentImageUri != null) {
                                App.selectedUri = studentImageUri;
                                App.Image = studentImageUri.getPath();
                            }
                        } else {
                            if (teacherImageUri != null) {
                                App.selectedUri = teacherImageUri;
                                App.Image = teacherImageUri.getPath();
                            }
                        }
                    } catch (Exception e) {
                        App.Image = "";
                        e.printStackTrace();
                    }
                    MainUser.setFirstName(et_firstName.getText().toString());
                    MainUser.setLastName(et_lastName.getText().toString());
                    MainUser.setEmailID(et_email.getText().toString());
                    MainUser.setRegMobNo(et_mobileNo.getText().toString());
                    MainUser.setSchoolName(et_schoolName.getText().toString());
                    if (isFromRegKeyLogin) {
                        otpCallRequest();
                        App.isFromNewRegister = false;
                        return;
                    } else {
                        otpCallRequest();
                        App.isFromNewRegister = true;
                        return;
                    }
                }
            }
        });

        getLocation();
        //  requestPermissions();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean result = false;
        if (requestCode == 1) {
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    new CallRequest(RegistrationActivity.this).regKeySendOtp(et_mobileNo.getText().toString(), et_email.getText().toString());
                } else {
                }
            }
        }

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            result = true;
        switch (requestCode) {

        }
    }

//    private static Account getAccount(AccountManager accountManager) {
//        Account[] accounts = accountManager.getAccountsByType("com.google");
//        Account account;
//        if (accounts.length > 0) {
//            account = accounts[0];
//        } else {
//            account = null;
//        }
//        return account;
//    }

    public void getLocation() {


        GPSTracker mGPS = new GPSTracker(this);

        if (mGPS.canGetLocation) {
            mGPS.getLocation();
            System.out.println("Lat" + mGPS.getLatitude() + "Lon" + mGPS.getLongitude());
        } else {

            System.out.println("Unable");
        }
        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            App.addresses = geocoder.getFromLocation(mGPS.getLatitude(), mGPS.getLongitude(), 5); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                if (status == 0) {
                    studentImageUri = result.getUri();
                    img_student.setImageURI(result.getUri());

                } else {
                    teacherImageUri = result.getUri();
                    img_teacher.setImageURI(result.getUri());
                }

                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    public void otpCallRequest() {
        showProgress(true, false);
        new CallRequest(RegistrationActivity.this).regKeySendOtp(et_mobileNo.getText().toString(), et_email.getText().toString());
    }


    public void IsFromLOginSendOTP() {

        startActivity(new Intent(RegistrationActivity.this, VerifyOtpActivity.class)
                .putExtra("name", et_firstName.getText().toString())
                .putExtra("surname", et_lastName.getText().toString())
                .putExtra("mobile", et_mobileNo.getText().toString())
                .putExtra("email", et_email.getText().toString())
                .putExtra("boardID", MainUser.getBoardID())
                .putExtra("mediumID", MainUser.getMediumID())
                .putExtra("standardID", MainUser.getStandardID())
                .putExtra("BatchID", MainUser.getBatchID())
                .putExtra("ClassID", MainUser.getClassID())
                .putExtra("BranchID", MainUser.getBranchID())
                .putExtra("subject_id", "")
                .putExtra("version", version)
                .putExtra("RegKey", MainUser.getRegKey())
                .putExtra("Guid", Guid)
                .putExtra("OTP", OTP)
                .putExtra("str_amount", str_amount)
                .putExtra("packageID", packageSelected)
                .putExtra("SchoolName", et_schoolName.getText().toString())
                .putExtra("isDemo", isDemo)
                .putExtra("isKeyRegiste",true)
                .putExtra("isFromMobileNoLogin", "true"));


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case send_otp:
                    btn_next.setEnabled(true);
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            OTP = jObj.getString("otp");
                            IsFromLOginSendOTP();
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
