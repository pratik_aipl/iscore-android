package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 12/25/2017.
 */

public class StandardModel implements Serializable {
    public String StandardID;
    public String StandardName;

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String Price;

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

}
