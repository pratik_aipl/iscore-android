package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class MainData {

    @JsonField
    boolean status;
    @JsonField(name = "MCQLastSync")
    String MCQLastSync;
    @JsonField(name = "root_path")
    String rootPath;

    @JsonField(name = "data")
    ImageRecords imageRecordsList;

    @JsonField(name = "data")
    TableRecords data;

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getMCQLastSync() {
        return MCQLastSync;
    }

    public ImageRecords getImageRecordsList() {
        return imageRecordsList;
    }

    public void setImageRecordsList(ImageRecords imageRecordsList) {
        this.imageRecordsList = imageRecordsList;
    }

    public void setMCQLastSync(String MCQLastSync) {
        this.MCQLastSync = MCQLastSync;
    }

    @JsonField(name = "data")
    List<StudentMcqTestHdr> studentMcqTestHdrs;

    public List<StudentMcqTestHdr> getStudentMcqTestHdrs() {
        return studentMcqTestHdrs;
    }

    public void setStudentMcqTestHdrs(List<StudentMcqTestHdr> studentMcqTestHdrs) {
        this.studentMcqTestHdrs = studentMcqTestHdrs;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public TableRecords getData() {
        return data;
    }

    public void setData(TableRecords data) {
        this.data = data;
    }
}
