package com.parshvaa.isccore.model;

import com.parshvaa.isccore.R;

public enum Tour {

    MCQ(R.string.tour_mcq, R.layout.activity_tour_mcq),
    GENRATE(R.string.tour_genrate, R.layout.activity_tour_genrate),
    BOARD(R.string.tour_board, R.layout.activity_tour_board),
    INSTITUTE(R.string.tour_institute, R.layout.activity_tour_institute),
    REPORTS(R.string.tour_report, R.layout.activity_tour_report);

    private int mTitleResId;
    private int mLayoutResId;

    Tour(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}