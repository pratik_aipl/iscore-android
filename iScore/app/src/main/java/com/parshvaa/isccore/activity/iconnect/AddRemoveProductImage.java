package com.parshvaa.isccore.activity.iconnect;

public interface AddRemoveProductImage {
     void onAddImage(int pos);
     void onRemoveImage(int pos, String id);
}
