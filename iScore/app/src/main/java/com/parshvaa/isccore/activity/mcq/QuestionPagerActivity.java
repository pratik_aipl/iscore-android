package com.parshvaa.isccore.activity.mcq;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import androidx.viewpager.widget.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.mcq.fragment.QuestionFragment;
import com.parshvaa.isccore.adapter.QuestionAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.custominterface.OnTaskCompleted;
import com.parshvaa.isccore.model.McqOption;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.StudentIncorrectQuestion;
import com.parshvaa.isccore.model.StudentMcqTestChap;
import com.parshvaa.isccore.model.StudentMcqTestDtl;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.model.StudentMcqTestLevel;
import com.parshvaa.isccore.model.StudentnotAppearednQuestion;
import com.parshvaa.isccore.model.TempMcqTestChap;
import com.parshvaa.isccore.model.TempMcqTestDtl;
import com.parshvaa.isccore.model.TempMcqTestHdr;
import com.parshvaa.isccore.model.TempMcqTestLevel;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustPagerTransformer;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.PagerSlidingTabStrip;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class QuestionPagerActivity extends BaseActivity implements PagerListner, OnTaskCompleted, AsynchTaskListner {
    public QuestionPagerActivity instance;
    public static QuestionPagerActivity Staticinstance;
    private static final String TAG = "QuestionPagerActivity";
    public static ViewPager viewPager;
    public static PagerSlidingTabStrip pagerTabStri;
    public ImageView img_back, img_home;
    public TextView tv_title, tv_attempt, tv_time, tv_header_subName;
    public Button btn_submit;
    public static int currentOpenedFragment = 0;
    public int TotalDuration = 0, tempTotalDuration, milliseconds, attempted, headerId, temp = 0;

    public MyDBManager mDb;
    public QuestionAdapter questionAdapter;

    public static ArrayList<StudentMcqTestDtl> DetlArray = new ArrayList<>();
    public static ArrayList<Long> timerArray = new ArrayList<>();
    public static ArrayList<McqQuestion> mcqArray = new ArrayList<>();

    public List<String> iDS = new ArrayList<>();
    private int page = 0;
    private Handler handler;
    private int delay = 5000; //milliseconds

    public ArrayList<McqQuestion> rightAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> nonAttemptAnsArray = new ArrayList<>();
    public ArrayList<McqQuestion> incorectAnsArray = new ArrayList<>();
    public static ArrayList<StudentMcqTestHdr> StuHdrArray = new ArrayList<>();
    public static JsonArray jStudHdrArray;

    public PagerSlidingTabStrip.PageListener pageListner;
    public CountDownTimer timer;

    public LinearLayout lin_time;
    long totalTimeCountInMilliseconds;      // total count down time in milliseconds
    long timeBlinkInMilliseconds;
    boolean blink;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    QuestionFragment frag;
    public ArrayList<QuestionFragment> fragments = new ArrayList<>();
    public FragmentCommunicator fragmentCommunicator;

    public boolean ContinueTime = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_question_pager);
        Utils.logUser();

        instance = this;
        Staticinstance = this;
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        viewPager = findViewById(R.id.pager);
        pagerTabStri = findViewById(R.id.pager_header);
        lin_time = findViewById(R.id.lin_time);
        img_back = findViewById(R.id.img_back);
        img_home = findViewById(R.id.img_home);
        tv_title = findViewById(R.id.tv_title);
        tv_attempt = findViewById(R.id.tv_attempt);
        tv_header_subName = findViewById(R.id.tv_header_subName);
        tv_time = findViewById(R.id.tv_time);
        btn_submit = findViewById(R.id.btn_submit);
        int tempHeadrID = mDb.getAllRecords("student_mcq_test_hdr");

        // tv_title.setText(" Test " + (++tempHeadrID) + "");
        // tv_header_subName.setText(App.mcqdata.subObj.getSubjectName());


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customHandler != null) {
                    customHandler.removeCallbacks(updateTimerThread);
                }

                gotoDashboard();
            }
        });
        getMcqData();
        tv_attempt.setText(Html.fromHtml("Attempt<font color=\"#fabb72\"> 0 </font><font color=\"#0d4568\"> /" + mcqArray.size() + "</font>"));

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnSubmit();
            }
        });
        timeBlinkInMilliseconds = 30 * 1000;
        startTime = SystemClock.uptimeMillis();

    }


    public interface FragmentCommunicator {
        void passDataToFragment(QuestionFragment ID);
    }


    public void timeUpdate(long time, int pageNumber) {

    }

    public void getMcqData() {

        if (App.mcqdata != null && App.mcqdata.getQueAppear() != null) {

            if (App.mcqdata.getQueAppear().equals("isRandom")) {
                isRandome();
            } else if (App.mcqdata.getQueAppear().equals("inCorect_and_NotAppear")) {
                inCorect_and_NotAppear();
            } else if (App.mcqdata.getQueAppear().equals("notAppear")) {
                notAppear();
            } else if (App.mcqdata.getQueAppear().equals("inCorect")) {
                inCorect();
            }
        }

        if (mcqArray != null && mcqArray.size() != 0) {
            TotalDuration = 0;
            for (int i = 0; i < mcqArray.size(); i++) {
                timerArray.add(Long.valueOf(0));
                TotalDuration += Integer.parseInt(mcqArray.get(i).getDuration());
            }
            //fillViewPager();
            setUpViewPapager();
            temp = TotalDuration;
            setTimer(temp);

        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("No any Quetions Found")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(instance, SubjectMcqActivity.class)
                                    //  .putExtra("suObj", mcqdata)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                            finish();
                            dialog.dismiss();


                        }
                    })
                    .show();
        }
    }

    public void isRandome() {
        mcqArray.clear();
        timerArray.clear();
        timerArray = new ArrayList<>();
        try {
            String bordId=mySharedPref.getBoardID();
            String mediumId=mySharedPref.getMediumID();
            String standardId=mySharedPref.getStandardID();
            mcqArray = (ArrayList) new CustomDatabaseQuery(this, new McqQuestion())
                    .execute(new String[]{DBNewQuery.get_mcq_random(bordId,mediumId,standardId,App.mcqdata.subObj.getIsMapped(), App.mcqdata.subObj.getSubjectID(), App.mcqdata.getLevel(), App.mcqdata.getSelctedChapID())}).get();
            defultQuetionsEntries();
            for (McqQuestion que : mcqArray) {
                que.optionsArray.clear();
                que.optionsArray = (ArrayList) new CustomDatabaseQuery(this, new McqOption())
                        .execute(new String[]{DBNewQuery.get_mcq_option(que.getMCQQuestionID())}).get();

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void inCorect_and_NotAppear() {
        mcqArray.clear();
        timerArray.clear();
        timerArray = new ArrayList<>();
        try {
            String bordId=mySharedPref.getBoardID();
            String mediumId=mySharedPref.getMediumID();
            String standardId=mySharedPref.getStandardID();
            mcqArray = (ArrayList) new CustomDatabaseQuery(this, new McqQuestion())
                    .execute(new String[]{DBNewQuery.get_mcq_incorect_and_notAppeard(bordId,mediumId,standardId,App.mcqdata.subObj.getIsMapped(), App.mcqdata.subObj.getSubjectID(), App.mcqdata.getLevel(), App.mcqdata.getSelctedChapID())}).get();
            defultQuetionsEntries();
            for (McqQuestion que : mcqArray) {
                que.optionsArray.clear();
                que.optionsArray = (ArrayList) new CustomDatabaseQuery(this, new McqOption())
                        .execute(new String[]{DBNewQuery.get_mcq_option(que.getMCQQuestionID())}).get();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notAppear() {
        mcqArray.clear();
        timerArray.clear();
        timerArray = new ArrayList<>();
        try {
            String bordId=mySharedPref.getBoardID();
            String mediumId=mySharedPref.getMediumID();
            String standardId=mySharedPref.getStandardID();
            mcqArray = (ArrayList) new CustomDatabaseQuery(this, new McqQuestion())
                    .execute(new String[]{DBNewQuery.get_mcq_notAppeard(bordId,mediumId,standardId,App.mcqdata.subObj.getIsMapped(), App.mcqdata.subObj.getSubjectID(), App.mcqdata.getLevel(), App.mcqdata.getSelctedChapID())}).get();
            defultQuetionsEntries();
            for (McqQuestion que : mcqArray) {
                que.optionsArray.clear();
                que.optionsArray = (ArrayList) new CustomDatabaseQuery(this, new McqOption())
                        .execute(new String[]{DBNewQuery.get_mcq_option(que.getMCQQuestionID())}).get();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void inCorect() {
        mcqArray.clear();
        timerArray.clear();
        timerArray = new ArrayList<>();
        try {
            String bordId=mySharedPref.getBoardID();
            String mediumId=mySharedPref.getMediumID();
            String standardId=mySharedPref.getStandardID();
            mcqArray = (ArrayList) new CustomDatabaseQuery(this, new McqQuestion())
                    .execute(new String[]{DBNewQuery.get_mcq_incorect(bordId,mediumId,standardId,App.mcqdata.subObj.getIsMapped(), App.mcqdata.subObj.getSubjectID(), App.mcqdata.getLevel(), App.mcqdata.getSelctedChapID())}).get();
            defultQuetionsEntries();
            for (McqQuestion que : mcqArray) {
                que.optionsArray.clear();
                que.optionsArray = (ArrayList) new CustomDatabaseQuery(this, new McqOption())
                        .execute(new String[]{DBNewQuery.get_mcq_option(que.getMCQQuestionID())}).get();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setUpViewPapager() {
        if (mcqArray != null && mcqArray.size() != 0) {


            for (int i = 0; i < mcqArray.size(); i++) {

                fragments.add(new QuestionFragment().newInstance(i));
            }

            viewPager.setPageTransformer(false, new CustPagerTransformer(this));


            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    pagerTabStri.currentPosition = position;
                    pagerTabStri.currentPositionOffset = positionOffset;

                    pagerTabStri.scrollToChild(position, (int) (positionOffset * PagerSlidingTabStrip.tabsContainer.getChildAt(position).getWidth()));

                    pagerTabStri.invalidate();

                    if (pagerTabStri.delegatePageListener != null) {
                        pagerTabStri.delegatePageListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    if (state == ViewPager.SCROLL_STATE_IDLE) {
                        pagerTabStri.scrollToChild(pagerTabStri.pager.getCurrentItem(), 0);
                    }

                    if (pagerTabStri.delegatePageListener != null) {
                        pagerTabStri.delegatePageListener.onPageScrollStateChanged(state);
                    }
                }

                @Override
                public void onPageSelected(int position) {
                    Utils.Log("TAG", "Page no In listener--> " + position);
                    if (pagerTabStri.delegatePageListener != null) {
                        currentOpenedFragment = position;
                        Utils.Log("TAG", "Page no In listener--> " + position);

                        pagerTabStri.delegatePageListener.onPageSelected(position);

                    }

                }
            });


            questionAdapter = new QuestionAdapter(getSupportFragmentManager(), fragments);
            viewPager.setAdapter(questionAdapter);

            final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                    .getDisplayMetrics());
            viewPager.setPageMargin(pageMargin);
            pagerTabStri.setViewPager(viewPager);
            questionAdapter.notifyDataSetChanged();

        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("No any Quetions Found")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            dialog.dismiss();


                        }
                    })
                    .show();

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        App.MCQactive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        App.MCQactive = false;
    }


    public void setTimer(int Duration) {
        if (App.mcqdata.isTimer()) {

            milliseconds = Duration * 1000;
            timer = new CountDownTimer(milliseconds, 300) {

                public void onTick(long millisUntilFinished) {
                    SimpleDateFormat tempDf = new SimpleDateFormat("mm:ss", Locale.ENGLISH);
                    if (millisUntilFinished < timeBlinkInMilliseconds) {
                        tv_time.setTextAppearance(getApplicationContext(), R.style.blinkText);
                        if (blink) {
                            tv_time.setVisibility(View.VISIBLE);
                            // if blink is true, textview will be visible
                        } else {
                            tv_time.setVisibility(View.INVISIBLE);
                        }

                        blink = !blink;
                    }


                    tv_time.setText("" + String.format("%02d : %02d ",
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                    tempTotalDuration = (int) TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                }

                public void onFinish() {
                    ((QuestionFragment) questionAdapter.getItem(viewPager.getCurrentItem())).stopTimerOnBack();
                    questionAdapter.notifyDataSetChanged();

                    if (!instance.isFinishing()) {
                        new AlertDialog.Builder(instance)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Time Over!")
                                .setCancelable(false)
                                .setMessage("Do you still wish to continue ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ContinueTime = true;
                                        lin_time.setVisibility(View.VISIBLE);
                                        tv_time.setVisibility(View.VISIBLE);
                                        ((QuestionFragment) questionAdapter.getItem(viewPager.getCurrentItem())).startTimer();
                                        questionAdapter.notifyDataSetChanged();
                                        customHandler.postDelayed(updateTimerThread, 0);

                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ContinueTime = false;
                                        showProgress(true, true);//Utils.showProgressDialog(instance);
                                        new InsertRecorrd(instance).execute();
                                        dialog.dismiss();

                                    }
                                })

                                .show();//show dialog
                    }


                    //Utils.showProgressDialog(instance);
                    //ne0  InsertRecorrd(instance).execute();


                }
            }.start();

        } else {
            lin_time.setVisibility(View.INVISIBLE);
            tv_time.setVisibility(View.GONE);
        }

    }


    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            TotalDuration += 1;
            int secs = TotalDuration;
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (TotalDuration % 1000);
            try {

                tv_time.setText("" + String.format("%02d", mins) + ":"
                        + String.format("%02d", secs));
                ////Utils.Log("TAG ", "TIMER :-> " + String.format("%02d", mins) + ":" + String.format("%02d", secs));
            } catch (Exception e) {
                tv_time.setText("00:00");
                e.printStackTrace();
            }
            customHandler.postDelayed(this, 1000);
        }
    };


    public int getCurrantPos() {
        return viewPager.getCurrentItem();
    }

    @Override
    public void onPageVisible(int pos) {
        if (iDS == null) {
            iDS.add(String.valueOf(pos));
            tv_attempt.setText(Html.fromHtml("Attempt<font color=\"#fabb72\"> " + "0" + " </font><font color=\"#0d4568\"> /" + mcqArray.size() + "</font>"));
        }
        if (!iDS.contains(pos + "")) {
            iDS.add(String.valueOf(pos));
        }

        tv_attempt.setText(Html.fromHtml("Attempt<font color=\"#fabb72\"> " + iDS.size() + " </font><font color=\"#0d4568\"> /" + mcqArray.size() + "</font>"));

    }

    @Override
    public void onCompleted() {
        showProgress(false, true);

        if (Utils.isNetworkAvailable(instance)) {
            sendMCQData();

        } else {
            startActivity(new Intent(instance, TestReportActivity.class)
                    .putExtra("temp", headerId)
                    .putExtra("rightAnsArray", rightAnsArray)
                    .putExtra("incorectAnsArray", incorectAnsArray)
                    .putExtra("nonAttemptAnsArray", nonAttemptAnsArray)
                    .putExtra("isTime", App.mcqdata != null && App.mcqdata.isTimer())
                    .putExtra("TotalDuration", TotalDuration));
            finish();
        }

    }


    @Override
    public void onCompleted(String msg) {

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {

                    case sendMcqData:
                        jStudHdrArray = null;
                        showProgress(false, true);

                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("MCQLastSync")) {
                                String lastMCQSyncTime = jObj.getString("MCQLastSync");
                                mySharedPref.setLastMCQlastSync(lastMCQSyncTime);
                                mySharedPref.setLastSyncTime(lastMCQSyncTime);
                            }
                            try {
                                if (jObj.getJSONArray("data") != null) {
                                    JSONArray jData = jObj.getJSONArray("data");
                                    parseWebMCQData(jData);
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }


                        } else {
                            startActivity(new Intent(instance, TestReportActivity.class)
                                    .putExtra("temp", headerId)
                                    .putExtra("rightAnsArray", rightAnsArray)
                                    .putExtra("incorectAnsArray", incorectAnsArray)
                                    .putExtra("nonAttemptAnsArray", nonAttemptAnsArray)
                                    .putExtra("isTime", App.mcqdata != null && App.mcqdata.isTimer())
                                    .putExtra("TotalDuration", TotalDuration));
                            finish();
                        }


                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                startActivity(new Intent(instance, TestReportActivity.class)
                        .putExtra("temp", headerId)
                        .putExtra("rightAnsArray", rightAnsArray)
                        .putExtra("incorectAnsArray", incorectAnsArray)
                        .putExtra("nonAttemptAnsArray", nonAttemptAnsArray)
                        .putExtra("isTime", App.mcqdata != null && App.mcqdata.isTimer())
                        .putExtra("TotalDuration", TotalDuration));
                finish();
            }
        }
    }

    public void parseWebMCQData(JSONArray jArray) {
        if (jArray != null && jArray.length() > 0) {

            StudentMcqTestHdr StudentHdrObj;
            for (int i = 0; i < jArray.length(); i++) {
                try {

                    JSONObject obj = jArray.getJSONObject(i);
                    StudentHdrObj = new StudentMcqTestHdr();
                    StudentHdrObj.setStudentID(obj.getString("StudentID"));
                    StudentHdrObj.setBoardID(obj.getString("BoardID"));
                    StudentHdrObj.setMediumID(obj.getString("MediumID"));
                    StudentHdrObj.setSubjectID(obj.getString("SubjectID"));

                    StudentHdrObj.setTimeleft(obj.getString("Timeleft"));
                    StudentHdrObj.setAddType(obj.getString("AddType"));
                    StudentHdrObj.setCreatedBy(obj.getString("CreatedBy"));
                    StudentHdrObj.setCreatedOn(obj.getString("CreatedOn"));
                    StudentHdrObj.setModifiedBy(obj.getString("ModifiedBy"));
                    StudentHdrObj.setModifiedOn(obj.getString("ModifiedOn"));
                    StudentHdrObj.setModifiedOn(obj.getString("ModifiedOn"));

                    long ID = mDb.insertWebRow(StudentHdrObj, "student_mcq_test_hdr");
                    int inserID = Integer.parseInt(ID + "");

                    if (obj.has("chapterArray")) {
                        JSONArray chapterArray = obj.getJSONArray("chapterArray");
                        StudentMcqTestChap StudentChapObj;
                        if (chapterArray != null && chapterArray.length() > 0) {
                            for (int k = 0; k < chapterArray.length(); k++) {
                                JSONObject chapObj = chapterArray.getJSONObject(k);
                                StudentChapObj = new StudentMcqTestChap();

                                StudentChapObj.setStudentMCQTestHDRID((inserID + ""));
                                StudentChapObj.setChapterid(chapObj.getString("ChapterID"));

                                mDb.insertWebRow(StudentChapObj, "student_mcq_test_chapter");
                            }
                        }
                    }


                    if (obj.has("detailArray")) {
                        JSONArray detailArray = obj.getJSONArray("detailArray");

                        if (detailArray != null && detailArray.length() > 0) {
                            StudentMcqTestDtl mcqTestDtl;
                            for (int k = 0; k < detailArray.length(); k++) {
                                JSONObject detailObj = detailArray.getJSONObject(k);
                                mcqTestDtl = new StudentMcqTestDtl();
                                mcqTestDtl.setQuestionID(detailObj.getString("QuestionID"));
                                mcqTestDtl.setAnswerID(detailObj.getString("AnswerID"));
                                mcqTestDtl.setIsAttempt(detailObj.getString("IsAttempt"));
                                mcqTestDtl.setSrNo(detailObj.getString("srNo"));
                                mcqTestDtl.setCreatedBy(detailObj.getString("CreatedBy"));
                                mcqTestDtl.setCreatedOn(detailObj.getString("CreatedOn"));
                                mcqTestDtl.setModifiedBy(detailObj.getString("ModifiedBy"));
                                mcqTestDtl.setModifiedOn(detailObj.getString("ModifiedOn"));


                                mcqTestDtl.setStudentMCQTestHDRID(inserID);
                                mDb.insertWebRow(mcqTestDtl, "student_mcq_test_dtl");
                            }
                        }
                    }
                    if (obj.has("levelArray")) {
                        JSONArray levelArray = obj.getJSONArray("levelArray");

                        if (levelArray != null && levelArray.length() > 0) {
                            StudentMcqTestLevel mcqTestLevel;
                            for (int k = 0; k < levelArray.length(); k++) {
                                JSONObject levelObj = levelArray.getJSONObject(k);
                                mcqTestLevel = new StudentMcqTestLevel();
                                mcqTestLevel.setStudentMCQTestHDRID(inserID + "");
                                mcqTestLevel.setLevelID(levelObj.getString("LevelID"));

                                mDb.insertWebRow(mcqTestLevel, "student_mcq_test_level");
                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        startActivity(new Intent(instance, TestReportActivity.class)
                .putExtra("temp", headerId)
                .putExtra("rightAnsArray", rightAnsArray)
                .putExtra("incorectAnsArray", incorectAnsArray)
                .putExtra("nonAttemptAnsArray", nonAttemptAnsArray)
                .putExtra("isTime", App.mcqdata != null && App.mcqdata.isTimer())
                .putExtra("TotalDuration", TotalDuration));
        finish();
    }


    public class InsertTime extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        long time;
        int pageNumber;

        private InsertTime(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {
            pageNumber = Integer.parseInt(id[0]);
            time = Long.parseLong(id[1]);
            McqQuestion q;
            if (pageNumber > 1 && pageNumber < 20) {
                q = QuestionPagerActivity.mcqArray.get(pageNumber - 1);
            } else {
                q = QuestionPagerActivity.mcqArray.get(pageNumber);
            }
            //  Utils.Log("TAG", "Do iN Back :-> " + pageNumber + " TIme  :-> " + time);
            TempMcqTestDtl tempmcqdtl = new TempMcqTestDtl();
            tempmcqdtl.setQuestionID(q.MCQQuestionID);
            if (App.mcqdata.isTimer()) {
                tempmcqdtl.setQuestionTime(String.valueOf(time));
            } else
                tempmcqdtl.setQuestionTime(String.valueOf(0));
            Cursor MCqDtl = mDb.getAllRows(DBQueries.getAllRowQueryStdMCQDetail("temp_mcq_test_dtl"));
            if (MCqDtl != null && MCqDtl.moveToFirst()) {
                do {

                } while (MCqDtl.moveToNext());
            }
            Cursor c = mDb.getAllRows(DBQueries.getAllMcqDtl(tempmcqdtl));
            if (c != null && c.moveToNext()) {
                mDb.dbQuery("UPDATE temp_mcq_test_dtl SET QuestionTime = " + time + " WHERE QuestionID = " + q.MCQQuestionID);
            } else {
                mDb.insertTempRow(tempmcqdtl, "temp_mcq_test_dtl");
            }
            c.close();
            return "";
        }
    }

    public class InsertRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public TempMcqTestHdr tempHeader;
        public TempMcqTestChap tempmcqchap;
        public StudentMcqTestHdr studmcq;
        public StudentMcqTestChap studchap;
        public StudentMcqTestLevel studLevel;
        public StudentMcqTestDtl studdtl;
        public App app;
        public CursorParserUniversal cParse;
        public int lastHeaderID = -1;

        private InsertRecorrd(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {
            studmcq = new StudentMcqTestHdr();
            studchap = new StudentMcqTestChap();
            studdtl = new StudentMcqTestDtl();
            tempHeader = new TempMcqTestHdr();

            int lastID = mDb.getLastRecordID("student_mcq_test_hdr", "StudentMCQTestHDRID");
            Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllRowQuery("temp_mcq_test_hdr"));
            Utils.Log("TAG", " Total Durtion :> TotalDuration:-> " + TotalDuration + " tempDuration :-> " + tempTotalDuration);
            if (MCqHdr != null && MCqHdr.moveToFirst()) {

                do {
                    tempHeader = (TempMcqTestHdr) cParse.parseCursor(MCqHdr, new TempMcqTestHdr());
                    tempHeader.setTempMCQTestHDRID(++lastID);

                    if (App.mcqdata.isTimer()) {
                        tempHeader.setTimerStatus(1);
                        tempHeader.setTestTime(String.valueOf(TotalDuration));
                    } else {
                        tempHeader.setTimerStatus(0);
                        tempHeader.setTestTime("0");
                    }
                    mDb.insertRow(tempHeader, "student_mcq_test_hdr");
                } while (MCqHdr.moveToNext());
                MCqHdr.close();


            } else {
                //Utils.showToast("Not any Mcq hader", getActivity());
            }

            mDb.emptyTable("temp_mcq_test_hdr");

            lastHeaderID = mDb.getLastRecordID("student_mcq_test_hdr", "StudentMCQTestHDRID");

            Cursor MCqDtl = mDb.getAllRows(DBQueries.getAllRowQueryStdMCQDetail("temp_mcq_test_dtl"));
            if (MCqDtl != null && MCqDtl.moveToFirst()) {
                do {
                    TempMcqTestDtl temp = (TempMcqTestDtl) cParse.parseCursor(MCqDtl, new TempMcqTestDtl());
                    studdtl = new StudentMcqTestDtl();
                    studdtl.setStudentMCQTestHDRID(lastHeaderID);
                    studdtl.setQuestionID(temp.getQuestionID());
                    studdtl.setAnswerID(temp.getAnswerID());
                    studdtl.setIsAttempt(temp.getIsAttempt());
                    studdtl.setQuestionTime(temp.getQuestionTime());
                    DetlArray.add(studdtl);
                    mDb.insertRow(studdtl, "student_mcq_test_dtl");
                } while (MCqDtl.moveToNext());
                MCqDtl.close();
            } else {
                //   Utils.showToast("Not any Mcq detail", getActivity());
            }

            for (int i = 0; i < DetlArray.size(); i++) {
                if (i == viewPager.getCurrentItem()) {
                    mDb.dbQuery("UPDATE student_mcq_test_dtl SET QuestionTime = " + timerArray.get(i) / 1000 + " WHERE StudentMCQTestHDRID = " + lastHeaderID + " AND QuestionID = " + DetlArray.get(i).getQuestionID());
                }
            }
            mDb.emptyTable("temp_mcq_test_dtl");


            Cursor MCqChap = mDb.getAllRows(DBQueries.getAllRowQuery("temp_mcq_test_chapter"));
            if (MCqChap != null && MCqChap.moveToFirst()) {
                do {
                    TempMcqTestChap temp = (TempMcqTestChap) cParse.parseCursor(MCqChap, new TempMcqTestChap());
                    studchap = new StudentMcqTestChap();
                    studchap.setStudentMCQTestHDRID(String.valueOf(lastHeaderID));
                    studchap.setChapterid(temp.getChapterid());
                    mDb.insertRow(studchap, "student_mcq_test_chapter");
                } while (MCqChap.moveToNext());
                MCqChap.close();
            } else {
                // Utils.showToast("Not any mcq Chapter", getActivity());
            }

            mDb.emptyTable("temp_mcq_test_chapter");


            Cursor Mcqlevel = mDb.getAllRows(DBQueries.getAllRowQuery("temp_mcq_test_level"));
            if (Mcqlevel != null && Mcqlevel.moveToFirst()) {
                do {
                    TempMcqTestLevel temp = (TempMcqTestLevel) cParse.parseCursor(Mcqlevel, new TempMcqTestLevel());
                    studLevel = new StudentMcqTestLevel();
                    studLevel.setStudentMCQTestHDRID(String.valueOf(lastHeaderID));
                    studLevel.setLevelID(temp.getLevelID());
                    mDb.insertRow(studLevel, "student_mcq_test_level");
                } while (Mcqlevel.moveToNext());
                Mcqlevel.close();
            } else {
                // Utils.showToast("Not any mcq Chapter", getActivity());
            }

            mDb.emptyTable("temp_mcq_test_level");


            attempted = mDb.getAllRecordsUsingQuery(DBQueries.getStudentMCQAttempted(lastHeaderID + ""));

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            headerId = tempHeader.getTempMCQTestHDRID();

//            if (!mySharedPref.isFirstMcqTest()) {
                Log.d(TAG, "AlertBOX: ");
                mySharedPref.setIsFirstMcq(true);
                Bundle bundle = new Bundle();
                mFirebaseAnalytics.logEvent("MCQTEST", bundle);
//            }

            new getRecorrd(instance).execute();
        }


    }

    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;


        public McqQuestion mcqQuestion;
        public McqOption option;
        public StudentMcqTestChap testChapter;
        public StudentMcqTestDtl testDetail;

        public ArrayList<McqQuestion> mcqArray = new ArrayList<>();
        public ArrayList<McqQuestion> tempArray = new ArrayList<>();
        public ArrayList<StudentMcqTestChap> chapterArray = new ArrayList<>();
        public ArrayList<StudentMcqTestDtl> testDetailArray = new ArrayList<>();
        public int totalRight = 0, totalWrong = 0, totalAttempted = 0, totalNonAttempted = 0;


        public OnTaskCompleted listener;

        private getRecorrd(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            this.listener = (OnTaskCompleted) context;

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {

            Cursor MCQchapter = mDb.getAllRows(DBQueries.getStudentMCQChapters(headerId + ""));
            if (MCQchapter != null && MCQchapter.moveToFirst()) {
                do {
                    testChapter = (StudentMcqTestChap) cParse.parseCursor(MCQchapter, new StudentMcqTestChap());
                    chapterArray.add(testChapter);
                } while (MCQchapter.moveToNext());
                MCQchapter.close();
            } else {
            }
            Cursor MCQDetail = mDb.getAllRows(DBQueries.getStudentMCQDetai(headerId + ""));

            if (MCQDetail != null && MCQDetail.moveToFirst()) {
                do {
                    testDetail = (StudentMcqTestDtl) cParse.parseCursor(MCQDetail, new StudentMcqTestDtl());
                    testDetailArray.add(testDetail);
                } while (MCQDetail.moveToNext());
                MCQDetail.close();
            } else {
            }
            String queIDS = "";
            for (StudentMcqTestDtl detail : testDetailArray) {
                queIDS += detail.getQuestionID() + ",";
            }
            try {
                queIDS = queIDS.substring(0, queIDS.length() - 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Cursor MCQQuetions = mDb.getAllRows(DBQueries.getAttemptedQuetion(queIDS));
            MCQQuetions.getCount();
            if (MCQQuetions != null && MCQQuetions.moveToFirst()) {
                do {
                    mcqQuestion = (McqQuestion) cParse.parseCursor(MCQQuetions, new McqQuestion());
                    mcqArray.add(mcqQuestion);
                } while (MCQQuetions.moveToNext());
                MCQQuetions.close();
            }
            int i = 0;
            for (Iterator<StudentMcqTestDtl> it = testDetailArray.iterator(); it.hasNext(); ) {
                StudentMcqTestDtl detail = it.next();
                //  Utils.Log("TAG ", " TAG : FIRST LOOP  :->" + ++i);
                int j = 0;
                for (Iterator<McqQuestion> mcqIT = mcqArray.iterator(); mcqIT.hasNext(); ) {
                    McqQuestion que = mcqIT.next();
                    //    Utils.Log("TAG ", " TAG : Second LOOP  :->" + i + " : " + ++j);

                    if (que.getMCQQuestionID().equalsIgnoreCase(detail.getQuestionID())) {
                        try {
                            //  Utils.Log("TAG ", " TAG : Second LOOP  try:->" + i + " : " + j);
                            tempArray.add((McqQuestion) que.clone());

                            System.out.print("");
                            break;
                        } catch (CloneNotSupportedException e) {
                        }

                    }


                }
            }
            mcqArray.clear();
            mcqArray = (ArrayList<McqQuestion>) tempArray.clone();
            // Utils.Log("TAG ", " TAG : mcqArray " + mcqArray.size());
            Cursor oc = null;
            totalAttempted = 0;
            for (McqQuestion que : mcqArray) {
                String rightAnswerId = "";
                for (StudentMcqTestDtl dtl : testDetailArray) {
                    if (dtl.getQuestionID().equalsIgnoreCase(que.getMCQQuestionID())) {
                        rightAnswerId = dtl.getAnswerID();
                        if (!TextUtils.isEmpty(rightAnswerId))
                            que.selectedAns = Integer.parseInt(rightAnswerId);
                        if (!dtl.getIsAttempt().isEmpty()) {
                            if (dtl.getIsAttempt().equalsIgnoreCase("1")) {
                                que.isAttempted = 1;
                                ++totalAttempted;
                            } else if (dtl.getIsAttempt().equalsIgnoreCase("0")) {
                                que.isAttempted = 0;
                            }
                        }
                    }
                }
                oc = mDb.getAllRows(DBQueries.getMCQAnswers(que.MCQQuestionID));
                if (oc != null && oc.moveToFirst()) {
                    que.optionsArray.clear();
                    do {
                        option = (McqOption) cParse.parseCursor(oc, new McqOption());
                        // Utils.Log("TAg", "is OPtin:->" + option.getMCQOPtionID() + " = " + rightAnswerId + " && " + option.isCorrect + " 1 ");
                        if (option.getMCQOPtionID().equalsIgnoreCase(rightAnswerId) && option.isCorrect.equalsIgnoreCase("1")) {
                            option.isRightAns = 1;
                            que.isRight = true;
                            ////Utils.Log("TAg", "is right:->" + que.isRight);
                        }
                        que.optionsArray.add(option);
                    } while (oc.moveToNext());
                } else {
                }
            }

            if (oc != null)
                oc.close();

            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    ++i;
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            nonAttemptAnsArray.clear();
            incorectAnsArray.clear();
            rightAnsArray.clear();


            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    rightAnsArray.add(que);
                    if (mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueIncorrect(que.MCQQuestionID)) == 1) {
                        mDb.dbQuery(DBNewQuery.DeleteQueIncorrect(que.MCQQuestionID));
                    }
                    if (mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueNotAppeared(que.MCQQuestionID)) == 1) {
                        mDb.dbQuery(DBNewQuery.DelteQueNotAppeared(que.MCQQuestionID));
                    }

                }
                if (que.isAttempted == 1 && !que.isRight) {
                    incorectAnsArray.add(que);
                    if (mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueNotAppeared(que.MCQQuestionID)) == 1) {
                        mDb.dbQuery(DBNewQuery.DelteQueNotAppeared(que.MCQQuestionID));
                    }
                    // Utils.Log("TAG "," CheckQueIncorrect :-> "+mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueIncorrect(que.MCQQuestionID)));
                    if (mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueIncorrect(que.MCQQuestionID)) == 0) {
                        StudentIncorrectQuestion studentIncorrectQuestion = new StudentIncorrectQuestion();
                        studentIncorrectQuestion.setQuestionID(que.MCQQuestionID);
                        mDb.insertRow(studentIncorrectQuestion, "student_incorrect_question");
                    }

                }
                if (que.isAttempted == 0) {
                    nonAttemptAnsArray.add(que);

                    //  Utils.Log("TAG "," CheckQueNotAppeared :-> "+mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueNotAppeared(que.MCQQuestionID)));
                    if (mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueIncorrect(que.MCQQuestionID)) == 1) {
                        mDb.dbQuery(DBNewQuery.DeleteQueIncorrect(que.MCQQuestionID));
                    }

                    if (mDb.getAllRecordsUsingQuery(DBNewQuery.CheckQueNotAppeared(que.MCQQuestionID)) == 0) {
                        StudentnotAppearednQuestion studentnotAppearednQuestion = new StudentnotAppearednQuestion();
                        studentnotAppearednQuestion.setQuestionID(que.MCQQuestionID);
                        mDb.insertRow(studentnotAppearednQuestion, "student_not_appeared_question");
                    }

                }

            }

            listener.onCompleted();

        }

    }

    public void defultQuetionsEntries() {
        TempMcqTestDtl tempmcqdtl;

        for (McqQuestion que : mcqArray) {
            tempmcqdtl = new TempMcqTestDtl();
            tempmcqdtl.setQuestionID(que.MCQQuestionID);
            tempmcqdtl.setAnswerID("");
            tempmcqdtl.setIsAttempt("0");
            Cursor c = mDb.getAllRows(DBQueries.getAllMcqDtl(tempmcqdtl));
            if (c != null && c.moveToNext()) {
                mDb.updateRow(tempmcqdtl, "temp_mcq_test_dtl", "QuestionID=" + que.MCQQuestionID);
            } else {
                mDb.insertTempRow(tempmcqdtl, "temp_mcq_test_dtl");
            }
            c.close();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        AlertBOX("Are you sure you want to leave the test?", "onBack");

    }

    public void btnSubmit() {

        AlertBOX("Are you sure about test submission?", "submit");
    }

    public void gotoDashboard() {
        AlertBOX("Sure about visiting the dashboard?", "dashboard");

    }

    public void AlertBOX(String msg, final String type) {

        if (timer != null)
            timer.cancel();
        if (questionAdapter != null) {
            ((QuestionFragment) questionAdapter.getItem(viewPager.getCurrentItem())).stopTimerOnBack();
            questionAdapter.notifyDataSetChanged();
        }
        if (customHandler != null) {
            customHandler.removeCallbacks(updateTimerThread);
        }

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton("Yes", (dialog, which) -> {
                    if (type.equals("submit")) {
                        showProgress(true, true);//Utils.showProgressDialog(instance);
                        TotalDuration = TotalDuration - tempTotalDuration;
                        for (int i = 0; i < timerArray.size(); i++) {
                            new InsertTime(instance).execute(String.valueOf(i), String.valueOf(timerArray.get(i) / 1000));
                            timeUpdate(timerArray.get(i) / 1000, i);
                        }
                        new InsertRecorrd(instance).execute();
                        dialog.dismiss();
                    }
                    if (type.equals("onBack")) {
                        finish();
                        dialog.dismiss();
                    }
                    if (type.equals("dashboard")) {
                        Log.d(TAG, "AlertBOX: "+mySharedPref.isFirstMcqTest());

                        showProgress(true, true);//Utils.showProgressDialog(instance);
                        TotalDuration = TotalDuration - tempTotalDuration;
                        startActivity(new Intent(instance, DashBoardActivity.class));
                        finish();
                        dialog.dismiss();
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            if (App.mcqdata.isTimer()) {
                                temp = tempTotalDuration;
                                if (!ContinueTime) {
                                    timer.cancel();
                                    setTimer(temp);
                                } else {
                                    customHandler.postDelayed(updateTimerThread, 0);
                                }
                                ((QuestionFragment) questionAdapter.getItem(viewPager.getCurrentItem())).startTimer();
                                questionAdapter.notifyDataSetChanged();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })
                .show();

    }

    public void sendMCQData() {
        StuHdrArray.clear();
        jStudHdrArray = null;

        try {
            StuHdrArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentMcqTestHdr())
                    .execute(new String[]{DBQueries.getAllStudentHdr(mySharedPref.getLastMCQSyncTime())}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        for (StudentMcqTestHdr header : StuHdrArray) {
            String hdr = String.valueOf(header.getStudentMCQTestHDRID());

            try {
                header.chapterArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentMcqTestChap())
                        .execute(new String[]{DBQueries.getStudentMcqChap(hdr)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            try {
                header.detailArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentMcqTestDtl())
                        .execute(new String[]{DBQueries.getStudentMCQDetail(hdr)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            try {
                header.levelArray = (ArrayList) new CustomDatabaseQuery(Staticinstance, new StudentMcqTestLevel())
                        .execute(new String[]{DBQueries.getStudentMcqLevel(hdr)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }


        }

        Gson gson = new GsonBuilder().create();
        jStudHdrArray = gson.toJsonTree(StuHdrArray).getAsJsonArray();

        //longInfo(String.valueOf(jStudHdrArray), "jStudHdrArray");

        if (jStudHdrArray.size() > 0) {
            new CallRequest(Staticinstance).sendMcqDataLogin(jStudHdrArray.toString());
        } else {
            new CallRequest(Staticinstance).sendMcqDataLogin("[]");
        }
    }
}
