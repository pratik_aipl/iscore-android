package com.parshvaa.isccore.activity.searchpaper.practisepaper.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.parshvaa.isccore.activity.searchpaper.feftdrawer.LeftSubjectAdapter;
import com.parshvaa.isccore.activity.searchpaper.practisepaper.adapter.RightAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.FilterArrayListListener;
import com.parshvaa.isccore.model.NavDrawerItem;
import com.parshvaa.isccore.model.Paper_type;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.FragmentDrawer;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by empiere-vaibhav on 1/11/2018.
 */

public class RightPaperTypeDrawer extends Fragment {

    private static String TAG = FragmentDrawer.class.getSimpleName();

    private RecyclerView recyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private RightAdapter adapter;
    private LeftSubjectAdapter LeftAdapter;
    private View containerView;
    private static String[] titles = null;
    //    private static int[] icons = null;
    private FragmentDrawerListener drawerListener;
    private static TypedArray icons;
    public Button btn_filter;
    public FilterArrayListListener filterListener;
    public ArrayList<Paper_type> paperTypeList = new ArrayList<>();
    private LevelrrayListListener levelListener;

    public RightPaperTypeDrawer() {

    }


    public void setDrawerListener(FilterArrayListListener listener) {
        this.filterListener = listener;
    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();


        // preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setLeftSubName(titles[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // drawer labels


        titles = getActivity().getResources().getStringArray(R.array.diff_level);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_left_drawer, container, false);
        recyclerView = layout.findViewById(R.id.recycler_paper);
        btn_filter = layout.findViewById(R.id.btn_filter);
        App.checked_paper_type = new ArrayList<>();
        try {

            //  System.out.println("Total Rows on try");

            paperTypeList = (ArrayList) new CustomDatabaseQuery(getActivity(), new Paper_type())
                    .execute(new String[]{DBNewQuery.getAllRows("paper_type")}).get();


        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Utils.Log("TAG", "Array Size :-> " + paperTypeList.size());
        adapter = new RightAdapter(paperTypeList, getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                drawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterListener.onRightDrawerItemSelected();
                mDrawerLayout.closeDrawers();
            }
        });
        return layout;
    }

    public interface LevelrrayListListener {
        void onDrawerItemSelected(ArrayList<String> SubjectId, ArrayList<String> LevelId);
    }

    public void setUpRightButton(int fragmentId, DrawerLayout drawerLayout, RelativeLayout drawerButton, final View mainLay) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        drawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                else
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
//                toolbar.setAlpha(1 - slideOffset / 2);
                mainLay.setTranslationX(0);
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {
        public RecyclerTouchListener(Activity activity, RecyclerView recyclerView, ClickListener clickListener) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }

        private GestureDetector gestureDetector;
        private FragmentDrawer.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final FragmentDrawer.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }


    }

    public interface FragmentDrawerListener {
        void onDrawerItemSelected(View view, int position);
    }
}
