package com.parshvaa.isccore.activity.iconnect;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */
@JsonObject
public class EMcqPaper implements Serializable {

    @JsonField
    public String PaperID = "";
    @JsonField
    public String SubjectName = "";
    @JsonField
    public String PaperFile = "";

    @JsonField
    public String ModifiedOn="";
    @JsonField
    public String ExpiryDate="";
    @JsonField
    public String TakenTest="";
    @JsonField
    public String StartTime="";
    @JsonField
    public String EndTime="";
    @JsonField
    public String PublishType="";

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getPublishType() {
        return PublishType;
    }

    public void setPublishType(String publishType) {
        PublishType = publishType;
    }

    public String getPaperID() {
        return PaperID;
    }

    public void setPaperID(String paperID) {
        PaperID = paperID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getPaperFile() {
        return PaperFile;
    }

    public void setPaperFile(String paperFile) {
        PaperFile = paperFile;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getTakenTest() {
        return TakenTest;
    }

    public void setTakenTest(String takenTest) {
        TakenTest = takenTest;
    }
}
