package com.parshvaa.isccore.activity.iconnect.activity;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.iconnect.fragment.IMcqTestFragment;
import com.parshvaa.isccore.activity.iconnect.fragment.IPracticePaperFragment;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.RobotoTextView;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ISearchActivity extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    public RelativeLayout mainLay;
    public ImageView img_back;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_isearch);
        Utils.logUser();

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Search Papers");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabFont();
    }

    private void setupTabFont() {

        RobotoTextView mcqTest = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        mcqTest.setText("MCQ TEST");
        tabLayout.getTabAt(0).setCustomView(mcqTest);

        RobotoTextView practicePaper = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_report_tab, null);
        practicePaper.setText("PRACTISE PAPER");
        tabLayout.getTabAt(1).setCustomView(practicePaper);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new IMcqTestFragment(), "MCQ TEST");
        adapter.addFragment(new IPracticePaperFragment(), "PRACTISE PAPER");

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}

