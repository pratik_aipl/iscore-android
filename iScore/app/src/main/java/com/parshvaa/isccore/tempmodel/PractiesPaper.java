package com.parshvaa.isccore.tempmodel;

import com.parshvaa.isccore.model.SetPaperQuestionTypes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 1/20/2018.
 */

public class PractiesPaper implements Serializable {
    public String SubjectName = "";
    public String subjectID = "";
    public String SubjectIcon = "";
    public String PaperTypeID = "";
    public String ExamTypeID = "";
    public String  TotalTest="";
    public String IsMapped="";
    public String OldSubjectID="";

    public String getOldSubjectID() {
        return OldSubjectID;
    }

    public void setOldSubjectID(String oldSubjectID) {
        OldSubjectID = oldSubjectID;
    }

    public String getIsMapped() {
        return IsMapped;
    }

    public void setIsMapped(String isMapped) {
        IsMapped = isMapped;
    }


    public String getTotalTest() {
        return TotalTest;
    }

    public void setTotalTest(String totalTest) {
        TotalTest = totalTest;
    }

    public String getSubjectIcon() {
        return SubjectIcon;
    }

    public void setSubjectIcon(String subjectIcon) {
        SubjectIcon = subjectIcon;
    }

    public String ChapterID = "";
    public String chapterNumber = "";
    public String Duration = "";
    public String ExamTypeName = "";
    public String ExamTypePatternId = "";
    public String StudentQuestionTypeID = "";
    public String StudentQuestionPaperID = "";

    public String getStudentQuestionPaperID() {
        return StudentQuestionPaperID;
    }

    public void setStudentQuestionPaperID(String studentQuestionPaperID) {
        StudentQuestionPaperID = studentQuestionPaperID;
    }

    public String getStudentQuestionTypeID() {
        return StudentQuestionTypeID;
    }

    public void setStudentQuestionTypeID(String studentQuestionTypeID) {
        StudentQuestionTypeID = studentQuestionTypeID;
    }

    public String getExamTypePatternId() {
        return ExamTypePatternId;
    }

    public void setExamTypePatternId(String examTypePatternId) {
        ExamTypePatternId = examTypePatternId;
    }

    public String getExamTypeName() {
        return ExamTypeName;
    }

    public void setExamTypeName(String examTypeName) {
        ExamTypeName = examTypeName;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String TotalMarks = "";
    public ArrayList<SetPaperQuestionTypes> setPaperQuestionTypesArray = new ArrayList<>();

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getPaperTypeID() {
        return PaperTypeID;
    }

    public void setPaperTypeID(String paperTypeID) {
        PaperTypeID = paperTypeID;
    }

    public String getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(String examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(String chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }


}
