package com.parshvaa.isccore.calendar.model;


import java.io.Serializable;

public class PaperDates implements Serializable {

 //   @JsonField(name = "date")
    String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
