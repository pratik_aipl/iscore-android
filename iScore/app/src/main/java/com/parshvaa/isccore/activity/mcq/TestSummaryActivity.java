package com.parshvaa.isccore.activity.mcq;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.OverScroller;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nineoldandroids.view.ViewHelper;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.mcq.fragment.CorrectSummaryFragment;
import com.parshvaa.isccore.activity.mcq.fragment.IncorrectFragment;
import com.parshvaa.isccore.activity.mcq.fragment.NotAppearedFragment;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.McqOption;
import com.parshvaa.isccore.model.McqQuestion;
import com.parshvaa.isccore.model.StudentMcqTestChap;
import com.parshvaa.isccore.model.StudentMcqTestDtl;
import com.parshvaa.isccore.model.TempMcqTestHdr;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.CacheFragmentStatePagerAdapter;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.observscroll.Scrollable;
import com.parshvaa.isccore.observscroll.SlidingTabLayout;
import com.parshvaa.isccore.observscroll.TouchInterceptionFrameLayout;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.Iterator;

public class TestSummaryActivity extends BaseActivity implements ObservableScrollViewCallbacks {

    public TestSummaryActivity instance;

    public TextView tv_non_attempted, tv_incorrect, tv_correct;

    private TabLayout mSlidingTabLayout;

    public TempMcqTestHdr tempHeader;
    public static ArrayList<McqQuestion> rightAnsArray = new ArrayList<>();
    public static ArrayList<McqQuestion> nonAttemptAnsArray = new ArrayList<>();
    public static ArrayList<McqQuestion> incorectAnsArray = new ArrayList<>();


    private static final float MAX_TEXT_SCALE_DELTA = 0.3f;
    private static final int INVALID_POINTER = -1;

    private View mImageView;
    private RelativeLayout mOverlayView;
    private TouchInterceptionFrameLayout mInterceptionLayout;
    private ViewPager mPager;
    private VelocityTracker mVelocityTracker;
    private OverScroller mScroller;
    private float mBaseTranslationY;
    private int mMaximumVelocity;
    private int mActivePointerId = INVALID_POINTER;
    private int mSlop;
    private int mFlexibleSpaceHeight;
    private int mTabHeight, headerId;
    private boolean mScrolled;
    private NavigationAdapter mPagerAdapter;

    ImageView img_back, img_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_test_summary);
        Utils.logUser();

        instance = this;
        showProgress(true, true);//Utils.showProgressDialog(instance);


        headerId = getIntent().getExtras().getInt("temp");
        rightAnsArray = (ArrayList<McqQuestion>) getIntent().getExtras().getSerializable("rightAnsArray");
        incorectAnsArray = (ArrayList<McqQuestion>) getIntent().getExtras().getSerializable("incorectAnsArray");
        nonAttemptAnsArray = (ArrayList<McqQuestion>) getIntent().getExtras().getSerializable("nonAttemptAnsArray");

        img_back = findViewById(R.id.img_back);
        img_home = findViewById(R.id.img_home);
        tv_non_attempted = findViewById(R.id.tv_non_attempted);
        tv_incorrect = findViewById(R.id.tv_incorrect);
        tv_correct = findViewById(R.id.tv_correct);

        tv_correct.setText("   " + rightAnsArray.size() + " Correct");
        tv_incorrect.setText("   " + incorectAnsArray.size() + " Incorrect");
        tv_non_attempted.setText("   " + nonAttemptAnsArray.size() + " Not Appeared");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });


        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ViewCompat.setElevation(findViewById(R.id.header), getResources().getDimension(R.dimen._4sdp));
        mPagerAdapter = new NavigationAdapter(getSupportFragmentManager());
        mPager = findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mImageView = findViewById(R.id.image);
        mOverlayView = findViewById(R.id.overlay);
        // because with padding, EdgeEffect of ViewPager become strange.
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mTabHeight = getResources().getDimensionPixelSize(R.dimen._50sdp);
        findViewById(R.id.pager_wrapper).setPadding(0, mFlexibleSpaceHeight, 0, 0);
        SlidingTabLayout slidingTabLayout = findViewById(R.id.sliding_tabs);
        slidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.wrong));
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(mPager);
        ((FrameLayout.LayoutParams) slidingTabLayout.getLayoutParams()).topMargin = mFlexibleSpaceHeight - mTabHeight;

        ViewConfiguration vc = ViewConfiguration.get(this);
        mSlop = vc.getScaledTouchSlop();
        mMaximumVelocity = vc.getScaledMaximumFlingVelocity();
        mInterceptionLayout = findViewById(R.id.container);
        //mInterceptionLayout.setScrollInterceptionListener(mInterceptionListener);
        mScroller = new OverScroller(getApplicationContext());
       /* ScrollUtils.addOnGlobalLayoutListener(mInterceptionLayout, new Runnable() {
            @Override
            public void run() {
                // Extra space is required to move mInterceptionLayout when it's scrolled.
                // It's better to adjust its height when it's laid out
                // than to adjust the height when scroll events (onMoveMotionEvent) occur
                // because it causes lagging.
                // See #87: https://github.com/ksoichiro/Android-ObservableScrollView/issues/87
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mInterceptionLayout.getLayoutParams();
                lp.height = getScreenHeight() + mFlexibleSpaceHeight - mTabHeight;
                mInterceptionLayout.requestLayout();

                updateFlexibleSpace();
            }
        });*/


        showProgress(false, true);
        //new sendToPrelimAndRedy(instance).execute();
    }

    public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)
                .setMessage("Are you sure you want to open dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(instance, DashBoardActivity.class));
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private TouchInterceptionFrameLayout.TouchInterceptionListener mInterceptionListener = new TouchInterceptionFrameLayout.TouchInterceptionListener() {
        @Override
        public boolean shouldInterceptTouchEvent(MotionEvent ev, boolean moving, float diffX, float diffY) {
            if (!mScrolled && mSlop < Math.abs(diffX) && Math.abs(diffY) < Math.abs(diffX)) {
                // Horizontal scroll is maybe handled by ViewPager
                return false;
            }

            Scrollable scrollable = getCurrentScrollable();
            if (scrollable == null) {
                mScrolled = false;
                return false;
            }

            // If interceptionLayout can move, it should intercept.
            // And once it begins to move, horizontal scroll shouldn't work any longer.
            int flexibleSpace = mFlexibleSpaceHeight - mTabHeight;
            int translationY = (int) ViewHelper.getTranslationY(mInterceptionLayout);
            boolean scrollingUp = 0 < diffY;
            boolean scrollingDown = diffY < 0;
            if (scrollingUp) {
                if (translationY < 0) {
                    mScrolled = true;
                    return true;
                }
            } else if (scrollingDown) {
                if (-flexibleSpace < translationY) {
                    mScrolled = true;
                    return true;
                }
            }
            mScrolled = false;
            return false;
        }

        @Override
        public void onDownMotionEvent(MotionEvent ev) {
            mActivePointerId = ev.getPointerId(0);
            mScroller.forceFinished(true);
            if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
            } else {
                mVelocityTracker.clear();
            }
            mBaseTranslationY = ViewHelper.getTranslationY(mInterceptionLayout);
            mVelocityTracker.addMovement(ev);
        }

        @Override
        public void onMoveMotionEvent(MotionEvent ev, float diffX, float diffY) {
            int flexibleSpace = mFlexibleSpaceHeight - mTabHeight;
            float translationY = ScrollUtils.getFloat(ViewHelper.getTranslationY(mInterceptionLayout) + diffY, -flexibleSpace, 0);
            MotionEvent e = MotionEvent.obtainNoHistory(ev);
            e.offsetLocation(0, translationY - mBaseTranslationY);
            mVelocityTracker.addMovement(e);
            updateFlexibleSpace(translationY);
        }

        @Override
        public void onUpOrCancelMotionEvent(MotionEvent ev) {
            mScrolled = false;
            try {
                mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
                int velocityY = (int) mVelocityTracker.getYVelocity(mActivePointerId);
                mActivePointerId = INVALID_POINTER;
                mScroller.forceFinished(true);
                int baseTranslationY = (int) ViewHelper.getTranslationY(mInterceptionLayout);

                int minY = -(mFlexibleSpaceHeight - mTabHeight);
                int maxY = 0;
                mScroller.fling(0, baseTranslationY, 0, velocityY, 0, 0, minY, maxY);
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        updateLayout();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void updateLayout() {
        boolean needsUpdate = false;
        float translationY = 0;
        if (mScroller.computeScrollOffset()) {
            translationY = mScroller.getCurrY();
            int flexibleSpace = mFlexibleSpaceHeight - mTabHeight;
            if (-flexibleSpace <= translationY && translationY <= 0) {
                needsUpdate = true;
            } else if (translationY < -flexibleSpace) {
                translationY = -flexibleSpace;
                needsUpdate = true;
            } else if (0 < translationY) {
                translationY = 0;
                needsUpdate = true;
            }
        }

        if (needsUpdate) {
            updateFlexibleSpace(translationY);

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    updateLayout();
                }
            });
        }
    }

    private Scrollable getCurrentScrollable() {
        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            return null;
        }
        View view = fragment.getView();
        if (view == null) {
            return null;
        }
        return (Scrollable) view.findViewById(R.id.scroll);
    }

    private void updateFlexibleSpace() {
        updateFlexibleSpace(ViewHelper.getTranslationY(mInterceptionLayout));
    }

    private void updateFlexibleSpace(float translationY) {
        ViewHelper.setTranslationY(mInterceptionLayout, translationY);
        int minOverlayTransitionY = getActionBarSize() - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-translationY / 2, minOverlayTransitionY, 0));

        // Change alpha of overlay
        float flexibleRange = mFlexibleSpaceHeight - getActionBarSize();
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat(-translationY / flexibleRange, 0, 1));

        // Scale title text
        float scale = 1 + ScrollUtils.getFloat((flexibleRange + translationY - mTabHeight) / flexibleRange, 0, MAX_TEXT_SCALE_DELTA);
        setPivotXToTitle();

    }

    private Fragment getCurrentFragment() {
        return mPagerAdapter.getItemAt(mPager.getCurrentItem());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setPivotXToTitle() {
        Configuration config = getResources().getConfiguration();
        if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT
                && config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            // ViewHelper.setPivotX(mTitleView, findViewById(android.R.id.content).getWidth());
        } else {
            //  ViewHelper.setPivotX(mTitleView, 0);
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    private static class NavigationAdapter extends CacheFragmentStatePagerAdapter {

        private static final String[] TITLES = new String[]{"Correct", "Incorrect", "Not Appeared"};


        public NavigationAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        protected Fragment createItem(int position) {
            Fragment f;
            final int pattern = position % 5;
            switch (pattern) {

                case 0: {
                    f = new CorrectSummaryFragment();
                    break;
                }
                case 1: {
                    f = new IncorrectFragment();
                    break;
                }
                case 2: {
                    f = new NotAppearedFragment();
                    break;
                }
                default: {
                    f = new CorrectSummaryFragment();
                    break;
                }

            }
            return f;
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }


    }


    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;


        public McqQuestion mcqQuestion;
        public McqOption option;
        public StudentMcqTestChap testChapter;
        public StudentMcqTestDtl testDetail;

        public ArrayList<McqQuestion> mcqArray = new ArrayList<>();
        public ArrayList<McqQuestion> tempArray = new ArrayList<>();
        public ArrayList<StudentMcqTestChap> chapterArray = new ArrayList<>();
        public ArrayList<StudentMcqTestDtl> testDetailArray = new ArrayList<>();
        public int totalRight = 0, totalWrong = 0, totalAttempted = 0, totalNonAttempted = 0;


        private getRecorrd(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {

            Cursor MCQchapter = mDb.getAllRows(DBQueries.getStudentMCQChapters(tempHeader.getTempMCQTestHDRID() + ""));
            if (MCQchapter != null && MCQchapter.moveToFirst()) {
                do {
                    testChapter = (StudentMcqTestChap) cParse.parseCursor(MCQchapter, new StudentMcqTestChap());
                    chapterArray.add(testChapter);
                } while (MCQchapter.moveToNext());
                MCQchapter.close();
            } else {
            }
            Cursor MCQDetail = mDb.getAllRows(DBQueries.getStudentMCQDetai(headerId + ""));

            if (MCQDetail != null && MCQDetail.moveToFirst()) {
                do {
                    testDetail = (StudentMcqTestDtl) cParse.parseCursor(MCQDetail, new StudentMcqTestDtl());
                    testDetailArray.add(testDetail);
                } while (MCQDetail.moveToNext());
                MCQDetail.close();
            } else {
            }
            String queIDS = "";
            for (StudentMcqTestDtl detail : testDetailArray) {
                queIDS += detail.getQuestionID() + ",";
            }
            queIDS = queIDS.substring(0, queIDS.length() - 1);
            Cursor MCQQuetions = mDb.getAllRows(DBQueries.getAttemptedQuetion(queIDS));
            MCQQuetions.getCount();
            if (MCQQuetions != null && MCQQuetions.moveToFirst()) {
                do {
                    mcqQuestion = (McqQuestion) cParse.parseCursor(MCQQuetions, new McqQuestion());
                    mcqArray.add(mcqQuestion);
                } while (MCQQuetions.moveToNext());
                MCQQuetions.close();
            }
            int i = 0;
            for (Iterator<StudentMcqTestDtl> it = testDetailArray.iterator(); it.hasNext(); ) {
                StudentMcqTestDtl detail = it.next();
                //  Utils.Log("TAG ", " TAG : FIRST LOOP  :->" + ++i);
                int j = 0;
                for (Iterator<McqQuestion> mcqIT = mcqArray.iterator(); mcqIT.hasNext(); ) {
                    McqQuestion que = mcqIT.next();
                    //    Utils.Log("TAG ", " TAG : Second LOOP  :->" + i + " : " + ++j);

                    if (que.getMCQQuestionID().equalsIgnoreCase(detail.getQuestionID())) {
                        try {
                            //  Utils.Log("TAG ", " TAG : Second LOOP  try:->" + i + " : " + j);
                            tempArray.add((McqQuestion) que.clone());

                            System.out.print("");
                            break;
                        } catch (CloneNotSupportedException e) {
                        }

                    }


                }
            }
            mcqArray.clear();
            mcqArray = (ArrayList<McqQuestion>) tempArray.clone();
            // Utils.Log("TAG ", " TAG : mcqArray " + mcqArray.size());
            Cursor oc = null;
            totalAttempted = 0;
            for (McqQuestion que : mcqArray) {
                String rightAnswerId = "";
                for (StudentMcqTestDtl dtl : testDetailArray) {
                    if (dtl.getQuestionID().equalsIgnoreCase(que.getMCQQuestionID())) {
                        rightAnswerId = dtl.getAnswerID();
                        try {
                            que.selectedAns = Integer.parseInt(rightAnswerId);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        if (!dtl.getIsAttempt().isEmpty()) {
                            if (dtl.getIsAttempt().equalsIgnoreCase("1")) {

                                que.isAttempted = 1;
                                ++totalAttempted;

                            } else if (dtl.getIsAttempt().equalsIgnoreCase("0")) {

                                que.isAttempted = 0;
                            }
                        }

                    }
                }
                oc = mDb.getAllRows(DBQueries.getMCQAnswers(que.MCQQuestionID));
                if (oc != null && oc.moveToFirst()) {
                    que.optionsArray.clear();
                    do {
                        option = (McqOption) cParse.parseCursor(oc, new McqOption());
                        // Utils.Log("TAg", "is OPtin:->" + option.getMCQOPtionID() + " = " + rightAnswerId + " && " + option.isCorrect + " 1 ");
                        if (option.getMCQOPtionID().equalsIgnoreCase(rightAnswerId) && option.isCorrect.equalsIgnoreCase("1")) {
                            option.isRightAns = 1;
                            que.isRight = true;
                            ////Utils.Log("TAg", "is right:->" + que.isRight);
                        }
                        que.optionsArray.add(option);
                    } while (oc.moveToNext());
                } else {
                }
            }

            if (oc != null)
                oc.close();

            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    ++i;
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tempHeader = (TempMcqTestHdr) getIntent().getExtras().getSerializable("temp");
            headerId = tempHeader.getTempMCQTestHDRID();
            nonAttemptAnsArray.clear();
            incorectAnsArray.clear();
            rightAnsArray.clear();

            McqQuestion question = new McqQuestion();
            question.isHeader = true;
            nonAttemptAnsArray.add(question);
            incorectAnsArray.add(question);
            rightAnsArray.add(question);

            for (McqQuestion que : mcqArray) {
                if (que.isRight) {
                    rightAnsArray.add(que);
                }
                if (que.isAttempted == 1 && !que.isRight) {
                    incorectAnsArray.add(que);
                }
                if (que.isAttempted == 0) {
                    nonAttemptAnsArray.add(que);
                }


            }


            showProgress(false, true);
        }
    }
}
