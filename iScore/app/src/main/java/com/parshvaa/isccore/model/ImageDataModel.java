package com.parshvaa.isccore.model;

import java.io.File;

public class ImageDataModel {

    String imagePath;
    String ID;
    File file;


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public ImageDataModel(String imagePath, String id, File file) {
        this.imagePath = imagePath;
        this.ID = id;
        this.file = file;
    }



    public String getImagePath() {
        return imagePath;
    }

    public File getFile() {
        return file;
    }
}
