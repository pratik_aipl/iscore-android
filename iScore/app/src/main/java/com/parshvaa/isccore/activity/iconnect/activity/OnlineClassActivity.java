package com.parshvaa.isccore.activity.iconnect.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.webkit.PermissionRequest;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Utils;

public class OnlineClassActivity extends BaseActivity {
    private static final String TAG = "OnlineClassDemoActivity";
    public ImageView img_back;
    public TextView tv_title;
    WebView myWebView;
    public ProgressDialog progressDialog;
    String CLASSLINK;

    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 102;
    private PermissionRequest myRequest;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
       // showProgress (true,true);
        CLASSLINK = getIntent().getStringExtra("CLASSLINK");

        myWebView = findViewById(R.id.webview);
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);


       // myWebView.setWebViewClient(new myWebClient());
        Utils.setWebViewSettings(myWebView);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        myWebView.getSettings().setSaveFormData(true);
        myWebView.getSettings().setSupportZoom(false);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        myWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if( URLUtil.isNetworkUrl(url) ) {
                    return false;
                }
                if (appInstalledOrNot(url)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity( intent );
                } else {
                    Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                    httpIntent.setData(Uri.parse(""+url));
                    startActivity(httpIntent);
                   // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.gotomeeting&hl=en")));

                  /*  Uri uri = Uri.parse(url);
                        view.loadUrl("http://play.google.com/store/apps/" + uri.getHost() + "?" + uri.getQuery());
                        return false;*/
                }
                return true;
            }

        });


/*
        myWebView.setWebChromeClient(new WebChromeClient() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                    myRequest = request;
                for (String permission : request.getResources()) {
                    switch (permission) {
                        case "android.webkit.resource.AUDIO_CAPTURE": {
                            askForPermission(request.getOrigin().toString(), Manifest.permission.RECORD_AUDIO, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
                            break;
                        }
                       // case "android.webkit.resource.VIDEO_CAPTURE":
//                        case "android.webkit.resource.RESOURCE_VIDEO_CAPTURE":
                        case "android.webkit.resource.CAMERA": {
                            askForPermission(request.getOrigin().toString(), Manifest.permission.CAMERA, MY_PERMISSIONS_REQUEST_CAMERA);
                            break;
                        }
                    }
                }
            }
        });

*/

        tv_title.setText("ONLINE CLASS");
        img_back.setOnClickListener(v -> {
          onBackPressed();
        });
        Log.d(TAG, "onCreatelinkk: "+CLASSLINK);

       // Toast.makeText(this, ""+CLASSLINK, Toast.LENGTH_SHORT).show();
        //myWebView.loadUrl("https://api.braincert.com/html5/build/whiteboard.php?token=bRa3YxHOVEYtsfjtcS5vJyhkJutHghdfYxJyHnSJwBcBKHpwnttIC0WgRrXA6UdOTHVWpGdJItLMqXy6jyb6NJpf73GrBvLfDUWF0DN--2FirI0DNW--2Fu--2B7CepjRmbkiO0Om0sSnXQc9WpFF--2FCZQsaGBGziqtcuIpQQdj5cFnmQOc9XrdyBocMjLTzfufHAFqpN6ctQAA1XBXsK4ciaWpkOHqA--3D--3D");
        myWebView.loadUrl(CLASSLINK);
        //myWebView.loadUrl("https://www.google.com/search?q="+CLASSLINK);
        //new CallRequest(OnlineClassActivity.this).generatePaperUrl(PlannerID, PaperID);

    }
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }
   @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  public void askForPermission(String origin, String permission, int requestCode) {
      Log.d("WebView", "inside askForPermission for" + origin + "with" + permission);

      if (ContextCompat.checkSelfPermission(getApplicationContext(),
              permission)
              != PackageManager.PERMISSION_GRANTED) {

          // Should we show an explanation?
          if (ActivityCompat.shouldShowRequestPermissionRationale(OnlineClassActivity.this,
                  permission)) {

              // Show an expanation to the user *asynchronously* -- don't block
              // this thread waiting for the user's response! After the user
              // sees the explanation, try again to request the permission.

          } else {

              // No explanation needed, we can request the permission.

              ActivityCompat.requestPermissions(OnlineClassActivity.this,
                      new String[]{permission},
                      requestCode);
          }
      } else {
          myRequest.grant(myRequest.getResources());
      }
  }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

     /*   if (myWebView.canGoBack()) {
            if (Utils.isNetworkAvailableWebView(OnlineClassActivity.this)) {
                myWebView.goBack();
            } else {
                Utils.showNetworkAlert(OnlineClassActivity.this);
            }
        }*/
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            // TODO Auto-generated method stub

            if (Utils.isNetworkAvailableWebView(OnlineClassActivity.this)) {
                view.loadUrl(url);
            } else {
                Utils.showNetworkAlert(OnlineClassActivity.this);
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Toast.makeText(OnlineClassActivity.this, "Your Internet Connection May not be active Or " + description , Toast.LENGTH_LONG).show();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
           // showProgress(false,true);

               // onBackPressed();
          }
        }

}

