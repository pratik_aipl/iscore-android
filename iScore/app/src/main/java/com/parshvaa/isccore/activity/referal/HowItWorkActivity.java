
package com.parshvaa.isccore.activity.referal;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.RobotoBoldButton;
import com.parshvaa.isccore.utils.RobotoEditTextView;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class HowItWorkActivity extends BaseActivity implements AsynchTaskListner {
    public ImageView img_back;
    public TextView tv_title, tv_rupee, tv_number;
    public HowItWorkActivity instance;
    public Button btn_withdraw;
    RobotoBoldButton btnCancel, btnWithdraw;

    RelativeLayout mWithdraw;
    RobotoEditTextView et_mobile;


    public String withdraw_money = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_it_work);
        instance = this;
        mWithdraw = findViewById(R.id.mWithdraw);
        et_mobile = findViewById(R.id.et_mobile);
        btnCancel = findViewById(R.id.btnCancel);
        btnWithdraw = findViewById(R.id.btnWithdraw);

        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> onBackPressed());
        tv_title = findViewById(R.id.tv_title);
        tv_rupee = findViewById(R.id.tv_rupee);
        btn_withdraw = findViewById(R.id.btn_withdraw);
        tv_number = findViewById(R.id.tv_number);
        tv_title.setText("Referral");
        showProgress(true, true);
        new CallRequest(instance).get_student_wallate_blance();
        btn_withdraw.setOnClickListener(view -> {
            if (!withdraw_money.equals("0")) {
                showNumberDialog();
            } else {
                Utils.showToast("Insufficient balance", instance);
            }
        });
    }

    private void showNumberDialog() {
        btn_withdraw.setVisibility(View.GONE);
        mWithdraw.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(MainUser.getRegMobNo())) {
            et_mobile.setText(MainUser.getRegMobNo());
        }
        btnCancel.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(this);
            mWithdraw.setVisibility(View.GONE);
            btn_withdraw.setVisibility(View.VISIBLE);
            et_mobile.setText("");
        });
        btnWithdraw.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(this);
            String mNumber = et_mobile.getText().toString();
            if (TextUtils.isEmpty(mNumber)) {
                et_mobile.setError("Please Enter Paytm Register No.");
            } else if (!mNumber.matches("^[0-9]{10}$")) {
                et_mobile.setError("Enter appropriate mobile number.");
            } else {
                mWithdraw.setVisibility(View.GONE);
                et_mobile.setText("");
                btn_withdraw.setVisibility(View.VISIBLE);
                showProgress(true, false);
                new CallRequest(instance).withdraw_money(withdraw_money, mNumber);
            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case get_student_wallate_blance:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            tv_rupee.setText(jObj.getString("Balance") + " \u20B9");
                            withdraw_money = jObj.getString("Balance");
                            tv_number.setText(jObj.getString("Number") + " of your referred friends have bought the app.");
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case withdraw_money:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            onBackPressed();
                            Utils.showToast(jObj.getString("message"), this);
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mWithdraw.getVisibility() == View.VISIBLE) {
            mWithdraw.setVisibility(View.GONE);
        } else
            super.onBackPressed();
    }
}
