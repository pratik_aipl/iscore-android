package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/26/2018.
 */

public class TempMcqTestLevel implements Serializable {

    public String StudentMCQTestHDRID = "", LevelID = "";

    public int StudentMCQTestLevelID = -1;

    public int getStudentMCQTestLevelID() {
        return StudentMCQTestLevelID;
    }

    public void setStudentMCQTestLevelID(int studentMCQTestLevelID) {
        StudentMCQTestLevelID = studentMCQTestLevelID;
    }

    public String getStudentMCQTestHDRID() {
        return StudentMCQTestHDRID;
    }

    public void setStudentMCQTestHDRID(String studentMCQTestHDRID) {
        StudentMCQTestHDRID = studentMCQTestHDRID;
    }

    public String getLevelID() {
        return LevelID;
    }

    public void setLevelID(String levelID) {
        LevelID = levelID;
    }
}
