package com.parshvaa.isccore.activity.searchpaper.practisepaper.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/26/2018.
 */

public class PractisePaperModel implements Serializable {

    public String headerID = "";
    public String subjectName = "";
    public String createdOn = "";
    public String testNO = "";
    public String ExamTypeName="";
    public String TotalMarks="";
    public String subjectID="";
    public String PaperTypeID="";
    public String ExamTypeID="";
    public String ExamTypePatternID="";
    public String Duration="";
    public String StudentQuestionPaperID="";

    public String getStudentQuestionPaperID() {
        return StudentQuestionPaperID;
    }

    public void setStudentQuestionPaperID(String studentQuestionPaperID) {
        StudentQuestionPaperID = studentQuestionPaperID;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getExamTypePatternID() {
        return ExamTypePatternID;
    }

    public void setExamTypePatternID(String examTypePatternID) {
        ExamTypePatternID = examTypePatternID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(String examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getPaperTypeID() {
        return PaperTypeID;
    }

    public void setPaperTypeID(String paperTypeID) {
        PaperTypeID = paperTypeID;
    }

    public String getExamTypeName() {
        return ExamTypeName;
    }

    public void setExamTypeName(String examTypeName) {
        ExamTypeName = examTypeName;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String getHeaderID() {
        return headerID;
    }

    public void setHeaderID(String headerID) {
        this.headerID = headerID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTestNO() {
        return testNO;
    }

    public void setTestNO(String testNO) {
        this.testNO = testNO;
    }

}
