package com.parshvaa.isccore.tempmodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Dipesh on 3/10/2017.
 */
public class TempExamTypePatternDetail implements Serializable{

   public String ExamTypePatternDetailID="", ExamTypePatternID="",ExamTypeID="",QuestionTypeID="",QuestionNO="",SubQuestionNO="",QuestionTypeText="",QuestionMarks="",NoOfQuestion="",DisplayOrder="",isQuestion="",isQuestionVisible="",ChapterID="",CreatedBy="",CreatedOn="",ModifiedBy="",ModifiedOn="";



    public TempExamTypePatternDetail(){

    }

    public String getExamTypePatternDetailID() {
        return ExamTypePatternDetailID;
    }

    public void setExamTypePatternDetailID(String examTypePatternDetailID) {
        ExamTypePatternDetailID = examTypePatternDetailID;
    }

    public String getExamTypePatternID() {
        return ExamTypePatternID;
    }

    public void setExamTypePatternID(String examTypePatternID) {
        ExamTypePatternID = examTypePatternID;
    }

    public String getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(String examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getQuestionNO() {
        return QuestionNO;
    }

    public void setQuestionNO(String questionNO) {
        QuestionNO = questionNO;
    }

    public String getSubQuestionNO() {
        return SubQuestionNO;
    }

    public void setSubQuestionNO(String subQuestionNO) {
        SubQuestionNO = subQuestionNO;
    }

    public String getQuestionTypeText() {
        return QuestionTypeText;
    }

    public void setQuestionTypeText(String questionTypeText) {
        QuestionTypeText = questionTypeText;
    }

    public String getQuestionMarks() {
        return QuestionMarks;
    }

    public void setQuestionMarks(String questionMarks) {
        QuestionMarks = questionMarks;
    }

    public String getNoOfQuestion() {
        return NoOfQuestion;
    }

    public void setNoOfQuestion(String noOfQuestion) {
        NoOfQuestion = noOfQuestion;
    }

    public String getDisplayOrder() {
        return DisplayOrder;
    }

    public void setDisplayOrder(String displayOrder) {
        DisplayOrder = displayOrder;
    }

    public String getIsQuestion() {
        return isQuestion;
    }

    public void setIsQuestion(String isQuestion) {
        this.isQuestion = isQuestion;
    }

    public String getIsQuestionVisible() {
        return isQuestionVisible;
    }

    public void setIsQuestionVisible(String isQuestionVisible) {
        this.isQuestionVisible = isQuestionVisible;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public ArrayList<MasterQuestionWithSubQue> tempMasterArray = new ArrayList();
}
