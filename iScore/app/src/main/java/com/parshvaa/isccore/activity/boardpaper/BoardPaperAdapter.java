package com.parshvaa.isccore.activity.boardpaper;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.isccore.R;

import java.util.List;

/**
 * Created by empiere-vaibhav on 1/10/2018.
 */

public class BoardPaperAdapter extends RecyclerView.Adapter<BoardPaperAdapter.MyViewHolder> {

    private List<BoardPaperPreview> paperTypeList;
    public Context context;
    public int row_index = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_date, tv_view_paper,tv_solution;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_date = view.findViewById(R.id.tv_date);
            tv_view_paper = view.findViewById(R.id.tv_view_paper);
            tv_solution = view.findViewById(R.id.tv_solution);

        }
    }


    public BoardPaperAdapter(List<BoardPaperPreview> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;
        this.context = context;
      }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_board_paper_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final BoardPaperPreview movie = paperTypeList.get(position);
        holder.tv_subject_name.setText(movie.getSubjectName());
        holder.tv_date.setText(movie.getBoardPaperName());
        holder.tv_view_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, BoardPaperPreviewActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("QFile", movie.getQFile())
                        .putExtra("PaperName", movie.getPaperName())
                        .putExtra("ViewPaper", "true"));

            }
        });
        holder.tv_solution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, BoardPaperPreviewActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("QFile", movie.getQAFile())
                        .putExtra("PaperName", movie.getAnswerPaperName())
                        .putExtra("ViewPaper", "true"));


            }
        });



    }


    @Override
    public int getItemCount() {
        return paperTypeList.size();
    }
}

