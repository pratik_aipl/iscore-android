package com.parshvaa.isccore.activity.sureshot;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 2/16/2018.
 */

public class SureShot implements Serializable {
    public String QFile="";
    public String QAFile="";
    public String SureShotPaperName="";
    public String SureShotPaperID="";

    public String getSureShotPaperID() {
        return SureShotPaperID;
    }

    public void setSureShotPaperID(String sureShotPaperID) {
        SureShotPaperID = sureShotPaperID;
    }

    public String getQFile() {
        return QFile;
    }

    public void setQFile(String QFile) {
        this.QFile = QFile;
    }

    public String getQAFile() {
        return QAFile;
    }

    public void setQAFile(String QAFile) {
        this.QAFile = QAFile;
    }

    public String getSureShotPaperName() {
        return SureShotPaperName;
    }

    public void setSureShotPaperName(String sureShotPaperName) {
        SureShotPaperName = sureShotPaperName;
    }

    public String getPaperName() {
        return PaperName;
    }

    public void setPaperName(String paperName) {
        PaperName = paperName;
    }

    public String getAnswerPaperName() {
        return AnswerPaperName;
    }

    public void setAnswerPaperName(String answerPaperName) {
        AnswerPaperName = answerPaperName;
    }

    public String PaperName="";
    public String AnswerPaperName="";
}

