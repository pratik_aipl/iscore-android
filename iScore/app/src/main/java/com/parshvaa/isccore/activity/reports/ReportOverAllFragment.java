package com.parshvaa.isccore.activity.reports;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.CctReport;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBConstant;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseFragment;
import com.parshvaa.isccore.utils.CursorParserUniversal;

import java.util.ArrayList;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportOverAllFragment extends BaseFragment {

    public TextView tv_mcq_count, tv_boardpaper_count, tv_cct_count, tv_paractiespaper_count,
            tv_mcq_accuracy, tv_cct_accuracy;
    public View v;
    public MyDBManager mDb;
    public int attempted;
    public int boardPaper;
    public int PractiesPaper;
    public CctReport cctObj;
    public CursorParserUniversal cParse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_overall, container, false);
        cParse = new CursorParserUniversal();
        mDb = MyDBManager.getInstance(getActivity());
        mDb.open(getActivity());

        attempted = mDb.getAllRecords("student_mcq_test_hdr");
        boardPaper = mDb.getAllRecords(DBConstant.board_paper_download);
        PractiesPaper = mDb.getAllRecords("student_question_paper");
        App.cctTotal = 0;
        App.cctTotalAccuracy = 0;
        App.cctArray=new ArrayList<>();
        Cursor c = mDb.getAllRows(DBQueries.getCctReport());
        if (c != null && c.moveToFirst()) {
            try {
                do {
                    cctObj = (CctReport) cParse.parseCursor(c, new CctReport());
                    App.cctTotal = App.cctTotal + Integer.parseInt(cctObj.getTotal());
                    App.cctTotalAccuracy = App.cctTotalAccuracy + Integer.parseInt(cctObj.getAcuuracy());
                    App.cctArray.add(cctObj);
                } while (c.moveToNext());
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (App.cctTotalAccuracy != 0 || App.cctArray.size() != 0)
            App.cctTotalAccuracy = (App.cctTotalAccuracy / App.cctArray.size());

        showProgress(false);
        tv_mcq_accuracy = v.findViewById(R.id.tv_mcq_accuracy);
        tv_cct_accuracy = v.findViewById(R.id.tv_cct_accuracy);
        tv_paractiespaper_count = v.findViewById(R.id.tv_paractiespaper_count);
        tv_cct_count = v.findViewById(R.id.tv_cct_count);
        tv_mcq_count = v.findViewById(R.id.tv_mcq_count);
        tv_boardpaper_count = v.findViewById(R.id.tv_boardpaper_count);
        tv_mcq_count.setText(attempted + "");
        tv_boardpaper_count.setText(boardPaper + "");
        tv_mcq_accuracy.setText(App.mcqTotalAccuracy + "% Accuracy");
        if (App.cctTotalAccuracy < 1)
            tv_cct_accuracy.setText("0% Accuracy");
        else
            tv_cct_accuracy.setText(App.cctTotalAccuracy + "% Accuracy");
        tv_cct_count.setText(App.cctTotal + "");
        tv_paractiespaper_count.setText(PractiesPaper + "");
        App.McqTotal = attempted;
        App.PractisePaperTotal = PractiesPaper;
        return v;
    }

}
