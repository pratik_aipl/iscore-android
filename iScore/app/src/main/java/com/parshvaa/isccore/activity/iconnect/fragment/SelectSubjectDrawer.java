package com.parshvaa.isccore.activity.iconnect.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;

import com.parshvaa.isccore.activity.searchpaper.feftdrawer.LeftSubjectAdapter;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.FragmentDrawer;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */

public class SelectSubjectDrawer extends Fragment {

    private static final String TAG = "SelectSubjectDrawer";
    private RecyclerView recyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private LeftSubjectAdapter adapter;
    private View containerView;
    public Button btn_filter;
    private SubjectArrayListListener subjectListener;
    public JsonParserUniversal jParser;
    public ArrayList<Subject> EvaSubjectArray = new ArrayList<>();

    public SelectSubjectDrawer() {

    }

    public void setDDrawerListener(SubjectArrayListListener listener) {
        this.subjectListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_left_drawer, container, false);
        btn_filter = layout.findViewById(R.id.btn_filter);
        jParser = new JsonParserUniversal();


        recyclerView = layout.findViewById(R.id.recycler_paper);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //drawerListener.onDrawerItemSelected(view, position);
                if (containerView != null)
                    mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }

    private void setupRecyclerView() {
        Context context = recyclerView.getContext();
        int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new LeftSubjectAdapter(EvaSubjectArray, context);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));

        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectListener.onDrawerItemSelected(adapter.checkdItem());
                mDrawerLayout.closeDrawers();
            }
        });
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }


    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {
        public RecyclerTouchListener(Activity activity, RecyclerView recyclerView, ClickListener clickListener) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }

        private GestureDetector gestureDetector;
        private FragmentDrawer.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final FragmentDrawer.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }
    }

    public interface SubjectArrayListListener {
        void onDrawerItemSelected(ArrayList<String> subjectId);
    }

    public void setUpButton(int fragmentId, DrawerLayout drawerLayout, Button drawerButton, final View mainLay, ArrayList<Subject> evaSubjectArray) {
        containerView = Objects.requireNonNull(getActivity()).findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        EvaSubjectArray = evaSubjectArray;

        drawerButton.setOnClickListener(v -> {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT))
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            else
                mDrawerLayout.openDrawer(Gravity.LEFT);
        });

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (mainLay != null)
                    mainLay.setTranslationX(0);
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.post(() -> mDrawerToggle.syncState());
        setupRecyclerView();

    }


}
