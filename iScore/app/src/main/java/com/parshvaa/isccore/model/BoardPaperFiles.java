package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/6/2017.
 */

@JsonObject
public class BoardPaperFiles implements Serializable {

    @JsonField
    public String BoardPaperFileID = "";
    @JsonField
    public String BoardID = "";
    @JsonField
    public String MediumID = "";
    @JsonField
    public String Rootpath="";
    @JsonField
    public String StandardID = "";
    @JsonField
    public String SubjectID = "";
    @JsonField
    public String BoardPaperID = "";
    @JsonField
    public String QFile = "";
    @JsonField
    public String QAFile = "";
    @JsonField
    public String CreatedBy = "";
    @JsonField
    public String CreatedOn = "";
    @JsonField
    public String ModifiedBy = "";
    @JsonField
    public String ModifiedOn = "";

    public String getRootpath() {
        return Rootpath;
    }

    public void setRootpath(String rootpath) {
        Rootpath = rootpath;
    }

    public String getBoardPaperFileID() {
        return BoardPaperFileID;
    }

    public void setBoardPaperFileID(String boardPaperFileID) {
        BoardPaperFileID = boardPaperFileID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getBoardPaperID() {
        return BoardPaperID;
    }

    public void setBoardPaperID(String boardPaperID) {
        BoardPaperID = boardPaperID;
    }

    public String getQFile() {
        return QFile;
    }

    public void setQFile(String QFile) {
        this.QFile = QFile;
    }

    public String getQAFile() {
        return QAFile;
    }

    public void setQAFile(String QAFile) {
        this.QAFile = QAFile;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }




}
