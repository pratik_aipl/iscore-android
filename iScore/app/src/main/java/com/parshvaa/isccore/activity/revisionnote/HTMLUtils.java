package com.parshvaa.isccore.activity.revisionnote;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;


import com.parshvaa.isccore.App;

import com.parshvaa.isccore.utils.MySharedPref;

/**
 * Created by Dipesh on 3/15/2017.
 */
public class HTMLUtils {
    public Context context;
    public static String className;
    public static App app;
    public MySharedPref mySharedPref;
    public HTMLUtils(FragmentActivity activity) {
        context = activity;
        mySharedPref = new MySharedPref(activity);
        app = App.getInstance();
    }
    public  String getPrelimTestHeader(String Date, String SubjectName, String ChapterId) {

        if (mySharedPref.getClassName().isEmpty()) {
            className = "isccore";

        } else {
            className = mySharedPref.getClassName();

        }
        if (!ChapterId.isEmpty()) {
            if (getCount(ChapterId) > 18) {
                ChapterId = ChapterId.substring(0, ChapterId.indexOf(",", 18));
                ChapterId = ChapterId + "...";
            }
        }
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <head>\n" +
                "        <title>Question List</title>\n" +
                "        <style type=\"text/css\">\n" +
                "            table td{ width: auto !important } \n" +
                "            table th{ width: auto !important } \n" +
                "            table{ width: 100% !important }\n" +
                "            .table {\n" +
                "                width: 100% !important \n" +
                "            }\n" +
                "            .table tr td {\n" +
                "                vertical-align: top;\n" +
                "            }\n" +
                "            .question p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +

                "            body * {\n" +
                "                font-size: 12px;\n" +
                "            }\n" +

                "            .answer p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +
                "            body {\n" +
              //  "                width: 100%;\n" +
                "                height: auto;\n" +
                "                margin: 0;\n" +
                "                padding: 0;\n" +
                "                font: 12pt \"Tahoma\";\n" +
                "            }\n" +
                "            * {\n" +
                "                box-sizing: border-box;\n" +
                "                -moz-box-sizing: border-box;\n" +
                "            }\n" +
                "            .page {\n" +
                "                width: 210mm;\n" +
                "                min-height: 297mm;\n" +
                "                padding: 0mm;\n" +
                "                margin: 10mm auto;\n" +
                "                /*border: 1px #D3D3D3 solid;*/\n" +
                "                /*border-radius: 5px;*/\n" +
                "                background: white;\n" +
                "                /*box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);*/\n" +
                "            }\n" +
                "            .subpage {\n" +
                "                padding: 1cm;\n" +
                "                /*border: 5px red solid;*/\n" +
                "                height: 257mm;\n" +
                "                /*outline: 2cm #FFEAEA solid;*/\n" +
                "            }\n" +
                "\n" +
                " .sub p {\n" +
                "\tdisplay:inline;\n" +
                " }" +
                "            @page {\n" +
                "                size: A4;\n" +
                "                margin: 0;\n" +
                "            }\n" +
                "            @media print {\n" +
                "                html, body {\n" +
                "                    width: 210mm;\n" +
                "                    height: 297mm;        \n" +
                "                }\n" +
                "                .page {\n" +
                "                    margin: 0;\n" +
                "                    border: initial;\n" +
                "                    border-radius: initial;\n" +
                "                    width: initial;\n" +
                "                    min-height: initial;\n" +
                "                    /*box-shadow: initial;*/\n" +
                "                    background: initial;\n" +
                "                    /*page-break-after: always;*/\n" +
                "                }\n" +
                "            }\n" +
                "      .tablenew tr td {\n" +

                "                 vertical-align: top; \n" +
                "              } " +
                "      .table tr.mtc td p{\n" +
                "                    margin: 0;\n" +
                "                 display: inline-block; \n" +
                "              } " +
                "        </style>\n" +
                "\n" +
                "<link rel=\"stylesheet\" href=\"file:///android_asset/mathscribe/jqmath-0.4.3.css\">" +
                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jquery-1.4.3.min.js\"></script>\n" +
                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jqmath-etc-0.4.6.min.js\"></script>\n" +
                "    </head>\n" +
                "    <body >\n" +
                "        <div class=\"book\">\n" +
                "            <div class=\"page\">\n" +
                "                <div class=\"subpage\">\n" +
                "                   <table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" class=\"table\" style=\"border: 1px solid black;\">\n" +
                "                        <tr>\n" +
                "                            <td colspan=\"3\" align=\"center\"><h3>" + className + "</h3></td>\n" +
                "                        </tr>\n" +
                "                        <tr>\n" +
                "                            <td width=\"100\"><b></b></td>\n" +
                "                            <td align=\"center\"><b>" + SubjectName + "</b></td>\n" +
                "                            <td align=\"right\" width=\"100\"><b></b></td>\n" +
                "                        </tr>\n" +
                "                        <tr>\n" +
                "                            <td width=\"170\"><b>Revision Note</b></td>\n" +
                "                            <td align=\"center\"><b>Chapter: " + ChapterId + "</b></td>\n" +
                "                            <td align=\"right\" width=\"150\"><b>Date : " + Date + " </b></td>\n" +
                "                        </tr>\n" +
                "                    </table>\n" +
                "                    <br/>\n" +
                "                    \n" +
                "                    \n" +
                "                        <table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" class=\"table\" style='width:100%;'>";
    }
    public static int getCount(String str) {
        try {
            int lastIndex = 0;
            int count = 0;
            while (lastIndex != -1) {

                lastIndex = str.indexOf(",", lastIndex);

                if (lastIndex != -1) {
                    count++;
                    lastIndex += ",".length();
                }
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    public static String getRevisionNoteShuffleScript(){
        return "<script src=\"file:///android_asset/jquery-1.9.1.min.js\" type=\"text/javascript\"></script>" +
                "<script type=\"text/javascript\">\n" +
                "            function shuffleArray(array) {\n" +
                "                var counter = array.length, temp, index;\n" +
                "                // While there are elements in the array\n" +
                "                while (counter > 0) {\n" +
                "                    // Pick a random index\n" +
                "                    index = Math.floor(Math.random() * counter);\n" +
                "\n" +
                "                    // Decrease counter by 1\n" +
                "                    counter--;\n" +
                "\n" +
                "                    // And swap the last element with it\n" +
                "                    temp = array[ counter ];\n" +
                "                    array[ counter ] = array[ index ];\n" +
                "                    array[ index ] = temp;\n" +
                "                }\n" +
                "                return array;\n" +
                "            }";
    }
}
