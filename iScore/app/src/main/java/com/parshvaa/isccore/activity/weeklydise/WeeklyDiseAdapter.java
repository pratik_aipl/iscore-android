package com.parshvaa.isccore.activity.weeklydise;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.isccore.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by empiere-vaibhav on 3/23/2018.
 */

public class WeeklyDiseAdapter extends RecyclerView.Adapter<WeeklyDiseAdapter.MyViewHolder> {

    private ArrayList<Date> startDate;
    private List<Date> endDate;
    public Context context;
    public int SubjectID;
    public String SubjectName;

    public WeeklyDiseAdapter(ArrayList<Date> startDate, List<Date> endDate, Context context) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_month, tv_expdate, tv_take_test, tv_date;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_month = view.findViewById(R.id.tv_month);
            tv_expdate = view.findViewById(R.id.tv_expdate);
            tv_date = view.findViewById(R.id.tv_date);
            tv_take_test = view.findViewById(R.id.tv_take_test);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_weekly_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SimpleDateFormat fmt = new SimpleDateFormat("dd MMM");

        holder.tv_subject_name.setText("WEEKLY REPORT" + " " + (position + 1));
        holder.tv_expdate.setText(fmt.format(startDate.get(position)) + "-" + fmt.format(endDate.get(position)));
        final SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(position==0){
                    context.startActivity(new Intent(context, WeeklyReportPreviewActivity.class)
                            //  .putExtra("suObj", mcqdata)
                            .putExtra("end_date",fmt2.format(endDate.get(position)))
                            .putExtra("start_date",fmt2.format(startDate.get(position)))
                            .putExtra("old_end_date","0000-00-00")
                            .putExtra("old_start_date","0000-00-00")
                            );
                }else{
                context.startActivity(new Intent(context, WeeklyReportPreviewActivity.class)
                        //  .putExtra("suObj", mcqdata)
                        .putExtra("end_date",fmt2.format(endDate.get(position)))
                        .putExtra("start_date",fmt2.format(startDate.get(position)))
                        .putExtra("old_end_date",fmt2.format(endDate.get(position-1)))
                        .putExtra("old_start_date",fmt2.format(startDate.get(position-1))
                        ) );}
            }
        });

    }


    @Override
    public int getItemCount() {
        return startDate.size();
    }

}



