package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Dipesh on 3/25/2017.
 */
@JsonObject
public class StudentSetPaperQuestionType implements Serializable {
    @JsonField
    public int StudentSetPaperQuestionTypeID = -1;
    @JsonField
    public String StudentQuestionPaperID = "", QuestionTypeID = "", TotalAsk = "", ToAnswer = "", TotalMark = "";

    public int getStudentSetPaperQuestionTypeID() {
        return StudentSetPaperQuestionTypeID;
    }

    public void setStudentSetPaperQuestionTypeID(int studentSetPaperQuestionTypeID) {
        StudentSetPaperQuestionTypeID = studentSetPaperQuestionTypeID;
    }

    public String getStudentQuestionPaperID() {
        return StudentQuestionPaperID;
    }

    public void setStudentQuestionPaperID(String studentQuestionPaperID) {
        StudentQuestionPaperID = studentQuestionPaperID;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getTotalAsk() {
        return TotalAsk;
    }

    public void setTotalAsk(String totalAsk) {
        TotalAsk = totalAsk;
    }

    public String getToAnswer() {
        return ToAnswer;
    }

    public void setToAnswer(String toAnswer) {
        ToAnswer = toAnswer;
    }

    public String getTotalMark() {
        return TotalMark;
    }

    public void setTotalMark(String totalMark) {
        TotalMark = totalMark;
    }

    @JsonField(name = "setdetailArray")
    public ArrayList<StudentSetPaperDetail> setdetailArray = new ArrayList<>();

    public ArrayList<StudentSetPaperDetail> getSetdetailArray() {
        return setdetailArray;
    }

    public void setSetdetailArray(ArrayList<StudentSetPaperDetail> setdetailArray) {
        this.setdetailArray = setdetailArray;
    }
}
