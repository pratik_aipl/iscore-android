package com.parshvaa.isccore.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;

import com.google.android.material.snackbar.Snackbar;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.crashlytics.android.Crashlytics;
import com.onesignal.OneSignal;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.MainUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;


/**
 * Created by admin on 10/12/2016.
 */
public class Utils {
    private static final String TAG = "Utils";
    /* for GCM */
    static final String DISPLAY_MESSAGE_ACTION = "com.maxinternational.max.DISPLAY_MESSAGE";
    static final String EXTRA_MESSAGE = "message";
    public static SimpleDateFormat Input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat outputAMPM = new SimpleDateFormat("dd MMM yyyy | hh:mm aa    ");
    public static SimpleDateFormat InputDate = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat output = new SimpleDateFormat("dd-MMM-yyyy");
    public static SimpleDateFormat outputDateFormate = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
    public static SimpleDateFormat outputDateFormatePMAM = new SimpleDateFormat("MMM dd, yyyy hh:mm aa");

    public static SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyyMMdd");
    public static Dialog dialog;
    static float density = 1;
    private static ProgressDialog mProgressDialog;
    static String Pleyaer = "";

    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return Settings.System.getInt(c.getContentResolver(), Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    public static boolean specialPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }

        return true;
    }

    public static String OneSignalPlearID() {
        Log.d(TAG, "OneSignalPlearID: " + Pleyaer);
        OneSignal.idsAvailable((userId, registrationId) -> {
            if (userId != null)
                Pleyaer = userId;
            Log.d("debug", "User:" + userId);

        });
        return Pleyaer;
    }

    public static String getHttp() {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            return Constant.HTTP;
        } else {
            return Constant.HTTPS;
            // do something for phones running an SDK before lollipop
        }
    }

    public static void Log(String tag, String msg) {
        Log.i(tag, msg);

    }

    public static void SystemOutPrintln(String tag, String msg) {
        System.out.println(tag + msg);

    }

    public static long strToMilli(String strTime) {
        long retVal = 0;
        String hour = strTime.substring(0, 2);
        String min = strTime.substring(3, 5);
        int h = Integer.parseInt(hour);
        int m = Integer.parseInt(min);
        int s = Integer.parseInt("00");

        String strDebug = String.format("%02d:%02d:%02d", h, m, s);
        if (rx.android.BuildConfig.DEBUG)
            Log.d(TAG, "strToMilli: " + strDebug);
        long lH = h * 60 * 60 * 1000;
        long lM = m * 60 * 1000;
        long lS = s * 1000;

        retVal = lH + lM + lS;
        return retVal;
    }

    public static String getDurationBreakdown(long diff) {
        long millis = diff;
        if (millis < 0) {
            return "00:00:00";
        }
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        return String.format(Locale.ENGLISH, "%02d:%02d:%02d", hours, minutes, seconds);
        //return "${getWithLeadZero(hours)}:${getWithLeadZero(minutes)}:${getWithLeadZero(seconds)}"
    }

    public static void showTwoButtonDialog(Context context, String title, String message, String yesButtonName, String noButtonName, DialogButtonListener dialogButtonListener) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        if (title != null)
            alertDialogBuilder.setTitle(title);
        else {
            alertDialogBuilder.setTitle("");
        }
        alertDialogBuilder
                .setMessage(Html.fromHtml(message))
                .setCancelable(false);
        if (yesButtonName != null) {
            yesButtonName = yesButtonName.equals("") ? "YES" : yesButtonName;
            alertDialogBuilder.setPositiveButton(yesButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onPositiveButtonClicked();
                }
            });
        }

        if (noButtonName != null) {
            noButtonName = noButtonName.equals("") ? "NO" : noButtonName;
            alertDialogBuilder.setNegativeButton(noButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onNegativButtonClicked();
                }
            });
        }
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }


    public static String replaceImagesPath_new(String data) {
        File image = new File(Constant.LOCAL_IMAGE_PATH);
        if (data.contains("<img src='")) {
            data = data.replaceAll("<img src='", "<img src='file://" + image.getAbsolutePath());
            //    data=data.replaceAll("%20"," ");
        } else {
            data = data.replaceAll("<img src=\"", "<img src=\"file://" + image.getAbsolutePath());
        }

        return data;
    }

    public static void showNetworkAlert(Context ct) {
        try {
            final AlertDialog alertDialog = new AlertDialog.Builder(ct).create();

            alertDialog.setTitle("Info");
            alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });

            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isNetworkAvailableWebView(Context activity) {

        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
            return true;
        } else {
            // showToast("Please connect to Internet", activity);
            return false;
        }

    }


    public static String replaceImagesPath(String data) {
        //http://staff.parshvaa.com/Uploads/10/MARATHI/Geometry/Geom_x_mm_1.png#-105#1
        File image = new File(Constant.LOCAL_IMAGE_PATH);
        Log.d(TAG, "replaceImagesPath: " + data);
        if (data.contains("src=\"http://staff.iccc.co.in/Uploads/")) {
            data = data.replaceAll("src=\"http://staff.iccc.co.in/Uploads/", "src=\"file://" + image.getAbsolutePath() + "/Uploads/");
            //    data=data.replaceAll("%20"," ");

        } else if (data.contains("src=\"https://staff.iccc.co.in/Uploads/")) {
            data = data.replaceAll("src=\"https://staff.iccc.co.in/Uploads/", "src=\"file://" + image.getAbsolutePath() + "/Uploads/");
            //    data=data.replaceAll("%20"," ");
            Log.d(TAG, "replaceImagesPath new >> "+data.toString());
        } else if (data.contains("src=\"/Uploads/")) {
            data = data.replaceAll("src=\"/Uploads/", "src=\"file://" + image.getAbsolutePath() + "/Uploads/");
        }
        return data;

    }

    public static String replaceImagesPathtemp(String data, Fetch fetch) {
        //http://staff.iccc.co.in/Uploads/10/MARATHI/Geometry/Geom_x_mm_1.png#-105#1
        File image = new File(Constant.LOCAL_IMAGE_PATH);
        Log.d(TAG, "replaceImagesPath: " + data);
        if (data.contains("src=\"http://staff.iccc.co.in/Uploads/")) {
            data = data.replaceAll("src=\"http://staff.iccc.co.in/Uploads/", "src=\"file://" + image.getAbsolutePath() + "/Uploads/");
            //    data=data.replaceAll("%20"," ");
            downloadFile(data, "src=\"file://" + image.getAbsolutePath() + "/Uploads/", fetch);

        } else if (data.contains("src=\"https://staff.iccc.co.in/Uploads/")) {
            data = data.replaceAll("src=\"https://staff.iccc.co.in/Uploads/", "src=\"file://" + image.getAbsolutePath() + "/Uploads/");
            //    data=data.replaceAll("%20"," ");
            downloadFile(data, "src=\"file://" + image.getAbsolutePath() + "/Uploads/", fetch);

        } else if (data.contains("src=\"/Uploads/")) {
            data = data.replaceAll("src=\"/Uploads/", "src=\"file://" + image.getAbsolutePath() + "/Uploads/");
            downloadFile(data, "src=\"file://" + image.getAbsolutePath() + "/Uploads/", fetch);

        } else {
            //data = data.replaceAll("<img alt=\"\" src=\"", "<img src=\"file://" + image.getAbsolutePath().toString());
        }

        return data;


    }

    private static void downloadFile(String url, String path, Fetch fetch) {
        Log.d(TAG, "downloadFile() called with: url = [" + url + "], path = [" + path + "]");
        File qLocalFile = new File(path);
        Request request = null;
        if (!qLocalFile.exists()) {
            request = new Request(url, path);
            request.setPriority(Priority.HIGH);
            request.setNetworkType(NetworkType.ALL);
            fetch.enqueue(request, updatedRequest -> {
                Log.d(TAG, "manageImagesDownload: ");
            }, error -> {
                // Toast.makeText(this, "error enqueue download!!!", Toast.LENGTH_SHORT).show();
            });

        }

    }

    public static String changeDateToMMDDYYYY(String input) {
        String outputDateStr = "";
        try {
            Date date = InputDate.parse(input);
            outputDateStr = output.format(date);
            ////Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }

    public static String changeDateAndTimeFormet(String input) {
        String outputDateStr = "";
        try {
            Date date = Input.parse(input);
            outputDateStr = outputDateFormatePMAM.format(date);
            ////Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }

    public static String getWeeklyDate(String input) {
        String outputDateStr = "";
        Date myDate, newDate = null;
        try {
            myDate = Input.parse(input);
            newDate = new Date(myDate.getTime() + 604800000L);
            outputDateStr = Input.format(newDate);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }

    @SuppressLint("WrongConstant")
    public static void getWeekCount(String input) {

       /* try {
            Date myDate = Input.parse(input);
            Calendar cal = Calendar.getInstance();
            cal.setTime(myDate);
            cal.add(Calendar.DAY_OF_WEEK, 1);
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

            if (dayOfWeek == cal.SATURDAY)
            Input.format(cal.getTime());

            //Log.i("TAg", "SATURDAY :->. " + Input.format(cal.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        Date myDate = null, CurantDate = null;
        try {
            myDate = Input.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Date> disable = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int month = cal.get(Calendar.MONTH);
        do {
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == Calendar.SATURDAY) {
                myDate = cal.getTime();
                disable.add(cal.getTime());

            }
            cal.add(Calendar.DAY_OF_MONTH, 1);
        } while (myDate.getTime() < new Date().getTime());
        SimpleDateFormat fmt = new SimpleDateFormat("EEE M/d/yyyy");
        for (Date date : disable)
            System.out.println(fmt.format(date));
    }


    public static String changeDateAndTimeAMPMFormet(String input) {
        String outputDateStr = "";
        try {
            Date date = Input.parse(input);
            outputDateStr = outputAMPM.format(date);
            ////Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }

    public static String DateFormetChangeUniversal(DateFormat inputFormat, DateFormat outputFormat, String input) {
        String outputDateStr;

        try {

            Date date = inputFormat.parse(input);

            outputDateStr = outputFormat.format(date);
            //Log.i("output", outputDateStr);
            return outputDateStr;


        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String changeDateToDDMMYYYY(String input) {
        //   21-05-1916
        //   1916-05-21//
      /*  Date date = new Date(input);
        SimpleDateFormat formatter5 = new SimpleDateFormat("yyyy-mm-dd");
        return formatter5.format(date);*/
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyy");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            // String input="2013-06-24";
            Date date = inputFormat.parse(input);

            outputDateStr = outputFormat.format(date);
            //Log.i("output", outputDateStr);
            return outputDateStr;


        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String convertDate(String input) {
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            return outputDateStr;

        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String getIMEInumber(Context ct) {
        try {
            if (checkPhoneStatePermission(ct)) {
                TelephonyManager mngr = (TelephonyManager) ct.getSystemService(Context.TELEPHONY_SERVICE);
                return mngr.getDeviceId();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    public static long milliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    public static String getGUID() {
        return UUID.randomUUID().toString();
    }


    public static boolean checkOutDated(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date strDate = sdf.parse(Constant.expiryDate);
            return System.currentTimeMillis() <= strDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
    }


    public static boolean checkMoreThanMonth(String date) {
        try {
            //Log.i("Date Check :", date);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date strDate = sdf.parse(date);
            Calendar original = toCalendar(strDate);

            Date d = new Date();
            Calendar calMonth = original;
            calMonth.set(Calendar.MONTH, +1);

            //Log.i("Date Check : date +-> ",calMonth.get(Calendar.YEAR) + "-" + calMonth.get(Calendar.MONTH) + "-" +calMonth.get(Calendar.DAY_OF_MONTH) + " " + calMonth.get(Calendar.HOUR) + "-" +calMonth.get(Calendar.MINUTE) + "-" + calMonth.get(Calendar.SECOND));

            //Log.i("Date Check : CALMONTH : ", calMonth.getTimeInMillis() + "");
            //Log.i("Date Check : STRDATE  : ", strDate.getTime() + "");

            //Log.i("Date Check : Diff : ", (calMonth.getTimeInMillis() - strDate.getTime()) + "");
            //Log.i("Date Check : Diff should: ", 30 * 24 * 60 * 60 * 1000 + "");
            //Log.i("Date Check : true", date);
//Log.i("Date Check : false", date);
/*
if (calMonth.getTimeInMillis() > System.currentTimeMillis()) {
    //Log.i("Date Check : true"  ,date);
    return true;
} else {
    //Log.i("Date Check : false"  ,date);
    return false;
}*/
            return (calMonth.getTimeInMillis() - strDate.getTime()) >= 30 * 24 * 60 * 60 * 1000;

        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        return display.getWidth();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText())
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

//    public static void setupUI(View view, final Context ct) {
//
//        //Set up touch listener for non-text box views to hide keyboard.
//        if (!(view instanceof EditText)) {
//
//            view.setOnTouchListener((v, event) -> {
//                hideSoftKeyboard((Activity) ct);
//                return false;
//            });
//        }
//
//        //If a layout container, iterate over children and seed recursion.
//        if (view instanceof ViewGroup) {
//
//            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
//
//                View innerView = ((ViewGroup) view).getChildAt(i);
//
//                setupUI(innerView, ct);
//            }
//        }
//    }


    public static boolean checkStoragePermission(Context ct) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ct, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions((Activity) ct, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,},
                        Constant.EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            //Log.i("iScore", "Permission granted");
            return true;
        }
    }


    public static boolean checkPhoneStatePermission(Context ct) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ct, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Log.i("iScore", "Permission granted");
                return true;
            } else {

                //Log.i("iScore", "Permission revoked");
                ActivityCompat.requestPermissions((Activity) ct, new String[]{Manifest.permission.READ_PHONE_STATE},
                        Constant.EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            //Log.i("iScore", "Permission granted");
            return true;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isEmpty(EditText et) {
        return TextUtils.isEmpty(et.getText());
    }

    public static void showProgressDialog(Context context, String msg) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.cancel();
            }
            if (dialog == null) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_progress_dialog);
                TextView txtView = dialog.findViewById(R.id.txt_di);
                txtView.setText(msg);
            }


            if (dialog != null && !dialog.isShowing()) {
//                Runnable r = new Runnable() {
//                    @Override
//                    public void run() {
                dialog.show();
//                    }
//                };
//                r.run();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void showProgressDialog(Context context) {
//        if (!((Activity) context).isFinishing()) {
//            //show dialog
//
//            showProgressDialog(context, "Please Wait..");
//        }
//    }

//    public static void hideProgressDialog() {
//        try {
//
//            if (dialog != null) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                    dialog = null;
//                }
//            }
//        } catch (IllegalArgumentException ie) {
//            ie.printStackTrace();
//
//        } catch (RuntimeException re) {
//            re.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    public static void addValueToEditor(SharedPreferences.Editor et, Object o) {
        Class c;
        try {
            c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());

            Field[] fields = c.getFields();
            for (Field f : fields) {
                f.setAccessible(true);
                if (f.getType() == String.class) {
                    et.putString(f.getName(), f.get(o).toString());

                } else if (f.getType() == Integer.class) {
                    et.putInt(f.getName(), Integer.parseInt(f.get(o).toString()));

                } else if (f.getType() == Boolean.class) {
                    et.putBoolean(f.getName(), Boolean.valueOf(f.get(o).toString()));
                }
            }

            et.commit();


        } catch (Exception e) {
            e.printStackTrace();


        }

    }

    public static String consvertNumberToString(String number) {
        String numberz = number;
        String returnz = "";
        try {
            final long Number = Long.parseLong(numberz);
            returnz = Words.convert(Number);
        } catch (NumberFormatException e) {
            //Toast.makeToast("illegal number or empty number" , toast.long)
        }
        return returnz;
    }

    public static void showCancelableSpinProgressDialog(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    public static void removeSimpleSpinProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.cancel();
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public static float getDisplayMetricsDensity(Context context) {
        density = context.getResources().getDisplayMetrics().density;

        return density;
    }

    public static int getPixel(Context context, int p) {
        if (density != 1) {
            return (int) (p * density + 0.5);
        }
        return p;
    }

    public static Animation FadeAnimation(float nFromFade, float nToFade) {
        Animation fadeAnimation = new AlphaAnimation(nToFade, nToFade);

        return fadeAnimation;
    }

    public static String getDeviceId(Context context) {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String uuid = tManager.getDeviceId();
        return uuid;
    }

    public static Animation inFromRightAnimation() {
        Animation inFromRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromRight;
    }

    public static Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromLeft;
    }

    public static Animation inFromBottomAnimation() {
        Animation inFromBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, +1.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromBottom;
    }

    public static Animation outToLeftAnimation() {
        Animation outToLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                -1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);
        return outToLeft;
    }

    public static Animation outToRightAnimation() {
        Animation outToRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);

        return outToRight;
    }

    public static Animation outToBottomAnimation() {
        Animation outToBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f);

        return outToBottom;
    }

    public static String create_imgage_view(String imageURL, int width, int height) {
        StringBuffer html = new StringBuffer(
                "<html lang='en'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>");
        html.append("<body style='width:" + width + "; height:" + height + "; padding:0; margin:0;'>");

        html.append("<div style='width:" + width + "; height:" + height + "; overflow:hidden;'>");
        html.append("<img src='" + imageURL + "' style='width:" + width + ";' border=0 />");
        html.append("</div>");

        html.append("</body>");
        html.append("</html>");

        return html.toString();
    }

    public static boolean checkWIFIConnected(Context activity) {
        ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi.isConnected();
    }

    public static boolean isNetworkAvailable(Context activity) {
        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void showToast(String msg, Context ctx) {
        Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
        /* toast.setGravity(Gravity.CENTER, 0, 0);*/
        toast.show();

    }

    public static void showToastinCenter(String msg, Context ctx) {
        Toast toast = Toast.makeText(ctx,
                msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public static void showSnakBar(String msg, final View view, Context ctx) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(ctx, R.color.textColor));

        /*.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar1 = Snackbar.make(ctx, "Message is restored!", Snackbar.LENGTH_SHORT);
                        snackbar1.show();
                    }
                });*/

        snackbar.show();
    }

    public static void showAlert(String msg, Context ctx) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ctx);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static void loadImageWithPicasso(Context context, String imagePath, ImageView iv, ProgressBar mProgress) {
        Log.d(TAG, "loadImageWithPicasso: " + imagePath);
        if (!imagePath.isEmpty()) {
            if (mProgress != null)
                mProgress.setVisibility(View.VISIBLE);
            ///Change by Brainvire
            Picasso.with(context).load(imagePath)
                    .placeholder(R.drawable.image_not_available)
                    .error(R.drawable.image_not_available)
                    .into(iv, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (mProgress != null)
                                mProgress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if (mProgress != null)
                                mProgress.setVisibility(View.GONE);
                        }
                    });

//            PicassoTrustAll.getInstance(context)
//                    .load(imagePath)
//                    .placeholder(R.drawable.image_not_available)
//                    .error(R.drawable.image_not_available).into(iv, new Callback() {
//                @Override
//                public void onSuccess() {
//                    if (mProgress != null)
//                        mProgress.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onError() {
//                    if (mProgress != null)
//                        mProgress.setVisibility(View.GONE);
//                }
//
//            });

        } else {
            Picasso.with(context).load(R.drawable.image_not_available).into(iv);
        }
    }

    public static void onCaptureImageResult(Context context, String imagePath, ImageView imageView) {
        if (!imagePath.contains(".webp")) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);

            ExifInterface ei = null;
            try {
                ei = new ExifInterface(imagePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(myBitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(myBitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(myBitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = myBitmap;
            }
            imageView.setImageBitmap(rotatedBitmap);

        } else {
            // byte[] webpEncodedData = loadFileAsByteArray(imagePath);


            // Bitmap bitmap = webpToBitmap(webpEncodedData);
            //  imageView.setImageBitmap(bitmap);
            Utils.loadImageWithPicasso(context, imagePath, imageView, null);
        }

    }

    public static void showSimpleSpinProgressDialog(Context context, String message) {

        showProgressDialog(context, message);
/*
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(message);
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.show();
        } else {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }*/
    }
 /*   public static void hideProgressDialog() {
        try {

            *//*if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }*//*

            if (dialog1 != null) {
                if (dialog1.isShowing()) {
                    dialog1.dismiss();
                    dialog1 = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    // public static String convert24HoursTo12HoursFormat(String twelveHourTime)
    // throws ParseException {
    // return outputformatter.format(inputformatter.parse(twelveHourTime));
    // }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * You can integrate interstitials in any placement in your app, but for
     * testing purposes we present integration tied to a button click
     */
    public static Typeface setNormalFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans.ttf");
        return font;
    }

    public static Typeface setBoldFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans-Bold.ttf");
        return font;
    }

    public static View setTabCaption() {

        return null;
    }

    public static String manageDecimal(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        if (temp.contains(".")) {
            int in = temp.indexOf(".") + 1;
            before = temp.substring(0, in);
            after = temp.substring(in);
            if (after.length() == 1) {
                displayRiskDecimal = before + after + "0";
            } else {
                displayRiskDecimal = before + after;
            }
            return displayRiskDecimal;
        } else {
            displayRiskDecimal = temp + ".00";
            return displayRiskDecimal;
        }
    }

    public static String manageDecimalPointForGetLines(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        int in = temp.indexOf(".") + 1;
        before = temp.substring(0, in);
        after = temp.substring(in);
        if (after.equals("00")) {
            displayRiskDecimal = before.substring(0, before.length() - 1);
        } else if (after.length() == 2) {
            if (after.startsWith("0", 1)) {
                displayRiskDecimal = after.substring(0, after.length() - 1);
                displayRiskDecimal = before + displayRiskDecimal;
            }
        }
        return displayRiskDecimal;
    }

    public static void hideKeyboard(Activity context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String removeLastComma(String s) {
        if (s.length() > 0) {
            s = s.substring(0, s.length() - 1);
            return s;
        } else {
            return s;
        }
    }


    public static String getAccuraccy(int totalAns, int rightAns) {
        if (rightAns != 0 && totalAns != 0)
            return String.valueOf(rightAns * 100 / totalAns);
        else
            return "0";
    }


    public static float getProgress(int totalAns, int rightAns) {
        if (rightAns != 0 && totalAns != 0)
            return (rightAns * 100 / totalAns);
        else
            return 1;

    }

    public static String convertArraytoCommaSeprated(ArrayList<String> array) {
        return TextUtils.join(",", array);
    }


    public static String getDataDir(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.dataDir;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    static int count = 0;

    private static void getFile(String dirPath) {
        File f = new File(dirPath);
        File[] files = f.listFiles();

        if (files != null)
            for (int i = 0; i < files.length; i++) {
                count++;
                File file = files[i];

                if (file.isDirectory()) {
                    getFile(file.getAbsolutePath());
                }
            }
    }

    public static int currentAppVersionCode(Activity act) {
        return BuildConfig.VERSION_CODE;
    }

    public static void logUser() {
        if (!BuildConfig.DEBUG) {
            Crashlytics.setUserIdentifier("ISccore");
            Crashlytics.setUserEmail("parshvaaedumentor@gmail.com");
            Crashlytics.setUserName("ISccore");
        }
    }

    public static void setWebViewSettings(WebView webview) {
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setAllowContentAccess(true);
        webview.getSettings().setAllowFileAccessFromFileURLs(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setDomStorageEnabled(true);

        webview.getSettings().setDatabaseEnabled(true);
        webview.getSettings().setAppCacheEnabled(true);
        webview.getSettings().setAppCacheMaxSize(1);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

//        webview.clearCache(true);
//        webview.clearHistory();
    }

    public static void deleteAllTableRecord(MyDBManager mDb) {
        mDb.emptyTable("boards");
        mDb.emptyTable("boards_paper");
        mDb.emptyTable("boards_paper_data");
        mDb.emptyTable("board_paper_files");
        mDb.emptyTable("chapters");
        mDb.emptyTable("examtypes");
        mDb.emptyTable("examtype_subject");
        mDb.emptyTable("masterquestion");
        mDb.emptyTable("mcqoption");
        mDb.emptyTable("mcqquestion");
        mDb.emptyTable("mediums");
        mDb.emptyTable("question_types");
        mDb.emptyTable("standards");
        mDb.emptyTable("subjects");
        mDb.emptyTable("temp_mcq_test_dtl");
        mDb.emptyTable("temp_mcq_test_level");
        mDb.emptyTable("temp_mcq_test_hdr");
        mDb.emptyTable("temp_mcq_test_chapter");
        mDb.emptyTable("student_mcq_test_hdr");
        mDb.emptyTable("student_mcq_test_dtl");
        mDb.emptyTable("temp_mcq_test_hdr");
        mDb.emptyTable("student_mcq_test_chapter");
        mDb.emptyTable("paper_type");
        mDb.emptyTable("exam_type_pattern");
        mDb.emptyTable("exam_type_pattern_detail");
        mDb.emptyTable("student_question_paper");
        mDb.emptyTable("student_question_paper_detail");
        mDb.emptyTable("student_question_paper_chapter");
        mDb.emptyTable("board_paper_download");
        mDb.emptyTable("student_set_paper_question_type");
        mDb.emptyTable("student_set_paper_detail");
        mDb.emptyTable("notification");
        mDb.emptyTable("class_evaluater_mcq_test_temp");
        mDb.emptyTable("class_evaluater_cct_count");
        mDb.emptyTable("updated_masterquetion");
        mDb.emptyTable("student_mcq_test_level");
        mDb.emptyTable("student_incorrect_question");
        mDb.emptyTable("student_not_appeared_question");
        mDb.emptyTable("zooki");
        EventBus.getDefault().post(new TableReload(true));
    }

    public static void removeTableRecord(MyDBManager mDb) {
        if (mDb.isDbOpen()) {
            mDb.removeTable("boards");
            mDb.removeTable("boards_paper");
            mDb.removeTable("boards_paper_data");
            mDb.removeTable("board_paper_files");
            mDb.removeTable("chapters");
            mDb.removeTable("examtypes");
            mDb.removeTable("examtype_subject");
            mDb.removeTable("masterquestion");
            mDb.removeTable("mcqoption");
            mDb.removeTable("mcqquestion");
            mDb.removeTable("mediums");
            mDb.removeTable("question_types");
            mDb.removeTable("standards");
            mDb.removeTable("subjects");
            mDb.removeTable("temp_mcq_test_dtl");
            mDb.removeTable("temp_mcq_test_level");
            mDb.removeTable("temp_mcq_test_hdr");
            mDb.removeTable("temp_mcq_test_chapter");
            mDb.removeTable("student_mcq_test_hdr");
            mDb.removeTable("student_mcq_test_dtl");
            mDb.removeTable("temp_mcq_test_hdr");
            mDb.removeTable("student_mcq_test_chapter");
            mDb.removeTable("paper_type");
            mDb.removeTable("exam_type_pattern");
            mDb.removeTable("exam_type_pattern_detail");
            mDb.removeTable("student_question_paper");
            mDb.removeTable("student_question_paper_detail");
            mDb.removeTable("student_question_paper_chapter");
            mDb.removeTable("board_paper_download");
            mDb.removeTable("student_set_paper_question_type");
            mDb.removeTable("student_set_paper_detail");
            mDb.removeTable("notification");
            mDb.removeTable("class_evaluater_mcq_test_temp");
            mDb.removeTable("class_evaluater_cct_count");
            mDb.removeTable("updated_masterquetion");
            mDb.removeTable("student_mcq_test_level");
            mDb.removeTable("student_incorrect_question");
            mDb.removeTable("student_not_appeared_question");
            mDb.removeTable("zooki");
//            mDb.close();
        }
    }


    public static MainUser DataSherdToAppClass(App app, MySharedPref mySharedPref) {
        App.mainUser = new MainUser();
        MainUser.setFirstName(mySharedPref.getFirstName());
        MainUser.setLastName(mySharedPref.getLastName());
        MainUser.setRegKey(mySharedPref.getReg_key());
        MainUser.setGuid(mySharedPref.getGuuid());
        MainUser.setClassID(mySharedPref.getClassID());
        MainUser.setBoardID(mySharedPref.getBoardID());
        MainUser.setMediumID(mySharedPref.getMediumID());
        MainUser.setStandardID(mySharedPref.getStandardID());
        MainUser.setEmailID(mySharedPref.getEmailID());
        MainUser.setRegMobNo(mySharedPref.getRegMobNo());
        MainUser.setSubjects(mySharedPref.getSubjects());
        MainUser.setStudentId(mySharedPref.getStudentID());
        MainUser.setStanderdName(mySharedPref.getStandardName());
        MainUser.setClassName(mySharedPref.getClassName());
        MainUser.setKeyStatus(mySharedPref.getKeyStatus());
        MainUser.setBranchID(mySharedPref.getBranchID());
        MainUser.setBatchID(mySharedPref.getBatchID());
        MainUser.setSchoolName(mySharedPref.getSchoolName());
        MainUser.setVersion(mySharedPref.getVersion());
        MainUser.setBoardName(mySharedPref.getBoardName());
        MainUser.setMediumName(mySharedPref.getMediumName());
        MainUser.setStudentCode(mySharedPref.getStudentCode());
        return App.mainUser;
    }

    public static void setTransaction(MyDBManager mDb) {
//        if (mDb != null && mDb.isDbOpen())
//            mDb.myDb.setTransactionSuccessful();
//        if (mDb != null && mDb.isDbOpen())
//            mDb.myDb.endTransaction();
    }

    public static void closeDb(MyDBManager mDb) {
        if (mDb.isDbOpen()) {
            if (mDb.myDb.inTransaction()) {
                mDb.getWritableDatabase().endTransaction();
            }
            mDb.getWritableDatabase().close();
            mDb.close();
        }

    }

    public static boolean checkDate(String answerPaperDate) {
        if (TextUtils.isEmpty(answerPaperDate))
            return false;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = format.parse(answerPaperDate);
            Date date = new Date();

            Log.d(TAG, "checkDate: " + date);
            Log.d(TAG, "checkDate: " + date1);
            return (date.getTime() >= date1.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void setImageSubject(Context context, File imageFile, ImageView img_class_logo) {
        Log.d(TAG, "setImage: " + imageFile);
        if (imageFile.exists()) {
            Glide.with(context)
                    .load(imageFile)
                    .apply(new RequestOptions().placeholder(R.drawable.sub_english)
                            .error(R.drawable.sub_english)
                            .dontAnimate())
                    .into(img_class_logo);
        } else
            img_class_logo.setImageResource(R.drawable.profile);
    }

    public static void setImageSubject(Context context, String imageFile, ImageView img_class_logo) {
        Log.d(TAG, "setImage: " + imageFile);
        if (!TextUtils.isEmpty(imageFile)) {
            Glide.with(context)
                    .load(imageFile)
                    .apply(new RequestOptions().placeholder(R.drawable.sub_english)
                            .error(R.drawable.sub_english)
                            .dontAnimate())
                    .into(img_class_logo);
        } else
            img_class_logo.setImageResource(R.drawable.sub_english);
    }

    public static void setImage(Context context, String imageFile, ImageView img_class_logo, int error_icon) {
        Log.d(TAG, "setImage: " + imageFile);
        if (!TextUtils.isEmpty(imageFile)) {
            Glide.with(context)
                    .load(imageFile)
                    .apply(new RequestOptions().placeholder(error_icon).error(error_icon)
                            .dontAnimate())
                    .into(img_class_logo);

        } else {
            img_class_logo.setImageResource(R.drawable.profile);
        }

    }

    public static void setImage(Context context, File imageFile, ImageView img_class_logo, int error_icon) {
        Log.d(TAG, "setImage: " + imageFile);
        if (imageFile.exists()) {
            Glide.with(context)
                    .load(imageFile)
                    .apply(new RequestOptions().placeholder(error_icon)
                            .error(error_icon)
                            .dontAnimate())
                    .into(img_class_logo);
        } else
            img_class_logo.setImageResource(R.drawable.profile);
    }

}
