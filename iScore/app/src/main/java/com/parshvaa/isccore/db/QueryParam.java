package com.parshvaa.isccore.db;

/**
 * Created by Karan - Empiere on 3/1/2017.
 */

public class QueryParam {
    public String key = "", value = "";

    public QueryParam(String key, String value) {
        this.key = key;
        this.value = value;
    }


}
