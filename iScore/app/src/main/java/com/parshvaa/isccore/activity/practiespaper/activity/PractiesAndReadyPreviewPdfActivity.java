package com.parshvaa.isccore.activity.practiespaper.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.practiespaper.HTMLUtils;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.ExamTypePatternDetail;
import com.parshvaa.isccore.model.MasterQuestion;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.tempmodel.PrelimQuestionListModel;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.RomanNumber;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Karan - Empiere on 1/22/2018.
 */

public class PractiesAndReadyPreviewPdfActivity extends BaseActivity {

    private static final String TAG = "PractiesAndReadyPreview";
    public String HTML = "";
    public WebView pdfview;
    public MyDBManager mDb;
    public PractiesAndReadyPreviewPdfActivity instance;
    public CursorParserUniversal cParse;

    boolean isLastPrinted = true;
    public FloatingActionButton imgprint;
    public ImageView img_back, img_rotation, img_home;
    public TextView tv_title;
    public static ArrayList<PrelimQuestionListModel> questionsList = new ArrayList<>();
    int p = 0;
    int n = 65;
    int j = 0;

    ArrayList<String> mtcArray = new ArrayList<>();
    public ExamTypePatternDetail examTypePatternDetail;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_preview_pdf);
        Utils.logUser();


        instance = this;
        showProgress(true, true);//Utils.showProgressDialog(instance);
        cParse = new CursorParserUniversal();
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        imgprint = findViewById(R.id.imgprint);
        img_back = findViewById(R.id.img_back);
        img_home = findViewById(R.id.img_home);
        tv_title = findViewById(R.id.tv_title);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });
        pdfview = findViewById(R.id.pdfView);
        pdfview.getSettings().setJavaScriptEnabled(true);

        pdfview.getSettings().setAllowFileAccess(true);
        pdfview.getSettings().setDomStorageEnabled(true);
        imgprint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                printPDF();
            }
        });
        Intent intent = getIntent();
        questionsList = (ArrayList<PrelimQuestionListModel>) intent.getSerializableExtra("questionsList");
        img_rotation = findViewById(R.id.img_rotation);
        img_rotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                }
            }
        });

        loadHTML();


        imgprint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                printPDF();
            }
        });

    }

    public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)
                .setMessage("Sure about visiting the dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(instance, DashBoardActivity.class));
                        dialog.dismiss();
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void loadHTML() {

        Date d = new Date();
        CharSequence currDate = DateFormat.format("dd-MMM-yyyy", d.getTime());

      //  if (App.practiesPaperObj != null && TextUtils.isEmpty(App.practiesPaperObj.getSubjectName()))
            HTML = HTMLUtils.getPrelimTestHeader(mySharedPref,
                    App.practiesPaperObj.getSubjectName(),
                    App.practiesPaperObj.getExamTypeName(),
                    App.practiesPaperObj.getTotalMarks(),
                    (String) currDate,
                    "All",
                    App.practiesPaperObj.getDuration());

        Log.d(TAG, "loadHTML: "+HTML);
        if (getIntent().getExtras().getBoolean("showAns")) {
            tv_title.setText("Answer Paper");
            new ModelAnsPaper(instance).execute();
        } else {
            tv_title.setText("Question Paper");
            new QuestionPaper(instance).execute();
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void printPDF() {
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = pdfview.createPrintDocumentAdapter();
        String jobName = getString(R.string.app_name) + " Report "
                + System.currentTimeMillis();
        PrintAttributes printAttrs = new PrintAttributes.Builder().
                setColorMode(PrintAttributes.COLOR_MODE_MONOCHROME).
                setMediaSize(PrintAttributes.MediaSize.NA_LETTER.asLandscape()).
                setMinMargins(PrintAttributes.Margins.NO_MARGINS).
                build();
        PrintJob printJob = printManager.print(jobName, printAdapter,
                printAttrs);
    }

    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {


        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                showProgress(false, true);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public class QuestionPaper extends AsyncTask<String, Void, String> {

        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        String StudentQuestionPaperDetailID = "";

        private QuestionPaper(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);


        }


        @Override
        protected String doInBackground(String... strings) {

            int p = 0;
            Cursor cExamTypePatrernDetail = mDb.getAllRows(DBQueries.getExamTypePatternDetail(App.practiesPaperObj.getExamTypePatternId()));

            if (cExamTypePatrernDetail != null && cExamTypePatrernDetail.moveToFirst()) {
                do {
                    examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());
                    if (examTypePatternDetail.getIsQuestion().equals("0")) {
                        HTML = HTML + "<tr>" +
                                "<td width=\"30\"><b>" + examTypePatternDetail.getQuestionNO() + "</b></td>" +
                                "<td width=\"10\"><b>" + examTypePatternDetail.getSubQuestionNO() + "</b></td>" +
                                "<td class=\"question\"><b>" + examTypePatternDetail.getQuestionTypeText() + "</b></td>" +
                                "<td align=\"right\" width=\"10\"><b>" + examTypePatternDetail.getQuestionMarks() + "</b></td>" +
                                "</tr>";
                    } else {
                        Cursor Ques;
                        MasterQuestion masterQuestion = null;
                        Ques = mDb.getAllRows(DBQueries.getQuestions
                                (App.practiesPaperObj.getStudentQuestionPaperID(), examTypePatternDetail.getExamTypePatternDetailID()));
                        if (Ques != null && Ques.moveToFirst()) {

                            do {
                                StudentQuestionPaperDetailID = Ques.getString(Ques.getColumnIndexOrThrow("StudentQuestionPaperDetailID"));
                                masterQuestion = (MasterQuestion) cParse.parseCursor(Ques, new MasterQuestion());
                                Utils.Log("Question Type : ", masterQuestion.getQuestionTypeID());
                                String QuesTypeId = masterQuestion.getQuestionTypeID();
                                String IsQuesType = "";
                                int i = 1;
                                Cursor cIsPassage = mDb.getAllRows(DBQueries.checkQusTypeisPassage(Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID"))));

                                int isPassage = 0;
                                if (cIsPassage != null && cIsPassage.moveToFirst()) {
                                    isPassage = cIsPassage.getInt(0);
                                    cIsPassage.close();
                                }


                                int isMTC = mDb.getAllRecordsUsingQuery(DBQueries.checkQusTypeisMTC(QuesTypeId + ""));
                                if (isMTC == 1) {
                                    String finalHTML = Utils.replaceImagesPath(masterQuestion.getQuestion());
                                    String finalANS = Utils.replaceImagesPath(masterQuestion.getAnswer());
                                    if (IsQuesType.equals("")) {
                                        IsQuesType = QuesTypeId;
                                        String HTML1;
                                        HTML1 = "<tr class=\"mtc\">\n" +
                                                "<td width=\"30\"><b></b></td>\n" +
                                                "<td width=\"20\"><b></b></td>\n";
                                        if (!examTypePatternDetail.isQuestionVisible.equals("0")) {
                                            HTML1 = HTML1 + "<td class=\"question\" ><div style=\"width:50%; float:left; \">"
                                                    + j++ + ") " + finalHTML + " </div>" +
                                                    "<div id='mtc" + (p) + "' style=\"width:50%; float:left;\">" + finalANS + "</div></td>\n";
                                            mtcArray.add(finalANS);
                                            p++;
                                        } else {
                                            HTML1 = HTML1 + "<td class=\"question\" ><div style=\"width:50%; float:left;\"> "
                                                    + "&nbsp;</div><div id='mtc" + (p) + "' style=\"width:50%; float:left;\">"
                                                    + finalANS + "</div></td>\n";
                                            mtcArray.add(finalANS);
                                            p++;
                                        }

                                        HTML1 = HTML1 + "<td align=\"right\" width=\"10\"><b></b></td>\n" + "</tr>";

                                        HTML = HTML + HTML1;
                                    } else if (!IsQuesType.equals(QuesTypeId)) {
                                        j = 1;
                                        n = 65;
                                        String HTML1;
                                        HTML1 = "<tr class=\"mtc\">\n" +
                                                "<td width=\"30\"><b></b></td>\n" +
                                                "<td width=\"20\"><b>" + examTypePatternDetail.getSubQuestionNO() + "</b></td>\n";
                                        if (!examTypePatternDetail.isQuestionVisible.equals("0")) {
                                            HTML1 = HTML1 + "<td class=\"question\" ><div style=\"width:50%; float:left; \">"
                                                    + j++ + ") " + (char) n++ + ") " + finalANS + "</div></td>\n";
                                            mtcArray.add(finalANS);
                                        } else {
                                            HTML1 = HTML1 + "<td class=\"question\" ><div style=\"width:50%; float:left;\"> "
                                                    + "&nbsp;</div><div  id='mtc" + (p) + "' style=\"width:50%; float:left;\">"
                                                    + finalANS + "</div></td>\n";
                                            mtcArray.add(finalANS);
                                            p++;
                                        }
                                        HTML1 = HTML1 + "<td align=\"right\" width=\"10\"><b></b></td>\n" + "</tr>";
                                        HTML = HTML + HTML1;
                                    }
                                    i++;
                                } else if (isPassage == 1) {
                                    int sub_questions_total, sub_question_type_total = -1, n = 1, sq = 0, sub_qt = 0, k = 1, SubQuestionTypeID = -1, sss = 1;
                                    sub_qt = 0;
                                    Cursor clabel = mDb.getAllRows(DBQueries.getLabel(masterQuestion.getMQuestionID()));
                                    //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                                    if (clabel != null && clabel.moveToFirst()) {

                                        k = 1;
                                        do {
                                            Cursor csqi = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                    StudentQuestionPaperDetailID,
                                                    clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID"))));
                                            if (csqi != null && csqi.moveToFirst()) {
                                                sq = 0;
                                                n = 1;
                                                do {
                                                    Cursor totla = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id_total(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                            StudentQuestionPaperDetailID,
                                                            clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")),
                                                            csqi.getString(csqi.getColumnIndex("SubQuestionTypeID"))));
                                                    sub_questions_total = totla.getCount();
                                                    totla.close();
                                                    if (n == 1) {
                                                        Cursor c = mDb.getAllRows(" SELECT DISTINCT(sqt.SubQuestionTypeID)\n" +
                                                                "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                                                                "INNER JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                                                                "INNER JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                                                                "INNER JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                                                                "WHERE `cqpsq`.`PaperID` = '" + App.practiesPaperObj.getStudentQuestionPaperID() + "'\n" +
                                                                "AND `cqpsq`.`DetailID` = '" + StudentQuestionPaperDetailID + "'\n" +
                                                                "AND `psqt`.`PassageSubQuestionTypeID` = '" + clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")) + "'");

                                                        if (c != null && c.moveToFirst()) {
                                                            sub_question_type_total = c.getCount();
                                                            c.close();
                                                        }


                                                    }
                                                    HTML = HTML + "<table> <tr class=\"margin_top\">\n" +
                                                            "<td width=\"30\"></td>\n" +
                                                            "<td width=\"30\" style=\"vertical-align: top;\"><b>";
                                                    if (sub_qt == 0 && n == 1) {
                                                        HTML = HTML + k + " </b></td>";
                                                    }
                                                    if (SubQuestionTypeID != csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"))) {
                                                        SubQuestionTypeID = csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"));
                                                        sq++;
                                                        sss = 1;
                                                        if (n == 1) {
                                                            HTML = HTML + "  <td width=\"660\">\n" +
                                                                    "<table width=\"100%\">\n" +
                                                                    "   <tr>\n" +
                                                                    "       <td width=\"30\">\n" +
                                                                    "           <b>";
                                                            HTML = HTML + csqi.getString(csqi.getColumnIndex("Label")) + ")" +
                                                                    "           </b>\n" +
                                                                    "       </td>\n" +
                                                                    "       <td width=\"630\">\n";

                                                            if (sub_question_type_total == 1) {
                                                                HTML = HTML + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                            } else {
                                                                HTML = HTML + RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                            }

                                                            HTML = HTML + "        </td>\n";
                                                            int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            HTML = HTML + " <td align=\"right\" width=\"30\">W" + marks + "</td>" +
                                                                    "   </tr>\n" +
                                                                    "</table>\n" +
                                                                    "</td>";
                                                        } else {
                                                            HTML = HTML + "<td width=\"660\">\n" +
                                                                    "<table>\n" +
                                                                    "   <tr>\n" +
                                                                    "       <td width=\"30\">\n" +
                                                                    "       </td>\n" +
                                                                    "       <td width=\"630\">\n" +
                                                                    RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType")) +
                                                                    "       </td>\n";
                                                            int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            HTML = HTML + " <td align=\"right\" width=\"30\">Q" + marks + "</td>" +
                                                                    "   </tr>\n" +
                                                                    "</table>\n" +
                                                                    "</td>    ";
                                                        }

                                           /* int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>";*/
                                                    }

                                                    HTML = HTML + "</tr>" +
                                                            " <tr class=\"margin_top\">\n" +
                                                            "   <td width=\"30\"></td>\n" +
                                                            "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                            "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";
                                                    if (sub_questions_total == 1) {
                                                        HTML = HTML + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                                    } else {
                                                        HTML = HTML + sss + ") " + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                                    }

                                                    HTML = HTML + "   </td>\n" +
                                                            "   <td align=\"right\" width=\"30\"></td>\n" +
                                                            "</tr>      ";
                                                    n++;
                                                    sss++;
                                                }
                                                while (csqi.moveToNext());
                                                csqi.close();
                                            }
                                            if (sub_qt == 0) {
                                                HTML = HTML + "<tr class=\"margin_top\">\n" +
                                                        "<td width=\"30\"></td>\n" +
                                                        "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                        "<td width=\"660\">" +
                                                        Utils.replaceImagesPath(masterQuestion.getQuestion()) +
                                                        "</td>\n" +
                                                        "<td align=\"right\" width=\"30\"></td>\n" +
                                                        "</tr>     \n";
                                            }

                                            sub_qt++;
                                        } while (clabel.moveToNext());
                                        clabel.close();
                                        k++;

                                    }
                                    HTML = HTML + "</table>";

                                } else {

                                    String finalHTML = Utils.replaceImagesPath(masterQuestion.getQuestion());
                                    String HTML1;
                                    HTML1 = "<tr>\n" +
                                            "<td width=\"30\"><b></b></td>\n" +
                                            "<td width=\"20\"><b>" + examTypePatternDetail.getSubQuestionNO() + "</b></td>\n" +
                                            "<td class=\"question\">" + finalHTML + "</td>\n" +
                                            "<td align=\"right\" width=\"10\"><b></b></td>\n" +
                                            "</tr>";
                                    HTML = HTML + HTML1;

                                }


                            } while (Ques.moveToNext());
                        } else {
                            HTML = HTML + "<tr>\n" +
                                    "<td width=\"30\"><b></b></td>\n" +
                                    "<td width=\"20\"><b>" + examTypePatternDetail.getSubQuestionNO() + "</b></td>\n" +
                                    "<td><b></b></td>\n" +
                                    "<td align=\"right\" width=\"10\"><b></b></td>\n" +
                                    "</tr>";
                        }

                    }
                } while (cExamTypePatrernDetail.moveToNext());

            } else {

            }
            HTML = HTML + "</table>" +
                    "\n" +
                    "<div style=\"height:150px !important; \"></div>     " +
                    "           </div>    \n" +
                    "            </div>\n" +

                    "       </div> \n" +
                    "" + HTMLUtils.getShuffleScript();


            for (String s : mtcArray) {
                s = s.replaceAll(System.getProperty("line.separator"), "");
                HTML = HTML + " \n mtcarray.push(\"" + s + "\");";
            }

            HTML = HTML + "\n var shuffledArray = shuffleArray(mtcarray);";
            n = 65;
            for (int l = 0; l < mtcArray.size(); l++) {
                HTML = HTML + "\n  document.getElementById('mtc" + l + "').innerHTML = \"" +
                        (char) n++ + ") \" + mtcarray[" + l + "]; ";
            }
            HTML = HTML + "    </script> </body>\n" +
                    "</html>";
            longInfo(HTML, "PDF ::--> ");
            return HTML;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String HTML) {
            WebSettings settings = pdfview.getSettings();
            //settings.setMinimumFontSize(30);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setBuiltInZoomControls(true);
            settings.setJavaScriptEnabled(true);
            settings.setSupportZoom(true);
            settings.setDomStorageEnabled(true);
            pdfview.setWebViewClient(new myWebClient());

            pdfview.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
        }
    }

    private static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }

    public class ModelAnsPaper extends AsyncTask<String, Void, String> {

        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        String examTypesID = "";

        String StudentQuestionPaperDetailID = "";

        private ModelAnsPaper(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);


        }


        @Override
        protected String doInBackground(String... strings) {


            Cursor cExamTypePatrernDetail = mDb.getAllRows(DBQueries.getExamTypePatternDetail(App.practiesPaperObj.getExamTypePatternId()));

            if (cExamTypePatrernDetail != null && cExamTypePatrernDetail.moveToFirst()) {
                do {
                    examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());

                    if (examTypePatternDetail.getIsQuestion().equals("0")) {
                        HTML = HTML + "<tr>" +
                                "<td width=\"30\"><b>" + examTypePatternDetail.getQuestionNO() + "</b></td>" +
                                "<td width=\"10\"><b>" + examTypePatternDetail.getSubQuestionNO() + "</b></td>" +
                                "<td class=\"question\"><b>" + examTypePatternDetail.getQuestionTypeText() + "</b></td>" +
                                "<td align=\"right\" width=\"10\"><b>" + examTypePatternDetail.getQuestionMarks() + "</b></td>" +
                                "</tr>";


                    } else {
                        Cursor Ques;

                        MasterQuestion masterQuestion = null;
                        Ques = mDb.getAllRows(DBQueries.getQuestions(App.practiesPaperObj.getStudentQuestionPaperID(), examTypePatternDetail.getExamTypePatternDetailID()));


                        if (Ques != null && Ques.moveToFirst()) {
                            do {
                                masterQuestion = (MasterQuestion) cParse.parseCursor(Ques, new MasterQuestion());

                                StudentQuestionPaperDetailID = Ques.getString(Ques.getColumnIndexOrThrow("StudentQuestionPaperDetailID"));

                                Cursor cIsPassage = mDb.getAllRows(DBQueries.checkQusTypeisPassage(Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID"))));

                                int isPassage = 0;
                                if (cIsPassage != null && cIsPassage.moveToFirst()) {
                                    isPassage = cIsPassage.getInt(0);
                                    cIsPassage.close();
                                }


                                if (isPassage == 1) {

                                    int sub_questions_total, sub_question_type_total = -1, n = 1, sq = 0, sub_qt = 0, k = 1, SubQuestionTypeID = -1, sss = 1;
                                    sub_qt = 0;
                                    Cursor clabel = mDb.getAllRows(DBQueries.getLabel(masterQuestion.getMQuestionID()));
                                    //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                                    if (clabel != null && clabel.moveToFirst()) {

                                        k = 1;
                                        do {
                                            Cursor csqi = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                    StudentQuestionPaperDetailID,
                                                    clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID"))));
                                            if (csqi != null && csqi.moveToFirst()) {
                                                sq = 0;
                                                n = 1;
                                                do {
                                                    Cursor totla = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id_total(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                            StudentQuestionPaperDetailID,
                                                            clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")),
                                                            csqi.getString(csqi.getColumnIndex("SubQuestionTypeID"))));
                                                    sub_questions_total = totla.getCount();
                                                    totla.close();
                                                    if (n == 1) {
                                                        Cursor c = mDb.getAllRows(" SELECT DISTINCT(sqt.SubQuestionTypeID)\n" +
                                                                "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                                                                "INNER JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                                                                "INNER JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                                                                "INNER JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                                                                "WHERE `cqpsq`.`PaperID` = '" + App.practiesPaperObj.getStudentQuestionPaperID() + "'\n" +
                                                                "AND `cqpsq`.`DetailID` = '" + StudentQuestionPaperDetailID + "'\n" +
                                                                "AND `psqt`.`PassageSubQuestionTypeID` = '" + clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")) + "'");

                                                        if (c != null && c.moveToFirst()) {
                                                            sub_question_type_total = c.getCount();
                                                            c.close();
                                                        }


                                                    }
                                                    HTML = HTML + "<table> <tr class=\"margin_top\">\n" +
                                                            "<td width=\"30\"></td>\n" +
                                                            "<td width=\"30\" style=\"vertical-align: top;\"><b>";
                                                    if (sub_qt == 0 && n == 1) {
                                                        HTML = HTML + k + " </b></td>";
                                                    }
                                                    if (SubQuestionTypeID != csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"))) {
                                                        SubQuestionTypeID = csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"));
                                                        sq++;
                                                        sss = 1;
                                                        if (n == 1) {
                                                            HTML = HTML + "  <td width=\"660\">\n" +
                                                                    "<table width=\"100%\">\n" +
                                                                    "   <tr>\n" +
                                                                    "       <td width=\"30\">\n" +
                                                                    "           <b>";
                                                            HTML = HTML + csqi.getString(csqi.getColumnIndex("Label")) + ")" +
                                                                    "           </b>\n" +
                                                                    "       </td>\n" +
                                                                    "       <td width=\"630\">\n";

                                                            if (sub_question_type_total == 1) {
                                                                HTML = HTML + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                            } else {
                                                                HTML = HTML + RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                            }

                                                            HTML = HTML + "        </td>\n";
                                                            int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            HTML = HTML + " <td align=\"right\" width=\"30\">W" + marks + "</td>" +
                                                                    "   </tr>\n" +
                                                                    "</table>\n" +
                                                                    "</td>";
                                                        } else {
                                                            HTML = HTML + "<td width=\"660\">\n" +
                                                                    "<table>\n" +
                                                                    "   <tr>\n" +
                                                                    "       <td width=\"30\">\n" +
                                                                    "       </td>\n" +
                                                                    "       <td width=\"630\">\n" +
                                                                    RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType")) +
                                                                    "       </td>\n";
                                                            int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                            HTML = HTML + " <td align=\"right\" width=\"30\">Q" + marks + "</td>" +
                                                                    "   </tr>\n" +
                                                                    "</table>\n" +
                                                                    "</td>    ";
                                                        }

                                           /* int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>";*/
                                                    }

                                                    HTML = HTML + "</tr>" +
                                                            " <tr class=\"margin_top\">\n" +
                                                            "   <td width=\"30\"></td>\n" +
                                                            "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                            "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";
                                                    if (sub_questions_total == 1) {
                                                        HTML = HTML + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                                        HTML = HTML + "   </td>\n" +
                                                                "   <td align=\"right\" width=\"30\"></td>\n" +
                                                                "</tr>" +
                                                                " <tr class=\"margin_top\">\n" +
                                                                "   <td width=\"30\"></td>\n" +
                                                                "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                                "   <td width=\"660\" style=\"padding-left: 5%;\" class=\"sub\">\nAns. ";

                                                        HTML = HTML + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Answer")));
                                                        HTML = HTML + "   </td>\n" +
                                                                "   <td align=\"right\" width=\"30\"></td>\n" +
                                                                "</tr>";
                                                    } else {
                                                        HTML = HTML + sss + ") " + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                                        HTML = HTML + "   </td>\n" +
                                                                "   <td align=\"right\" width=\"30\"></td>\n" +
                                                                "</tr>" +
                                                                " <tr class=\"margin_top\">\n" +
                                                                "   <td width=\"30\"></td>\n" +
                                                                "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                                "   <td width=\"660\" style=\"padding-left: 5%;\" class=\"sub\">\nAns. ";

                                                        HTML = HTML + Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Answer")));
                                                        HTML = HTML + "   </td>\n" +
                                                                "   <td align=\"right\" width=\"30\"></td>\n" +
                                                                "</tr>";
                                                    }

                                                    HTML = HTML + "   </td>\n" +
                                                            "   <td align=\"right\" width=\"30\"></td>\n" +
                                                            "</tr>      ";
                                                    n++;
                                                    sss++;
                                                }
                                                while (csqi.moveToNext());
                                                csqi.close();
                                            }
                                            if (sub_qt == 0) {
                                                HTML = HTML + "<tr class=\"margin_top\">\n" +
                                                        "<td width=\"30\"></td>\n" +
                                                        "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                        "<td width=\"660\">" +
                                                        Utils.replaceImagesPath(masterQuestion.getQuestion()) +
                                                        "</td>\n" +
                                                        "<td align=\"right\" width=\"30\"></td>\n" +
                                                        "</tr>     \n" +
                                                        "<tr class=\"margin_top\">\n" +
                                                        "<td width=\"30\"></td>\n" +
                                                        "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                        "<td width=\"660\">" +
                                                        Utils.replaceImagesPath(masterQuestion.getAnswer()) +
                                                        "</td>\n" +
                                                        "<td align=\"right\" width=\"30\"></td>\n" +
                                                        "</tr>     \n";
                                            }

                                            sub_qt++;
                                        } while (clabel.moveToNext());
                                        clabel.close();
                                        k++;

                                    }
                                    HTML = HTML + "</table>";

                                } else {
                                    String finalHTML = Utils.replaceImagesPath(masterQuestion.getQuestion());
                                    HTML = HTML + "<tr>" +
                                            "<td width=\"30\"><b></b></td>" +
                                            "<td width=\"10\"><b>" + examTypePatternDetail.getSubQuestionNO() + "</b></td>" +
                                            "<td class=\"question\">" + finalHTML + "</td>" +
                                            "<td align=\"right\" width=\"10\"><b></b></td>" +
                                            "</tr>";


                                    if (getIntent().getExtras().getBoolean("showAns")) {
                                        String finalANS = Utils.replaceImagesPath(masterQuestion.getAnswer());
                                        HTML = HTML + "<tr>\n" +
                                                "                                    <td width=\"30\"><b></b></td>\n" +
                                                "                                    <td width=\"10\"><b>Ans</b></td>\n" +
                                                "                                    <td class=\"question\">" + finalANS + "</td>\n" +
                                                "                                    <td align=\"right\" width=\"10\"><b></b></td>\n" +
                                                "                                </tr>";
                                    }
                                }
                            } while (Ques.moveToNext());
                        } else {
                            HTML = HTML + "<tr>\n" +
                                    "                                    <td width=\"30\"><b></b></td>\n" +
                                    "                                    <td width=\"10\"><b>" + examTypePatternDetail.getSubQuestionNO() + "</b></td>\n" +
                                    "                                    <td><b></b></td>\n" +
                                    "                                    <td align=\"right\" width=\"10\"><b></b></td>\n" +
                                    "                                </tr>";

                            if (getIntent().getExtras().getBoolean("showAns")) {
                                HTML = HTML + "<tr>\n" +
                                        "                                    <td width=\"30\"><b></b></td>\n" +
                                        "                                    <td width=\"10\"><b>Ans</b></td>\n" +
                                        "                                    <td><b></b></td>\n" +
                                        "                                    <td align=\"right\" width=\"10\"><b></b></td>\n" +
                                        "                                </tr>";
                            }
                        }

                    }
                } while (cExamTypePatrernDetail.moveToNext());
                cExamTypePatrernDetail.close();
                //table.addCell(new PdfPCell(new Paragraph("test image <img src='file://storage/emulated/0/iScore/Uploads/English (First Language)/piediagram1.gif' />")));
            } else {
                Utils.showToast("Not any ExamTypePatternDetail Found ", instance);
            }
            HTML = HTML + "</table>" +
                    "\n" +
                    "<div style=\"height:150px !important; \"></div>     " +
                    "           </div>    \n" +
                    "            </div>\n" +
                    "     <br/><br/>" +
                    "    </body>\n" +
                    "</html>";


            return HTML;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String HTML) {
            WebSettings settings = pdfview.getSettings();
            //settings.setMinimumFontSize(30);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setBuiltInZoomControls(true);
            settings.setJavaScriptEnabled(true);
            settings.setSupportZoom(true);
            settings.setDomStorageEnabled(true);
            pdfview.setWebViewClient(new myWebClient());

            pdfview.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
        }
    }
}
