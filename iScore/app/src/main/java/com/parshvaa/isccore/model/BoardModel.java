package com.parshvaa.isccore.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 12/25/2017.
 */

public class BoardModel implements Serializable {
    public String BoardID=" ", BoardName=" ", BoardFullName=" ";

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getBoardFullName() {
        return BoardFullName;
    }

    public void setBoardFullName(String boardFullName) {
        BoardFullName = boardFullName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }
}
