package com.parshvaa.isccore.activity.weeklydise;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

public class WeeklyDiseActivity extends BaseActivity {

    public RecyclerView recycler_mcq_sub;
    private WeeklyDiseAdapter adapter;
    public JsonParserUniversal jParser;
    public ImageView img_back;
    public TextView tv_title;
    boolean run = true;
    Date myDate = null;
    ArrayList<Date> endDate = new ArrayList<>();
    ArrayList<Date> startDate = new ArrayList<>();
    public Date start = null;
    public String Created_date = "";
    private LinearLayout emptyView;
    public TextView tv_empty;
    public Date current_date=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_weekly_dise);
        Utils.logUser();



        jParser = new JsonParserUniversal();
        recycler_mcq_sub = findViewById(R.id.recycler_mcq_sub);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);

        recycler_mcq_sub.addItemDecoration(new ItemOffsetDecoration(spacing));
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Weekly Digest");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);

        emptyView.setVisibility(View.GONE);

        Created_date = mySharedPref.getCreatedOn();
        //2018-03-28 11:20:19
        try {
            myDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(Created_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        current_date= Calendar.getInstance().getTime();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        while (run) {
            if (myDate.getTime() < current_date.getTime()) {
                int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
                if (dayOfWeek == Calendar.SATURDAY) {
                    endDate.add(cal.getTime());
                }
                myDate = cal.getTime();
                cal.add(Calendar.DAY_OF_WEEK, 1);
            } else {
                run = false;
            }
        }


        for (int i = 0; i < endDate.size(); i++) {

            if (i == 0) {
                Date myDate;

                try {
                   myDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(Created_date);
                    startDate.add(myDate);
                    start = endDate.get(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Calendar cal1 = Calendar.getInstance();
                cal1.setTime(start);
                cal1.add(Calendar.DATE, 1);
                start = endDate.get(i);
                startDate.add(cal1.getTime());

            }
            System.out.println("Start Date -> " + fmt.format(startDate.get(i)) + " End Date ->" + fmt.format(endDate.get(i)));
        }
        setupRecyclerView(startDate, endDate);
    }

    private void setupRecyclerView(ArrayList<Date> startDate, ArrayList<Date> endDate) {
        final Context context = recycler_mcq_sub.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_mcq_sub.setLayoutAnimation(controller);
        recycler_mcq_sub.scheduleLayoutAnimation();
        Collections.reverse(startDate);
        Collections.reverse(endDate);
        recycler_mcq_sub.setLayoutManager(new LinearLayoutManager(context));
        if (startDate.size() == 0 && endDate.size() == 0) {
            tv_empty.setText("No Report Generate");
             recycler_mcq_sub.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);

        } else {
            recycler_mcq_sub.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            adapter = new WeeklyDiseAdapter(startDate, endDate, context);
            recycler_mcq_sub.setAdapter(adapter);
        }
    }

}
