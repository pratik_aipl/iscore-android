package com.parshvaa.isccore.activity.searchpaper.practisepaper.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parshvaa.isccore.activity.practiespaper.activity.PractiesPaperSolutionActivity;
import com.parshvaa.isccore.activity.practiespaper.activity.setpaper.SetPaperSolutionActivity;
import com.parshvaa.isccore.activity.searchpaper.practisepaper.model.PractisePaperModel;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.PaperTypeInterface;
import com.parshvaa.isccore.model.PrelimTestRecord;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.PrelimQuestionListModel;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.RomanNumber;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by empiere-vaibhav on 1/12/2018.
 */

public class PractcePaperAdapter extends RecyclerView.Adapter<PractcePaperAdapter.MyViewHolder> {

    private List<PractisePaperModel> paperTypeList;
    public Context context;
    public int row_index = 0;
    public PaperTypeInterface paperTypeInterface;
    public String SubjectName;
    public long StudentQuestionPaperID = -1;
    public String PageNo = "";
    public PrelimTestRecord prelimObj;
    public ArrayList<PrelimTestRecord> TempPrelimTestRecordArrayList = new ArrayList<>();
    public PractisePaperModel testModel;
    public ArrayList<PrelimQuestionListModel> questionsList = new ArrayList<>();
    public ArrayList<String> iDSArray = new ArrayList<String>();
    public int sub_questions_total, sub_question_type_total, n = 1, sq = 0, sub_qt = 0, k = 1, SubQuestionTypeID = -1, sss = 1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_paper_type, tv_marks, tv_date;


        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_paper_type = view.findViewById(R.id.tv_paper_type);
            tv_marks = view.findViewById(R.id.tv_marks);
            tv_date = view.findViewById(R.id.tv_date);


        }
    }


    public PractcePaperAdapter(List<PractisePaperModel> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;

        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_serach_practise_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        testModel = paperTypeList.get(position);
        holder.tv_subject_name.setText(testModel.getSubjectName());
        holder.tv_paper_type.setText(testModel.getExamTypeName());
        holder.tv_marks.setText(String.valueOf(testModel.getTotalMarks()));

        String date = testModel.getCreatedOn();
        date = date.substring(0, 10);

        holder.tv_date.setText(Utils.changeDateToDDMMYYYY(date));



       /* headerId = Integer.parseInt(testModel.getStudentQuestionPaperID());
        SubjectID = Integer.parseInt(testModel.getSubjectID());
        SubjectName = testModel.getSubjectName();*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StudentQuestionPaperID = Long.parseLong(paperTypeList.get(position).getStudentQuestionPaperID());
                App.practiesPaperObj.setStudentQuestionPaperID(String.valueOf(StudentQuestionPaperID));
                App.practiesPaperObj.setSubjectName(String.valueOf(paperTypeList.get(position).getSubjectName()));
                App.practiesPaperObj.setExamTypeName(String.valueOf(paperTypeList.get(position).getExamTypeName()));
                App.practiesPaperObj.setTotalMarks(String.valueOf(paperTypeList.get(position).getTotalMarks()));
                App.practiesPaperObj.setDuration(String.valueOf(paperTypeList.get(position).getDuration()));
                App.practiesPaperObj.setExamTypePatternId(String.valueOf(paperTypeList.get(position).getExamTypePatternID()));
                if (paperTypeList.get(position).getPaperTypeID().equals("3") || paperTypeList.get(position).getPaperTypeID().equals(3)) {
                    new sendToSetPaper(context).execute();

                } else {
                    try {
                        TempPrelimTestRecordArrayList = (ArrayList) new CustomDatabaseQuery(context, new PrelimTestRecord())
                                .execute(new String[]{DBNewQuery.getPrelimPaperQuestionsIsPassage(String.valueOf(StudentQuestionPaperID))}).get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    questionsList.clear();

                    new sendToPrelimAndRedy(context).execute();
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return paperTypeList.size();
    }


    public class sendToPrelimAndRedy extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;


        private sendToPrelimAndRedy(Context context) {
            this.context = context;
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {
            PageNo = "";
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {


            ArrayList<PrelimTestRecord> prelimTestRecordArrayList = new ArrayList<>();
            for (int i = 0; i < TempPrelimTestRecordArrayList.size(); i++) {
                String HTML = "";
                if (TempPrelimTestRecordArrayList.get(i).getIsPassage() != null && TempPrelimTestRecordArrayList.get(i).getIsPassage().equals("1")) {
                    HTML = "";
                    sub_qt = 0;
                    Cursor clabel = mDb.getAllRows(DBQueries.getLabel(TempPrelimTestRecordArrayList.get(i).getMQuestionID()));
                    //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                    if (clabel != null && clabel.moveToFirst()) {

                        k = 1;
                        do {
                            Cursor csqi = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id(App.practiesPaperObj.getStudentQuestionPaperID(),
                                    TempPrelimTestRecordArrayList.get(i).getStudentQuestionPaperDetailID(),
                                    clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID"))));
                            if (csqi != null && csqi.moveToFirst()) {
                                sq = 0;
                                n = 1;
                                do {
                                    Cursor totla = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id_total(App.practiesPaperObj.getStudentQuestionPaperID(),
                                            TempPrelimTestRecordArrayList.get(i).getStudentQuestionPaperDetailID(),
                                            clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")),
                                            csqi.getString(csqi.getColumnIndex("SubQuestionTypeID"))));
                                    sub_questions_total = totla.getCount();
                                    totla.close();
                                    if (n == 1) {
                                        Cursor c = mDb.getAllRows(" SELECT DISTINCT(sqt.SubQuestionTypeID)\n" +
                                                "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                                                "INNER JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                                                "INNER JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                                                "INNER JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                                                "WHERE `cqpsq`.`PaperID` = '" + App.practiesPaperObj.getStudentQuestionPaperID() + "'\n" +
                                                "AND `cqpsq`.`DetailID` = '" + TempPrelimTestRecordArrayList.get(i).getStudentQuestionPaperDetailID() + "'\n" +
                                                "AND `psqt`.`PassageSubQuestionTypeID` = '" + clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")) + "'");

                                        if (c != null && c.moveToFirst()) {
                                            sub_question_type_total = c.getCount();
                                            c.close();
                                        }


                                    }
                                    HTML = HTML + "<table> <tr class=\"margin_top\">\n" +
                                            "<td width=\"30\"></td>\n" +
                                            "<td width=\"30\" style=\"vertical-align: top;\"><b>";
                                    if (sub_qt == 0 && n == 1) {
                                        HTML = HTML + k + " </b></td>";
                                    }
                                    if (SubQuestionTypeID != csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"))) {
                                        SubQuestionTypeID = csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"));
                                        sq++;
                                        sss = 1;
                                        if (n == 1) {
                                            HTML = HTML + "  <td width=\"660\">\n" +
                                                    "<table width=\"100%\">\n" +
                                                    "   <tr>\n" +
                                                    "       <td width=\"30\">\n" +
                                                    "           <b>";
                                            HTML = HTML + csqi.getString(csqi.getColumnIndex("Label")) + ")" +
                                                    "           </b>\n" +
                                                    "       </td>\n" +
                                                    "       <td width=\"630\">\n";

                                            if (sub_question_type_total == 1) {
                                                HTML = HTML + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                            } else {
                                                HTML = HTML + RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                            }

                                            HTML = HTML + "        </td>\n";
                                            int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">W" + marks + "</td>" +
                                                    "   </tr>\n" +
                                                    "</table>\n" +
                                                    "</td>";
                                        } else {
                                            HTML = HTML + "<td width=\"660\">\n" +
                                                    "<table>\n" +
                                                    "   <tr>\n" +
                                                    "       <td width=\"30\">\n" +
                                                    "       </td>\n" +
                                                    "       <td width=\"630\">\n" +
                                                    RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType")) +
                                                    "       </td>\n";
                                            int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">Q" + marks + "</td>" +
                                                    "   </tr>\n" +
                                                    "</table>\n" +
                                                    "</td>    ";
                                        }

                                           /* int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>";*/
                                    }

                                    HTML = HTML + "</tr>" +
                                            " <tr class=\"margin_top\">\n" +
                                            "   <td width=\"30\"></td>\n" +
                                            "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                            "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";
                                    if (sub_questions_total == 1) {
                                        HTML = HTML +csqi.getString(csqi.getColumnIndex("Question"));
                                    } else {
                                        HTML = HTML + sss + ") " + csqi.getString(csqi.getColumnIndex("Question"));
                                    }

                                    HTML = HTML + "   </td>\n" +
                                            "   <td align=\"right\" width=\"30\"></td>\n" +
                                            "</tr>      ";
                                    n++;
                                    sss++;
                                }
                                while (csqi.moveToNext());
                                csqi.close();
                            }
                            if (sub_qt == 0) {
                                HTML = HTML + "<tr class=\"margin_top\">\n" +
                                        "<td width=\"30\"></td>\n" +
                                        "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                        "<td width=\"660\">" +
                                        TempPrelimTestRecordArrayList.get(i).getQuestion() +
                                        "</td>\n" +
                                        "<td align=\"right\" width=\"30\"></td>\n" +
                                        "</tr>     \n";
                            }

                            sub_qt++;
                        }

                        while (clabel.moveToNext());
                        clabel.close();
                        k++;

                    } else {
                    }
                    TempPrelimTestRecordArrayList.get(i).setQuestion(HTML + "</table>");
                    //TempPrelimTestRecordArrayList.get(i).setQuestion("test");
                }
                if (TempPrelimTestRecordArrayList.get(i).getPageNo().equals(PageNo)) {
                    prelimObj = new PrelimTestRecord();
                    prelimObj.setAnswer(TempPrelimTestRecordArrayList.get(i).getAnswer());
                    prelimObj.setExamTypePatternDetailID(TempPrelimTestRecordArrayList.get(i).getExamTypePatternDetailID());
                    prelimObj.setIsQuestion(TempPrelimTestRecordArrayList.get(i).getIsQuestion());
                    prelimObj.setPageNo(TempPrelimTestRecordArrayList.get(i).getPageNo());
                    prelimObj.setQuestionNo(TempPrelimTestRecordArrayList.get(i).getQuestionNo());
                    prelimObj.setQuestionTypeText(TempPrelimTestRecordArrayList.get(i).getQuestionTypeText());
                    prelimObj.setSubQuestionNo(TempPrelimTestRecordArrayList.get(i).getSubQuestionNo());
                    prelimObj.setQuestion(TempPrelimTestRecordArrayList.get(i).getQuestion());

                    prelimObj.setQuestionMarks(TempPrelimTestRecordArrayList.get(i).getQuestionMarks());
                    prelimObj.setQuestionTypeID(TempPrelimTestRecordArrayList.get(i).getQuestionTypeID());
                    prelimTestRecordArrayList.add(prelimObj);

                    //  stringArrayListHashMap.put(PageNo, prelimTestRecordArrayList);
                } else {

                    questionsList.add(new PrelimQuestionListModel(PageNo, prelimTestRecordArrayList));

                    PageNo = TempPrelimTestRecordArrayList.get(i).getPageNo();
                    prelimTestRecordArrayList = new ArrayList<>();
                    prelimObj = new PrelimTestRecord();


                    prelimObj.setAnswer(TempPrelimTestRecordArrayList.get(i).getAnswer());
                    prelimObj.setExamTypePatternDetailID(TempPrelimTestRecordArrayList.get(i).getExamTypePatternDetailID());
                    prelimObj.setIsQuestion(TempPrelimTestRecordArrayList.get(i).getIsQuestion());
                    prelimObj.setPageNo(TempPrelimTestRecordArrayList.get(i).getPageNo());
                    prelimObj.setQuestionNo(TempPrelimTestRecordArrayList.get(i).getQuestionNo());
                    prelimObj.setQuestionTypeText(TempPrelimTestRecordArrayList.get(i).getQuestionTypeText());
                    prelimObj.setSubQuestionNo(TempPrelimTestRecordArrayList.get(i).getSubQuestionNo());
                    prelimObj.setQuestion(TempPrelimTestRecordArrayList.get(i).getQuestion());

                    prelimObj.setQuestionMarks(TempPrelimTestRecordArrayList.get(i).getQuestionMarks());
                    prelimObj.setQuestionTypeID(TempPrelimTestRecordArrayList.get(i).getQuestionTypeID());
                    prelimTestRecordArrayList.add(prelimObj);

                    //    stringArrayListHashMap.put(PageNo, prelimTestRecordArrayList);
                }


            }
            questionsList.add(new PrelimQuestionListModel(PageNo, prelimTestRecordArrayList));
            questionsList.remove(0);
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            showProgress(false);


            context.startActivity(new Intent(context, PractiesPaperSolutionActivity.class)
                    .putExtra("questionsList", questionsList));


        }
    }

    public class sendToSetPaper extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;


        private sendToSetPaper(Context context) {
            this.context = context;
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);

        }

        @Override
        protected void onPreExecute() {
            iDSArray.clear();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {
            Cursor c = mDb.getAllRows(DBNewQuery.getSetPaperQuestionTypeID(App.practiesPaperObj.getStudentQuestionPaperID()));

            if (c != null && c.moveToFirst()) {
                do {
                    iDSArray.add(c.getString(0));

                } while (c.moveToNext());

                c.close();
            }
            App.practiesPaperObj.setStudentQuestionTypeID(Utils.convertArraytoCommaSeprated(iDSArray));

            return "";
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
//            showProgress(false);
            context.startActivity(new Intent(context, SetPaperSolutionActivity.class).putExtra("questionsList", questionsList));
        }
    }
}

