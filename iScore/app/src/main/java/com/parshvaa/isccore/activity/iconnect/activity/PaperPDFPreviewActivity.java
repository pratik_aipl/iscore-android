package com.parshvaa.isccore.activity.iconnect.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.boardpaper.pdfview.PDFView;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.BoardPaperDownload;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.FileDownloader;
import com.parshvaa.isccore.utils.Utils;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PaperPDFPreviewActivity extends BaseActivity {
    private static final String TAG = "PaperPDFPreviewActivity";
    public String QFile="", PaperName;
    public PDFView pdfview;
    public RetryPolicy retryPolicy;
    public static ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    public FloatingActionButton img_share, imgprint,imgadd;
    public File pdf;
    public ImageView img_back,img_rotation;
    public TextView tv_title;
    public PaperPDFPreviewActivity instance;
    public PaperPDFPreviewActivity Staticinstance;
    public static MyDBManager mDb;

    RelativeLayout main_view;
    public ArrayList<BoardPaperDownload> BoardpaperArray = new ArrayList<>();
    public static JsonArray jBoardPaperArray;

    String paper_id, isOnlyQuestionPaper, AnswerPaperDate,UPLOADSTARTTIME,UPLOADENDTIME,STARTTIME,ENDTIME;
    long milli,Tenmilli,Currentmillisecond,millii;
    View parentLayout;
    Date Enddate,Startdate;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_board_paper_preview);
        Utils.logUser();
         parentLayout = findViewById(android.R.id.content);

        instance = this;
        Staticinstance = this;
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        main_view = findViewById(R.id.main_view);


        paper_id = getIntent().getExtras().getString("id");
        PaperName = getIntent().getStringExtra(Constant.PAPERNAME);
        QFile = getIntent().getStringExtra(Constant.PAPERLINK);
        Log.d(TAG, "onCreate: "+QFile);
        isOnlyQuestionPaper = getIntent().getExtras().getString(Constant.isOnlyQuestionPaper);
        AnswerPaperDate = getIntent().getExtras().getString(Constant.AnswerPaperDate);

        STARTTIME = getIntent().getExtras().getString(Constant.STARTTIME);
        ENDTIME = getIntent().getExtras().getString(Constant.ENDTIME);
        img_rotation = findViewById(R.id.img_rotation);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
        try {
             Startdate = format.parse(STARTTIME);
             Enddate = format.parse(ENDTIME);
             Currentmillisecond = new Date().getTime();

            Log.d(TAG, "startmilli: "+Startdate.getTime());
            Log.d(TAG, "Endmilli: "+Enddate.getTime());
            Log.d(TAG, "currentmilli: "+Currentmillisecond);
            milli=Enddate.getTime()-Currentmillisecond;
            Tenmilli=milli-600000;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        new CountDownTimer(milli,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }
            @Override
            public void onFinish() {
                gotoBack();
            }
        }.start();

        new CountDownTimer(Tenmilli,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }
            @Override
            public void onFinish() {
                //TenMinuteAlert();
                millii=Enddate.getTime()-Currentmillisecond;
                milliseconds(millii);
            }
        }.start();

            showProgress(true, true);//Utils.showProgressDialog(instance);
            new DownloadFile().execute(QFile, paper_id);



        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Preview Paper");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        retryPolicy = new DefaultRetryPolicy();
        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);

        pdfview = findViewById(R.id.pdfView);

        img_share = findViewById(R.id.img_share);
        imgprint = findViewById(R.id.imgprint);
        imgadd = findViewById(R.id.imgadd);
        img_share.setVisibility(View.GONE);
        imgprint.setVisibility(View.GONE);
        imgadd.setVisibility(View.VISIBLE);
        imgadd.setBackgroundResource(R.drawable.addimg);
        imgadd.setOnClickListener(v -> GotoUploadImg());

        img_rotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });
    }
    public void gotoBack() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("You have completed your time limit for your test.\n All the best")
                .setPositiveButton("Yes", (dialog, which) -> {
                    onBackPressed();

                })
                .show();
    }

    public void milliseconds(long milliseconds) {
         long minutes = (milliseconds / 1000) / 60;
         long seconds = (milliseconds / 1000) % 60;
         Snackbar snackbar = Snackbar
                .make(parentLayout, "Exam over in "+ minutes+" minutes.\n Good luck !", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public void GotoUploadImg() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("Are You Sure You want To Upload Paper ?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    showProgress(true, true);//Utils.showProgressDialog(instance);
                    startActivity(new Intent(instance, UploadPaperImagesActivity.class)
                            .putExtra(Constant.PAPERID,paper_id)
                            .putExtra(Constant.TYPE,"UPLOAD"));
                    finish();


                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Void doInBackground(String... strings) {
            QFile = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            paper_id = strings[1];  // -> maven.pdf
            String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + "board-paper-file";

            File qDirectory = new File(SDCardPath);
            if (!qDirectory.exists()) qDirectory.mkdirs();

            String qLocalUrl = SDCardPath + "/" + paper_id;
            File qLocalFile = new File(qLocalUrl);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(QFile, qLocalFile, getApplicationContext());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            pdf = new File(Constant.LOCAL_IMAGE_PATH + "/board-paper-file/" + paper_id);
            if (pdf.isFile()) {
                Utils.Log("TAG", " if POST EXECUTE ::-> " + Constant.LOCAL_IMAGE_PATH + "/board-paper-file/" + paper_id);
                pdfview.fromFile(pdf).load();
                showProgress(false, true);//Utils.showProgressDialog(instance);

            } else {
                new AlertDialog.Builder(instance)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Alert")
                        .setCancelable(false)
                        .setMessage("Paper not available ")
                        .setPositiveButton("Ok", (dialog, which) -> {
                            finish();
                            dialog.dismiss();


                        })
                        .show();
                Utils.Log("TAG", " else POST EXECUTE ::-> " + Constant.LOCAL_IMAGE_PATH + "/board-paper-file/" + paper_id);

            }
        }
    }
    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    finish();
                    dialog.dismiss();
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
