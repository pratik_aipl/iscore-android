package com.parshvaa.isccore.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.net.URL;

public class LongThread implements Runnable {

    int threadNo;
    public Handler handler;
    public String imageUrl;
    public String localPath;
    public static final String TAG = "LongThread";
    public Context ct;

    public LongThread() {
    }

    public LongThread(Context ct, int threadNo, String imageUrl, String localPath, Handler handler) {
        this.threadNo = threadNo;
        this.handler = handler;
        this.imageUrl = imageUrl;
        this.ct = ct;
        this.localPath = localPath;
    }

    @Override
    public void run() {
        Log.i(TAG, "Starting Thread : " + threadNo);
        getBitmap(imageUrl);


    }


    public void sendMessage(int what, String msg) {
        Message message = handler.obtainMessage(what, msg);
        message.sendToTarget();
    }

    private void getBitmap(String path) {

        URL url = null;

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(path));
        request.setDescription("iScore - " + path);
        request.setTitle(path.substring(path.lastIndexOf("/") + 1));
        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            //request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        String dir = localPath.substring(0, localPath.lastIndexOf("/"));
        String fileName = localPath.substring(localPath.lastIndexOf("/") + 1);

        //Log.i("Dir path : ",  dir + "   : File_name --> " + fileName);
       // request.setDestinationInExternalPublicDir(dir, fileName);
        request.setDestinationUri(Uri.parse("file://"+localPath));

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) ct.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);


        sendMessage(threadNo, "Thread Completed");
        Log.i(TAG, "Thread Completed " + threadNo);


    }

  /*  private void getBitmap(String path) {

        URL url = null;
        try {
            url = new URL(path);

            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;

            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }

            out.close();
            in.close();
            byte[] response = out.toByteArray();

            FileOutputStream fos = new FileOutputStream(localPath);
            fos.write(response);
            fos.close();

            sendMessage(threadNo, "Thread Completed");
            Log.i(TAG, "Thread Completed " + threadNo);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
*/
}