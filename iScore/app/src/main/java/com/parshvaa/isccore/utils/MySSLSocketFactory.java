//package com.parshvaa.isccore.utils;
//
//import org.apache.http.conn.ssl.SSLSocketFactory;
//
//import java.io.IOException;
//import java.net.Socket;
//import java.net.UnknownHostException;
//import java.security.KeyManagementException;
//import java.security.KeyStore;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.UnrecoverableKeyException;
//import java.security.cert.CertificateException;
//import java.security.cert.X509Certificate;
//
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
//
//
//public class MySSLSocketFactory extends SSLSocketFactory {
//    SSLContext sslContext = SSLContext.getInstance("TLS");
//
//    public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
//        super(truststore);
//
//        TrustManager tm = new X509TrustManager() {
//            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//            }
//
//            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//                try {
//                    chain[0].checkValidity();
//                } catch (Exception e) {
//                    throw new CertificateException("Certificate not valid or trusted.");
//                }
//            }
//
//            public X509Certificate[] getAcceptedIssuers() {
//                return null;
//            }
//        };
//        sslContext.init(null, new TrustManager[] { tm }, null);
//        //////////////////////////////////////////////////////////////////////////////////////////////////
////        TrustManager[] tm = new TrustManager[] {
////
////                new X509TrustManager() {
////
////                    public X509Certificate[] getAcceptedIssuers() {
////
////                        X509Certificate[] myTrustedAnchors = new X509Certificate[0];
////
////                        return myTrustedAnchors;
////                    }
////
////                    @Override
////                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
////
////                    }
////
////                    @Override
////                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
////                        if(chain == null || chain.length == 0)throw new IllegalArgumentException("Certificate is null or empty");
////                        if(authType == null || authType.length() == 0) throw new IllegalArgumentException("Authtype is null or empty");
////                        if(!authType.equalsIgnoreCase("ECDHE_RSA") &&
////                                !authType.equalsIgnoreCase("ECDHE_ECDSA") &&
////                                !authType.equalsIgnoreCase("RSA") &&
////                                !authType.equalsIgnoreCase("ECDSA")) throw new CertificateException("Certificate is not trust");
////                        try {
////                            chain[0].checkValidity();
////                        } catch (Exception e) {
////                            throw new CertificateException("Certificate is not valid or trusted");
////                        }
////                    }
////                }
////        };
////        sslContext.init(null, tm , null);
//        //////////////////////////////////////////////////////////////////////////////////////////////////
//    }
//
//    @Override
//    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
//        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
//    }
//
//    @Override
//    public Socket createSocket() throws IOException {
//        return sslContext.getSocketFactory().createSocket();
//    }
//
//}
