package com.parshvaa.isccore.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parshvaa.isccore.model.Tour;


/**
 * Created by admin on 3/22/2017.
 */
public class MyViewPagerAdapter extends PagerAdapter {

    private Context mContext;

    public MyViewPagerAdapter(Context context) {

        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        Tour modelObject = Tour.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return Tour.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Tour customPagerEnum = Tour.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

}