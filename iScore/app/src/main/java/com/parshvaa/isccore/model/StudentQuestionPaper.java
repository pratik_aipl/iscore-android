package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by Karan - Empiere on 3/20/2017.
 */

@JsonObject
public class StudentQuestionPaper implements Serializable {
    @JsonField
    public int StudentQuestionPaperID = -1;
    @JsonField
    public String ExamTypePatternID = "";
    @JsonField
    public String StudentID = "";
    @JsonField
    public String PaperTypeID = "";
    @JsonField
    public String SubjectID = "";
    @JsonField
    public String TotalMarks = "";
    @JsonField
    public String Duration = "";
    @JsonField
    public String CreatedBy = "";
    @JsonField
    public String CreatedOn = "";
    @JsonField
    public String ModifiedBy = "";
    @JsonField
    public String ModifiedOn = "";

    public String getPaperTypeID() {
        return PaperTypeID;
    }

    public void setPaperTypeID(String paperTypeID) {
        PaperTypeID = paperTypeID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public int getStudentQuestionPaperID() {
        return StudentQuestionPaperID;
    }

    public void setStudentQuestionPaperID(int studentQuestionPaperID) {
        StudentQuestionPaperID = studentQuestionPaperID;
    }

    public String getExamTypePatternID() {
        return ExamTypePatternID;
    }

    public void setExamTypePatternID(String examTypePatternID) {
        ExamTypePatternID = examTypePatternID;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public ArrayList<StudentQuestionPaperDetail> getDetailArray() {
        return detailArray;
    }

    public void setDetailArray(ArrayList<StudentQuestionPaperDetail> detailArray) {
        this.detailArray = detailArray;
    }

    public ArrayList<StudentQuestionPaperChapter> getChapterArray() {
        return chapterArray;
    }

    public void setChapterArray(ArrayList<StudentQuestionPaperChapter> chapterArray) {
        this.chapterArray = chapterArray;
    }

    public ArrayList<StudentSetPaperQuestionType> getQueTypeArray() {
        return queTypeArray;
    }

    public void setQueTypeArray(ArrayList<StudentSetPaperQuestionType> queTypeArray) {
        this.queTypeArray = queTypeArray;
    }

    @JsonField(name = "chapterArray")
    public ArrayList<StudentQuestionPaperChapter> chapterArray=new ArrayList<>();
    @JsonField(name = "detailArray")
    public ArrayList<StudentQuestionPaperDetail> detailArray=new ArrayList<>();
    @JsonField
    public ArrayList<StudentSetPaperQuestionType> queTypeArray=new ArrayList<>();


}
