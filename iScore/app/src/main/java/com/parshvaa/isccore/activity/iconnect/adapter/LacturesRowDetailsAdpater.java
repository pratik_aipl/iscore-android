package com.parshvaa.isccore.activity.iconnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;


import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.iconnect.AddPaperStartTime;
import com.parshvaa.isccore.activity.iconnect.LaturesDataModel;
import com.parshvaa.isccore.custominterface.GenerateOnlineClassURL;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.utils.RobotoBoldTextview;
import com.parshvaa.isccore.utils.RobotoTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LacturesRowDetailsAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private static final String TAG = "OrderDetailsAdpater";
    Context context;
    List<LaturesDataModel> dataList;
    String tittle;
    GenerateOnlineClassURL generateOnlineClassURL;
    AddPaperStartTime addPaperStartTime;

    public LacturesRowDetailsAdpater(Context context, List<LaturesDataModel> dataLists) {
        this.context = context;
        this.dataList = dataLists;
        this.generateOnlineClassURL = (GenerateOnlineClassURL) context;
        this.addPaperStartTime = (AddPaperStartTime) context;

        Log.d(TAG, "DashboardRowDetailsAdpater: "+dataLists.size());
        Log.d(TAG, "ttttt: "+tittle);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_raw_data, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        LaturesDataModel rawModel = dataList.get(position);


            holder.name.setText(rawModel.getTeacherName());

            holder.tvBoard.setText(rawModel.getBoardName());
            holder.tvMedium.setText(rawModel.getMediumName());

            holder.tvStdanderd.setText(rawModel.getStandardName());
            holder.tvSubject.setText(rawModel.getSubjectName());

            holder.tv_class_name.setText(rawModel.getTitle());
            holder.tvSubmissionDt.setText(rawModel.getPaperDate());

            holder.tv_time.setText(rawModel.getTime());

            int totalMinutesInt = Integer.parseInt(rawModel.getDuration());
            int hours = totalMinutesInt / 60;
            int hoursToDisplay = hours;
            if (hours > 12) {
                hoursToDisplay = hoursToDisplay - 12;
            }
            int minutesToDisplay = totalMinutesInt - (hours * 60);
            String minToDisplay = null;
            if(minutesToDisplay == 0 ) minToDisplay = "00";
            else if( minutesToDisplay < 10 ) minToDisplay = "00" + minutesToDisplay ;
            else minToDisplay = "" + minutesToDisplay ;
            String displayValue = hoursToDisplay + ":" + minToDisplay;

            holder.tv_hours.setText(displayValue);



            if(rawModel.getIsLive().equalsIgnoreCase("1")){
                holder.img_right.setVisibility(View.VISIBLE);
            }else{
                holder.img_right.setVisibility(View.GONE);
            }


        DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa", Locale.ENGLISH);
        try {
            Date Startdate = format.parse(rawModel.getStartTime());
            Date Enddate = format.parse(rawModel.getEndTime());

            long Currentmillisecond = new Date().getTime();

            Log.d(TAG, "startmilli: "+Startdate.getTime());
            Log.d(TAG, "Endmilli: "+Enddate.getTime());
            Log.d(TAG, "currentmilli: "+Currentmillisecond);

            if(Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
                animation.setDuration(1000); //1 second duration for each animation cycle
                animation.setInterpolator(new LinearInterpolator());
                animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
                animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
                holder.img_right.startAnimation(animation); //to start animation
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.img_right.setOnClickListener(view -> {
            DateFormat dff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //   DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            String formattedDate= dff.format(new Date());

            addPaperStartTime.onPaperStartTime( MainUser.getStudentId(),rawModel.getClassLessonPlannerID(),"",formattedDate);

            Intent httpIntent = new Intent(Intent.ACTION_VIEW);
            httpIntent.setData(Uri.parse(""+rawModel.getJoinURL()));
            context.startActivity(httpIntent);

          /*  context.startActivity(new Intent(context, OnlineClassActivity.class)
                    .putExtra("CLASSLINK", rawModel.getJoinURL()));*/
        });

     /*   holder.img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateOnlineClassURL.ongnrtURL(rawModel.getLiveClassID(),"0",rawModel.getTitle(),rawModel.getTitle());
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.tv_class_name)
        TextView tv_class_name;
        @BindView(R.id.img_right)
        ImageView img_right;
        @BindView(R.id.tv_board)
        RobotoBoldTextview tvBoard;
        @BindView(R.id.tv_medium)
        RobotoTextView tvMedium;
        @BindView(R.id.tv_stdanderd)
        RobotoBoldTextview tvStdanderd;
        @BindView(R.id.tv_subject)
        RobotoTextView tvSubject;
        @BindView(R.id.tv_submission_dt)
        RobotoBoldTextview tvSubmissionDt;
        @BindView(R.id.tv_time)
        RobotoBoldTextview tv_time;
        @BindView(R.id.tv_hours)
        RobotoBoldTextview tv_hours;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}