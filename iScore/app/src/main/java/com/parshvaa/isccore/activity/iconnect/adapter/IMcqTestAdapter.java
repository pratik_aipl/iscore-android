package com.parshvaa.isccore.activity.iconnect.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.iconnect.activity.CctTestActivity;
import com.parshvaa.isccore.activity.iconnect.activity.CctTestResultActivity;
import com.parshvaa.isccore.activity.iconnect.EMcqPaper;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */

public class IMcqTestAdapter extends RecyclerView.Adapter<IMcqTestAdapter.MyViewHolder> {
    private static final String TAG = "IMcqTestAdapter";
    private List<EMcqPaper> paperTypeList;
    public Context context;
    public int SubjectID;
    public String SubjectName;
    String isType;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_month, tv_expdate, tv_take_test, tv_date,lbl_exp,tv_start_date,tv_end_date;
        LinearLayout lin_time;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = view.findViewById(R.id.tv_subject_name);
            tv_month = view.findViewById(R.id.tv_month);
            tv_expdate = view.findViewById(R.id.tv_expdate);
            tv_date = view.findViewById(R.id.tv_date);
            tv_take_test = view.findViewById(R.id.tv_take_test);

            lbl_exp = view.findViewById(R.id.lbl_exp);
            tv_start_date = view.findViewById(R.id.tv_start_date);
            tv_end_date = view.findViewById(R.id.tv_end_date);

            lin_time = view.findViewById(R.id.lin_time);
        }
    }


    public IMcqTestAdapter(List<EMcqPaper> paperTypeList, Context context, String isType) {
        this.paperTypeList = paperTypeList;
        this.context = context;
        this.isType = isType;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_serach_imcq_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        EMcqPaper cons = paperTypeList.get(position);
        holder.tv_subject_name.setText(cons.getSubjectName());

        Log.d(TAG, "onBindViewHolder: " + cons.getModifiedOn());
        Log.d(TAG, "onBindViewHolder: " + cons.getExpiryDate());

        String startDate = "";
        String endDate = "";

        try {
            startDate = cons.getModifiedOn();
            startDate = startDate.substring(0, 10);
            endDate = cons.getExpiryDate();
            endDate = endDate.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startDate = Utils.changeDateToMMDDYYYY(startDate);
        holder.tv_month.setText(startDate.substring(3, 6));
        holder.tv_date.setText(startDate.substring(0, 2));
        holder.tv_expdate.setText(Utils.changeDateToMMDDYYYY(endDate));

        if (isType.equalsIgnoreCase("1")) {

            holder.lbl_exp.setText("Paper Date :");
            holder.tv_take_test.setText("TAKE TEST");
            holder.tv_take_test.setTextColor(context.getResources().getColor(R.color.colorPrimary));

            if(cons.getPublishType().equalsIgnoreCase("1")){
                holder.lin_time.setVisibility(View.VISIBLE);
                holder.tv_start_date.setText("Start Date : "+cons.getStartTime());
                holder.tv_end_date.setText("End Date   : "+cons.getEndTime());
            }


            holder.tv_take_test.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(cons.getPublishType().equalsIgnoreCase("1")){
                        DateFormat formatt = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
                        try {
                            Date Startdate = formatt.parse(cons.getStartTime());
                            Date Enddate = formatt.parse(cons.getEndTime());

                            long Currentmillisecond = new Date().getTime();

                            Date c = Calendar.getInstance().getTime();
                            System.out.println("Current time => " + c);
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            String CurrentDate = df.format(c);

                            Log.d(TAG, "startmilli: "+Startdate.getTime());
                            Log.d(TAG, "Endmilli: "+Enddate.getTime());
                            Log.d(TAG, "currentmilli: "+Currentmillisecond);

                            if(Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                                Intent intent = new Intent(context, CctTestActivity.class);
                                intent.putExtra("id", cons.getPaperID());
                                context.startActivity(intent);
                            }else{
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                                builder1.setMessage("Alert! To view this Not In a Time");
                                builder1.setCancelable(true);
                                builder1.setNegativeButton("OK", (dialog, i) -> {
                                    dialog.cancel();
                                });
                                AlertDialog alert11 = builder1.create();
                                alert11.show();
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }else{
                        Intent intent = new Intent(context, CctTestActivity.class);
                        intent.putExtra("id", cons.getPaperID());
                        context.startActivity(intent);
                    }




                }
            });

        }
        else {
            holder.tv_take_test.setText("VIEW REPORT");
            holder.tv_take_test.setTextColor(context.getResources().getColor(R.color.colorRed));
            holder.tv_take_test.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cons.getTakenTest().equalsIgnoreCase("0")) {
                        context.startActivity(new Intent(context, CctTestResultActivity.class)
                                .putExtra("type", "1")
                                .putExtra("taken_id", cons.getPaperID())
                                .putExtra("subject_name", cons.getSubjectName()));
                    } else {
                        new androidx.appcompat.app.AlertDialog.Builder(context)
                                .setTitle("")
                                .setMessage("You Missed Exam.")
                                .setPositiveButton(android.R.string.yes, (dialog, which) -> dialog.dismiss())
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
            });

        }



    }


    @Override
    public int getItemCount() {
        return paperTypeList.size();
    }


}


