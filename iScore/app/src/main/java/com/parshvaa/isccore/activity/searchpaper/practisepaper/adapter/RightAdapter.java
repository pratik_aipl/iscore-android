package com.parshvaa.isccore.activity.searchpaper.practisepaper.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.Paper_type;
import com.parshvaa.isccore.R;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 1/12/2018.
 */

public class RightAdapter extends RecyclerView.Adapter<RightAdapter.MyViewHolder> {
    ArrayList<Paper_type> data = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    ArrayList<String> checked_items = new ArrayList<>();

    public RightAdapter(ArrayList<Paper_type> data, Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_checkbox_sub_raw, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Paper_type current = data.get(position);
        holder.chk_group.setText(current.getPaperTypeName());

        holder.chk_group.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    compoundButton.setChecked(true);
                    App.checked_paper_type.add(current.getPaperTypeID());

                } else {
                    compoundButton.setChecked(false);
                    App.checked_paper_type.remove(current.getPaperTypeID());
                }
            }
        });

    }

    public ArrayList<String> checkdItem() {
        return checked_items;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox chk_group;
        View itemView;

        public MyViewHolder(View itemView) {
            super(itemView);
            chk_group = itemView.findViewById(R.id.chk_group);
            this.itemView = itemView;

        }
    }
}


