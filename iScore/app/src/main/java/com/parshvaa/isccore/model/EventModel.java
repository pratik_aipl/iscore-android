package com.parshvaa.isccore.model;

import java.io.Serializable;
import java.util.List;

public class EventModel implements Serializable{

    public String EventID;
    public String Title;
    public String BannerImage;
    public String Duration;
    public String LimitSeat;
    public String WatchOn;
    public String Language;
    public String Age;
    public String Text;
    public String ArtistID;
    public String Instruction;
    public String About;
    public String Terms;
    public String StartDate;
    public String EndDate;
    public String CourseType;
    public String CourseTitle;
    public String StartDateTime;
    public String EndDateTime;
    public String CourseDuration;
    public String ActualPrice;
    public String Price;
    public String ClassHolderShare;
    public String OntimeLink;
    public String isDemo;
    public String DemoTitle;
    public String DemoStartDateTime;
    public String DemoEndDateTime;
    public String DemoCourseDuration;
    public String DemoLink;
    public String DemoPrice;
    public String DemoClassHolderShare;
    public String isActive;
    public String CreatedBy;
    public String CreatedOn;
    public String ModifiedBy;
    public String ModifiedOn;
    public String isEventDemo;
    public String isRegister;
    public String isSubscribe;
    public String DemoImage;
    public String ArtistName;
    public String Type;
    List<ArtiseModel> Artists;
    List<CourseModel> CourseDetails;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getArtistName() {
        return ArtistName;
    }

    public void setArtistName(String artistName) {
        ArtistName = artistName;
    }

    public List<CourseModel> getCourseDetails() {
        return CourseDetails;
    }

    public void setCourseDetails(List<CourseModel> courseDetails) {
        CourseDetails = courseDetails;
    }

    public List<ArtiseModel> getArtists() {
        return Artists;
    }

    public void setArtists(List<ArtiseModel> artists) {
        Artists = artists;
    }

    public String getDemoImage() {
        return DemoImage;
    }

    public void setDemoImage(String demoImage) {
        DemoImage = demoImage;
    }

    public String getIsEventDemo() {
        return isEventDemo;
    }

    public void setIsEventDemo(String isEventDemo) {
        this.isEventDemo = isEventDemo;
    }

    public String getIsRegister() {
        return isRegister;
    }

    public void setIsRegister(String isRegister) {
        this.isRegister = isRegister;
    }

    public String getIsSubscribe() {
        return isSubscribe;
    }

    public void setIsSubscribe(String isSubscribe) {
        this.isSubscribe = isSubscribe;
    }

    public String getEventID() {
        return EventID;
    }

    public void setEventID(String eventID) {
        EventID = eventID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getBannerImage() {
        return BannerImage;
    }

    public void setBannerImage(String bannerImage) {
        BannerImage = bannerImage;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getLimitSeat() {
        return LimitSeat;
    }

    public void setLimitSeat(String limitSeat) {
        LimitSeat = limitSeat;
    }

    public String getWatchOn() {
        return WatchOn;
    }

    public void setWatchOn(String watchOn) {
        WatchOn = watchOn;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getArtistID() {
        return ArtistID;
    }

    public void setArtistID(String artistID) {
        ArtistID = artistID;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }

    public String getAbout() {
        return About;
    }

    public void setAbout(String about) {
        About = about;
    }

    public String getTerms() {
        return Terms;
    }

    public void setTerms(String terms) {
        Terms = terms;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getCourseType() {
        return CourseType;
    }

    public void setCourseType(String courseType) {
        CourseType = courseType;
    }

    public String getCourseTitle() {
        return CourseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        CourseTitle = courseTitle;
    }

    public String getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }

    public String getCourseDuration() {
        return CourseDuration;
    }

    public void setCourseDuration(String courseDuration) {
        CourseDuration = courseDuration;
    }

    public String getActualPrice() {
        return ActualPrice;
    }

    public void setActualPrice(String actualPrice) {
        ActualPrice = actualPrice;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getClassHolderShare() {
        return ClassHolderShare;
    }

    public void setClassHolderShare(String classHolderShare) {
        ClassHolderShare = classHolderShare;
    }

    public String getOntimeLink() {
        return OntimeLink;
    }

    public void setOntimeLink(String ontimeLink) {
        OntimeLink = ontimeLink;
    }

    public String getIsDemo() {
        return isDemo;
    }

    public void setIsDemo(String isDemo) {
        this.isDemo = isDemo;
    }

    public String getDemoTitle() {
        return DemoTitle;
    }

    public void setDemoTitle(String demoTitle) {
        DemoTitle = demoTitle;
    }

    public String getDemoStartDateTime() {
        return DemoStartDateTime;
    }

    public void setDemoStartDateTime(String demoStartDateTime) {
        DemoStartDateTime = demoStartDateTime;
    }

    public String getDemoEndDateTime() {
        return DemoEndDateTime;
    }

    public void setDemoEndDateTime(String demoEndDateTime) {
        DemoEndDateTime = demoEndDateTime;
    }

    public String getDemoCourseDuration() {
        return DemoCourseDuration;
    }

    public void setDemoCourseDuration(String demoCourseDuration) {
        DemoCourseDuration = demoCourseDuration;
    }

    public String getDemoLink() {
        return DemoLink;
    }

    public void setDemoLink(String demoLink) {
        DemoLink = demoLink;
    }

    public String getDemoPrice() {
        return DemoPrice;
    }

    public void setDemoPrice(String demoPrice) {
        DemoPrice = demoPrice;
    }

    public String getDemoClassHolderShare() {
        return DemoClassHolderShare;
    }

    public void setDemoClassHolderShare(String demoClassHolderShare) {
        DemoClassHolderShare = demoClassHolderShare;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
