package com.parshvaa.isccore.activity.iconnect.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.OverScroller;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.activity.iconnect.fragment.ICorrectSummaryFragment;
import com.parshvaa.isccore.activity.iconnect.fragment.IIncorrectFragment;
import com.parshvaa.isccore.activity.iconnect.fragment.INotAppearedFragment;
import com.parshvaa.isccore.model.EvaluterMcqTestMain;
import com.parshvaa.isccore.model.EvaluterQuestionOption;
import com.parshvaa.isccore.model.TempMcqTestHdr;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.CacheFragmentStatePagerAdapter;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.observscroll.Scrollable;
import com.parshvaa.isccore.observscroll.SlidingTabLayout;
import com.parshvaa.isccore.observscroll.TouchInterceptionFrameLayout;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;
import com.nineoldandroids.view.ViewHelper;

import java.util.ArrayList;

public class CctSummaryActivity extends BaseActivity {
    public CctSummaryActivity instance;
    public TextView tv_non_attempted, tv_incorrect, tv_correct;
    ImageView img_back,img_home;

    private static final float MAX_TEXT_SCALE_DELTA = 0.3f;
    private static final int INVALID_POINTER = -1;

    private View mImageView;
    private RelativeLayout mOverlayView;
    private TouchInterceptionFrameLayout mInterceptionLayout;
    private ViewPager mPager;
    private VelocityTracker mVelocityTracker;
    private OverScroller mScroller;
    private float mBaseTranslationY;
    private int mMaximumVelocity;
    private int mActivePointerId = INVALID_POINTER;
    private int mSlop;
    private int mFlexibleSpaceHeight;
    private int mTabHeight,headerId;
    private boolean mScrolled;
    private NavigationAdapter mPagerAdapter;
    public TempMcqTestHdr tempHeader;
    public static ArrayList<EvaluterMcqTestMain> rightAnsArray = new ArrayList<>();
    public static ArrayList<EvaluterMcqTestMain> nonAttemptAnsArray = new ArrayList<>();
    public static ArrayList<EvaluterMcqTestMain> incorectAnsArray = new ArrayList<>();

    public EvaluterMcqTestMain evaMcqTestMain = new EvaluterMcqTestMain();
    public static ArrayList<EvaluterMcqTestMain> EvalMainArray = new ArrayList<>();
    public EvaluterQuestionOption options = new EvaluterQuestionOption();
    public String start = "", id = "", subject_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_cct_summary);
        Utils.logUser();
        instance = this;
        showProgress(true,true);//Utils.showProgressDialog(instance);

        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ViewCompat.setElevation(findViewById(R.id.header), getResources().getDimension(R.dimen._4sdp));
        mPagerAdapter = new NavigationAdapter(getSupportFragmentManager());
        mPager = findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mImageView = findViewById(R.id.image);
        mOverlayView = findViewById(R.id.overlay);

        // Padding for ViewPager must be set outside the ViewPager itself
        // because with padding, EdgeEffect of ViewPager become strange.
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mTabHeight = getResources().getDimensionPixelSize(R.dimen._50sdp);
        findViewById(R.id.pager_wrapper).setPadding(0, mFlexibleSpaceHeight, 0, 0);
        id = getIntent().getExtras().getString("temp");
        rightAnsArray = (ArrayList<EvaluterMcqTestMain>) getIntent().getExtras().getSerializable("rightAnsArray");
        incorectAnsArray = (ArrayList<EvaluterMcqTestMain>) getIntent().getExtras().getSerializable("incorectAnsArray");
        nonAttemptAnsArray = (ArrayList<EvaluterMcqTestMain>) getIntent().getExtras().getSerializable("nonAttemptAnsArray");

        img_back = findViewById(R.id.img_back);
        img_home = findViewById(R.id.img_home);
        tv_non_attempted = findViewById(R.id.tv_non_attempted);
        tv_incorrect = findViewById(R.id.tv_incorrect);
        tv_correct = findViewById(R.id.tv_correct);

        tv_correct.setText("   "+(rightAnsArray.size())+" Correct");
        tv_incorrect.setText("   "+(incorectAnsArray.size())+" Incorrect");
        tv_non_attempted.setText("   "+(nonAttemptAnsArray.size())+" Not answerred");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });

        SlidingTabLayout slidingTabLayout = findViewById(R.id.sliding_tabs);
        slidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.wrong));
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(mPager);
        ((FrameLayout.LayoutParams) slidingTabLayout.getLayoutParams()).topMargin = mFlexibleSpaceHeight - mTabHeight;

        ViewConfiguration vc = ViewConfiguration.get(this);
        mSlop = vc.getScaledTouchSlop();
        mMaximumVelocity = vc.getScaledMaximumFlingVelocity();
        mInterceptionLayout = findViewById(R.id.container);
        // mInterceptionLayout.setScrollInterceptionListener(mInterceptionListener);
        // mScroller = new OverScroller(getApplicationContext());
       /* ScrollUtils.addOnGlobalLayoutListener(mInterceptionLayout, new Runnable() {
            @Override
            public void run() {
                // Extra space is required to move mInterceptionLayout when it's scrolled.
                // It's better to adjust its height when it's laid out
                // than to adjust the height when scroll events (onMoveMotionEvent) occur
                // because it causes lagging.
                // See #87: https://github.com/ksoichiro/Android-ObservableScrollView/issues/87
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mInterceptionLayout.getLayoutParams();
                lp.height = getScreenHeight() + mFlexibleSpaceHeight- mTabHeight;
                mInterceptionLayout.requestLayout();

                updateFlexibleSpace();
            }
        });*/
        showProgress(false, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)
                .setMessage("Are you sure you want to open dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivity(new Intent(instance, DashBoardActivity.class));
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
    private TouchInterceptionFrameLayout.TouchInterceptionListener mInterceptionListener = new TouchInterceptionFrameLayout.TouchInterceptionListener() {
        @Override
        public boolean shouldInterceptTouchEvent(MotionEvent ev, boolean moving, float diffX, float diffY) {
            if (!mScrolled && mSlop < Math.abs(diffX) && Math.abs(diffY) < Math.abs(diffX)) {
                // Horizontal scroll is maybe handled by ViewPager
                return false;
            }

            Scrollable scrollable = null;
            if (scrollable == null) {
                mScrolled = false;
                return false;
            }

            // If interceptionLayout can move, it should intercept.
            // And once it begins to move, horizontal scroll shouldn't work any longer.
            int flexibleSpace = mFlexibleSpaceHeight - mTabHeight;
            int translationY = (int) ViewHelper.getTranslationY(mInterceptionLayout);
            boolean scrollingUp = 0 < diffY;
            boolean scrollingDown = diffY < 0;
            if (scrollingUp) {
                if (translationY < 0) {
                    mScrolled = true;
                    return true;
                }
            } else if (scrollingDown) {
                if (-flexibleSpace < translationY) {
                    mScrolled = true;
                    return true;
                }
            }
            mScrolled = false;
            return false;
        }

        @Override
        public void onDownMotionEvent(MotionEvent ev) {
            mActivePointerId = ev.getPointerId(0);
            mScroller.forceFinished(true);
            if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
            } else {
                mVelocityTracker.clear();
            }
            mBaseTranslationY = ViewHelper.getTranslationY(mInterceptionLayout);
            mVelocityTracker.addMovement(ev);
        }

        @Override
        public void onMoveMotionEvent(MotionEvent ev, float diffX, float diffY) {
            int flexibleSpace = mFlexibleSpaceHeight - mTabHeight;
            float translationY = ScrollUtils.getFloat(ViewHelper.getTranslationY(mInterceptionLayout) + diffY, -flexibleSpace, 0);
            MotionEvent e = MotionEvent.obtainNoHistory(ev);
            e.offsetLocation(0, translationY - mBaseTranslationY);
            mVelocityTracker.addMovement(e);
            updateFlexibleSpace(translationY);
        }

        @Override
        public void onUpOrCancelMotionEvent(MotionEvent ev) {
            mScrolled = false;
            mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
            int velocityY = (int) mVelocityTracker.getYVelocity(mActivePointerId);
            mActivePointerId = INVALID_POINTER;
            mScroller.forceFinished(true);
            int baseTranslationY = (int) ViewHelper.getTranslationY(mInterceptionLayout);

            int minY = -(mFlexibleSpaceHeight - mTabHeight);
            int maxY = 0;
            mScroller.fling(0, baseTranslationY, 0, velocityY, 0, 0, minY, maxY);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    updateLayout();
                }
            });
        }
    };

    private void updateLayout() {
        boolean needsUpdate = false;
        float translationY = 0;
        if (mScroller.computeScrollOffset()) {
            translationY = mScroller.getCurrY();
            int flexibleSpace = mFlexibleSpaceHeight - mTabHeight;
            if (-flexibleSpace <= translationY && translationY <= 0) {
                needsUpdate = true;
            } else if (translationY < -flexibleSpace) {
                translationY = -flexibleSpace;
                needsUpdate = true;
            } else if (0 < translationY) {
                translationY = 0;
                needsUpdate = true;
            }
        }

        if (needsUpdate) {
            updateFlexibleSpace(translationY);

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    updateLayout();
                }
            });
        }
    }

    private Scrollable getCurrentScrollable() {
        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            return null;
        }
        View view = fragment.getView();
        if (view == null) {
            return null;
        }
        return (Scrollable) view.findViewById(R.id.scroll);
    }

    private void updateFlexibleSpace() {
        updateFlexibleSpace(ViewHelper.getTranslationY(mInterceptionLayout));
    }

    private void updateFlexibleSpace(float translationY) {
        ViewHelper.setTranslationY(mInterceptionLayout, translationY);
        int minOverlayTransitionY = getActionBarSize() - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-translationY / 2, minOverlayTransitionY, 0));

        // Change alpha of overlay
        float flexibleRange = mFlexibleSpaceHeight - getActionBarSize();
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat(-translationY / flexibleRange, 0, 1));

        // Scale title text
        float scale = 1 + ScrollUtils.getFloat((flexibleRange + translationY - mTabHeight) / flexibleRange, 0, MAX_TEXT_SCALE_DELTA);
        setPivotXToTitle();

    }

    private Fragment getCurrentFragment() {
        return mPagerAdapter.getItemAt(mPager.getCurrentItem());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setPivotXToTitle() {
        Configuration config = getResources().getConfiguration();
        if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT
                && config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            // ViewHelper.setPivotX(mTitleView, findViewById(android.R.id.content).getWidth());
        } else {
            //  ViewHelper.setPivotX(mTitleView, 0);
        }
    }



    private static class NavigationAdapter extends CacheFragmentStatePagerAdapter {

        private static final String[] TITLES = new String[]{"Correct", "Incorrect", "Not Appeared"};


        public NavigationAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        protected Fragment createItem(int position) {
            Fragment f;
            final int pattern = position % 5;
            switch (pattern) {

                case 0: {
                    f = new ICorrectSummaryFragment();
                    break;
                }
                case 1: {
                    f = new IIncorrectFragment();
                    break;
                }
                case 2: {
                    f = new INotAppearedFragment();
                    break;
                }
                default: {
                    f = new ICorrectSummaryFragment();
                    break;
                }

            }
            return f;
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }


    }


}
