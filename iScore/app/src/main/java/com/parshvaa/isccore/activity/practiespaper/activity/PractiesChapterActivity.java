package com.parshvaa.isccore.activity.practiespaper.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.nineoldandroids.view.ViewHelper;
import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.practiespaper.activity.setpaper.SetAskQueActivity;
import com.parshvaa.isccore.activity.practiespaper.adapter.GenChapterExpandableAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.ChapterInterface;
import com.parshvaa.isccore.custominterface.OnTaskCompleted;
import com.parshvaa.isccore.model.Chapter;
import com.parshvaa.isccore.model.ClassQuestionPaperSubQuestion;
import com.parshvaa.isccore.model.ExamTypePatternDetail;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.PrelimTestRecord;
import com.parshvaa.isccore.model.StudentQuestionPaper;
import com.parshvaa.isccore.model.StudentQuestionPaperChapter;
import com.parshvaa.isccore.model.StudentQuestionPaperDetail;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.ObservableExpandableListView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.MasterQuestionWithSubQue;
import com.parshvaa.isccore.tempmodel.PrelimQuestionListModel;
import com.parshvaa.isccore.tempmodel.SubQueKey;
import com.parshvaa.isccore.tempmodel.SubQueLabel;
import com.parshvaa.isccore.db.DBConstant;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.RomanNumber;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PractiesChapterActivity extends BaseActivity implements ObservableScrollViewCallbacks, ChapterInterface, OnTaskCompleted {

    public PractiesChapterActivity instance;
    private Toolbar mToolbar;
    private View mFlexibleSpaceView, mListBackgroundView, mOverlayView, mImageView;
    private int mActionBarSize, mFlexibleSpaceImageHeight;
    public ImageView img_back, img_sub;
    public TextView mTitleView, tv_subName, tv_totalAttempts;
    public Chapter chapterObj;
    public Button btn_next;
    public CheckBox cbSelectAll;
    public ObservableExpandableListView expChapter;
    public GenChapterExpandableAdapter chapterAdapter;
    public Subject subObj;
    public ArrayList<Chapter> chapterArray = new ArrayList<>();

    public String quesIds = "", chapterIds = "";
    public boolean checkcheckPattenId = false, onClick = false;
    public boolean checkPatternDetail = false;
    public ExamTypePatternDetail examTypePatternDetail;
    public String PageNo = "";

    public int count = 0;
    public long StudentQuestionPaperID = -1, DetailID = -1;
    public PrelimTestRecord readyObj;
    public ArrayList<PrelimTestRecord> TempReadyTestRecordArrayList = new ArrayList<>();

    public ArrayList<PrelimQuestionListModel> ReadyquestionsList = new ArrayList<>();
    public ClassQuestionPaperSubQuestion subQuesObj;
    public SubQueLabel subQuesLabelObj;
    public SubQueKey subQueskeyObj;

    public ArrayList<ClassQuestionPaperSubQuestion> subQuesArray = new ArrayList<>();
    public ArrayList<SubQueLabel> subQuesLabelArray = new ArrayList<>();
    public int sub_questions_total, sub_question_type_total, n = 1, sq = 0, sub_qt = 0, k = 1, SubQuestionTypeID = -1, sss = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practies_chapter);
        btn_next = findViewById(R.id.btn_next);
        instance = this;
        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mActionBarSize = getActionBarSize();

        expChapter = findViewById(R.id.expChapter);
        mToolbar = findViewById(R.id.toolbar);
        mOverlayView = findViewById(R.id.overlay);
        mImageView = findViewById(R.id.image);
        mListBackgroundView = findViewById(R.id.list_background);
        expChapter.setScrollViewCallbacks(this);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        img_back = findViewById(R.id.img_back);
        img_sub = findViewById(R.id.img_sub);
        mTitleView = findViewById(R.id.tv_title);
        tv_subName = findViewById(R.id.tv_subName);
        tv_totalAttempts = findViewById(R.id.tv_totalAttempts);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        File image = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + App.practiesPaperObj.getSubjectIcon());
        Utils.Log("Image Path :::: ", image.getPath());
        if (image.exists()) {
            Utils.setImage(this,image,img_sub,R.drawable.sub_english);
        }
        tv_subName.setText(App.practiesPaperObj.getSubjectName());
        tv_totalAttempts.setText("Total Test " + App.practiesPaperObj.getTotalTest());
        mTitleView.setText("Select Chapter");
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chapterAdapter.getSelectedChaptersCount() == 0) {
                    Utils.showToast("Kindly select minimum 1 chapter.", instance);
                    return;
                } else {
                    if (!onClick) {
                        onClick = true;
                        Utils.Log("TAG", "CH ID :-> " + chapterAdapter.getSelectedChapters());
                        App.practiesPaperObj.setChapterID(chapterAdapter.getSelectedChapters());
                        if (getIntent().hasExtra("ReadyPaper")) {
                            new getReadyTest(instance).execute();
                        } else if (getIntent().hasExtra("SetPaper")) {
                            startActivity(new Intent(instance, SetAskQueActivity.class));
                        }
                    }
                }
            }
        });
        addHeaderInList();
        addFooterInList();
        getChapter();

        chapterAdapter = new GenChapterExpandableAdapter(instance, expChapter, chapterArray, cbSelectAll);
        expChapter.setAdapter(chapterAdapter);
        expChapter.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        expChapter.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });

    }

    public void addFooterInList() {
        View paddingView = new View(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        paddingView = inflater.inflate(R.layout.expand_footer, null);

        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen._50sdp));
        paddingView.setLayoutParams(lp);
        // This is required to disable header's list selector effect
        paddingView.setClickable(true);
        expChapter.addFooterView(paddingView);

    }

    public void getChapter() {
        try {
            chapterArray = (ArrayList) new CustomDatabaseQuery(this, new Chapter())
                    .execute(new String[]{DBNewQuery.getChapterPractiesPaper(App.practiesPaperObj.getSubjectID())}).get();
        } catch (Exception e) {
            e.printStackTrace();
        }


        for (Chapter main : chapterArray) {
            try {
                main.subChapters = (ArrayList) new CustomDatabaseQuery(this, new Chapter())
                        .execute(new String[]{DBNewQuery.getSubChapterPractiesPaper(main.getChapterID())}).get();
                if (main.subChapters.size() < 1) {
                    main.subChapters.add(main);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void addHeaderInList() {
        View paddingView = new View(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        paddingView = inflater.inflate(R.layout.expand_header, null);

        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen._200sdp));
        paddingView.setLayoutParams(lp);
        // This is required to disable header's list selector effect
        paddingView.setClickable(true);
        expChapter.addHeaderView(paddingView);
        cbSelectAll = paddingView.findViewById(R.id.cbSelectAll);
        cbSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mySharedPref.getVersion().equals("0")) {
                    if (cbSelectAll.isChecked()) {
                        chapterAdapter.selectAll();
                        chapterAdapter.notifyDataSetChanged();
                    } else {
                        chapterAdapter.deSelectAll();
                        chapterAdapter.notifyDataSetChanged();
                    }
                } else {
                    cbSelectAll.setChecked(false);
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(PractiesChapterActivity.this);
                    builder1.setMessage("Alert! To view this please purchase the complete version.");
                    builder1.setCancelable(true);
                    builder1.setNegativeButton("Close", (dialog, i) -> {
                        dialog.cancel();
                    });
                    builder1.setPositiveButton(
                            "Subscribe",
                            (dialog, id) -> {
                                dialog.cancel();
                                startActivity(new Intent(PractiesChapterActivity.this, ChooseOptionActivity.class));
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
//                if (cbSelectAll.isChecked()) {
//                    chapterAdapter.selectAll();
//                    chapterAdapter.notifyDataSetChanged();
//                } else {
//                    chapterAdapter.deSelectAll();
//                    chapterAdapter.notifyDataSetChanged();
//                }
            }
        });
    }

    @Override
    public void onCheckBoxDeselect(boolean b) {
        cbSelectAll.setChecked(b);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mFlexibleSpaceView, -scrollY);

        float flexibleRange = mFlexibleSpaceImageHeight - mActionBarSize;
        int minOverlayTransitionY = mActionBarSize - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));

        // Translate list background
        ViewHelper.setTranslationY(mListBackgroundView, Math.max(0, -scrollY + mFlexibleSpaceImageHeight));

        // Change alpha of overlay
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));

        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 180);

        mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    @Override
    public void onCompleted() {
        showProgress(false, true);
    }

    @Override
    public void onCompleted(String msg) {
        showProgress(false, true);

        if (ReadyquestionsList.size() > 0) {
            startActivity(new Intent(instance, PractiesPaperSolutionActivity.class)
                    .putExtra("questionsList", ReadyquestionsList));
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(instance);
            builder1.setMessage(msg);
            builder1.setCancelable(true);
            builder1.setNegativeButton("Close", (dialog, i) -> {
                dialog.cancel();
                finish();
            });

//            builder1.setPositiveButton(
//                    "Subscribe",
//                    (dialog, id) -> {
//                        dialog.cancel();
//                        startActivity(new Intent(PractiesChapterActivity.this, ChooseOptionActivity.class));
//                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    //For Ready Paper
    public class getReadyTest extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        public OnTaskCompleted listener;

        String examTypesID = "";
        public float mark = 0;
        public String MPSQID = "0";
        String msg = "";


        private getReadyTest(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            this.listener = (OnTaskCompleted) context;

        }

        @Override
        protected void onPreExecute() {
            showProgress(true, true);//Utils.showProgressDialog(instance);
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... id) {
            Cursor cExamTypes = mDb.getAllRows(DBQueries.getExamtypesID(App.practiesPaperObj.getSubjectID()));
            String messagee = "";


            Cursor c = mDb.getAllRows(DBQueries.getExamTypePatternId(App.practiesPaperObj.getSubjectID(), App.practiesPaperObj.getExamTypeID()));
            if (c != null && c.moveToFirst()) {
                ////Utils.Log("Cursor Object Detail", DatabaseUtils.dumpCursorToString(c));
                App.practiesPaperObj.setExamTypePatternId(getExamPatternID(c));
            }
            c.close();


            if (!App.practiesPaperObj.getExamTypePatternId().isEmpty()) {
                StudentQuestionPaper sPaper = new StudentQuestionPaper();
                sPaper.setExamTypePatternID(App.practiesPaperObj.getExamTypePatternId());
                sPaper.setSubjectID(App.practiesPaperObj.getOldSubjectID());
                sPaper.setStudentID(MainUser.getStudentId());
                sPaper.setPaperTypeID(App.practiesPaperObj.getPaperTypeID());

                StudentQuestionPaperID = mDb.insertRow(sPaper, DBConstant.student_question_paper);


            } else {
                showProgress(false, true);
                if (mySharedPref.getVersion().equals("1")) {
                    messagee = "Alert! To view this please purchase the complete version.";
                } else {
                    if (App.practiesPaperObj.getExamTypePatternId().equals("1")) {
                        messagee = "Please carefully read the instruction.";
                    } else {
                        messagee = "Please read the instructions and selects chapters accordingly.";
                    }
                }
                return messagee;
            }


// start

            Cursor cExamTypePatrernDetail = mDb.getAllRows(DBQueries.getExamTypePatternDetail(App.practiesPaperObj.getExamTypePatternId()));

            if (cExamTypePatrernDetail != null && cExamTypePatrernDetail.moveToFirst()) {

                String CheckQuestionID = "";
                do {
                    examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());
                    if (examTypePatternDetail.getIsQuestion().equals("0")) {
                        examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());


                    } else {
                        Cursor Ques;
                        MasterQuestionWithSubQue masterQuestion = null;
                        List<String> list = new ArrayList<String>(Arrays.asList(App.practiesPaperObj.getChapterID().split(",")));
                        for (int i = 0; i < list.size(); i++) {
                            //for (String cID : list) {
                            StudentQuestionPaperChapter cp = new StudentQuestionPaperChapter();
                            cp.setChapterID(list.get(i));
                            try {
                                cp.setStudentQuestionPaperID((int) StudentQuestionPaperID);
                            } catch (NumberFormatException e) {
                                cp.setStudentQuestionPaperID(-1);
                                e.printStackTrace();
                            }

                            mDb.insertRow(cp, DBConstant.student_question_paper_chapter);
                        }
                        Ques = mDb.getAllRows(DBQueries.getRandomSubQuestionWithID(App.practiesPaperObj.getChapterID(), examTypePatternDetail.getQuestionTypeID(), CheckQuestionID));
                        outerloop:
                        if (Ques != null && Ques.moveToFirst()) {

                            do {
                                subQuesLabelArray.clear();

                                masterQuestion = (MasterQuestionWithSubQue) cParse.parseCursor(Ques, new MasterQuestionWithSubQue());
                                quesIds = quesIds + masterQuestion.getMQuestionID() + ",";
                                chapterIds = chapterIds + masterQuestion.getChapterID() + ",";
                                String QuesTypeId = masterQuestion.getQuestionTypeID();
                                String IsQuesType = "";
                                int i = 1;
                                int isMTC = mDb.getAllRecordsUsingQuery(DBQueries.checkQusTypeisMTC(QuesTypeId + ""));


                                CheckQuestionID = CheckQuestionID + Ques.getString(Ques.getColumnIndex("MQuestionID")) + ",";
                                StudentQuestionPaperDetail cpd = new StudentQuestionPaperDetail();
                                cpd.setStudentQuestionPaperID((int) StudentQuestionPaperID);
                                cpd.setMQuestionID(masterQuestion.getMQuestionID());
                                cpd.setExamTypePatternDetailID(examTypePatternDetail.getExamTypePatternDetailID());
                                cpd.setCQuestionID(0);
                                cpd.setMasterorCustomized(0);
                                DetailID = mDb.insertRow(cpd, DBConstant.student_question_paper_detail);
                                Cursor cIsPassage = mDb.getAllRows(DBQueries.checkQusTypeisPassage(cpd.getMQuestionID()));

                                int isPassage = 0;
                                if (cIsPassage != null && cIsPassage.moveToFirst()) {
                                    isPassage = cIsPassage.getInt(0);
                                    cIsPassage.close();
                                }

                                if (isPassage == 1) {
                                    Cursor clabel = mDb.getAllRows(DBQueries.getLabel(Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID"))));
                                    //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                                    if (clabel != null && clabel.moveToFirst()) {

                                        do {

                                            subQuesLabelObj = new SubQueLabel();
                                            subQuesLabelObj.setLabel(clabel.getString(clabel.getColumnIndexOrThrow("Label")));
                                            //  Log.i("LAbel", "=> " + clabel.getString(clabel.getColumnIndexOrThrow("Label")));
                                            mark = clabel.getInt(clabel.getColumnIndexOrThrow("Mark"));
                                            subQueskeyObj = new SubQueKey();
                                            // Log.i("LAbel", "=> " + clabel.getString(clabel.getColumnIndexOrThrow("Label")) + " mark " + "=> " + mark);
                                            while (0 < mark) {
                                                Cursor csqi = mDb.getAllRows(DBQueries.getPassageSubquetions(MPSQID,
                                                        Ques.getString(Ques.getColumnIndexOrThrow("MQuestionID")),
                                                        QuesTypeId, String.valueOf(mark), clabel.getString(clabel.getColumnIndexOrThrow("PassageSubQuestionTypeID"))));

                                                // longInfo(DatabaseUtils.dumpCursorToString(csqi), "Cursor");
                                                if (csqi != null && csqi.moveToFirst()) {
                                                    boolean isExistes = false;
                                                    // Log.i("In IF Line NO ", "=> 490");
                                                    try {

                                                        if (MPSQID.equals("0")) {
                                                            MPSQID = csqi.getString(csqi.getColumnIndex("MPSQID"));
                                                        } else {
                                                            MPSQID = MPSQID + "," + csqi.getString(csqi.getColumnIndex("MPSQID"));
                                                        }
                                                        //Utils.Log("ID :->", csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")));
                                                        mark = mark - csqi.getFloat(csqi.getColumnIndexOrThrow("Marks"));
                                                        //  Utils.Log("In IF :-> ", mark + "");
                                                        subQuesObj = new ClassQuestionPaperSubQuestion();
                                                        subQuesObj.setMPSQID(csqi.getString(csqi.getColumnIndex("MPSQID")));
                                                        subQuesObj.setDetailID(String.valueOf(DetailID));
                                                        subQuesObj.setPaperID(String.valueOf(StudentQuestionPaperID));
                                                        for (SubQueLabel lblObj : subQuesLabelArray) {

                                                            if (lblObj.getLabel().equals(clabel.getString(clabel.getColumnIndexOrThrow("Label")))) {
                                                                for (SubQueKey keyObj : lblObj.keyArray) {
                                                                    if (keyObj.getKey().equals(csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")))) {
                                                                        //      Log.i(" IF LAbel", "=> " + clabel.getString(clabel.getColumnIndexOrThrow("Label")) + " mark " + "=> " + mark);
                                                                        keyObj.internalArray.add(subQuesObj);
                                                                        // lblObj.keyArray.add(keyObj);
                                                                        isExistes = true;
                                                                        break;
                                                                    } else {
                                                                        isExistes = false;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (!isExistes) {
                                                            //   Log.i(" ELSE Label - 1", "=> " + clabel.getString(clabel.getColumnIndexOrThrow("Label")) + " mark " + "=> " + mark);

                                                            subQueskeyObj.setKey(csqi.getString(csqi.getColumnIndex("SubQuestionTypeID")));
                                                            subQueskeyObj.internalArray.add(subQuesObj);
                                                        }
                                                        if (subQuesLabelArray.size() <= 0 || subQuesLabelArray == null) {
                                                            subQuesLabelObj.keyArray.add(subQueskeyObj);
                                                            subQuesLabelArray.add(subQuesLabelObj);
                                                        }

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                        Utils.Log("In CATCH :-> ", mark + "");
                                                        mDb.dbQuery(DBNewQuery.DeleteStudent_question_paper((int) StudentQuestionPaperID));
                                                        csqi.close();
                                                        msg = "Sub Question Not Avaliable.!";
                                                        break outerloop;
                                                    }
                                                } else {
                                                    Utils.Log("In ELSE :-> ", mark + "");

                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_paper_detal((int) StudentQuestionPaperID));
                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_paper_Chapter((int) StudentQuestionPaperID));
                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_set_question_type((int) StudentQuestionPaperID));
                                                    mDb.dbQuery(DBNewQuery.DeleteStudent_question_paper((int) StudentQuestionPaperID));

                                                    csqi.close();
                                                    msg = "Sub Question Not Avaliable.!";
                                                    break outerloop;
                                                }

                                            }

                                            boolean islblExistes = false;
                                            for (SubQueLabel lblObj : subQuesLabelArray) {
                                                if (lblObj.getLabel().equals(subQuesLabelObj.getLabel())) {
                                                    islblExistes = true;
                                                    break;
                                                } else {
                                                    islblExistes = false;
                                                }
                                            }
                                            if (!islblExistes) {
                                                subQuesLabelObj.keyArray.add(subQueskeyObj);
                                                subQuesLabelArray.add(subQuesLabelObj);
                                            }


                                        } while (clabel.moveToNext());
                                        clabel.close();

                                        for (SubQueLabel lblObj : subQuesLabelArray) {
                                            for (SubQueKey keyObj : lblObj.keyArray) {
                                                for (ClassQuestionPaperSubQuestion obj : keyObj.internalArray) {
                                                    mDb.insertRow(obj, DBConstant.class_question_paper_sub_question);
                                                }


                                            }

                                        }
                                        Gson gson = new GsonBuilder().create();
                                        JsonArray jStudQuePaperArray = gson.toJsonTree(subQuesLabelArray).getAsJsonArray();

                                        // longInfo(String.valueOf(jStudQuePaperArray), "LAbel Array");
                                    }

                                } else {

                                }
                            } while (Ques.moveToNext());
                        }
                    }

                } while (cExamTypePatrernDetail.moveToNext());
            }

// get Qution with Qution Type

            return msg;
        }

        public String getExamPatternID(Cursor c) {
            String tempExamTypePatternId = "";

            do {
                Cursor cExamTypePatrernDetail = mDb.getAllRows(DBQueries.getExamTypePatternDetail(c.getString(c.getColumnIndex("ExamTypePatternID"))));

                if (cExamTypePatrernDetail != null && cExamTypePatrernDetail.moveToFirst()) {
                    boolean checkQuestions = false;
                    String CheckQuestionID = "";
                    do {
                        examTypePatternDetail = (ExamTypePatternDetail) cParse.parseCursor(cExamTypePatrernDetail, new ExamTypePatternDetail());
                        if (examTypePatternDetail.getIsQuestion().equals("0")) {

                        } else {
                            Cursor Ques;
                            Ques = mDb.getAllRows(DBQueries.getRandomSubQuestionWithID(App.practiesPaperObj.getChapterID(), examTypePatternDetail.getQuestionTypeID(), CheckQuestionID));

                            // Utils.Log("Cursor Ques", DatabaseUtils.dumpCursorToString(Ques));
                            if (Ques != null && Ques.moveToFirst()) {
                                checkQuestions = true;
                            } else {
                                checkQuestions = false;
                                break;
                            }
                            CheckQuestionID = CheckQuestionID + Ques.getString(Ques.getColumnIndex("MQuestionID")) + ",";
                        }

                    } while (cExamTypePatrernDetail.moveToNext());
                    cExamTypePatrernDetail.close();

                    if (checkQuestions) {
                        checkPatternDetail = true;
                        tempExamTypePatternId = c.getString(0);
                        break;
                    } else {
                        checkPatternDetail = false;
                    }
                }
                if (checkPatternDetail) {
                    checkcheckPattenId = true;
                    break;
                }
            } while (c.moveToNext());
            //c.close();
            // Utils.Log("TAG Final checkcheckPattenId ==> ", String.valueOf(checkcheckPattenId));
            return tempExamTypePatternId;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String HTML = "";
            try {
                TempReadyTestRecordArrayList = (ArrayList) new CustomDatabaseQuery(instance, new PrelimTestRecord())
                        .execute(new String[]{DBNewQuery.getPrelimPaperQuestionsIsPassage(String.valueOf(StudentQuestionPaperID))}).get();
            } catch (Exception e) {
                e.printStackTrace();
            }

            App.practiesPaperObj.setStudentQuestionPaperID(String.valueOf(StudentQuestionPaperID));
            ArrayList<PrelimTestRecord> prelimTestRecordArrayList = new ArrayList<>();
            for (int i = 0; i < TempReadyTestRecordArrayList.size(); i++) {
                if (TempReadyTestRecordArrayList.get(i).getIsPassage() != null) {
                    if (TempReadyTestRecordArrayList.get(i).getIsPassage().equals("1")) {
                        HTML = "";
                        sub_qt = 0;
                        Cursor clabel = mDb.getAllRows(DBQueries.getLabel(TempReadyTestRecordArrayList.get(i).getMQuestionID()));
                        //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                        if (clabel != null && clabel.moveToFirst()) {

                            k = 1;
                            do {
                                Cursor csqi = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id(App.practiesPaperObj.getStudentQuestionPaperID(),
                                        TempReadyTestRecordArrayList.get(i).getStudentQuestionPaperDetailID(),
                                        clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID"))));
                                if (csqi != null && csqi.moveToFirst()) {
                                    sq = 0;
                                    n = 1;
                                    do {
                                        Cursor totla = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id_total(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                TempReadyTestRecordArrayList.get(i).getStudentQuestionPaperDetailID(),
                                                clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")),
                                                csqi.getString(csqi.getColumnIndex("SubQuestionTypeID"))));
                                        sub_questions_total = totla.getCount();
                                        totla.close();
                                        if (n == 1) {
                                            Cursor c = mDb.getAllRows(" SELECT DISTINCT(sqt.SubQuestionTypeID)\n" +
                                                    "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                                                    "INNER JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                                                    "INNER JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                                                    "INNER JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                                                    "WHERE `cqpsq`.`PaperID` = '" + App.practiesPaperObj.getStudentQuestionPaperID() + "'\n" +
                                                    "AND `cqpsq`.`DetailID` = '" + TempReadyTestRecordArrayList.get(i).getStudentQuestionPaperDetailID() + "'\n" +
                                                    "AND `psqt`.`PassageSubQuestionTypeID` = '" + clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")) + "'");

                                            if (c != null && c.moveToFirst()) {
                                                sub_question_type_total = c.getCount();
                                                c.close();
                                            }


                                        }
                                        HTML = HTML + "<table> <tr class=\"margin_top\">\n" +
                                                "<td width=\"30\"></td>\n" +
                                                "<td width=\"30\" style=\"vertical-align: top;\"><b>";
                                        if (sub_qt == 0 && n == 1) {
                                            HTML = HTML + k + " </b></td>";
                                        }
                                        if (SubQuestionTypeID != csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"))) {
                                            SubQuestionTypeID = csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"));
                                            sq++;
                                            sss = 1;
                                            if (n == 1) {
                                                HTML = HTML + "  <td width=\"660\">\n" +
                                                        "<table width=\"100%\">\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "           <b>";
                                                HTML = HTML + csqi.getString(csqi.getColumnIndex("Label")) + ")" +
                                                        "           </b>\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"630\">\n";

                                                if (sub_question_type_total == 1) {
                                                    HTML = HTML + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                } else {
                                                    HTML = HTML + RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                }

                                                HTML = HTML + "        </td>\n";

                                                double marks = sub_questions_total * csqi.getFloat(csqi.getColumnIndex("QuestionTypeMarks"));

                                                DecimalFormat format = new DecimalFormat();
                                                format.setDecimalSeparatorAlwaysShown(false);

                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + format.format(marks) + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>";
                                            } else {
                                                HTML = HTML + "<td width=\"660\">\n" +
                                                        "<table>\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"630\">\n" +
                                                        RomanNumber.toRoman(sq) + ") " + csqi.getString(csqi.getColumnIndex("QuestionType")) +
                                                        "       </td>\n";

                                                double marks = sub_questions_total * csqi.getFloat(csqi.getColumnIndex("QuestionTypeMarks"));


                                                DecimalFormat format = new DecimalFormat();
                                                format.setDecimalSeparatorAlwaysShown(false);

                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + format.format(marks) + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>    ";
                                            }

                                           /* int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>";*/
                                        }

                                        HTML = HTML + "</tr>" +
                                                " <tr class=\"margin_top\">\n" +
                                                "   <td width=\"30\"></td>\n" +
                                                "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                "   <td width=\"660\" style=\"padding-left: 6%;\" class=\"sub\">\n";
                                        if (sub_questions_total == 1) {
                                            HTML = HTML + csqi.getString(csqi.getColumnIndex("Question"));
                                        } else {
                                            HTML = HTML + sss + ") " + csqi.getString(csqi.getColumnIndex("Question"));
                                        }

                                        HTML = HTML + "   </td>\n" +
                                                "   <td align=\"right\" width=\"30\"></td>\n" +
                                                "</tr>      ";
                                        n++;
                                        sss++;
                                    }
                                    while (csqi.moveToNext());
                                    csqi.close();
                                }
                                if (sub_qt == 0) {
                                    HTML = HTML + "<tr class=\"margin_top\">\n" +
                                            "<td width=\"30\"></td>\n" +
                                            "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                            "<td width=\"660\">" +
                                            TempReadyTestRecordArrayList.get(i).getQuestion() +
                                            "</td>\n" +
                                            "<td align=\"right\" width=\"30\"></td>\n" +
                                            "</tr>     \n";
                                }

                                sub_qt++;
                            } while (clabel.moveToNext());
                            clabel.close();
                            k++;

                        } else {
                        }
                        TempReadyTestRecordArrayList.get(i).setQuestion(HTML + "</table>");
                        // TempReadyTestRecordArrayList.get(i).setQuestion("test");

                    }
                }
                if (TempReadyTestRecordArrayList.get(i).getPageNo().equals(PageNo)) {
                    readyObj = new PrelimTestRecord();
                    readyObj.setAnswer(TempReadyTestRecordArrayList.get(i).getAnswer());
                    readyObj.setExamTypePatternDetailID(TempReadyTestRecordArrayList.get(i).getExamTypePatternDetailID());
                    readyObj.setIsQuestion(TempReadyTestRecordArrayList.get(i).getIsQuestion());
                    readyObj.setPageNo(TempReadyTestRecordArrayList.get(i).getPageNo());
                    readyObj.setQuestionNo(TempReadyTestRecordArrayList.get(i).getQuestionNo());
                    readyObj.setQuestionTypeText(TempReadyTestRecordArrayList.get(i).getQuestionTypeText());
                    readyObj.setSubQuestionNo(TempReadyTestRecordArrayList.get(i).getSubQuestionNo());
                    readyObj.setQuestion(TempReadyTestRecordArrayList.get(i).getQuestion());
                    readyObj.setIsPassage(TempReadyTestRecordArrayList.get(i).getIsPassage());
                    readyObj.setQuestionMarks(TempReadyTestRecordArrayList.get(i).getQuestionMarks());
                    readyObj.setQuestionTypeID(TempReadyTestRecordArrayList.get(i).getQuestionTypeID());

                    prelimTestRecordArrayList.add(readyObj);

                    //  stringArrayListHashMap.put(PageNo, prelimTestRecordArrayList);
                } else {

                    ReadyquestionsList.add(new PrelimQuestionListModel(PageNo, prelimTestRecordArrayList));

                    PageNo = TempReadyTestRecordArrayList.get(i).getPageNo();
                    prelimTestRecordArrayList = new ArrayList<>();
                    readyObj = new PrelimTestRecord();


                    readyObj.setAnswer(TempReadyTestRecordArrayList.get(i).getAnswer());
                    readyObj.setExamTypePatternDetailID(TempReadyTestRecordArrayList.get(i).getExamTypePatternDetailID());
                    readyObj.setIsQuestion(TempReadyTestRecordArrayList.get(i).getIsQuestion());
                    readyObj.setPageNo(TempReadyTestRecordArrayList.get(i).getPageNo());
                    readyObj.setQuestionNo(TempReadyTestRecordArrayList.get(i).getQuestionNo());
                    readyObj.setQuestionTypeText(TempReadyTestRecordArrayList.get(i).getQuestionTypeText());
                    readyObj.setSubQuestionNo(TempReadyTestRecordArrayList.get(i).getSubQuestionNo());
                    readyObj.setQuestion(TempReadyTestRecordArrayList.get(i).getQuestion());
                    readyObj.setIsPassage(TempReadyTestRecordArrayList.get(i).getIsPassage());
                    readyObj.setQuestionMarks(TempReadyTestRecordArrayList.get(i).getQuestionMarks());
                    readyObj.setQuestionTypeID(TempReadyTestRecordArrayList.get(i).getQuestionTypeID());


                    prelimTestRecordArrayList.add(readyObj);

                    //    stringArrayListHashMap.put(PageNo, prelimTestRecordArrayList);
                }


            }
            ReadyquestionsList.add(new PrelimQuestionListModel(PageNo, prelimTestRecordArrayList));
            ReadyquestionsList.remove(0);
           /* Gson gson = new GsonBuilder().create();
            JsonArray jStudQuePaperArray = gson.toJsonTree(ReadyquestionsList).getAsJsonArray();

            longInfo(String.valueOf(jStudQuePaperArray), "ReadyquestionsList");*/

            listener.onCompleted(s);
        }
    }


    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onClick = false;
    }
}

