package com.parshvaa.isccore.utils;

import android.os.Environment;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {
    public static final String MCQTEST = "MCQTEST";
    public static final String PRACTICEPAPER = "PRACTICEPAPER";
    public static final String REVISE = "REVISE";
    public static final String REPORT = "REPORT";
    public static int EXTERNAL_STORAGE_PERMISSION = 9999;
    public static int READ_PHONE_STATE = 568;
    public static String PREFRENCE = "shared_pref";

    public static final String CONNECT_TO_WIFI = "WIFI";
    public static final String CONNECT_TO_MOBILE = "MOBILE";
    public static final String NOT_CONNECT = "NOT_CONNECT";
    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    public static String LOCAL_IMAGE_PATH = Environment.getExternalStorageDirectory() + "/.iScore";
    public static String LOCAL_Library_PATH = Environment.getExternalStorageDirectory() + "/Download";
    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;

    public static String HTTP = "http";
    public static String HTTPS = "https";
    //------------------- PAYTM TEST Cradantial--------------------------------------
    /*
    public static final String M_ID = "PARSHV66654838019682"; //paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail"; //paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPSTAGING";
    public static final String CALLBACK_URL = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
    public static String TXN_STATUS = "https://securegw-stage.paytm.in/merchant-status/getTxnStatus?";
*/

    //------------------- PAYTM LIVE Cradantial--------------------------------------

    public static final String M_ID = "Parshv15070673965035"; //paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail109"; //paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPPROD";
    public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";
    //  public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback.jsp";
    public static String TXN_STATUS_URL = "https://securegw.paytm.in/merchant-status/getTxnStatus?";

    //------------------- LIVE SERVER--------------------------------------
    public static String IMAGEURL = "https://staff.iccc.co.in/";
   // public static String BASIC_URL = Utils.getHttp() + "://staff.iccc.co.in/web_services/";
    public static String BASIC_URL = "https://staff.iccc.co.in/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL ="https://staff.iccc.co.in/";
//    public static String URL_VERSION = "version_57/";
    public static String URL_VERSION = "version_58/";

    //------------------- PEMP SERVER--------------------------------------
 /*  public static String BASIC_URL = Utils.getHttp() + "://test.pemiscore.parshvaa.com/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = "https://test.pemevaluater.parshvaa.com/";
    public static String URL_VERSION = "version_40/";*/

    //------------------- TEST SERVER--------------------------------------
     /*public static String BASIC_URL = Utils.getHttp() + "://test.escore.parshvaa.com/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = Utils.getHttp() + "://test.evaluator.parshvaa.com/";
    public static String URL_VERSION = "version_39/";*/

    public static String EVEL_LIB_SYSTEM_IMAGE_PATH = BASIC_VIEW_PAPER_EVA_URL + "library_system/";

    /*
     ** TABLE NAME **
     */
    public static String sub_question_types = "sub_question_types";
    public static String passage_sub_question_type = "passage_sub_question_type";
    public static String master_passage_sub_question = "master_passage_sub_question";
    public static String exam_type_pattern_detail = "exam_type_pattern_detail";
    public static String exam_type_pattern = "exam_type_pattern";
    public static String examtypes = "examtypes";
    public static String examtype_subject = "examtype_subject";
    public static String paper_type = "paper_type";
    public static String chapters = "chapters";
    public static String subjects = "subjects";
    public static String mcqquestion = "mcqquestion";
    public static String mcqoption = "mcqoption";
    public static String masterquestion = "masterquestion";
    public static String question_types = "question_types";
    public static String class_evaluater_cct_count = "class_evaluater_cct_count";
    public static String student_mcq_test_hdr = "student_mcq_test_hdr";
    public static String student_mcq_test_chapter = "student_mcq_test_chapter";
    public static String student_mcq_test_dtl = "student_mcq_test_dtl";
    public static String student_mcq_test_level = "student_mcq_test_level";
    public static String student_question_paper="student_question_paper";
    public static String student_question_paper_chapter="student_question_paper_chapter";
    public static String student_question_paper_detail="student_question_paper_detail";
    public static String class_question_paper_sub_question="class_question_paper_sub_question";
    public static String student_set_paper_question_type="student_set_paper_question_type";
    public static String student_set_paper_detail="student_set_paper_detail";
    public static String board_paper_download="board_paper_download";
    public static String mcqImage="mcqImage";
    public static String masterImage="masterImage";
    public static String tableRecords="tableRecords";
    public static String isOnlyQuestionPaper="isOnlyQuestionPaper";
    public static String AnswerPaperDate="AnswerPaperDate";
    public static String temp_mcq_test_hdr="temp_mcq_test_hdr";
    public static String temp_mcq_test_chapter="temp_mcq_test_chapter";
    public static String temp_mcq_test_level="temp_mcq_test_level";
    public static String temp_mcq_test_dtl="temp_mcq_test_dtl";
    public static String notification="notification";
    public static String expiryDate="30/04/2022";
    public static String ReferAndEarn="ReferAndEarn";
    public static String Category="Category";
    public static String Image="Image";
    public static String data="data";
    public static String Subscribe="Subscribe";
    public static String path="path";
    public static String isDemo="isDemo";
    public static String isKeyRegiste="isKeyRegiste";
    public static String MobLogin="MobLogin";

    public static String UPLOADSTARTTIME="UploadStartTime";
    public static String UPLOADENDTIME="UploadEndTime";
    public static String STARTTIME="starttime";
    public static String ENDTIME="endtime";
    public static String PAPERID="paperID";
    public static String PAPERNAME="papername";
    public static String PAPERLINK="paperlink";
    public static String TYPE="type";
    public static String FROM="FROM";


    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE, POST_WITH_JSON, Eval_POST_WITH_JSON
    }


    public enum REQUESTS {
        getCountry, getOldConsultation, getImages, getMasterQuestionImages, getMcqQuestionImages,
        getCard, getCategoryWaitTime, getDBfile, getWaitTime, reschedule, forgot_password, saveSchedule, getSlots, addMinutes, saveCard, getLogin, getCategory, getBrands, register, getCallHistory, getScheduleCallHistory, editProfile, getUserProfile, getMinutes, rateFeedback,
        setCall, getCallToken, startCall, endCall, paymentTransaction, getTechnician, registerDevice, getState, getQulification, getDoctorEdit, getDoctor, getStatePin, getPatientEditProfile, getSpecialist, updateToken, updateDoctorToken, updatePatientToken, getDepartment, verifyOTP, getTimeSlot, changeLiveStatus, getChemist, getFreeDay, CreateConsultation, connectCall, setPrescription, viewEarning, getPetient, forgot_pw, sendOTP, resendOTP, changePass, callStatus, webLogin, getSpinner,
        getBoard, getMedium, getStandeard, getSubject, getBoardPaperFIle, getBoardsPaperTable, getPackage,getFlashBanner,getDashPopup,getFlashBannerSubject,
        notification, getSubjectTable, getChapterTable, getMcqQuetions, getPaper_Type, getExamtype_subject, getExamtypes, getExamTypePattern, getExamTypePatternDetail, getMasterQuestionTable, getQuesTypeTable, getMcqOptions, sendMcqData, sendMcqDataNew, sendGanreatPaperNew, sendGanreatPaper,ClassEventCheck, sendBoardPaperDownload, upadateProfile,UploadPaperImages, qurySendMail, EventRegister,GetEvent, SendMcqData,getDate,SendPaperEnterTime,
        getDataByDate,getOnlineClassURL,
        getTableRecords, getStudentDetails, get_paper, get_subject_mcq_pape, check_app_login, UpdateTokan, getSilentLogin, evaluator_connect, view_paper, get_evaluator_subjects, mcq_view_paper, view_model_answer, submit_mcq_test, get_recent_mcq_paper, get_mcq_test_result, get_recent_question_paper, evaluator_register, mcq_view_summary, getCctReport, send_otp, get_updated_masterquetion, checkKey, direct_register, get_subject_board, get_boardpaper, get_single_question_paper,get_single_upload_question_paper, get_zooki, get_sure_shot, get_moderatepaper, get_moderatepaper_subject, get_textbook_chapter_list,get_textbook_list,get_notice_board, get_library_parent_list, get_library_chapter_list, get_system_library_files, get_personal_library_sub_folder_files_list, check_login_token,check_login_user, sendIncorrectQuestion, sendNotAppearedQuestion, change_class_key, get_student_class_dtl,CheckClass, resend_otp, update_last_sync_time, EventSubscribe, subscribe, verify_bord_medium_standard, getTxnStatus, Verifychecksum, get_master_passage_sub_question, get_passage_sub_question_types, get_sub_question_types, get_student_wallate_blance, withdraw_money, varify_refral_code, GetPromoCode
    }

    public static int SUBJECT_TABLE = 2;
    public static int CHAPTER_TABLE = 3;

    /**
     * This is for PUSH Notification
     **/

    public static final String SENDER_ID = "";
    public static final String DISPLAY_ACTION = "com.markteq.wms";

    public static final int TWITTER_LOGIN_REQUEST_CODE = 1;


}
