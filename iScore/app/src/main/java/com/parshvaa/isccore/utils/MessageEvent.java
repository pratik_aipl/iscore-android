package com.parshvaa.isccore.utils;

public class MessageEvent {
    Exception e;
    boolean isFaild;

    public MessageEvent(Exception e, boolean isFaild) {
        this.e = e;
        this.isFaild = isFaild;
    }

    public Exception getE() {
        return e;
    }

    public boolean isFaild() {
        return isFaild;
    }
}
