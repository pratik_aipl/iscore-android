package com.parshvaa.isccore.activity.event;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.ArtiseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ArtiseListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<ArtiseModel> subCatList;



    public ArtiseListAdpater(Context context, List<ArtiseModel> subCatLists) {
        this.context = context;
        this.subCatList = subCatLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.event_artise_row_item, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        ArtiseModel listModel = subCatList.get(position);

        holder.tvArtiseName.setText(listModel.getArtistName());
        if (!TextUtils.isEmpty(listModel.getImage()))
            Glide.with(context)
                    .load(listModel.getImage())
                    .centerCrop()
                    .placeholder(R.drawable.iscore_final_logo)
                    .into(holder.imgArtise);
    }


    public void data() {

    }

    @Override
    public int getItemCount() {
        return subCatList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_artise)
        CircleImageView imgArtise;
        @BindView(R.id.tv_artise_name)
        TextView tvArtiseName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}