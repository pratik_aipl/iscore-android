package com.parshvaa.isccore.activity.library.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.FileDownloader;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class NotesViewActivity extends BaseActivity {
    public WebView web_notes;

    public App app;
    public String URL, FileName;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_notes_view);
        Utils.logUser();

        web_notes = findViewById(R.id.web_notes);

        URL = getIntent().getStringExtra("URL");

        WebSettings settings = web_notes.getSettings();
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(false);
        settings.setDomStorageEnabled(true);
        web_notes.setWebViewClient(new myWebClient());

        // pdfview.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
        web_notes.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + URL);
        Utils.Log("TAG", "URL :-> " + "http://drive.google.com/viewerng/viewer?embedded=true&url=" + URL);
    }

    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            showProgress(true, true);//Utils.showProgressDialog(NotesViewActivity.this);
            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                showProgress(false, true);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            FileName = strings[0]; // -> http://maven.apache.org/maven-1.x/maven.pdf
            // -> maven.pdf
            String SDCardPath = Constant.LOCAL_IMAGE_PATH;

            File qDirectory = new File(SDCardPath);
            if (!qDirectory.exists()) qDirectory.mkdirs();
            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            String qLocalUrl = SDCardPath + "/" + "SMJV" + date + ".pdf";
            File qLocalFile = new File(qLocalUrl);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(FileName, qLocalFile, getApplicationContext());

            } else {
                msg = "you have alerady downloaded";

            }
            return msg;
        }

        @Override
        protected void onPostExecute(final String aVoid) {

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (aVoid.equals("")) {
                        Utils.showToast("Download Complete", NotesViewActivity.this);

                    } else {
                        Utils.showToast(aVoid, NotesViewActivity.this);
                    }

                }
            };
            r.run();


            //showProgress(false)();

        }
    }
}

