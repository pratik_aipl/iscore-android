package com.parshvaa.isccore.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class CourseModel implements Serializable{

    public String EventSeriesID;
    public String EventID;
    public String Title;
    public String StartDateTime;
    public String EndDateTime;
    public String Duration;
    public String Link;
    public String Price;
    public String ClassHolderShare;

    public String getEventSeriesID() {
        return EventSeriesID;
    }

    public void setEventSeriesID(String eventSeriesID) {
        EventSeriesID = eventSeriesID;
    }

    public String getEventID() {
        return EventID;
    }

    public void setEventID(String eventID) {
        EventID = eventID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getClassHolderShare() {
        return ClassHolderShare;
    }

    public void setClassHolderShare(String classHolderShare) {
        ClassHolderShare = classHolderShare;
    }
}
