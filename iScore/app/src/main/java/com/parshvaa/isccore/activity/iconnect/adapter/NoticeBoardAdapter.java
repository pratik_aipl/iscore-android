package com.parshvaa.isccore.activity.iconnect.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.parshvaa.isccore.activity.iconnect.NoticeBoard;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.utils.Utils;

import java.util.List;

/**
 * Created by Karan - Empiere on 2/28/2018.
 */

public class NoticeBoardAdapter extends RecyclerView.Adapter<NoticeBoardAdapter.MyViewHolder> {
    private static final String TAG = "NoticeBoardAdapter";
    private List<NoticeBoard> noticArrayList;
    public Context context;
    public int SubjectID;
    public String SubjectName;
    public String pathUrl;
    public AQuery aQuery;

    public NoticeBoardAdapter(List<NoticeBoard> noticArrayList, Context context, String pathUrl) {
        this.noticArrayList = noticArrayList;
        this.context = context;
        this.pathUrl = pathUrl;
        aQuery = new AQuery(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_notice_board_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_date, tv_discription;
        ImageView mNoticeImage;

        public MyViewHolder(View view) {
            super(view);
            mNoticeImage = view.findViewById(R.id.mNoticeImage);
            tv_title = view.findViewById(R.id.tv_title);
            tv_date = view.findViewById(R.id.tv_date);
            tv_discription = view.findViewById(R.id.tv_description);
        }
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NoticeBoard noticObj = noticArrayList.get(position);
        holder.tv_title.setText(noticObj.getTitle());
        holder.tv_date.setText(Utils.changeDateAndTimeFormet(noticObj.getCreatedOn()));
        holder.tv_discription.setText(noticObj.getDescription());
        Log.d(TAG, "onBindViewHolder: " + (!TextUtils.isEmpty(noticObj.getImage())));

        if (!TextUtils.isEmpty(noticObj.getImage()) && !noticObj.getImage().equalsIgnoreCase("https://evaluater.iccc.co.in/notice_board/")) {
            int dimensionInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, context.getResources().getDisplayMetrics());
            holder.mNoticeImage.getLayoutParams().height = dimensionInDp;
            holder.mNoticeImage.requestLayout();
            Log.d(TAG, "onBindViewHolder: " + pathUrl + noticObj.getImage());
            aQuery.id(holder.mNoticeImage).progress(null).image(pathUrl + noticObj.getImage(), true, true, holder.mNoticeImage.getLayoutParams().width, R.drawable.profile, null, 0, 0.0f);
        } else {
            int dimensionInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, context.getResources().getDisplayMetrics());
            holder.mNoticeImage.getLayoutParams().height = dimensionInDp;
            holder.mNoticeImage.requestLayout();
        }

    }

    @Override
    public int getItemCount() {
        return noticArrayList.size();
    }

}
