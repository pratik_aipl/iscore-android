package com.parshvaa.isccore.activity.iconnect.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parshvaa.isccore.activity.iconnect.AddPaperStartTime;
import com.parshvaa.isccore.activity.iconnect.adapter.TestPaperAdapter;
import com.parshvaa.isccore.activity.iconnect.ITestPaper;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TestPaperActivity extends BaseActivity implements AsynchTaskListner, AddPaperStartTime {
    private static final String TAG = "TestPaperActivity";
    public RecyclerView recycler_mcq_sub;
    private TestPaperAdapter adapter;
    public JsonParserUniversal jParser;
    public ArrayList<Subject> EvaSubjectArray = new ArrayList<>();
    public ITestPaper emp;
    public ArrayList<ITestPaper> paperArray = new ArrayList<>();
    public ImageView img_back;
    public TextView tv_title;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public MyDBManager mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_test_paper);
        Utils.logUser();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);


        jParser = new JsonParserUniversal();
        mDb.dbQuery("UPDATE notification SET isTypeOpen = 1, isOpen=1 WHERE ModuleType = 'test_paper'");
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Test Paper ");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);
        recycler_mcq_sub = findViewById(R.id.recycler_paper_sub);

        emptyView.setVisibility(View.GONE);

        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recycler_mcq_sub.addItemDecoration(new ItemOffsetDecoration(spacing));
        showProgress(true, true);
        new CallRequest(TestPaperActivity.this).get_recent_question_paper_activity();
    }

    @Override
    public void onResume() {
        super.onResume();
      //  showProgress(true, true);
      //  new CallRequest(TestPaperActivity.this).get_recent_question_paper_activity();
    }

    private void setupRecyclerView() {
        final Context context = recycler_mcq_sub.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_mcq_sub.setLayoutAnimation(controller);
        recycler_mcq_sub.scheduleLayoutAnimation();
        recycler_mcq_sub.setLayoutManager(new LinearLayoutManager(context));
        adapter = new TestPaperAdapter(paperArray, context);
        recycler_mcq_sub.setAdapter(adapter);
        recycler_mcq_sub.addItemDecoration(new ItemOffsetDecoration(spacing));
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case get_recent_question_paper:

                    Log.d(TAG, "result: "+result);
                    paperArray.clear();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {

                                for (int i = 0; i < jDataArray.length(); i++) {
                                    JSONObject jpaper = jDataArray.getJSONObject(i);
                                    emp = (ITestPaper) jParser.parseJson(jpaper, new ITestPaper());
                                    paperArray.add(emp);
                                }
                                setupRecyclerView();
                                showProgress(false, true);

                            }

                        } else {
                            showProgress(false, true);
                            tv_empty.setText(jObj.getString("message"));
                            recycler_mcq_sub.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        tv_empty.setText("message");
                        recycler_mcq_sub.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                    break;

                    case SendPaperEnterTime:
                        showProgress(false, true);
                        Log.d(TAG, "result: "+result);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jDataArray = jObj.getJSONArray("data");

                        }

                    } catch (JSONException e) {
                        showProgress(false, true);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    @Override
    public void onPaperStartTime(String StudID, String HistoryID, String Type, String Time) {
        showProgress(true,false);
        new CallRequest(TestPaperActivity.this).SendPaperEnterTime(StudID,HistoryID,Type,Time);

    }
}
