package com.parshvaa.isccore.activity.profile;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.activity.library.videoPleyar.utility.Util;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.BoardPaperDownload;
import com.parshvaa.isccore.model.ClassQuestionPaperSubQuestion;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.StudentMcqTestChap;
import com.parshvaa.isccore.model.StudentMcqTestDtl;
import com.parshvaa.isccore.model.StudentMcqTestHdr;
import com.parshvaa.isccore.model.StudentMcqTestLevel;
import com.parshvaa.isccore.model.StudentQuestionPaper;
import com.parshvaa.isccore.model.StudentQuestionPaperChapter;
import com.parshvaa.isccore.model.StudentQuestionPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperDetail;
import com.parshvaa.isccore.model.StudentSetPaperQuestionType;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "MyProfileActivity";
    public App app;
    public WebView progress;
    public ImageView img_edit_prof, img_edit_info;
    public TextView btn_subscrib, tv_licence_status, tv_title, tv_days, tv_medium, tv_board, tv_std, tv_key, tv_mobile,tv_SchoolName, tv_email, tv_name, tv_full_name, tv_stud_id, tv_previous_score, tv_sync_time;

    public ImageView img_back;
    public CircleImageView img_profile;
    public AQuery aQuery;

    public String testHeaderIDS = "";
    public FloatingActionButton fab_sync;


    public MyDBManager mDb;
    public CursorParserUniversal cParse;

    public static StudentMcqTestHdr StudentHdrObj;
    public static StudentMcqTestChap StudentChapObj;
    public static StudentMcqTestDtl mcqTestDtl;
    public static StudentMcqTestLevel mcqTestLevel;

    public static ArrayList<StudentMcqTestHdr> StuHdrArray = new ArrayList<>();
    public ArrayList<StudentQuestionPaper> StuQuePaperArray = new ArrayList<>();
    public ArrayList<BoardPaperDownload> BoardpaperArray = new ArrayList<>();
    public static JsonArray jStudQuePaperArray;
    public static JsonArray jStudHdrArray;
    public static JsonArray jBoardPaperArray;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        Utils.logUser();


        aQuery = new AQuery(this);

        app = App.getInstance();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        cParse = new CursorParserUniversal();
        progress = findViewById(R.id.progress);
        img_edit_prof = findViewById(R.id.img_edit_prof);
        img_edit_info = findViewById(R.id.img_edit_info);
        img_profile = findViewById(R.id.img_profile);

        tv_key = findViewById(R.id.tv_key);
        tv_sync_time = findViewById(R.id.tv_sync_time);
        tv_days = findViewById(R.id.tv_days);
        tv_medium = findViewById(R.id.tv_medium);
        tv_board = findViewById(R.id.tv_board);
        tv_std = findViewById(R.id.tv_std);
        fab_sync = findViewById(R.id.fab_sync);
        tv_mobile = findViewById(R.id.tv_mobile);
        tv_SchoolName = findViewById(R.id.tv_SchoolName);
        tv_email = findViewById(R.id.tv_email);
        tv_name = findViewById(R.id.tv_name);
        tv_full_name = findViewById(R.id.tv_full_name);
        tv_stud_id = findViewById(R.id.tv_stud_id);
        tv_licence_status = findViewById(R.id.tv_licence_status);
        btn_subscrib = findViewById(R.id.btn_subscrib);
        tv_previous_score = findViewById(R.id.tv_previous_score);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> {
            Intent intent = new Intent(MyProfileActivity.this, DashBoardActivity.class);
            startActivity(intent);
        });

        tv_title = findViewById(R.id.tv_title);
        setData();
        img_edit_prof.setOnClickListener(view -> {
            Intent intent = new Intent(MyProfileActivity.this, EditProfileActivity.class);
            startActivity(intent);

        });
        img_edit_info.setOnClickListener(view -> {
            Intent intent = new Intent(MyProfileActivity.this, EditProfileActivity.class);
            startActivity(intent);

        });
        fab_sync.setOnClickListener(v -> {
            if (Utils.isNetworkAvailable(MyProfileActivity.this)) {
                getMCQData();
            } else {
                Utils.showToast(getResources().getString(R.string.conect_internet), MyProfileActivity.this);

            }
        });

        if (mySharedPref.getVersion().equals("0")) {
            tv_licence_status.setText("Licence : Purchased");
            btn_subscrib.setVisibility(View.GONE);
        } else {
            tv_licence_status.setText("Licence : Demo");
            btn_subscrib.setVisibility(View.VISIBLE);
            btn_subscrib.setOnClickListener(v -> startActivity(new Intent(MyProfileActivity.this, ChooseOptionActivity.class)));
        }
        DayCont();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setData() {
        testHeaderIDS = getTestHeaderID();
        int totalAttQue = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQAttempted(testHeaderIDS));
        int totalRightAns = mDb.getTotalUsingJoinQuery(DBQueries.getTotalMCQCorrectAnswers(testHeaderIDS));

        App.mcqTotalAccuracy = 0;
        App.mcqTotalAccuracy = Integer.parseInt(Utils.getAccuraccy(totalAttQue, totalRightAns));


        tv_title.setText("My profile");

        tv_name.setText(MainUser.getFirstName() + " " + MainUser.getLastName());
        tv_full_name.setText(MainUser.getFirstName() + " " + MainUser.getLastName());
        tv_stud_id.setText(MainUser.getStudentCode());
        tv_email.setText(MainUser.getEmailID());
        tv_key.setText(MainUser.getRegKey());
        tv_mobile.setText(MainUser.getRegMobNo());
        tv_SchoolName.setText(TextUtils.isEmpty(MainUser.getSchoolName())?"":MainUser.getSchoolName());
        tv_sync_time.setText(Utils.changeDateAndTimeAMPMFormet(mySharedPref.getLastSyncTime()));
        tv_medium.setText(MainUser.getMediumName());
        tv_std.setText(MainUser.getStanderdName());
        tv_board.setText(MainUser.getBoardName());
        tv_previous_score.setText(Html.fromHtml("Your Previous Year School Result: " + "<font color=\"#0d4568\">" + mySharedPref.getlblPrviousTarget() + "%" + "</font>"));

        if (!TextUtils.isEmpty(mySharedPref.getProf_image())) {
            String ClassLogoName = mySharedPref.getProf_image().substring(mySharedPref.getProf_image().lastIndexOf("/") + 1);
            File imageDirClassLogoName = new File(Constant.LOCAL_IMAGE_PATH + "/profile/");
            File imageFile = new File(imageDirClassLogoName.getAbsolutePath() + "/" + ClassLogoName);
            Log.d(TAG, "setData: " + mySharedPref.getProf_image());
            if (!TextUtils.isEmpty(ClassLogoName)) {
                if (imageFile.exists()) {
                    Utils.setImage(this, imageFile, img_profile,R.drawable.profile);
                } else {
                    if (Utils.isNetworkAvailable(this)) {
                        Util.downloadSubjectIcon(this, aQuery, mySharedPref.getProf_image(), img_profile, imageFile, R.drawable.profile);
                    }
                }
            } else {
                img_profile.setImageResource(R.drawable.profile);
            }
        } else {
            img_profile.setImageResource(R.drawable.profile);
        }

        WebViewSetting(progress, String.valueOf(App.mcqTotalAccuracy), mySharedPref.getlblTarget());
    }

    public String getTestHeaderID() {
        String IDS = "";
        StudentMcqTestHdr stdntHdr;
        Cursor MCqHdr = mDb.getAllRows(DBQueries.getAllRowQuery("student_mcq_test_hdr"));
        if (MCqHdr != null && MCqHdr.moveToFirst()) {
            do {
                stdntHdr = (StudentMcqTestHdr) cParse.parseCursor(MCqHdr, new StudentMcqTestHdr());
                IDS += stdntHdr.getStudentMCQTestHDRID() + ",";
            } while (MCqHdr.moveToNext());
            MCqHdr.close();
        }

        return Utils.removeLastComma(IDS);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void WebViewSetting(WebView webview, String mcqTotalAccuracy, String Goal) {
        WebSettings settings = webview.getSettings();
        //settings.setMinimumFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        webview.setWebViewClient(new myWebClient());
        //webview.loadUrl("file:///android_asset/progressbar/progress.html");
        webview.loadDataWithBaseURL("", ProgressBarHtmL(mcqTotalAccuracy, Goal), "text/html", "UTF-8", "");

    }

    public void DayCont() {
        Calendar start_calendar = Calendar.getInstance();
        Calendar end_calendar = Calendar.getInstance();

        long start_millis = start_calendar.getTimeInMillis(); //get the start time in milliseconds
        end_calendar.set(2020, 3, 30);
        long end_millis = end_calendar.getTimeInMillis();
        //  Utils.Log("TAG ", "MIll :-> " + "Start :-> " + start_millis + " End :" + end_millis + " " + (end_millis - start_millis));// 10 = November, month start at 0 = January
        long total_millis = (end_millis - start_millis); //total time in milliseconds

        //1000 = 1 second interval
        CountDownTimer cdt = new CountDownTimer(total_millis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                millisUntilFinished -= TimeUnit.DAYS.toMillis(days);

                long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);
                tv_days.setText(days + "");
                //    Utils.Log("TAG", "Counter:-> " + days + ":" + hours + ":" + minutes + ":" + seconds); //You can compute the millisUntilFinished on hours/minutes/seconds
            }

            @Override
            public void onFinish() {
                Utils.Log("TAG", "Finish!");
            }
        };
        cdt.start();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        showProgress(false, true);
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj = new JSONObject(result);
                switch (request) {
                    case sendMcqData:
                        jStudHdrArray = null;
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("MCQLastSync")) {
                                String lastMCQSyncTime = jObj.getString("MCQLastSync");
                                mySharedPref.setLastMCQlastSync(lastMCQSyncTime);
                                mySharedPref.setLastSyncTime(lastMCQSyncTime);
                            }
                            if (jObj.has("data") && jObj.getJSONArray("data") != null) {
                                parseWebMCQData(jObj.getJSONArray("data"));
                            }
                        }
                        getGeneratePaperData();
                        break;
                    case sendGanreatPaper:
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("GeneratePaperLastSync")) {
                                String lastGeneratePaperSyncTime = jObj.getString("GeneratePaperLastSync");
                                mySharedPref.setLastGeneratePaperlastSync(lastGeneratePaperSyncTime);
                                mySharedPref.setLastSyncTime(lastGeneratePaperSyncTime);
                            }
                            if (jObj.has("data") && jObj.getJSONArray("data") != null) {
                                parseWebGeneratePaperData(jObj.getJSONArray("data"));
                            }
                        }
                        getBoardPaperData();
                        break;

                    case sendBoardPaperDownload:
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("BoardPaperLastSync")) {
                                String setLastBoardPaperDownloadSyncTime = jObj.getString("BoardPaperLastSync");
                                mySharedPref.setLastBoardPaperDownloadSyncTime(setLastBoardPaperDownloadSyncTime);
                                mySharedPref.setLastSyncTime(setLastBoardPaperDownloadSyncTime);
                            }
                            if (jObj.has("data") && jObj.getJSONArray("data") != null) {
                                parseWebBoardPaperData(jObj.getJSONArray("data"));
                            }
                        }
                        tv_sync_time.setText(Utils.changeDateAndTimeAMPMFormet(mySharedPref.getLastSyncTime()));
                        break;


                }
            } catch (Exception e) {
                showProgress(false, true);
                e.printStackTrace();
            }
        }
    }

    public void parseWebMCQData(JSONArray jArray) {
        if (jArray != null && jArray.length() > 0) {


            for (int i = 0; i < jArray.length(); i++) {
                try {
                    JSONObject obj = jArray.getJSONObject(i);
                    StudentHdrObj = new StudentMcqTestHdr();
                    StudentHdrObj.setStudentID(obj.getString("StudentID"));
                    StudentHdrObj.setBoardID(obj.getString("BoardID"));
                    StudentHdrObj.setMediumID(obj.getString("MediumID"));
                    StudentHdrObj.setSubjectID(obj.getString("SubjectID"));

                    StudentHdrObj.setTimeleft(obj.getString("Timeleft"));
                    StudentHdrObj.setAddType(obj.getString("AddType"));
                    StudentHdrObj.setCreatedBy(obj.getString("CreatedBy"));
                    StudentHdrObj.setCreatedOn(obj.getString("CreatedOn"));
                    StudentHdrObj.setModifiedBy(obj.getString("ModifiedBy"));
                    StudentHdrObj.setModifiedOn(obj.getString("ModifiedOn"));
                    long ID = mDb.insertWebRow(StudentHdrObj, Constant.student_mcq_test_hdr);
                    int inserID = Integer.parseInt(ID + "");
                    if (obj.has("chapterArray")) {
                        JSONArray chapterArray = obj.getJSONArray("chapterArray");
                        if (chapterArray != null && chapterArray.length() > 0) {
                            for (int k = 0; k < chapterArray.length(); k++) {
                                JSONObject chapObj = chapterArray.getJSONObject(k);
                                StudentChapObj = new StudentMcqTestChap();
                                StudentChapObj.setStudentMCQTestHDRID((inserID + ""));
                                StudentChapObj.setChapterid(chapObj.getString("ChapterID"));
                                mDb.insertWebRow(StudentChapObj, Constant.student_mcq_test_chapter);
                            }
                        }
                    }
                    if (obj.has("detailArray")) {
                        JSONArray detailArray = obj.getJSONArray("detailArray");
                        if (detailArray != null && detailArray.length() > 0) {
                            for (int k = 0; k < detailArray.length(); k++) {
                                JSONObject detailObj = detailArray.getJSONObject(k);
                                mcqTestDtl = new StudentMcqTestDtl();
                                mcqTestDtl.setQuestionID(detailObj.getString("QuestionID"));
                                mcqTestDtl.setAnswerID(detailObj.getString("AnswerID"));
                                mcqTestDtl.setIsAttempt(detailObj.getString("IsAttempt"));
                                mcqTestDtl.setSrNo(detailObj.getString("srNo"));
                                mcqTestDtl.setCreatedBy(detailObj.getString("CreatedBy"));
                                mcqTestDtl.setCreatedOn(detailObj.getString("CreatedOn"));
                                mcqTestDtl.setModifiedBy(detailObj.getString("ModifiedBy"));
                                mcqTestDtl.setModifiedOn(detailObj.getString("ModifiedOn"));
                                mcqTestDtl.setStudentMCQTestHDRID(inserID);
                                mDb.insertWebRow(mcqTestDtl, Constant.student_mcq_test_dtl);
                            }
                        }
                    }
                    if (obj.has("levelArray")) {
                        JSONArray levelArray = obj.getJSONArray("levelArray");
                        if (levelArray != null && levelArray.length() > 0) {
                            for (int k = 0; k < levelArray.length(); k++) {
                                JSONObject levelObj = levelArray.getJSONObject(k);
                                mcqTestLevel = new StudentMcqTestLevel();
                                mcqTestLevel.setStudentMCQTestHDRID(inserID + "");
                                mcqTestLevel.setLevelID(levelObj.getString("LevelID"));
                                mDb.insertWebRow(mcqTestLevel, Constant.student_mcq_test_level);
                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public void parseWebGeneratePaperData(JSONArray jArray) {
        if (jArray != null && jArray.length() > 0) {
            try {
                StudentQuestionPaper StudentQuePaeprObj;
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject obj = jArray.getJSONObject(i);

                    StudentQuePaeprObj = new StudentQuestionPaper();
                    StudentQuePaeprObj.setExamTypePatternID(obj.getString("ExamTypePatternID"));
                    StudentQuePaeprObj.setStudentID(obj.getString("StudentID"));
                    StudentQuePaeprObj.setPaperTypeID(obj.getString("PaperTypeID"));
                    StudentQuePaeprObj.setSubjectID(obj.getString("SubjectID"));
                    StudentQuePaeprObj.setTotalMarks(obj.getString("TotalMarks"));
                    StudentQuePaeprObj.setDuration(obj.getString("Duration"));
                    StudentQuePaeprObj.setCreatedBy(obj.getString("CreatedBy"));
                    StudentQuePaeprObj.setCreatedOn(obj.getString("CreatedOn"));
                    StudentQuePaeprObj.setModifiedBy(obj.getString("ModifiedBy"));
                    StudentQuePaeprObj.setModifiedOn(obj.getString("ModifiedOn"));
                    long ID = mDb.insertWebRow(StudentQuePaeprObj, Constant.student_question_paper);
                    StuQuePaperArray.add(StudentQuePaeprObj);
                    int inserID = Integer.parseInt(ID + "");


                    if (obj.has("chapterArray")) {
                        JSONArray chapterArray = obj.getJSONArray("chapterArray");

                        if (chapterArray != null && chapterArray.length() > 0) {
                            StudentQuestionPaperChapter StudentQuePaeprChpObj;
                            for (int k = 0; k < chapterArray.length(); k++) {
                                JSONObject chapObj = chapterArray.getJSONObject(k);
                                StudentQuePaeprChpObj = new StudentQuestionPaperChapter();

                                StudentQuePaeprChpObj.setStudentQuestionPaperID((inserID));
                                StudentQuePaeprChpObj.setChapterID(chapObj.getString("ChapterID"));

                                mDb.insertWebRow(StudentQuePaeprChpObj, Constant.student_question_paper_chapter);
                            }
                        }
                    }
                    if (obj.has("detailArray")) {
                        JSONArray detailArray = obj.getJSONArray("detailArray");

                        if (detailArray != null && detailArray.length() > 0) {
                            StudentQuestionPaperDetail StudentQuePaeprDtlObj;
                            for (int k = 0; k < detailArray.length(); k++) {
                                JSONObject detailObj = detailArray.getJSONObject(k);
                                StudentQuePaeprDtlObj = new StudentQuestionPaperDetail();
                                StudentQuePaeprDtlObj.setExamTypePatternDetailID(detailObj.getString("ExamTypePatternDetailID"));
                                StudentQuePaeprDtlObj.setMasterorCustomized(Integer.parseInt(detailObj.getString("MasterorCustomized")));
                                StudentQuePaeprDtlObj.setMQuestionID(detailObj.getString("MQuestionID"));
                                StudentQuePaeprDtlObj.setCQuestionID(Integer.parseInt(detailObj.getString("CQuestionID")));

                                StudentQuePaeprDtlObj.setStudentQuestionPaperID(inserID);


                                long l_d_id = mDb.insertWebRow(StudentQuePaeprDtlObj, Constant.student_question_paper_detail);
                                int DetailID = Integer.parseInt(l_d_id + "");

                                if (detailObj.has("subQuesArray")) {
                                    JSONArray subQueArray = detailObj.getJSONArray("subQuesArray");

                                    if (subQueArray != null && subQueArray.length() > 0) {
                                        for (int j = 0; j < subQueArray.length(); j++) {
                                            JSONObject subObj = subQueArray.getJSONObject(j);
                                            ClassQuestionPaperSubQuestion sub = new ClassQuestionPaperSubQuestion();
                                            sub.setPaperID(inserID + "");
                                            sub.setMPSQID(subObj.getString("MPSQID"));
                                            sub.setDetailID(DetailID + "");
                                            Utils.Log("Prelim :-> ", sub.getPaperID());
                                            mDb.insertWebRow(sub, Constant.class_question_paper_sub_question);
                                        }
                                    }
                                }

                            }
                        }
                    }

                    if (obj.has("queTypeArray")) {
                        JSONArray queTypeArray = obj.getJSONArray("queTypeArray");

                        if (queTypeArray != null && queTypeArray.length() > 0) {
                            StudentSetPaperQuestionType StudentSetPaperQueTypeObj;
                            for (int k = 0; k < queTypeArray.length(); k++) {
                                JSONObject queTypeObj = queTypeArray.getJSONObject(k);
                                StudentSetPaperQueTypeObj = new StudentSetPaperQuestionType();
                                StudentSetPaperQueTypeObj.setQuestionTypeID(queTypeObj.getString("QuestionTypeID"));
                                StudentSetPaperQueTypeObj.setTotalAsk(queTypeObj.getString("TotalAsk"));
                                StudentSetPaperQueTypeObj.setToAnswer(queTypeObj.getString("ToAnswer"));
                                StudentSetPaperQueTypeObj.setTotalMark(queTypeObj.getString("TotalMark"));

                                StudentSetPaperQueTypeObj.setStudentQuestionPaperID(inserID + "");
                                long QuTypeID = mDb.insertWebRow(StudentSetPaperQueTypeObj, Constant.student_set_paper_question_type);
                                int inserQuTypeID = Integer.parseInt(QuTypeID + "");

                                if (queTypeObj.has("setdetailArray")) {
                                    Utils.Log("TAG ", "in SET detail::->");
                                    JSONArray setdetailArray = queTypeObj.getJSONArray("setdetailArray");

                                    if (setdetailArray != null && setdetailArray.length() > 0) {
                                        StudentSetPaperDetail StudentSetPaperDtlObj;
                                        for (int j = 0; j < setdetailArray.length(); j++) {
                                            Utils.Log("TAG ", "in SET detail  for setdetailArray ::->");
                                            JSONObject setdetailObj = setdetailArray.getJSONObject(j);
                                            StudentSetPaperDtlObj = new StudentSetPaperDetail();
                                            StudentSetPaperDtlObj.setStudentSetPaperQuestionTypeID(inserQuTypeID);
                                            try {
                                                StudentSetPaperDtlObj.setMQuestionID(setdetailObj.getString("MQuestionID"));
                                            } catch (NumberFormatException e) {
                                                e.printStackTrace();
                                                StudentSetPaperDtlObj.setMQuestionID("-2");
                                            }
                                            long l_d_id = mDb.insertWebRow(StudentSetPaperDtlObj, Constant.student_set_paper_detail);
                                            int DetailID = Integer.parseInt(l_d_id + "");
                                            if (setdetailObj.has("subQuesArray")) {
                                                JSONArray subQueArray = setdetailObj.getJSONArray("subQuesArray");
                                                if (subQueArray != null && subQueArray.length() > 0) {
                                                    for (int l = 0; l < subQueArray.length(); l++) {
                                                        JSONObject subObj = subQueArray.getJSONObject(l);
                                                        ClassQuestionPaperSubQuestion sub = new ClassQuestionPaperSubQuestion();
                                                        sub.setPaperID(inserID + "");
                                                        sub.setMPSQID(subObj.getString("MPSQID"));
                                                        sub.setDetailID(DetailID + "");
                                                        Utils.Log("Set Paper :-> ", sub.getPaperID());
                                                        mDb.insertWebRow(sub, Constant.class_question_paper_sub_question);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    Utils.Log("TAG ", "else SET detail::->");

                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void parseWebBoardPaperData(JSONArray jArray) {

        if (jArray != null && jArray.length() > 0) {
            try {
                BoardPaperDownload boardPaperObj;
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject obj = jArray.getJSONObject(i);
                    boardPaperObj = new BoardPaperDownload();
                    boardPaperObj.setBoardPaperAnswers(obj.getString("BoardPaperAnswers"));
                    boardPaperObj.setBoardPaperID(obj.getString("BoardPaperID"));
                    boardPaperObj.setStudentID(obj.getString("StudentID"));
                    boardPaperObj.setSubjectID(obj.getString("SubjectID"));
                    boardPaperObj.setCreatedOn(obj.getString("CreatedOn"));
                    mDb.insertWebRow(boardPaperObj, Constant.board_paper_download);
                    BoardpaperArray.add(boardPaperObj);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class myWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {

        }
    }

    public String ProgressBarHtmL(String mcqTotalAccuracy, String Goal) {
        return "<!DOCTYPE html>" +
                "<head>  " +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
                "<meta charset=\"utf-8\" />" +
                "  <link rel='stylesheet' type='text/css' href='file:///android_asset/bootstrap.min.css'/>" +
/*
                "  <link rel=\"stylesheet\" type=\"text/css\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\" />" +
               */ "  <script type=\"text/javascript\" src=\"file:///android_asset/jquery.min.js\"></script>" +
                "  <script type=\"text/javascript\" src=\"file:///android_asset/bootstrap.min.js\"></script>" +

                "<Script>" +
                "  $(document).ready(function() {" +
                "    $(function () {" +
                "       $('[data-toggle=\"tooltip\"]').tooltip({trigger: 'manual'}).tooltip('show');\n" +
                "  });" +
                "  $(\".progress-bar\").each(function(){" +
                "    each_bar_width = $(this).attr('aria-valuenow');" +
                "    $(this).width(each_bar_width + '%');" +
                "  });" +
                "   });" +
                "  </Script>" +
                "<style>" +
                ".barWrapper{height: 70px;margin-top: 50px;}" +
                "" +
                ".tooltip{ " +
                "  position:relative;" +
                "  float:right;" +
                "} " +
                ".tooltip > .tooltip-inner {" +
                "  padding: 3px 6px;" +
                "  color: #fff;" +
                "  font-weight: bold;" +
                "  font-size: 11px;" +
                "  width: auto;" +
                "  height: auto;" +
                "  border-radius: 4px;" +
                "  line-height: 14px;" +
                "  display: table-cell;" +
                "  vertical-align: middle;" +
                "}" +
                ".tooltip.top{top:-34px !important; opacity: 1; margin-top: 0; width:auto;}" +
                "" +
                ".tooltip.top > .tooltip-inner {  background-color: #f49738; }" +
                "" +
                ".popOver + .tooltip.top > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-top: 6px solid #f49738; left:50% !important}" +
                "" +
                "" +
                "" +
                ".tooltip.bottom > .tooltip-inner {  background-color: #206e9e; }" +
                ".tooltip.bottom{top:12px !important; opacity: 1; margin-top: 0;}" +
                ".popOver + .tooltip.bottom > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-bottom: 6px solid #206e9e;  left:50% !important}" +
                "" +
                "section{" +
                "  margin:100px auto; " +
                "  height:1000px;" +
                "}" +
                ".progress{" +
                "  border-radius:0;" +
                "  overflow:visible;" +
                "  height: 10px;" +
                "  margin-bottom: 0;" +
                "}" +
                ".progress-bar{ " +
                "  -webkit-transition: width 1.5s ease-in-out;" +
                "  transition: width 1.5s ease-in-out;" +
                "}" +
                "" +
                ".me-at.progress{ position: relative; z-index: 11; background: rgba(0,0,0,0);}" +
                ".me-at.progress .progress-bar{   background:#f49738; opacity: 1; z-index: 11}" +
                "" +
                ".mygoal.progress{position: relative;top: -10px; z-index: 1}" +
                "" +
                ".mygoal.progress .progress-bar{   background:#206e9e; opacity: .9; z-index: 11}" +
                "" +
                "</style>" +
                "" +
                "</head>" +
                "<body>" +
                "     " +
                "  <!--<h2 class=\"text-center\">Scroll down the page a bit</h2><br><br> -->" +


                "<div class=\"container\">" +
                "  " +
                "    " +
                "    " +
                "       " +
                "      <div class=\"barWrapper\"> " +
                "        <div class=\"progress me-at\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"" + mcqTotalAccuracy + "\" aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ME (" + mcqTotalAccuracy + "%)\"> </span>     " +
                "          </div> " +
                "        </div>" +
                "        <div class=\"progress mygoal\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"" + mySharedPref.getlblTarget() + "\" aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"MY GOAL (" + mySharedPref.getlblTarget() + "%)\"> </span>     " +
                "          </div>" +
                "        </div>" +
                "      </div>" +
                " " +
                "</div>   " +
                "</body>" +

                "</html>";
    }

    public void getMCQData() {
        showProgress(true, true);//Utils.showProgressDialog(Staticinstance);
        StuHdrArray.clear();
        jStudHdrArray = null;
        try {
            StuHdrArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentMcqTestHdr())
                    .execute(new String[]{DBQueries.getAllStudentHdr(mySharedPref.getLastMCQSyncTime())}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        for (StudentMcqTestHdr header : StuHdrArray) {
            String hdr = String.valueOf(header.getStudentMCQTestHDRID());
            try {
                header.chapterArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentMcqTestChap())
                        .execute(new String[]{DBQueries.getStudentMcqChap(hdr)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            try {
                header.detailArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentMcqTestDtl())
                        .execute(new String[]{DBQueries.getStudentMCQDetail(hdr)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            try {
                header.levelArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentMcqTestLevel())
                        .execute(new String[]{DBQueries.getStudentMcqLevel(hdr)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }


        }

        Gson gson = new GsonBuilder().create();
        jStudHdrArray = gson.toJsonTree(StuHdrArray).getAsJsonArray();
        if (jStudHdrArray.size() > 0) {
            new CallRequest(MyProfileActivity.this).sendMcqDataLogin(jStudHdrArray.toString());
        } else {
            new CallRequest(MyProfileActivity.this).sendMcqDataLogin("[]");
        }
    }

    public void getGeneratePaperData() {
        StuQuePaperArray.clear();
        jStudQuePaperArray = null;
        try {
            StuQuePaperArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentQuestionPaper())
                    .execute(new String[]{DBQueries.getAllStudentQuestionPaper(mySharedPref.getLastGeneratePaperSyncTime())}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        for (StudentQuestionPaper header : StuQuePaperArray) {
            String hdr = String.valueOf(header.getStudentQuestionPaperID());
            if (header.getPaperTypeID().equals("1") || header.getPaperTypeID().equals(1)) {
                StudentQuePaperDetail(header, hdr);
            } else if (header.getPaperTypeID().equals("2") || header.getPaperTypeID().equals(2)) {
                StudentQuePaperChap(header, hdr);
                StudentQuePaperDetail(header, hdr);
            } else {
                StudentQuePaperChap(header, hdr);
                try {
                    header.queTypeArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentSetPaperQuestionType())
                            .execute(new String[]{DBQueries.getStudentSetPaperQueType(hdr)}).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                for (StudentSetPaperQuestionType Quetypeid : header.queTypeArray) {
                    String id = String.valueOf(Quetypeid.getStudentSetPaperQuestionTypeID());
                    try {
                        Quetypeid.setdetailArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentSetPaperDetail())
                                .execute(new String[]{DBQueries.getStudentSetPaperDtl(id)}).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    for (StudentSetPaperDetail detaiObj : Quetypeid.setdetailArray) {
                        String detaiId = String.valueOf(detaiObj.getStudentSetPaperDetailID());

                        try {
                            detaiObj.subQuesArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new ClassQuestionPaperSubQuestion())
                                    .execute(new String[]{DBQueries.getClassSubQue(detaiId)}).get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        }
        Gson gson = new GsonBuilder().create();
        jStudQuePaperArray = gson.toJsonTree(StuQuePaperArray).getAsJsonArray();
        if (jStudQuePaperArray.size() > 0) {
            new CallRequest(MyProfileActivity.this).sendGanreatPaperLogin(String.valueOf(jStudQuePaperArray));
        } else {
            new CallRequest(MyProfileActivity.this).sendGanreatPaperLogin("[]");
        }

    }

    public void getBoardPaperData() {
        BoardpaperArray.clear();
        jBoardPaperArray = null;
        try {
            BoardpaperArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new BoardPaperDownload())
                    .execute(new String[]{DBQueries.getAllBoardPapereDownload(mySharedPref.getLastBoardPaperDownloadSyncTime())}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Gson gson = new GsonBuilder().create();
        jBoardPaperArray = gson.toJsonTree(BoardpaperArray).getAsJsonArray();
        if (jBoardPaperArray.size() > 0) {
            new CallRequest(MyProfileActivity.this).sendBoardPaperDownloadLogin(String.valueOf(jBoardPaperArray));
        } else {
            new CallRequest(MyProfileActivity.this).sendBoardPaperDownloadLogin("[]");
        }
    }

    public void StudentQuePaperDetail(StudentQuestionPaper header, String hdr) {
        try {
            header.detailArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentQuestionPaperDetail())
                    .execute(new String[]{DBQueries.getStudentQuePaperDetail(hdr)}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        for (StudentQuestionPaperDetail detaiObj : header.detailArray) {
            String detaiId = String.valueOf(detaiObj.getStudentQuestionPaperDetailID());
            try {
                detaiObj.subQuesArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new ClassQuestionPaperSubQuestion())
                        .execute(new String[]{DBQueries.getClassSubQue(detaiId)}).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public void StudentQuePaperChap(StudentQuestionPaper header, String hdr) {
        try {
            header.chapterArray = (ArrayList) new CustomDatabaseQuery(MyProfileActivity.this, new StudentQuestionPaperChapter())
                    .execute(new String[]{DBQueries.getStudentQuePaperChap(hdr)}).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
