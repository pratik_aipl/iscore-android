package com.parshvaa.isccore.activity.event;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.EventModel;
import com.parshvaa.isccore.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<EventModel> subCatList;



    public EventListAdpater(Context context, List<EventModel> subCatLists) {
        this.context = context;
        this.subCatList = subCatLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.event_row_item, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        EventModel listModel = subCatList.get(position);


        holder.tvEventBy.setText("By "+listModel.getArtistName());

        if (listModel.getIsEventDemo().equalsIgnoreCase("0")) {

            holder.tvEventName.setText(Html.fromHtml(listModel.getTitle()));
            holder.tvDate.setText(listModel.getStartDate() + " - " + listModel.getEndDate());

            if (!TextUtils.isEmpty(listModel.getBannerImage()))
                Glide.with(context)
                        .load(listModel.getBannerImage())
                        .placeholder(R.drawable.iscore_final_logo)
                        .into(holder.imgEvent);

            Log.d(TAG, "getIsSubscribe: "+listModel.getIsSubscribe());
            if (listModel.getIsSubscribe().equalsIgnoreCase("1")) {
                holder.linCourInfo.setVisibility(View.GONE);
                holder.imgPlay.setVisibility(View.VISIBLE);

                String dateToStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                System.out.println(dateToStr);

                for (int i = 0; i <listModel.getCourseDetails().size() ; i++) {
                    if(Utils.convertDate(listModel.getCourseDetails().get(i).getStartDateTime()).equals(dateToStr)){

                        DateFormat formatt = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.ENGLISH);
                        try {
                            Date Startdate = formatt.parse(listModel.getCourseDetails().get(i).getStartDateTime());
                            Date Enddate = formatt.parse(listModel.getCourseDetails().get(i).getEndDateTime());

                            long Currentmillisecond = new Date().getTime();

                            Date c = Calendar.getInstance().getTime();
                            System.out.println("Current time => " + c);
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            String CurrentDate = df.format(c);
                            String Start_DATE = Utils.convertDate(listModel.getCourseDetails().get(i).getStartDateTime());

                            Log.d(TAG, "startmilli: "+Startdate.getTime());
                            Log.d(TAG, "Endmilli: "+Enddate.getTime());
                            Log.d(TAG, "currentmilli: "+Currentmillisecond);
                            Log.d(TAG, "Start_DATE: "+Start_DATE);
                            Log.d(TAG, "CurrentDate: "+CurrentDate);

                            if(CurrentDate.equals(Start_DATE) && Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                                Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
                                animation.setDuration(1000); //1 second duration for each animation cycle
                                animation.setInterpolator(new LinearInterpolator());
                                animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
                                animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
                                holder.imgPlay.startAnimation(animation); //to start animation
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }



            } else {
                holder.linCourInfo.setVisibility(View.VISIBLE);
                holder.imgPlay.setVisibility(View.GONE);

                holder.tvPrice.setText("\u20B9 " + listModel.getPrice()+" /-");
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.tvActulPrice.setText("\u20B9 " + listModel.getActualPrice()+" /-");
            }

        } else {
            holder.tvEventName.setText(Html.fromHtml(listModel.getDemoTitle()));
            holder.tvDate.setText(listModel.getDemoStartDateTime());

            if (!TextUtils.isEmpty(listModel.getDemoImage()))
                Glide.with(context)
                        .load(listModel.getDemoImage())
                        .centerCrop()
                        .placeholder(R.drawable.iscore_final_logo)
                        .into(holder.imgEvent);

            if (listModel.getIsRegister().equalsIgnoreCase("0")) {
                holder.linCourInfo.setVisibility(View.VISIBLE);
                holder.imgPlay.setVisibility(View.GONE);

                holder.tvPrice.setText("DEMO");
                holder.tvActulPrice.setText("FREE");
            } else {
                holder.linCourInfo.setVisibility(View.GONE);
                holder.imgPlay.setVisibility(View.VISIBLE);

                DateFormat formatt = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.ENGLISH);
                try {
                    Date Startdate = formatt.parse(listModel.getDemoStartDateTime());
                    Date Enddate = formatt.parse(listModel.getDemoEndDateTime());

                    long Currentmillisecond = new Date().getTime();

                    Date c = Calendar.getInstance().getTime();
                    System.out.println("Current time => " + c);
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String CurrentDate = df.format(c);
                    String Start_DATE = Utils.convertDate(listModel.getDemoStartDateTime());

                    Log.d(TAG, "startmilli: "+Startdate.getTime());
                    Log.d(TAG, "Endmilli: "+Enddate.getTime());
                    Log.d(TAG, "currentmilli: "+Currentmillisecond);
                    Log.d(TAG, "Start_DATE: "+Start_DATE);
                    Log.d(TAG, "CurrentDate: "+CurrentDate);

                    if(CurrentDate.equals(Start_DATE) && Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                        Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
                        animation.setDuration(1000); //1 second duration for each animation cycle
                        animation.setInterpolator(new LinearInterpolator());
                        animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
                        animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
                        holder.imgPlay.startAnimation(animation); //to start animation
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

        }


        holder.imgEvent.setOnClickListener(view ->
                context.startActivity(new Intent(context,EventDetailsActivity.class)
        .putExtra("EventData",listModel)));
        holder.tvContinue.setOnClickListener(view ->
                context.startActivity(new Intent(context,EventDetailsActivity.class)
        .putExtra("EventData",listModel)));

        holder.imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listModel.getIsEventDemo().equalsIgnoreCase("0")){
                    String dateToStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    System.out.println(dateToStr);

                    for (int i = 0; i <listModel.getCourseDetails().size() ; i++) {
                        if(Utils.convertDate(listModel.getCourseDetails().get(i).getStartDateTime()).equals(dateToStr)){

                            String EventLink=listModel.getCourseDetails().get(i).getLink();
                            if(listModel.getType().equalsIgnoreCase("url")){
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(EventLink));
                                context.startActivity(browserIntent);
                            }else{
                                DateFormat formatt = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.ENGLISH);
                                try {
                                    Date Startdate = formatt.parse(listModel.getCourseDetails().get(i).getStartDateTime());
                                    Date Enddate = formatt.parse(listModel.getCourseDetails().get(i).getEndDateTime());

                                    long Currentmillisecond = new Date().getTime();

                                    Date c = Calendar.getInstance().getTime();
                                    System.out.println("Current time => " + c);
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                    String CurrentDate = df.format(c);
                                    String Start_DATE = Utils.convertDate(listModel.getCourseDetails().get(i).getStartDateTime());

                                    if(Currentmillisecond>Startdate.getTime() && Currentmillisecond<Enddate.getTime()){
                                        Intent intent = new Intent(context, EventVideoViewActivity.class);
                                        intent.putExtra("FILENAME", EventLink);
                                        context.startActivity(intent);
                                    }else{
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                                        builder1.setMessage("Alert! To view this Not In a Time");
                                        builder1.setCancelable(true);
                                        builder1.setNegativeButton("OK", (dialog, i1) -> dialog.cancel());
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }


                        }
                    }
                }else {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(listModel.getDemoLink()));
                    context.startActivity(browserIntent);
                }
            }
        });
    }


    public void data(){

    }
    @Override
    public int getItemCount() {
        return subCatList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_event)
        ImageView imgEvent;
        @BindView(R.id.tv_event_name)
        TextView tvEventName;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_event_by)
        TextView tvEventBy;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_actul_price)
        TextView tvActulPrice;
        @BindView(R.id.tv_continue)
        TextView tvContinue;
        @BindView(R.id.img_play)
        ImageView imgPlay;
        @BindView(R.id.lin_cour_info)
        LinearLayout linCourInfo;
        @BindView(R.id.lin_main_click)
        LinearLayout lin_main_click;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}