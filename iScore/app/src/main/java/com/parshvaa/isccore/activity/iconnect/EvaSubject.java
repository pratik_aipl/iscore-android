package com.parshvaa.isccore.activity.iconnect;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */

public class EvaSubject implements Serializable {
    public String SubjectID;

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String SubjectName;
}
