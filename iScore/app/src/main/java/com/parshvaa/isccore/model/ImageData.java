package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class ImageData {

    @JsonField
    boolean status;

    @JsonField(name = "root_path")
    String rootPath;
    @JsonField(name = "data")
    ImageRecords imageRecordsList;

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }


    public ImageRecords getImageRecordsList() {
        return imageRecordsList;
    }

    public void setImageRecordsList(ImageRecords imageRecordsList) {
        this.imageRecordsList = imageRecordsList;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
