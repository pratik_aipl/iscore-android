package com.parshvaa.isccore.activity.practiespaper.activity.setpaper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.text.format.DateFormat;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.db.DBQueries;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CursorParserUniversal;
import com.parshvaa.isccore.utils.RomanNumber;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Karan - Empiere on 1/22/2018.
 */

public class SetPreviewPdfActivity extends BaseActivity {

    public String HTML = "", INNER_HTML = "";
    public WebView pdfview;
    public MyDBManager mDb;
    public SetPreviewPdfActivity instance;
    public CursorParserUniversal cParse;

    boolean isLastPrinted = true;
    public FloatingActionButton imgprint;
    public ImageView img_back, img_rotation, img_home;
    public TextView tv_title;
    ArrayList<String> mtcArray = new ArrayList<>();
    ArrayList<String> mtcQArray = new ArrayList<>();
    public String StudentQuestionTypeID = "";
    public int sub_questions_total, sub_question_type_total, n = 1, sq = 0, sub_qt = 0, k = 1, SubQuestionTypeID = -1, sss = 1;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_preview_pdf);
        Utils.logUser();


        instance = this;
        showProgress(true, true);//Utils.showProgressDialog(instance);
        cParse = new CursorParserUniversal();
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        imgprint = findViewById(R.id.imgprint);
        img_home = findViewById(R.id.img_home);
        img_rotation = findViewById(R.id.img_rotation);

        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });

        img_rotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                }
            }
        });

        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        pdfview = findViewById(R.id.pdfView);
        imgprint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                printPDF();
            }
        });

        loadHTML();
    }

    public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)
                .setMessage("Sure about visiting the dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        startActivity(new Intent(instance, DashBoardActivity.class));
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void loadHTML() {

        Date d = new Date();
        CharSequence currDate = DateFormat.format("dd-MMM-yyyy", d.getTime());

        // Utils.Log("Chapter Number in Preivew Set Chapter Load Html", chapnumber);

        HTML = SetHTMLUtils.getPrelimTestHeader(mySharedPref,String.valueOf(currDate),
                App.practiesPaperObj.getSubjectName(),
                App.practiesPaperObj.getChapterNumber(),
                App.practiesPaperObj.getTotalMarks(),
                App.practiesPaperObj.getDuration(),
                "set Paper");


        if (getIntent().getExtras().getBoolean("showAns")) {
            tv_title.setText("Answer Paper");
            new ModelAnsPaper(instance).execute();
        } else {
            tv_title.setText("Question Paper");
            new QuestionPaper(instance).execute();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void printPDF() {
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = pdfview.createPrintDocumentAdapter();
        String jobName = getString(R.string.app_name) + " Report "
                + System.currentTimeMillis();
        PrintAttributes printAttrs = new PrintAttributes.Builder().
                setColorMode(PrintAttributes.COLOR_MODE_MONOCHROME).
                setMediaSize(PrintAttributes.MediaSize.NA_LETTER.asLandscape()).
                setMinMargins(PrintAttributes.Margins.NO_MARGINS).
                build();
        PrintJob printJob = printManager.print(jobName, printAdapter,
                printAttrs);
    }

    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                showProgress(false, true);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public class QuestionPaper extends AsyncTask<String, Void, String> {
        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        String examTypesID = "";

        private QuestionPaper(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);


        }

        @Override
        protected String doInBackground(String... strings) {


            Cursor cQuesType = mDb.getAllRows(DBQueries.getQuestionTypes(App.practiesPaperObj.getStudentQuestionTypeID()));
            ArrayList<String> QuesTypeIds = new ArrayList<>();
            if (cQuesType != null && cQuesType.moveToFirst()) {
                do {
                    QuesTypeIds.add(cQuesType.getString(cQuesType.getColumnIndex("StudentSetPaperQuestionTypeID")));
                } while (cQuesType.moveToNext());
            }

            StudentQuestionTypeID = Utils.convertArraytoCommaSeprated(QuesTypeIds);
//            Utils.Log("TAG StudentQuestionTypeID Query : ", DBQueries.getQuestions(App.practiesPaperObj.getStudentQuestionTypeID()));
            Cursor c = mDb.getAllRows(DBNewQuery.getSetPaperQuestions(App.practiesPaperObj.getStudentQuestionTypeID()));
            int j = 1;
            int p = 0;
            int n = 65;
            ArrayList<String> mtcArray = new ArrayList<>();

            if (c != null && c.moveToFirst()) {
//                Utils.Log("Final Question : ", "row count : " + c.getCount());
                Date d = new Date();
                CharSequence currDate = DateFormat.format("dd-MMM-yyyy", d.getTime());
                // Utils.Log("Chapter Number in Preivew Set Chapter Load Html", chapnumber);

                HTML = new SetHTMLUtils(instance).getPrelimTestHeader(mySharedPref,
                        String.valueOf(currDate),
                        App.practiesPaperObj.getSubjectName(),
                        App.practiesPaperObj.getChapterNumber(),
                        App.practiesPaperObj.getTotalMarks(),
                        App.practiesPaperObj.getDuration(),
                        "set Paper");
                int i = 1;
                int k = 1;
                int outerIndex = 1;
                String QuestionTypeID = "";
                do {
                    // Utils.Log("Cursor Object", DatabaseUtils.dumpCursorToString(c));
                    if (QuestionTypeID.equals("")) {
                        QuestionTypeID = c.getString(c.getColumnIndex("QuestionTypeID"));
                        if (!c.getString(c.getColumnIndex("TotalAsk")).equals(c.getString(c.getColumnIndex("ToAnswer")))) {
                            HTML = HTML + "<br/><tr>\n" +
                                    "                                        <td width=\"5%\" valign=\"top\"><b>Q." + k++ + ")</b></td>\n" +
                                    "                                         <td width=\"90%\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + "( any " + Utils.consvertNumberToString(c.getString(c.getColumnIndex("ToAnswer"))) + " ) </b></td>\n" +
                                    "                                         <td style=\"text-align:right;\" width=\"5%\"><b>" + (int) Double.parseDouble(c.getString(c.getColumnIndex("TotalMark"))) + "</b></td>\n" +
                                    "                                     </tr>";
                        } else {
                            HTML = HTML + "<br/><tr>\n" +
                                    "                                        <td width=\"5%\" valign=\"top\"><b>Q." + k++ + ")</b></td>\n" +
                                    "                                         <td width=\"90%\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + " </b></td>\n" +
                                    "                                         <td style=\"text-align:right;\" width=\"5%\"><b>" + (int) Double.parseDouble(c.getString(c.getColumnIndex("TotalMark"))) + "</b></td>\n" +
                                    "                                     </tr>";
                        }
                    } else if (!c.getString(c.getColumnIndex("QuestionTypeID")).equals(QuestionTypeID)) {
                        i = 1;

                        QuestionTypeID = c.getString(c.getColumnIndex("QuestionTypeID"));

                        ////Utils.Log("Final Question : ", "QuestionTypeID : " +  QuestionTypeID);
                        if (!c.getString(c.getColumnIndex("TotalAsk")).equals(c.getString(c.getColumnIndex("ToAnswer")))) {
                            HTML = HTML + "<tr>\n" +
                                    "                                        <td width=\"5%\"><b>Q." + k++ + ")</b></td>\n" +
                                    "                                         <td width=\"90%\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + "( any " + Utils.consvertNumberToString(c.getString(c.getColumnIndex("ToAnswer"))) + " ) </b></td>" +
                                    "                                         <td style=\"text-align:right;\"  width=\"5%\"><b>" + (int) Double.parseDouble(c.getString(c.getColumnIndex("TotalMark"))) + "</b></td>\n" +
                                    "                                     </tr>";
                        } else {
                            HTML = HTML + "<tr>\n" +
                                    "                                        <td width=\"5%\"><b>Q." + k++ + ")</b></td>\n" +
                                    "                                         <td width=\"90%\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + " </b></td>" +
                                    "                                         <td style=\"text-align:right;\"  width=\"5%\"><b>" + (int) Double.parseDouble(c.getString(c.getColumnIndex("TotalMark"))) + "</b></td>\n" +
                                    "                                     </tr>";
                        }
                    }
                    Utils.Log("Final QuestionType : ", c.getString(c.getColumnIndex("QuestionTypeID")));
                    int isMTC = mDb.getAllRecordsUsingQuery(DBQueries.checkQusTypeisMTC(QuestionTypeID + ""));
                    int IsPassage = mDb.getAllRecordsUsingQuery(DBQueries.checkQusTypeisPassage(c.getString(c.getColumnIndexOrThrow("MQuestionID"))));

                    if (isMTC == 1) {
                        //if (QuestionTypeID.equals("74") || QuestionTypeID.equals("70") || QuestionTypeID.equals("84") || QuestionTypeID.equals("146")) {
                        try {
                            String finalHTML = Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question")));
                            String finalANS = Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer")));
                            ////Utils.Log("Final Question : ", "MQuestionID : " +  c.getString(c.getColumnIndex("MQuestionID")));
                            ////Utils.Log("Final Question : ", "MQuestionID : " +  c.getString(c.getColumnIndex("MQuestionID")));

                            if (!c.getString(c.getColumnIndexOrThrow("Question")).equals("")) {
                                ////Utils.Log("Final Question : ", Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))));
//                                Utils.Log("Going in if  :->  ", "J  = " + j);
                                String HTML1 = "";

                                if ((mtcArray.size() % 3) == 0 && mtcArray.size() > 0) {
                                    j = 1;
                                    n = 65;

                                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                            "<td colspan='2'><br></td></tr>\n";

                                }

                                if (((mtcArray.size() % 3) == 0 && mtcArray.size() > 0) || mtcArray.size() == 0) {
                                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                            "<td align=\"right\"><b>(" + (outerIndex++) + ")&nbsp;</b></td>\n";
                                } else {
                                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                            "<td width=\"30\"><b></b></td>\n";
                                }

                                HTML1 = HTML1 + "<td class=\"question\" ><div style=\"width:43%; float:left; \">"
                                        + j++ + ") " + finalHTML + " </div>" +
                                        "<div style=\"width:3%; float:left;\" >" + (char) n++ + ")</div> " + "<div id='mtc" + (p) + "' style=\"width:50%; float:left;\">" + finalANS + "</div></td>\n";
                                mtcArray.add(finalANS);
                                p++;


                                HTML1 = HTML1 + "<td align=\"right\" width=\"10\"><b></b></td>\n" + "</tr>";

                                HTML = HTML + HTML1;
                            } else {
                                HTML = HTML + "";
                            }
                        } catch (NullPointerException e) {
                            Utils.Log("Final Question : ", "Going Blank");
                            e.printStackTrace();
                            HTML = HTML + "";
                        }
                    } else if (c.getString(c.getColumnIndex("isPassage")).equals("1")) {
                        //INNER_HTML = "";
                        sub_qt = 0;
                        Cursor clabel = mDb.getAllRows(DBQueries.getLabel(c.getString(c.getColumnIndexOrThrow("MQuestionID"))));
                        //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                        if (clabel != null && clabel.moveToFirst()) {

                            k = 1;
                            do {
                                Cursor csqi = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id(App.practiesPaperObj.getStudentQuestionPaperID(),
                                        c.getString(c.getColumnIndex("StudentSetPaperDetailID")),
                                        clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID"))));
                                if (csqi != null && csqi.moveToFirst()) {
                                    sq = 0;
                                    n = 1;
                                    do {

                                        Cursor totla = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id_total(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                c.getString(c.getColumnIndex("StudentSetPaperDetailID")),
                                                clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")),
                                                csqi.getString(csqi.getColumnIndex("SubQuestionTypeID"))));
                                        sub_questions_total = totla.getCount();
                                        totla.close();
                                        if (n == 1) {
                                            Cursor count = mDb.getAllRows(" SELECT DISTINCT(sqt.SubQuestionTypeID)\n" +
                                                    "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                                                    "INNER JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                                                    "INNER JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                                                    "INNER JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                                                    "WHERE `cqpsq`.`PaperID` = '" + App.practiesPaperObj.getStudentQuestionPaperID() + "'\n" +
                                                    "AND `cqpsq`.`DetailID` = '" + c.getString(c.getColumnIndex("StudentSetPaperDetailID")) + "'\n" +
                                                    "AND `psqt`.`PassageSubQuestionTypeID` = '" + clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")) + "'");

                                            if (count != null && count.moveToFirst()) {
                                                sub_question_type_total = count.getCount();
                                                count.close();
                                            }


                                        }
                                        HTML = HTML + "<table>" +
                                                " <tr class=\"margin_top\">\n" +
                                                "<td width=\"30\"></td>\n" +
                                                "<td width=\"30\" style=\"vertical-align: top;\"><b>";
                                        if (sub_qt == 0 && n == 1) {
                                            HTML = HTML + k + " </b></td>";
                                        }
                                        if (SubQuestionTypeID != csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"))) {
                                            SubQuestionTypeID = csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"));
                                            sq++;
                                            sss = 1;
                                            if (n == 1) {
                                                HTML = HTML + "  <td width=\"660\">\n" +
                                                        "<table width=\"100%\">\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "           <b>";
                                                HTML = HTML + csqi.getString(csqi.getColumnIndex("Label")) + ".)" +
                                                        "           </b>\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"630\">\n";

                                                if (sub_question_type_total == 1) {
                                                    HTML = HTML + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                } else {
                                                    HTML = HTML + RomanNumber.toRoman(sq) + ".) " + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                }

                                                HTML = HTML + "        </td>\n";
                                                int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>";
                                            } else {
                                                HTML = HTML + "<td width=\"660\">\n" +
                                                        "<table>\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"600\">\n" +
                                                        RomanNumber.toRoman(sq) + ".) " + csqi.getString(csqi.getColumnIndex("QuestionType")) +
                                                        "       </td>\n";
                                                int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>    ";
                                            }

                                           /* int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>";*/
                                        }

                                        HTML = HTML + "</tr>" +
                                                " <tr class=\"margin_top\">\n" +
                                                "   <td width=\"30\"></td>\n" +
                                                "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                "   <td width=\"660\" style=\"padding-left: 5%;\" class=\"sub\">\n";
                                        if (sub_questions_total == 1) {
                                            HTML = HTML + csqi.getString(csqi.getColumnIndex("Question"));
                                        } else {
                                            HTML = HTML + sss + " .) " + csqi.getString(csqi.getColumnIndex("Question"));
                                        }

                                        HTML = HTML + "   </td>\n" +
                                                "   <td align=\"right\" width=\"30\"></td>\n" +
                                                "</tr>      ";
                                        n++;
                                        sss++;
                                    }
                                    while (csqi.moveToNext());
                                    csqi.close();
                                }
                                if (sub_qt == 0) {
                                    HTML = HTML + "<tr class=\"margin_top\">\n" +
                                            "<td width=\"30\"></td>\n" +
                                            "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                            "<td width=\"660\">" +
                                            Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))) +
                                            "</td>\n" +
                                            "<td align=\"right\" width=\"30\"></td>\n" +
                                            "</tr>     \n";
                                }

                                sub_qt++;
                            }

                            while (clabel.moveToNext());
                            clabel.close();
                            k++;

                        }
                        HTML = HTML + "</table>";

                    } else {
                        try {
                            if (!c.getString(c.getColumnIndexOrThrow("Question")).equals("")) {
                                ////Utils.Log("Final Question : ", Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))));
                                HTML = HTML + "<tr>\n" +
                                        "                                        <td valign=\"top\" width=\"5%\"><b>" + i + ")</b></td>" +
                                        "                                        <td colspan=\"2\" width=\"90%\" class=\"question\">" + Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))) + "</td>\n" +
                                        "                                    </tr>";
                            } else {
                                HTML = HTML + "";
                            }
                        } catch (NullPointerException e) {
                            Utils.Log("Final Question : ", "Going Blank");
                            e.printStackTrace();
                            HTML = HTML + "";
                        }
                    }
                    i++;
                } while (c.moveToNext());
                c.close();


                HTML = HTML + "</table>" +
                        "\n" +
                        "<div style=\"height:180px !important; \"></div>     " +
                        "           </div>    \n" +
                        "            </div>\n" +

                        "       </div> \n" +
                        "" + SetHTMLUtils.getShuffleScript();


                int increment = 1;
                HTML = HTML + " var obj = {};";

                n = 65;
                HTML = HTML + " obj.product" + increment + " = [];";
                for (int a = 1; a < mtcArray.size(); a++) {
                    if (a > 0 && (a % 3 == 0)) {
                        n = 65;
                        increment++;
                        HTML = HTML + " obj.product" + increment + " = [];";

                    }

                    String s = mtcArray.get(a).replaceAll(System.getProperty("line.separator"), "");
                    HTML = HTML + " \n obj.product" + increment + ".push(\"" + s + "\");";
                }


                HTML = HTML + "\n" +
                        "            var $i=1;\n" +
                        "            $.each(obj, function (idx, product) {\n" +
                        "                \n" +
                        "                var shuffledArray = shuffleArray(product);\n" +
                        "                \n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[0];\n" +
                        "                $i++;\n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[1];\n" +
                        "                $i++;\n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[2];\n" +
                        "                $i++;\n" +
                        "                \n" +
                        "            });";


                HTML = HTML + "    </script> </body>\n" +
                        "</html>";
            } else {

                Utils.showToast("Not any Subject Found ", instance);
            }
            return HTML;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String HTML) {
            WebSettings settings = pdfview.getSettings();
            //settings.setMinimumFontSize(30);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setBuiltInZoomControls(true);
            settings.setJavaScriptEnabled(true);
            settings.setSupportZoom(true);
            settings.setDomStorageEnabled(true);
            pdfview.setWebViewClient(new myWebClient());

            pdfview.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
        }
    }

    public class ModelAnsPaper extends AsyncTask<String, Void, String> {

        Context context;
        private MyDBManager mDb;
        public App app;
        public CursorParserUniversal cParse;
        String examTypesID = "";
        int j = 1;
        int p = 0;
        int n = 65;
        int k = 1;


        private ModelAnsPaper(Context context) {
            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);


        }

        @Override
        protected String doInBackground(String... id) {

            Cursor cQuesType = mDb.getAllRows(DBQueries.getQuestionTypes(App.practiesPaperObj.getStudentQuestionTypeID()));
            ArrayList<String> QuesTypeIds = new ArrayList<>();
            if (cQuesType != null && cQuesType.moveToFirst()) {
                do {
                    QuesTypeIds.add(cQuesType.getString(cQuesType.getColumnIndex("StudentSetPaperQuestionTypeID")));
                } while (cQuesType.moveToNext());
            }
            StudentQuestionTypeID = Utils.convertArraytoCommaSeprated(QuesTypeIds);

            Cursor c = mDb.getAllRows(DBNewQuery.getSetPaperQuestions(App.practiesPaperObj.getStudentQuestionTypeID()));
            int j = 1;
            int p = 0;
            int n = 65;
            mtcArray.clear();
            if (c != null && c.moveToFirst()) {

                //  Utils.Log("Final Question : ", "row count : " + c.getCount());
                int outerIndex = 1;
                int i = 1;
                String QuestionTypeID = "";
                do {
                    //  Utils.Log("Going in do  :->  ", "MTC ARRAY SIZE   = " + mtcArray.size());

                    if (QuestionTypeID.equals("")) {
                        //   Utils.Log("Going in QuestionTypeID.equals(\"\")  :->  ", "MTC ARRAY SIZE   = " + mtcArray.size());

                        QuestionTypeID = c.getString(c.getColumnIndex("QuestionTypeID"));
                        HTML = HTML + "<tr>\n" +
                                "<td width=\"40\"><b>Q." + k++ + ")</td>" +
                                "                                         <td width=\"550\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + "( ANY " + Utils.consvertNumberToString(c.getString(c.getColumnIndex("ToAnswer"))) + " ) </b></td>\n" +
                                "                                         <td align=\"right\" width=\"40\"><br/><b>" + (int) Double.parseDouble(c.getString(c.getColumnIndex("TotalMark"))) + "</b></td>\n" +
                                "                                     </tr>";

                    } else if (!c.getString(c.getColumnIndex("QuestionTypeID")).equals(QuestionTypeID)) {
                        //  Utils.Log("Going in QuestionTypeID.equals(\"\")  else  :->  ", "MTC ARRAY SIZE   = " + mtcArray.size());

                        //mtcLastAnsPrint();
                        i = 1;

                        QuestionTypeID = c.getString(c.getColumnIndex("QuestionTypeID"));
                        //  Utils.Log("Final Question : ", "QuestionTypeID : " + QuestionTypeID);

                        HTML = HTML + "<tr>\n" +
                                "<td width=\"40\"><b>Q." + k++ + ")</td>" +

                                "<td colspan=\"2\"><b>" + c.getString(c.getColumnIndex("QuestionType")) + "( ANY " + Utils.consvertNumberToString(c.getString(c.getColumnIndex("ToAnswer"))) + " ) </b></td>" +
                                "<td align=\"right\"><b>" + (int) Double.parseDouble(c.getString(c.getColumnIndex("TotalMark"))) + "</b></td>\n" +
                                " </tr>";

                    }

                    // Utils.Log("Final Question : ", "MQuestionID : " + c.getString(c.getColumnIndex("MQuestionID")));

                    int isMTC = mDb.getAllRecordsUsingQuery(DBQueries.checkQusTypeisMTC(QuestionTypeID + ""));
                    if (isMTC == 1) {
//                if (QuestionTypeID.equals("74") || QuestionTypeID.equals("70") || QuestionTypeID.equals("84") || QuestionTypeID.equals("146")) {
                        try {
                            String finalHTML = Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question")));
                            String finalANS = Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer")));
                            ////Utils.Log("Final Question : ", "MQuestionID : " +  c.getString(c.getColumnIndex("MQuestionID")));
                            ////Utils.Log("Final Question : ", "MQuestionID : " +  c.getString(c.getColumnIndex("MQuestionID")));

                            if (!c.getString(c.getColumnIndexOrThrow("Question")).equals("")) {
                                ////Utils.Log("Final Question : ", Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))));

                                //  Utils.Log("Going in if  :->  ", "J  = " + j);
                                String HTML1 = "";

                                if ((mtcArray.size() % 3) == 0 && mtcArray.size() > 0) {
                                    j = 1;
                                    n = 65;
                                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                            "<td align=\"right\"><b> ANS.&nbsp; </b></td>";
                                    int startIndex = mtcArray.size() - 3;
                                    for (int m = 1; m <= 3; m++) {
                                        if (m != 1) {
                                            HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                                    "<td width=\"30\"><b></b></td>\n";
                                        }
                                        HTML1 = HTML1 + "<td class=\"question\" ><strong><div style=\"width:40%; float:left; \">"
                                                + m + ") " + mtcQArray.get(startIndex) + " </div>" +
                                                "<div style=\"width:5%; float:left;\" >Ans) &nbsp; </div> "
                                                + "<div  style=\"width:50%; float:left;\">" + mtcArray.get(startIndex) + "</div></strong></td></tr>\n";
                                        startIndex++;
                                    }
                                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                            "<td colspan='2'><br></td></tr>\n";
                                }

                                if (((mtcArray.size() % 3) == 0 && mtcArray.size() > 0) || mtcArray.size() == 0) {
                                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                            "<td align=\"right\"><b>(" + (outerIndex++) + ")&nbsp;</b></td>\n";
                                    Utils.Log("TAG ", "in IF::->" + mtcArray.size());
                                } else {
                                    Utils.Log("TAG ", "in else::->" + mtcArray.size());
                                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                                            "<td width=\"30\"><b></b></td>\n";
                                }

                                HTML1 = HTML1 + "<td class=\"question\" ><div style=\"width:43%; float:left; \">"
                                        + j++ + ") " + finalHTML + " </div>" +
                                        "<div style=\"width:3%; float:left;\" >" + (char) n++ + ")</div> " + "<div id='mtc" + (p) + "' style=\"width:50%; float:left;\">" + finalANS + "</div></td>\n";
                                mtcQArray.add(finalHTML);
                                mtcArray.add(finalANS);
                                p++;


                                HTML1 = HTML1 + "<td align=\"right\" width=\"10\"><b></b></td>\n" + "</tr>";

                                HTML = HTML + HTML1;
                            } else {
                                HTML = HTML + "";
                            }
                        } catch (NullPointerException e) {
                            Utils.Log("Final Question : ", "Going Blank");
                            e.printStackTrace();
                            HTML = HTML + "";
                        }
                    } else if (c.getString(c.getColumnIndex("isPassage")).equals("1")) {
                        //INNER_HTML = "";
                        sub_qt = 0;
                        Cursor clabel = mDb.getAllRows(DBQueries.getLabel(c.getString(c.getColumnIndexOrThrow("MQuestionID"))));
                        //     System.out.println("LABEL LOG: -> "+ DatabaseUtils.dumpCursorToString(clabel) );
                        if (clabel != null && clabel.moveToFirst()) {

                            k = 1;
                            do {
                                Cursor csqi = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id(App.practiesPaperObj.getStudentQuestionPaperID(),
                                        c.getString(c.getColumnIndex("StudentSetPaperDetailID")),
                                        clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID"))));
                                if (csqi != null && csqi.moveToFirst()) {
                                    sq = 0;
                                    n = 1;
                                    do {

                                        Cursor totla = mDb.getAllRows(DBQueries.get_paper_master_sub_question_type_by_id_total(App.practiesPaperObj.getStudentQuestionPaperID(),
                                                c.getString(c.getColumnIndex("StudentSetPaperDetailID")),
                                                clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")),
                                                csqi.getString(csqi.getColumnIndex("SubQuestionTypeID"))));
                                        sub_questions_total = totla.getCount();
                                        totla.close();
                                        if (n == 1) {
                                            Cursor count = mDb.getAllRows(" SELECT DISTINCT(sqt.SubQuestionTypeID)\n" +
                                                    "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                                                    "INNER JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                                                    "INNER JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                                                    "INNER JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                                                    "WHERE `cqpsq`.`PaperID` = '" + App.practiesPaperObj.getStudentQuestionPaperID() + "'\n" +
                                                    "AND `cqpsq`.`DetailID` = '" + c.getString(c.getColumnIndex("StudentSetPaperDetailID")) + "'\n" +
                                                    "AND `psqt`.`PassageSubQuestionTypeID` = '" + clabel.getString(clabel.getColumnIndex("PassageSubQuestionTypeID")) + "'");

                                            if (count != null && count.moveToFirst()) {
                                                sub_question_type_total = count.getCount();
                                                count.close();
                                            }


                                        }
                                        HTML = HTML + "<table>" +
                                                " <tr class=\"margin_top\">\n" +
                                                "<td width=\"30\"></td>\n" +
                                                "<td width=\"30\" style=\"vertical-align: top;\"><b>";
                                        if (sub_qt == 0 && n == 1) {
                                            HTML = HTML + k + " </b></td>";
                                        }
                                        if (SubQuestionTypeID != csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"))) {
                                            SubQuestionTypeID = csqi.getInt(csqi.getColumnIndex("SubQuestionTypeID"));
                                            sq++;
                                            sss = 1;
                                            if (n == 1) {
                                                HTML = HTML + "  <td width=\"660\">\n" +
                                                        "<table width=\"100%\">\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "           <b>";
                                                HTML = HTML + csqi.getString(csqi.getColumnIndex("Label")) + ".)" +
                                                        "           </b>\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"630\">\n";

                                                if (sub_question_type_total == 1) {
                                                    HTML = HTML + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                } else {
                                                    HTML = HTML + RomanNumber.toRoman(sq) + ".) " + csqi.getString(csqi.getColumnIndex("QuestionType"));
                                                }
                                                HTML = HTML + "        </td>\n";
                                                int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                //int marks = csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>";
                                            } else {
                                                HTML = HTML + "<td width=\"660\">\n" +
                                                        "<table>\n" +
                                                        "   <tr>\n" +
                                                        "       <td width=\"30\">\n" +
                                                        "       </td>\n" +
                                                        "       <td width=\"600\">\n" +
                                                        RomanNumber.toRoman(sq) + ".) " + csqi.getString(csqi.getColumnIndex("QuestionType")) +
                                                        "       </td>\n";
                                                int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                                HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>" +
                                                        "   </tr>\n" +
                                                        "</table>\n" +
                                                        "</td>    ";
                                            }

                                           /* int marks = sub_questions_total * csqi.getInt(csqi.getColumnIndex("QuestionTypeMarks"));
                                            HTML = HTML + " <td align=\"right\" width=\"30\">" + marks + "</td>";*/
                                        }

                                        HTML = HTML + "</tr>" +
                                                " <tr class=\"margin_top\">\n" +
                                                "   <td width=\"30\"></td>\n" +
                                                "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                "   <td width=\"660\" style=\"padding-left: 5%;\" class=\"sub\">\n";
                                        if (sub_questions_total == 1) {
                                            HTML = HTML +  Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                            HTML = HTML + "   </td>\n" +
                                                    "   <td align=\"right\" width=\"30\"></td>\n" +
                                                    "</tr>" +
                                                    " <tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 5%;\" class=\"sub\">\nAns. ";

                                            HTML = HTML +  Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Answer")));
                                            HTML = HTML + "   </td>\n" +
                                                    "   <td align=\"right\" width=\"30\"></td>\n" +
                                                    "</tr>";

                                        } else {
                                            HTML = HTML + sss + " .) " +  Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Question")));
                                            HTML = HTML + "   </td>\n" +
                                                    "   <td align=\"right\" width=\"30\"></td>\n" +
                                                    "</tr>" +
                                                    " <tr class=\"margin_top\">\n" +
                                                    "   <td width=\"30\"></td>\n" +
                                                    "   <td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                                    "   <td width=\"660\" style=\"padding-left: 5%;\" class=\"sub\">\nAns. ";

                                            HTML = HTML +  Utils.replaceImagesPath(csqi.getString(csqi.getColumnIndex("Answer")));
                                            HTML = HTML + "   </td>\n" +
                                                    "   <td align=\"right\" width=\"30\"></td>\n" +
                                                    "</tr>";
                                        }


                                        n++;
                                        sss++;
                                    }
                                    while (csqi.moveToNext());
                                    csqi.close();
                                }
                                if (sub_qt == 0) {
                                    HTML = HTML + "<tr class=\"margin_top\">\n" +
                                            "<td width=\"30\"></td>\n" +
                                            "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                            "<td width=\"660\">" +
                                            Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))) +
                                            "</td>\n" +
                                            "<td align=\"right\" width=\"30\"></td>\n" +
                                            "</tr>     \n"+
                                            "<tr class=\"margin_top\">\n" +
                                            "<td width=\"30\"></td>\n" +
                                            "<td width=\"30\" style=\"vertical-align: top;\"></td>\n" +
                                            "<td width=\"660\">" +
                                            Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer"))) +
                                            "</td>\n" +
                                            "<td align=\"right\" width=\"30\"></td>\n" +
                                            "</tr>     \n";
                                }

                                sub_qt++;
                            }

                            while (clabel.moveToNext());
                            clabel.close();
                            k++;

                        }
                        HTML = HTML + "</table>";

                    } else {
                        //mtcLastAnsPrint();
                        try {

                            if (!c.getString(c.getColumnIndexOrThrow("Question")).equals("")) {

                                // Utils.Log("Final Question : ", Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))));
                                HTML = HTML + "<tr>\n" +
                                        "                                        <td valign=\"top\"><b>" + i + ")</b></td>" +
                                        "                                        <td colspan=\"2\" class=\"question\">" + Utils.replaceImagesPath(c.getString(c.getColumnIndex("Question"))) + "</td>\n" +
                                        "                                    </tr>";

                                Utils.Log("Final Answer : ", Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer"))));
                                HTML = HTML + "<tr>\n" +
                                        "                                        <td valign=\"top\"><b>ANS</b></td>" +
                                        "                                        <td colspan=\"2\" class=\"question\">" + Utils.replaceImagesPath(c.getString(c.getColumnIndex("Answer"))) + "</td>\n" +
                                        "                                    </tr>";
                            } else {
                                HTML = HTML + "";
                            }
                        } catch (NullPointerException e) {
                            // Utils.Log("Final Question : ", "Going Blank");
                            e.printStackTrace();
                            HTML = HTML + "";
                        }
                    }
                    i++;
                } while (c.moveToNext());
                c.close();
                mtcLastAnsPrint();
                HTML = HTML + "<tr></tr> <tr></tr> <tr></tr> <tr></tr></table> " +
                        "\n" +
                        "<div style=\"height:180px !important; \"></div>     " +
                        "                </div>    \n" +
                        "            </div>\n" +
                        "        </div>\n" +
                        "" + SetHTMLUtils.getRevisionNoteShuffleScript();


                int increment = 1;
                HTML = HTML + " var obj = {};";

                n = 65;
                HTML = HTML + " obj.product" + increment + " = [];";
                for (int a = 1; a < mtcArray.size(); a++) {
                    if (a > 0 && (a % 3 == 0)) {
                        n = 65;
                        increment++;
                        HTML = HTML + " obj.product" + increment + " = [];";

                    }

                    String s = mtcArray.get(a).replaceAll(System.getProperty("line.separator"), "");
                    HTML = HTML + " \n obj.product" + increment + ".push(\"" + s + "\");";
                }


                HTML = HTML + "\n" +
                        "            var $i=1;\n" +
                        "            $.each(obj, function (idx, product) {\n" +
                        "                \n" +
                        "                var shuffledArray = shuffleArray(product);\n" +
                        "                \n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[0];\n" +
                        "                $i++;\n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[1];\n" +
                        "                $i++;\n" +
                        "                document.getElementById('mtc'+$i).innerHTML = shuffledArray[2];\n" +
                        "                $i++;\n" +
                        "                \n" +
                        "            });";


                HTML = HTML + "    </script> </body>\n" +
                        "</html>";
            } else {
                new AlertDialog.Builder(instance)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("oops..!")
                        .setCancelable(false)
                        .setMessage("No Questions Found ...!")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })

                        .show();
          /*  Utils.showToast("Not any Subject Found ", getActivity());*/
            }

            return HTML;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String HTML) {
            WebSettings settings = pdfview.getSettings();
            //settings.setMinimumFontSize(30);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);
            settings.setBuiltInZoomControls(true);
            settings.setJavaScriptEnabled(true);
            settings.setSupportZoom(true);
            settings.setDomStorageEnabled(true);

            pdfview.setWebViewClient(new myWebClient());
            pdfview.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");

        }
    }


    public void mtcLastAnsPrint() {


        ////Utils.Log("Final Answer : reminder :: ", mtcArray.size() + "  :: " + isLastPrinted);

        if (mtcArray.size() > 0 && isLastPrinted) {
            String HTML1 = "";
            HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                    "<td align=\"right\"><b> ANS.&nbsp; </b></td>";

            int reminder = mtcArray.size() % 3;
            ////Utils.Log("Final Answer : reminder :: ", reminder + "");

            if (reminder == 0) {
                reminder = 3;
            }
            int startIndex = mtcArray.size() - reminder;

            for (int m = 1; m <= reminder; m++) {
                if (m != 1) {
                    HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                            "<td width=\"30\"><b></b></td>\n";
                }

                HTML1 = HTML1 + "<td class=\"question\" ><strong><div style=\"width:40%; float:left; \">"
                        + m + ") " + mtcQArray.get(startIndex) + " </div>" +
                        "<div style=\"width:5%; float:left;\" >Ans) &nbsp; </div> "
                        + "<div  style=\"width:50%; float:left;\">" + mtcArray.get(startIndex) + "</div></strong></td></tr>\n";
                startIndex++;
            }

            HTML1 = HTML1 + "<tr class=\"mtc\">\n" +
                    "<td width=\"30\"><b></b></td></br></tr>\n";
            HTML = HTML + HTML1;
            //   isLastPrinted = false;
        }
    }

}
