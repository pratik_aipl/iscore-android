package com.parshvaa.isccore.activity.iconnect.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.parshvaa.isccore.activity.iconnect.fragment.CctTestFrgment;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */

public class CctTestAdapter extends FragmentPagerAdapter {

    int mNumOfTabs;

    public CctTestAdapter(FragmentManager fm, int i) {
        super(fm);
        this.mNumOfTabs = i;
    }

    @Override
    public Fragment getItem(int position) {
        return  new CctTestFrgment().newInstance(position);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position + 1);
    }
}
