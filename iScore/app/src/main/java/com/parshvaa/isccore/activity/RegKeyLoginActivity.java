package com.parshvaa.isccore.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import android.widget.Button;
import android.widget.EditText;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.BuildConfig;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class RegKeyLoginActivity extends BaseActivity implements AsynchTaskListner {

    public RegKeyLoginActivity instance;
    public Button btn_signup, btn_login;
    public EditText etRegKey;
    public String data;
    public MyDBManager mDb;
    public App app;
    String Guid;
    public String DeviceID = "";
    String version = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_regkey_login);
        Utils.logUser();
        instance = this;

        instance = this;
        app = App.getInstance();
        mySharedPref = new MySharedPref(RegKeyLoginActivity.this);
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        etRegKey = findViewById(R.id.et_password);

        if (!mySharedPref.getGuuid().isEmpty()) {
            Guid = mySharedPref.getGuuid();
        } else {
            Guid = UUID.randomUUID().toString();
        }

        btn_login = findViewById(R.id.btn_login);
        btn_signup = findViewById(R.id.btn_signup);
        if (BuildConfig.DEBUG)
            etRegKey.setText("055728004919");
        btn_login.setOnClickListener(view -> {
         btn_login.setEnabled(false);
            if (etRegKey.getText().toString().equals("")) {
                etRegKey.setError("Please enter the valid key.");
            } else {
                if (Utils.isNetworkAvailable(instance)) {
                    showProgress(true, true);
                    new CallRequest(instance).checkKey(etRegKey.getText().toString(), Guid, DeviceID);
                } else {
                    String regKey = etRegKey.getText().toString();
                    if (!mySharedPref.getReg_key().isEmpty()) {
                        if (regKey.equals(mySharedPref.getReg_key()) && Guid.equals(mySharedPref.getGuuid())) {
                            if (mySharedPref.getFirstName().isEmpty() && mySharedPref.getLastName().isEmpty()) {
                                goAheadOffline();
                            } else {
                                redirectToHome();
                            }
                        } else {
                            Utils.showToast("You're not authorized to access the key on this Device.", instance);
                        }
                    } else {
                        Utils.showToast("Please connect internet to Login", instance);
                    }
                }
            }

        });
        btn_signup.setOnClickListener(v -> startActivity(new Intent(instance, RegistrationActivity.class).putExtra(Constant.isDemo,true)));
    }

    public void goAheadOffline() {
        if (mySharedPref.getKeyStatus().equalsIgnoreCase("P")) {
            if (Utils.isNetworkAvailable(instance)) {
                startActivity(new Intent(instance, RegistrationActivity.class)
                       .putExtra("RegKeyLogin", "true")
                        .putExtra(Constant.isDemo,false));
            } else {
                Utils.showToast("Please connect Internet to Register", instance);
            }
        } else {
            startFlow();
        }
    }

    public void redirectToHome() {
        MainUser.setFirstName(mySharedPref.getFirstName());
        MainUser.setLastName(mySharedPref.getLastName());
        MainUser.setRegKey(mySharedPref.getReg_key());
        MainUser.setGuid(mySharedPref.getGuuid());
        MainUser.setClassID(mySharedPref.getClassID());
        MainUser.setBoardID(mySharedPref.getBoardID());
        MainUser.setMediumID(mySharedPref.getMediumID());
        MainUser.setStandardID(mySharedPref.getStandardID());
        MainUser.setEmailID(mySharedPref.getEmailID());
        MainUser.setRegMobNo(mySharedPref.getRegMobNo());
        MainUser.setSubjects(mySharedPref.getSubjects());
        MainUser.setStudentId(mySharedPref.getStudentID());
        MainUser.setStanderdName(mySharedPref.getStandardName());
        MainUser.setClassName(mySharedPref.getClassName());
        MainUser.setKeyStatus(mySharedPref.getKeyStatus());
        MainUser.setBranchID(mySharedPref.getBranchID());
        MainUser.setBatchID(mySharedPref.getBatchID());
        MainUser.setSchoolName(mySharedPref.getSchoolName());
        Intent i = new Intent(instance, DashBoardActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                switch (request) {
                    case checkKey:
                        showProgress(false, true);
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");

                            MainUser.setKeyType(jData.getString("KeyType"));
                            MainUser.setStudentId(jData.getString("StudentID"));
                            MainUser.setBoardID(jData.getString("BoardID"));
                            MainUser.setBoardName(jData.getString("BoardName"));
                            MainUser.setMediumID(jData.getString("MediumID"));
                            MainUser.setMediumName(jData.getString("MediumName"));
                            MainUser.setStandardID(jData.getString("StandardID"));
                            MainUser.setStanderdName(jData.getString("StandardName"));
                            MainUser.setClassID(jData.getString("ClassID"));
                            MainUser.setBranchID(jData.getString("BranchID"));
                            MainUser.setBatchID(jData.getString("BatchID"));
                            MainUser.setRegKey(jData.getString("StudentKey"));
                            MainUser.setActiveKey(jData.getString("ActiveKey"));
                            MainUser.setFirstName(jData.getString("FirstName"));
                            MainUser.setLastName(jData.getString("LastName"));
                            MainUser.setEmailID(jData.getString("EmailID"));
                            MainUser.setRegMobNo(jData.getString("RegMobNo"));

                            App.app_Key = etRegKey.getText().toString();


                            if (Utils.checkOutDated(mySharedPref.getExpiryDate())) {
                                goAhead();
                            } else {
                                new AlertDialog.Builder(this)
                                        .setTitle("Expired")
                                        .setMessage("Your iSccore Product has Expired, Kindly contact Parshwa Publications")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                RegKeyLoginActivity.this.finish();
                                                dialog.dismiss();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }

                        } else {
                            btn_login.setEnabled(true);
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;
                }
            } catch (JSONException e) {
                btn_login.setEnabled(true);

                Utils.showToast("Invalid Register key", this);
                e.printStackTrace();
            }
        }
    }

    public void goAhead() {
        if (Utils.isNetworkAvailable(RegKeyLoginActivity.this)) {
            startActivity(new Intent(RegKeyLoginActivity.this, RegistrationActivity.class)
                    .putExtra("RegKeyLogin", "true").putExtra("isDemo",false));
        } else {
            Utils.showToast("Please connect Internet to Sing-up", RegKeyLoginActivity.this);
        }

    }

    public void startFlow() {
        if (mySharedPref.getDownloadStatus().equals("n")) {
            if (!Utils.isNetworkAvailable(this)) {
                new AlertDialog.Builder(RegKeyLoginActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("oops..!")
                        .setCancelable(false)
                        .setMessage("kindly switch on internet connection!")
                        .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            } else {
//                startActivity(new Intent(RegKeyLoginActivity.this, DataDownloadActivity.class));
                startActivity(new Intent(RegKeyLoginActivity.this, DashBoardActivity.class));
            }
        } else {
//            startActivity(new Intent(RegKeyLoginActivity.this, DataDownloadActivity.class));
            startActivity(new Intent(RegKeyLoginActivity.this, DashBoardActivity.class));
        }
    }
}
