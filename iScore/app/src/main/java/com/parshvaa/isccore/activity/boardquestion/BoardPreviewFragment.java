package com.parshvaa.isccore.activity.boardquestion;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.TextView;

import com.parshvaa.isccore.activity.revisionnote.adapter.PreviewRevisionNoteAdapter;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.TempQuestionTypes;
import com.parshvaa.isccore.utils.ItemOffsetDecoration;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.parshvaa.isccore.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by empiere-vaibhav on 1/20/2018.
 */

public class BoardPreviewFragment extends Fragment {
    public View v;
    private static final String ARG_PAGE_NUMBER = "page_number";
    private static final String ARG_ARRAY = "page_number";
    private ArrayList<String> mKeys=new ArrayList<>();

    private List<TempQuestionTypes> typesArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PreviewRevisionNoteAdapter mAdapter;
    private boolean mFabIsShown;
    public int pageNumber = 0;
    public ArrayList<TempQuestionTypes> tempArray = new ArrayList<>();
    HashMap<String, ArrayList<TempQuestionTypes>> stringArrayListHashMap=new HashMap<>();
    public TextView tv_que_no;


    public Fragment newInstance(int page, ArrayList<TempQuestionTypes> arrayList, HashMap<String, ArrayList<TempQuestionTypes>> stringArrayListHashMap) {
        BoardPreviewFragment fragment = new BoardPreviewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        args.putSerializable("list", (Serializable) arrayList);
        args.putSerializable("hasList", (Serializable) stringArrayListHashMap);
        fragment.setArguments(args);
        return fragment;

    }

//    @Override
//    public void setMenuVisibility(final boolean visible) {
//        try {
//            if (visible) {
//                //  pageNumber = ((BoardPreviewRevisionNoteActivity) getActivity()).viewPager.getCurrentItem();
//                pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
//                stringArrayListHashMap = (HashMap<String, ArrayList<TempQuestionTypes>>) getArguments().getSerializable("hasList");
//                mKeys = new ArrayList<>(stringArrayListHashMap.keySet());
//                typesArrayList = stringArrayListHashMap.get(getKey(pageNumber));
//                setupRecyclerView();
//                tv_que_no.setText(Html.fromHtml("Q-" + (pageNumber + 1) + " " + typesArrayList.get(0).getQuestionType()));
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            typesArrayList = stringArrayListHashMap.get(getKey(pageNumber));
//
//        }
//    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(
                R.layout.fragment_paper_solution, container, false);
        recyclerView = v.findViewById(R.id.recycler_paper);
        int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));

        tv_que_no = v.findViewById(R.id.tv_que_no);

//        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
//        typesArrayList = (List<TempQuestionTypes>) getArguments().getSerializable("list");
//        stringArrayListHashMap = (HashMap<String, ArrayList<TempQuestionTypes>>) getArguments().getSerializable("hasList");

        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
        stringArrayListHashMap = (HashMap<String, ArrayList<TempQuestionTypes>>) getArguments().getSerializable("hasList");
        mKeys = new ArrayList<>(stringArrayListHashMap.keySet());
        typesArrayList = stringArrayListHashMap.get(getKey(pageNumber));
        setupRecyclerView();
        tv_que_no.setText(Html.fromHtml("Q-" + (pageNumber + 1) + " " + typesArrayList.get(0).getQuestionType()));


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    hideFab();
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    hideFab();
                } else {
                    showFab();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        setupRecyclerView();

        return v;

    }

    public String getKey(int position) {
        Utils.Log("mKeys List size", "==>" + mKeys.size());
        Utils.Log("In Fragment Key", "==>" + mKeys.get(position));
        return mKeys.get(position);
    }

    private void showFab() {
        if (!mFabIsShown) {
            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_left_button).scaleX(1).scaleY(1).setDuration(200).start();
            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_right_button).scaleX(1).scaleY(1).setDuration(200).start();
            mFabIsShown = true;
        }
    }

    private void hideFab() {
        if (mFabIsShown) {
            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_right_button).scaleX(0).scaleY(0).setDuration(200).start();

            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((BoardPreviewRevisionNoteActivity) getActivity()).float_left_button).scaleX(0).scaleY(0).setDuration(200).start();
            mFabIsShown = false;
        }
    }

    private void setupRecyclerView() {
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        mAdapter = new PreviewRevisionNoteAdapter(typesArrayList, getActivity(), recyclerView);
        recyclerView.setAdapter(mAdapter);
    }
}


