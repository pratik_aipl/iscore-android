package com.parshvaa.isccore.db;

import android.util.Log;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.TempMcqTestDtl;
import com.parshvaa.isccore.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Karan - Empiere on 2/28/2017.
 */

public class DBQueries {

    private static final String TAG = "DBQueries";
    public App app;


    public void DBQueries() {
        app = App.getInstance();
    }

    public static String getAllSubjects() {

        ArrayList<QueryParam> maps = new ArrayList<>();

        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("SubjectID IN ", "(" + MainUser.subjects + ")"));
        maps.add(new QueryParam("ext", "AND"));
        maps.add(new QueryParam("BoardID", MainUser.BoardID));
        maps.add(new QueryParam("ext", "AND"));
        maps.add(new QueryParam("MediumID", MainUser.MediumID));
        maps.add(new QueryParam("ext", "AND"));
        maps.add(new QueryParam("StandardID", MainUser.StandardID));

        maps.add(new QueryParam("ext", "ORDER BY SubjectOrder ASC"));
        return getAllQuery("subjects", maps);
    }

    public static String getAllSubjectBoardpaper() {

        return "SELECT distinct s.* FROM subjects s JOIN board_paper_files bpf on s.SubjectID = bpf.SubjectID where s.SubjectID IN (" + MainUser.subjects + ") AND s.BoardID=" + MainUser.BoardID + " AND s.MediumID=" + MainUser.MediumID + " AND s.StandardID=" + MainUser.StandardID + " ORDER BY SubjectOrder ASC";
    }

    public static String getAllBoardpaper(String sujectID) {

        return "SELECT distinct bp.* FROM boards_paper bp JOIN board_paper_files bpf on bp.BoardPaperID = bpf.BoardPaperID " +
                "AND bpf.SubjectID = " + sujectID;
    }

    public static String getAllSubjectMCQ() {
        Utils.Log("TAG mcq QUERY::--> ", "SELECT * FROM subjects where SubjectID IN (" + MainUser.subjects + ") AND BoardID=" + MainUser.BoardID + " AND MediumID=" + MainUser.MediumID + " AND StandardID=" + MainUser.StandardID + " AND isMCQ='1' ORDER BY SubjectOrder ASC");
        return "SELECT * FROM subjects where SubjectID IN (" + MainUser.subjects + ") AND BoardID=" + MainUser.BoardID + " AND MediumID=" + MainUser.MediumID + " AND StandardID=" + MainUser.StandardID + " AND isMCQ = 1 ORDER BY SubjectOrder ASC";
    }

    public static String getChapter(String QuePaperID) {

        return "SELECT * FROM student_question_paper_chapter where StudentQuestionPaperID = " + QuePaperID;
    }

    public static String getChapterNumber(String ChapterID) {
        Utils.Log("TAG QUERY::-->", "SELECT GROUP_CONCAT(DISTINCT ChapterNumber) as ChapterNumber FROM chapters WHERE ChapterID IN (" + ChapterID + ")");
        return "SELECT GROUP_CONCAT(DISTINCT ChapterNumber) as ChapterNumber FROM chapters WHERE ChapterID IN (" + ChapterID + ")";
    }

    public static String getAllSubjectGenrate() {
        return "SELECT * FROM subjects where SubjectID IN (" + MainUser.subjects + ") AND BoardID=" + MainUser.BoardID + " AND MediumID=" + MainUser.MediumID + " AND StandardID=" + MainUser.StandardID + " AND isGeneratePaper='1' ORDER BY SubjectOrder ASC";
    }

    public static String getStudentMcqDtl(String HDRid) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentMCQTestHDRID", HDRid));
        return getAllQuery("student_mcq_test_dtl", maps);
    }

    public static String getStudentMcqLevel(String HDRid) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentMCQTestHDRID", HDRid));
        return getAllQuery("student_mcq_test_level", maps);
    }

    public static String getStudentMcqChap(String HDRid) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentMCQTestHDRID", HDRid));
        return getAllQuery("student_mcq_test_chapter", maps);
    }


    public static String getAllStudentHdr(String lastSyncTime) {
        if (lastSyncTime.isEmpty()) {
//            Utils.Log("TAG Query : ", "Select * From 'student_mcq_test_hdr' where null'" + lastSyncTime + "'");
            return "Select * From 'student_mcq_test_hdr'";
        } else {
//            Utils.Log("TAG Query : ", "Select * From 'student_mcq_test_hdr' where CreatedOn > '" + lastSyncTime + "'");
            return "Select * From 'student_mcq_test_hdr' where CreatedOn > '" + lastSyncTime + "'";
        }

    }

    public static String getNotAppeared() {
        return "Select * From student_not_appeared_question";
    }

    public static String getIncorrect() {
        return "Select * From student_incorrect_question";
    }

    public static String getAllStudentQuestionPaper(String lastSyncTime) {
        if (lastSyncTime.isEmpty()) {
            return "Select * From 'student_question_paper'";
        } else {
            return "Select * From 'student_question_paper' where CreatedOn > '" + lastSyncTime + "'";
        }

    }

    public static String getAllBoardPapereDownload(String lastSyncTime) {
        if (lastSyncTime.isEmpty()) {
//            Utils.Log("TAG Query : ", "Select * From 'board_paper_download' where null'" + lastSyncTime + "'");

            return "Select * From 'board_paper_download'";
        } else {
//            Utils.Log("TAG Query : ", "Select * From 'board_paper_download' where CreatedOn > '" + lastSyncTime + "'");
            return "Select * From 'board_paper_download' where CreatedOn > '" + lastSyncTime + "'";
        }
    }

    public static String getStudentQuePaperChap(String HDRid) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentQuestionPaperID", HDRid));

        return getAllQuery("student_question_paper_chapter", maps);
    }

    public static String getStudentQuePaperDetail(String HDRid) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentQuestionPaperID", HDRid));
        return getAllQuery("student_question_paper_detail", maps);
    }

    public static String getStudentSetPaperQueType(String HDRid) {

        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentQuestionPaperID", HDRid));
        return getAllQuery("student_set_paper_question_type", maps);

    }

    public static String getStudentSetPaperDtl(String id) {

        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentSetPaperQuestionTypeID", id));
        return getAllQuery("student_set_paper_detail", maps);
    }

    public static String getClassSubQue(String id) {
        return "SELECT * FROM `class_question_paper_sub_question` where DetailID = " + id;
    }

    /* public static String getStudentMcqChap(int HDRid){
         return "Select * FROM 'student_mcq_test_chapter' where StudentMCQTestHDRID "+HDRid;
     }
     public static String getStudentMcqDtl(int HDRid){
         return "Select * FROM 'student_mcq_test_dtl' where StudentMCQTestHDRID "+HDRid;
     }*/
    public static String getAllSubject() {
        return "SELECT * FROM `subjects` where SubjectID IN '(" + MainUser.subjects + ")' AND BoardID '" + MainUser.BoardID + "' AND MediumID '" + MainUser.MediumID + "' AND StandardID '" + MainUser.StandardID + "' AND  ORDER BY SubjectOrder'" + " ASC";
    }

    public static String getAllNotification() {
        return "SELECT * FROM notification";

    }

    public static String getAllPaperDtl() {
        return "SELECT * FROM student_question_paper_detail";

    }

    public static String getCctReport() {
        return "SELECT * FROM class_evaluater_cct_count";

    }

    public static String deletCompletedNotification(String id) {
        return "delete from notification where id IN (" + id + ")";
    }

    public static String getBoardPAPER(String subjectid) {
        ArrayList<QueryParam> maps = new ArrayList<>();

        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("SubjectID", subjectid));
        return getAllQuery("boards_paper", maps);
    }

    public static String getGanratPDF(String subjectid, String Boardpaperid) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("SubjectID", subjectid));
        maps.add(new QueryParam("ext", "AND"));
        maps.add(new QueryParam("BoardPaperID", Boardpaperid));

        return getAllQuery("board_paper_files", maps);
    }

    public static String getAllMcqDtl(TempMcqTestDtl tempmcqdtl) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("QuestionID", tempmcqdtl.getQuestionID()));
       /* maps.add(new QueryParam("ext", "AND"));
        maps.add(new QueryParam("AnswerID", tempmcqdtl.getAnswerID()));*/
        return getAllQuery("temp_mcq_test_dtl", maps);
    }

    /*
      public static String getAllEvaluterMcq(EvaluterMcqTestMain tempmcqdtl) {
          maps.clear();
          maps.add(new QueryParam("ext", "where"));
          maps.add(new QueryParam("QuestionID", tempmcqdtl.getQuestionID()));
         *//* maps.add(new QueryParam("ext", "AND"));
        maps.add(new QueryParam("AnswerID", tempmcqdtl.getAnswerID()));*//*
        return getAllQuery("class_evaluater_mcq_test_temp", maps);
    }
    public static String getAllSTudentMCQDtl() {
        maps.clear();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("isAttempt", "1"));
        return getAllQuery("student_mcq_test_dtl", maps);
    }
*/
    public static String getSubjectWiseQuesTypes(String SubjectId) {
        return "SELECT * FROM `question_types` where SubjectID='" + SubjectId + "' ORDER BY RevisionNo";
    }

    public static String getSubjectWiseQuesTypes(String SubjectId, String ChapterIds) {
        //return "SELECT * FROM `question_types` where SubjectID='"+SubjectId+"' ORDER BY RevisionNo";
        return "SELECT `qt`.`QuestionTypeID`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `qm` " +
                "JOIN `question_types` `qt` ON `qt`.`QuestionTypeID` = `qm`.`QuestionTypeID` " +
                "WHERE `qm`.`SubjectID` = '" + SubjectId + "' AND `qm`.`ChapterID` IN (" + ChapterIds + ") " +
                "GROUP BY `qt`.`QuestionTypeID` ORDER BY `qt`.`RevisionNo` ASC";

    }

    public static String getSubjectWiseQuesTypesSetpaper(String SubjectId, String ChapterIds) {
        //return "SELECT * FROM `question_types` where SubjectID='"+SubjectId+"' ORDER BY RevisionNo";
        return "SELECT COUNT(qm.MQuestionID) as TotalQuestion, `qt`.`QuestionTypeID`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `qm` " +
                "JOIN `question_types` `qt` ON `qt`.`QuestionTypeID` = `qm`.`QuestionTypeID` " +
                "WHERE `qm`.`SubjectID` = '" + SubjectId + "' AND `qm`.`ChapterID` IN (" + ChapterIds + ")" +
                "GROUP BY `qt`.`QuestionTypeID` ";


    }

    public static String getMCQAnswers(String questionID) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("MCQQuestionID", questionID));

        return getAllQuery("mcqoption", maps);
    }

    public static String getChapters(String SubjectID) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("SubjectID", SubjectID));
        return getAllQuery("chapters", maps);
    }

    public static String getChapterMCQ(String SubjectID) {
//        Utils.Log("TAG QUERY ::-->", "select  * from chapters where SubjectID = " + SubjectID + " AND ParentChapterID = 0 ORDER BY DisplayOrder ASC ");
        return "select  * from chapters where SubjectID = " + SubjectID + " AND ParentChapterID = 0 AND isMCQ = 1 ORDER BY DisplayOrder ASC ";

    }

    public static String getEvaMCQ() {
        return "select * from class_evaluater_mcq_test_temp";
    }

    public static String getSubChapter(String ParentChapterID) {
        return "select  * from chapters where ParentChapterID = " + ParentChapterID + " AND isMCQ = 1 ORDER BY DisplayOrder ASC ";
    }

    public static String getChapterGenrate(String SubjectID) {
//        Utils.Log("TAG QUERY ::-->", "select  * from chapters where SubjectID = " + SubjectID + " AND ParentChapterID = 0 ORDER BY DisplayOrder ASC ");
        return "select  * from chapters where SubjectID = " + SubjectID + " AND ParentChapterID = 0 AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC ";

    }

    public static String getSubChapterGenrate(String ParentChapterID) {
        return "select  * from chapters where ParentChapterID = " + ParentChapterID + " AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC ";
    }


    public static String getStudentMCQChapters(String headerID) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentMCQTestHDRID", headerID));
        return getAllQuery("student_mcq_test_chapter", maps);

    }

    public static String getlevelid1() {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("LevelID", "1"));
        return getAllQuery("student_mcq_test_hdr", maps);
    }

    public static String getlevelid2() {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("LevelID", "2"));
        return getAllQuery("student_mcq_test_hdr", maps);
    }

    public static String getlevelid3() {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("LevelID", "3"));
        return getAllQuery("student_mcq_test_hdr", maps);
    }

    public static String getStudentMCQDetail(String headerID) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentMCQTestHDRID", headerID));
        return getAllQuery("student_mcq_test_dtl", maps);
    }

    public static String getStudentMCQDetai(String headerID) {
        //maps.clear();
        // maps.add(new QueryParam("ext", "where"));
        // maps.add(new QueryParam("StudentMCQTestHDRID", headerID));
        //return getAllQuery("student_mcq_test_dtl", maps);
        return " select  * from student_mcq_test_dtl where StudentMCQTestHDRID = " + headerID + " ORDER BY StudentMCQTestDTLID ASC";
    }

    public static String getStudentMCQAttempted(String headerID) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("StudentMCQTestHDRID", headerID));
        maps.add(new QueryParam("ext", "AND"));
        maps.add(new QueryParam("IsAttempt", "1"));


        return getAllQuery("student_mcq_test_dtl", maps);
    }

    public static String getAttemptedQuetion(String quetinoID) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("MCQQuestionID IN ", "(" + quetinoID + ")"));
        maps.add(new QueryParam("ext", ""));
        return getAllQuery("mcqquestion", maps);
    }

    public static String getRandomExamTypeTest(String examTypeID, String paperTypeId) {
        String query = " select  * from  examtypes where ExamTypeID IN (" + examTypeID + ") AND (PaperType LIKE '" + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + "' OR PaperType = '" + paperTypeId + "') ORDER BY RANDOM() LIMIT 1";

        Log.d(TAG, "getRandomExamTypeTest: " + query);
        return query;


    }

    public static String getExamtypesID(String subjectId) {
        ArrayList<QueryParam> maps = new ArrayList<>();
        maps.add(new QueryParam("ext", "where"));
        maps.add(new QueryParam("subjectID", subjectId));
        Log.d(TAG, "getExamtypesID: " + subjectId);
        return getCustomFields("ExamTypeID", "examtype_subject", maps);
    }

    public static String getRandomSubQuestionSubjectWise(String subjectId, String questionTypeID, String CheckQuestionId) {
        if (CheckQuestionId.length() > 1) {
            CheckQuestionId = CheckQuestionId.replaceAll(",$", "");
        }
        String query = "select * from masterquestion where SubjectID = '" + subjectId + "' AND QuestionTypeID = '" + questionTypeID + "' AND MQuestionID NOT IN (" + CheckQuestionId + ") ORDER BY Random() LIMIT 1";
        Log.d(TAG, "getRandomSubQuestionSubjectWise: " + query);
        return query;
    }

    public static String getExamTypePatternId(String subjectId, String examType) {
        String q = "select ExamTypePatternID from exam_type_pattern where ExamTypeID='" + examType + "' AND SubjectID='" + subjectId + "' ORDER BY Random()";
        Log.d(TAG, "getExamTypePatternId: " + q);
        return q;
    }

    public static String getExamTypePatternCount(String subjectId, String paperType) {
        //return "select ExamTypePatternID from exam_type_pattern where ExamTypeID='" + examType + "' AND SubjectID='" + subjectId + "'";
        return "SELECT ExamTypePatternID FROM exam_type_pattern ETP INNER JOIN examtypes  ET ON ETP.ExamTypeID " +
                "= ET.ExamTypeID WHERE ETP.SubjectID=" + subjectId + " AND ET.PaperType LIKE ('%" + paperType + "%')";

    }

    public static String getExamTypePatternIdBySubject(String subjectId) {
        return "select ExamTypePatternID from exam_type_pattern where SubjectID='" + subjectId + "'";
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT 1";
    }

    public static String getExamTypePatternIdByExamType(String examType) {
        return "select ExamTypePatternID from exam_type_pattern where ExamTypeID='" + examType + "'";
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT 1";
    }

    public static String getRandomSubQuestion(String ChapterIds, String QuestionType) {
        return "select distinct * from masterquestion where ChapterID IN (" + ChapterIds + ") AND QuestionTypeID = '" + QuestionType + "' ORDER BY Random() LIMIT 1";
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT 1";
    }

    public static String getRandomSubQuestionWithID(String ChapterIds, String QuestionType, String QuesID) {
        if (QuesID.length() > 1) {
            QuesID = QuesID.replaceAll(",$", "");
        }
        String query = "";
        query = "select distinct * from masterquestion where QuestionTypeID = '" + QuestionType + "'";
        if (!ChapterIds.equals("")) {
            query += " AND ChapterID IN (" + ChapterIds + ") ";
        }

        if (!QuesID.equals("")) {
            query += " AND MQuestionID NOT IN (" + QuesID + ") ";
        }

        query += " ORDER BY Random() LIMIT 1 ";

        Log.d(TAG, "getRandomSubQuestionWithID: " + query);
        return query;
    }

    public static String checkQusTypeisMTC(String QuesID) {
        return "select * from question_types where QuestionTypeID = " + QuesID + " AND isMTC='1'";
    }

    public static String checkQusTypeisPassage(String QuesID) {
        Utils.Log("Is PAssage :-> ", "SELECT `isPassage` FROM `question_types` WHERE QuestionTypeID = (SELECT QuestionTypeID FROM masterquestion WHERE MQuestionID =" + QuesID + ")");
        return "SELECT `isPassage` FROM `question_types` WHERE QuestionTypeID = (SELECT QuestionTypeID FROM masterquestion WHERE MQuestionID =" + QuesID + ")";
    }

    public static String getRandomSubQuestion(String ChapterIds, String QuestionType, String Limit) {
        return "select distinct * from masterquestion where ChapterID IN (" + ChapterIds + ") AND QuestionTypeID = '" + QuestionType + "' ORDER BY Random() LIMIT " + Limit;
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT "+Limit;
    }

    public static String getLabel(String MQuestionID) {
        Utils.Log("LABEL :-> ", "SELECT PassageSubQuestionTypeID,Label, Mark FROM passage_sub_question_type WHERE QuestionTypeID = (SELECT QuestionTypeID FROM masterquestion WHERE MQuestionID=" + MQuestionID);
        return "SELECT PassageSubQuestionTypeID,Label, Mark FROM passage_sub_question_type WHERE QuestionTypeID = (SELECT QuestionTypeID FROM masterquestion WHERE MQuestionID=" + MQuestionID + ")";
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT "+Limit;
    }

    public static String getPassageSubquetions(String MPSQID, String MQuestionID, String QuestionTypeID, String Mark, String PSQTID) {
        String query = "SELECT sqt.Marks, mpsb.MPSQID, psqtt.PassageSubQuestionTypeID, mpsb.SubQuestionTypeID\n" +
                " FROM master_passage_sub_question as mpsb LEFT JOIN passage_sub_question_type as psqtt ON\n" +
                " mpsb.PassageSubQuestionTypeID=psqtt.PassageSubQuestionTypeID JOIN sub_question_types as sqt ON\n" +
                " mpsb.SubQuestionTypeID=sqt.SubQuestionTypeID where mpsb.PassageSubQuestionTypeID=" + PSQTID + " AND mpsb.MQuestionID = " + MQuestionID +
                " AND mpsb.MPSQID NOT IN(" + MPSQID + ") AND mpsb.PassageSubQuestionTypeID =\n" +
                " (SELECT PassageSubQuestionTypeID from passage_sub_question_type as psqt WHERE\n" +
                " psqt.QuestionTypeID=" + QuestionTypeID + " AND psqt.PassageSubQuestionTypeID=mpsb.PassageSubQuestionTypeID)\n" +
                " AND sqt.Marks <= " + Mark + " ORDER BY Random() LIMIT 1";
        Utils.Log("getPassageSubquetions", query);
        return query;
    }

    public static String getPassageSubquetionsRivistion(String MPSQID, String MQuestionID, String QuestionTypeID, String Mark, String PSQTID) {
        String query = "SELECT sqt.Marks, mpsb.MPSQID, mpsb.Question as SubQuestion, mpsb.Answer as SubAnswer, psqtt.PassageSubQuestionTypeID, mpsb.SubQuestionTypeID, sqt.QuestionType as SubQuestionType \n" +
                " FROM master_passage_sub_question as mpsb LEFT JOIN passage_sub_question_type as psqtt ON\n" +
                " mpsb.PassageSubQuestionTypeID=psqtt.PassageSubQuestionTypeID JOIN sub_question_types as sqt ON\n" +
                " mpsb.SubQuestionTypeID=sqt.SubQuestionTypeID where mpsb.PassageSubQuestionTypeID=" + PSQTID + " AND mpsb.MQuestionID = " + MQuestionID +
                " AND mpsb.MPSQID NOT IN(" + MPSQID + ") AND mpsb.PassageSubQuestionTypeID =\n" +
                " (SELECT PassageSubQuestionTypeID from passage_sub_question_type as psqt WHERE\n" +
                " psqt.QuestionTypeID=" + QuestionTypeID + " AND psqt.PassageSubQuestionTypeID=mpsb.PassageSubQuestionTypeID)\n" +
                "ORDER BY psqtt.PassageSubQuestionTypeID";
        Utils.Log("getPassageSubquetions", query);
        return query;
    }

    public static String get_paper_master_sub_question_type_by_id(String PaperID, String DetailID, String PassageSubQuestionTypeID) {
        String query = "SELECT `Question`, `Answer`, `cqpsq`.`MPSQID`, `sqt`.`SubQuestionTypeID`, \n" +
                "`cqpsq`.`ClassQuestionPaperSubQuestionID`, `cqpsq`.`DetailID`, `psqt`.`Label`, `psqt`.`Mark`,\n" +
                " `sqt`.`QuestionType`, `mpsq`.`SubQuestionTypeID`, `psqt`.`PassageSubQuestionTypeID`,\n" +
                " `sqt`.`Marks` as `QuestionTypeMarks`\n" +
                "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                "JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                "JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                "JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                "WHERE `cqpsq`.`PaperID` = '" + PaperID + "'\n" +
                "AND `cqpsq`.`DetailID` = '" + DetailID + "'\n" +
                "AND `psqt`.`PassageSubQuestionTypeID` = '" + PassageSubQuestionTypeID + "'";
        Utils.Log("get_paper_master_sub_question_type_by_id", query);
        return query;
    }

    public static String get_paper_master_sub_question_type_by_id_total(String PaperID, String DetailID, String PassageSubQuestionTypeID, String subQueTypeID) {
        String query = "SELECT `Question`, `Answer`, `cqpsq`.`MPSQID`, `sqt`.`SubQuestionTypeID`, \n" +
                "`cqpsq`.`ClassQuestionPaperSubQuestionID`, `cqpsq`.`DetailID`, `psqt`.`Label`, `psqt`.`Mark`,\n" +
                " `sqt`.`QuestionType`, `mpsq`.`SubQuestionTypeID`, `psqt`.`PassageSubQuestionTypeID`,\n" +
                " `sqt`.`Marks` as `QuestionTypeMarks`\n" +
                "FROM `class_question_paper_sub_question` `cqpsq`\n" +
                "JOIN `master_passage_sub_question` `mpsq` ON `cqpsq`.`MPSQID` = `mpsq`.`MPSQID`\n" +
                "JOIN `passage_sub_question_type` `psqt` ON `mpsq`.`PassageSubQuestionTypeID` = `psqt`.`PassageSubQuestionTypeID`\n" +
                "JOIN `sub_question_types` `sqt` ON `mpsq`.`SubQuestionTypeID` = `sqt`.`SubQuestionTypeID`\n" +
                "WHERE `cqpsq`.`PaperID` = '" + PaperID + "'\n" +
                "AND `cqpsq`.`DetailID` = '" + DetailID + "'\n" +
                "AND `psqt`.`PassageSubQuestionTypeID` = '" + PassageSubQuestionTypeID + "'" +
                "AND `sqt`.`SubQuestionTypeID` = '" + subQueTypeID + "'";
        // Log.i("get_paper_master_sub_question_type_by_id", query);
        Utils.Log("get_paper_master_sub_question_type_by_id", query);
        return query;
    }

    public static String getExamTypePatternDetail(String PatternId) {
        Utils.Log("TAG MOdelAns getExamTypePatternDetail::-->", "select  * from  exam_type_pattern_detail where ExamTypePatternID = '" + PatternId + "' order by DisplayOrder");


        return "select  * from  exam_type_pattern_detail where ExamTypePatternID = '" + PatternId + "' order by DisplayOrder";
    }


    public static String getAllRowQuery(String tableName) {
        return " select  * from  " + tableName;
    }

    public static String getAllSujectviseHdrID(String subID) {
        return "select * from student_mcq_test_hdr where SubjectID = " + subID;
    }

    public static String getAllSujectviseHdrIDONE(String subID, String levewlID) {
        return "select * from student_mcq_test_hdr where SubjectID = " + subID + " AND LevelID = " + levewlID;
    }

    public static String getAllRowQueryStdMCQDetail(String tableName) {
        return " select  * from  " + tableName + " ORDER BY TempMCQTestDTLID ASC";
    }

    public static String getQuestions(String SubjectID, String QuesTypeIDs, String Chapters) {
        return "SELECT  distinct qt.isPassage, `q`.`MQuestionID`, `q`.`QuestionTypeID`, `q`.`Question`, `q`.`Answer`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `q` JOIN `question_types` `qt` ON `q`.`QuestionTypeID` = `qt`.`QuestionTypeID` WHERE q.QuestionTypeID IN (" + QuesTypeIDs + ") AND q.ChapterID IN (" + Chapters + ") ORDER BY `qt`.`RevisionNo`,`q`.`QuestionTypeID` ";

    }

    public static String getQuestions(String StudentQuestionPaperID, String ExamTypePatternDetailID) {
        String s = "SELECT `qm`.`Question`, `qm`.`Answer`,`qm`.`MQuestionID`, `qm`.`QuestionTypeID`,qpd.StudentQuestionPaperDetailID FROM `student_question_paper_detail` `qpd` JOIN `masterquestion` `qm` ON `qpd`.`MQuestionID` = `qm`.`MQuestionID` WHERE `qpd`.`StudentQuestionPaperID` = '" + StudentQuestionPaperID + "' AND `qpd`.`ExamTypePatternDetailID` = '" + ExamTypePatternDetailID + "'";

        Utils.Log("MODEL ANS QUery::->", s);

        return s;
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT 1";
    }

    public static String getQuestions(String StudentQuestionTypeID) {
        return "SELECT mq.Question, mq.Answer, sspd.MQuestionID, qt.QuestionType, sspq.QuestionTypeID, sspq.TotalAsk, sspq.ToAnswer, sspq.TotalMark " +
                "FROM student_set_paper_question_type sspq " +
                "LEFT JOIN question_types qt ON qt.QuestionTypeID = sspq.QuestionTypeID " +
                "LEFT JOIN student_set_paper_detail sspd ON sspd.StudentSetPaperQuestionTypeID = sspq.StudentSetPaperQuestionTypeID " +
                "LEFT JOIN masterquestion mq on mq.MQuestionID=sspd.MQuestionID " +
                "WHERE sspq.StudentSetPaperQuestionTypeID " +
                "IN (" + StudentQuestionTypeID + ")";
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT 1";
    }

    public static String getExamTypeTest(String examTypeID, String paperTypeId) {
        return " select  * from  examtypes where ExamTypeID IN (" +
                examTypeID + ") AND (PaperType LIKE '" + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + "' OR PaperType = '" + paperTypeId + "')";
    }

    public static String getRandomExamTypeId(String examTypeID, String paperTypeId) {
        return " select  ExamTypeID from  examtypes where ExamTypeID IN (" +
                examTypeID + ") AND (PaperType LIKE '" + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + ",%' OR PaperType LIKE '%," + paperTypeId + "' OR PaperType = '" + paperTypeId + "') ORDER BY Random() LIMIT 1";
    }

    public static String getlevel() {
        return " select * from student_mcq_test_hdr where LevelID = '1'";
    }

    public static String getAllQuery(String tableName, LinkedHashMap<String, String> map) {
        String sQuery = " select  * from  " + tableName + " ";


        List<String> values = new ArrayList<String>(map.values());
        List<String> keys = new ArrayList<String>(map.keySet());
        for (int j = values.size(); j > 0; j--) {
            int i = j - 1;
            Utils.SystemOutPrintln(keys.get(i), "====>" + values.get(i));
            if (keys.get(i).equals("ext")) {
                sQuery = sQuery + values.get(i) + " ";
            } else if (keys.get(i).equals(" IN ")) {
                sQuery = sQuery + keys.get(i) + values.get(i) + " ";
            } else {
                sQuery = sQuery + keys.get(i) + "=" + values.get(i) + " ";
            }


        }

        return sQuery;


    }


    public static String getTotalMCQCorrectAnswers(String headersID) {
        return "SELECT count(StudentMCQTestDTLID) as total_right FROM student_mcq_test_dtl dtl" +
                " LEFT JOIN mcqoption op ON dtl.AnswerID = op.MCQOPtionID WHERE   dtl.StudentMCQTestHDRID in("
                + headersID + ") AND dtl.IsAttempt = 1 AND op.isCorrect = 1";
    }

    public static String getTotalMCQAttempted(String headersID) {
        return "SELECT count(StudentMCQTestDTLID) as total_right FROM student_mcq_test_dtl dtl " +
                "WHERE   dtl.StudentMCQTestHDRID in("
                + headersID + ")";


    }

    public static String getAccuracy_cct(String subID) {
        return "select Acuuracy from class_evaluater_cct_count where SubjectID = " + subID;
    }

    public static String getTotalTest(String headersID) {
        return "SELECT count(StudentQuestionPaperID) as total FROM student_question_paper dtl " +
                "WHERE   dtl.SubjectID=subjects.SubjectID";
    }

    public static String getTotalAttemptedSubjectsWithName() {
        return "SELECT GROUP_CONCAT(student_mcq_test_hdr.StudentMCQTestHDRID) as StudentMCQTestHDRID, " +
                "student_mcq_test_hdr.SubjectID as SubjectID, subjects.SubjectName FROM student_mcq_test_hdr LEFT JOIN subjects on " +
                "student_mcq_test_hdr.SubjectID=subjects.SubjectID GROUP BY student_mcq_test_hdr.SubjectID";
    }

    public static String getTotalTestSubjectsWithName() {

        return "SELECT GROUP_CONCAT(student_question_paper.StudentQuestionPaperID) as StudentQuestionPaperID, " +
                "student_question_paper.SubjectID as SubjectID, subjects.SubjectName FROM student_question_paper LEFT JOIN subjects on " +
                "student_question_paper.SubjectID=subjects.SubjectID GROUP BY student_question_paper.SubjectID";


       /* return "SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn, s.SubjectName, et.ExamTypeName, et.Duration, et.TotalMarks,sqp.ExamTypePatternID,pt.PaperTypeName,sqp.PaperTypeID,sqp.TotalMarks AS SetPaperMarks,sqp.Duration AS SetPaperDuration" +
                " FROM student_question_paper sqp" +
                " LEFT JOIN exam_type_pattern etp ON sqp.ExamTypePatternID = etp.ExamTypePatternID" +
                " LEFT JOIN examtypes et ON etp.ExamTypeID = et.ExamTypeID" +
                " LEFT JOIN subjects s ON sqp.SubjectID = s.SubjectID" +
                " LEFT JOIN paper_type pt ON sqp.PaperTypeID=pt.PaperTypeID" +
                " ORDER BY sqp.StudentQuestionPaperID" ;*/


    }

    public static String getTotalAttemptedSubjectsCount() {
        return "SELECT COUNT(student_mcq_test_hdr.StudentMCQTestHDRID) as StudentMCQTestHDRID, " +
                "student_mcq_test_hdr.SubjectID, subjects.SubjectName FROM student_mcq_test_hdr LEFT JOIN subjects on " +
                "student_mcq_test_hdr.SubjectID=subjects.SubjectID GROUP BY student_mcq_test_hdr.SubjectID";
    }

    public static String getTotalGenratePaperSubjectsCount() {
        return "SELECT COUNT(student_question_paper.StudentQuestionPaperID) as StudentQuestionPaperID, " +
                "student_question_paper.SubjectID, subjects.SubjectName FROM student_question_paper LEFT JOIN subjects on " +
                "student_question_paper.SubjectID=subjects.SubjectID GROUP BY student_question_paper.SubjectID";
    }

    public static String getNoOfLastMCQTest(int no) {
        return "SELECT h.StudentMCQTestHDRID, h.CreatedOn, s.SubjectName FROM student_mcq_test_hdr" +
                " h LEFT JOIN subjects s on h.SubjectID=s.SubjectID ORDER BY StudentMCQTestHDRID DESC limit " + no;
    }

    public static String getNoOfLastMCQTestSubjectWise(String id) {
        return "SELECT h.StudentMCQTestHDRID, h.CreatedOn, s.SubjectName FROM student_mcq_test_hdr" +
                " h LEFT JOIN subjects s on h.SubjectID=s.SubjectID where s.SubjectID=" + id + "  ORDER BY StudentMCQTestHDRID DESC ";
    }

    public static String getNoOfLastMCQTestSubjectWise(String id, int Leval) {
        return "SELECT h.StudentMCQTestHDRID, h.CreatedOn, s.SubjectName FROM student_mcq_test_hdr" +
                " h LEFT JOIN subjects s on h.SubjectID=s.SubjectID where s.SubjectID=" + id + " AND h.LevelID=" + Leval + " ORDER BY StudentMCQTestHDRID DESC ";
    }

    public static String getGenratPaperTestSubjectWise(String subjectId, String paperTypeID) {


     /*   return "SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn, s.SubjectName, et.ExamTypeName, etp.Duration, et.TotalMarks,sqp.ExamTypePatternID FROM student_question_paper sqp" +
                " JOIN exam_type_pattern etp ON sqp.ExamTypePatternID = etp.ExamTypePatternID" +
                " JOIN examtypes et ON etp.ExamTypeID = et.ExamTypeID" +
                " JOIN subjects s ON etp.SubjectID = s.SubjectID " +
                " WHERE sqp.StudentID =" + app.mainUser.getStudentId() + "" +
                " AND etp.SubjectID =" + subjectId + " AND (et.PaperType LIKE '" + paperTypeID + ",%' OR " +
                "et.PaperType LIKE '%," + paperTypeID + ",%' OR et.PaperType LIKE '%," +
                paperTypeID + "' OR et.PaperType ='" + paperTypeID + "') ORDER BY sqp.StudentQuestionPaperID DESC";*/
        return "SELECT sqp.StudentQuestionPaperID, strftime('%d-%m-%Y',sqp.CreatedOn) as CreatedOn, s.SubjectName,et.Duration" +
                ", et.ExamTypeName, et.TotalMarks, sqp.PaperTypeID, pt.PaperTypeName, pt.PaperTypeID,sqp.TotalMarks AS SetPaperMarks,sqp.Duration AS SetPaperDuration,sqp.ExamTypePatternID " +
                "FROM `student_question_paper` `sqp` " +
                "LEFT JOIN `exam_type_pattern` `etp` ON `sqp`.`ExamTypePatternID` = `etp`.`ExamTypePatternID` " +
                "LEFT JOIN `examtypes` `et` ON `etp`.`ExamTypeID` = `et`.`ExamTypeID` " +
                "LEFT JOIN `paper_type` `pt` ON `sqp`.`PaperTypeID` = `pt`.`PaperTypeID` " +
                "LEFT JOIN `subjects` `s` ON `sqp`.`SubjectID` = `s`.`SubjectID` " +
                "WHERE `sqp`.`StudentID` = '" + MainUser.getStudentId() + "' " +
                "AND `sqp`.`SubjectID` = '" + subjectId + "' " +
                "AND `sqp`.`PaperTypeID` = '" + paperTypeID + "' ORDER BY sqp.StudentQuestionPaperID DESC ";
    }

    public static String getGenratPaperTestSubjectWise(int no) {

/*        return "SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn, s.SubjectName, et.ExamTypeName, etp.Duration, et.TotalMarks,sqp.ExamTypePatternID FROM student_question_paper sqp" +
                " JOIN exam_type_pattern etp ON sqp.ExamTypePatternID = etp.ExamTypePatternID" +
                " JOIN examtypes et ON etp.ExamTypeID = et.ExamTypeID" +
                " JOIN subjects s ON etp.SubjectID = s.SubjectID " +
                " ORDER BY sqp.StudentQuestionPaperID DESC limit " + no;*/

        return "SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn, s.SubjectName, et.ExamTypeName, et.Duration, et.TotalMarks,sqp.ExamTypePatternID,pt.PaperTypeName,sqp.PaperTypeID,sqp.TotalMarks AS SetPaperMarks,sqp.Duration AS SetPaperDuration" +
                " FROM student_question_paper sqp" +
                " LEFT JOIN exam_type_pattern etp ON sqp.ExamTypePatternID = etp.ExamTypePatternID" +
                " LEFT JOIN examtypes et ON etp.ExamTypeID = et.ExamTypeID" +
                " LEFT JOIN subjects s ON sqp.SubjectID = s.SubjectID" +
                " LEFT JOIN paper_type pt ON sqp.PaperTypeID=pt.PaperTypeID" +
                " ORDER BY sqp.StudentQuestionPaperID DESC limit " + no;
    }

    public static String getQuestionTypes(String PaperId) {
        return "select * from student_set_paper_question_type where StudentQuestionPaperID='" + PaperId + "'";
        //   return "select ExamTypePatternID from exam_type_pattern where ExamTypeID ="+examType+" AND SubjectID ="+subjectId+" ORDER BY Random() LIMIT 1";
    }

    public static String getInstruction(String examTypeID, String SunjectId) {
        return "SELECT `Instruction` FROM `exam_type_pattern` WHERE `SubjectID` = '" + SunjectId + "' AND `ExamTypeID` = '" + examTypeID + "' LIMIT 1";
    }

    public static String getBoardPapersRecords(String boardId, String mediumId, String standardID) {
        return "Select * from board_paper_files where BoardID='" + boardId + "' " +
                "AND MediumID='" + mediumId + "' AND StandardID='" + standardID + "'";
    }

    public static String getAttemptedEvaluterMCQ() {
        return "select * from class_evaluater_mcq_test_temp where IsAttempt=1";
    }

    public static String getAllQuery(String tableName, ArrayList<QueryParam> map) {
        String sQuery = " select  * from  " + tableName + " ";


        for (QueryParam q : map) {
            if (q.key.contains(" IN ")) {
                sQuery = sQuery + q.key + q.value + " ";
            } else if (q.key.equals("ext")) {
                sQuery = sQuery + " " + q.value + " ";
            } else if (q.key.equals("LIMIT")) {
                sQuery = sQuery + " LIMIT " + q.value + " ";
            } else {
                sQuery = sQuery + q.key + "=" + q.value + " ";
            }
        }

//        Utils.Log("Query :", sQuery);
        return sQuery;


    }

    public static String getCustomFields(String filedName, String tableName, ArrayList<QueryParam> map) {
        String sQuery = " select  " + filedName + " from  " + tableName + " ";


        for (QueryParam q : map) {
            if (q.key.contains(" IN ")) {
                sQuery = sQuery + q.key + q.value + " ";
            } else if (q.key.equals("ext")) {
                sQuery = sQuery + " " + q.value + " ";
            } else if (q.key.equals("LIMIT")) {
                sQuery = sQuery + " LIMIT " + q.value + " ";
            } else {
                sQuery = sQuery + q.key + "=" + q.value + " ";
            }
        }

//        Utils.Log("Query :", sQuery);
        return sQuery;


    }

    public static String getBoardQuestions(String QuesTypeIDs, String Chapters) {
        return "SELECT  distinct `q`.`MQuestionID`, `q`.`QuestionTypeID`, `q`.`Question`, `q`.`Answer`, `qt`.`QuestionType`, `qt`.`Marks` FROM `masterquestion` `q` JOIN `question_types` `qt` ON `q`.`QuestionTypeID` = `qt`.`QuestionTypeID` WHERE q.QuestionTypeID IN (" + QuesTypeIDs + ") AND q.ChapterID IN (" + Chapters + ") AND q.isBoard=1 ORDER BY `qt`.`RevisionNo`,`q`.`QuestionTypeID` ";
    }
}
