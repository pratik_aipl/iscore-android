package com.parshvaa.isccore.activity.changestandard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.StandardModel;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 3/21/2018.
 */

public class StandardActivity extends BaseActivity implements AsynchTaskListner {
    public StandardActivity instance;
    public RecyclerView recycler_standard;
    public String[] list_name;
    public TextView lbl_title;
    public FloatingActionButton btn_next, btn_prev;
    public ArrayList<StandardModel> stdList = new ArrayList<>();
    public String mediumId;
    public StandardModel standardModel;
    public ArrayList<StandardModel> StandeardArray = new ArrayList<>();
    public ArrayList<String> strStandeardArray = new ArrayList<>();
    public String email;
    public PaymentStandardAdapter standardAdapter;
    public App app;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_standard);
        Utils.logUser();
        jParser = new JsonParserUniversal();
        instance = this;
        app = App.getInstance();
        recycler_standard = findViewById(R.id.recycler_standard);
        lbl_title = findViewById(R.id.lbl_title);
        btn_next = findViewById(R.id.btn_next);
        btn_prev = findViewById(R.id.btn_prev);
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);
        tv_empty.setText("No Standard Available");

        new CallRequest(StandardActivity.this).getStandeard(MainUser.getMediumID());

        btn_next.setOnClickListener(v -> {
            if (Utils.isNetworkAvailable(instance)) {
                if (TextUtils.isEmpty(standardAdapter.getStandardId())) {
                    Utils.showToast("Please Select Standard ", StandardActivity.this);
                } else {
                    Bundle bundle = new Bundle();
                    mFirebaseAnalytics.logEvent("ADDPAYMENTINFO", bundle);
                    MainUser.setStandardID(standardAdapter.getStandardId());
                    MainUser.setStanderdName(standardAdapter.getStandardName());
                    startActivity(new Intent(instance, PaymentConfirmActivity.class)
                    .putExtra("amount",standardAdapter.getStandardAmount()));
                }
            } else {
                Utils.showToast("No Internet, Please try again later", instance);

            }
        });
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setupRecyclerView(ArrayList<StandardModel> standeardArray) {

        final Context context = recycler_standard.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);

        recycler_standard.setLayoutAnimation(controller);
        recycler_standard.scheduleLayoutAnimation();
        recycler_standard.setLayoutManager(new LinearLayoutManager(context));
        standardAdapter = new PaymentStandardAdapter(standeardArray, context);
        recycler_standard.setAdapter(standardAdapter);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case getStandeard:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            emptyView.setVisibility(View.GONE);
                            JSONArray jBoardArray = jObj.getJSONArray("data");
                            if (jBoardArray != null && jBoardArray.length() > 0) {
                                for (int i = 0; i < jBoardArray.length(); i++) {
                                    standardModel = (StandardModel) jParser.parseJson(jBoardArray.getJSONObject(i), new StandardModel());
                                    StandeardArray.add(standardModel);
                                }
                                setupRecyclerView(StandeardArray);
                            } else {
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        }else {
                            btn_next.setAlpha(0.5f);
                            btn_next.setClickable(false);
                            recycler_standard.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public class PaymentStandardAdapter extends RecyclerView.Adapter<PaymentStandardAdapter.ViewHolder> {
        private ArrayList<StandardModel> offersList;
        public Context context;
        public int row_index = 0;
        private int lastSelectedPosition = -1;
        public String standardName, standardId,amount;

        public PaymentStandardAdapter(ArrayList<StandardModel> offersListIn, Context ctx) {
            offersList = offersListIn;
            context = ctx;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_stand_raw, parent, false);

            ViewHolder viewHolder =
                    new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder,
                                     final int position) {
            StandardModel StandardObj = offersList.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    row_index = position;
                    notifyDataSetChanged();
                }
            });
            holder.tv_stdName.setText(StandardObj.getStandardName());
            holder.tv_amount.setText(StandardObj.getPrice());
            if (row_index == position) {
                holder.rel_std.setBackgroundResource(R.drawable.squar_border_red);
                standardName = StandardObj.getStandardName();
                standardId = StandardObj.getStandardID();
                amount = StandardObj.getPrice();
            } else {
                holder.rel_std.setBackgroundColor(Color.parseColor("#ffffff"));
            }


            //since only one radio button is allowed to be selected,
            // this condition un-checks previous selections
        }

        @Override
        public int getItemCount() {
            return offersList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {


            public TextView tv_stdName, tv_amount;
            public RelativeLayout rel_std;

            public ViewHolder(View view) {
                super(view);
                tv_stdName = view.findViewById(R.id.tv_stdName);
                tv_amount = view.findViewById(R.id.tv_amount);
                rel_std = view.findViewById(R.id.rel_std);

            }
        }

        public String getStandardName() {
            return standardName;
        }
        public String getStandardAmount() {
            return amount;
        }

        public String getStandardId() {
            return standardId;
        }

    }
}