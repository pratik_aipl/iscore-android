package com.parshvaa.isccore.activity.event;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.model.ArtiseModel;
import com.parshvaa.isccore.model.CourseModel;
import com.parshvaa.isccore.model.EventModel;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.RobotoBoldButton;
import com.parshvaa.isccore.utils.RobotoButton;
import com.parshvaa.isccore.utils.RobotoEditTextView;
import com.parshvaa.isccore.utils.RobotoTextView;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventDetailsActivity extends BaseActivity implements AsynchTaskListner {

    private static final String TAG = "EventDetailsActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.btn_subscribe)
    RobotoButton btnSubscribe;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.img_event)
    ImageView imgEvent;
    @BindView(R.id.tv_event_title)
    TextView tvEventTitle;
    @BindView(R.id.tv_event_date)
    TextView tvEventDate;
    @BindView(R.id.tv_event_watch)
    TextView tvEventWatch;
    @BindView(R.id.tv_event_duration)
    TextView tvEventDuration;
    @BindView(R.id.tv_event_lang)
    TextView tvEventLang;
    @BindView(R.id.tv_event_age)
    TextView tvEventAge;
    @BindView(R.id.recy_artise)
    RecyclerView recyArtise;
    @BindView(R.id.tv_event_note)
    TextView tvEventNote;
    @BindView(R.id.tv_event_about)
    TextView tvEventAbout;
    @BindView(R.id.recy_course)
    RecyclerView recyCourse;
    @BindView(R.id.tv_event_tc)
    TextView tvEventTc;
    @BindView(R.id.tv_event_Instruction)
    TextView tvEventInstruction;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_actul_price)
    TextView tvActulPrice;
    @BindView(R.id.btn_submit)
    RobotoButton btnSubmit;
    @BindView(R.id.lin_bottom)
    LinearLayout linBottom;
    @BindView(R.id.lin_course_detl)
    LinearLayout lin_course_detl;


    EventModel eventModel;
    public ArrayList<EventModel> eventList = new ArrayList<>();

    ArtiseListAdpater eventListAdpater;
    CourseListAdpater courseListAdpater;
    @BindView(R.id.et_mobile)
    RobotoEditTextView etMobile;
    @BindView(R.id.lin_editbox)
    LinearLayout linEditbox;
    @BindView(R.id.et_email)
    RobotoEditTextView etEmail;
    @BindView(R.id.lin_editbox_email)
    LinearLayout linEditboxEmail;
    @BindView(R.id.btnCancel)
    RobotoBoldButton btnCancel;
    @BindView(R.id.btnWithdraw)
    RobotoBoldButton btnWithdraw;
    @BindView(R.id.mWithdraw)
    RelativeLayout mWithdraw;
    @BindView(R.id.img_about)
    ImageView img_about;
  @BindView(R.id.img_tc)
    ImageView img_tc;
  @BindView(R.id.img_course_dtl)
    ImageView img_course_dtl;
  @BindView(R.id.img_Instruction)
    ImageView imgInstruction;

  String E_ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        ButterKnife.bind(this);

       if(App.isType.equalsIgnoreCase("event_register")){
           btnSubmit.setText("REGISTER");
           E_ID = getIntent().getExtras().getString("Event_ID");
           Log.d(TAG, "Event_ID>>>: "+E_ID);
           showNumberDialog();

       }else{
           eventModel = (EventModel) getIntent().getSerializableExtra("EventData");
           eventList.add(eventModel);
           Log.d(TAG, "eventList: " + eventList.size());
           tvEventDuration.setText(eventModel.getDuration());
           tvEventLang.setText(eventModel.getLanguage());
           tvEventAge.setText(eventModel.getAge());
           tvEventNote.setText(Html.fromHtml(eventModel.getText()));

           tvEventAbout.setText(Html.fromHtml(eventModel.getAbout()));
           tvEventTc.setText(Html.fromHtml(eventModel.getTerms()));
           tvEventInstruction.setText(Html.fromHtml(eventModel.getInstruction()));

           if (eventModel.getArtists().size() != 0)
               setupArtiseRecyclerView(eventModel.getArtists());

           if (eventModel.getIsEventDemo().equalsIgnoreCase("0")) {

               tvEventTitle.setText(Html.fromHtml(eventModel.getTitle()));
               tvEventDate.setText(eventModel.getStartDate() + " - " + eventModel.getEndDate());
               tvEventWatch.setText("Watch On " + eventModel.getWatchOn());

               if (!TextUtils.isEmpty(eventModel.getBannerImage()))
                   Glide.with(this)
                           .load(eventModel.getBannerImage())
                           .placeholder(R.drawable.iscore_final_logo)
                           .into(imgEvent);

               btnSubmit.setText("BUY NOW");
               tvPrice.setText("\u20B9 " + eventModel.getPrice()+" /-");
               tvPrice.setPaintFlags(tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
               tvActulPrice.setText("\u20B9 " + eventModel.getActualPrice()+" /-");

               lin_course_detl.setVisibility(View.VISIBLE);
               if (eventModel.getCourseDetails().size() != 0)
                   setupCourseRecyclerView(eventModel.getCourseDetails());

               if (eventModel.getIsSubscribe().equalsIgnoreCase("1")) {
                   linBottom.setVisibility(View.GONE);
                   // imgPlay.setVisibility(View.VISIBLE);
               } else {
                   linBottom.setVisibility(View.VISIBLE);
                   // imgPlay.setVisibility(View.GONE);
               }

           } else {
               lin_course_detl.setVisibility(View.GONE);
               tvEventTitle.setText(Html.fromHtml(eventModel.getDemoTitle()));
               tvEventDate.setText(eventModel.getDemoStartDateTime());
               tvEventWatch.setText("Watch On " + eventModel.getWatchOn());
               btnSubmit.setText("REGISTER");

               tvActulPrice.setText("DEMO");

               if (!TextUtils.isEmpty(eventModel.getDemoImage()))
                   Glide.with(this)
                           .load(eventModel.getDemoImage())
                           .centerCrop()
                           .placeholder(R.drawable.iscore_final_logo)
                           .into(imgEvent);
               if (eventModel.getIsRegister().equalsIgnoreCase("0")) {
                   linBottom.setVisibility(View.VISIBLE);
               } else {
                   linBottom.setVisibility(View.GONE);
               }
           }

       }

        imgBack.setOnClickListener(view -> onBackPressed());

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(eventModel.getIsEventDemo().equalsIgnoreCase("0")){
                    if (Utils.isNetworkAvailable(EventDetailsActivity.this)) {
                        Bundle bundle = new Bundle();
                        mFirebaseAnalytics.logEvent("ADDPAYMENTINFO", bundle);
                        startActivity(new Intent(EventDetailsActivity.this, BuyNowEventPaymentConfirmActivity.class)
                                .putExtra("EventName",eventModel.getTitle())
                                .putExtra("EventID",eventModel.getEventID())
                                .putExtra("StartDate",eventModel.getStartDate())
                                .putExtra("EndDate",eventModel.getEndDate())
                                .putExtra("Artist",eventModel.getArtistName())
                                .putExtra("amount",eventModel.getActualPrice()));
                    } else {
                        Utils.showToast("No Internet, Please try again later", EventDetailsActivity.this);
                    }

                }else{
                    showNumberDialog();
                }
            }
        });


    }
    public void abouthide(View view) {
        if (tvEventAbout.getVisibility() == View.VISIBLE){
            tvEventAbout.setVisibility(View.GONE);
            img_about.setImageResource(R.drawable.down_arrow);
        }else{
            tvEventAbout.setVisibility(View.VISIBLE);
            img_about.setImageResource(R.drawable.up_arrow);
        }
    }
    public void tchide(View view) {
        if (tvEventTc.getVisibility() == View.VISIBLE){
            tvEventTc.setVisibility(View.GONE);
            img_tc.setImageResource(R.drawable.down_arrow);
        }else{
            tvEventTc.setVisibility(View.VISIBLE);
            img_tc.setImageResource(R.drawable.up_arrow);
        }
    }
    public void coursehide(View view) {
        if (recyCourse.getVisibility() == View.VISIBLE){
            recyCourse.setVisibility(View.GONE);
            img_course_dtl.setImageResource(R.drawable.down_arrow);
        }else{
            recyCourse.setVisibility(View.VISIBLE);
            img_course_dtl.setImageResource(R.drawable.up_arrow);
        }
    }
    public void Instructionhide(View view) {
        if (tvEventInstruction.getVisibility() == View.VISIBLE){
            tvEventInstruction.setVisibility(View.GONE);
            imgInstruction.setImageResource(R.drawable.down_arrow);
        }else{
            tvEventInstruction.setVisibility(View.VISIBLE);
            imgInstruction.setImageResource(R.drawable.up_arrow);
        }
    }
    private void setupArtiseRecyclerView(List<ArtiseModel> artiseModels) {
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_from_right);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyArtise.setLayoutManager(mLayoutManager);
        recyArtise.setLayoutAnimation(controller);
        recyArtise.scheduleLayoutAnimation();
        eventListAdpater = new ArtiseListAdpater(EventDetailsActivity.this, artiseModels);
        recyArtise.setAdapter(eventListAdpater);
    }

    private void setupCourseRecyclerView(List<CourseModel> courseModels) {
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_from_right);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyCourse.setLayoutManager(mLayoutManager);
        recyCourse.setLayoutAnimation(controller);
        recyCourse.scheduleLayoutAnimation();
        courseListAdpater = new CourseListAdpater(EventDetailsActivity.this, courseModels);
        recyCourse.setAdapter(courseListAdpater);
    }


    private void showNumberDialog() {
        //btnWithdraw.setVisibility(View.GONE);
        mWithdraw.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(MainUser.getRegMobNo())) {
            etMobile.setText(MainUser.getRegMobNo());
        }
        if (!TextUtils.isEmpty(MainUser.getEmailID())) {
            etEmail.setText(MainUser.getEmailID());
        }
        btnCancel.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(this);
            mWithdraw.setVisibility(View.GONE);
           // btnWithdraw.setVisibility(View.VISIBLE);
            etMobile.setText("");
            etEmail.setText("");
            if(App.isType.equalsIgnoreCase("event_register")){
                startActivity(new Intent(this,EventListActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        btnWithdraw.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(this);
            String mNumber = etMobile.getText().toString();
            String email = etEmail.getText().toString();
            if (TextUtils.isEmpty(mNumber)) {
                etMobile.setError("Please Enter Mobile No.");
            } else if (!mNumber.matches("^[0-9]{10}$")) {
                etMobile.setError("Enter appropriate mobile number.");
            } else if (TextUtils.isEmpty(email)) {
                etEmail.setError("Please Enter Email Id");
            } else if (!email.matches("^(.+)@(.+)$")) {
                etEmail.setError("Enter appropriate Email ID");
            } else {
                mWithdraw.setVisibility(View.GONE);
               // btnWithdraw.setVisibility(View.VISIBLE);
                showProgress(true, false);
                new CallRequest(this).EventRegister(mNumber, email,App.isType.equalsIgnoreCase("event_register")?E_ID:eventModel.getEventID());
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case EventRegister:
                    showProgress(false, true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if(App.isType.equalsIgnoreCase("event_register")){
                                startActivity(new Intent(this,EventListActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            }else{
                                onBackPressed();
                            }
                            Utils.showToast(jObj.getString("message"), this);
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
