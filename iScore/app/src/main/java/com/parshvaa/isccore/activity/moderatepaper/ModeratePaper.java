package com.parshvaa.isccore.activity.moderatepaper;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 2/16/2018.
 */

public class ModeratePaper implements Serializable {
    public String QFile = "";
    public String QAFile = "";
    public String ModeratePaperName = "";
    public String PaperName = "";

    public String getModeratePaperID() {
        return ModeratePaperID;
    }

    public void setModeratePaperID(String moderatePaperID) {
        ModeratePaperID = moderatePaperID;
    }

    public String ModeratePaperID= "";

    public String getQFile() {
        return QFile;
    }

    public void setQFile(String QFile) {
        this.QFile = QFile;
    }

    public String getQAFile() {
        return QAFile;
    }

    public void setQAFile(String QAFile) {
        this.QAFile = QAFile;
    }

    public String getModeratePaperName() {
        return ModeratePaperName;
    }

    public void setModeratePaperName(String moderatePaperName) {
        ModeratePaperName = moderatePaperName;
    }

    public String getPaperName() {
        return PaperName;
    }

    public void setPaperName(String paperName) {
        PaperName = paperName;
    }

    public String getAnswerPaperName() {
        return AnswerPaperName;
    }

    public void setAnswerPaperName(String answerPaperName) {
        AnswerPaperName = answerPaperName;
    }

    public String AnswerPaperName = "";
}
