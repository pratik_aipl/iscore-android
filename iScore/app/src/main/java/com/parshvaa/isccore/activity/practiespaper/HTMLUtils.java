package com.parshvaa.isccore.activity.practiespaper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import androidx.fragment.app.FragmentActivity;
import android.text.Html;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.db.DBConstant;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.Utils;

import java.io.ByteArrayOutputStream;

/**
 * Created by Dipesh on 3/15/2017.
 */
public class HTMLUtils {


    public Context context;
    public static String className;
    public  static App app;

    public HTMLUtils(FragmentActivity activity) {
        context = activity;
        app = App.getInstance();
    }

    public static PdfPCell getImagesWithHTML(PdfPCell cell, String data, Font font) {
        Paragraph para;
        if (!data.contains("<img src=")) {
            para = new Paragraph(data, font);
            para.setFont(font);
            cell.addElement(para);
        } else {

            String[] items = data.split("<img");
            for (String item : items) {
                if (item.contains("src")) {
                    int totalLengh = items.length;
                    String coreItem = item;
                    String tempImge = item.substring(item.indexOf("src=") + 5, item.indexOf("' />"));
                    int tempLengh = tempImge.length();

                    cell.addElement(getImageFromLocal(tempImge));
                    cell.addElement(new PdfPCell(getImageFromLocal(tempImge), true));
                    if (totalLengh > (tempLengh + 12)) {
                        para = new Paragraph(String.valueOf(Html.fromHtml(coreItem.substring(coreItem.indexOf("' />") + 4))), font);
                        para.setFont(font);
                    }

                } else {
                    para = new Paragraph(String.valueOf(Html.fromHtml(item)), font);
                    para.setFont(font);
                    cell.addElement(para);
                }
            }

        }


        return cell;

    }

    public static String getHTML(String data) {
        String prefixHTML = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title>MathJax MathML Test Page</title>\n" +
                "<!-- Copyright (c) 2010-2016 The MathJax Consortium -->\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/math_jx/MathJax.js?config=MML_HTMLorMML-full\"></script>\n" +
                "\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<p>";

        String postFixHtml = "</p>\n" +
                "\n" +
                "</body>\n" +
                "</html>";


        return prefixHTML + data + postFixHtml;


    }

    public static Image getImageFromLocal(String path) {
        try {
            String SDCardPath = Environment.getExternalStorageDirectory()
                    + "/" + DBConstant.IMAGE_FOLDER;

            Utils.Log("IMAGE PATH :::", SDCardPath + path);

            Drawable d = Drawable.createFromPath(SDCardPath + path);

            BitmapDrawable bitDw = ((BitmapDrawable) d);

            Bitmap bmp = bitDw.getBitmap();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

            Image image = Image.getInstance(stream.toByteArray());
            image.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
            return image;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String getPrelimTestHeader(MySharedPref mySharedPref, String subjectName, String exam, String marks, String date, String ChapterId, String duration) {

        if(mySharedPref.getClassName().isEmpty()){
             className= "iScore";

        }else{
            className= mySharedPref.getClassName();

        }
        Utils.Log("TAG","CHAPTERID HTML::--->"+ChapterId);
        if (!ChapterId.isEmpty()) {
            if (getCount(ChapterId) > 18) {
                ChapterId = ChapterId.substring(0, ChapterId.indexOf(",", 18));
                ChapterId = ChapterId + "...";
            }
        }
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <head>\n" +
                "        <title>Question List</title>\n" +
                "        <style type=\"text/css\">\n" +

                "            table {\n" +
                "                width: 100%;\n" +
                "            }\n" +
                "            body * {\n" +
                "                font: 100%;\n" +
                "            }\n" +


                "            .table tr td {\n" +
                "                vertical-align: top;\n" +
                "            }\n" +
                "            .question p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +
                "            .answer p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +
                "            body {\n" +
                "                width: 100%;\n" +
                "                height: auto;\n" +
                "                margin: 0;\n" +
                "                padding: 0;\n" +
                "                font: 12pt \"Tahoma\";\n" +
                "            }\n" +
                "            * {\n" +
                "                box-sizing: border-box;\n" +
                "                -moz-box-sizing: border-box;\n" +
                "            }\n" +
                "            .page {\n" +
                "                width: 210mm;\n" +
                "                min-height: 297mm;\n" +
                "                padding: 0mm;\n" +
                "                margin: 10mm auto;\n" +
                "                /*border: 1px #D3D3D3 solid;*/\n" +
                "                /*border-radius: 5px;*/\n" +
                "                background: white;\n" +
                "                /*box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);*/\n" +
                "            }\n" +
                "            .subpage {\n" +
                "                padding: 1cm;\n" +
                "                /*border: 5px red solid;*/\n" +
                "                height: 257mm;\n" +
                "                /*outline: 2cm #FFEAEA solid;*/\n" +
                "            }\n" +
                "\n" +
                " .sub p {\n" +
                "\tdisplay:inline;\n" +
                " }" +
                "            @page {\n" +
                "                size: A4;\n" +
                "                margin: 0;\n" +
                "            }\n" +
                "            @media print {\n" +
                "                html, body {\n" +
                "                    width: 210mm;\n" +
                "                    height: 297mm;        \n" +
                "                }\n" +
                "                .page {\n" +
                "                    margin: 0;\n" +
                "                    border: initial;\n" +
                "                    border-radius: initial;\n" +
                "                    width: initial;\n" +
                "                    min-height: initial;\n" +
                "                    /*box-shadow: initial;*/\n" +
                "                    background: initial;\n" +
                "                    /*page-break-after: always;*/\n" +
                "                }\n" +
                "            }\n " +

                "      .tablenew tr td {\n" +

                "                 vertical-align: top; \n" +
                "              } " +
                "      .table tr.mtc td p{\n" +
                "                    margin: 0;\n" +
                "                 display: inline-block; \n" +
                "              } " +

                "        </style>\n" +
                "        <script type=\"text/javascript\">\n" +
                "//            window.print();\n" +
                "        </script>\n" +
 "\n" +
                "<link rel=\"stylesheet\" href=\"file:///android_asset/mathscribe/jqmath-0.4.3.css\">" +
                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jquery-1.4.3.min.js\"></script>\n" +
                "<script type=\"text/javascript\">   var mtcarray = [];</script>"+
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jqmath-etc-0.4.6.min.js\"></script>\n" +
                "    </head>\n" +
                "    <body style=\"font-size:12px !important; font-family:'arial' !important;\">\n" +
                "        <div class=\"book\">\n" +
                "            <div class=\"page\">\n" +
                "                <div class=\"subpage\">\n" +
                "                    <table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" class=\"table\" style=\"border: 1px solid black;\">\n" +
                "                        <tr>\n" +
                "                            <td colspan=\"3\" align=\"center\"><h3 style='line-height:20px; margin:5px auto;'>" +className+" </h3></td>\n" +
                "                        </tr>\n" +
                "                        <tr>\n" +
                "                            <td width=\"100\"><b>Exam: " + exam + "</b></td>\n" +
                "                            <td align=\"center\"><b>" + subjectName + "</b></td>\n" +
                "                            <td align=\"right\" width=\"100\"><b>Marks: " + marks + "</b></td>\n" +
                "                        </tr>\n" +
                "                        <tr>\n" +
                "                            <td width=\"170\"><b>Date : " + date + " </b></td>\n" +
                "                            <td align=\"center\"><b>Chapter: " + ChapterId + "</b></td>\n" +
                "                            <td align=\"right\" width=\"150\"><b>Time: " + duration + "</b></td>\n" +
                "                        </tr>\n" +
                "                    </table>\n" +
                "                    <br/>" +
                "                       <table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" class=\"table tablenew\" >";
    }

    public static int getCount(String str) {
        try {
            int lastIndex = 0;
            int count = 0;
            while (lastIndex != -1) {

                lastIndex = str.indexOf(",", lastIndex);

                if (lastIndex != -1) {
                    count++;
                    lastIndex += ",".length();
                }
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }



    public static String getShuffleScript(){
        return " <script type=\"text/javascript\">\n" +
                "            function shuffleArray(array) {\n" +
                "                var counter = array.length, temp, index;\n" +
                "                // While there are elements in the array\n" +
                "                while (counter > 0) {\n" +
                "                    // Pick a random index\n" +
                "                    index = Math.floor(Math.random() * counter);\n" +
                "\n" +
                "                    // Decrease counter by 1\n" +
                "                    counter--;\n" +
                "\n" +
                "                    // And swap the last element with it\n" +
                "                    temp = array[ counter ];\n" +
                "                    array[ counter ] = array[ index ];\n" +
                "                    array[ index ] = temp;\n" +
                "                }\n" +
                "                return array;\n" +
                "            }\n";
    }
}
