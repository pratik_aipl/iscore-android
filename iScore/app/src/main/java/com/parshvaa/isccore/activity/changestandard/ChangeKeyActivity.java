package com.parshvaa.isccore.activity.changestandard;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.activity.DashBoardActivity;
import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.model.KeyData;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.MySharedPref;
import com.parshvaa.isccore.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Karan - Empiere on 3/21/2018.
 */

public class ChangeKeyActivity extends BaseActivity implements AsynchTaskListner {

    public ChangeKeyActivity instance;
    public Button btn_signup, btn_login;
    public EditText etRegKey;
    public TextView montserratTextView3;
    public String data;
    public MyDBManager mDb;
    public App app;
    public JsonParserUniversal jParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_regkey_login);
        Utils.logUser();
        instance = this;

        app = App.getInstance();
        jParser = new JsonParserUniversal();
        mySharedPref = new MySharedPref(instance);
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);

        etRegKey = findViewById(R.id.et_password);
        btn_login = findViewById(R.id.btn_login);
        btn_signup = findViewById(R.id.btn_signup);
        montserratTextView3 = findViewById(R.id.montserratTextView3);
        btn_signup.setVisibility(View.GONE);
        montserratTextView3.setVisibility(View.GONE);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etRegKey.getText().toString().equals("")) {
                    etRegKey.setError("Please enter the valid key.");
                } else {
                    if (Utils.isNetworkAvailable(instance)) {
                        showProgress(true, false);
                        new CallRequest(instance).change_class_key(etRegKey.getText().toString());
                    } else {
                        Utils.showToast("Please connect to Internet", instance);
                    }
                }


            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case change_class_key:
                        showProgress(false, false);
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            //   app.bubblesManager.recycle();
                            Utils.deleteAllTableRecord(mDb);
                            App.deleteCache(instance);
                            mySharedPref.clearApp();

                            System.out.println("Sagar Clearing -------------------");

                            //mySharedPref.printAll();
                            //System.out.println("Sagar started Filling -------------------");

                            App.mainUser = new MainUser();


                            mySharedPref.setIsLoggedIn(true);
                            mySharedPref.setDownloadStatus("n");
                            mySharedPref.setIsProperLogin("true");

                            JSONObject jData = jObj.getJSONObject("data");
                            MainUser.setStudentId(jData.getString("StudentID"));
                            MainUser.setFirstName(jData.getString("FirstName"));
                            MainUser.setLastName(jData.getString("LastName"));
                            MainUser.setGuid(jData.getString("IMEINo"));
                            MainUser.setEmailID(jData.getString("EmailID"));
                            MainUser.setRegMobNo(jData.getString("RegMobNo"));
                            MainUser.setSchoolName(jData.getString("SchoolName"));
                            MainUser.setProfile_image(jData.getString("ProfileImage"));
                            MainUser.setStudentCode(jData.getString("StudentCode"));

                            mySharedPref.setStudent_id(MainUser.getStudentId());
                            mySharedPref.setFirst_name(MainUser.getFirstName());
                            mySharedPref.setLast_name(MainUser.getLastName());
                            mySharedPref.setGuuid(MainUser.getGuid());
                            mySharedPref.setEmailID(MainUser.getEmailID());
                            mySharedPref.setRegMobNo(MainUser.getRegMobNo());
                            mySharedPref.setStudentCode(MainUser.getStudentCode());
                            mySharedPref.setProfileImage(MainUser.getProfile_image());
                            int verCode = Utils.currentAppVersionCode(instance);
                            mySharedPref.setVersioCode(String.valueOf(verCode));


                            KeyData keydata;
                            MainUser.keyDataArray.clear();
                            JSONArray jsonArray = jData.getJSONArray("KeyData");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jKey = jsonArray.getJSONObject(i);
                                keydata = (KeyData) jParser.parseJson(jKey, new KeyData());
                                MainUser.keyDataArray.add(keydata);

                                MainUser.setRegKey(MainUser.keyDataArray.get(i).getStudentKey());
                                MainUser.setPreviousYearScore(MainUser.keyDataArray.get(i).getPreviousYearScore());

                                mySharedPref.setStudent_id(MainUser.keyDataArray.get(i).getStudentID());
                                mySharedPref.setBoardID(MainUser.keyDataArray.get(i).getBoardID());
                                mySharedPref.setMediumID(MainUser.keyDataArray.get(i).getMediumID());
                                mySharedPref.setStandardID(MainUser.keyDataArray.get(i).getStandardID());
                                mySharedPref.setClassID(MainUser.keyDataArray.get(i).getClassID());
                                mySharedPref.setBranchID(MainUser.keyDataArray.get(i).getBranchID());
                                mySharedPref.setBatchID(MainUser.keyDataArray.get(i).getBatchID());
                                mySharedPref.setReg_key(MainUser.keyDataArray.get(i).getStudentKey());
                                mySharedPref.setVersion(MainUser.keyDataArray.get(i).getIsDemo());
                                mySharedPref.setStandardName(MainUser.keyDataArray.get(i).getStandardName());
                                mySharedPref.setClassName(MainUser.keyDataArray.get(i).getClassName());
                                mySharedPref.setBoardName(MainUser.keyDataArray.get(i).getBoardName());
                                mySharedPref.setMediumName(MainUser.keyDataArray.get(i).getMediumName());
                                mySharedPref.setClassLogo(MainUser.keyDataArray.get(i).getClassLogo());
                                mySharedPref.setClassLogo2(MainUser.keyDataArray.get(i).getSplashLogo());
                                mySharedPref.setClassRoundLogo(MainUser.keyDataArray.get(i).getClassRoundLogo());
                                mySharedPref.setCreatedOn(MainUser.keyDataArray.get(i).getCreatedOn());

                                mySharedPref.setlblPrviousTarget(MainUser.keyDataArray.get(i).getPreviousYearScore());

                            }

                            System.out.println("Sagar started printing  -------------------");
                            //        mySharedPref.printAll();

                            String fileName = MainUser.getProfile_image().substring(MainUser.getProfile_image().lastIndexOf("/") + 1);
                            String ClassLogoName = mySharedPref.getClassLogo().substring(mySharedPref.getClassLogo().lastIndexOf("/") + 1);
                            String fileNameCircle = MainUser.getProfile_image().substring(MainUser.getProfile_image().lastIndexOf("/") + 1);
                            String ClassLogoNameCircle = mySharedPref.getClassRoundLogo().substring(mySharedPref.getClassRoundLogo().lastIndexOf("/") + 1);


                            if (Utils.checkOutDated(mySharedPref.getExpiryDate())) {
                                Intent intent = new Intent(instance, DashBoardActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                new AlertDialog.Builder(this)
                                        .setTitle("Expired")
                                        .setMessage("Your iSccore Product has Expired, Kindly contact Parshwa Publications")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                                dialog.dismiss();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();


                            }

                        } else {
                            showProgress(false, true);
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        // Utils.showToast(jObj.getString("message"), DashBoardActivity.this);


                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
