package com.parshvaa.isccore.activity.mcq;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nineoldandroids.view.ViewHelper;
import com.parshvaa.isccore.activity.changestandard.ChooseOptionActivity;
import com.parshvaa.isccore.adapter.ChapterExpandableAdapter;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.ChapterInterface;
import com.parshvaa.isccore.model.Chapter;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.observscroll.ObservableExpandableListView;
import com.parshvaa.isccore.observscroll.ObservableScrollViewCallbacks;
import com.parshvaa.isccore.observscroll.ScrollState;
import com.parshvaa.isccore.observscroll.ScrollUtils;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.tempmodel.ChapterAccuracy;
import com.parshvaa.isccore.db.DBNewQuery;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.CustomDatabaseQuery;
import com.parshvaa.isccore.utils.ProgressBarAnimation;
import com.parshvaa.isccore.utils.Utils;

import java.io.File;
import java.util.ArrayList;

public class ChapterMcqActivity extends BaseActivity implements ObservableScrollViewCallbacks, ChapterInterface {

    public ChapterMcqActivity instance;
    private Toolbar mToolbar;
    private View mFlexibleSpaceView, mListBackgroundView, mOverlayView, mImageView;
    private int mActionBarSize, mFlexibleSpaceImageHeight;
    public ImageView img_back;
    public TextView tv_subName, tv_accuracy, tv_totalAttempts;
    public ImageView img_sub;
    public Chapter chapterObj;
    public Button btn_next;
    public CheckBox cbSelectAll;
    public ObservableExpandableListView expChapter;
    public ChapterExpandableAdapter chapterAdapter;
    private static final float MAX_TEXT_SCALE_DELTA = 0.3f;
    public ProgressBar pbar_total_accuracy;
    public ArrayList<ChapterAccuracy> chapterArray = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter_mcq);
        Utils.logUser();
        instance = this;
        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mActionBarSize = getActionBarSize();
        expChapter = findViewById(R.id.expChapter);
        mToolbar = findViewById(R.id.toolbar);
        mOverlayView = findViewById(R.id.overlay);
        mImageView = findViewById(R.id.image);
        img_sub = findViewById(R.id.img_sub);
        btn_next = findViewById(R.id.btn_next);
        pbar_total_accuracy = findViewById(R.id.pbar_total_accuracy);
        mListBackgroundView = findViewById(R.id.list_background);
        expChapter.setScrollViewCallbacks(this);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        img_back = findViewById(R.id.img_back);
        tv_subName = findViewById(R.id.tv_subName);
        tv_accuracy = findViewById(R.id.tv_accuracy);
        tv_totalAttempts = findViewById(R.id.tv_totalAttempts);
        if (App.mcqdata == null || App.mcqdata.subObj == null) {
            startActivity(new Intent(instance, SubjectMcqActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        } else {
            tv_subName.setText(App.mcqdata.subObj.getSubjectName());
            File image = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + App.mcqdata.subObj.getSubjectIcon());
            tv_accuracy.setText("Accuracy " + App.mcqdata.subObj.getAccuracy1() + "%");
            tv_totalAttempts.setText("Total Test " + App.mcqdata.subObj.getTotalSubjectTest());
            ProgressBarAnimation anim = new ProgressBarAnimation(pbar_total_accuracy, 0, App.mcqdata.subObj.getAccuracy1());
            anim.setDuration(1000);
            pbar_total_accuracy.startAnimation(anim);
            img_back.setOnClickListener(v -> onBackPressed());
            if (image.exists()) {
                img_sub.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
            }

            btn_next.setOnClickListener(v -> {
                if (chapterAdapter.getSelectedChaptersCount() == 0) {
                    Utils.showToast("Kindly select minimum 1 chapter.", instance);
                    return;
                }
                App.mcqdata.setSelctedChapID(chapterAdapter.getSelectedChapters());
                startActivity(new Intent(ChapterMcqActivity.this, DifficultyLevelActivity.class));
            });
            getChapter();
            addHeaderInList();
            addFooterInList();
            chapterAdapter = new ChapterExpandableAdapter(ChapterMcqActivity.this, expChapter, chapterArray, cbSelectAll);
            expChapter.setAdapter(chapterAdapter);
            expChapter.setOnGroupClickListener((parent, v, groupPosition, id) -> false);
        }
    }

    public void addHeaderInList() {
        View paddingView = new View(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        paddingView = inflater.inflate(R.layout.expand_header, null);
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,getResources().getDimensionPixelSize(R.dimen._200sdp));
        paddingView.setLayoutParams(lp);
        paddingView.setClickable(true);
        expChapter.addHeaderView(paddingView);
        cbSelectAll = paddingView.findViewById(R.id.cbSelectAll);
        cbSelectAll.setOnClickListener(view -> {
            if (mySharedPref.getVersion().equals("0")) {
                if (cbSelectAll.isChecked()) {
                    chapterAdapter.selectAll();
                    chapterAdapter.notifyDataSetChanged();
                } else {
                    chapterAdapter.deSelectAll();
                    chapterAdapter.notifyDataSetChanged();
                }
            } else {
                cbSelectAll.setChecked(false);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(ChapterMcqActivity.this);
                builder1.setMessage("Alert! To view this please purchase the complete version.");
                builder1.setCancelable(true);
                builder1.setNegativeButton("Close", (dialog, i) -> {
                    dialog.cancel();
                });
                builder1.setPositiveButton(
                        "Subscribe",
                        (dialog, id) -> {
                            dialog.cancel();
                            startActivity(new Intent(ChapterMcqActivity.this, ChooseOptionActivity.class));
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });


    }

    public void addFooterInList() {
        View paddingView = new View(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        paddingView = inflater.inflate(R.layout.expand_footer, null);
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,getResources().getDimensionPixelSize(R.dimen._50sdp));
        paddingView.setLayoutParams(lp);
        paddingView.setClickable(true);
        expChapter.addFooterView(paddingView);
    }

    public void getChapter() {
        try {
            chapterArray = (ArrayList) new CustomDatabaseQuery(this, new ChapterAccuracy())
                    .execute(new String[]{DBNewQuery.get_chapter_for_mcq_with_accuracy(App.mcqdata.subObj.getSubjectID())}).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (ChapterAccuracy main : chapterArray) {
            try {
                main.subChapters = (ArrayList) new CustomDatabaseQuery(this, new ChapterAccuracy())
                        .execute(new String[]{DBNewQuery.get_child_chapter_for_mcq(main.getChapterID())}).get();
                if (main.subChapters.size() < 1) {
                    main.subChapters.add(main);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCheckBoxDeselect(boolean b) {
//        if (mySharedPref.getVersion().equals("0")) {
            cbSelectAll.setChecked(b);
//        } else {
//            cbSelectAll.setChecked(false);
//            AlertDialog.Builder builder1 = new AlertDialog.Builder(ChapterMcqActivity.this);
//            builder1.setMessage("Alert! To view this please purchase the complete version.");
//            builder1.setCancelable(true);
//            builder1.setNegativeButton("Close", (dialog, i) -> {
//                dialog.cancel();
//            });
//            builder1.setPositiveButton(
//                    "Subscribe",
//                    (dialog, id) -> {
//                        dialog.cancel();
//                        startActivity(new Intent(ChapterMcqActivity.this, ChooseOptionActivity.class));
//                    });
//
//            AlertDialog alert11 = builder1.create();
//            alert11.show();
//        }

    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mFlexibleSpaceView, -scrollY);

        float flexibleRange = mFlexibleSpaceImageHeight - mActionBarSize;
        int minOverlayTransitionY = mActionBarSize - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));

        // Translate list background
        ViewHelper.setTranslationY(mListBackgroundView, Math.max(0, -scrollY + mFlexibleSpaceImageHeight));

        // Change alpha of overlay
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));

        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 180);

        mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}

