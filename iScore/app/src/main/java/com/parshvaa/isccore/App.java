package com.parshvaa.isccore;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.net.Uri;
import android.os.StrictMode;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.onesignal.OneSignal;
import com.parshvaa.isccore.activity.library.PersonalFiles;
import com.parshvaa.isccore.activity.library.SystemFiles;
import com.parshvaa.isccore.activity.SplashActivity;
import com.parshvaa.isccore.model.CctReport;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.model.Subject;
import com.parshvaa.isccore.tempmodel.McqData;
import com.parshvaa.isccore.tempmodel.PractiesPaper;

import com.parshvaa.isccore.db.MyDBManager;
import com.parshvaa.isccore.utils.MySharedPref;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.HttpUrlConnectionDownloader;
import com.tonyodev.fetch2core.Downloader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by admin on 10/12/2016.
 */
public class App extends MultiDexApplication {
    private static final String TAG = "App";
    public static App app;
    public static MainUser mainUser;
    public static String app_Key = "", UserType = "student", bannerImage = "", categoryImage = "", Image = "";
    public static boolean isFarst = true;
    public static String myPref = "isccore_pref";
    public static String image_count = "";
    public static MySharedPref mySharedPref;
    public static String Target = "";
    public static int badgeCount = 0;
    public static String Otp = "";
    public static boolean isFromNewRegister = false;
    public static HashMap<Integer, Boolean> flags = new HashMap<>();
    public static McqData mcqdata;
    public static Subject subObj;
    public static PractiesPaper practiesPaperObj;
    public static ArrayList<CctReport> cctArray = new ArrayList<>();
    public static int cctTotal = 0, cctTotalAccuracy = 0, mcqTotalAccuracy = 0, McqTotal = 0, PractisePaperTotal = 0;
    public static ArrayList<String> checked_level = new ArrayList<>();
    public static ArrayList<String> checked_subject = new ArrayList<>();
    public static ArrayList<String> checked_paper_type = new ArrayList<>();
    public static Uri selectedUri;
    public static List<Address> addresses;
    public static boolean MCQactive = false;
    public static FetchConfiguration fetchConfiguration;
    public int notiCount = 0, NotificationID;

    public static Context mContext;
    public static boolean isNoti = false;
    public static String isType = "";
    public static ArrayList<SystemFiles> video_list = new ArrayList<>();
    public static ArrayList<PersonalFiles> perrsonal_video_list = new ArrayList<>();
    public String bigPicture;
    public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";
    private MyDBManager mDb;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Fresco.initialize(getApplicationContext());

        mContext = this;
        mDb = MyDBManager.getInstance(mContext);
        mDb.open(mContext);
        if (!BuildConfig.DEBUG)
            Fabric.with(new Fabric.Builder(this).debuggable(true).kits(new Crashlytics()).build());

        fetchConfiguration = new FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(1)
                .enableRetryOnNetworkGain(true)
                .enableFileExistChecks(true)
                .enableLogging(true)
                .setHttpDownloader(new HttpUrlConnectionDownloader(Downloader.FileDownloaderType.PARALLEL))
                .build();


        OneSignal.startInit(this)
                .setNotificationOpenedHandler(result -> {
                    try {
                        isNoti = true;
                        JSONObject jsonObject = new JSONObject();
                        jsonObject = result.toJSONObject();
                        //  //Log.i("One Signal", "==> " + result.stringify());
                        JSONObject jNoti = jsonObject.getJSONObject("notification");
                        JSONObject jpayload = jNoti.getJSONObject("payload");
                        NotificationID = jNoti.getInt("androidNotificationId");
                        JSONObject additionalData = jpayload.getJSONObject("additionalData");
                        if (additionalData.getString("type").equalsIgnoreCase("cct")) {
                            isType = "cct";
                        } else if (additionalData.getString("type").equalsIgnoreCase("subscribe")) {
                            isType = "subscribe";
                        } else if (additionalData.getString("type").equalsIgnoreCase("referral")) {
                            isType = "referral";
                        } else if (additionalData.getString("type").equalsIgnoreCase("notice_board")) {
                            isType = "notice_board";
                        } else if (additionalData.getString("type").equalsIgnoreCase("test_paper")) {
                            isType = "test_paper";
                        } else if (additionalData.getString("type").equalsIgnoreCase("library")) {
                            isType = "library";
                        } else if (additionalData.getString("type").equalsIgnoreCase("zooki")) {
                            isType = "zooki";
                        } else if (additionalData.getString("type").equalsIgnoreCase("event")) {
                            isType = "event";
                        } else if (additionalData.getString("type").equalsIgnoreCase("event_register")) {
                            isType = "event_register";
                        } else if (additionalData.getString("type").equalsIgnoreCase("refernearn")) {
                            isType = "refernearn";
                        } else if (additionalData.getString("type").equalsIgnoreCase("mcq")) {
                            isType = "mcq";
                        }else if (additionalData.getString("type").equalsIgnoreCase("practice")) {
                            isType = "practice";
                        }else if (additionalData.getString("type").equalsIgnoreCase("notes")) {
                            isType = "notes";
                        } else if (additionalData.getString("type").equalsIgnoreCase("dashboard")) {
                            isType = "dashboard";
                        } else if (additionalData.getString("type").equalsIgnoreCase("callnow")) {
                            isType = "callnow";
                        } else {
                            isType = "1";
                        }
                        bigPicture = jpayload.has("bigPicture") ? jpayload.getString("bigPicture") : "";
                        Intent intent = new Intent(mContext, SplashActivity.class);
                        intent.putExtra("Notification_type_id", additionalData.getString("notification_type_id"));
                        intent.putExtra("Event_id", additionalData.getString("Event_id"));
                        intent.putExtra("paper_type", additionalData.getString("paper_type"));
                        intent.putExtra("NotificationID", NotificationID);
                        intent.putExtra("title", jpayload.getString("title"));

                        intent.putExtra("bigPicture", bigPicture);
                        intent.putExtra("created_on", additionalData.getString("created_on"));
                        intent.putExtra("body", jpayload.getString("body"));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                })
                .setNotificationReceivedHandler(notification -> {
                    Log.d(TAG, "onCreate: notification --> " + new Gson().toJson(notification));
                })

                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();
//        OneSignal.setRequiresUserPrivacyConsent(true);
        mySharedPref = new MySharedPref(this);
        app = this;
        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this);
    }

    public static void deleteCache(Context context) {
        deleteDir(context.getCacheDir());
    }

    //------------------- Noti Bubble ------------------- //
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.app = instance;
    }

    public static App getInstance() {
        return app;
    }
}