package com.parshvaa.isccore.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.parshvaa.isccore.App;
import com.parshvaa.isccore.custominterface.AsynchTaskListner;
import com.parshvaa.isccore.R;
import com.parshvaa.isccore.model.KeyData;
import com.parshvaa.isccore.model.MainData;
import com.parshvaa.isccore.model.MainUser;
import com.parshvaa.isccore.observscroll.BaseActivity;
import com.parshvaa.isccore.utils.CallRequest;
import com.parshvaa.isccore.utils.Constant;
import com.parshvaa.isccore.utils.FileDownloader;
import com.parshvaa.isccore.utils.JsonParserUniversal;
import com.parshvaa.isccore.utils.Utils;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegistrationConfirmActivity extends BaseActivity implements AsynchTaskListner {

    private static final String TAG = "RegistrationConfirmActi";

    public Button btn_confirm;
    public LinearLayout lin_locataion;
    public ImageView img_back;
    public TextView tv_standard,tv_SchoolName, tv_medium, tv_board, tv_mobile, tv_email, tv_name, tv_location;

    public App app;
    public JsonParserUniversal jParser;
    public CircleImageView img_profile;

    public String first_name, last_name, email, mobileNo, boardName, meduimName, standardName,
            boardId, mediumId, standardId, SchoolName="",version = "", address = "", Guid = "", PleyaerID = "";

    public Handler customHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_registration_confirm);
        Utils.logUser();
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        int verCode = Utils.currentAppVersionCode(this);
        version = String.valueOf(verCode);
        PleyaerID = Utils.OneSignalPlearID();
        if (!mySharedPref.getGuuid().isEmpty()) {
            Guid = mySharedPref.getGuuid();
        } else {
            Guid = UUID.randomUUID().toString();
        }
        img_back = findViewById(R.id.img_back);
        lin_locataion = findViewById(R.id.lin_locataion);
        img_profile = findViewById(R.id.img_profile);
        tv_name = findViewById(R.id.tv_name);
        tv_SchoolName= findViewById(R.id.tv_SchoolName);
        tv_board = findViewById(R.id.tv_board);
        tv_email = findViewById(R.id.tv_email);
        tv_mobile = findViewById(R.id.tv_mobile);
        tv_medium = findViewById(R.id.tv_medium);
        tv_standard = findViewById(R.id.tv_standard);
        tv_location = findViewById(R.id.tv_location);
        lin_locataion.setVisibility(View.GONE);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_location.setVisibility(View.GONE);
        first_name = MainUser.getFirstName();
        last_name = MainUser.getLastName();
        email = MainUser.getEmailID();
        mobileNo = MainUser.getRegMobNo();
        boardName = MainUser.getBoardName();
        meduimName = MainUser.getMediumName();
        standardName = MainUser.getStanderdName();
        mediumId = MainUser.getMediumID();
        SchoolName = MainUser.getSchoolName();
        boardId = MainUser.getBoardID();
        standardId = MainUser.getStandardID();
        lin_locataion.setVisibility(View.GONE);
        tv_name.setText(first_name + " " + last_name);

        tv_board.setText(boardName);
        tv_email.setText(email);
        tv_mobile.setText(mobileNo);
        tv_medium.setText(meduimName);
        tv_SchoolName.setText(SchoolName);
        tv_standard.setText(standardName);
        if (App.selectedUri != null) {
            img_profile.setImageURI(App.selectedUri);
        }
        btn_confirm = findViewById(R.id.btn_confirm);
        try {
            if (App.addresses.size() >= 2) {
                address = App.addresses.get(1).getAddressLine(0);
            } else {
                if (App.addresses.size() > 0)
                    address = App.addresses.get(0).getAddressLine(0);
            }
            tv_location.setText(address);
        } catch (Exception e) {
            address = "TESTING";
            e.printStackTrace();
        }

        btn_confirm.setOnClickListener(v -> {
            if (Utils.isNetworkAvailable(RegistrationConfirmActivity.this)) {
                btn_confirm.setEnabled(false);
                if (App.isFromNewRegister) {
                    showProgress(true, false);
                    if (PleyaerID.equalsIgnoreCase("")) {
                        customHandler.postDelayed(updateTimerThread, 0);
                    } else {
                        new CallRequest(RegistrationConfirmActivity.this).directRegister(first_name, last_name, mobileNo,SchoolName, email,
                                boardId, mediumId, standardId, "", "", "", "", Guid, App.Image, App.UserType
                                , address, PleyaerID);
                    }
                } else {
                    showProgress(true, false);//Utils.showProgressDialog(instance);
                    if (PleyaerID.equalsIgnoreCase("")) {
                        customHandler.postDelayed(updateTimerThread, 0);
                    } else {
                        new CallRequest(RegistrationConfirmActivity.this).register(first_name, last_name, mobileNo, email,
                                MainUser.getBoardID(), MainUser.getMediumID(), MainUser.getStandardID(),
                                MainUser.getBatchID(), MainUser.getClassID(), MainUser.getBranchID(),
                                MainUser.getRegKey(),SchoolName, MainUser.getKeyType(), Guid, App.Image, App.UserType
                                , address, PleyaerID);
                    }
                }
            } else {
                Utils.showToast("Please connect internet to Register", RegistrationConfirmActivity.this);
            }

        });
    }

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            if (!PleyaerID.equalsIgnoreCase("")) {
                if (App.isFromNewRegister) {
                    new CallRequest(RegistrationConfirmActivity.this).directRegister(first_name, last_name, mobileNo,SchoolName, email,
                            boardId, mediumId, standardId, "", "", "", "", Guid, App.Image, App.UserType
                            , address, PleyaerID);
                    return;
                } else {
                    new CallRequest(RegistrationConfirmActivity.this).register(first_name, last_name, mobileNo, email,
                            MainUser.getBoardID(), MainUser.getMediumID(), MainUser.getStandardID(),
                            MainUser.getBatchID(), MainUser.getClassID(), MainUser.getBranchID(),
                            MainUser.getRegKey(),SchoolName, MainUser.getKeyType(), Guid, App.Image, App.UserType
                            , address, PleyaerID);
                    return;
                }
            } else {
                PleyaerID = Utils.OneSignalPlearID();
                customHandler.postDelayed(this, 1000);
            }

        }
    };

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.d(TAG, "onTaskCompleted: " + result);
            switch (request) {

                case getTableRecords:
                    btn_confirm.setEnabled(true);
                    showProgress(false, true);
                    try {
                        MainData mainData = LoganSquare.parse(result, MainData.class);
                        if (mainData.isStatus()) {
                            prefs.save(Constant.tableRecords, gson.toJson(mainData.getData()));
                            tableRecords = mainData.getData();
                        }
                        showProgress(false, true);
                        callPage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case register:
                    btn_confirm.setEnabled(true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");
                            if (jData != null && jData.length() > 0) {
                                MainUser.setStudentId(jData.getString("StudentID"));
                                MainUser.setStudentId(jData.getString("StudentID"));
                                MainUser.setFirstName(jData.getString("FirstName"));
                                MainUser.setLastName(jData.getString("LastName"));
                                MainUser.setGuid(jData.getString("IMEINo"));
                                MainUser.setEmailID(jData.getString("EmailID"));
                                MainUser.setRegMobNo(jData.getString("RegMobNo"));
                                MainUser.setSchoolName(jData.getString("SchoolName"));
                                MainUser.setStudentCode(jData.getString("StudentCode"));
                                DataSetInSherdPref(jData);
                                showProgress(false, true);
                                if (Utils.isNetworkAvailable(this)) {
                                    showProgress(true, false);
                                    new CallRequest(this).getTableRecords();
                                } else {
                                    callPage();
                                }
                            }
                        } else {
                            showProgress(false, true);
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        showProgress(false, true);
                        Utils.showToast("Please try again later", this);
                        e.printStackTrace();
                    }
                    break;
                case direct_register:
                    btn_confirm.setEnabled(true);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");
                            if (jData != null && jData.length() > 0) {

                                MainUser.setStudentId(jData.getString("StudentID"));
                                MainUser.setFirstName(jData.getString("FirstName"));
                                MainUser.setLastName(jData.getString("LastName"));
                                MainUser.setGuid(jData.getString("IMEINo"));
                                MainUser.setEmailID(jData.getString("EmailID"));
                                MainUser.setRegMobNo(jData.getString("RegMobNo"));
                                MainUser.setSchoolName(jData.getString("SchoolName"));
                                MainUser.setStudentCode(jData.getString("StudentCode"));

                                DataSetInSherdPref(jData);
                                showProgress(false, true);
                                if (Utils.isNetworkAvailable(this)) {
                                    showProgress(true, false);
                                    new CallRequest(this).getTableRecords();
                                } else {
                                    callPage();
                                }
                            }
                        } else {
                            showProgress(false, true);
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        showProgress(false, true);
                        Utils.showToast("Please try again later", this);
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }

    private void callPage() {
        mySharedPref.setIsLoggedIn(true);
        mySharedPref.setDownloadStatus("n");
        mySharedPref.setDownloadStatus("y");
        mySharedPref.setIsProperLogin("true");

        startActivity(new Intent(RegistrationConfirmActivity.this, DashBoardActivity.class)
                .addCategory(Intent.CATEGORY_HOME)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }

    public void DataSetInSherdPref(JSONObject jData) {
        mySharedPref.setStudentCode(MainUser.getStudentCode());
        mySharedPref.setStudent_id(MainUser.getStudentId());
        mySharedPref.setFirst_name(MainUser.getFirstName());
        mySharedPref.setLast_name(MainUser.getLastName());
        mySharedPref.setGuuid(MainUser.getGuid());
        mySharedPref.setEmailID(MainUser.getEmailID());
        mySharedPref.setRegMobNo(MainUser.getRegMobNo());
        try {
            mySharedPref.setProfileImage(jData.getString("ProfileImage"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mySharedPref.setVersioCode(version);
        mySharedPref.setIsLoggedIn(true);
        mySharedPref.setDownloadStatus("n");
        mySharedPref.setIsProperLogin("true");

        //copyFile(inputpath, fileName, Constant.LOCAL_IMAGE_PATH + "/profile/");
        try {
            JSONArray jsonArray = jData.getJSONArray("KeyData");
            KeyData keydata;
            for (int i = 0; i < jsonArray.length(); i++) {
                keydata = (KeyData) jParser.parseJson(jsonArray.getJSONObject(i), new KeyData());
                MainUser.keyDataArray.add(keydata);
                MainUser.setRegKey(MainUser.keyDataArray.get(i).getStudentKey());
                mySharedPref.setStudent_id(MainUser.keyDataArray.get(i).getStudentID());
                mySharedPref.setBoardID(MainUser.keyDataArray.get(i).getBoardID());
                mySharedPref.setMediumID(MainUser.keyDataArray.get(i).getMediumID());
                mySharedPref.setStandardID(MainUser.keyDataArray.get(i).getStandardID());
                mySharedPref.setClassID(MainUser.keyDataArray.get(i).getClassID());
                mySharedPref.setBranchID(MainUser.keyDataArray.get(i).getBranchID());
                mySharedPref.setBatchID(MainUser.keyDataArray.get(i).getBatchID());
                mySharedPref.setReg_key(MainUser.keyDataArray.get(i).getStudentKey());
                mySharedPref.setVersion(MainUser.keyDataArray.get(i).getIsDemo());
                mySharedPref.setStandardName(MainUser.keyDataArray.get(i).getStandardName());
                mySharedPref.setClassName(MainUser.keyDataArray.get(i).getClassName());
                mySharedPref.setBoardName(MainUser.keyDataArray.get(i).getBoardName());
                mySharedPref.setMediumName(MainUser.keyDataArray.get(i).getMediumName());
                mySharedPref.setClassLogo(MainUser.keyDataArray.get(i).getClassLogo());
                mySharedPref.setClassLogo2(MainUser.keyDataArray.get(i).getSplashLogo());
                mySharedPref.setClassRoundLogo(MainUser.keyDataArray.get(i).getClassRoundLogo());
                mySharedPref.setlblPrviousTarget(MainUser.keyDataArray.get(i).getPreviousYearScore());
                mySharedPref.setCreatedOn(MainUser.keyDataArray.get(i).getCreatedOn());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
