package com.parshvaa.isccore.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 5/23/2018.
 */

@JsonObject
public class MasterPassageSubQuestion implements Serializable {


    @JsonField
    public String MPSQID = "", MQuestionID = "", BetaMQuestionID = "", SubQuestionTypeID = "", PassageSubQuestionTypeID = "",
            Question = "", Answer = "", CreatedBy = "", CreatedOn = "", ModifiedBy = "", ModifiedOn = "";

    public String getMPSQID() {
        return MPSQID;
    }

    public void setMPSQID(String MPSQID) {
        this.MPSQID = MPSQID;
    }

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public String getBetaMQuestionID() {
        return BetaMQuestionID;
    }

    public void setBetaMQuestionID(String betaMQuestionID) {
        BetaMQuestionID = betaMQuestionID;
    }

    public String getSubQuestionTypeID() {
        return SubQuestionTypeID;
    }

    public void setSubQuestionTypeID(String subQuestionTypeID) {
        SubQuestionTypeID = subQuestionTypeID;
    }

    public String getPassageSubQuestionTypeID() {
        return PassageSubQuestionTypeID;
    }

    public void setPassageSubQuestionTypeID(String passageSubQuestionTypeID) {
        PassageSubQuestionTypeID = passageSubQuestionTypeID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
